<?php
//Date
//เลือกข้อมูลทั้งหมด
define("Date_All_Data", "SELECT HRTIME_DB.D_transaction.PERID, HRTIME_DB.D_transaction.event_time, STAFF.Medperson.NAME, STAFF.Medperson.SURNAME, STAFF.Depart.Dep_name, STAFF.Depart.Dep_Group_name
FROM HRTIME_DB.D_transaction
JOIN STAFF.Medperson
ON HRTIME_DB.D_transaction.PERID = STAFF.Medperson.PERID
JOIN STAFF.Depart
ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
WHERE HRTIME_DB.D_transaction.PERID != 0
ORDER BY  STAFF.Depart.Dep_Group_name ASC");

//เลือกข้อมูลเฉพาะหน่วยงาน
// define("Date_Select_Depart_Data", "SELECT HRTIME_DB.D_transaction.PERID,HRTIME_DB.D_transaction.door_id, HRTIME_DB.D_transaction.event_time, STAFF.Medperson.NAME, STAFF.Medperson.SURNAME, STAFF.Depart.Dep_Code, STAFF.Depart.Dep_name, STAFF.Depart.Dep_Group_name
// FROM HRTIME_DB.D_transaction
// JOIN STAFF.Medperson
// ON HRTIME_DB.D_transaction.PERID = STAFF.Medperson.PERID
// JOIN STAFF.Depart
// ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
// WHERE HRTIME_DB.D_transaction.PERID != 0
// AND Dep_Code
// AND HRTIME_DB.D_transaction.event_time LIKE "2018-12-19%"
// ORDER BY  STAFF.Depart.Dep_Group_name,HRTIME_DB.D_transaction.event_time ASC");

//เลือกค่าหน่วยงานทั้งหมด
// define("Date_Get_Department","SELECT Dep_code,Dep_name,Dep_Group_name
// FROM Depart");

define("Date_Get_Department","SELECT STAFF.Depart.Dep_Code,STAFF.Depart.Edit_code,STAFF.Depart.Dep_name,STAFF.Depart.Dep_Group_name
FROM STAFF.Depart
ORDER BY STAFF.Depart.Dep_name ASC
");

//---------------------------------------------------------------------------------------//
//Login
//ตรวจสอบการ Login
define("CheckLogin", "select * from ".constant('DBTABLEMEDPERSON'));
//---------------------------------------------------------------------------------------//


//---------------------------------------------------------------------------------------//
?>