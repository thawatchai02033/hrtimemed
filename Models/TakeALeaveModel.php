<?php
class TakeALeaveModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function GETALLDEPARTMENT()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรภายในหน่วยงาน
    public function GETPERSONFROMDEPART($DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.POS_LEVEL,
            A.Date_In,
            B.Dep_name,
            B.Dep_Code,
            C.PosName
            FROM
            STAFF.Medperson AS A
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
            LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
            WHERE
            B.Dep_Code = " . $DepCode[$i]->Dep_Code . "  AND
            A.CSTATUS != 0 AND
            A.PERID NOT IN (SELECT PERID FROM HRTIME_DB.c_medperson WHERE HRTIME_DB.c_medperson.Dep_Code_Old = " . $DepCode[$i]->Dep_Code . ")
            ORDER BY
            A.PERID ASC
            ");

                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        array_push($myArray, $data);

                    }

                    $queryOtherPerson = mysqli_query($this->db->hostDB, "
            SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.POS_LEVEL,
            A.Date_In,
            B.Dep_name,
            B.Dep_Code,
            C.PosName
            FROM
            HRTIME_DB.c_medperson AS D
            LEFT JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
            LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
            WHERE
            D.Dep_Code = " . $DepCode[$i]->Dep_Code . "  AND
            A.CSTATUS != 0
            ORDER BY
            A.PERID ASC
            ");

                    if ($queryOtherPerson) {
                        while ($dataOhterPerson = mysqli_fetch_assoc($queryOtherPerson)) {
                            array_push($myArray, $dataOhterPerson);
                        }
                    }

                } else {

                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLCOUNTRY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.*
        FROM
            HRTIME_DB.tbl_country AS A");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกมูลบุคลากรหน่วยงานที่เกี่ยวข้อง
    public function GETALLDATAPERSON()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                STAFF.Medperson.PERID,
                STAFF.Medperson.`NAME`,
                STAFF.Medperson.SURNAME,
                STAFF.Medperson.Iname,
                STAFF.Medperson.TITLE,
                STAFF.Medperson.DEP_WORK,
                STAFF.Medperson.NewPos,
                STAFF.Depart.Dep_name,
                STAFF.Positions.PosName
                FROM
                STAFF.Medperson
                INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
                INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
                WHERE
                STAFF.Medperson.CSTATUS != 0
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLDATAPERSONDep()
    {
        $myArray = array();
        if ($this->db->hostDB) {

            $queryPersonalOther = mysqli_query($this->db->hostDB, "
                        SELECT
                        B.PERID,
                        B.`NAME`,
                        B.SURNAME,
                        B.SEX,
                        B.TITLE,
                        B.NewPos,
                        D.Dep_name,
                        D.Dep_Group_name,
                        A.Dep_Code,
                        D.Edit_code,
                        C.PosName
                    FROM
                        HRTIME_DB.c_medperson AS A
                    INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                    INNER JOIN STAFF.Positions AS C ON B.NewPos = C.PosCode
                    INNER JOIN STAFF.Depart AS D ON B.DEP_WORK = D.Dep_Code
                    WHERE
                        B.PERID = " . $_SESSION['PERID']);

            if (mysqli_num_rows($queryPersonalOther) > 0) {
                while ($DepData = mysqli_fetch_assoc($queryPersonalOther)) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Edit_code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                     A.Dep_Code = " . $DepData['Dep_Code'] . "
                    ORDER BY
                    A.Dep_name ASC
                    limit 1
                    ");
                    if ($query) {
                        while ($Dep_data = mysqli_fetch_assoc($query)) {
                            $query = mysqli_query($this->db->hostDB, "SELECT
                STAFF.Medperson.PERID,
                STAFF.Medperson.`NAME`,
                STAFF.Medperson.SURNAME,
                STAFF.Medperson.Iname,
                STAFF.Medperson.TITLE,
                STAFF.Medperson.DEP_WORK,
                STAFF.Medperson.NewPos,
                STAFF.Depart.Dep_name,
                STAFF.Positions.PosName
                FROM
                STAFF.Medperson
                INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
                INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
                WHERE
                STAFF.Medperson.CSTATUS != 0
                AND 
                STAFF.Depart.Edit_code = " . $Dep_data['Edit_code'] . "
                ");
                            if ($query) {
                                while ($data = mysqli_fetch_assoc($query)) {

                                    $myArray[] = $data;

                                }
                            } else {
                            }
                            $myJSON = json_encode($myArray);
                        }
                    } else {
                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Edit_code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = " . $_SESSION['PERID'] . "
                    ORDER BY
                    A.Dep_name ASC
                    limit 1
                    ");
                if ($query) {
                    while ($Dep_data = mysqli_fetch_assoc($query)) {
                        $query = mysqli_query($this->db->hostDB, "SELECT
                STAFF.Medperson.PERID,
                STAFF.Medperson.`NAME`,
                STAFF.Medperson.SURNAME,
                STAFF.Medperson.Iname,
                STAFF.Medperson.TITLE,
                STAFF.Medperson.DEP_WORK,
                STAFF.Medperson.NewPos,
                STAFF.Depart.Dep_name,
                STAFF.Positions.PosName
                FROM
                STAFF.Medperson
                INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
                INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
                WHERE
                STAFF.Medperson.CSTATUS != 0
                AND 
                STAFF.Depart.Edit_code = " . $Dep_data['Edit_code'] . "
                ");
                        if ($query) {
                            while ($data = mysqli_fetch_assoc($query)) {

                                $myArray[] = $data;

                            }
                        } else {
                        }
                        $myJSON = json_encode($myArray);
                    }
                } else {
                }
            }
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลชนิดของการลา
    public function GETLEAVETYPE()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave.Leave_Type,
            HRTIME_DB.c_leave.Leave_Detail
            FROM
            HRTIME_DB.c_leave
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }
    //เรียกข้อมูลประเภทวันลา
    public function GETDAYTYPE()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave_day.L_Day_Type,
            HRTIME_DB.c_leave_day.L_Day_Detail
            FROM
            HRTIME_DB.c_leave_day
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลการลาของบุคลากรทุกคนในหน่วยงาน
    public function GETMORELEAVE($DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.`NAME`,
            B.SURNAME,
            B.Iname,
            B.TITLE,
            C.Dep_Code,
            C.Dep_name,
            C.Dep_Group_name,
            D.Leave_Detail,
            E.PosName,
            C.Edit_code
            FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
            INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
            INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
            WHERE
            C.Edit_code = " . $DepCode[$i]->Dep_Code . "
            ORDER BY
            A.No_Id DESC
            ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $myArray[] = $data;
                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เพิ่มข้อมูลการลาโดยเช็คข้อมูลของวันที่เริ่มต้นและวันที่สิ้นสุดถ้ามีข้อมูลอยู่แล้วให้ส่งสถานะเป็น false กลับไป
    public function INSERTDATALEAVEPERSON($DayTypeEnd, $DayTypeStart, $DepCode, $Leave_Date_End, $Leave_Date_Start, $Leave_Reason, $Leave_TypeS, $PerId, $PerId_Create, $DateSum, $DateSumWork, $Country, $approver)
    {

        $dateResult = date("d-m-Y");

        list($yearS, $monthS, $dayS) = explode('-', $Leave_Date_Start);

        if ($monthS < 10) {
            $monthS = '0'.$monthS;
        }

        list($yearE, $monthE, $dayE) = explode('-', $Leave_Date_End);

        if ($monthE < 10) {
            $monthE = '0'.$monthE;
        }

        $myArray = array();
        if ($this->db->hostDB) {

            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " .$_SESSION['PERID']
            );
            if($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {

                    $queryCheckApproveDay = mysqli_query($this->db->hostDB2, "SELECT * FROM HR_CENTER.`working_summary_tb` where classify = '3' AND perid = '" . $PerId . "' AND monthly BETWEEN '".$yearS."-".$monthS."-01' AND '".$yearE."-".$monthE."-01'");

                    if (mysqli_num_rows($queryCheckApproveDay) == 0) {

                            $query = mysqli_query($this->db->hostDB, "SELECT
            c_take_a_leave.No_Id,
            c_take_a_leave.T_Leave_Type,
            c_take_a_leave.T_Leave_Reason,
            c_take_a_leave.T_Leave_Date_Start,
            c_take_a_leave.T_Leave_Date_End,
            c_take_a_leave.T_Day_Type_Start,
            c_take_a_leave.T_Day_Type_End,
            c_take_a_leave.T_Leave_CreateBY,
            c_take_a_leave.T_Leave_UpdateBY,
            c_take_a_leave.T_Leave_CreateT,
            c_take_a_leave.T_Leave_UpdateT,
            c_take_a_leave.PERID,
            c_take_a_leave.Dep_code
            FROM
            c_take_a_leave
            WHERE
            c_take_a_leave.PERID = " . $PerId . " AND
            c_take_a_leave.T_Leave_Date_Start
            BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'
            UNION
            SELECT
            c_take_a_leave.No_Id,
            c_take_a_leave.T_Leave_Type,
            c_take_a_leave.T_Leave_Reason,
            c_take_a_leave.T_Leave_Date_Start,
            c_take_a_leave.T_Leave_Date_End,
            c_take_a_leave.T_Day_Type_Start,
            c_take_a_leave.T_Day_Type_End,
            c_take_a_leave.T_Leave_CreateBY,
            c_take_a_leave.T_Leave_UpdateBY,
            c_take_a_leave.T_Leave_CreateT,
            c_take_a_leave.T_Leave_UpdateT,
            c_take_a_leave.PERID,
            c_take_a_leave.Dep_code
            FROM
            c_take_a_leave
            WHERE
            c_take_a_leave.PERID = " . $PerId . " AND
            c_take_a_leave.T_Leave_Date_End
            BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'");
                            if ($query) {
                                if ($query->num_rows == 0) {
                                    $query = mysqli_query($this->db->hostDB, "INSERT INTO c_take_a_leave (
                        T_Leave_Type,
                        T_Leave_Reason,
                        T_Leave_Date_Start,
                        T_Leave_Date_End,
                        T_Day_Type_Start,
                        T_Day_Type_End,
                        T_All_Summary,
                        T_Work_Summary,
                        T_Leave_CreateBY,
                        T_Leave_UpdateBY,
                        T_Leave_CreateT,
                        T_Leave_UpdateT,
                        PERID,
                        Dep_code,
                        T_Status_Leave,
                        Country,
                        AgentPerson
                    )
                    VALUES
                        (
                            " . $Leave_TypeS . ",
                            '" . $Leave_Reason . "',
                            '" . $Leave_Date_Start . "',
                            '" . $Leave_Date_End . "',
                            '" . $DayTypeStart . "',
                            '" . $DayTypeEnd . "',
                            " . $DateSum . ",
                            " . $DateSumWork . ",
                            '" . $PerId_Create . "',
                            '" . $PerId_Create . "',
                            CURRENT_TIMESTAMP,
                            CURRENT_TIMESTAMP,
                            '" . $PerId . "',
                            " . $DepCode . ",
                            '0',
                             '" . $Country . "',
                              '" . $approver . "'
                        )");
                                    if ($query) {
                                        ////////////////////////////////////
                                        /// Query ไม่เหมือนกับตัว USER
                                        /// /////////////////////////
                                        $queryLineNoti = mysqli_query($this->db->hostDB, "SELECT
                            A.id,
                            A.perid,
                            A.line_token,
                            A.process,
                            A.group_name,
                            C.`NAME`,
                            C.SURNAME,
                            B.Perid,
                            D.Dep_name,
                            D.Edit_code,
                            D.Dep_Group_name,
                            B.Dep_code
                            FROM
                            STAFF.D_admin AS B
                            LEFT JOIN HR_CENTER.line_token AS A ON B.Perid = A.perid
                            LEFT JOIN STAFF.Medperson AS C ON C.PERID = B.Perid
                            LEFT JOIN STAFF.Depart AS D ON D.Dep_Code = C.DEP_WORK
                            WHERE
                            B.Dep_code = $DepCode
                            AND C.CSTATUS != 0
                            ");
                                        if ($queryLineNoti) {
                                            if ($queryLineNoti->num_rows > 0) {
                                                while ($dataLineNoti = mysqli_fetch_assoc($queryLineNoti)) {
                                                    $queryPersonal = mysqli_query($this->db->hostDB, "SELECT
                                    STAFF.Medperson.PERID,
                                    STAFF.Medperson.`NAME`,
                                    STAFF.Medperson.SURNAME
                                    FROM
                                    STAFF.Medperson
                                    WHERE
                                    STAFF.Medperson.PERID = $PerId
                                    ");
                                                    if ($queryPersonal) {
                                                        while ($dataPersonal = mysqli_fetch_assoc($queryPersonal)) {
                                                            if ($dataLineNoti["line_token"] != '' || $dataLineNoti["line_token"] != null) {
                                                                define('LINE_API', "https://notify-api.line.me/api/notify");

                                                                $token = $dataLineNoti["line_token"]; //ใส่Token ที่copy เอาไว้
                                                                switch ($Leave_TypeS){
                                                                    case '0': $Type = 'ลากิจส่วนตัว';
                                                                        break;
                                                                    case  '1': $Type = 'ลาป่วย';
                                                                        break;
                                                                    case  '2': $Type = 'ลาพักผ่อน';
                                                                        break;
                                                                    case  '3': $Type = 'ลาศึกษาต่อ';
                                                                        break;
                                                                    case  '4': $Type = 'ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย';
                                                                        break;
                                                                    case  '5': $Type = 'ลาคลอดบุตร';
                                                                        break;
                                                                    case  '6': $Type = 'ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์';
                                                                        break;
                                                                    case  '7': $Type = 'ไปราชการ';
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }

                                                                $message = "แจ้งการลา คุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " ( " . $Type . " ) ตั้งแต่วันที่ " . $this->DateToThai($Leave_Date_Start) . " ( " . $DayTypeStart . " ) " . $this->DateToThai($Leave_Date_End) . " ( " . $DayTypeEnd . " ) เหตุผล ".$Leave_Reason; //ข้อความที่ต้องการส่ง สูงสุด 1000 ตัวอักษร

                                                                $queryData = array('message' => $message);
                                                                $queryData = http_build_query($queryData, '', '&');
                                                                $headerOptions = array(
                                                                    'http' => array(
                                                                        'method' => 'POST',
                                                                        'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
                                                                            . "Authorization: Bearer " . $token . "\r\n"
                                                                            . "Content-Length: " . strlen($queryData) . "\r\n",
                                                                        'content' => $queryData,
                                                                    ),
                                                                );
                                                                $context = stream_context_create($headerOptions);
                                                                $result = file_get_contents(LINE_API, false, $context);
                                                                $res = json_decode($result);
                                                            }

                                                            /*$mail = new PHPMailer();
                                                            $PerfixNameToSend = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataPersonal['PERID']);
                                                            $s = str_replace('><', '> <', $PerfixNameToSend);
                                                            $fresult = trim(strip_tags($s));
                                                            $mail->From = $fresult . "@medicine.psu.ac.th";
                                                            $mail->isHTML(true);
                                                            $mail->FromName = $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                            $PerfixName = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataLineNoti["perid"]);
                                                            $s = str_replace('><', '> <', $PerfixName);
                                                            $fresult2 = trim(strip_tags($s));
                                                            $length = 10;
                                                            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
                                                            $body = "";
                                                            $body .= "ระบบบันทึกเวลา เข้า - ออกงาน ->> มีข้อมูลการลา ของคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " เข้ามาใหม่ แจ้งลาเมื่อวันที่ " . $dateResult;
                                                            //ส่งเมลล์ไปยังเมลล์ของผู้ประเมิน
                                                            // $fmail = $fresult2 . "@medicine.psu.ac.th";
                                                            $fmail = "jthawatc@medicine.psu.ac.th";
                                                            $mail->AddAddress($fmail);
                                                            $mail->Subject = "แจ้งการลาคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                            $mail->Body = $body;
                                                            $mail->CharSet = "utf-8";
                                                            $mail->IsSMTP();
                                                            $mail->SMTPAuth = false;
                                                            $mail->Host = "medicine.psu.ac.th";
                                                            $mail->Port = 25;
                                                            $mail->Send();*/
                                                            // if (!$mail->Send()) {
                                                            //     echo 'Error: ' . $mail->ErrorInfo;
                                                            // } else {
                                                            //     echo "complete";
                                                            // }
                                                        }
                                                    }
                                                    // echo $dataLineNoti["line_token"];
                                                    // $myArray[] = $dataLineNoti;
                                                }
                                            }
                                        }
                                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                        echo json_encode($arr);
                                    } else {
                                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                        echo json_encode($arr);
                                    }
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้ เนื่องจากมีข้อมูลอยู่ในระบบแล้ว");
                                    echo json_encode($arr);
                                }
                            }
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก รหัสบุคลากร $PerId ที่เลือกได้ทำการยืนยันข้อมูลการปฏิบัติงานเรียบร้อยแล้ว");
                        echo json_encode($arr);
                    }

                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '005_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {

                            $queryCheckApproveDay = mysqli_query($this->db->hostDB2, "SELECT * FROM HR_CENTER.`working_summary_tb` where classify = '3' AND perid = '" . $PerId . "' AND monthly BETWEEN '".$yearS."-".$monthS."-01' AND '".$yearE."-".$monthE."-01'");

                            if (mysqli_num_rows($queryCheckApproveDay) == 0) {

                                    $query = mysqli_query($this->db->hostDB, "SELECT
            c_take_a_leave.No_Id,
            c_take_a_leave.T_Leave_Type,
            c_take_a_leave.T_Leave_Reason,
            c_take_a_leave.T_Leave_Date_Start,
            c_take_a_leave.T_Leave_Date_End,
            c_take_a_leave.T_Day_Type_Start,
            c_take_a_leave.T_Day_Type_End,
            c_take_a_leave.T_Leave_CreateBY,
            c_take_a_leave.T_Leave_UpdateBY,
            c_take_a_leave.T_Leave_CreateT,
            c_take_a_leave.T_Leave_UpdateT,
            c_take_a_leave.PERID,
            c_take_a_leave.Dep_code
            FROM
            c_take_a_leave
            WHERE
            c_take_a_leave.PERID = " . $PerId . " AND
            c_take_a_leave.T_Leave_Date_Start
            BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'
            UNION
            SELECT
            c_take_a_leave.No_Id,
            c_take_a_leave.T_Leave_Type,
            c_take_a_leave.T_Leave_Reason,
            c_take_a_leave.T_Leave_Date_Start,
            c_take_a_leave.T_Leave_Date_End,
            c_take_a_leave.T_Day_Type_Start,
            c_take_a_leave.T_Day_Type_End,
            c_take_a_leave.T_Leave_CreateBY,
            c_take_a_leave.T_Leave_UpdateBY,
            c_take_a_leave.T_Leave_CreateT,
            c_take_a_leave.T_Leave_UpdateT,
            c_take_a_leave.PERID,
            c_take_a_leave.Dep_code
            FROM
            c_take_a_leave
            WHERE
            c_take_a_leave.PERID = " . $PerId . " AND
            c_take_a_leave.T_Leave_Date_End
            BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'");
                                    if ($query) {
                                        if ($query->num_rows == 0) {
                                            $query = mysqli_query($this->db->hostDB, "INSERT INTO c_take_a_leave (
                        T_Leave_Type,
                        T_Leave_Reason,
                        T_Leave_Date_Start,
                        T_Leave_Date_End,
                        T_Day_Type_Start,
                        T_Day_Type_End,
                        T_All_Summary,
                        T_Work_Summary,
                        T_Leave_CreateBY,
                        T_Leave_UpdateBY,
                        T_Leave_CreateT,
                        T_Leave_UpdateT,
                        PERID,
                        Dep_code,
                        T_Status_Leave,
                        Country,
                        AgentPerson
                    )
                    VALUES
                        (
                            " . $Leave_TypeS . ",
                            '" . $Leave_Reason . "',
                            '" . $Leave_Date_Start . "',
                            '" . $Leave_Date_End . "',
                            '" . $DayTypeStart . "',
                            '" . $DayTypeEnd . "',
                            " . $DateSum . ",
                            " . $DateSumWork . ",
                            '" . $PerId_Create . "',
                            '" . $PerId_Create . "',
                            CURRENT_TIMESTAMP,
                            CURRENT_TIMESTAMP,
                            '" . $PerId . "',
                            " . $DepCode . ",
                            '0',
                             '" . $Country . "',
                              '" . $approver . "'
                        )");
                                            if ($query) {
                                                $queryLineNoti = mysqli_query($this->db->hostDB, "
                                            SELECT
                                                    A.id,
                                                    A.perid,
                                                    A.line_token,
                                                    A.process,
                                                    A.group_name,
                                                    C.`NAME`,
                                                    C.SURNAME,
                                                    D.Edit_code,
                                                    D.Dep_name,
                                                    D.Dep_Group_name,
                                                    D.Dep_Code
                                                    FROM
                                                    HR_CENTER.line_token AS A
                                                    INNER JOIN STAFF.Medperson AS C ON C.PERID = A.perid
                                                    INNER JOIN STAFF.Depart AS D ON D.Dep_Code = C.DEP_WORK
                                                    WHERE
                                                    C.CSTATUS != 0 AND
                                                    D.Dep_Code = $DepCode AND
                                                    A.process = 'HR-Time' AND
                                                    A.line_token IS NOT NULL
                            ");
                                                if ($queryLineNoti) {
                                                    if ($queryLineNoti->num_rows > 0) {
                                                        while ($dataLineNoti = mysqli_fetch_assoc($queryLineNoti)) {
                                                            $queryPersonal = mysqli_query($this->db->hostDB, "SELECT
                                    STAFF.Medperson.PERID,
                                    STAFF.Medperson.`NAME`,
                                    STAFF.Medperson.SURNAME
                                    FROM
                                    STAFF.Medperson
                                    WHERE
                                    STAFF.Medperson.PERID = $PerId
                                    ");
                                                            if ($queryPersonal) {
                                                                while ($dataPersonal = mysqli_fetch_assoc($queryPersonal)) {
                                                                    if ($dataLineNoti["line_token"] != '' || $dataLineNoti["line_token"] != null) {
                                                                        define('LINE_API', "https://notify-api.line.me/api/notify");

                                                                        $token = $dataLineNoti["line_token"]; //ใส่Token ที่copy เอาไว้
                                                                        switch ($Leave_TypeS){
                                                                            case '0': $Type = 'ลากิจส่วนตัว';
                                                                                break;
                                                                            case  '1': $Type = 'ลาป่วย';
                                                                                break;
                                                                            case  '2': $Type = 'ลาพักผ่อน';
                                                                                break;
                                                                            case  '3': $Type = 'ลาศึกษาต่อ';
                                                                                break;
                                                                            case  '4': $Type = 'ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย';
                                                                                break;
                                                                            case  '5': $Type = 'ลาคลอดบุตร';
                                                                                break;
                                                                            case  '6': $Type = 'ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์';
                                                                                break;
                                                                            case  '7': $Type = 'ไปราชการ';
                                                                                break;
                                                                            default:
                                                                                break;
                                                                        }

                                                                        $message = "แจ้งการลา คุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " ( " . $Type . " ) ตั้งแต่วันที่ " . $this->DateToThai($Leave_Date_Start) . " ( " . $DayTypeStart . " ) " . $this->DateToThai($Leave_Date_End) . " ( " . $DayTypeEnd . " ) เหตุผล ".$Leave_Reason; //ข้อความที่ต้องการส่ง สูงสุด 1000 ตัวอักษร

                                                                        $queryData = array('message' => $message);
                                                                        $queryData = http_build_query($queryData, '', '&');
                                                                        $headerOptions = array(
                                                                            'http' => array(
                                                                                'method' => 'POST',
                                                                                'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
                                                                                    . "Authorization: Bearer " . $token . "\r\n"
                                                                                    . "Content-Length: " . strlen($queryData) . "\r\n",
                                                                                'content' => $queryData,
                                                                            ),
                                                                        );
                                                                        $context = stream_context_create($headerOptions);
                                                                        $result = file_get_contents(LINE_API, false, $context);
                                                                        $res = json_decode($result);
                                                                    }

                                                                    /*$mail = new PHPMailer();
                                                                    $PerfixNameToSend = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataPersonal['PERID']);
                                                                    $s = str_replace('><', '> <', $PerfixNameToSend);
                                                                    $fresult = trim(strip_tags($s));
                                                                    $mail->From = $fresult . "@medicine.psu.ac.th";
                                                                    $mail->isHTML(true);
                                                                    $mail->FromName = $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                                    $PerfixName = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataLineNoti["perid"]);
                                                                    $s = str_replace('><', '> <', $PerfixName);
                                                                    $fresult2 = trim(strip_tags($s));
                                                                    $length = 10;
                                                                    $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
                                                                    $body = "";
                                                                    $body .= "ระบบบันทึกเวลา เข้า - ออกงาน ->> มีข้อมูลการลา ของคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " เข้ามาใหม่ แจ้งลาเมื่อวันที่ " . $dateResult;
                                                                    //ส่งเมลล์ไปยังเมลล์ของผู้ประเมิน
                                                                    // $fmail = $fresult2 . "@medicine.psu.ac.th";
                                                                    $fmail = "jthawatc@medicine.psu.ac.th";
                                                                    $mail->AddAddress($fmail);
                                                                    $mail->Subject = "แจ้งการลาคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                                    $mail->Body = $body;
                                                                    $mail->CharSet = "utf-8";
                                                                    $mail->IsSMTP();
                                                                    $mail->SMTPAuth = false;
                                                                    $mail->Host = "medicine.psu.ac.th";
                                                                    $mail->Port = 25;
                                                                    $mail->Send();*/
                                                                    // if (!$mail->Send()) {
                                                                    //     echo 'Error: ' . $mail->ErrorInfo;
                                                                    // } else {
                                                                    //     echo "complete";
                                                                    // }
                                                                }
                                                            }
                                                            // echo $dataLineNoti["line_token"];
                                                            // $myArray[] = $dataLineNoti;
                                                        }
                                                    }
                                                }
                                                $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                                echo json_encode($arr);
                                            } else {
                                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                                echo json_encode($arr);
                                            }
                                        } else {
                                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้ เนื่องจากมีข้อมูลอยู่ในระบบแล้ว");
                                            echo json_encode($arr);
                                        }
                                    }


                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก รหัสบุคลากร $PerId ที่เลือกได้ทำการยืนยันข้อมูลการปฏิบัติงานเรียบร้อยแล้ว");
                                echo json_encode($arr);
                            }

                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                        }
                    } else {
                        echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                    }
                }
            }
        } else {
            return false;
        }
    }

    // เรียกข้อมูลประเภทวันลา
    public function SELECTLEAVEPERSON($PerId)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave_day.L_Day_Type,
            HRTIME_DB.c_leave_day.L_Day_Detail
            FROM
            HRTIME_DB.c_leave_day
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลประวัติการลาทั้งหมดของบุคลากรรายบุคคล
    public function GETLEAVEPERSONDATADEP($DepCode, $PerId)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            // $query = mysqli_query($this->db->hostDB, "SELECT
            // HRTIME_DB.c_take_a_leave.*,
            // HRTIME_DB.c_leave.Leave_Detail,
            // STAFF.Depart.Dep_name,
            // STAFF.Medperson.`NAME`,
            // STAFF.Medperson.SURNAME,
            // STAFF.Medperson.POS_LEVEL
            // FROM
            // HRTIME_DB.c_take_a_leave
            // LEFT JOIN STAFF.Medperson ON HRTIME_DB.c_take_a_leave.PERID = STAFF.Medperson.PERID
            // LEFT JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
            // INNER JOIN HRTIME_DB.c_leave ON HRTIME_DB.c_leave.Leave_Type = HRTIME_DB.c_take_a_leave.T_Leave_Type
            // WHERE
            // c_take_a_leave.Dep_code = " . $DepCode . " AND
            // c_take_a_leave.PERID = '" . $PerId . "' AND
            // c_take_a_leave.T_Status_Leave != '0'
            // ORDER BY
            // No_Id
            // DESC
            // ");
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_take_a_leave.*,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.POS_LEVEL,
            STAFF.Medperson.Iname,
            STAFF.Medperson.TITLE,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            HRTIME_DB.c_leave.Leave_Detail,
            STAFF.Positions.PosName
        FROM
            HRTIME_DB.c_take_a_leave
        INNER JOIN STAFF.Medperson ON STAFF.Medperson.PERID = HRTIME_DB.c_take_a_leave.PERID
        INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
        INNER JOIN HRTIME_DB.c_leave ON HRTIME_DB.c_leave.Leave_Type = HRTIME_DB.c_take_a_leave.T_Leave_Type
        INNER JOIN STAFF.Positions ON STAFF.Positions.PosCode = STAFF.Medperson.NewPos
        WHERE
            c_take_a_leave.PERID = '" . $PerId . "'
        ORDER BY
            c_take_a_leave.No_Id DESC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function UPDATELEAVEPERSONDATA($No_Id, $LeaveTSelect, $DateStart, $LeaveS_Select, $DateEnd, $LeaveE_Select, $Details, $PerId, $Status, $DepCode, $DayOff, $DayAllLeave)
    {
        $myArray = array();
        $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " .$_SESSION['PERID']
        );
        if($queryPermiss) {
            if (mysqli_num_rows($queryPermiss) >= 1) {
                if ($DayOff == '1') {
                    if ($this->db->hostDB) {
                        $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
            SET T_Leave_Type = '" . $LeaveTSelect . "',
            T_Leave_Date_Start = '" . $DateStart . "',
            T_Leave_Date_End = '" . $DateEnd . "',
            T_Day_Type_Start = '" . $LeaveS_Select . "',
            T_Day_Type_End = '" . $LeaveE_Select . "',
            T_Leave_Reason = '" . $Details . "',
            T_All_Summary = '" . $DayAllLeave . "',
            T_Work_Summary = 0,
            T_Leave_UpdateBY = '" . $PerId . "',
            T_Leave_UpdateT = CURRENT_TIMESTAMP,
            T_Status_Leave = $Status
            WHERE No_Id = " . $No_Id);
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                        }
                        echo json_encode($arr);
                    } else {
                        return false;
                    }
                } else {
                    if ($this->db->hostDB) {
                        $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
            SET T_Leave_Type = '" . $LeaveTSelect . "',
            T_Leave_Date_Start = '" . $DateStart . "',
            T_Leave_Date_End = '" . $DateEnd . "',
            T_Day_Type_Start = '" . $LeaveS_Select . "',
            T_Day_Type_End = '" . $LeaveE_Select . "',
            T_Leave_Reason = '" . $Details . "',
            T_All_Summary = null,
            T_Work_Summary = null,
            T_Leave_UpdateBY = '" . $PerId . "',
            T_Leave_UpdateT = CURRENT_TIMESTAMP,
            T_Status_Leave = $Status
            WHERE No_Id = " . $No_Id);
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                        }
                        echo json_encode($arr);
                    } else {
                        return false;
                    }
                }
            } else {
                $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '005_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                );
                if ($queryPermiss) {
                    if (mysqli_num_rows($queryPermiss) >= 1) {
                        if ($DayOff == '1') {
                            if ($this->db->hostDB) {
                                $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
            SET T_Leave_Type = '" . $LeaveTSelect . "',
            T_Leave_Date_Start = '" . $DateStart . "',
            T_Leave_Date_End = '" . $DateEnd . "',
            T_Day_Type_Start = '" . $LeaveS_Select . "',
            T_Day_Type_End = '" . $LeaveE_Select . "',
            T_Leave_Reason = '" . $Details . "',
            T_All_Summary = '" . $DayAllLeave . "',
            T_Work_Summary = 0,
            T_Leave_UpdateBY = '" . $PerId . "',
            T_Leave_UpdateT = CURRENT_TIMESTAMP,
            T_Status_Leave = $Status
            WHERE No_Id = " . $No_Id);
                                if ($query) {
                                    $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                                }
                                echo json_encode($arr);
                            } else {
                                return false;
                            }
                        } else {
                            if ($this->db->hostDB) {
                                $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
            SET T_Leave_Type = '" . $LeaveTSelect . "',
            T_Leave_Date_Start = '" . $DateStart . "',
            T_Leave_Date_End = '" . $DateEnd . "',
            T_Day_Type_Start = '" . $LeaveS_Select . "',
            T_Day_Type_End = '" . $LeaveE_Select . "',
            T_Leave_Reason = '" . $Details . "',
            T_All_Summary = null,
            T_Work_Summary = null,
            T_Leave_UpdateBY = '" . $PerId . "',
            T_Leave_UpdateT = CURRENT_TIMESTAMP,
            T_Status_Leave = $Status
            WHERE No_Id = " . $No_Id);
                                if ($query) {
                                    $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                                }
                                echo json_encode($arr);
                            } else {
                                return false;
                            }
                        }

                    } else {
                        echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                    }
                } else {
                    echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                }
            }
        }
    }

    public function DELETEDATA($No_Id,$DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " .$_SESSION['PERID']
            );
            if($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    $querySelectData = mysqli_query($this->db->hostDB, "SELECT * FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
                    if (mysqli_num_rows($querySelectData) >= 1) {
                        $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                        }
                        echo json_encode($arr);
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้ เนื่องจากไม่พบข้อมูลในระบบ");
                        echo json_encode($arr);
                    }
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '006_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            $querySelectData = mysqli_query($this->db->hostDB, "SELECT * FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
                            if (mysqli_num_rows($querySelectData) >= 1) {
                                $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
                                if ($query) {
                                    $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                                }
                                echo json_encode($arr);
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้ เนื่องจากไม่พบข้อมูลในระบบ");
                                echo json_encode($arr);
                            }
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                        }
                    } else {
                        echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                    }
                }
            }
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                D_holiday
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function DateToThai($date){
        $dateToThai = explode('-', $date);
        $date = $dateToThai[2];
        $month = '';
        $year = ((int)$dateToThai[0]) + 543;

        switch ($dateToThai[1]) {
            case "01":
                $month = 'มกราคม';
                break;
            case "02":
                $month = 'กุมภาพันธ์';
                break;
            case "03":
                $month = 'มีนาคม';
                break;
            case "04":
                $month = 'เมษายน';
                break;
            case "05":
                $month = 'พฤษภาคม';
                break;
            case "06":
                $month = 'มิถุนายน';
                break;
            case "07":
                $month = 'กรกฎาคม';
                break;
            case "08":
                $month = 'สิงหาคม';
                break;
            case "09":
                $month = 'กันยายน';
                break;
            case "10":
                $month = 'ตุลาคม';
                break;
            case "11":
                $month = 'พฤศจิกายน';
                break;
            case "12":
                $month = 'ธันวาคม';
                break;
            default:
                break;
        }

        return $date . ' ' . $month . ' ' . $year;
    }

}
