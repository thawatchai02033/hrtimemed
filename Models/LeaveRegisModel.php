<?php
class LeaveRegisModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function DateAllData()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, constant('Date_All_Data'));
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }
    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GatAllDepartment()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรของหน่วยงานแต่ล่ะหน่วยงาน
    public function GETPERSONDATENOCORD($DepCode)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                    S.NAME,
                    S.SURNAME,
                    S.PERID,
                    S.DEP_WORK,
                    D.Dep_name,
                    P_Time_DateTime,
                    P_Time_TimeN,
                    P_Time_TimeO,
                    P_Time_TimeHN,
                    P_Time_TimeHO,
                    P_Time_UpdateBY,
                    P_Time_UpdateT
                FROM
                    STAFF.Medperson AS S
                    LEFT JOIN ( SELECT PERID, P_Time_DateTime, P_Time_TimeN, P_Time_TimeO, P_Time_TimeHN, P_Time_TimeHO, P_Time_UpdateBY, P_Time_UpdateT FROM HRTIME_DB.c_person_time WHERE P_Time_DateTime = '" . $dateNow . "' ) AS H ON S.PERID = H.PERID
                    LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
                WHERE
                    S.DEP_WORK = " . $DepCode . "
                    AND S.CSTATUS != 0
                ORDER BY
                    S.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรภายในหน่วยงาน
    public function REGISSAVEDATA($PERID, $DepCode, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, $_TimeLeave1, $_DayLeave1, $_DayWorkLeave1, $_DayRestLeave2, $_Year, $PERID_Update)
    {
        $arr = array();
        if ($this->db->hostDB) {

            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " .$_SESSION['PERID']
            );
            if($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                ");
                    if ($query->num_rows == 0) {
                        $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, Time_Leave, Day_Leave, DayWork_Leave, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                VALUES ($PERID, $DepCode, 0, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, 0, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP ),
                ($PERID, $DepCode, 1, $_TimeLeave1, $_DayLeave1, $_DayWorkLeave1, 0, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP );
                ");
                        $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                VALUES ($PERID, $DepCode, 2, $_DayRestLeave2, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP );
                ");
                        if ($queryInsert) {
                            $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบความครบถ้วนของข้อมูล");
                        }
                        $myJSON = json_encode($arr);
                        echo $myJSON;
                    } else {
                        $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                AND 
                HRTIME_DB.c_leave_set.Leave_Type = 0
                ");
                        if ($query->num_rows == 0) {
                            $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, Time_Leave, Day_Leave, DayWork_Leave, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                    VALUES ($PERID, $DepCode, 0, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, 0, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP )");
                        }else{
                            $queryUpdate = mysqli_query($this->db->hostDB, "UPDATE HRTIME_DB.c_leave_set
                    SET Leave_Type = 0,Time_Leave = $_TimeLeave0, Day_Leave = $_DayLeave0,
                    DayWork_Leave = $_DayWorkLeave0,
                    Update_By = $PERID_Update, 
                    Update_Time = CURRENT_TIMESTAMP,
                    Year = $_Year
                    WHERE HRTIME_DB.c_leave_set.PERID = $PERID
                    AND HRTIME_DB.c_leave_set.Leave_Type = 0");
                        }
                        $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                AND 
                HRTIME_DB.c_leave_set.Leave_Type = 1
                ");
                        if ($query->num_rows == 0) {
                            $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, Time_Leave, Day_Leave, DayWork_Leave, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                    VALUES ($PERID, $DepCode, 1, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, 0, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP )");
                        }else{
                            $queryUpdate = mysqli_query($this->db->hostDB, "UPDATE HRTIME_DB.c_leave_set
                    SET Leave_Type = 2,DayRest_Leave = $_DayRestLeave2,
                    Update_By = $PERID_Update, 
                    Update_Time = CURRENT_TIMESTAMP,
                     Year = $_Year
                    WHERE HRTIME_DB.c_leave_set.PERID = $PERID
                    AND HRTIME_DB.c_leave_set.Leave_Type = 2
                    ");
                        }
                        $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                AND 
                HRTIME_DB.c_leave_set.Leave_Type = 2
                ");
                        if ($query->num_rows == 0) {
                            $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                    VALUES ($PERID, $DepCode, 2, $_DayRestLeave2, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP );
                ");
                        }else{
                            $queryUpdate = mysqli_query($this->db->hostDB, "UPDATE HRTIME_DB.c_leave_set
                    SET Leave_Type = 1,Time_Leave = $_TimeLeave1, Day_Leave = $_DayLeave1,
                    DayWork_Leave = $_DayWorkLeave1,
                    Update_By = $PERID_Update, 
                    Update_Time = CURRENT_TIMESTAMP,
                    Year = $_Year
                    WHERE HRTIME_DB.c_leave_set.PERID = $PERID
                    AND HRTIME_DB.c_leave_set.Leave_Type = 1
                    ");
                        }
                        if ($queryUpdate) {
                            $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ โปรดตรวจสอบความครบถ้วนของข้อมูล");
                        }

                        $myJSON = json_encode($arr);
                        echo $myJSON;
                    }
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '008_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                ");
                            if ($query->num_rows == 0) {
                                $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, Time_Leave, Day_Leave, DayWork_Leave, Year, DayRest_Leave, Create_By, Create_Time, Update_By, Update_Time)
                VALUES ($PERID, $DepCode, 0, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, 0, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP ),
                ($PERID, $DepCode, 1, $_TimeLeave1, $_DayLeave1, $_DayWorkLeave1, 0, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP );
                ");
                                $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                VALUES ($PERID, $DepCode, 2, $_DayRestLeave2, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP );
                ");
                                if ($queryInsert) {
                                    $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ โปรดตรวจสอบความครบถ้วนของข้อมูล");
                                }
                                $myJSON = json_encode($arr);
                                echo $myJSON;
                            } else {
                                $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                AND 
                HRTIME_DB.c_leave_set.Leave_Type = 0
                ");
                                if ($query->num_rows == 0) {
                                    $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, Time_Leave, Day_Leave, DayWork_Leave, Year, DayRest_Leave, Create_By, Create_Time, Update_By, Update_Time)
                    VALUES ($PERID, $DepCode, 0, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, 0, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP )");
                                }else{
                                    $queryUpdate = mysqli_query($this->db->hostDB, "UPDATE HRTIME_DB.c_leave_set
                    SET Leave_Type = 0,Time_Leave = $_TimeLeave0, Day_Leave = $_DayLeave0,
                    DayWork_Leave = $_DayWorkLeave0,
                    Update_By = $PERID_Update, 
                    Update_Time = CURRENT_TIMESTAMP,
                    Year = $_Year
                    WHERE HRTIME_DB.c_leave_set.PERID = $PERID
                    AND HRTIME_DB.c_leave_set.Leave_Type = 0");
                                }
                                $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                AND 
                HRTIME_DB.c_leave_set.Leave_Type = 1
                ");
                                if ($query->num_rows == 0) {
                                    $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, Time_Leave, Day_Leave, DayWork_Leave, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                    VALUES ($PERID, $DepCode, 1, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, 0, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP )");
                                }else{
                                    $queryUpdate = mysqli_query($this->db->hostDB, "UPDATE HRTIME_DB.c_leave_set
                    SET Leave_Type = 2,DayRest_Leave = $_DayRestLeave2,
                    Update_By = $PERID_Update, 
                    Update_Time = CURRENT_TIMESTAMP,
                    Year = $_Year
                    WHERE HRTIME_DB.c_leave_set.PERID = $PERID
                    AND HRTIME_DB.c_leave_set.Leave_Type = 2
                    ");
                                }
                                $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PERID
                AND 
                HRTIME_DB.c_leave_set.Leave_Type = 2
                ");
                                if ($query->num_rows == 0) {
                                    $queryInsert = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.c_leave_set(PERID, Dep_Code, Leave_Type, DayRest_Leave, Year, Create_By, Create_Time, Update_By, Update_Time)
                    VALUES ($PERID, $DepCode, 2, $_DayRestLeave2, $_Year, $PERID_Update, CURRENT_TIMESTAMP, $PERID_Update, CURRENT_TIMESTAMP );
                ");
                                }else{
                                    $queryUpdate = mysqli_query($this->db->hostDB, "UPDATE HRTIME_DB.c_leave_set
                    SET Leave_Type = 1,Time_Leave = $_TimeLeave1, Day_Leave = $_DayLeave1,
                    DayWork_Leave = $_DayWorkLeave1,
                    Update_By = $PERID_Update, 
                    Update_Time = CURRENT_TIMESTAMP,
                    Year = $_Year
                    WHERE HRTIME_DB.c_leave_set.PERID = $PERID
                    AND HRTIME_DB.c_leave_set.Leave_Type = 1
                    ");
                                }
                                if ($queryUpdate) {
                                    $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ โปรดตรวจสอบความครบถ้วนของข้อมูล");
                                }

                                $myJSON = json_encode($arr);
                                echo $myJSON;
                            }
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                        }
                    } else {
                        echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                    }
                }
            }
        } else {
            return false;
        }
    }

    //เรียกข้อมูลการตั้งค่าการลาเริ่มต้น
    public function GERDATAREGIS($DepCode)
    {
        $myArray = array();

        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            C.Dep_Code
            FROM
            HRTIME_DB.c_leave_set AS A
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            WHERE
            C.Dep_Code = " . $DepCode[$i]->Dep_Code);
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }

                    $queryOtherPerson = mysqli_query($this->db->hostDB, "SELECT
                            A.*
                            FROM
                            HRTIME_DB.c_medperson AS A
                            WHERE
                            A.Dep_Code = " . $DepCode[$i]->Dep_Code);
                    if(mysqli_num_rows($queryOtherPerson) > 0){
                        while ($OtherPerson = mysqli_fetch_assoc($queryOtherPerson)) {

                            $queryPerson = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            C.Dep_Code
            FROM
            HRTIME_DB.c_leave_set AS A
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            WHERE
            A.PERID = ". $OtherPerson['PERID'] ."
            AND
            C.Dep_Code = " . $OtherPerson['Dep_Code_Old']);
                            if (mysqli_num_rows($queryPerson) > 0) {
                                while ($dataPerson = mysqli_fetch_assoc($queryPerson)) {

                                    array_push($myArray, $dataPerson);

                                }
                            }

                        }
                    }
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }
}
