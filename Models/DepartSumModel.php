<?php
class DepartSumModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GatAllDepartment()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                C_admin.PERID
            FROM
                C_admin
            WHERE
                C_admin.PERID = $PERID");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                    B.Dep_Code,
                    B.Edit_code,
                    B.Dep_name,
                    B.Dep_Group_name,
                    B.Telephone,
                    B.Doc_In,
                    B.Doc_Out,
                    B.Doc_go,
                    B.Doc_in2,
                    B.Dep_status
                    FROM
                    STAFF.D_admin AS A
                    INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                    WHERE
                    A.Perid = $PERID
                    ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                        A.Dep_Code,
                        A.Dep_name,
                        A.Dep_Group_name
                        FROM
                        STAFF.Depart AS A
                        INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                        WHERE
                        B.PERID = $PERID
                        ORDER BY
                        A.Dep_name ASC
                        ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, constant('Date_Get_Department'));
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //  เรียกบุคลากรทุกคนภายในหน่วยงาน
    public function GETPERSONALDEPART()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.PERID,
            A.`NAME`,
            A.SURNAME,
            B.Dep_Code,
            B.Edit_code,
            B.Dep_name
            FROM
            STAFF.Depart AS B
            INNER JOIN STAFF.Medperson AS A ON B.Dep_Code = A.DEP_WORK
            WHERE
            A.CSTATUS != 0
            ");

            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {

            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการลาของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTLEAVESUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.T_Leave_Type,
            A.T_Leave_Reason,
            A.T_Leave_Date_Start,
            A.T_Leave_Date_End,
            A.T_Day_Type_Start,
            A.T_Day_Type_End,
            A.T_All_Summary,
            A.T_Work_Summary,
            A.T_Leave_CreateBY,
            A.T_Leave_UpdateBY,
            A.T_Leave_CreateT,
            A.T_Leave_UpdateT,
            A.PERID,
            A.Dep_code,
            A.T_Status_Leave,
            B.Edit_code
            FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            WHERE
            B.Edit_code = $Dep_Code AND
            A.T_Leave_Date_End >= '$Date_Cordi' AND
            A.T_Leave_Date_End <= '$Date_Cordi2' AND
            A.T_Status_Leave = '1'
        UNION
        SELECT
            A.No_Id,
            A.T_Leave_Type,
            A.T_Leave_Reason,
            A.T_Leave_Date_Start,
            A.T_Leave_Date_End,
            A.T_Day_Type_Start,
            A.T_Day_Type_End,
            A.T_All_Summary,
            A.T_Work_Summary,
            A.T_Leave_CreateBY,
            A.T_Leave_UpdateBY,
            A.T_Leave_CreateT,
            A.T_Leave_UpdateT,
            A.PERID,
            A.Dep_code,
            A.T_Status_Leave,
            B.Edit_code
            FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            WHERE
            B.Edit_code = $Dep_Code AND
            A.T_Leave_Date_Start >= '$Date_Cordi' AND
            A.T_Leave_Date_Start <= '$Date_Cordi2' AND
            A.T_Status_Leave = '1'
            ");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                A.No_Id,
                A.T_Leave_Type,
                A.T_Leave_Reason,
                A.T_Leave_Date_Start,
                A.T_Leave_Date_End,
                A.T_Day_Type_Start,
                A.T_Day_Type_End,
                A.T_All_Summary,
                A.T_Work_Summary,
                A.T_Leave_CreateBY,
                A.T_Leave_UpdateBY,
                A.T_Leave_CreateT,
                A.T_Leave_UpdateT,
                A.PERID,
                A.Dep_code,
                A.T_Status_Leave,
                B.Edit_code
                FROM
                HRTIME_DB.c_take_a_leave AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                B.Edit_code = $Dep_Code AND
                A.T_Leave_Date_End  = '$Date_Cordi' AND
                A.T_Status_Leave = '1'");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
                $myJSON = json_encode($myArray);
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
                $myJSON = json_encode($myArray);
            }
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการปฏิบัติงานของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTWORKSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.PERID,
            A.Dep_Code,
            A.A_Datetime,
            A.A_Time_in,
            A.A_Status_in,
            A.A_Door_in,
            A.A_Time_in2,
            A.A_Status_in2,
            A.A_Door_in2,
            A.A_Time_ho,
            A.A_Status_ho,
            A.A_Door_ho,
            A.A_Time_hn,
            A.A_Status_hn,
            A.A_Door_hn,
            A.A_Time_out,
            A.A_Status_out,
            A.A_Door_out,
            A.A_Time_out2,
            A.A_Status_out2,
            A.A_Door_out2,
            A.Create_By,
            A.Create_Time,
            A.Update_By,
            A.Update_Time,
            B.Edit_code
            FROM
            HRTIME_DB.A_transaction AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_Code
            WHERE
            B.Edit_code = $Dep_Code AND
            A.A_Datetime BETWEEN '$Date_Cordi' AND '$Date_Cordi2'");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                    *
                    FROM
                    D_holiday
                    ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }
}
