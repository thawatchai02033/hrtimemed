<?php
require_once './libs/phpmailer/maler/class.phpmailer.php';

class TakeALeaveUserModel extends Model
{

    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function GETALLDEPARTMENT()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            C_admin.PERID
        FROM
            C_admin
        WHERE
            C_admin.PERID = $PERID");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Edit_code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                STAFF.D_admin AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Edit_code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, constant('Date_Get_Department'));
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรภายในหน่วยงาน
    public function GETPERSONFROMDEPART($DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson. NAME,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.PERID
        FROM
            STAFF.Medperson
        WHERE
            STAFF.Medperson.DEP_WORK = " . $DepCode . "
        AND
            STAFF.Medperson.CSTATUS != 0
        ORDER BY
            STAFF.Medperson.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLCOUNTRY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.*
        FROM
            HRTIME_DB.tbl_country AS A");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลชนิดของการลา
    public function GETLEAVETYPE()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave.Leave_Type,
            HRTIME_DB.c_leave.Leave_Detail
            FROM
            HRTIME_DB.c_leave
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลประเภทวันลา
    public function GETDAYTYPE()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave_day.L_Day_Type,
            HRTIME_DB.c_leave_day.L_Day_Detail
            FROM
            HRTIME_DB.c_leave_day
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เพิ่มข้อมูลการลาโดยเช็คข้อมูลของวันที่เริ่มต้นและวันที่สิ้นสุดถ้ามีข้อมูลอยู่แล้วให้ส่งสถานะเป็น false กลับไป
    public function INSERTDATALEAVEUSERPERSON($DayTypeEnd, $DayTypeStart, $DepCode, $EditCode, $Leave_Date_End, $Leave_Date_Start, $Leave_Reason, $Leave_TypeS, $PerId, $PerId_Create, $DateSum, $DateSumWork, $Country, $approver)
    {

        $dateResult = date("d-m-Y");

        list($yearS, $monthS, $dayS) = explode('-', $Leave_Date_Start);

        if ($monthS < 10) {
            $monthS = '0' . $monthS;
        }

        list($yearE, $monthE, $dayE) = explode('-', $Leave_Date_End);

        if ($monthE < 10) {
            $monthE = '0' . $monthE;
        }

        $myArray = array();
        if ($this->db->hostDB) {

            $queryPersonOther = mysqli_query($this->db->hostDB, "SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.POS_LEVEL,
            A.Date_In,
            B.Dep_name,
            B.Dep_Code,
            C.PosName
            FROM
            HRTIME_DB.c_medperson AS D
            LEFT JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = D.Dep_Code
            LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
            WHERE
            D.PERID = " . $PerId . " AND
            A.CSTATUS != 0
            ORDER BY
            A.PERID ASC
            ");
            if (mysqli_num_rows($queryPersonOther) > 0) {
                while ($dataOtherDep = mysqli_fetch_assoc($queryPersonOther)) {
                    $queryCheckApproveDay = mysqli_query($this->db->hostDB2, "SELECT * FROM HR_CENTER.`working_summary_tb` where classify = '3' AND perid = '" . $PerId . "' AND monthly BETWEEN '" . $yearS . "-" . $monthS . "-01' AND '" . $yearE . "-" . $monthE . "-01'");

                    if (mysqli_num_rows($queryCheckApproveDay) == 0) {
                        $query = mysqli_query($this->db->hostDB, "SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.PERID = " . $PerId . " AND
                c_take_a_leave.T_Leave_Date_Start
                BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'
                UNION
                SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.PERID = " . $PerId . " AND
                c_take_a_leave.T_Leave_Date_End
                BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'");
                        if ($query) {
                            if ($query->num_rows == 0) {
                                $query = mysqli_query($this->db->hostDB, "INSERT INTO c_take_a_leave (
                            T_Leave_Type,
                            T_Leave_Reason,
                            T_Leave_Date_Start,
                            T_Leave_Date_End,
                            T_Day_Type_Start,
                            T_Day_Type_End,
                            T_All_Summary,
                            T_Work_Summary,
                            T_Leave_CreateBY,
                            T_Leave_UpdateBY,
                            T_Leave_CreateT,
                            T_Leave_UpdateT,
                            PERID,
                            Dep_code,
                            T_Status_Leave,
                            Country,
                            AgentPerson
                        )
                        VALUES
                            (
                                " . $Leave_TypeS . ",
                                '" . $Leave_Reason . "',
                                '" . $Leave_Date_Start . "',
                                '" . $Leave_Date_End . "',
                                '" . $DayTypeStart . "',
                                '" . $DayTypeEnd . "',
                                " . $DateSum . ",
                                " . $DateSumWork . ",
                                '" . $PerId_Create . "',
                                '" . $PerId_Create . "',
                                CURRENT_TIMESTAMP,
                                CURRENT_TIMESTAMP,
                                '" . $PerId . "',
                                '" . $dataOtherDep['Dep_Code'] . "',
                                '0',
                                 '" . $Country . "',
                                  '" . $approver . "'
                            )");
                                if ($query) {
                                    $queryLineNoti = mysqli_query($this->db->hostDB, "
                                            SELECT
                                                    A.id,
                                                    A.perid,
                                                    A.line_token,
                                                    A.process,
                                                    A.group_name,
                                                    C.`NAME`,
                                                    C.SURNAME,
                                                    D.Edit_code,
                                                    D.Dep_name,
                                                    D.Dep_Group_name,
                                                    D.Dep_Code
                                                    FROM
                                                    HR_CENTER.line_token AS A
                                                    INNER JOIN STAFF.Medperson AS C ON C.PERID = A.perid
                                                    INNER JOIN STAFF.Depart AS D ON D.Dep_Code = C.DEP_WORK
                                                    WHERE
                                                    C.CSTATUS != 0 AND
                                                    D.Edit_code = '" . $dataOtherDep['Edit_code'] . "' AND
                                                    A.process = 'HR-Time' AND
                                                    A.line_token IS NOT NULL
                            ");
                                    if ($queryLineNoti) {
                                        if ($queryLineNoti->num_rows > 0) {
                                            while ($dataLineNoti = mysqli_fetch_assoc($queryLineNoti)) {
                                                $queryPersonal = mysqli_query($this->db->hostDB, "SELECT
                                    STAFF.Medperson.PERID,
                                    STAFF.Medperson.`NAME`,
                                    STAFF.Medperson.SURNAME
                                    FROM
                                    STAFF.Medperson
                                    WHERE
                                    STAFF.Medperson.PERID = $PerId
                                    ");
                                                if ($queryPersonal) {
                                                    while ($dataPersonal = mysqli_fetch_assoc($queryPersonal)) {
                                                        if ($dataLineNoti["line_token"] != '' || $dataLineNoti["line_token"] != null) {
                                                            define('LINE_API', "https://notify-api.line.me/api/notify");

                                                            $token = $dataLineNoti["line_token"]; //ใส่Token ที่copy เอาไว้
                                                            switch ($Leave_TypeS) {
                                                                case '0':
                                                                    $Type = 'ลากิจส่วนตัว';
                                                                    break;
                                                                case  '1':
                                                                    $Type = 'ลาป่วย';
                                                                    break;
                                                                case  '2':
                                                                    $Type = 'ลาพักผ่อน';
                                                                    break;
                                                                case  '3':
                                                                    $Type = 'ลาศึกษาต่อ';
                                                                    break;
                                                                case  '4':
                                                                    $Type = 'ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย';
                                                                    break;
                                                                case  '5':
                                                                    $Type = 'ลาคลอดบุตร';
                                                                    break;
                                                                case  '6':
                                                                    $Type = 'ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์';
                                                                    break;
                                                                case  '7':
                                                                    $Type = 'ไปราชการ';
                                                                    break;
                                                                default:
                                                                    break;
                                                            }

                                                            $message = "แจ้งการลา คุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " ( " . $Type . " ) ตั้งแต่วันที่ " . $this->DateToThai($Leave_Date_Start) . " ( " . $DayTypeStart . " ) " . $this->DateToThai($Leave_Date_End) . " ( " . $DayTypeEnd . " ) เหตุผล " . $Leave_Reason; //ข้อความที่ต้องการส่ง สูงสุด 1000 ตัวอักษร

                                                            $queryData = array('message' => $message);
                                                            $queryData = http_build_query($queryData, '', '&');
                                                            $headerOptions = array(
                                                                'http' => array(
                                                                    'method' => 'POST',
                                                                    'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
                                                                        . "Authorization: Bearer " . $token . "\r\n"
                                                                        . "Content-Length: " . strlen($queryData) . "\r\n",
                                                                    'content' => $queryData,
                                                                ),
                                                            );
                                                            $context = stream_context_create($headerOptions);
                                                            $result = file_get_contents(LINE_API, false, $context);
                                                            $res = json_decode($result);
                                                        }

                                                        /*$mail = new PHPMailer();
                                                        $PerfixNameToSend = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataPersonal['PERID']);
                                                        $s = str_replace('><', '> <', $PerfixNameToSend);
                                                        $fresult = trim(strip_tags($s));
                                                        $mail->From = $fresult . "@medicine.psu.ac.th";
                                                        $mail->isHTML(true);
                                                        $mail->FromName = $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                        $PerfixName = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataLineNoti["perid"]);
                                                        $s = str_replace('><', '> <', $PerfixName);
                                                        $fresult2 = trim(strip_tags($s));
                                                        $length = 10;
                                                        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
                                                        $body = "";
                                                        $body .= "ระบบบันทึกเวลา เข้า - ออกงาน ->> มีข้อมูลการลา ของคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " เข้ามาใหม่ แจ้งลาเมื่อวันที่ " . $dateResult;
                                                        //ส่งเมลล์ไปยังเมลล์ของผู้ประเมิน
                                                        // $fmail = $fresult2 . "@medicine.psu.ac.th";
                                                        $fmail = "jthawatc@medicine.psu.ac.th";
                                                        $mail->AddAddress($fmail);
                                                        $mail->Subject = "แจ้งการลาคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                        $mail->Body = $body;
                                                        $mail->CharSet = "utf-8";
                                                        $mail->IsSMTP();
                                                        $mail->SMTPAuth = false;
                                                        $mail->Host = "medicine.psu.ac.th";
                                                        $mail->Port = 25;
                                                        $mail->Send();*/
                                                        // if (!$mail->Send()) {
                                                        //     echo 'Error: ' . $mail->ErrorInfo;
                                                        // } else {
                                                        //     echo "complete";
                                                        // }
                                                    }
                                                }
                                                // echo $dataLineNoti["line_token"];
                                                // $myArray[] = $dataLineNoti;
                                            }
                                        }
                                    }
                                    $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                    echo json_encode($arr);
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                    echo json_encode($arr);
                                }
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้ เนื่องจากมีข้อมูลอยู่ในระบบแล้ว");
                                echo json_encode($arr);
                            }
                        }
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก รหัสบุคลากร $PerId ที่เลือกได้ทำการยืนยันข้อมูลการปฏิบัติงานเรียบร้อยแล้ว");
                        echo json_encode($arr);
                    }
                }
            } else {
                $queryCheckApproveDay = mysqli_query($this->db->hostDB2, "SELECT * FROM HR_CENTER.`working_summary_tb` where classify = '3' AND perid = '" . $PerId . "' AND monthly BETWEEN '" . $yearS . "-" . $monthS . "-01' AND '" . $yearE . "-" . $monthE . "-01'");

                if (mysqli_num_rows($queryCheckApproveDay) == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.PERID = " . $PerId . " AND
                c_take_a_leave.T_Leave_Date_Start
                BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'
                UNION
                SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.PERID = " . $PerId . " AND
                c_take_a_leave.T_Leave_Date_End
                BETWEEN '" . $Leave_Date_Start . "' AND '" . $Leave_Date_End . "'");
                    if ($query) {
                        if ($query->num_rows == 0) {
                            $query = mysqli_query($this->db->hostDB, "INSERT INTO c_take_a_leave (
                            T_Leave_Type,
                            T_Leave_Reason,
                            T_Leave_Date_Start,
                            T_Leave_Date_End,
                            T_Day_Type_Start,
                            T_Day_Type_End,
                            T_All_Summary,
                            T_Work_Summary,
                            T_Leave_CreateBY,
                            T_Leave_UpdateBY,
                            T_Leave_CreateT,
                            T_Leave_UpdateT,
                            PERID,
                            Dep_code,
                            T_Status_Leave,
                            Country,
                            AgentPerson
                        )
                        VALUES
                            (
                                " . $Leave_TypeS . ",
                                '" . $Leave_Reason . "',
                                '" . $Leave_Date_Start . "',
                                '" . $Leave_Date_End . "',
                                '" . $DayTypeStart . "',
                                '" . $DayTypeEnd . "',
                                " . $DateSum . ",
                                " . $DateSumWork . ",
                                '" . $PerId_Create . "',
                                '" . $PerId_Create . "',
                                CURRENT_TIMESTAMP,
                                CURRENT_TIMESTAMP,
                                '" . $PerId . "',
                                " . $DepCode . ",
                                '0',
                                 '" . $Country . "',
                                  '" . $approver . "'
                            )");
                            if ($query) {
                                $queryLineNoti = mysqli_query($this->db->hostDB, "
                                            SELECT
                                                    A.id,
                                                    A.perid,
                                                    A.line_token,
                                                    A.process,
                                                    A.group_name,
                                                    C.`NAME`,
                                                    C.SURNAME,
                                                    D.Edit_code,
                                                    D.Dep_name,
                                                    D.Dep_Group_name,
                                                    D.Dep_Code
                                                    FROM
                                                    HR_CENTER.line_token AS A
                                                    INNER JOIN STAFF.Medperson AS C ON C.PERID = A.perid
                                                    INNER JOIN STAFF.Depart AS D ON D.Dep_Code = C.DEP_WORK
                                                    WHERE
                                                    C.CSTATUS != 0 AND
                                                    D.Edit_code = $EditCode AND
                                                    A.process = 'HR-Time' AND
                                                    A.line_token IS NOT NULL
                            ");
                                if ($queryLineNoti) {
                                    if ($queryLineNoti->num_rows > 0) {
                                        while ($dataLineNoti = mysqli_fetch_assoc($queryLineNoti)) {
                                            $queryPersonal = mysqli_query($this->db->hostDB, "SELECT
                                    STAFF.Medperson.PERID,
                                    STAFF.Medperson.`NAME`,
                                    STAFF.Medperson.SURNAME
                                    FROM
                                    STAFF.Medperson
                                    WHERE
                                    STAFF.Medperson.PERID = $PerId
                                    ");
                                            if ($queryPersonal) {
                                                while ($dataPersonal = mysqli_fetch_assoc($queryPersonal)) {
                                                    if ($dataLineNoti["line_token"] != '' || $dataLineNoti["line_token"] != null) {
                                                        define('LINE_API', "https://notify-api.line.me/api/notify");

                                                        $token = $dataLineNoti["line_token"]; //ใส่Token ที่copy เอาไว้
                                                        switch ($Leave_TypeS) {
                                                            case '0':
                                                                $Type = 'ลากิจส่วนตัว';
                                                                break;
                                                            case  '1':
                                                                $Type = 'ลาป่วย';
                                                                break;
                                                            case  '2':
                                                                $Type = 'ลาพักผ่อน';
                                                                break;
                                                            case  '3':
                                                                $Type = 'ลาศึกษาต่อ';
                                                                break;
                                                            case  '4':
                                                                $Type = 'ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย';
                                                                break;
                                                            case  '5':
                                                                $Type = 'ลาคลอดบุตร';
                                                                break;
                                                            case  '6':
                                                                $Type = 'ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์';
                                                                break;
                                                            case  '7':
                                                                $Type = 'ไปราชการ';
                                                                break;
                                                            default:
                                                                break;
                                                        }

                                                        $message = "แจ้งการลา คุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " ( " . $Type . " ) ตั้งแต่วันที่ " . $this->DateToThai($Leave_Date_Start) . " ( " . $DayTypeStart . " ) " . $this->DateToThai($Leave_Date_End) . " ( " . $DayTypeEnd . " ) เหตุผล " . $Leave_Reason; //ข้อความที่ต้องการส่ง สูงสุด 1000 ตัวอักษร

                                                        $queryData = array('message' => $message);
                                                        $queryData = http_build_query($queryData, '', '&');
                                                        $headerOptions = array(
                                                            'http' => array(
                                                                'method' => 'POST',
                                                                'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
                                                                    . "Authorization: Bearer " . $token . "\r\n"
                                                                    . "Content-Length: " . strlen($queryData) . "\r\n",
                                                                'content' => $queryData,
                                                            ),
                                                        );
                                                        $context = stream_context_create($headerOptions);
                                                        $result = file_get_contents(LINE_API, false, $context);
                                                        $res = json_decode($result);
                                                    }

                                                    /*$mail = new PHPMailer();
                                                    $PerfixNameToSend = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataPersonal['PERID']);
                                                    $s = str_replace('><', '> <', $PerfixNameToSend);
                                                    $fresult = trim(strip_tags($s));
                                                    $mail->From = $fresult . "@medicine.psu.ac.th";
                                                    $mail->isHTML(true);
                                                    $mail->FromName = $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                    $PerfixName = file_get_contents('https://medinfo.psu.ac.th/admin/id2mail.php?medid=' . $dataLineNoti["perid"]);
                                                    $s = str_replace('><', '> <', $PerfixName);
                                                    $fresult2 = trim(strip_tags($s));
                                                    $length = 10;
                                                    $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
                                                    $body = "";
                                                    $body .= "ระบบบันทึกเวลา เข้า - ออกงาน ->> มีข้อมูลการลา ของคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'] . " เข้ามาใหม่ แจ้งลาเมื่อวันที่ " . $dateResult;
                                                    //ส่งเมลล์ไปยังเมลล์ของผู้ประเมิน
                                                    // $fmail = $fresult2 . "@medicine.psu.ac.th";
                                                    $fmail = "jthawatc@medicine.psu.ac.th";
                                                    $mail->AddAddress($fmail);
                                                    $mail->Subject = "แจ้งการลาคุณ " . $dataPersonal['NAME'] . " " . $dataPersonal['SURNAME'];
                                                    $mail->Body = $body;
                                                    $mail->CharSet = "utf-8";
                                                    $mail->IsSMTP();
                                                    $mail->SMTPAuth = false;
                                                    $mail->Host = "medicine.psu.ac.th";
                                                    $mail->Port = 25;
                                                    $mail->Send();*/
                                                    // if (!$mail->Send()) {
                                                    //     echo 'Error: ' . $mail->ErrorInfo;
                                                    // } else {
                                                    //     echo "complete";
                                                    // }
                                                }
                                            }
                                            // echo $dataLineNoti["line_token"];
                                            // $myArray[] = $dataLineNoti;
                                        }
                                    }
                                }
                                $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                echo json_encode($arr);
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                echo json_encode($arr);
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้ เนื่องจากมีข้อมูลอยู่ในระบบแล้ว");
                            echo json_encode($arr);
                        }
                    }
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก รหัสบุคลากร $PerId ที่เลือกได้ทำการยืนยันข้อมูลการปฏิบัติงานเรียบร้อยแล้ว");
                    echo json_encode($arr);
                }
            }

        } else {
            return false;
        }
    }

    // เรียกข้อมูลประเภทวันลา
    public function SELECTLEAVEPERSON($PerId)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave_day.L_Day_Type,
            HRTIME_DB.c_leave_day.L_Day_Detail
            FROM
            HRTIME_DB.c_leave_day
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลประวัติการลาทั้งหมดของบุคลากรรายบุคคล
    public function GETLEAVEUSERPERSONDATADEP($DepCode, $PerId)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_take_a_leave.*,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.Iname,
            STAFF.Medperson.TITLE,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            HRTIME_DB.c_leave.Leave_Detail,
            STAFF.Positions.PosName
        FROM
            HRTIME_DB.c_take_a_leave
        INNER JOIN STAFF.Medperson ON STAFF.Medperson.PERID = HRTIME_DB.c_take_a_leave.PERID
        INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
        INNER JOIN HRTIME_DB.c_leave ON HRTIME_DB.c_leave.Leave_Type = HRTIME_DB.c_take_a_leave.T_Leave_Type
        INNER JOIN STAFF.Positions ON STAFF.Positions.PosCode = STAFF.Medperson.NewPos
        WHERE
            c_take_a_leave.PERID = '" . $PerId . "'
        ORDER BY
            c_take_a_leave.No_Id DESC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function UPDATELEAVEPERSONDATA($No_Id, $LeaveTSelect, $DateStart, $LeaveS_Select, $DateEnd, $LeaveE_Select, $Details, $PerId)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
            SET T_Leave_Type = '" . $LeaveTSelect . "',
            T_Leave_Reason = '" . $Details . "',
            T_Leave_UpdateBY = '" . $PerId . "',
            T_Leave_UpdateT = CURRENT_TIMESTAMP
            WHERE No_Id = " . $No_Id);
            if ($query) {
                $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    public function DELETEDATA($No_Id)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
            if ($query) {
                $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    // เรียกข้อมูลประวัติการลาล่าสุดเพื่อนำข้อมูลไปแสดงในการพิมพ์ใบลา ป่วย ลาคลอดบุตร ลากิจกส่วนตัว
    public function GETHISTORYLEAVEUSER($PerId, $TypeLeave, $DateCondi)
    {
        $myArray = array();
        $myArray2 = array();
        $myArray3 = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            `c_take_a_leave`
        WHERE
            c_take_a_leave.PERID = $PerId
        AND c_take_a_leave.T_Leave_Type = $TypeLeave
        AND c_take_a_leave.T_Leave_Date_Start < '" . $DateCondi . "'
        ORDER BY
            c_take_a_leave.No_Id ASC
        LIMIT 1");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }

                $query2 = mysqli_query($this->db->hostDB, "SELECT
                    *
                FROM
                    c_take_a_leave
                WHERE
                    c_take_a_leave.PERID = $PerId
                AND
                    c_take_a_leave.T_Status_Leave = 1
                AND
                    c_take_a_leave.T_Leave_Type = " . $TypeLeave . "
                ORDER BY
                c_take_a_leave.T_Leave_Date_Start ASC");
                if ($query2) {
                    while ($data = mysqli_fetch_assoc($query2)) {

                        $myArray2[] = $data;

                    }
                }

                $query3 = mysqli_query($this->db->hostDB, "SELECT
                A.PERID,
                A.`NAME`,
                A.SURNAME,
                A.Iname,
                A.TITLE,
                A.DEP_WORK,
                B.Edit_code,
                B.Dep_name,
                B.Dep_Group_name,
                A.Date_In,
                C.PosName,
                C.PosGroup
                FROM
                STAFF.Medperson AS A
                INNER JOIN STAFF.Depart AS B ON A.DEP_WORK = B.Dep_Code
                INNER JOIN STAFF.Positions AS C ON A.NewPos = C.PosCode
                WHERE
                A.PERID = " . $PerId);

                if ($query3) {
                    while ($data = mysqli_fetch_assoc($query3)) {

                        $myArray3[] = $data;

                    }
                }
            } else {

            }
            $arr = array('Leave1' => $myArray, 'Leave2' => $myArray2, 'Profile' => $myArray3);
            $myJSON = json_encode($arr);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                D_holiday
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETHISTORYLEAVEYEAR($Date_Start, $Date_End, $PERID, $Dep_Code, $Leave_Type)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.T_Leave_Type,
            A.T_Leave_Reason,
            A.T_Leave_Date_Start,
            A.T_Leave_Date_End,
            A.T_Day_Type_Start,
            A.T_Day_Type_End,
            A.T_Work_Summary,
            A.T_Leave_CreateBY,
            A.T_Leave_UpdateBY,
            A.T_Leave_CreateT,
            A.T_Leave_UpdateT,
            A.PERID,
            A.Dep_code,
            A.T_Status_Leave,
            B.`NAME`,
            B.SURNAME,
            B.Iname,
            B.TITLE,
            B.DEP_WORK,
            C.Edit_code,
            C.Dep_name,
            B.Date_In,
            D.PosName,
            B.NewPos,
            D.PosGroup
        FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
            INNER JOIN STAFF.Positions AS D ON B.NewPos = D.PosCode
        WHERE
            A.PERID = $PERID
            AND A.T_Leave_Type = $Leave_Type
            AND A.T_Leave_Date_Start BETWEEN '" . $Date_Start . "'
            AND '" . $Date_End . "'
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // อัพเดทข้อมูลวันที่รวม และวันที่ทำการรายการลาสำหรับคนวันที่ในการลา = 10 (คนที่ลาผ่านระบบเก่า)
    public function UPDATEDATECOUNT($DepCode, $PerId, $DataLeaveAll)
    {
        $myArray = array();
        $i = 0;
        $numItems = count($DataLeaveAll);
        $status_in = '';
        $status_in2 = '';
        $status_ho = '';
        $status_hn = '';
        $status_out = '';
        $status_out2 = '';

        if ($this->db->hostDB) {
            foreach ($DataLeaveAll as $key => $value) {
                $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                set T_All_Summary = $value->CountDayOff,
                T_Work_Summary = $value->CountDayWork
                WHERE No_Id = " . $value->LeaveData->No_Id);
                if ($query) {
                    if (++$i === $numItems) {
                        $arr = array('Status' => true, 'Message' => "อัพเดทจำนวนวันของการลาสำเร็จ");
                    }
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถอัพเดทข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    // เรียกข้อมูลประวัติการลาย้อนหลังล่าสุด
    public function GETHISTORYLEAVELAST($PerId, $DepCode, $TypeLeave, $DateCondi)
    {
        $myArray = array();
        $myArrayApprove = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT *
            FROM `c_take_a_leave`
            WHERE
            c_take_a_leave.PERID = $PerId
            AND c_take_a_leave.T_Leave_Type = $TypeLeave
            AND c_take_a_leave.T_Leave_Date_Start < '" . $DateCondi . "'
            AND c_take_a_leave.T_Status_Leave = 1
            ORDER BY c_take_a_leave.T_Leave_Date_Start DESC
            LIMIT 1");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
                $queryApprove = mysqli_query($this->db->hostDB, "SELECT
                A.No_Id,
                A.PERID,
                A.Dep_Code,
                A.P_STATUS,
                A.POSI_STATUS,
                A.CREATE_BY,
                A.CREATE_TIME,
                A.UPDATE_BY,
                A.UPDATE_TIME,
                C.Dep_name,
                C.Dep_Group_name,
                C.Edit_code,
                B.`NAME`,
                B.SURNAME,
                B.Iname,
                B.TITLE,
                B.NewPos
                FROM
                HRTIME_DB.c_config_print AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
                WHERE
                C.Dep_Code = $DepCode
                ");
                if ($queryApprove) {
                    while ($data = mysqli_fetch_assoc($queryApprove)) {

                        $myArrayApprove[] = $data;

                    }
                } else {
                }
            } else {
            }
            $arr = array('LeaveHis' => $myArray, 'PersonApprove' => $myArrayApprove);
            $myJSON = json_encode($arr);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลบุคลากรเพื่อแสดงข้อมูลประวัติบุคลากร
    public function GETDATAPERSONAL()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.Date_In,
            B.Dep_name,
            C.PosName
            FROM
            STAFF.Medperson AS A
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
            LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
            WHERE
            A.PERID = " . $_SESSION['PERID'] . " AND
            A.CSTATUS != 0
            ORDER BY
            A.PERID ASC
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกมูลบุคลากรหน่วยงานที่เกี่ยวข้อง
    public function GETALLDATAPERSON()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                STAFF.Medperson.PERID,
                STAFF.Medperson.`NAME`,
                STAFF.Medperson.SURNAME,
                STAFF.Medperson.Iname,
                STAFF.Medperson.TITLE,
                STAFF.Medperson.DEP_WORK,
                STAFF.Medperson.NewPos,
                STAFF.Depart.Dep_name,
                STAFF.Positions.PosName
                FROM
                STAFF.Medperson
                INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
                INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
                WHERE
                STAFF.Medperson.CSTATUS != 0
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }


    public function GETALLDATAPERSONDep()
    {
        $myArray = array();
        if ($this->db->hostDB) {

            $queryPersonalOther = mysqli_query($this->db->hostDB, "
                        SELECT
                        B.PERID,
                        B.`NAME`,
                        B.SURNAME,
                        B.SEX,
                        B.TITLE,
                        B.NewPos,
                        D.Dep_name,
                        D.Dep_Group_name,
                        A.Dep_Code,
                        D.Edit_code,
                        C.PosName
                    FROM
                        HRTIME_DB.c_medperson AS A
                    INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                    INNER JOIN STAFF.Positions AS C ON B.NewPos = C.PosCode
                    INNER JOIN STAFF.Depart AS D ON B.DEP_WORK = D.Dep_Code
                    WHERE
                        B.PERID = " . $_SESSION['PERID']);

            if (mysqli_num_rows($queryPersonalOther) > 0) {
                while ($DepData = mysqli_fetch_assoc($queryPersonalOther)) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Edit_code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    LEFT JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                     A.Dep_Code = " . $DepData['Dep_Code'] . "
                    ORDER BY
                    A.Dep_name ASC
                    limit 1
                    ");
                    if ($query) {
                        while ($Dep_data = mysqli_fetch_assoc($query)) {
                            $query = mysqli_query($this->db->hostDB, "SELECT
                STAFF.Medperson.PERID,
                STAFF.Medperson.`NAME`,
                STAFF.Medperson.SURNAME,
                STAFF.Medperson.Iname,
                STAFF.Medperson.TITLE,
                STAFF.Medperson.DEP_WORK,
                STAFF.Medperson.NewPos,
                STAFF.Depart.Dep_name,
                STAFF.Positions.PosName
                FROM
                STAFF.Medperson
                INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
                INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
                WHERE
                STAFF.Medperson.CSTATUS != 0
                AND 
                STAFF.Depart.Edit_code = " . $Dep_data['Edit_code'] . "
                ");
                            if ($query) {
                                while ($data = mysqli_fetch_assoc($query)) {

                                    $myArray[] = $data;

                                }
                            } else {
                            }
                            $myJSON = json_encode($myArray);
                        }
                    } else {
                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Edit_code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = " . $_SESSION['PERID'] . "
                    ORDER BY
                    A.Dep_name ASC
                    limit 1
                    ");
                if ($query) {
                    while ($Dep_data = mysqli_fetch_assoc($query)) {
                        $query = mysqli_query($this->db->hostDB, "SELECT
                STAFF.Medperson.PERID,
                STAFF.Medperson.`NAME`,
                STAFF.Medperson.SURNAME,
                STAFF.Medperson.Iname,
                STAFF.Medperson.TITLE,
                STAFF.Medperson.DEP_WORK,
                STAFF.Medperson.NewPos,
                STAFF.Depart.Dep_name,
                STAFF.Positions.PosName
                FROM
                STAFF.Medperson
                INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
                INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
                WHERE
                STAFF.Medperson.CSTATUS != 0
                AND 
                STAFF.Depart.Edit_code = " . $Dep_data['Edit_code'] . "
                ");
                        if ($query) {
                            while ($data = mysqli_fetch_assoc($query)) {

                                $myArray[] = $data;

                            }
                        } else {
                        }
                        $myJSON = json_encode($myArray);
                    }
                } else {
                }
            }
            return $myJSON;
        } else {
            return false;
        }
    }

    // อัพเดทจำนวนครั้งการลา
    public function UPDATELEAVETIME($ObjLeaveTime)
    {
        $i = 0;
        $numItems = count($ObjLeaveTime);
        $myArray = array();
        if ($this->db->hostDB) {
            foreach ($ObjLeaveTime as $key => $value) {
                $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
            SET T_Leave_Time = $value->LeaveTime,
            Fiscal_Year = $value->Fiscal_Year,
            Fiscal_Year_Status = $value->Fiscal_Year_Status
            WHERE
            c_take_a_leave.No_Id = $value->No_Id");
                if ($query) {
                    if (++$i === $numItems) {
                        $arr = array('Status' => true, 'Message' => "อัพเดทจำนวนครั้งในการลาสำเร็จ");
                    }
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถอัพเดทข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    //เรียกข้อมูลการตั้งค่าการลาเริ่มต้นของหน่วยงาน
    public function GETLEAVESET($Dep_Code, $PerId)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.PERID = $PerId"
            );
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลการตั้งค่าเริ่มต้นของการลา
    public function GETLEAVESTART($PerId, $TypeLeave)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HRTIME_DB.c_leave_set
                WHERE
                HRTIME_DB.c_leave_set.Leave_Type = $TypeLeave
                AND
                HRTIME_DB.c_leave_set.PERID = $PerId"
            );
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETMAXVALUELEAVE($Year, $PerId)
    {
        $LeaveHistory = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i <= 7; $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            Max(A.T_Leave_Time) AS MaxLeave,
            A.T_Leave_Type,
            B.Leave_Detail,
            Sum(A.T_Work_Summary) AS SumWorkDay
            FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN HRTIME_DB.c_leave AS B ON A.T_Leave_Type = B.Leave_Type
            WHERE
            A.PERID = $PerId AND
            A.T_Leave_Type = $i AND
            A.T_Status_Leave != 0 AND
            A.T_Leave_Date_Start BETWEEN '" . ((int)$Year - 1) . "-10-01' AND '" . $Year . "-09-30'"
                );
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {
                        array_push($LeaveHistory, array($data));
                    }
                } else {
                }
            }
            $myJSON = json_encode($LeaveHistory);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function DELETEDATAUSER($No_Id)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $querySelectData = mysqli_query($this->db->hostDB, "SELECT * FROM c_take_a_leave
    WHERE No_Id = " . $No_Id);
            if (mysqli_num_rows($querySelectData) >= 1) {
                $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
    WHERE No_Id = " . $No_Id);
                if ($query) {
                    $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                }
                echo json_encode($arr);
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้ เนื่องจากไม่พบข้อมูลในระบบ");
                echo json_encode($arr);
            }
        } else {
            return false;
        }
    }

    public function UPDATEAPPROVEMANAGER($ManagerAppr, $ApproveFrist, $Investigate, $LeaveId)
    {
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
            SET ManagerAppr = " . $ManagerAppr . ",
            ApproveFrist = " . $ApproveFrist . ",
            Investigate = " . $Investigate . "
            WHERE
            c_take_a_leave.No_Id = $LeaveId");
            if ($query) {
                $arr = array('Status' => true, 'Message' => "อัพเดทประวัติผู้อนุมัติสำเร็จ");
            } else {
                $arr = array('Status' => false, 'Message' => "อัพเดทประวัติผู้อนุมัติไม่สำเร็จ");
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    public function DateToThai($date)
    {
        $dateToThai = explode('-', $date);
        $date = $dateToThai[2];
        $month = '';
        $year = ((int)$dateToThai[0]) + 543;

        switch ($dateToThai[1]) {
            case "01":
                $month = 'มกราคม';
                break;
            case "02":
                $month = 'กุมภาพันธ์';
                break;
            case "03":
                $month = 'มีนาคม';
                break;
            case "04":
                $month = 'เมษายน';
                break;
            case "05":
                $month = 'พฤษภาคม';
                break;
            case "06":
                $month = 'มิถุนายน';
                break;
            case "07":
                $month = 'กรกฎาคม';
                break;
            case "08":
                $month = 'สิงหาคม';
                break;
            case "09":
                $month = 'กันยายน';
                break;
            case "10":
                $month = 'ตุลาคม';
                break;
            case "11":
                $month = 'พฤศจิกายน';
                break;
            case "12":
                $month = 'ธันวาคม';
                break;
            default:
                break;
        }

        return $date . ' ' . $month . ' ' . $year;
    }

}
