<?php
ini_set('max_execution_time', 600);

class DateModel extends Model
{

    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function DateAllData()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, constant('Date_All_Data'));
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GatAllDepartment()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                D_holiday
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0 และ Dep_Code = Depcode
    public function GetDataFromDep($DepCode, $DateConrdi)
    {
        $myArray = array();
        $Depart_Data = array();
        for ($i = 0; $i < count($DepCode); $i++) {
            if ($this->db->hostDB) {
                $query = mysqli_query($this->db->hostDB, "
                SELECT
                    A.PERID,
                    A.door_id,
                    A.card_no,
                    A.event_time,
                    B.`NAME`,
                    B.SURNAME,
                    B.POS_WORK,
                    C.Dep_Code,
                    C.Dep_name,
                    C.Dep_Group_name
                FROM
                    HRTIME_DB.D_transaction AS A
                LEFT JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                LEFT JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                LEFT JOIN HRTIME_DB.C_door AS D ON D.door_id = A.door_id
                LEFT JOIN HRTIME_DB.C_card AS E ON E.CARD_NO = A.card_no
                WHERE
                    A.PERID != 0
                AND A.event_time BETWEEN '" . $DateConrdi . " 00:00:00'
                AND '" . $DateConrdi . " 23:59:59'
                AND C.Dep_Code = " . $DepCode[$i]->Dep_Code . "
                AND B.CSTATUS != 0
                AND D.zone_id != 5
                AND D.zone_id != 9
                AND E.dept_id NOT IN (SELECT id FROM ignore_deppartment)
                ORDER BY C.Dep_Group_name,A.event_time ASC");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

//                        $myArray[] = $data;
                        array_push($Depart_Data, $data);

                    }

                    $queryPersonDepOther = mysqli_query($this->db->hostDB, "
                        SELECT
                            A.PERID,
                            A.door_id,
                            A.card_no,
                            A.event_time,
                            B.`NAME`,
                            B.SURNAME,
                            B.POS_WORK,
                            C.Dep_Code,
                            C.Dep_name,
                            C.Dep_Group_name
                        FROM
                            HRTIME_DB.c_medperson AS K
                        LEFT JOIN HRTIME_DB.D_transaction AS A ON A.PERID = K.PERID
                        LEFT JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                        LEFT JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                        LEFT JOIN HRTIME_DB.C_door AS D ON D.door_id = A.door_id
                        LEFT JOIN HRTIME_DB.C_card AS E ON E.CARD_NO = A.card_no
                        WHERE
                            A.PERID != 0
                        AND A.event_time BETWEEN '" . $DateConrdi . " 00:00:00'
                        AND '" . $DateConrdi . " 23:59:59'
                        AND K.Dep_Code = " . $DepCode[$i]->Dep_Code . "
                        AND B.CSTATUS != 0
                        AND D.zone_id != 5
                        AND D.zone_id != 9
                        AND E.dept_id NOT IN (SELECT id FROM ignore_deppartment)
                        ORDER BY C.Dep_Group_name,A.event_time ASC");
                    if ($queryPersonDepOther) {
                        while ($data = mysqli_fetch_assoc($queryPersonDepOther)) {

                            array_push($Depart_Data, $data);

                        }
                    }
                } else {
                }
//                $myJSON = json_encode($myArray);
//                echo $myJSON;
            } else {
                return false;
            }
        }
        $myJSON = json_encode($Depart_Data);
        echo $myJSON;
    }

    // เรียกข้อมูลรายบุคคลจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID = key และ Dep_Code = Depcode event_time = Dataconrdi
    public function GetDataFromDep_Condi($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT HRTIME_DB.D_transaction.PERID,HRTIME_DB.D_transaction.door_id, HRTIME_DB.D_transaction.event_time, STAFF.Medperson.NAME, STAFF.Medperson.SURNAME, STAFF.Depart.Dep_Code, STAFF.Depart.Dep_name, STAFF.Depart.Dep_Group_name
            FROM HRTIME_DB.D_transaction
            JOIN STAFF.Medperson
            ON HRTIME_DB.D_transaction.PERID = STAFF.Medperson.PERID
            JOIN STAFF.Depart
            ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
            WHERE HRTIME_DB.D_transaction.PERID = " . $PERID . "
            AND STAFF.Depart.Dep_Code = " . $DepCode . "
            AND HRTIME_DB.D_transaction.event_time BETWEEN '" . $DateConrdi . " 00:00:00'
                AND '" . $DateConrdi . " 23:59:59'
            ORDER BY  STAFF.Depart.Dep_Group_name,HRTIME_DB.D_transaction.event_time ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรของหน่วยงานแต่ล่ะหน่วยงาน โดยใช้เงื่อนไขวันที่และฝ่าย
    public function GETPERSONDATE($DepCode, $DateConrdi)
    {
        $myArray = array();
        $DoorDataArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            S.NAME,
            S.SURNAME,
            S.PERID,
            S.DEP_WORK,
            D.Dep_name,
            D.Dep_Code,
            P_Time_DateTime,
            P_Time_TimeN,
            P_Time_TimeO,
            P_Time_TimeHN,
            P_Time_TimeHO,
            P_DoorID,
            P_Time_UpdateBY,
            P_Time_UpdateT,
                    door_name
        FROM
            STAFF.Medperson AS S
        LEFT JOIN (
            SELECT
        B.PERID,
        B.P_Time_DateTime,
        B.P_Time_TimeN,
        B.P_Time_TimeO,
        B.P_Time_TimeHN,
        B.P_Time_TimeHO,
        B.P_DoorID,
        B.P_Time_UpdateBY,
        B.P_Time_UpdateT,
        C_door.door_name
        FROM
            HRTIME_DB.c_person_time AS A
        LEFT JOIN HRTIME_DB.c_person_time AS B ON B.P_Time_ID = (
            SELECT
                MAX(P_Time_ID)
            FROM
                HRTIME_DB.c_person_time
            WHERE
                P_Time_DateTime <= '" . $DateConrdi . "'
            AND PERID = A.PERID
        )

        LEFT JOIN C_door ON C_door.door_id = B.P_DoorID
        GROUP BY
            B.PERID
        ) AS H ON S.PERID = H.PERID
        LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
        WHERE
            D.Dep_Code = " . $DepCode[$i]->Dep_Code . "
        AND S.CSTATUS != 0
        ORDER BY
            S.PERID ASC");
                if ($query) {

                    while ($data = mysqli_fetch_assoc($query)) {
                        $DoorDataArray = array();

                        if ($data['P_Time_DateTime'] != null || $data['P_Time_DateTime'] != '') {
                            $queryAllDATA = mysqli_query($this->db->hostDB, "SELECT
                                A.P_Time_ID,
                                A.PERID,
                                A.Dep_code,
                                A.P_Time_DateTime,
                                A.P_Time_TimeN,
                                A.P_Time_TimeO,
                                A.P_Time_TimeHN,
                                A.P_Time_TimeHO,
                                A.P_Time_CreateBY,
                                A.P_Time_UpdateBY,
                                A.P_Time_CreateT,
                                A.P_Time_UpdateT,
                                B.door_name,
                                B.zone_id,
                                B.door_id
                                FROM
                                c_person_time AS A
                                LEFT JOIN C_door AS B ON B.door_id = A.P_DoorID
                                WHERE A.PERID = " . $data['PERID'] . "
                                AND 
                                A.P_Time_DateTime = '" . $data['P_Time_DateTime'] . "'
                            ");
                            if ($queryAllDATA) {

                                while ($dataAllData = mysqli_fetch_assoc($queryAllDATA)) {
                                    if ($dataAllData['door_id'] != null) {
                                        array_push($DoorDataArray, array(
                                            'door_id' => $dataAllData['door_id'],
                                            'door_name' => $dataAllData['door_name']
                                        ));
                                    } else {
                                        array_push($DoorDataArray, array(
                                            'door_id' => 0,
                                            'door_name' => 'ประตูทั้งหมด'
                                        ));
                                    }
                                }

                                array_push($myArray, array(
                                    'NAME' => $data['NAME'],
                                    'SURNAME' => $data['SURNAME'],
                                    'PERID' => $data['PERID'],
                                    'DEP_WORK' => $data['DEP_WORK'],
                                    'Dep_name' => $data['Dep_name'],
                                    'Dep_Code' => $data['Dep_Code'],
                                    'P_Time_DateTime' => $data['P_Time_DateTime'],
                                    'P_Time_TimeN' => $data['P_Time_TimeN'],
                                    'P_Time_TimeO' => $data['P_Time_TimeO'],
                                    'P_Time_TimeHN' => $data['P_Time_TimeHN'],
                                    'P_Time_TimeHO' => $data['P_Time_TimeHO'],
                                    'P_Time_UpdateBY' => $data['P_Time_UpdateBY'],
                                    'P_Time_UpdateT' => $data['P_Time_UpdateT'],
                                    'DoorData' => $DoorDataArray
                                ));

                            } else {
                                return false;
                            }
                        } else {
                            array_push($myArray, array(
                                'NAME' => $data['NAME'],
                                'SURNAME' => $data['SURNAME'],
                                'PERID' => $data['PERID'],
                                'DEP_WORK' => $data['DEP_WORK'],
                                'Dep_name' => $data['Dep_name'],
                                'Dep_Code' => $data['Dep_Code'],
                                'P_Time_DateTime' => $data['P_Time_DateTime'],
                                'P_Time_TimeN' => $data['P_Time_TimeN'],
                                'P_Time_TimeO' => $data['P_Time_TimeO'],
                                'P_Time_TimeHN' => $data['P_Time_TimeHN'],
                                'P_Time_TimeHO' => $data['P_Time_TimeHO'],
                                'P_Time_UpdateBY' => $data['P_Time_UpdateBY'],
                                'P_Time_UpdateT' => $data['P_Time_UpdateT'],
                                'DoorData' => $DoorDataArray
                            ));
                        }
//                        $myArray[] = $data;
//                        array_push($myArray, $data);
                    }

                    $queryPersonDepOther = mysqli_query($this->db->hostDB, "SELECT
            S.NAME,
            S.SURNAME,
            S.PERID,
            S.DEP_WORK,
            D.Dep_name,
            D.Dep_Code,
            P_Time_DateTime,
            P_Time_TimeN,
            P_Time_TimeO,
            P_Time_TimeHN,
            P_Time_TimeHO,
            P_DoorID,
            P_Time_UpdateBY,
            P_Time_UpdateT,
                    door_name
        FROM
            HRTIME_DB.c_medperson AS P
        LEFT JOIN
            STAFF.Medperson AS S ON P.PERID = S.PERID
        LEFT JOIN (
            SELECT
        B.PERID,
        B.P_Time_DateTime,
        B.P_Time_TimeN,
        B.P_Time_TimeO,
        B.P_Time_TimeHN,
        B.P_Time_TimeHO,
        B.P_DoorID,
        B.P_Time_UpdateBY,
        B.P_Time_UpdateT,
        C_door.door_name
        FROM
            HRTIME_DB.c_person_time AS A
        LEFT JOIN HRTIME_DB.c_person_time AS B ON B.P_Time_ID = (
            SELECT
                MAX(P_Time_ID)
            FROM
                HRTIME_DB.c_person_time
            WHERE
                P_Time_DateTime <= '" . $DateConrdi . "'
            AND PERID = A.PERID
        )

        LEFT JOIN C_door ON C_door.door_id = B.P_DoorID
        GROUP BY
            B.PERID
        ) AS H ON S.PERID = H.PERID
        LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
        WHERE
            P.Dep_Code = " . $DepCode[$i]->Dep_Code . "
        AND S.CSTATUS != 0
        ORDER BY
            S.PERID ASC");
                    if ($queryPersonDepOther) {

                        while ($data = mysqli_fetch_assoc($queryPersonDepOther)) {
                            $DoorDataArray = array();

                            if ($data['P_Time_DateTime'] != null || $data['P_Time_DateTime'] != '') {
                                $queryAllDATA = mysqli_query($this->db->hostDB, "SELECT
                                A.P_Time_ID,
                                A.PERID,
                                A.Dep_code,
                                A.P_Time_DateTime,
                                A.P_Time_TimeN,
                                A.P_Time_TimeO,
                                A.P_Time_TimeHN,
                                A.P_Time_TimeHO,
                                A.P_Time_CreateBY,
                                A.P_Time_UpdateBY,
                                A.P_Time_CreateT,
                                A.P_Time_UpdateT,
                                B.door_name,
                                B.zone_id,
                                B.door_id
                                FROM
                                c_person_time AS A
                                LEFT JOIN C_door AS B ON B.door_id = A.P_DoorID
                                WHERE A.PERID = " . $data['PERID'] . "
                                AND 
                                A.P_Time_DateTime = '" . $data['P_Time_DateTime'] . "'
                            ");
                                if ($queryAllDATA) {

                                    while ($dataAllData = mysqli_fetch_assoc($queryAllDATA)) {
                                        if ($dataAllData['door_id'] != null) {
                                            array_push($DoorDataArray, array(
                                                'door_id' => $dataAllData['door_id'],
                                                'door_name' => $dataAllData['door_name']
                                            ));
                                        } else {
                                            array_push($DoorDataArray, array(
                                                'door_id' => 0,
                                                'door_name' => 'ประตูทั้งหมด'
                                            ));
                                        }
                                    }

                                    array_push($myArray, array(
                                        'NAME' => $data['NAME'],
                                        'SURNAME' => $data['SURNAME'],
                                        'PERID' => $data['PERID'],
                                        'DEP_WORK' => $data['DEP_WORK'],
                                        'Dep_name' => $data['Dep_name'],
                                        'Dep_Code' => $data['Dep_Code'],
                                        'P_Time_DateTime' => $data['P_Time_DateTime'],
                                        'P_Time_TimeN' => $data['P_Time_TimeN'],
                                        'P_Time_TimeO' => $data['P_Time_TimeO'],
                                        'P_Time_TimeHN' => $data['P_Time_TimeHN'],
                                        'P_Time_TimeHO' => $data['P_Time_TimeHO'],
                                        'P_Time_UpdateBY' => $data['P_Time_UpdateBY'],
                                        'P_Time_UpdateT' => $data['P_Time_UpdateT'],
                                        'DoorData' => $DoorDataArray
                                    ));

                                } else {
                                    return false;
                                }
                            } else {
                                array_push($myArray, array(
                                    'NAME' => $data['NAME'],
                                    'SURNAME' => $data['SURNAME'],
                                    'PERID' => $data['PERID'],
                                    'DEP_WORK' => $data['DEP_WORK'],
                                    'Dep_name' => $data['Dep_name'],
                                    'Dep_Code' => $data['Dep_Code'],
                                    'P_Time_DateTime' => $data['P_Time_DateTime'],
                                    'P_Time_TimeN' => $data['P_Time_TimeN'],
                                    'P_Time_TimeO' => $data['P_Time_TimeO'],
                                    'P_Time_TimeHN' => $data['P_Time_TimeHN'],
                                    'P_Time_TimeHO' => $data['P_Time_TimeHO'],
                                    'P_Time_UpdateBY' => $data['P_Time_UpdateBY'],
                                    'P_Time_UpdateT' => $data['P_Time_UpdateT'],
                                    'DoorData' => $DoorDataArray
                                ));
                            }
                        }
                    }

                } else {
                }
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เลือกบุคลากรรายบุคคลที่ได้จากการตั้งค่าเวลาปฏิบัติงานกรณีพิเศษ โดยใช้เงื่อนไขวันที่มาเปรียบเทียบ และ PERID มาเปรียบเทียบ
    public function GETPERSONDATEPERSONAL($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
        S.NAME,
        S.SURNAME,
        S.PERID,
        S.DEP_WORK,
        D.Dep_name,
        D.Dep_Code,
        P_Time_DateTime,
        P_Time_TimeN,
        P_Time_TimeO,
        P_Time_TimeHN,
        P_Time_TimeHO,
        P_Time_UpdateBY,
        P_Time_UpdateT
    FROM
        STAFF.Medperson AS S
    INNER JOIN (
        SELECT
	B.PERID,
	B.P_Time_DateTime,
	B.P_Time_TimeN,
	B.P_Time_TimeO,
	B.P_Time_TimeHN,
	B.P_Time_TimeHO,
	B.P_Time_UpdateBY,
	B.P_Time_UpdateT
    FROM
        HRTIME_DB.c_person_time AS A
    INNER JOIN HRTIME_DB.c_person_time AS B ON B.P_Time_ID = (
        SELECT
            MAX(P_Time_ID)
        FROM
            HRTIME_DB.c_person_time
        WHERE
            P_Time_DateTime <= '" . $DateConrdi . "'
        AND PERID = A.PERID
    )
    GROUP BY
        B.PERID
    ) AS H ON S.PERID = H.PERID
    INNER JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
    WHERE
        S.DEP_WORK = " . $DepCode . "
    AND S.PERID = " . $PERID . "
    AND S.CSTATUS != 0
    ORDER BY
        S.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรของหน่วยงานแต่ล่ะหน่วยงาน
    public function GETPERSONDATENOCORD($DepCode)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                    S.NAME,
                    S.SURNAME,
                    S.PERID,
                    S.DEP_WORK,
                    D.Dep_name,
                    P_Time_DateTime,
                    P_Time_TimeN,
                    P_Time_TimeO,
                    P_Time_TimeHN,
                    P_Time_TimeHO,
                    P_Time_UpdateBY,
                    P_Time_UpdateT
                FROM
                    STAFF.Medperson AS S
                    LEFT JOIN ( SELECT PERID, P_Time_DateTime, P_Time_TimeN, P_Time_TimeO, P_Time_TimeHN, P_Time_TimeHO, P_Time_UpdateBY, P_Time_UpdateT FROM HRTIME_DB.c_person_time WHERE P_Time_DateTime = '" . $dateNow . "' ) AS H ON S.PERID = H.PERID
                    LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
                WHERE
                    S.DEP_WORK = " . $DepCode . "
                    AND S.CSTATUS != 0
                ORDER BY
                    S.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลเวลาของหน่วยงานจากการตั้งค่าเวลา
    public function GETDATESETTING($Date, $DepCode)
    {
        $myArray = array();
        $myArray2 = array();
        $Dep_Time_Data = array();

        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.D_Time_ID,
            A.D_Time_DateTime,
            A.D_Time_TimeN,
            A.D_Time_TimeO,
            A.D_Time_TimeHN,
            A.D_Time_TimeHO,
            A.D_Time_CreateBY,
            A.D_Time_UpdateBY,
            A.D_Time_CreateT,
            A.D_Time_UpdateT,
            A.Dep_code,
            A.door_id,
            B.Dep_Code
            FROM
            HRTIME_DB.c_depart_time AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            WHERE
            A.D_Time_DateTime <= '" . $Date . "' AND
            B.Dep_Code = '" . $DepCode[$i]->Dep_Code . "'
            ORDER BY
            A.D_Time_DateTime DESC
            LIMIT 1");
                if (mysqli_num_rows($query) > 0) {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $myArray2 = array();
//                        array_push($myArray, $data);

                        $query2 = mysqli_query($this->db->hostDB, "SELECT
                    A.D_Time_ID,
                    A.D_Time_DateTime,
                    A.D_Time_TimeN,
                    A.D_Time_TimeO,
                    A.D_Time_TimeHN,
                    A.D_Time_TimeHO,
                    A.D_Time_CreateBY,
                    A.D_Time_UpdateBY,
                    A.D_Time_CreateT,
                    A.D_Time_UpdateT,
                    A.Dep_code,
                    A.door_id,
                    B.Dep_Code
                    FROM
                    HRTIME_DB.c_depart_time AS A
                    INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                    WHERE
                    A.D_Time_DateTime = '" . $data['D_Time_DateTime'] . "' AND
                    B.Dep_Code = '" . $data['Dep_Code'] . "'
                    ORDER BY
                    A.D_Time_DateTime DESC");
                        if ($query2) {
                            while ($dataAllDoor = mysqli_fetch_assoc($query2)) {
//                                    array_push($myArray2, $data);
                                array_push($myArray2, array('door_id' => $dataAllDoor['door_id']));
                            }

                        }

                        array_push($myArray, array(
                            'D_Time_ID' => $data['D_Time_ID'],
                            'D_Time_DateTime' => $data['D_Time_DateTime'],
                            'D_Time_TimeN' => $data['D_Time_TimeN'],
                            'D_Time_TimeO' => $data['D_Time_TimeO'],
                            'D_Time_TimeHN' => $data['D_Time_TimeHN'],
                            'D_Time_TimeHO' => $data['D_Time_TimeHO'],
                            'D_Time_CreateBY' => $data['D_Time_CreateBY'],
                            'D_Time_UpdateBY' => $data['D_Time_UpdateBY'],
                            'D_Time_CreateT' => $data['D_Time_CreateT'],
                            'D_Time_UpdateT' => $data['D_Time_UpdateT'],
                            'Dep_code' => $data['Dep_code'],
                            'Dep_Code' => $data['Dep_Code'],
                            'DoorData' => $myArray2
                        ));

                    }

                } else {
                }
            }
//            array_push($Dep_Time_Data, array('Data1' => $myArray, 'Data2' => $myArray2));
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //คำนวณหาจำนวนวัน โดยใช้วันที่ปัจจุบันลบกับวันที่เลือกทำรายการ เพื่อหาค่าจำนวนวัน < 30 ไม่อนุญาติให้แก้ไขข้อมูล
    public function CALDATEDIFFCONDI($DateStart, $DateEnd)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT DATEDIFF('$DateStart', '$DateEnd') AS DateDiff");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //คำนวณหาวันที่ หักลบ จากการคลิกปุ่มเลื่อนวันที่ไปข้างหน้า หรือ หลัง
    public function CALDATESUBCONDI($DateStart, $IntevalDay)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT DATE_SUB('$DateStart', INTERVAL $IntevalDay DAY)");
            if ($query) {
                while ($data = mysqli_fetch_row($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLDOORHOS()
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            C_door
        WHERE
            zone_id != 5
            AND zone_id != 9");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //คำนวณหาวันที่ เพิ่ม จากการคลิกปุ่มเลื่อนวันที่ไปข้างหน้า
    public function CALDATEADDCONDI($DateStart, $IntevalDay)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT DATE_ADD('$DateStart', INTERVAL $IntevalDay DAY)");
            if ($query) {
                while ($data = mysqli_fetch_row($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLDOORHOSOUTDepart()
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            C_door
        WHERE
            zone_id = 5
        UNION
        SELECT
            *
        FROM
            C_door
        WHERE
            zone_id = 9");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETRANGETIMEDATA($TimeStart, $TimeEnd, $DepCode)
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Dep_name,
            HRTIME_DB.D_transaction.event_time
        FROM
            HRTIME_DB.D_transaction
            INNER JOIN STAFF.Medperson ON HRTIME_DB.D_transaction.PERID = STAFF.Medperson.PERID
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
        WHERE
            HRTIME_DB.D_transaction.event_time BETWEEN '" . $TimeStart . "'
            AND '" . $TimeEnd . "'
            AND STAFF.Depart.Dep_Code = '" . $DepCode . "'
        ORDER BY
            STAFF.Medperson.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETCARDNOPERSON($PERID)
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            C_card.PERID,
            C_card.CARD_NO
            FROM
            C_card
            WHERE
            C_card.PERID = " . $PERID . "
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เพิ่มข้อมูลที่ได้จากการ Approve จากหัวหน้าหน่วยงาน เพื่อนำไปสู่การประมวลผลสรุปรายงาน
    public function APPROVEDATATIME($C_PERID, $U_PERID, $OBJDATA, $DATENOW)
    {
        $myArray = array();
        $i = 0;
        $numItems = count($OBJDATA);
        $status_in = '';
        $status_in2 = '';
        $status_ho = '';
        $status_hn = '';
        $status_out = '';
        $status_out2 = '';

        if ($this->db->hostDB) {
            foreach ($OBJDATA as $key => $value) {
                if (strpos($value->TIME_IN, "สาย") > 0) {
                    $status_in = "สาย";
                } else {
                    $status_in = "";
                }
                if (strpos($value->TIME_IN2, "สาย") > 0) {
                    $status_in2 = "สาย";
                } else {
                    $status_in2 = "";
                }
                if (strpos($value->TIME_HO, "สาย") > 0) {
                    $status_ho = "สาย";
                } else if (strpos($value->TIME_HO, "ออกก่อน") > 0) {
                    $status_ho = "ออกก่อน";
                } else {
                    $status_ho = "";
                }
                if (strpos($value->TIME_HN, "สาย") > 0) {
                    $status_hn = "สาย";
                } else {
                    $status_hn = "";
                }
                if (strpos($value->TIME_OUT, "สาย") > 0) {
                    $status_out = "สาย";
                } else if (strpos($value->TIME_OUT, "ออกก่อน") > 0) {
                    $status_out = "ออกก่อน";
                } else {
                    $status_out = "";
                }
                if (strpos($value->TIME_OUT2, "สาย") > 0) {
                    $status_out2 = "สาย";
                } else if (strpos($value->TIME_OUT2, "ออกก่อน") > 0) {
                    $status_out2 = "ออกก่อน";
                } else {
                    $status_out2 = "";
                }

                // ตรวจสอบข้อมูลการลาถ้ามีการลาในเวลาการปฏิบัติงานให้ ตั้งค่าเป็นค่าว่างเอาไว้
                if (strpos($value->TIME_IN, "ลา") > 0) {
                    $value->TIME_IN = "";
                } else if (strpos($value->TIME_IN, "ไปราชการ") > 0) {
                    $value->TIME_IN = "";
                }
                if (strpos($value->TIME_IN2, "ลา") > 0) {
                    $value->TIME_IN2 = "";
                } else if (strpos($value->TIME_IN2, "ไปราชการ") > 0) {
                    $value->TIME_IN2 = "";
                }
                if (strpos($value->TIME_HO, "ลา") > 0) {
                    $value->TIME_HO = "";
                } else if (strpos($value->TIME_HO, "ไปราชการ") > 0) {
                    $value->TIME_HO = "";
                }
                if (strpos($value->TIME_HN, "ลา") > 0) {
                    $value->TIME_HN = "";
                } else if (strpos($value->TIME_HN, "ไปราชการ") > 0) {
                    $value->TIME_HN = "";
                }
                if (strpos($value->TIME_OUT, "ลา") > 0) {
                    $value->TIME_OUT = "";
                } else if (strpos($value->TIME_OUT, "ไปราชการ") > 0) {
                    $value->TIME_OUT = "";
                }
                if (strpos($value->TIME_OUT2, "ลา") > 0) {
                    $value->TIME_OUT2 = "";
                } else if (strpos($value->TIME_OUT2, "ไปราชการ") > 0) {
                    $value->TIME_OUT2 = "";
                }
                /////////////////////////////////////////////////////////////////////

                $query = mysqli_query($this->db->hostDB, "INSERT INTO `A_transaction` (
                    `PERID`,
                    `Dep_Code`,
                    `A_Datetime`,
                    `A_Time_in`,
                    `A_Status_in`,
                    `A_Door_in`,
                    `A_Time_in2`,
                    `A_Status_in2`,
                    `A_Door_in2`,
                    `A_Time_ho`,
                    `A_Status_ho`,
                    `A_Door_ho`,
                    `A_Time_hn`,
                    `A_Status_hn`,
                    `A_Door_hn`,
                    `A_Time_out`,
                    `A_Status_out`,
                    `A_Door_out`,
                    `A_Time_out2`,
                    `A_Status_out2`,
                    `A_Door_out2`,
                    `Create_By`,
                    `Create_Time`,
                    `Update_By`,
                    `Update_Time`
                )
                VALUES
                    (
                        '" . $value->PERID . "',
                        '" . $value->DEP_CODE . "',
                        '" . $DATENOW . "',
                        '" . $value->TIME_IN . "',
                        '" . $status_in . "',
                        '" . $value->DOOR_IN . "',
                        '" . $value->TIME_IN2 . "',
                        '" . $status_in2 . "',
                        '" . $value->DOOR_IN2 . "',
                        '" . $value->TIME_HO . "',
                        '" . $status_ho . "',
                        '" . $value->DOOR_HO . "',
                        '" . $value->TIME_HN . "',
                        '" . $status_hn . "',
                        '" . $value->DOOR_HN . "',
                        '" . $value->TIME_OUT . "',
                        '" . $status_out . "',
                        '" . $value->DOOR_OUT . "',
                        '" . $value->TIME_OUT2 . "',
                        '" . $status_out2 . "',
                        '" . $value->DOOR_OUT2 . "',
                        '" . $C_PERID . "',
                        CURRENT_TIMESTAMP,
                        '" . $U_PERID . "',
                        CURRENT_TIMESTAMP
                    );
                ");
                if ($query) {
                    if (++$i === $numItems) {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    }
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    // เรียกข้อมูลการปฏิบัติงานของบุคลากรในหน่วยงานที่ได้จากการ Approve โดยหัวหน้างาน
    public function GETDATAAPPORVEDEPART($DepCode, $DateConrdi)
    {
        $myArray = array();
        $allDataApprove = array();

        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.PERID,
            A.Dep_Code,
            A.A_Datetime,
            A.A_Time_in,
            A.A_Status_in,
            A.A_Door_in,
            A.A_Time_in2,
            A.A_Status_in2,
            A.A_Door_in2,
            A.A_Time_ho,
            A.A_Status_ho,
            A.A_Door_ho,
            A.A_Time_hn,
            A.A_Status_hn,
            A.A_Door_hn,
            A.A_Time_out,
            A.A_Status_out,
            A.A_Door_out,
            A.A_Time_out2,
            A.A_Status_out2,
            A.A_Door_out2,
            A.Create_By,
            A.Create_Time,
            A.Update_By,
            A.Update_Time,
            B.`NAME`,
            B.SURNAME,
            B.POS_WORK,
            C.Dep_Code
        FROM
            A_transaction AS A
        INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
        WHERE
            A.A_Datetime = '" . $DateConrdi . "'
        AND C.Dep_Code = " . $DepCode[$i]->Dep_Code);
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

//                        $myArray[] = $data;
                        array_push($allDataApprove, $data);

                    }

                    $queryPersonDepOther = mysqli_query($this->db->hostDB, "
                        SELECT
                                    A.No_Id,
                                    A.PERID,
                                    A.Dep_Code,
                                    A.A_Datetime,
                                    A.A_Time_in,
                                    A.A_Status_in,
                                    A.A_Door_in,
                                    A.A_Time_in2,
                                    A.A_Status_in2,
                                    A.A_Door_in2,
                                    A.A_Time_ho,
                                    A.A_Status_ho,
                                    A.A_Door_ho,
                                    A.A_Time_hn,
                                    A.A_Status_hn,
                                    A.A_Door_hn,
                                    A.A_Time_out,
                                    A.A_Status_out,
                                    A.A_Door_out,
                                    A.A_Time_out2,
                                    A.A_Status_out2,
                                    A.A_Door_out2,
                                    A.Create_By,
                                    A.Create_Time,
                                    A.Update_By,
                                    A.Update_Time,
                                    B.`NAME`,
                                    B.SURNAME,
                                    B.POS_WORK,
                                    C.Dep_Code
                                FROM
                                 c_medperson AS D
                                LEFT JOIN A_transaction AS A ON D.PERID = A.PERID
                                INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
                                WHERE
                                    A.A_Datetime = '" . $DateConrdi . "'
                                AND D.Dep_Code = " . $DepCode[$i]->Dep_Code);

                    if (mysqli_num_rows($queryPersonDepOther) > 0) {
                        while ($dataDepOther = mysqli_fetch_assoc($queryPersonDepOther)) {
                            array_push($allDataApprove, $dataDepOther);
                        }
                    }

                } else {
                }
            }

            echo json_encode($allDataApprove);

//            $query = mysqli_query($this->db->hostDB, "SELECT
//            A.No_Id,
//            A.PERID,
//            A.Dep_Code,
//            A.A_Datetime,
//            A.A_Time_in,
//            A.A_Status_in,
//            A.A_Door_in,
//            A.A_Time_in2,
//            A.A_Status_in2,
//            A.A_Door_in2,
//            A.A_Time_ho,
//            A.A_Status_ho,
//            A.A_Door_ho,
//            A.A_Time_hn,
//            A.A_Status_hn,
//            A.A_Door_hn,
//            A.A_Time_out,
//            A.A_Status_out,
//            A.A_Door_out,
//            A.A_Time_out2,
//            A.A_Status_out2,
//            A.A_Door_out2,
//            A.Create_By,
//            A.Create_Time,
//            A.Update_By,
//            A.Update_Time,
//            B.`NAME`,
//            B.SURNAME,
//            B.POS_WORK,
//            C.Dep_Code
//        FROM
//            A_transaction AS A
//        INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
//        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
//        WHERE
//            A.A_Datetime LIKE '" . $DateConrdi . "%'
//        AND C.Dep_Code = " . $DepCode);
//            if ($query) {
//                while ($data = mysqli_fetch_assoc($query)) {
//
//                    $myArray[] = $data;
//
//                }
//            } else {
//            }
//            $myJSON = json_encode($myArray);
//            echo $myJSON;
        } else {
            return false;
        }
    }

    //แก้ไขข้อมูลใหม่จากตารางApprove
    public function UPDATEDATAFORMAPPROVE($DepCode, $DateConrdi, $PERID, $OBJDATA)
    {
        $myArray = array();
        $i = 0;
        $numItems = count($OBJDATA);
        $status_in = '';
        $status_in2 = '';
        $status_ho = '';
        $status_hn = '';
        $status_out = '';
        $status_out2 = '';
        $arr = '';
        $date = $DateConrdi;
        list($year, $month, $day) = explode('-', $DateConrdi);

        if ($this->db->hostDB) {
            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    foreach ($OBJDATA as $key => $value) {
                        if (strpos($value->TIME_IN2, "สาย") > 0) {
                            $status_in2 = "สาย";
                        } else {
                            $status_in2 = "";
                        }
                        if (strpos($value->TIME_HO, "สาย") > 0) {
                            $status_ho = "สาย";
                        } else if (strpos($value->TIME_HO, "ออกก่อน") > 0) {
                            $status_ho = "ออกก่อน";
                        } else {
                            $status_ho = "";
                        }
                        if (strpos($value->TIME_HN, "สาย") > 0) {
                            $status_hn = "สาย";
                        } else {
                            $status_hn = "";
                        }
                        if (strpos($value->TIME_OUT2, "สาย") > 0) {
                            $status_out2 = "สาย";
                        } else if (strpos($value->TIME_OUT2, "ออกก่อน") > 0) {
                            $status_out2 = "ออกก่อน";
                        } else {
                            $status_out2 = "";
                        }

                        $queryCheckApproveDay = mysqli_query($this->db->hostDB2, "SELECT * FROM HR_CENTER.`working_summary_tb` where classify = '3' AND perid = '" . $value->PERID . "' AND monthly like '%" . $year . "-" . $month . "-%'");

                        if (mysqli_num_rows($queryCheckApproveDay) == 0) {
                            $query = mysqli_query($this->db->hostDB, "UPDATE A_transaction
                SET A_Time_in2 = '" . $value->TIME_IN2 . "',
                A_Door_in2 = '" . $value->DOOR_IN2 . "',
                A_Status_in2 = '" . $status_in2 . "',
                A_Time_ho = '" . $value->TIME_HO . "',
                A_Door_ho = '" . $value->DOOR_HO . "',
                A_Status_ho = '" . $status_ho . "',
                A_Time_hn = '" . $value->TIME_HN . "',
                A_Door_hn = '" . $value->DOOR_HN . "',
                A_Status_hn = '" . $status_hn . "',
                A_Time_out2 = '" . $value->TIME_OUT2 . "',
                A_Door_out2 = '" . $value->DOOR_OUT2 . "',
                A_Status_out2 = '" . $status_out2 . "',
                Update_By = '" . $PERID . "',
                Update_Time = CURRENT_TIMESTAMP
                WHERE A_Datetime = '" . $DateConrdi . "'
                AND PERID = '" . $value->PERID . "'");

                            if ($query) {
                                if (++$i === $numItems) {
                                    $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                                }
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก รหัสบุคลากร $value->PERID ที่เลือกได้ทำการยืนยันข้อมูลการปฏิบัติงานเรียบร้อยแล้ว");
                        }
                    }
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '001_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );

                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            foreach ($OBJDATA as $key => $value) {
                                if (strpos($value->TIME_IN2, "สาย") > 0) {
                                    $status_in2 = "สาย";
                                } else {
                                    $status_in2 = "";
                                }
                                if (strpos($value->TIME_HO, "สาย") > 0) {
                                    $status_ho = "สาย";
                                } else if (strpos($value->TIME_HO, "ออกก่อน") > 0) {
                                    $status_ho = "ออกก่อน";
                                } else {
                                    $status_ho = "";
                                }
                                if (strpos($value->TIME_HN, "สาย") > 0) {
                                    $status_hn = "สาย";
                                } else {
                                    $status_hn = "";
                                }
                                if (strpos($value->TIME_OUT2, "สาย") > 0) {
                                    $status_out2 = "สาย";
                                } else if (strpos($value->TIME_OUT2, "ออกก่อน") > 0) {
                                    $status_out2 = "ออกก่อน";
                                } else {
                                    $status_out2 = "";
                                }

                                $queryCheckApproveDay = mysqli_query($this->db->hostDB2, "SELECT * FROM HR_CENTER.`working_summary_tb` where classify = '3' AND perid = '" . $value->PERID . "' AND monthly like '%" . $year . "-" . $month . "-%'");

                                if (mysqli_num_rows($queryCheckApproveDay) == 0) {
                                    $query = mysqli_query($this->db->hostDB, "UPDATE A_transaction
                SET A_Time_in2 = '" . $value->TIME_IN2 . "',
                A_Door_in2 = '" . $value->DOOR_IN2 . "',
                A_Status_in2 = '" . $status_in2 . "',
                A_Time_ho = '" . $value->TIME_HO . "',
                A_Door_ho = '" . $value->DOOR_HO . "',
                A_Status_ho = '" . $status_ho . "',
                A_Time_hn = '" . $value->TIME_HN . "',
                A_Door_hn = '" . $value->DOOR_HN . "',
                A_Status_hn = '" . $status_hn . "',
                A_Time_out2 = '" . $value->TIME_OUT2 . "',
                A_Door_out2 = '" . $value->DOOR_OUT2 . "',
                A_Status_out2 = '" . $status_out2 . "',
                Update_By = '" . $PERID . "',
                Update_Time = CURRENT_TIMESTAMP
                WHERE A_Datetime = '" . $DateConrdi . "'
                AND PERID = '" . $value->PERID . "'");

                                    if ($query) {
                                        if (++$i === $numItems) {
                                            $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                                        }
                                    } else {
                                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                                    }
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก รหัสบุคลากร $value->PERID ที่เลือกได้ทำการยืนยันข้อมูลการปฏิบัติงานเรียบร้อยแล้ว");
                                }
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่ได้รับสิทธิในการแก้ไขข้อมูล");
                        }
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                    }
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    public function GETLEAVEPERSONDATA($DepCode, $DateConrdi)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.T_Leave_Type,
            A.T_Leave_Reason,
            A.T_Leave_Date_Start,
            A.T_Leave_Date_End,
            A.T_Day_Type_Start,
            A.T_Day_Type_End,
            A.T_Leave_CreateBY,
            A.T_Leave_UpdateBY,
            A.T_Leave_CreateT,
            A.T_Leave_UpdateT,
            A.PERID,
            A.Dep_code,
            B.Leave_Detail,
            C.Dep_Code
            FROM
            c_take_a_leave AS A
            INNER JOIN c_leave AS B ON A.T_Leave_Type = B.Leave_Type
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_code
            WHERE
            C.Dep_Code = " . $DepCode[$i]->Dep_Code . " AND
            A.T_Leave_Date_Start <= '" . $DateConrdi . "' AND
            A.T_Leave_Date_End >= '" . $DateConrdi . "'
            AND T_Status_Leave = '1'");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

//                        $myArray[] = $data;
                        array_push($myArray, $data);
                    }

                    $queryPersonDepOther = mysqli_query($this->db->hostDB, "SELECT
                            A.No_Id,
                            A.T_Leave_Type,
                            A.T_Leave_Reason,
                            A.T_Leave_Date_Start,
                            A.T_Leave_Date_End,
                            A.T_Day_Type_Start,
                            A.T_Day_Type_End,
                            A.T_Leave_CreateBY,
                            A.T_Leave_UpdateBY,
                            A.T_Leave_CreateT,
                            A.T_Leave_UpdateT,
                            A.PERID,
                            A.Dep_code,
                            B.Leave_Detail,
                            C.Dep_Code
                            FROM
                            c_medperson AS D
                            LEFT JOIN c_take_a_leave AS A ON D.PERID = A.PERID
                            INNER JOIN c_leave AS B ON A.T_Leave_Type = B.Leave_Type
                            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_code
                            WHERE
                            D.Dep_Code = " . $DepCode[$i]->Dep_Code . " AND
                            A.T_Leave_Date_Start <= '" . $DateConrdi . "' AND
                            A.T_Leave_Date_End >= '" . $DateConrdi . "'
                            AND T_Status_Leave = '1'");

                    if ($queryPersonDepOther) {
                        while ($dataDepOther = mysqli_fetch_assoc($queryPersonDepOther)) {


                            array_push($myArray, $dataDepOther);
                        }
                    }

                } else {
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // บันทึกข้อมูลการขาดงานของบุคลากรในหน่วยงาน
    public function SAVEABSENCEDATA($No_Id, $PERID, $DepCode, $DateConrdi, $PERID_person)
    {
        $myArray = array();
        $date = $DateConrdi;

        list($year, $month, $day) = explode('-', $DateConrdi);

        if ($this->db->hostDB) {

            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {

                    $queryCheckApproveDay = mysqli_query($this->db->hostDB2, "SELECT * FROM HR_CENTER.`working_summary_tb` where classify = '3' AND perid = '" . $PERID_person . "' AND monthly like '%" . $year . "-" . $month . "-%'");

                    if (mysqli_num_rows($queryCheckApproveDay) == 0) {
                        $query = mysqli_query($this->db->hostDB, "UPDATE A_transaction
                SET A_Status_in2 = 'ขาดงาน',
                A_Status_ho = 'ขาดงาน',
                A_Status_hn = 'ขาดงาน',
                A_Status_out2 = 'ขาดงาน',
                Update_By = '" . $PERID . "',
                Update_Time = CURRENT_TIMESTAMP
                WHERE No_Id = '" . $No_Id . "'");

                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "บันทึกการขาดงานสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                        }
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก รหัสบุคลากร $PERID_person ที่เลือกได้ทำการยืนยันข้อมูลการปฏิบัติงานเรียบร้อยแล้ว");
                    }

                    echo json_encode($arr);
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '001_B'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            $query = mysqli_query($this->db->hostDB, "UPDATE A_transaction
                SET A_Status_in2 = 'ขาดงาน',
                A_Status_ho = 'ขาดงาน',
                A_Status_hn = 'ขาดงาน',
                A_Status_out2 = 'ขาดงาน',
                Update_By = '" . $PERID . "',
                Update_Time = CURRENT_TIMESTAMP
                WHERE No_Id = '" . $No_Id . "'");

                            if ($query) {
                                $arr = array('Status' => true, 'Message' => "บันทึกการขาดงานสำเร็จ");
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                            }

                            echo json_encode($arr);
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                        }
                    } else {
                        echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                    }
                }
            }

//            $query = mysqli_query($this->db->hostDB, "UPDATE A_transaction
//                SET A_Status_in2 = 'ขาดงาน',
//                A_Status_ho = 'ขาดงาน',
//                A_Status_hn = 'ขาดงาน',
//                A_Status_out2 = 'ขาดงาน',
//                Update_By = '" . $PERID . "',
//                Update_Time = CURRENT_TIMESTAMP
//                WHERE No_Id = '" . $No_Id . "'");
//
//            if ($query) {
//                $arr = array('Status' => true, 'Message' => "บันทึกการขาดงานสำเร็จ");
//            } else {
//                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
//            }
//            echo json_encode($arr);
        }
    }

    // บันทึกหมายเหตุประจำวันของบุคลากรในหน่วยงาน
    public function SAVECOMMENTDEP($DepCode, $PerId, $DateConrdi, $Comment, $PerId_Create, $Time_I, $Time_O)
    {
        $myArray = array();
        $RangeTime = 0;

        $queryPersonOther = mysqli_query($this->db->hostDB, "SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.POS_LEVEL,
            A.Date_In,
            B.Dep_name,
            B.Dep_Code,
            C.PosName
            FROM
            HRTIME_DB.c_medperson AS D
            LEFT JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = D.Dep_Code
            LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
            WHERE
            D.PERID = " . $PerId . " AND
            A.CSTATUS != 0
            ORDER BY
            A.PERID ASC
            ");
        if (mysqli_num_rows($queryPersonOther) > 0) {
            while ($dataOtherDep = mysqli_fetch_assoc($queryPersonOther)) {
                if ($Time_I != '' && $Time_O == '') {
                    $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        '" . $Time_I . "',
                        null ,
                        '0',
                        '" . $PerId . "',
                        '" . $dataOtherDep['Dep_Code'] . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
                } else if ($Time_I == '' && $Time_O != '') {
                    $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        null ,
                        '" . $Time_O . "',
                        '0',
                        '" . $PerId . "',
                        '" . $dataOtherDep['Dep_Code'] . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
                } else if ($Time_I != '' && $Time_O != '') {
                    $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        '" . $Time_I . "',
                        '" . $Time_O . "',
                        '0',
                        '" . $PerId . "',
                        '" . $dataOtherDep['Dep_Code'] . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
                } else {
                    $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        null,
                        null,
                        null,
                        '" . $PerId . "',
                        '" . $dataOtherDep['Dep_Code'] . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
                }

                if ($this->db->hostDB) {

                    $queryCheckRow = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            c_comment
        WHERE
            c_comment.PERID = $PerId
            AND c_comment.Comment_Date LIKE '" . $DateConrdi . "%'");

                    if ($queryCheckRow->num_rows == 0) {
                        $query = mysqli_query($this->db->hostDB, $sqlQuery);
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");

                            $sqlQuery = trim(preg_replace('/\s\s+/', ' ', $sqlQuery));
                            mysqli_query($this->db->hostDB, 'INSERT INTO HRTIME_DB.`log_hrtime` (
                                `process`,
                                `perid`,
                                `type`,
                                `command`
                            )
                            VALUES
                                ("Date_Comment", "' . $_SESSION['PERID'] . '" , "insert", "' . $sqlQuery . '")
                            ');
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                        }
                        echo json_encode($arr);
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ เนื่อองจากมีข้อมูลบันทึกในระบบแล้ว");
                        echo json_encode($arr);
                    }
                } else {
                    return false;
                }
            }
        } else {
            if ($Time_I != '' && $Time_O == '') {
                $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        '" . $Time_I . "',
                        null ,
                        '0',
                        '" . $PerId . "',
                        '" . $DepCode . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
            } else if ($Time_I == '' && $Time_O != '') {
                $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        null ,
                        '" . $Time_O . "',
                        '0',
                        '" . $PerId . "',
                        '" . $DepCode . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
            } else if ($Time_I != '' && $Time_O != '') {
                $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        '" . $Time_I . "',
                        '" . $Time_O . "',
                        '0',
                        '" . $PerId . "',
                        '" . $DepCode . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
            } else {
                $sqlQuery = "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `Time_I`,
                    `Time_O`,
                    `Time_Status`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        null,
                        null,
                        null,
                        '" . $PerId . "',
                        '" . $DepCode . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ";
            }

            if ($this->db->hostDB) {

                $queryCheckRow = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            c_comment
        WHERE
            c_comment.PERID = $PerId
            AND c_comment.Comment_Date LIKE '" . $DateConrdi . "%'");

                if ($queryCheckRow->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, $sqlQuery);
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");

                        $sqlQuery = trim(preg_replace('/\s\s+/', ' ', $sqlQuery));
                        mysqli_query($this->db->hostDB, 'INSERT INTO HRTIME_DB.`log_hrtime` (
                                `process`,
                                `perid`,
                                `type`,
                                `command`
                            )
                            VALUES
                                ("Date_Comment", "' . $_SESSION['PERID'] . '" , "insert", "' . $sqlQuery . '")
                            ');
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                    }
                    echo json_encode($arr);
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ เนื่อองจากมีข้อมูลบันทึกในระบบแล้ว");
                    echo json_encode($arr);
                }
            } else {
                return false;
            }
        }


    }

    //เรียกข้อมูลบุคลากรภายในหน่วยงาน
    public function GETPERSONFROMDEPART($DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.POS_LEVEL,
            A.Date_In,
            B.Dep_name,
            B.Dep_Code,
            C.PosName
            FROM
            STAFF.Medperson AS A
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
            LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
            WHERE
            B.Dep_Code = " . $DepCode[$i]->Dep_Code . " AND
            A.CSTATUS != 0
            ORDER BY
            A.PERID ASC
            ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        array_push($myArray, $data);

                    }

                    $queryPersonOther = mysqli_query($this->db->hostDB, "SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.POS_LEVEL,
            A.Date_In,
            B.Dep_name,
            B.Dep_Code,
            C.PosName
            FROM
            HRTIME_DB.c_medperson AS D
            LEFT JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
            LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
            WHERE
            D.Dep_Code = " . $DepCode[$i]->Dep_Code . " AND
            A.CSTATUS != 0
            ORDER BY
            A.PERID ASC
            ");
                    if ($queryPersonOther) {
                        while ($data = mysqli_fetch_assoc($queryPersonOther)) {
                            array_push($myArray, $data);
                        }
                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกดูหมายเหตุประจำวันของบุคลากรในหน่วยงาน
    public function GETCOMMENTDEP($DepCode, $DateConrdi)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.`NAME`,
            B.SURNAME,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Dep_Code
        FROM
            HRTIME_DB.c_comment AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
            WHERE
            A.Comment_Date = '" . $DateConrdi . "'
            AND
            STAFF.Depart.Dep_Code = " . $DepCode[$i]->Dep_Code . "
            ORDER BY A.Comment_ID DESC");
                if (mysqli_num_rows($query) > 0) {
                    while ($data = mysqli_fetch_assoc($query)) {
//                        $myArray[] = $data;
                        array_push($myArray, $data);
                    }

                    $queryPersonOther = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.`NAME`,
            B.SURNAME,
            C.Dep_name,
            C.Dep_Group_name,
            C.Dep_Code
        FROM
            HRTIME_DB.c_medperson AS D
            INNER JOIN HRTIME_DB.c_comment AS A ON A.PERID = D.PERID
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
            WHERE
            A.Comment_Date = '" . $DateConrdi . "'
            AND
            D.Dep_Code = " . $DepCode[$i]->Dep_Code . "
            ORDER BY A.Comment_ID DESC");
                    if ($queryPersonOther) {
                        while ($data = mysqli_fetch_assoc($queryPersonOther)) {
                            array_push($myArray, $data);
                        }
                    }
                }
            }
            echo json_encode($myArray);
        } else {
            return false;
        }
    }

    // เรียกดูหมายเหตุประจำวันของบุคลากรในหน่วยงาน
    public function GETALLCOMMENTDEP($DepCode, $DateConrdi)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.`NAME`,
            B.SURNAME,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Dep_Code
        FROM
            HRTIME_DB.c_comment AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
            WHERE
            STAFF.Depart.Dep_Code = " . $DepCode[$i]->Dep_Code . "
            ORDER BY A.Comment_ID DESC");
                if (mysqli_num_rows($query) > 0) {
                    while ($data = mysqli_fetch_assoc($query)) {
//                        $myArray[] = $data;
                        array_push($myArray, $data);
                    }

                    $queryPersonOther = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.`NAME`,
            B.SURNAME,
            C.Dep_name,
            C.Dep_Group_name,
            C.Dep_Code
        FROM
            HRTIME_DB.c_medperson AS D
            INNER JOIN HRTIME_DB.c_comment AS A ON A.PERID = D.PERID
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
            WHERE
            D.Dep_Code = " . $DepCode[$i]->Dep_Code . "
            ORDER BY A.Comment_ID DESC");
                    if ($queryPersonOther) {
                        while ($data = mysqli_fetch_assoc($queryPersonOther)) {
                            array_push($myArray, $data);
                        }
                    }
                }
            }
            echo json_encode($myArray);
        } else {
            return false;
        }
    }

    public function APPROVECOMMENTDATA($CommentData, $Condi)
    {

        $ResultData = array();
        $sqlQuery = '';
        if ($CommentData->Time_I != null && $CommentData->Time_O == null) {
            if ($CommentData->Late) {
                $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                SET A_Time_in2 = '" . $CommentData->Time_I . "',
                A_Status_in2 = 'สาย'
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'";
            } else {
                $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                SET A_Time_in2 = '" . $CommentData->Time_I . "',
                A_Status_in2 = ''
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'";
            }
        } else if ($CommentData->Time_I == null && $CommentData->Time_O != null) {
            if ($CommentData->Before) {
                $sqlQuery = "
                UPDATE HRTIME_DB.A_transaction
                SET A_Time_out2 = '" . $CommentData->Time_O . "',
                A_Status_out2 = 'ออกก่อน'
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'
                ";
            } else {
                $sqlQuery = "
                UPDATE HRTIME_DB.A_transaction
                SET A_Time_out2 = '" . $CommentData->Time_O . "',
                 A_Status_out2 = ''
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'
                ";
            }
        } else if ($CommentData->Time_I != null && $CommentData->Time_O != null) {
            if ($CommentData->Late && !$CommentData->Before) {
                $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                SET A_Time_in2 = '" . $CommentData->Time_I . "',
                A_Time_out2 = '" . $CommentData->Time_O . "',
                A_Status_in2 = 'สาย',
                 A_Status_out2 = ''
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'";
            } else if (!$CommentData->Late && $CommentData->Before) {
                $sqlQuery = "
                UPDATE HRTIME_DB.A_transaction
               SET A_Time_in2 = '" . $CommentData->Time_I . "',
                A_Time_out2 = '" . $CommentData->Time_O . "',
                A_Status_out2 = 'ออกก่อน',
                A_Status_in2 = '',
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'
                ";
            } else if ($CommentData->Late && $CommentData->Before) {
                $sqlQuery = "
                UPDATE HRTIME_DB.A_transaction
               SET A_Time_in2 = '" . $CommentData->Time_I . "',
                A_Time_out2 = '" . $CommentData->Time_O . "',
                A_Status_out2 = 'ออกก่อน',
                A_Status_in2 = 'สาย'
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'
                ";
            } else {
                $sqlQuery = "
                UPDATE HRTIME_DB.A_transaction
                SET A_Time_in2 = '" . $CommentData->Time_I . "',
                A_Time_out2 = '" . $CommentData->Time_O . "',
                A_Status_out2 = '',
                A_Status_in2 = ''
                WHERE PERID = $CommentData->PERID
                AND A_Datetime = '" . $CommentData->Comment_Date . "'
                ";
            }
        }

        if ($this->db->hostDB) {

            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $CommentData->Dep_Code
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {


                    if ($Condi) {
                        $sqlCheckApprove = mysqli_query($this->db->hostDB, "SELECT
                            A.*
                            FROM
                            A_transaction AS A
                            WHERE
                            A.Dep_Code = $CommentData->Dep_Code AND
                            A.A_Datetime = '$CommentData->Comment_Date'");

                        if (mysqli_num_rows($sqlCheckApprove) >= 1) {
                            $queryTime = mysqli_query($this->db->hostDB, $sqlQuery);

                            if ($queryTime) {
                                $queryUpdate = mysqli_query($this->db->hostDB, "
                    UPDATE HRTIME_DB.c_comment
                    SET Time_Status = 1
                    WHERE Comment_ID = $CommentData->Comment_ID
                    ");
                                if ($queryUpdate) {
                                    $ResultData = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                                } else {
                                    $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                                }
                            } else {
                                $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                            }
                        } else {
                            $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ ข้อมูลวันเวลาดังกล่าว ยังไม่ได้รับการยืนยันข้อมูล");
                        }
                    } else {
                        $queryUpdate = mysqli_query($this->db->hostDB, "
                    UPDATE HRTIME_DB.c_comment
                    SET Time_Status = 2
                    WHERE Comment_ID = $CommentData->Comment_ID
                    ");
                        if ($queryUpdate) {
                            $ResultData = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                        } else {
                            $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                        }
                    }
                    echo json_encode($ResultData);
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $CommentData->Dep_Code
                    AND
                    D.Opt_C_Code = '001_C'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {

                            if ($Condi) {

                                $sqlCheckApprove = mysqli_query($this->db->hostDB, "SELECT
                            A.*
                            FROM
                            A_transaction AS A
                            WHERE
                            A.Dep_Code = $CommentData->Dep_Code AND
                            A.A_Datetime = '$CommentData->Comment_Date'");

                                if (mysqli_num_rows($sqlCheckApprove) >= 1) {
                                    $queryTime = mysqli_query($this->db->hostDB, $sqlQuery);

                                    if ($queryTime) {
                                        $queryUpdate = mysqli_query($this->db->hostDB, "
                    UPDATE HRTIME_DB.c_comment
                    SET Time_Status = 1
                    WHERE Comment_ID = $CommentData->Comment_ID
                    ");
                                        if ($queryUpdate) {
                                            $ResultData = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                                        } else {
                                            $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                                        }
                                    } else {
                                        $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                                    }
                                } else {
                                    $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ ข้อมูลวันเวลาดังกล่าว ยังไม่ได้รับการยืนยันข้อมูล");
                                }

                            } else {
                                $queryUpdate = mysqli_query($this->db->hostDB, "
                    UPDATE HRTIME_DB.c_comment
                    SET Time_Status = 2
                    WHERE Comment_ID = $CommentData->Comment_ID
                    ");
                                if ($queryUpdate) {
                                    $ResultData = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                                } else {
                                    $ResultData = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                                }
                            }
                            echo json_encode($ResultData);
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่ได้รับสิทธิในการจัดการข้อมูล"));
                        }
                    }
                }
            }
        }
    }

    public function GETCOMMENTREPORTDEP($Dep_Code, $Date_Cordi, $Date_Cordi2)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.Comment_ID,
            A.Comment_Details,
            A.Comment_Date,
            A.PERID,
            A.Dep_Code,
            A.Create_BY,
            A.Create_T,
            B.`NAME`,
            B.SURNAME,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Dep_Code
        FROM
            HRTIME_DB.c_comment AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
            WHERE
            STAFF.Depart.Dep_Code = $Dep_Code
            AND A.Comment_Date
            BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETREFPESONAL($DepCode)
    {
        $RefPersonal = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $query = mysqli_query($this->db->hostDB, "
            SELECT
            A.*,
            C.Dep_name AS Dep_name_New,
            B.Dep_name AS Dep_name_Old
            FROM
            HRTIME_DB.c_medperson AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_Code_Old
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
            WHERE
            A.Dep_Code_Old = " . $DepCode[$i]->Dep_Code);
                if (mysqli_num_rows($query) > 0) {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $RefPersonal[] = $data;
                    }
                } else {
                    $query2 = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.*,
                        C.Dep_name AS Dep_name_New,
                        B.Dep_name AS Dep_name_Old
                        FROM
                        HRTIME_DB.c_medperson AS A
                        INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_Code_Old
                        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
                        WHERE
                        A.Dep_Code = " . $DepCode[$i]->Dep_Code);
                    while ($data = mysqli_fetch_assoc($query2)) {
                        $RefPersonal[] = $data;
                    }
                }
            }
            echo json_encode($RefPersonal);
        } else {
            return false;
        }
    }

    public function D_GETEDITTIME($DepCode, $date)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($DepCode); $i++) {
                $querySel = mysqli_query($this->db->hostDB, "
           SELECT
                A.id AS id,
                A.PERID AS PERID,
                A.date AS date,
                A.time_in AS time_in,
                A.time_out AS time_out,
                A.`status` AS `status`,
                A.Create_By AS Create_By,
                A.Create_Time AS Create_Time,
                A.Update_By AS Update_By,
                A.Update_Time AS Update_Time,
                B.`NAME` AS `NAME`,
                B.SURNAME AS SURNAME,
                C.Dep_Code AS Dep_Code,
                C.Edit_code AS Edit_code,
                C.Dep_name AS Dep_name,
                C.Dep_Group_name AS Dep_Group_name,
            IF
            ( ISNULL( D.Dep_Code ), C.Dep_Code, D.Dep_Code ) AS Dep_Code_Current,
                    D.Dep_Code_Old 
                FROM
                    (
                        ( HRTIME_DB.c_edit_time AS A JOIN STAFF.Medperson AS B ON ( ( A.PERID = B.PERID ) ) )
                        JOIN STAFF.Depart AS C ON ( ( B.DEP_WORK = C.Dep_Code ) ) 
                    )
                    LEFT JOIN HRTIME_DB.c_medperson AS D ON A.PERID = D.PERID 
            ");

                if ($querySel) {
                    if ($querySel) {
                        while ($data = mysqli_fetch_assoc($querySel)) {
                            array_push($myArray, $data);
                        }
                    }
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            echo false;
        }
    }

    public function CONSIDEREDITTIME($EditTimeData, $Dep_Code, $PERID)
    {
        $myArray = array();
        $Late = '';
        $Before = '';
        if ($this->db->hostDB) {
            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $Dep_Code
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {

                    for ($i = 0; $i < count($EditTimeData); $i++) {
                        $queryUpdate = mysqli_query($this->db->hostDB, "
                        UPDATE c_edit_time 
                        SET Status = " . $EditTimeData[$i]->status2 . ",
                         Update_By = '" . $PERID . "'
                         WHERE
                         id = '" . $EditTimeData[$i]->id . "'
                    ");
                        $Late = '';
                        $Before = '';

                        list($datetimeIn, $timeIn) = explode(" ", $EditTimeData[$i]->time_in);
                        list($datetimeOut, $timeOut) = explode(" ", $EditTimeData[$i]->time_out);

                        if($EditTimeData[$i]->Late) {
                            $Late = 'สาย';
                        }

                        if($EditTimeData[$i]->Before) {
                            $Before = 'ออกก่อน';
                        }


                        if ($timeIn != null && $timeOut == null) {
                            $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                                SET A_Time_in2 = '" . $timeIn . "',
                                A_Status_in2 = '" . $Late . "'
                                WHERE PERID = " . $EditTimeData[$i]->PERID . "
                                AND A_Datetime = '" . $EditTimeData[$i]->date . "'";
                        } else if (timeIn == null && $timeOut!== null) {
                            $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                                SET A_Time_out2 = '" . $timeOut . "',
                                A_Status_out2 = '" . $Before . "'
                                WHERE PERID = " . $EditTimeData[$i]->PERID . "
                                AND A_Datetime = '" . $EditTimeData[$i]->date . "'";
                        } else {
                            $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                                SET A_Time_in2 = '" . $timeIn . "',
                                A_Status_in2 = '" . $Late . "',
                                A_Time_out2 = '" . $timeOut . "',
                                A_Status_out2 = '" . $Before . "'
                                WHERE PERID = " . $EditTimeData[$i]->PERID . "
                                AND A_Datetime = '" . $EditTimeData[$i]->date . "'";
                        }

                        echo $sqlQuery;

                        $sqlCheckApprove = mysqli_query($this->db->hostDB, "SELECT
                            A.*
                            FROM
                            A_transaction AS A
                            WHERE
                            A.Dep_Code = " . $EditTimeData[$i]->Dep_Code . " AND
                            A.A_Datetime = '" . $EditTimeData[$i]->date . "'");

                        if (mysqli_num_rows($sqlCheckApprove) >= 1) {
                            $queryTime = mysqli_query($this->db->hostDB, $sqlQuery);
                        }
                    }
                    echo json_encode(array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ"));
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $Dep_Code
                    AND
                    D.Opt_C_Code = '001_C'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {

                            for ($i = 0; $i < count($EditTimeData); $i++) {
                                $queryUpdate = mysqli_query($this->db->hostDB, "
                        UPDATE c_edit_time 
                        SET Status = " . $EditTimeData[$i]->status2 . ",
                         Update_By = '" . $PERID . "'
                         WHERE
                         id = '" . $EditTimeData[$i]->id . "'
                    ");
                                $Late = '';
                                $Before = '';

                                list($datetimeIn, $timeIn) = explode(" ", $EditTimeData[$i]->time_in);
                                list($datetimeOut, $timeOut) = explode(" ", $EditTimeData[$i]->time_out);

                                if($EditTimeData[$i]->Late) {
                                    $Late = 'สาย';
                                }

                                if($EditTimeData[$i]->Before) {
                                    $Before = 'ออกก่อน';
                                }


                                if ($timeIn != null && $timeOut == null) {
                                    $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                                SET A_Time_in2 = '" . $timeIn . "',
                                A_Status_in2 = '" . $Late . "'
                                WHERE PERID = " . $EditTimeData[$i]->PERID . "
                                AND A_Datetime = '" . $EditTimeData[$i]->date . "'";
                                } else if (timeIn == null && $timeOut!== null) {
                                    $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                                SET A_Time_out2 = '" . $timeOut . "',
                                A_Status_out2 = '" . $Before . "'
                                WHERE PERID = " . $EditTimeData[$i]->PERID . "
                                AND A_Datetime = '" . $EditTimeData[$i]->date . "'";
                                } else {
                                    $sqlQuery = "UPDATE HRTIME_DB.A_transaction
                                SET A_Time_in2 = '" . $timeIn . "',
                                A_Status_in2 = '" . $Late . "',
                                A_Time_out2 = '" . $timeOut . "',
                                A_Status_out2 = '" . $Before . "'
                                 WHERE PERID = " . $EditTimeData[$i]->PERID . "
                                AND A_Datetime = '" . $EditTimeData[$i]->date . "'";
                                }

                                $sqlCheckApprove = mysqli_query($this->db->hostDB, "SELECT
                            A.*
                            FROM
                            A_transaction AS A
                            WHERE
                            A.Dep_Code = '" . $EditTimeData[$i]->Dep_Code . "' AND
                            A.A_Datetime = '" . $EditTimeData[$i]->date . "'");

                                if (mysqli_num_rows($sqlCheckApprove) >= 1) {
                                    $queryTime = mysqli_query($this->db->hostDB, $sqlQuery);
                                }
                            }
                            echo json_encode(array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ"));
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่ได้รับสิทธิในการจัดการข้อมูล"));
                        }
                    }
                }
            }

        } else {
            echo false;
        }
    }
}
