<?php

$ManageLogin = new ManageLoginModel();

class ManageLoginModel extends Model
{
    public $ObjData = "";
    public $Status;
    public $CheckMenu;
    public $CheckUser;
    public $CheckStatus;
    public $perid_session;
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    public function CheckLoginModel()
    {
        $PermissData = array();

        if (!isset($_SESSION['PERID'])) {
            // header("location: http://localhost/MedMisProject/MVCworkTime2/ManageTime");
            echo ("<script>location.href = './Login';</script>");
            exit(0);
        } else {
            if ($this->db->hostDB) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                STAFF.Medperson.`NAME`,
                STAFF.Medperson.SURNAME,
                STAFF.Medperson.PERID,
                STAFF.Medperson.POS_LEVEL,
                STAFF.Depart.Dep_Code,
                STAFF.Depart.Edit_code,
                STAFF.Depart.Dep_name
            FROM
                STAFF.Medperson
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
            INNER JOIN HRTIME_DB.c_admin_permiss ON HRTIME_DB.c_admin_permiss.PERID = STAFF.Medperson.PERID
            WHERE
                STAFF.Medperson.PERID = " . $_SESSION['PERID']);
                $objResult = mysqli_fetch_array($query);
                if ($query) {
                    if (mysqli_num_rows($query) >= 1) {
                        $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                                            A.*,
                                            C.Permiss_ID,
                                            C.Permiss_Details,
                                            D.Opt_C_Code,
                                            D.Opt_C_Details
                                            FROM
                                            c_admin_permiss AS A
                                            LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                                            LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                                            LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                                            WHERE
                                            A.PERID = " . $_SESSION['PERID']);
                        if($queryPermiss){
                            while ($data = mysqli_fetch_assoc($queryPermiss)) {

                                $PermissData[] = $data;

                            }
                            $_SESSION['Permission'] = json_encode($PermissData);
                        }
                        return array("Status" => "Administrator", "DataPerson" => $objResult);
                    } else {
                        if ($this->db->hostDB) {
                            $query = mysqli_query($this->db->hostDB, "SELECT
                            STAFF.Medperson.`NAME`,
                            STAFF.Medperson.SURNAME,
                            STAFF.Medperson.PERID,
                            STAFF.Medperson.POS_LEVEL,
                            STAFF.Depart.Dep_Code,
                            STAFF.Depart.Edit_code,
                            STAFF.Depart.Dep_name
                        FROM
                            STAFF.Medperson
                        LEFT JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
                        INNER JOIN HRTIME_DB.c_user_permiss ON HRTIME_DB.c_user_permiss.PERID = STAFF.Medperson.PERID
                        WHERE
                            STAFF.Medperson.PERID = " . $_SESSION['PERID']);
                            $objResult = mysqli_fetch_array($query);
                            if ($query) {
                                if (mysqli_num_rows($query) >= 1) {
                                    if ($this->db->hostDB) {
                                        $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                                            A.*,
                                            C.Permiss_Details,
                                            D.Opt_C_Details
                                            FROM
                                            c_user_permiss AS A
                                            LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                                            LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                                            LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                                            WHERE
                                            A.PERID = " . $_SESSION['PERID']);
                                        if($queryPermiss){
                                            while ($data = mysqli_fetch_assoc($query)) {

                                                $PermissData[] = $data;

                                            }
                                            $_SESSION['Permission'] = json_encode($PermissData);
                                        }
                                        return array("Status" => "Supervisor", "DataPerson" => $objResult);
                                    }
                                } else {
                                    if ($this->db->hostDB) {
                                        $query = mysqli_query($this->db->hostDB, "SELECT
                                        STAFF.Medperson.`NAME`,
                                        STAFF.Medperson.SURNAME,
                                        STAFF.Medperson.PERID,
                                        STAFF.Medperson.POS_LEVEL,
                                        STAFF.Depart.Dep_Code,
                                        STAFF.Depart.Edit_code,
                                        STAFF.Depart.Dep_name
                                        FROM
                                        STAFF.Medperson
                                        LEFT JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
                                        WHERE
                                            STAFF.Medperson.PERID = " . $_SESSION['PERID']);
                                        $objResult = mysqli_fetch_array($query);
                                        if ($query) {
                                            if (mysqli_num_rows($query) >= 1) {
                                                return array("Status" => "User", "DataPerson" => $objResult);
                                            } else {
                                                echo ("<script>location.href = './Login';</script>");
                                            }
                                        } else {

                                        }
                                    } else {

                                    }
                                }
                            } else {
                                echo ("<script>location.href = './Login';</script>");
                            }
                        } else {

                        }
                    }
                } else {
                    echo ("<script>location.href = './Login';</script>");
                }
                // return $query ;
            } else {

            }
            session_write_close();
            $this->db->CloseConnection();
        }
    }

    public function CheckStatusUser()
    {
        if ($this->CheckUser) {
            if ($this->Status != "ผู้ดูแลระบบ") {
                if ($_GET['url'] == 'Report') {
                    echo ("<script>location.href = './Date';</script>");
                } else if ($_GET['url'] == 'ReportOfDay') {
                    echo ("<script>location.href = './Date';</script>");
                }
            }
        } else {
            if ($this->Status != "หัวหน้าหน่วยงาน") {
                if ($_GET['url'] == 'Month') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'More') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'ManageTime') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'TakeALeave') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'NoteOfLeave') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'ConfigPrint') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'LeaveRegis') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'RegisMail') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'Date') {
                    echo ("<script>location.href = './WorkTime';</script>");
                } else if ($_GET['url'] == 'Report') {
                    echo ("<script>location.href = './Date';</script>");
                } else if ($_GET['url'] == 'ReportOfDay') {
                    echo ("<script>location.href = './Date';</script>");
                }
                 else {

                }
            } else {
                if ($_GET['url'] == 'Report') {
                    echo ("<script>location.href = './Date';</script>");
                } else if ($_GET['url'] == 'ReportOfDay') {
                    echo ("<script>location.href = './Date';</script>");
                }
            }
            // echo ("<script>location.href = './Date';</script>");
        }
    }

    // เรียกข้อมูลใบลาเพื่อนับการ count แสดงจำนวนตัวเลขบนหน้า slider bar
    public function GetCountNoteOfLeave()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            `c_take_a_leave`
        WHERE
            c_take_a_leave.T_Status_Leave = 0
            ");
            if ($query) {
                $countData = mysqli_num_rows($query);
            } else {

            }
            return $countData;
        } else {
            return false;
        }

    }

    public function TestServiceModel()
    {
        setInterval(function () {
            echo "hi!\n";
        }, 1000);
    }
}
