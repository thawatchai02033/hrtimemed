<?php
class RegisMailModel extends Model
{

    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function SAVEDATAREGIS($Token, $PerID, $DepCode)
    {
        $myArray = array();
        date_default_timezone_set("Asia/Bangkok");
        $dateNow = date("Y-m-d H:i:s");
        if ($this->db->hostDB) {

            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " .$_SESSION['PERID']
            );
            if($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HR_CENTER.line_token
                WHERE
                HR_CENTER.line_token.perid = $PerID            
            ");
                    if ($query->num_rows == 0) {
                        $query = mysqli_query($this->db->hostDB, "INSERT INTO HR_CENTER.line_token(perid, line_token, process)
                VALUES ($PerID, '" . $Token . "','HR-Time')");
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => 'บันทึกข้อมูลใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้โปรดลองอีกครั้ง");
                        }
                        echo json_encode($arr);
                    } else {
                        $query = mysqli_query($this->db->hostDB, "UPDATE HR_CENTER.line_token
                    SET line_token = '" . $Token . "'
                    WHERE perid = $PerID");
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => 'แก้ไขข้อมูลใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่แก้ไขข้อมูลได้");
                        }
                        echo json_encode($arr);
                    }
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '009_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                HR_CENTER.line_token
                WHERE
                HR_CENTER.line_token.perid = $PerID            
            ");
                            if (mysqli_num_rows($query) <= 0) {
                                $query = mysqli_query($this->db->hostDB, "INSERT INTO HR_CENTER.line_token(perid, line_token, process)
                VALUES ($PerID, '" . $Token . "','HR-Time')");
                                if ($query) {
                                    $arr = array('Status' => true, 'Message' => 'บันทึกข้อมูลใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้โปรดลองอีกครั้ง");
                                }
                                echo json_encode($arr);
                            } else {
                                $query = mysqli_query($this->db->hostDB, "UPDATE HR_CENTER.line_token
                    SET line_token = '" . $Token . "'
                    WHERE perid = $PerID");
                                if ($query) {
                                    $arr = array('Status' => true, 'Message' => 'แก้ไขข้อมูลใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่แก้ไขข้อมูลได้");
                                }
                                echo json_encode($arr);
                            }
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                        }
                    }
                }
            }
        } else {
            return false;
        }
    }
}
