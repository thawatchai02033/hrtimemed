<?php

class WorkTimeModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function DateAllData()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, constant('Date_All_Data'));
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GatAllDepartment()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            C_admin.PERID
        FROM
            C_admin
        WHERE
            C_admin.PERID = $PERID");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Edit_code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                STAFF.D_admin AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Edit_code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, constant('Date_Get_Department'));
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function CheckComment($PerId, $DateConrdi)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $queryCheckRow = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            c_comment
        WHERE
            c_comment.PERID = $PerId
            AND c_comment.Comment_Date LIKE '" . $DateConrdi . "%'");

            if (mysqli_num_rows($queryCheckRow) > 0) {
                $arr = array('Status' => true);
                echo json_encode($arr);
            } else {
                $arr = array('Status' => false);
                echo json_encode($arr);
            }
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                D_holiday
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0 และ Dep_Code = Depcode
    public function GetDataFromDep($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
                SELECT
                    A.PERID,
                    A.door_id,
                    A.card_no,
                    A.event_time,
                    B.`NAME`,
                    B.SURNAME,
                    B.POS_WORK,
                    C.Dep_Code,
                    C.Dep_name,
                    C.Dep_Group_name
                FROM
                    HRTIME_DB.D_transaction AS A
                LEFT JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                LEFT JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                LEFT JOIN HRTIME_DB.C_door AS D ON D.door_id = A.door_id
                LEFT JOIN HRTIME_DB.C_card AS E ON E.CARD_NO = A.card_no
                WHERE
                    A.PERID = $PERID
                AND A.event_time BETWEEN '" . $DateConrdi . " 00:00:00'
                AND '" . $DateConrdi . " 23:59:59'
                AND C.Dep_Code = " . $DepCode . "
                AND B.CSTATUS != 0
                AND D.zone_id != 5
                AND D.zone_id != 9
                AND E.dept_id NOT IN (SELECT id FROM ignore_deppartment)
                ORDER BY C.Dep_Group_name,A.event_time ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // ฟังก์เรียกข้อมูลเวลาเพื่อไปแสดงในรูปแบบของ timeline โดยใช้เงื่อนไขวันที่
    public function GetTimeLineData($DepCode, $DateStart, $DateEnd, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
                SELECT
                    A.PERID,
                    A.door_id,
                    A.card_no,
                    A.event_time,
                    B.`NAME`,
                    B.SURNAME,
                    C.Dep_Code,
                    C.Dep_name,
                    C.Dep_Group_name
                FROM
                    HRTIME_DB.D_transaction AS A
                LEFT JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                LEFT JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                LEFT JOIN HRTIME_DB.C_door AS D ON D.door_id = A.door_id
                LEFT JOIN HRTIME_DB.C_card AS E ON E.CARD_NO = A.card_no
                WHERE
                    A.PERID = $PERID
                AND A.event_time >= '" . $DateStart . " 00:00:00'
                AND A.event_time <= '" . $DateEnd . " 23:59:59'
                AND B.CSTATUS != 0
                AND D.zone_id != 5
                AND D.zone_id != 9
                AND E.dept_id NOT IN (SELECT id FROM ignore_deppartment)
                ORDER BY C.Dep_Group_name,A.event_time ASC") or die(mysqli_error($this->db->hostDB));
            if (mysqli_num_rows($query) > 0) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {

            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลรายบุคคลจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID = key และ Dep_Code = Depcode event_time = Dataconrdi
    public function GetDataFromDep_Condi($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT HRTIME_DB.D_transaction.PERID,HRTIME_DB.D_transaction.door_id, HRTIME_DB.D_transaction.event_time, STAFF.Medperson.NAME, STAFF.Medperson.SURNAME, STAFF.Depart.Dep_Code, STAFF.Depart.Dep_name, STAFF.Depart.Dep_Group_name
            FROM HRTIME_DB.D_transaction
            JOIN STAFF.Medperson
            ON HRTIME_DB.D_transaction.PERID = STAFF.Medperson.PERID
            JOIN STAFF.Depart
            ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
            WHERE HRTIME_DB.D_transaction.PERID = " . $PERID . "
            AND STAFF.Depart.Dep_Code = " . $DepCode . "
            AND HRTIME_DB.D_transaction.event_time LIKE '" . $DateConrdi . "%'
            ORDER BY  STAFF.Depart.Dep_Group_name,HRTIME_DB.D_transaction.event_time ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรของหน่วยงานแต่ล่ะหน่วยงาน โดยใช้เงื่อนไขวันที่และฝ่าย
//    public function GETPERSONDATE($DepCode, $DateConrdi, $PERID)
//    {
//        $myArray = array();
//        $dateNow = date("Y-m-d");
//
//        if ($this->db->hostDB) {
//            $query = mysqli_query($this->db->hostDB, "SELECT
//            S.NAME,
//            S.SURNAME,
//            S.PERID,
//            S.DEP_WORK,
//            D.Dep_name,
//            D.Dep_Code,
//            P_Time_DateTime,
//            P_Time_TimeN,
//            P_Time_TimeO,
//            P_Time_TimeHN,
//            P_Time_TimeHO,
//            P_DoorID,
//            P_Time_UpdateBY,
//            P_Time_UpdateT,
//                    door_name
//        FROM
//            STAFF.Medperson AS S
//        LEFT JOIN (
//            SELECT
//        B.PERID,
//        B.P_Time_DateTime,
//        B.P_Time_TimeN,
//        B.P_Time_TimeO,
//        B.P_Time_TimeHN,
//        B.P_Time_TimeHO,
//        B.P_DoorID,
//        B.P_Time_UpdateBY,
//        B.P_Time_UpdateT,
//        C_door.door_name
//        FROM
//            HRTIME_DB.c_person_time AS A
//        LEFT JOIN HRTIME_DB.c_person_time AS B ON B.P_Time_ID = (
//            SELECT
//                MAX(P_Time_ID)
//            FROM
//                HRTIME_DB.c_person_time
//            WHERE
//                P_Time_DateTime <= '" . $DateConrdi . "'
//            AND PERID = A.PERID
//        )
//
//        LEFT JOIN C_door ON C_door.door_id = B.P_DoorID
//        GROUP BY
//            B.PERID
//        ) AS H ON S.PERID = H.PERID
//        LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
//        WHERE
//            S.PERID = " . $PERID . "
//        AND S.CSTATUS != 0
//        ORDER BY
//            S.PERID ASC");
//            if ($query) {
//                while ($data = mysqli_fetch_assoc($query)) {
//
//                    $myArray[] = $data;
//
//                }
//            } else {
//            }
//            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
//            $myJSON = json_encode($myArray);
//            echo $myJSON;
//        } else {
//            return false;
//        }
//    }

    public function GETPERSONDATE($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        $DoorDataArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            S.NAME,
            S.SURNAME,
            S.PERID,
            S.DEP_WORK,
            D.Dep_name,
            D.Dep_Code,
            P_Time_DateTime,
            P_Time_TimeN,
            P_Time_TimeO,
            P_Time_TimeHN,
            P_Time_TimeHO,
            P_DoorID,
            P_Time_UpdateBY,
            P_Time_UpdateT,
                    door_name
        FROM
            STAFF.Medperson AS S
        LEFT JOIN (
            SELECT
        B.PERID,
        B.P_Time_DateTime,
        B.P_Time_TimeN,
        B.P_Time_TimeO,
        B.P_Time_TimeHN,
        B.P_Time_TimeHO,
        B.P_DoorID,
        B.P_Time_UpdateBY,
        B.P_Time_UpdateT,
        C_door.door_name
        FROM
            HRTIME_DB.c_person_time AS A
        LEFT JOIN HRTIME_DB.c_person_time AS B ON B.P_Time_ID = (
            SELECT
                MAX(P_Time_ID)
            FROM
                HRTIME_DB.c_person_time
            WHERE
                P_Time_DateTime <= '" . $DateConrdi . "'
            AND PERID = A.PERID
        )

        LEFT JOIN C_door ON C_door.door_id = B.P_DoorID
        GROUP BY
            B.PERID
        ) AS H ON S.PERID = H.PERID
        LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
        WHERE
            S.PERID = " . $PERID . "
        AND S.CSTATUS != 0
        ORDER BY
            S.PERID ASC");
            if ($query) {

                while ($data = mysqli_fetch_assoc($query)) {
                    $DoorDataArray = array();

                    if ($data['P_Time_DateTime'] != null || $data['P_Time_DateTime'] != '') {
                        $queryAllDATA = mysqli_query($this->db->hostDB, "SELECT
                                A.P_Time_ID,
                                A.PERID,
                                A.Dep_code,
                                A.P_Time_DateTime,
                                A.P_Time_TimeN,
                                A.P_Time_TimeO,
                                A.P_Time_TimeHN,
                                A.P_Time_TimeHO,
                                A.P_Time_CreateBY,
                                A.P_Time_UpdateBY,
                                A.P_Time_CreateT,
                                A.P_Time_UpdateT,
                                B.door_name,
                                B.zone_id,
                                B.door_id
                                FROM
                                c_person_time AS A
                                LEFT JOIN C_door AS B ON B.door_id = A.P_DoorID
                                WHERE A.PERID = " . $data['PERID'] . "
                                AND 
                                A.P_Time_DateTime = '" . $data['P_Time_DateTime'] . "'
                            ");
                        if ($queryAllDATA) {

                            while ($dataAllData = mysqli_fetch_assoc($queryAllDATA)) {
                                if ($dataAllData['door_id'] != null) {
                                    array_push($DoorDataArray, array(
                                        'door_id' => $dataAllData['door_id'],
                                        'door_name' => $dataAllData['door_name']
                                    ));
                                } else {
                                    array_push($DoorDataArray, array(
                                        'door_id' => 0,
                                        'door_name' => 'ประตูทั้งหมด'
                                    ));
                                }
                            }

                            array_push($myArray, array(
                                'NAME' => $data['NAME'],
                                'SURNAME' => $data['SURNAME'],
                                'PERID' => $data['PERID'],
                                'DEP_WORK' => $data['DEP_WORK'],
                                'Dep_name' => $data['Dep_name'],
                                'Dep_Code' => $data['Dep_Code'],
                                'P_Time_DateTime' => $data['P_Time_DateTime'],
                                'P_Time_TimeN' => $data['P_Time_TimeN'],
                                'P_Time_TimeO' => $data['P_Time_TimeO'],
                                'P_Time_TimeHN' => $data['P_Time_TimeHN'],
                                'P_Time_TimeHO' => $data['P_Time_TimeHO'],
                                'P_Time_UpdateBY' => $data['P_Time_UpdateBY'],
                                'P_Time_UpdateT' => $data['P_Time_UpdateT'],
                                'DoorData' => $DoorDataArray
                            ));

                        } else {
                            return false;
                        }
                    } else {
                        array_push($myArray, array(
                            'NAME' => $data['NAME'],
                            'SURNAME' => $data['SURNAME'],
                            'PERID' => $data['PERID'],
                            'DEP_WORK' => $data['DEP_WORK'],
                            'Dep_name' => $data['Dep_name'],
                            'Dep_Code' => $data['Dep_Code'],
                            'P_Time_DateTime' => $data['P_Time_DateTime'],
                            'P_Time_TimeN' => $data['P_Time_TimeN'],
                            'P_Time_TimeO' => $data['P_Time_TimeO'],
                            'P_Time_TimeHN' => $data['P_Time_TimeHN'],
                            'P_Time_TimeHO' => $data['P_Time_TimeHO'],
                            'P_Time_UpdateBY' => $data['P_Time_UpdateBY'],
                            'P_Time_UpdateT' => $data['P_Time_UpdateT'],
                            'DoorData' => $DoorDataArray
                        ));
                    }
                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เลือกบุคลากรรายบุคคลที่ได้จากการตั้งค่าเวลาปฏิบัติงานกรณีพิเศษ โดยใช้เงื่อนไขวันที่มาเปรียบเทียบ และ PERID มาเปรียบเทียบ
    public function GETPERSONDATEPERSONAL($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
        S.NAME,
        S.SURNAME,
        S.PERID,
        S.DEP_WORK,
        D.Dep_name,
        D.Dep_Code,
        P_Time_DateTime,
        P_Time_TimeN,
        P_Time_TimeO,
        P_Time_TimeHN,
        P_Time_TimeHO,
        P_Time_UpdateBY,
        P_Time_UpdateT
    FROM
        STAFF.Medperson AS S
    INNER JOIN (
        SELECT
	B.PERID,
	B.P_Time_DateTime,
	B.P_Time_TimeN,
	B.P_Time_TimeO,
	B.P_Time_TimeHN,
	B.P_Time_TimeHO,
	B.P_Time_UpdateBY,
	B.P_Time_UpdateT
    FROM
        HRTIME_DB.c_person_time AS A
    INNER JOIN HRTIME_DB.c_person_time AS B ON B.P_Time_ID = (
        SELECT
            MAX(P_Time_ID)
        FROM
            HRTIME_DB.c_person_time
        WHERE
            P_Time_DateTime <= '" . $DateConrdi . "'
        AND PERID = A.PERID
    )
    GROUP BY
        B.PERID
    ) AS H ON S.PERID = H.PERID
    INNER JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
    WHERE
        S.DEP_WORK = " . $DepCode . "
    AND S.PERID = " . $PERID . "
    AND S.CSTATUS != 0
    ORDER BY
        S.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรของหน่วยงานแต่ล่ะหน่วยงาน
    public function GETPERSONDATENOCORD($DepCode)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                    S.NAME,
                    S.SURNAME,
                    S.PERID,
                    S.DEP_WORK,
                    D.Dep_name,
                    P_Time_DateTime,
                    P_Time_TimeN,
                    P_Time_TimeO,
                    P_Time_TimeHN,
                    P_Time_TimeHO,
                    P_Time_UpdateBY,
                    P_Time_UpdateT
                FROM
                    STAFF.Medperson AS S
                    LEFT JOIN ( SELECT PERID, P_Time_DateTime, P_Time_TimeN, P_Time_TimeO, P_Time_TimeHN, P_Time_TimeHO, P_Time_UpdateBY, P_Time_UpdateT FROM HRTIME_DB.c_person_time WHERE P_Time_DateTime = '" . $dateNow . "' ) AS H ON S.PERID = H.PERID
                    LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
                WHERE
                    S.DEP_WORK = " . $DepCode . "
                    AND S.CSTATUS != 0
                ORDER BY
                    S.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลเวลาของหน่วยงานจากการตั้งค่าเวลา
    /*public function GETDATESETTING($Date, $DepCode)
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.D_Time_ID,
            A.D_Time_DateTime,
            A.D_Time_TimeN,
            A.D_Time_TimeO,
            A.D_Time_TimeHN,
            A.D_Time_TimeHO,
            A.D_Time_CreateBY,
            A.D_Time_UpdateBY,
            A.D_Time_CreateT,
            A.D_Time_UpdateT,
            A.Dep_code,
            A.door_id,
            B.Edit_code
            FROM
            HRTIME_DB.c_depart_time AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            WHERE
            A.D_Time_DateTime <= '" . $Date . "' AND
            B.Edit_code = '" . $DepCode . "'
            ORDER BY
            A.D_Time_DateTime DESC
            LIMIT 1");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }*/

    //คำนวณหาจำนวนวัน โดยใช้วันที่ปัจจุบันลบกับวันที่เลือกทำรายการ เพื่อหาค่าจำนวนวัน < 30 ไม่อนุญาติให้แก้ไขข้อมูล
    public function CALDATEDIFFCONDI($DateStart, $DateEnd)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT DATEDIFF('$DateStart', '$DateEnd') AS DateDiff");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //คำนวณหาวันที่ หักลบ จากการคลิกปุ่มเลื่อนวันที่ไปข้างหน้า หรือ หลัง
    public function CALDATESUBCONDI($DateStart, $IntevalDay)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT DATE_SUB('$DateStart', INTERVAL $IntevalDay DAY)");
            if ($query) {
                while ($data = mysqli_fetch_row($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLDOORHOS()
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            C_door
        WHERE
            zone_id != 5
            AND zone_id != 9");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //คำนวณหาวันที่ เพิ่ม จากการคลิกปุ่มเลื่อนวันที่ไปข้างหน้า
    public function CALDATEADDCONDI($DateStart, $IntevalDay)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT DATE_ADD('$DateStart', INTERVAL $IntevalDay DAY)");
            if ($query) {
                while ($data = mysqli_fetch_row($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLDOORHOSOUTDepart()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            C_door
        WHERE
            zone_id = 5
        UNION
        SELECT
            *
        FROM
            C_door
        WHERE
            zone_id = 9");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $myArray[] = $data;
                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETRANGETIMEDATA($TimeStart, $TimeEnd, $DepCode)
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Dep_name,
            HRTIME_DB.D_transaction.event_time
        FROM
            HRTIME_DB.D_transaction
            INNER JOIN STAFF.Medperson ON HRTIME_DB.D_transaction.PERID = STAFF.Medperson.PERID
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
        WHERE
            HRTIME_DB.D_transaction.event_time >= '" . $TimeStart . "'
            AND HRTIME_DB.D_transaction.event_time <= '" . $TimeEnd . "'
            AND STAFF.Depart.Dep_Code = '" . $DepCode . "'
        ORDER BY
            STAFF.Medperson.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETCARDNOPERSON($PERID)
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            C_card.PERID,
            C_card.CARD_NO
            FROM
            C_card
            WHERE
            C_card.PERID = " . $PERID . "
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เพิ่มข้อมูลที่ได้จากการ Approve จากหัวหน้าหน่วยงาน เพื่อนำไปสู่การประมวลผลสรุปรายงาน
    public function APPROVEDATATIME($C_PERID, $U_PERID, $OBJDATA, $DATENOW)
    {
        $myArray = array();
        $i = 0;
        $numItems = count($OBJDATA);
        $status_in = '';
        $status_in2 = '';
        $status_ho = '';
        $status_hn = '';
        $status_out = '';
        $status_out2 = '';

        if ($this->db->hostDB) {
            foreach ($OBJDATA as $key => $value) {
                if (strpos($value->TIME_IN, "สาย") > 0) {
                    $status_in = "สาย";
                } else {
                    $status_in = "";
                }
                if (strpos($value->TIME_IN2, "สาย") > 0) {
                    $status_in2 = "สาย";
                } else {
                    $status_in2 = "";
                }
                if (strpos($value->TIME_HO, "สาย") > 0) {
                    $status_ho = "สาย";
                } else if (strpos($value->TIME_HO, "ออกก่อน") > 0) {
                    $status_ho = "ออกก่อน";
                } else {
                    $status_ho = "";
                }
                if (strpos($value->TIME_HN, "สาย") > 0) {
                    $status_hn = "สาย";
                } else {
                    $status_hn = "";
                }
                if (strpos($value->TIME_OUT, "สาย") > 0) {
                    $status_out = "สาย";
                } else if (strpos($value->TIME_OUT, "ออกก่อน") > 0) {
                    $status_out = "ออกก่อน";
                } else {
                    $status_out = "";
                }
                if (strpos($value->TIME_OUT2, "สาย") > 0) {
                    $status_out2 = "สาย";
                } else if (strpos($value->TIME_OUT2, "ออกก่อน") > 0) {
                    $status_out2 = "ออกก่อน";
                } else {
                    $status_out2 = "";
                }

                // ตรวจสอบข้อมูลการลาถ้ามีการลาในเวลาการปฏิบัติงานให้ ตั้งค่าเป็นค่าว่างเอาไว้
                if (strpos($value->TIME_IN, "ลา") > 0) {
                    $value->TIME_IN = "";
                } else if (strpos($value->TIME_IN, "อื่นๆ") > 0) {
                    $value->TIME_IN = "";
                }
                if (strpos($value->TIME_IN2, "ลา") > 0) {
                    $value->TIME_IN2 = "";
                } else if (strpos($value->TIME_IN2, "อื่นๆ") > 0) {
                    $value->TIME_IN2 = "";
                }
                if (strpos($value->TIME_HO, "ลา") > 0) {
                    $value->TIME_HO = "";
                } else if (strpos($value->TIME_HO, "อื่นๆ") > 0) {
                    $value->TIME_HO = "";
                }
                if (strpos($value->TIME_HN, "ลา") > 0) {
                    $value->TIME_HN = "";
                } else if (strpos($value->TIME_HN, "อื่นๆ") > 0) {
                    $value->TIME_HN = "";
                }
                if (strpos($value->TIME_OUT, "ลา") > 0) {
                    $value->TIME_OUT = "";
                } else if (strpos($value->TIME_OUT, "อื่นๆ") > 0) {
                    $value->TIME_OUT = "";
                }
                if (strpos($value->TIME_OUT2, "ลา") > 0) {
                    $value->TIME_OUT2 = "";
                } else if (strpos($value->TIME_OUT2, "อื่นๆ") > 0) {
                    $value->TIME_OUT2 = "";
                }
                /////////////////////////////////////////////////////////////////////

                $query = mysqli_query($this->db->hostDB, "INSERT INTO `A_transaction` (
                    `PERID`,
                    `Dep_Code`,
                    `A_Datetime`,
                    `A_Time_in`,
                    `A_Status_in`,
                    `A_Door_in`,
                    `A_Time_in2`,
                    `A_Status_in2`,
                    `A_Door_in2`,
                    `A_Time_ho`,
                    `A_Status_ho`,
                    `A_Door_ho`,
                    `A_Time_hn`,
                    `A_Status_hn`,
                    `A_Door_hn`,
                    `A_Time_out`,
                    `A_Status_out`,
                    `A_Door_out`,
                    `A_Time_out2`,
                    `A_Status_out2`,
                    `A_Door_out2`,
                    `Create_By`,
                    `Create_Time`,
                    `Update_By`,
                    `Update_Time`
                )
                VALUES
                    (
                        '" . $value->PERID . "',
                        '" . $value->DEP_CODE . "',
                        '" . $DATENOW . "',
                        '" . $value->TIME_IN . "',
                        '" . $status_in . "',
                        '" . $value->DOOR_IN . "',
                        '" . $value->TIME_IN2 . "',
                        '" . $status_in2 . "',
                        '" . $value->DOOR_IN2 . "',
                        '" . $value->TIME_HO . "',
                        '" . $status_ho . "',
                        '" . $value->DOOR_HO . "',
                        '" . $value->TIME_HN . "',
                        '" . $status_hn . "',
                        '" . $value->DOOR_HN . "',
                        '" . $value->TIME_OUT . "',
                        '" . $status_out . "',
                        '" . $value->DOOR_OUT . "',
                        '" . $value->TIME_OUT2 . "',
                        '" . $status_out2 . "',
                        '" . $value->DOOR_OUT2 . "',
                        '" . $C_PERID . "',
                        CURRENT_TIMESTAMP,
                        '" . $U_PERID . "',
                        CURRENT_TIMESTAMP
                    );
                ");
                if ($query) {
                    if (++$i === $numItems) {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    }
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    // เรียกข้อมูลการปฏิบัติงานของบุคลากรในหน่วยงานที่ได้จากการ Approve โดยหัวหน้างาน
    public function GETDATAAPPORVEDEPART($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.PERID,
            A.Dep_Code,
            A.A_Datetime,
            A.A_Time_in,
            A.A_Status_in,
            A.A_Door_in,
            A.A_Time_in2,
            A.A_Status_in2,
            A.A_Door_in2,
            A.A_Time_ho,
            A.A_Status_ho,
            A.A_Door_ho,
            A.A_Time_hn,
            A.A_Status_hn,
            A.A_Door_hn,
            A.A_Time_out,
            A.A_Status_out,
            A.A_Door_out,
            A.A_Time_out2,
            A.A_Status_out2,
            A.A_Door_out2,
            A.Create_By,
            A.Create_Time,
            A.Update_By,
            A.Update_Time,
            B.`NAME`,
            B.SURNAME,
            C.Edit_code
        FROM
            A_transaction AS A
        INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
        WHERE
            A.A_Datetime = '" . $DateConrdi . "'
        AND B.PERID =  " . $PERID);
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //แก้ไขข้อมูลใหม่จากตารางApprove
    public function UPDATEDATAFORMAPPROVE($DepCode, $DateConrdi, $PERID, $OBJDATA)
    {
        $myArray = array();
        $i = 0;
        $numItems = count($OBJDATA);
        $status_in = '';
        $status_in2 = '';
        $status_ho = '';
        $status_hn = '';
        $status_out = '';
        $status_out2 = '';

        if ($this->db->hostDB) {
            foreach ($OBJDATA as $key => $value) {
                if (strpos($value->TIME_IN2, "สาย") > 0) {
                    $status_in2 = "สาย";
                } else {
                    $status_in2 = "";
                }
                if (strpos($value->TIME_HO, "สาย") > 0) {
                    $status_ho = "สาย";
                } else if (strpos($value->TIME_HO, "ออกก่อน") > 0) {
                    $status_ho = "ออกก่อน";
                } else {
                    $status_ho = "";
                }
                if (strpos($value->TIME_HN, "สาย") > 0) {
                    $status_hn = "สาย";
                } else {
                    $status_hn = "";
                }
                if (strpos($value->TIME_OUT2, "สาย") > 0) {
                    $status_out2 = "สาย";
                } else if (strpos($value->TIME_OUT2, "ออกก่อน") > 0) {
                    $status_out2 = "ออกก่อน";
                } else {
                    $status_out2 = "";
                }

                $query = mysqli_query($this->db->hostDB, "UPDATE A_transaction
                SET A_Time_in2 = '" . $value->TIME_IN2 . "',
                A_Door_in2 = '" . $value->DOOR_IN2 . "',
                A_Status_in2 = '" . $status_in2 . "',
                A_Time_ho = '" . $value->TIME_HO . "',
                A_Door_ho = '" . $value->DOOR_HO . "',
                A_Status_ho = '" . $status_ho . "',
                A_Time_hn = '" . $value->TIME_HN . "',
                A_Door_hn = '" . $value->DOOR_HN . "',
                A_Status_hn = '" . $status_hn . "',
                A_Time_out2 = '" . $value->TIME_OUT2 . "',
                A_Door_out2 = '" . $value->DOOR_OUT2 . "',
                A_Status_out2 = '" . $status_out2 . "',
                Update_By = '" . $PERID . "',
                Update_Time = CURRENT_TIMESTAMP
                WHERE A_Datetime = '" . $DateConrdi . "'
                AND PERID = '" . $value->PERID . "'");

                if ($query) {
                    if (++$i === $numItems) {
                        $arr = array('Status' => true, 'Message' => "แก้ไขข้อมูลสำเร็จ");
                    }
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    public function GETLEAVEPERSONDATA($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.T_Leave_Type,
            A.T_Leave_Reason,
            A.T_Leave_Date_Start,
            A.T_Leave_Date_End,
            A.T_Day_Type_Start,
            A.T_Day_Type_End,
            A.T_Leave_CreateBY,
            A.T_Leave_UpdateBY,
            A.T_Leave_CreateT,
            A.T_Leave_UpdateT,
            A.PERID,
            A.Dep_code,
            B.Leave_Detail,
            C.Edit_code
            FROM
            c_take_a_leave AS A
            INNER JOIN c_leave AS B ON A.T_Leave_Type = B.Leave_Type
            INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_code
            WHERE
            A.PERID = $PERID AND
            A.T_Leave_Date_Start <= '" . $DateConrdi . "' AND
            A.T_Leave_Date_End >= '" . $DateConrdi . "'
            AND T_Status_Leave = '1'");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // บันทึกข้อมูลการขาดงานของบุคลากรในหน่วยงาน
    public function SAVEABSENCEDATA($No_Id, $PERID)
    {
        $myArray = array();

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "UPDATE A_transaction
                SET A_Status_in2 = 'ขาดงาน',
                A_Status_ho = 'ขาดงาน',
                A_Status_hn = 'ขาดงาน',
                A_Status_out2 = 'ขาดงาน',
                Update_By = '" . $PERID . "',
                Update_Time = CURRENT_TIMESTAMP
                WHERE No_Id = '" . $No_Id . "'");

            if ($query) {
                $arr = array('Status' => true, 'Message' => "บันทึกการขาดงานสำเร็จ");
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้");
            }
            echo json_encode($arr);
        }
    }

    // บันทึกหมายเหตุประจำวันของบุคลากรในหน่วยงาน
    public function SAVECOMMENTDEP($DepCode, $PerId, $DateConrdi, $Comment, $PerId_Create)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            c_comment
        WHERE
            c_comment.PERID = $PerId
            AND c_comment.Comment_Date LIKE '" . $DateConrdi . "%'");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "INSERT INTO `c_comment` (
                    `Comment_Details`,
                    `Comment_Date`,
                    `PERID`,
                    `Dep_Code`,
                    `Create_BY`,
                    `Create_T`
                )
                VALUES
                    (
                        '" . $Comment . "',
                        '" . $DateConrdi . "',
                        '" . $PerId . "',
                        '" . $DepCode . "',
                        '" . $PerId_Create . "',
                        CURRENT_TIMESTAMP
                    );
                ");
                if ($query) {
                    $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                }
                echo json_encode($arr);
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ เนื่อองจากมีข้อมูลบันทึกในระบบแล้ว");
                echo json_encode($arr);
            }
        } else {
            return false;
        }
    }

    // เรียกดูหมายเหตุประจำวันของบุคลากรในหน่วยงาน
    public function GETALLCOMMENTDEP($DepCode, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.`NAME`,
            B.SURNAME,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Edit_code
        FROM
            HRTIME_DB.c_comment AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
            WHERE
            STAFF.Depart.Edit_code = $DepCode
            AND
            B.PERID = " . $PERID);
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
                echo json_encode($myArray);
            }
        } else {
            return false;
        }
    }

    public function GETCOMMENTDEP($DepCode, $DateConrdi, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.`NAME`,
            B.SURNAME,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Edit_code
        FROM
            HRTIME_DB.c_comment AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
            WHERE
            STAFF.Depart.Edit_code = $DepCode
            AND
            B.PERID = $PERID
            AND
            A.Comment_Date LIKE '" . $DateConrdi . "%'");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
                echo json_encode($myArray);
            }
        } else {
            return false;
        }
    }

    public function GETCOMMENTREPORTDEP($Dep_Code, $Date_Cordi, $Date_Cordi2)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.Comment_ID,
            A.Comment_Details,
            A.Comment_Date,
            A.PERID,
            A.Dep_Code,
            A.Create_BY,
            A.Create_T,
            B.`NAME`,
            B.SURNAME,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Edit_code
        FROM
            HRTIME_DB.c_comment AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
            WHERE
            STAFF.Depart.Edit_code = $Dep_Code
            AND A.Comment_Date
            BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรภายในหน่วยงาน
    public function GETPERSONFROMDEPART($DepCode, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                A.`NAME`,
                A.SURNAME,
                A.PERID,
                A.POS_LEVEL,
                A.Date_In,
                B.Dep_name,
                B.Dep_Code,
                C.PosName
                FROM
                STAFF.Medperson AS A
                LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
                LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
                WHERE
                A.PERID = $PERID AND
                B.Dep_Code = " . $DepCode . " AND
                A.CSTATUS != 0
                ORDER BY
                A.PERID ASC
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลเวลาของหน่วยงานจากการตั้งค่าเวลา
    public function GETDATESETTING($Date, $DepCode, $PERID)
    {
        $myArray = array();
        $myArray2 = array();
        $Dep_Time_Data = array();

        if ($this->db->hostDB) {

            for ($i = 0; $i < count($DepCode); $i++) {

                $queryPersonalOther = mysqli_query($this->db->hostDB, "
                    SELECT
                    A.C_Med_ID,
                    A.PERID,
                    A.Dep_Code,
                    A.Dep_Code_Old,
                    A.Create_By,
                    A.Create_Time
                    FROM
                    c_medperson AS A
                    WHERE
                    A.PERID = $PERID AND
                    A.Dep_Code_Old = '" . $DepCode[$i]->Dep_Code . "'
                ");

                if (mysqli_num_rows($queryPersonalOther) > 0) {
                    while ($dataPersonal = mysqli_fetch_assoc($queryPersonalOther)) {
                        $query = mysqli_query($this->db->hostDB, "SELECT
            A.D_Time_ID,
            A.D_Time_DateTime,
            A.D_Time_TimeN,
            A.D_Time_TimeO,
            A.D_Time_TimeHN,
            A.D_Time_TimeHO,
            A.D_Time_CreateBY,
            A.D_Time_UpdateBY,
            A.D_Time_CreateT,
            A.D_Time_UpdateT,
            A.Dep_code,
            A.door_id,
            B.Dep_Code
            FROM
            HRTIME_DB.c_depart_time AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            WHERE
            A.D_Time_DateTime <= '" . $Date . "' AND
            B.Dep_Code = '" . $dataPersonal['Dep_Code'] . "'
            ORDER BY
            A.D_Time_DateTime DESC
            LIMIT 1");
                        if ($query) {
                            while ($data = mysqli_fetch_assoc($query)) {
                                $myArray2 = array();
//                        array_push($myArray, $data);

                                $query2 = mysqli_query($this->db->hostDB, "SELECT
                    A.D_Time_ID,
                    A.D_Time_DateTime,
                    A.D_Time_TimeN,
                    A.D_Time_TimeO,
                    A.D_Time_TimeHN,
                    A.D_Time_TimeHO,
                    A.D_Time_CreateBY,
                    A.D_Time_UpdateBY,
                    A.D_Time_CreateT,
                    A.D_Time_UpdateT,
                    A.Dep_code,
                    A.door_id,
                    B.Dep_Code
                    FROM
                    HRTIME_DB.c_depart_time AS A
                    INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                    WHERE
                    A.D_Time_DateTime = '" . $data['D_Time_DateTime'] . "' AND
                    B.Dep_Code = '" . $data['Dep_Code'] . "'
                    ORDER BY
                    A.D_Time_DateTime DESC");
                                if ($query2) {
                                    while ($dataAllDoor = mysqli_fetch_assoc($query2)) {
//                                    array_push($myArray2, $data);
                                        array_push($myArray2, array('door_id' => $dataAllDoor['door_id']));
                                    }

                                }

                                array_push($myArray, array(
                                    'D_Time_ID' => $data['D_Time_ID'],
                                    'D_Time_DateTime' => $data['D_Time_DateTime'],
                                    'D_Time_TimeN' => $data['D_Time_TimeN'],
                                    'D_Time_TimeO' => $data['D_Time_TimeO'],
                                    'D_Time_TimeHN' => $data['D_Time_TimeHN'],
                                    'D_Time_TimeHO' => $data['D_Time_TimeHO'],
                                    'D_Time_CreateBY' => $data['D_Time_CreateBY'],
                                    'D_Time_UpdateBY' => $data['D_Time_UpdateBY'],
                                    'D_Time_CreateT' => $data['D_Time_CreateT'],
                                    'D_Time_UpdateT' => $data['D_Time_UpdateT'],
                                    'Dep_code' => $data['Dep_code'],
                                    'Dep_Code_Old' => $dataPersonal['Dep_Code_Old'],
                                    'Cordi' => true,
                                    'DoorData' => $myArray2
                                ));

                            }

                        } else {
                        }
                    }
                } else {
                    $query = mysqli_query($this->db->hostDB, "SELECT
            A.D_Time_ID,
            A.D_Time_DateTime,
            A.D_Time_TimeN,
            A.D_Time_TimeO,
            A.D_Time_TimeHN,
            A.D_Time_TimeHO,
            A.D_Time_CreateBY,
            A.D_Time_UpdateBY,
            A.D_Time_CreateT,
            A.D_Time_UpdateT,
            A.Dep_code,
            A.door_id,
            B.Dep_Code
            FROM
            HRTIME_DB.c_depart_time AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            WHERE
            A.D_Time_DateTime <= '" . $Date . "' AND
            B.Dep_Code = '" . $DepCode[$i]->Dep_Code . "'
            ORDER BY
            A.D_Time_DateTime DESC
            LIMIT 1");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {
                            $myArray2 = array();
//                        array_push($myArray, $data);

                            $query2 = mysqli_query($this->db->hostDB, "SELECT
                    A.D_Time_ID,
                    A.D_Time_DateTime,
                    A.D_Time_TimeN,
                    A.D_Time_TimeO,
                    A.D_Time_TimeHN,
                    A.D_Time_TimeHO,
                    A.D_Time_CreateBY,
                    A.D_Time_UpdateBY,
                    A.D_Time_CreateT,
                    A.D_Time_UpdateT,
                    A.Dep_code,
                    A.door_id,
                    B.Dep_Code
                    FROM
                    HRTIME_DB.c_depart_time AS A
                    INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                    WHERE
                    A.D_Time_DateTime = '" . $data['D_Time_DateTime'] . "' AND
                    B.Dep_Code = '" . $data['Dep_Code'] . "'
                    ORDER BY
                    A.D_Time_DateTime DESC");
                            if ($query2) {
                                while ($dataAllDoor = mysqli_fetch_assoc($query2)) {
//                                    array_push($myArray2, $data);
                                    array_push($myArray2, array('door_id' => $dataAllDoor['door_id']));
                                }

                            }

                            array_push($myArray, array(
                                'D_Time_ID' => $data['D_Time_ID'],
                                'D_Time_DateTime' => $data['D_Time_DateTime'],
                                'D_Time_TimeN' => $data['D_Time_TimeN'],
                                'D_Time_TimeO' => $data['D_Time_TimeO'],
                                'D_Time_TimeHN' => $data['D_Time_TimeHN'],
                                'D_Time_TimeHO' => $data['D_Time_TimeHO'],
                                'D_Time_CreateBY' => $data['D_Time_CreateBY'],
                                'D_Time_UpdateBY' => $data['D_Time_UpdateBY'],
                                'D_Time_CreateT' => $data['D_Time_CreateT'],
                                'D_Time_UpdateT' => $data['D_Time_UpdateT'],
                                'Dep_code' => $data['Dep_code'],
                                'Dep_Code_Old' => null,
                                'Cordi' => false,
                                'DoorData' => $myArray2
                            ));

                        }

                    }
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //บันทึกข้อมูลการแก้ไขเวลา
    public function SAVADATAEDITTIME($PERID, $date, $time_in, $time_out, $Create_By, $Create_Time)
    {
        $myArray = array();
        $values = '';
        if ($this->db->hostDB) {
            $querySel = mysqli_query($this->db->hostDB, "
                SELECT * FROM c_edit_time 
                WHERE 
                PERID = '" . $PERID . "' AND
                date = '" . $date . "'
            ");
            if ($querySel) {
                if (mysqli_num_rows($querySel) > 0) {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ เนื่องจากวันที่บันทึกข้อมูล มีการบันทึกข้อมูลแล้ว");
                    echo json_encode($arr);
                } else {
                    if ($time_in == '' && $time_out != '') {
                        $values = "(
                            '" . $PERID . "',
                            '" . $date . "',
                            null,
                            '" . $time_out . "',
                            '" . $Create_By . "',
                            " . $Create_Time . "
                        )";
                    } else if ($time_in != '' && $time_out == '') {
                        $values = "(
                            '" . $PERID . "',
                            '" . $date . "',
                             '" . $time_in . "',
                            null,
                            '" . $Create_By . "',
                            " . $Create_Time . "
                        )";
                    } else {
                        $values = "(
                            '" . $PERID . "',
                            '" . $date . "',
                            '" . $time_in . "',
                            '" . $time_out . "',
                            '" . $Create_By . "',
                            " . $Create_Time . "
                        );";
                    }
                    $query = mysqli_query($this->db->hostDB, "INSERT INTO `c_edit_time` (
                    `PERID`,
                    `date`,
                    `time_in`,
                    `time_out`,
                    `Create_By`,
                    `Create_Time`
                )
                VALUES " . $values);
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                    }
                    echo json_encode($arr);
                }
            }
        } else {
            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ โปรดติดต่อฝ่าย IT");
            echo json_encode($arr);
        }
    }

    public function CHECKDATAEDITTIME($PERID, $date)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $querySel = mysqli_query($this->db->hostDB, "
                SELECT * FROM c_edit_time 
                WHERE 
                PERID = '" . $PERID . "' AND
                date = '" . $date . "'
            ");
            if ($querySel) {
                if (mysqli_num_rows($querySel) > 0) {
                    $arr = array('Status' => true);
                } else {
                    $arr = array('Status' => false);
                }
                $myJSON = json_encode($arr);
                echo $myJSON;
            }
        } else {
            echo false;
        }
    }

    public function GETDATAEDITTIME($PERID, $date)
    {
        $myArray = array();
        if ($this->db->hostDB) {

            $querySel = mysqli_query($this->db->hostDB, "
                SELECT * FROM c_edit_time 
                WHERE 
                PERID = '" . $PERID . "' AND
                date = '" . $date . "'
            ");
            if ($querySel) {
                while ($data = mysqli_fetch_assoc($querySel)) {

                    $myArray[] = $data;

                }
                $myJSON = json_encode($myArray);
                echo $myJSON;
            }
        } else {
            echo false;
        }
    }
}
