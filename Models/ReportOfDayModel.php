<?php
class ReportOfDayModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GatAllDepartment()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                C_admin.PERID
            FROM
                C_admin
            WHERE
                C_admin.PERID = $PERID");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                    B.Dep_Code,
                    B.Edit_code,
                    B.Dep_name,
                    B.Dep_Group_name,
                    B.Telephone,
                    B.Doc_In,
                    B.Doc_Out,
                    B.Doc_go,
                    B.Doc_in2,
                    B.Dep_status
                    FROM
                    STAFF.D_admin AS A
                    INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                    WHERE
                    A.Perid = $PERID
                    ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                        A.Dep_Code,
                        A.Dep_name,
                        A.Edit_code,
                        A.Dep_Group_name
                        FROM
                        STAFF.Depart AS A
                        INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                        WHERE
                        B.PERID = $PERID
                        ORDER BY
                        A.Dep_name ASC
                        ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, constant('Date_Get_Department'));
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //  เรียกบุคลากรทุกคนภายในหน่วยงาน
    public function GETDEPART()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            B.Dep_Code,
            B.Edit_code,
            B.Dep_name,
            B.Dep_Group_name
            FROM
            STAFF.Depart AS B
            ");

            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {

            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการลาของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTLEAVEOFDAYSUMMARY($Date_Cordi)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.Edit_code,
            B.Dep_Code,
            B.Dep_name,
            B.Dep_Group_name,
            C.`NAME`,
            C.SURNAME
            FROM
            c_take_a_leave AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            INNER JOIN STAFF.Medperson AS C ON C.PERID = A.PERID
            WHERE
            A.T_Leave_Date_Start <= '$Date_Cordi' AND
            A.T_Leave_Date_End >= '$Date_Cordi'
            AND T_Status_Leave = '1'
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการปฏิบัติงานของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTWORKOFDAYSUMMARY($Date_Cordi)
    {
        ini_set("memory_limit", "-1");
        // set_time_limit(0);
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.No_Id,
            A.PERID,
            A.Dep_Code,
            A.A_Datetime,
            A.A_Time_in,
            A.A_Status_in,
            A.A_Door_in,
            A.A_Time_in2,
            A.A_Status_in2,
            A.A_Door_in2,
            A.A_Time_ho,
            A.A_Status_ho,
            A.A_Door_ho,
            A.A_Time_hn,
            A.A_Status_hn,
            A.A_Door_hn,
            A.A_Time_out,
            A.A_Status_out,
            A.A_Door_out,
            A.A_Time_out2,
            A.A_Status_out2,
            A.A_Door_out2,
            A.Create_By,
            A.Create_Time,
            A.Update_By,
            A.Update_Time,
            B.Edit_code,
            C.`NAME`,
            C.SURNAME
        FROM
            HRTIME_DB.A_transaction AS A
        INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_Code
        INNER JOIN STAFF.Medperson AS C ON C.PERID = A.PERID
        WHERE
            A.A_Datetime = '$Date_Cordi'
        ORDER BY
            A.Dep_Code ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                    *
                    FROM
                    D_holiday
                    ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETCOMMENTREPORTOFDAYDEP($Date_Cordi)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.Comment_ID,
            A.Comment_Details,
            A.Comment_Date,
            A.PERID,
            A.Dep_Code,
            A.Create_BY,
            A.Create_T,
            B.`NAME`,
            B.SURNAME,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Edit_code
        FROM
            HRTIME_DB.c_comment AS A
            INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
            INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
            WHERE
            A.Comment_Date = '$Date_Cordi'");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }
}
