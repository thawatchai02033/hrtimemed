<?php


class ReportLoadUnitModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GETAllDEPART()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                D_holiday
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //  เรียกข้อมูลบุคลากรในหน่วยงานโดยใช้เงื่อนไข Dep_Code
    public function GETPERSONFORMPERID($PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.PERID,
            A.`NAME`,
            A.SURNAME,
            A.POS_WORK,
            B.Dep_Code,
            B.Edit_code,
            B.Dep_name
            FROM
            STAFF.Depart AS B
            INNER JOIN STAFF.Medperson AS A ON B.Dep_Code = A.DEP_WORK
            WHERE
            A.PERID = $PERID
            AND A.CSTATUS != 0
            ORDER BY A.PERID ASC
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETDATAWORKINGPERSON($PERID, $Date_Start, $Date_End)
    {
        $WorkingData = array();
        $LeaveData = array();
        $LeaveData2 = array();
        $SummaryWorkingData = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                A_transaction.*
                FROM
                A_transaction
                WHERE
                A_transaction.PERID = $PERID AND
                A_transaction.A_Datetime BETWEEN '$Date_Start' AND '$Date_End'
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $WorkingData[] = $data;

                }
            } else {
            }

            // -------------------------------------------------- //

            $query = mysqli_query($this->db->hostDB, "SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.PERID = $PERID
                ORDER BY c_take_a_leave.T_Leave_Date_Start ASC
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $LeaveData[] = $data;

                }
            } else {
            }

            // -------------------------------------------------- //

            $query = mysqli_query($this->db->hostDB, "SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.PERID = $PERID AND
                c_take_a_leave.T_Leave_Date_Start < '$Date_Start' AND c_take_a_leave.T_Leave_Date_End >= '$Date_Start'
                ORDER BY c_take_a_leave.T_Leave_Date_Start ASC
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $LeaveData2[] = $data;

                }
            } else {
            }
            array_push($SummaryWorkingData, array('WorkingData' => $WorkingData, 'LeaveData' => $LeaveData, 'LeaveData2' => $LeaveData2));
            $myJSON = json_encode($SummaryWorkingData);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //  เรียกบุคลากรทุกคนภายในหน่วยงาน
    public function GETPERSONALDEPART($PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.PERID,
            A.`NAME`,
            A.SURNAME,
            A.POS_WORK,
            B.Dep_Code,
            B.Edit_code,
            B.Dep_name
            FROM
            STAFF.Depart AS B
            INNER JOIN STAFF.Medperson AS A ON B.Dep_Code = A.DEP_WORK
            WHERE
            A.PERID = $PERID
            AND A.CSTATUS != 0
            ");

            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {

            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการลาของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTLEAVESUMMARY($PERID, $Date_Cordi, $Date_Cordi2)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
            SELECT
            A.*,
            B.Edit_code,
            B.Dep_Code
            FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN STAFF.Depart AS B ON A.Dep_code = B.Dep_Code
            WHERE
            A.PERID = $PERID AND
            A.T_Leave_Date_Start <= '$Date_Cordi2' AND
            A.T_Leave_Date_End >= '$Date_Cordi' AND
            A.T_Status_Leave = '1'
            ");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                *
            FROM
                c_take_a_leave
            WHERE
                c_take_a_leave.PERID = $PERID
            AND T_Leave_Date_End = '$Date_Cordi'
            AND T_Status_Leave = '1'
            ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
                $myJSON = json_encode($myArray);
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
                $myJSON = json_encode($myArray);
            }
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETDATAWORKINGHOUR($PERID, $Date_Cordi)
    {
        $myArray = array();
        list($year, $month, $day) = explode('-', $Date_Cordi);
        $Date = explode("-", $Date_Cordi);
        if ($this->db->hostDB2) {
            $query = mysqli_query($this->db->hostDB2, "
            SELECT *
            FROM HR_FINANCE.`D_form_pay`
            WHERE
            D_form_pay.perid_pay = '".$PERID."' AND
            D_form_pay.monthly LIKE '$Date[0]-$Date[1]%' AND
            D_form_pay.cstatus = 0
            ORDER BY D_form_pay.date_lu
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETDATAWORKINGSUMMARY($PERID, $Date_Cordi)
    {
        $myArray = array();
        list($year, $month, $day) = explode('-', $Date_Cordi);
        $Date = explode("-", $Date_Cordi);
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB2, "
            SELECT *
            FROM HR_FINANCE.`D_form_pay`
            WHERE
            D_form_pay.perid_pay = '".$PERID."' AND
            D_form_pay.monthly LIKE '$Date[0]-$Date[1]%' AND
            D_form_pay.cstatus = 0
            ORDER BY D_form_pay.date_lu
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETDOCTORSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2){
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($Dep_Code); $i++) {
                $query = mysqli_query($this->db->hostDB, "
                SELECT
                A.`no`,
                A.perid,
                A.day_of_month,
                A.count,
                B.`NAME`,
                B.SURNAME,
                C.Dep_Code,
                C.Edit_code,
                C.Dep_name,
                C.Dep_Group_name
                FROM
                HRTIME_DB.doctor_summary AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.perid
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                WHERE
                C.Dep_Code = " . $Dep_Code[$i]->Dep_Code . " AND
                A.day_of_month BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

//                        $myArray[] = $data;
                        array_push($myArray, $data);

                    }

                    $queryOtherPerson = mysqli_query($this->db->hostDB, "SELECT
            A.*
            FROM
            HRTIME_DB.c_medperson AS A
            WHERE
            A.Dep_Code = " . $Dep_Code[$i]->Dep_Code . "
                ");
                    if ($queryOtherPerson) {
                        while ($dataOther = mysqli_fetch_assoc($queryOtherPerson)) {

                            $queryDataOtherPerson = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.`no`,
                                A.perid,
                                A.day_of_month,
                                A.count,
                                B.`NAME`,
                                B.SURNAME,
                                C.Dep_Code,
                                C.Edit_code,
                                C.Dep_name,
                                C.Dep_Group_name
                                FROM
                                HRTIME_DB.doctor_summary AS A
                                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.perid
                                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                                WHERE
                                A.perid = " . $dataOther['PERID'] . " AND
                                A.day_of_month BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                            ");
                            if ($queryDataOtherPerson) {
                                while ($dataOtherPerson = mysqli_fetch_assoc($queryDataOtherPerson)) {

                                    array_push($myArray, $dataOtherPerson);

                                }
                            }

                        }
                    } else {
                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }
}
