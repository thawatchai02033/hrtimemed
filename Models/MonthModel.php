<?php

class MonthModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GatAllDepartment()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.*
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.*
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.*
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //  เรียกบุคลากรทุกคนภายในหน่วยงาน
    public function GETPERSONALDEPART($Dep_Code)
    {
        $PersonData = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($Dep_Code); $i++) {
                $query = mysqli_query($this->db->hostDB, "
                SELECT
                A.PERID,
                A.`NAME`,
                A.SURNAME,
                B.Dep_Code,
                B.Edit_code,
                B.Dep_name,
                C.PosName,
                C.PosCode,
                A.POS_WORK
                FROM
                STAFF.Depart AS B
                INNER JOIN STAFF.Medperson AS A ON B.Dep_Code = A.DEP_WORK
                INNER JOIN STAFF.Positions AS C ON A.NewPos = C.PosCode
                WHERE
                B.Dep_Code = " . $Dep_Code[$i]->Dep_Code . " AND
                A.CSTATUS != 0
                ORDER BY
                A.PERID ASC
            ");
                if(mysqli_num_rows($query) > 0){
                    if ($query) {

                        while ($data = mysqli_fetch_assoc($query)) {

                            array_push($PersonData, $data);

                        }

                        $queryOtherPerson = mysqli_query($this->db->hostDB, "
                        SELECT
                            A.PERID,
                            A.`NAME`,
                            A.SURNAME,
                            A.POS_WORK,
                            B.Dep_Code,
                            B.Edit_code,
                            B.Dep_name,
                            C.PosName,
                            C.PosCode
                        FROM
                            HRTIME_DB.c_medperson AS D
                        INNER JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
                        INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
                        INNER JOIN STAFF.Positions AS C ON A.NewPos = C.PosCode
                        WHERE
                            D.Dep_Code = " . $Dep_Code[$i]->Dep_Code . "
                        AND A.CSTATUS != 0
                        ORDER BY
                            A.PERID ASC");

                        if(mysqli_num_rows($queryOtherPerson) > 0){
                            if ($queryOtherPerson) {

                                while ($dataOtherPerson = mysqli_fetch_assoc($queryOtherPerson)) {

                                    array_push($PersonData, $dataOtherPerson);

                                }

                            } else {

                            }
                        }

                    } else {

                    }
                } else {
                    $queryOtherPerson = mysqli_query($this->db->hostDB, "
                        SELECT
                            A.PERID,
                            A.`NAME`,
                            A.SURNAME,
                            A.POS_WORK,
                            B.Dep_Code,
                            B.Edit_code,
                            B.Dep_name,
                            C.PosName,
                            C.PosCode
                        FROM
                            HRTIME_DB.c_medperson AS D
                        INNER JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
                        INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
                        INNER JOIN STAFF.Positions AS C ON A.NewPos = C.PosCode
                        WHERE
                            D.Dep_Code = " . $Dep_Code[$i]->Dep_Code . "
                        AND A.CSTATUS != 0
                        ORDER BY
                            A.PERID ASC");

                    if(mysqli_num_rows($queryOtherPerson) > 0){
                        if ($queryOtherPerson) {

                            while ($dataOtherPerson = mysqli_fetch_assoc($queryOtherPerson)) {

                                array_push($PersonData, $dataOtherPerson);

                            }

                        } else {

                        }
                    }
                }

            }
            $myJSON = json_encode($PersonData);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการลาของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTLEAVEOFMONTH($Dep_Code, $Date_Cordi, $Date_Cordi2)
    {
        $Arr_ResultData = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($Dep_Code); $i++) {
                $query = mysqli_query($this->db->hostDB, "
            SELECT
            A.*,
            B.Edit_code
            FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN STAFF.Depart AS B ON A.Dep_code = B.Dep_Code
            WHERE
            B.Dep_Code = " . $Dep_Code[$i]->Dep_Code . " AND
            A.T_Leave_Date_End >= '$Date_Cordi' AND
            A.T_Status_Leave = '1'
            ");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                *
            FROM
                c_take_a_leave
            WHERE
                c_take_a_leave.Dep_code = " . $Dep_Code[$i]->Dep_Code . " 
            AND T_Leave_Date_End = '$Date_Cordi'
            AND T_Status_Leave = '1'
            ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            array_push($Arr_ResultData, $data);

                        }

                        $queryCheckPersonOther = mysqli_query($this->db->hostDB, "
                            SELECT
                            A.*
                            FROM
                            HRTIME_DB.c_medperson AS A
                            WHERE
                            A.Dep_Code = " . $Dep_Code[$i]->Dep_Code . "
                        ");

                        if(mysqli_num_rows($queryCheckPersonOther) > 0){
                            while ($dataPersonOther = mysqli_fetch_assoc($queryCheckPersonOther)) {

                                $queryPersonOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*,
                                B.Edit_code
                                FROM
                                HRTIME_DB.c_take_a_leave AS A
                                INNER JOIN STAFF.Depart AS B ON A.Dep_code = B.Dep_Code
                                WHERE
                                PERID = " . $dataPersonOther['PERID'] . " AND
                                B.Dep_Code = " . $dataPersonOther['Dep_Code_Old'] . " AND
                                A.T_Leave_Date_End >= '$Date_Cordi' AND
                                A.T_Status_Leave = '1'
                                ");

                                if (mysqli_num_rows($queryPersonOther) > 0) {
                                    while ($PersonOther = mysqli_fetch_assoc($queryPersonOther)) {
                                        array_push($Arr_ResultData, $PersonOther);
                                    }
                                } else {
                                    $queryPersonOther = mysqli_query($this->db->hostDB, "SELECT
                                    *
                                FROM
                                    c_take_a_leave
                                WHERE
                                PERID = " . $dataPersonOther['PERID'] . "
                                AND c_take_a_leave.Dep_code = " . $dataPersonOther['Dep_Code_Old'] . "
                                AND T_Leave_Date_End = '$Date_Cordi'
                                AND T_Status_Leave = '1'
                                ");

                                    if (mysqli_num_rows($queryPersonOther) > 0) {
                                        while ($PersonOther = mysqli_fetch_assoc($queryPersonOther)) {
                                            array_push($Arr_ResultData, $PersonOther);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                    }
                } else {
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            array_push($Arr_ResultData, $data);

                        }

                        $queryCheckPersonOther = mysqli_query($this->db->hostDB, "
                            SELECT
                            A.*
                            FROM
                            HRTIME_DB.c_medperson AS A
                            WHERE
                            A.Dep_Code = " . $Dep_Code[$i]->Dep_Code . "
                        ");

                        if(mysqli_num_rows($queryCheckPersonOther) > 0){
                            while ($dataPersonOther = mysqli_fetch_assoc($queryCheckPersonOther)) {
                                $queryPersonOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*,
                                B.Edit_code
                                FROM
                                HRTIME_DB.c_take_a_leave AS A
                                INNER JOIN STAFF.Depart AS B ON A.Dep_code = B.Dep_Code
                                WHERE
                                PERID = " . $dataPersonOther['PERID'] . " AND
                                B.Dep_Code = " . $dataPersonOther['Dep_Code_Old'] . " AND
                                A.T_Leave_Date_End >= '$Date_Cordi' AND
                                A.T_Status_Leave = '1'
                                ");

                                if (mysqli_num_rows($queryPersonOther) > 0) {
                                    while ($PersonOther = mysqli_fetch_assoc($queryPersonOther)) {
                                        array_push($Arr_ResultData, $PersonOther);
                                    }
                                } else {
                                    $queryPersonOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                    *
                                FROM
                                    c_take_a_leave
                                WHERE
                                PERID = " . $dataPersonOther['PERID'] . "
                                AND c_take_a_leave.Dep_code = " . $dataPersonOther['Dep_Code_Old'] . "
                                AND T_Leave_Date_End = '$Date_Cordi'
                                AND T_Status_Leave = '1'
                                ");

                                    if (mysqli_num_rows($queryPersonOther) > 0) {
                                        while ($PersonOther = mysqli_fetch_assoc($queryPersonOther)) {
                                            array_push($Arr_ResultData, $PersonOther);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                    }
                }
            }
            $myJSON = json_encode($Arr_ResultData);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการปฏิบัติงานของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTWORKOFMONTH($Dep_Code, $Date_Cordi, $Date_Cordi2)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($Dep_Code); $i++) {
                $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.Edit_code
            FROM
            HRTIME_DB.A_transaction AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_Code
            WHERE
            A.Dep_Code = " . $Dep_Code[$i]->Dep_Code . " AND
            A.A_Datetime BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

//                        $myArray[] = $data;
                        array_push($myArray, $data);

                    }

                    $queryOtherPerson = mysqli_query($this->db->hostDB, "SELECT
            A.*
            FROM
            HRTIME_DB.c_medperson AS A
            WHERE
            A.Dep_Code = " . $Dep_Code[$i]->Dep_Code . "
                ");
                    if ($queryOtherPerson) {
                        while ($dataOther = mysqli_fetch_assoc($queryOtherPerson)) {

                            $queryDataOtherPerson = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.Edit_code
            FROM
            HRTIME_DB.A_transaction AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_Code
            WHERE
            A.PERID = " . $dataOther['PERID'] . " AND
            A.Dep_Code = " . $dataOther['Dep_Code'] . " AND
            A.A_Datetime BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ");
                            if ($queryDataOtherPerson) {
                                while ($dataOtherPerson = mysqli_fetch_assoc($queryDataOtherPerson)) {

                                    array_push($myArray, $dataOtherPerson);

                                }
                            }

                        }
                    } else {
                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }


    public function GETDOCTORSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2){
        $myArray = array();
        if ($this->db->hostDB) {
            for ($i = 0; $i < count($Dep_Code); $i++) {
                $query = mysqli_query($this->db->hostDB, "
                SELECT
                A.`no`,
                A.perid,
                A.day_of_month,
                A.count,
                B.`NAME`,
                B.SURNAME,
                C.Dep_Code,
                C.Edit_code,
                C.Dep_name,
                C.Dep_Group_name
                FROM
                HRTIME_DB.doctor_summary AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.perid
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                WHERE
                C.Dep_Code = " . $Dep_Code[$i]->Dep_Code . " AND
                A.day_of_month BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

//                        $myArray[] = $data;
                        array_push($myArray, $data);

                    }

                    $queryOtherPerson = mysqli_query($this->db->hostDB, "SELECT
            A.*
            FROM
            HRTIME_DB.c_medperson AS A
            WHERE
            A.Dep_Code = " . $Dep_Code[$i]->Dep_Code . "
                ");
                    if ($queryOtherPerson) {
                        while ($dataOther = mysqli_fetch_assoc($queryOtherPerson)) {

                            $queryDataOtherPerson = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.`no`,
                                A.perid,
                                A.day_of_month,
                                A.count,
                                B.`NAME`,
                                B.SURNAME,
                                C.Dep_Code,
                                C.Edit_code,
                                C.Dep_name,
                                C.Dep_Group_name
                                FROM
                                HRTIME_DB.doctor_summary AS A
                                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.perid
                                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                                WHERE
                                A.perid = " . $dataOther['PERID'] . " AND
                                A.day_of_month BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                            ");
                            if ($queryDataOtherPerson) {
                                while ($dataOtherPerson = mysqli_fetch_assoc($queryDataOtherPerson)) {

                                    array_push($myArray, $dataOtherPerson);

                                }
                            }

                        }
                    } else {
                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                    *
                    FROM
                    D_holiday
                    ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $myArray[] = $data;
                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }
}
