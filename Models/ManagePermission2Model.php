<?php

class ManagePermission2Model extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function GETALLDEPARTMENT()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //  บันทึกสิทธิ์ รายชื่อผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function SAVEMASTERDATA($PERID)
    {
        $MasterDataArr = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT * FROM HRTIME_DB.`c_master` WHERE PERID = " . $PERID);
            if (mysqli_num_rows($query) > 0) {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ รายชื่อที่ท่านเลือกมีในฐานข้อมูลแล้ว");
            } else {
                $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_master` (
                    `PERID`
                )
                VALUES(
                    $PERID
                    )
                ");
                if ($query) {
                    $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.Master_ID,
                        A.PERID,
                        B.`NAME`,
                        B.SURNAME,
                        C.Dep_Code,
                        C.Edit_code,
                        C.Dep_name,
                        C.Dep_Group_name
                        FROM
                        HRTIME_DB.c_master AS A
                        INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK");
                    if (mysqli_num_rows($queryMaster) > 0) {
                        while ($masterData = mysqli_fetch_assoc($queryMaster)) {
                            $MasterDataArr[] = $masterData;
                        }
                    }
                    $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ", 'Master_Data' => $MasterDataArr);
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }


    //  บันทึกสิทธิ์ รายชื่อผู้ตัวแมน,รองหัวหน้า,หัวหน้าประจำหน่วยมีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function SAVEVICEDATA($PERID)
    {
        $MasterDataArr = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT * FROM HRTIME_DB.`c_vice` WHERE PERID = " . $PERID);
            if (mysqli_num_rows($query) > 0) {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้ รายชื่อที่ท่านเลือกมีในฐานข้อมูลแล้ว");
            } else {
                $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_vice` (
                    `PERID`
                )
                VALUES(
                    $PERID
                    )
                ");
                if ($query) {
                    $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.*,
                        B.`NAME`,
                        B.SURNAME,
                        C.Dep_Code,
                        C.Edit_code,
                        C.Dep_name,
                        C.Dep_Group_name
                        FROM
                        HRTIME_DB.c_vice AS A
                        INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK");
                    if (mysqli_num_rows($queryMaster) > 0) {
                        while ($masterData = mysqli_fetch_assoc($queryMaster)) {
                            $MasterDataArr[] = $masterData;
                        }
                    }
                    $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ", 'Master_Data' => $MasterDataArr);
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    public function DELETEDATAPERMISSADMIN($PERID)
    {
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_admin_permiss`
                WHERE PERID = " . $PERID);
            if ($query) {
                $query = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_master`
                WHERE PERID = " . $PERID);

                if ($query) {
                    $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                }
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    public function DELETEDATAPERMISSVICE($PERID)
    {
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_user_permiss`
                WHERE PERID = " . $PERID);
            if ($query) {
                $query = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_vice`
                WHERE PERID = " . $PERID);

                if ($query) {
                    $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                }
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    public function DELETEPERSONCHANGEDEP($PERID)
    {
        if ($this->db->hostDB) {

            $querySel = mysqli_query($this->db->hostDB, "
                SELECT * FROM HRTIME_DB.`c_medperson`
                WHERE PERID = " . $PERID);

            if(mysqli_num_rows($querySel) > 0) {
               while ($dataSel = mysqli_fetch_assoc($querySel)) {
                   mysqli_query($this->db->hostDB, 'UPDATE A_transaction SET Dep_Code = ' . $dataSel["Dep_Code_Old"] . ' WHERE PERID = ' . $PERID);
                   mysqli_query($this->db->hostDB, 'UPDATE c_take_a_leave SET Dep_Code = ' . $dataSel["Dep_Code_Old"] . ' WHERE PERID = ' . $PERID);
                   mysqli_query($this->db->hostDB, 'UPDATE c_leave_set SET Dep_Code = ' . $dataSel["Dep_Code_Old"] . ' WHERE PERID = ' . $PERID);
                   mysqli_query($this->db->hostDB, 'UPDATE c_person_time SET Dep_Code = ' . $dataSel["Dep_Code_Old"] . ' WHERE PERID = ' . $PERID);
                   mysqli_query($this->db->hostDB, 'UPDATE c_comment SET Dep_Code = ' . $dataSel["Dep_Code_Old"] . ' WHERE PERID = ' . $PERID);
               }
                $query = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_medperson`
                WHERE PERID = " . $PERID);

                if ($query) {
                    $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_medperson`
                WHERE PERID = " . $PERID);

                if ($query) {
                    $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    //
    public function GETMASTERDATA()
    {
        $MasterDataArr = array();
        if ($this->db->hostDB) {
            $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.Master_ID,
                        A.PERID,
                        B.`NAME`,
                        B.SURNAME,
                        C.Dep_Code,
                        C.Edit_code,
                        C.Dep_name,
                        C.Dep_Group_name
                        FROM
                        HRTIME_DB.c_master AS A
                        INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK");
            if (mysqli_num_rows($queryMaster) > 0) {
                while ($masterData = mysqli_fetch_assoc($queryMaster)) {
                    $MasterDataArr[] = $masterData;
                }
            }
            echo json_encode($MasterDataArr);
        } else {
            return false;
        }
    }

    public function GETVICEDATA()
    {
        $MasterDataArr = array();
        if ($this->db->hostDB) {
            $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.*,
                        B.`NAME`,
                        B.SURNAME,
                        C.Dep_Code,
                        C.Edit_code,
                        C.Dep_name,
                        C.Dep_Group_name
                        FROM
                        HRTIME_DB.c_vice AS A
                        INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                        INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK");
            if (mysqli_num_rows($queryMaster) > 0) {
                while ($masterData = mysqli_fetch_assoc($queryMaster)) {
                    $MasterDataArr[] = $masterData;
                }
            }
            echo json_encode($MasterDataArr);
        } else {
            return false;
        }
    }

    public function GETPERMISSMASTER($PERID)
    {
        $MasterDataArr = array();
        if ($this->db->hostDB) {
            $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.Master_ID,
                        A.PERID,
                        A.Dep_Code,
                        A.Create_By,
                        A.Create_Time
                        FROM
                        c_admin_permiss AS A
                        WHERE
                        A.PERID = $PERID
                        ");
            if (mysqli_num_rows($queryMaster) > 0) {
                while ($masterData = mysqli_fetch_assoc($queryMaster)) {
                    $MasterDataArr[] = $masterData;
                }
            }
            echo json_encode($MasterDataArr);
        } else {
            return false;
        }
    }

    public function GETPERMISSVICE($PERID)
    {
        $MasterDataArr = array();
        $MasterOptDataArr = array();
        $MIXMasterArr = array();
        if ($this->db->hostDB) {
            $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.*
                        FROM
                        c_user_permiss AS A
                        WHERE
                        A.PERID = $PERID
                        ");
            if (mysqli_num_rows($queryMaster) > 0) {
                while ($masterData = mysqli_fetch_assoc($queryMaster)) {
                    $MasterDataArr[] = $masterData;
                }

                $queryMasterOption = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.User_ID,
                        A.PERID,
                        A.Menu_Permiss,
                        A.Option_Permiss,
                        A.Create_By,
                        A.Update_By,
                        A.Create_Time,
                        A.Update_Time,
                        B.Opt_C_Details,
                        C.Permiss_Details
                        FROM
                        c_user_option AS A
                        LEFT JOIN c_option_permiss AS B ON B.Opt_C_Code = A.Option_Permiss
                        LEFT JOIN c_permission AS C ON C.Permiss_ID = A.Menu_Permiss
                        WHERE
                        A.PERID = $PERID
                        ");

                if (mysqli_num_rows($queryMasterOption) > 0) {
                    while ($masterDataOption = mysqli_fetch_assoc($queryMasterOption)) {
                        $MasterOptDataArr[] = $masterDataOption;
                    }
                }

                array_push($MIXMasterArr, array(
                    'ViceData' => $MasterDataArr,
                    'ViceOpt' => $MasterOptDataArr
                ));
            }
            echo json_encode($MIXMasterArr);
        } else {
            return false;
        }
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function SAVEDATAPERMISSADMIN($PERID, $PERID_PERSON, $Permiss_Dep)
    {
        $Query_Insert = '';
        $Count = count($Permiss_Dep);
        for ($i = 0; $i < count($Permiss_Dep); $i++) {
            if ($Permiss_Dep[$i]->IsChecked == true) {
                if (strlen($Query_Insert) > 0) {
                    $Query_Insert .= ', (' . $PERID_PERSON . ', "' . $Permiss_Dep[$i]->Dep_Code . '", ' . $PERID . ', CURRENT_TIME)';
                } else {
                    $Query_Insert .= '(' . $PERID_PERSON . ', "' . $Permiss_Dep[$i]->Dep_Code . '", ' . $PERID . ', CURRENT_TIME)';
                }
            }
        }
        if ($this->db->hostDB) {
            $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.*
                        FROM
                        HRTIME_DB.`c_admin_permiss` AS A
                        WHERE A.PERID = $PERID_PERSON");
            if (mysqli_num_rows($queryMaster) > 0) {
                $QueryRemoveData = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_admin_permiss`
                WHERE PERID = " . $PERID_PERSON);
                if ($QueryRemoveData) {
                    $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_admin_permiss` (
                    `PERID`,
                    `Dep_Code`,
                    `Create_By`,
                    `Create_Time`
                )
                VALUES
                    $Query_Insert
                ");
                    if ($query) {
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                        }
                        echo json_encode($arr);
                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_admin_permiss` (
                    `PERID`,
                    `Dep_Code`,
                    `Create_By`,
                    `Create_Time`
                )
                VALUES
                    $Query_Insert
                ");
                if ($query) {
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                    }
                    echo json_encode($arr);
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้ โปรดเลือกหน่วยงาน");
                    echo json_encode($arr);
                }
            }
        } else {
            return false;
        }
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function SAVEDATAPERMISSVICE($PERID, $PERID_PERSON, $Permiss_Dep)
    {
        $Query_Insert = '';
        $Count = count($Permiss_Dep);
        for ($i = 0; $i < count($Permiss_Dep); $i++) {
            if ($Permiss_Dep[$i]->IsChecked == true) {
                if (strlen($Query_Insert) > 0) {
                    $Query_Insert .= ', (' . $PERID_PERSON . ', "' . $Permiss_Dep[$i]->Dep_Code . '", ' . $PERID . ', CURRENT_TIME)';
                } else {
                    $Query_Insert .= '(' . $PERID_PERSON . ', "' . $Permiss_Dep[$i]->Dep_Code . '", ' . $PERID . ', CURRENT_TIME)';
                }
            }
        }
        if ($this->db->hostDB) {
            $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.*
                        FROM
                        HRTIME_DB.`c_user_permiss` AS A
                        WHERE A.PERID = $PERID_PERSON");
            if (mysqli_num_rows($queryMaster) > 0) {
                $QueryRemoveData = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_user_permiss`
                WHERE PERID = " . $PERID_PERSON);
                if ($QueryRemoveData) {
                    $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_user_permiss` (
                    `PERID`,
                    `Dep_Code`,
                    `Create_By`,
                    `Create_Time`
                )
                VALUES
                    $Query_Insert
                ");
                    if ($query) {
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                        }
                        echo json_encode($arr);
                    } else {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                        echo json_encode($arr);
                    }
                } else {
                    $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    echo json_encode($arr);
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_user_permiss` (
                    `PERID`,
                    `Dep_Code`,
                    `Create_By`,
                    `Create_Time`
                )
                VALUES
                    $Query_Insert
                ");
                if ($query) {
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                    }
                    echo json_encode($arr);
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้ โปรดเลือกหน่วยงาน");
                    echo json_encode($arr);
                }
            }
        } else {
            return false;
        }
    }

    public function SAVEDATAFUNCVICE($PERID, $PERID_PERSON, $Func_Permiss, $Option_Permiss)
    {
        $Query_Fuc_Insert = '';
        $Query_Opt_Insert = '';
        $Count = count($Option_Permiss);
        for ($i = 0; $i < count($Func_Permiss); $i++) {
            if ($Func_Permiss[$i]->IsChecked == true) {
                for ($k = 0; $k < count($Option_Permiss); $k++) {
                    if ($Option_Permiss[$k]->IsChecked == true) {
                        if ($Func_Permiss[$i]->Permiss_ID == $Option_Permiss[$k]->Permiss_ID) {
                            if ($i < count($Func_Permiss)) {
                                if (strlen($Query_Opt_Insert) > 0) {
                                    $Query_Opt_Insert .= ', (' . $PERID_PERSON . ', "' . $Func_Permiss[$i]->Permiss_ID . '", "' . $Option_Permiss[$k]->Opt_C_Code . '", ' . $PERID . ', ' . $PERID . ', CURRENT_TIME, CURRENT_TIME)';
                                } else {
                                    $Query_Opt_Insert .= '(' . $PERID_PERSON . ', "' . $Func_Permiss[$i]->Permiss_ID . '", "' . $Option_Permiss[$k]->Opt_C_Code . '", ' . $PERID . ', ' . $PERID . ', CURRENT_TIME, CURRENT_TIME)';
                                }
                            } else {
                                $Query_Opt_Insert .= '(' . $PERID_PERSON . ', "' . $Func_Permiss[$i]->Permiss_ID . '", "' . $Option_Permiss[$k]->Opt_C_Code . '", ' . $PERID . ', ' . $PERID . ', CURRENT_TIME, CURRENT_TIME)';
                            }
                        } else {
                            if ($k == ($Count - 1)) {
                                if (strlen($Query_Opt_Insert) > 0) {
                                    $Query_Opt_Insert .= ', (' . $PERID_PERSON . ', "' . $Func_Permiss[$i]->Permiss_ID . '", null, ' . $PERID . ', ' . $PERID . ', CURRENT_TIME, CURRENT_TIME)';
                                } else {
                                    $Query_Opt_Insert .= '(' . $PERID_PERSON . ', "' . $Func_Permiss[$i]->Permiss_ID . '", null, ' . $PERID . ', ' . $PERID . ', CURRENT_TIME, CURRENT_TIME)';
                                }
                            }
                        }
                    } else {
                        if ($k == ($Count - 1)) {
                            if (strlen($Query_Opt_Insert) > 0) {
                                $Query_Opt_Insert .= ', (' . $PERID_PERSON . ', "' . $Func_Permiss[$i]->Permiss_ID . '", null, ' . $PERID . ', ' . $PERID . ', CURRENT_TIME, CURRENT_TIME)';
                            } else {
                                $Query_Opt_Insert .= '(' . $PERID_PERSON . ', "' . $Func_Permiss[$i]->Permiss_ID . '", null, ' . $PERID . ', ' . $PERID . ', CURRENT_TIME, CURRENT_TIME)';
                            }
                        }
                    }
                }
            }
        }

        if ($this->db->hostDB) {
            $queryMaster = mysqli_query($this->db->hostDB, "
                        SELECT
                        A.*
                        FROM
                        HRTIME_DB.`c_user_option` AS A
                        WHERE A.PERID = $PERID_PERSON");
            if (mysqli_num_rows($queryMaster) > 0) {
                $QueryRemoveData = mysqli_query($this->db->hostDB, "
                DELETE FROM HRTIME_DB.`c_user_option`
                WHERE PERID = " . $PERID_PERSON);
                if ($QueryRemoveData) {
                    $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_user_option` (
                    `PERID`,
                    `Menu_Permiss`,
                    `Option_Permiss`,
                    `Create_By`,
                    `Update_By`,
                    `Create_Time`,
                    `Update_Time`
                )
                VALUES
                    $Query_Opt_Insert
                ");
                    if ($query) {
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                        }
                        echo json_encode($arr);
                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_user_option` (
                    `PERID`,
                    `Menu_Permiss`,
                    `Option_Permiss`,
                    `Create_By`,
                    `Update_By`,
                    `Create_Time`,
                    `Update_Time`
                )
                VALUES
                    $Query_Opt_Insert
                ");
                if ($query) {
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                    }
                    echo json_encode($arr);
                }
            }
        } else {
            return false;
        }
    }

    //เรียกมูลบุคลากรหน่วยงานที่เกี่ยวข้อง
    public function GETALLDATAPERSON2()
    {
        $myArray = array();
        if ($this->db->hostDB) {

            $queryPersonalOther = mysqli_query($this->db->hostDB, "
                        SELECT
                        B.PERID,
                        B.`NAME`,
                        B.SURNAME,
                        B.SEX,
                        B.TITLE,
                        B.NewPos,
                        D.Dep_name,
                        D.Dep_Group_name,
                        A.Dep_Code,
                        D.Edit_code,
                        C.PosName
                    FROM
                        HRTIME_DB.c_medperson AS A
                    INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                    INNER JOIN STAFF.Positions AS C ON B.NewPos = C.PosCode
                    INNER JOIN STAFF.Depart AS D ON D.Dep_Code = A.Dep_Code
                    WHERE
                        B.PERID = " . $_SESSION['PERID']);

            if (mysqli_num_rows($queryPersonalOther) > 0) {
                while ($DepData = mysqli_fetch_assoc($queryPersonalOther)) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.DEP_WORK,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Edit_code,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0
            AND STAFF.Depart.Dep_Code = " . $DepData['Dep_Code'] . "
            Limit 1");

                    if (mysqli_num_rows($query) > 0) {

                        while ($data = mysqli_fetch_assoc($query)) {
                            $query2 = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.DEP_WORK,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Edit_code,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0
            AND 
            STAFF.Depart.Edit_code = " . $data['Edit_code']);
                            if ($query2) {
                                while ($dataPerson = mysqli_fetch_assoc($query2)) {

                                    $myArray[] = $dataPerson;

                                }
                            } else {
                            }
                        }

                    } else {
                        $query2 = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.DEP_WORK,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Edit_code,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0
            AND 
            STAFF.Depart.Edit_code = " . $DepData['Edit_code']);
                        if ($query2) {
                            while ($dataPerson = mysqli_fetch_assoc($query2)) {

                                $myArray[] = $dataPerson;

                            }
                        }
                    }
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.DEP_WORK,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Edit_code,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0
            AND STAFF.Medperson.PERID = " . $_SESSION['PERID']);

                if (mysqli_num_rows($query) > 0) {

                    while ($data = mysqli_fetch_assoc($query)) {
                        $query2 = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.DEP_WORK,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
             STAFF.Depart.Dep_Code,
            STAFF.Depart.Edit_code,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0
            AND 
            STAFF.Depart.Edit_code = " . $data['Edit_code']);
                        if ($query2) {
                            while ($dataPerson = mysqli_fetch_assoc($query2)) {

                                $myArray[] = $dataPerson;

                            }
                        } else {
                        }
                    }

                }
            }
            return json_encode($myArray);
        } else {
            return false;
        }
    }

    public function GETALLDATAPERSON()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.DEP_WORK,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Edit_code,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0");

            if (mysqli_num_rows($query) > 0) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $myArray[] = $data;
                }
                $myJSON = json_encode($myArray);
            }

            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกมูลบุคลากรหน่วยงานที่เกี่ยวข้อง โดยอ้างอิงจาก Dep_code ที่กำหนด
    public function GETALLPERSONAL_REF()
    {
        $DepartArr = array();
        $DataPersonal = array();
        if ($this->db->hostDB) {

            $query = mysqli_query($this->db->hostDB, "SELECT
            *
            FROM HRTIME_DB.`c_admin_permiss`
            WHERE
            PERID = " . $_SESSION['PERID']
            );
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $myArray = array();
                    $queryPersonal = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
            STAFF.Depart.Dep_Group_name,
            STAFF.Depart.Dep_Code,
            STAFF.Depart.Edit_Code,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0
            AND 
            STAFF.Depart.Dep_Code = " . $data['Dep_Code'] . "
            ORDER BY STAFF.Depart.Edit_code ASC");
                    if ($queryPersonal) {
                        while ($dataPerson = mysqli_fetch_assoc($queryPersonal)) {

                            $myArray[] = $dataPerson;

                        }

                        $queryPersonalOther = mysqli_query($this->db->hostDB, "
                        SELECT
                        B.PERID,
                        B.`NAME`,
                        B.SURNAME,
                        B.SEX,
                        B.TITLE,
                        B.NewPos,
                        D.Dep_name,
                        D.Dep_Group_name,
                        D.Dep_Code,
                        D.Edit_code,
                        C.PosName
                    FROM
                        HRTIME_DB.c_medperson AS A
                    INNER JOIN STAFF.Medperson AS B ON A.PERID = B.PERID
                    INNER JOIN STAFF.Positions AS C ON B.NewPos = C.PosCode
                    INNER JOIN STAFF.Depart AS D ON B.DEP_WORK = D.Dep_Code
                    WHERE
                        A.Dep_Code = " . $data['Dep_Code']);

                        if ($queryPersonalOther) {
                            while ($dataPersonOhter = mysqli_fetch_assoc($queryPersonalOther)) {

                                array_push($myArray, $dataPersonOhter);

                            }
                        }

                        array_push($DataPersonal, array('Data' => $myArray));
                    } else {
                    }
                }
            } else {

            }

            $myJSON = json_encode($DataPersonal);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLDEPART_REF()
    {
        $DepartArr = array();
        if ($this->db->hostDB) {

            $query = mysqli_query($this->db->hostDB, "SELECT
                A.*,
                B.Dep_name,
                B.Dep_Group_name
                FROM
                HRTIME_DB.c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_Code
            WHERE
            PERID = " . $_SESSION['PERID']
            );
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $DepartArr[] = $data;
                }
            } else {

            }

            $myJSON = json_encode($DepartArr);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function SAVEDATAPERSONCHANGEDEP($PERID, $Dep_Code, $Dep_Code_Old, $PERID_CREATE)
    {
        if ($this->db->hostDB) {

            $CheckDataDupe = mysqli_query($this->db->hostDB, "
                SELECT
                A.*
                FROM
                HRTIME_DB.c_medperson AS A
                WHERE
                A.PERID = $PERID");

            if (mysqli_num_rows($CheckDataDupe) > 0) {
                $RemoveDataDupe = mysqli_query($this->db->hostDB, "
                DELETE
                FROM
                HRTIME_DB.c_medperson
                WHERE
                PERID = $PERID");

                if ($RemoveDataDupe) {
                    $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_medperson` (
                    `PERID`,
                    `Dep_Code`,
                    `Dep_Code_Old`,
                    `Create_By`,
                    `Create_Time`
                )
                VALUES
                    ('$PERID', '$Dep_Code', '$Dep_Code_Old', '$PERID_CREATE', CURRENT_TIME)
                ");

                    mysqli_query($this->db->hostDB, 'UPDATE A_transaction SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                    mysqli_query($this->db->hostDB, 'UPDATE c_take_a_leave SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                    mysqli_query($this->db->hostDB, 'UPDATE c_leave_set SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                    mysqli_query($this->db->hostDB, 'UPDATE c_person_time SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                    mysqli_query($this->db->hostDB, 'UPDATE c_comment SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);

                    if ($query) {
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                        }
                        echo json_encode($arr);
                    }
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                    echo json_encode($arr);
                }
            } else {

                mysqli_query($this->db->hostDB, 'UPDATE A_transaction SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                mysqli_query($this->db->hostDB, 'UPDATE c_take_a_leave SET Dep_Code  =' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                mysqli_query($this->db->hostDB, 'UPDATE c_leave_set SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                mysqli_query($this->db->hostDB, 'UPDATE c_person_time SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);
                mysqli_query($this->db->hostDB, 'UPDATE c_comment SET Dep_Code = ' . $Dep_Code . ' WHERE PERID = ' . $PERID);

                $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`c_medperson` (
                    `PERID`,
                    `Dep_Code`,
                    `Dep_Code_Old`,
                    `Create_By`,
                    `Create_Time`
                )
                VALUES
                    ('$PERID', '$Dep_Code', '$Dep_Code_Old', '$PERID_CREATE', CURRENT_TIME)
                ");
                if ($query) {
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                    }
                    echo json_encode($arr);
                }
            }
        } else {
            return false;
        }
    }

    public function GETDATAPERSONCHANGEDEP()
    {
        $GetPerson = array();
        if ($this->db->hostDB) {

            $query = mysqli_query($this->db->hostDB, "
                SELECT
                A.*,
                B.`NAME`,
                B.SURNAME,
                D.Edit_code AS Edit_code_Old,
                D.Dep_name AS Dep_name_Old,
                D.Dep_Group_name AS Dep_Group_name_Old,
                C.Edit_code,
                C.Dep_name,
                C.Dep_Group_name
                FROM
                HRTIME_DB.c_medperson AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = A.Dep_Code
                INNER JOIN STAFF.Depart AS D ON D.Dep_Code = A.Dep_Code_Old
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $GetPerson[] = $data;
                }
            } else {

            }

            $myJSON = json_encode($GetPerson);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLMENU_FUNC()
    {
        $Menu_Func = array();
        if ($this->db->hostDB) {

            $query = mysqli_query($this->db->hostDB, "
            SELECT * FROM HRTIME_DB.`c_permission`
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $Menu_Func[] = $data;
                }
            } else {

            }

            $myJSON = json_encode($Menu_Func);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETALLOPTION_FUNC()
    {
        $Option_Func = array();
        if ($this->db->hostDB) {

            $query = mysqli_query($this->db->hostDB, "
            SELECT * FROM HRTIME_DB.`c_option_permiss`
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {
                    $Option_Func[] = $data;
                }
            } else {

            }

            $myJSON = json_encode($Option_Func);
            return $myJSON;
        } else {
            return false;
        }
    }
}
