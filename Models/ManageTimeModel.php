<?php

class ManageTimeModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->d_transaction เงื่อนไข PERID != 0
    public function DateAllData()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, constant('Date_All_Data'));
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลคณะหน่วยงานจากฐานข้อมูลSTAFF->Depart
    public function GatAllDepartment()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลประตูจากฐานข้อมูลSTAFF->Depart
    public function GatAllDoor()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT * FROM `C_door` WHERE `door_status` = 1 AND zone_id != 5 AND zone_id != 9 ORDER BY door_name ASC");
            if (mysqli_num_rows($query) > 0) {
                array_push($myArray, array(
                    'dev_id' => "",
                    'dev_sn' => "",
                    'door_id' => 0,
                    'door_name' => "ประตูทั้งหมด",
                    'door_status' => 1,
                    'zone_id' => 1));
                while ($data = mysqli_fetch_assoc($query)) {
                    array_push($myArray, $data);
                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลการตั้งค่าเวลาหน่วยงานฐานข้อมูลHRTIME_DB->c_depart_time
    public function GET_SET_Depart($DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.*,
            B.Edit_code
            FROM
            HRTIME_DB.c_depart_time AS A
            INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
            WHERE
            B.Dep_Code = $DepCode");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // บันทึกและแก้ไขข้อมูลตั้งค่าหน่วยการปฏิบัติงานหน่วยงาน
    public function INSERTDepartTIME($Date, $TimeN, $TimeO, $TimeHN, $TimeHO, $Perid, $DepCode, $door_id)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");
        $dateResult = date("d-m-Y");
        $valueSql = '';
        if ((count($door_id) - 1) <= 2) {
            for ($i = 0; $i <= count($door_id) - 1; $i++) {
                if ($i == count($door_id) - 1) {
                    $valueSql .= "('" . $Date . "', '" . $TimeN . "', '" . $TimeO . "', '" . $TimeHN . "', '" . $TimeHO . "', '" . $Perid . "', '" . $Perid . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" . $DepCode . "', '" . $door_id[$i]->doorId . "')";
                } else {
                    $valueSql .= "('" . $Date . "', '" . $TimeN . "', '" . $TimeO . "', '" . $TimeHN . "', '" . $TimeHO . "', '" . $Perid . "', '" . $Perid . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" . $DepCode . "', '" . $door_id[$i]->doorId . "') ,";
                }
            }
            if ($Date < date("Y-m-d")) {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลย้อนหลังได้");
                echo json_encode($arr);
            } else {
                $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " . $_SESSION['PERID']
                );
                if ($queryPermiss) {
                    if (mysqli_num_rows($queryPermiss) >= 1) {
                        if ($this->db->hostDB) { //date("Y-m-d") == $Date อย่าลิมเงื่อนไขให้เช็ควัน กับห้ามอัพเดทย้อนหลัง
                            $query = mysqli_query($this->db->hostDB, "SELECT * FROM `c_depart_time` WHERE Dep_code = " . $DepCode . " AND D_Time_DateTime = '" . $Date . "'");

                            if ($query) {
                                if ($query->num_rows == 0) {
                                    $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_depart_time (D_Time_DateTime, D_Time_TimeN, D_Time_TimeO, D_Time_TimeHN, D_Time_TimeHO, D_Time_CreateBY, D_Time_UpdateBY, D_Time_CreateT, D_Time_UpdateT, Dep_code, door_id)
                VALUES $valueSql");
                                    if ($InsertQ) {
                                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                        echo json_encode($arr);
                                    } else {
                                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                        echo json_encode($arr);
                                    }
                                } else {

                                    if ((count($door_id) - 1) <= 2) {
                                        while ($data = mysqli_fetch_assoc($query)) {
                                            $DeleteQ = mysqli_query($this->db->hostDB, "DELETE FROM `c_depart_time`  WHERE D_Time_ID = " . $data['D_Time_ID']);
                                        }

                                        if ($DeleteQ) {
                                            $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_depart_time (D_Time_DateTime, D_Time_TimeN, D_Time_TimeO, D_Time_TimeHN, D_Time_TimeHO, D_Time_CreateBY, D_Time_UpdateBY, D_Time_CreateT, D_Time_UpdateT, Dep_code, door_id)
                VALUES $valueSql");

                                            if ($InsertQ) {
                                                $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                                echo json_encode($arr);
                                            } else {
                                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                                echo json_encode($arr);
                                            }
                                        } else {
                                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                            echo json_encode($arr);
                                        }
                                    } else {
                                        $arr = array('Status' => false, 'Message' => "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากจำกัดประตูหน่วยงานไว้ไม่เกินจำนวน 3 ประตู");
                                        echo json_encode($arr);
                                    }
                                }
                            } else {
                            }
                        } else {
                            return false;
                        }
                    } else {
                        $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '004_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                        );

                        if ($queryPermiss) {
                            if (mysqli_num_rows($queryPermiss) >= 1) {
                                if ($this->db->hostDB) { //date("Y-m-d") == $Date อย่าลิมเงื่อนไขให้เช็ควัน กับห้ามอัพเดทย้อนหลัง
                                    $query = mysqli_query($this->db->hostDB, "SELECT * FROM `c_depart_time` WHERE Dep_code = " . $DepCode . " AND D_Time_DateTime = '" . $Date . "'");

                                    if ($query) {
                                        if ($query->num_rows == 0) {
                                            $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_depart_time (D_Time_DateTime, D_Time_TimeN, D_Time_TimeO, D_Time_TimeHN, D_Time_TimeHO, D_Time_CreateBY, D_Time_UpdateBY, D_Time_CreateT, D_Time_UpdateT, Dep_code, door_id)
                VALUES $valueSql");
                                            if ($InsertQ) {
                                                $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                                echo json_encode($arr);
                                            } else {
                                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                                echo json_encode($arr);
                                            }
                                        } else {

                                            if ((count($door_id) - 1) <= 2) {
                                                while ($data = mysqli_fetch_assoc($query)) {
                                                    $DeleteQ = mysqli_query($this->db->hostDB, "DELETE FROM `c_depart_time`  WHERE D_Time_ID = " . $data['D_Time_ID']);
                                                }

                                                if ($DeleteQ) {
                                                    $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_depart_time (D_Time_DateTime, D_Time_TimeN, D_Time_TimeO, D_Time_TimeHN, D_Time_TimeHO, D_Time_CreateBY, D_Time_UpdateBY, D_Time_CreateT, D_Time_UpdateT, Dep_code, door_id)
                VALUES $valueSql");

                                                    if ($InsertQ) {
                                                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                                        echo json_encode($arr);
                                                    } else {
                                                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                                        echo json_encode($arr);
                                                    }
                                                } else {
                                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                                    echo json_encode($arr);
                                                }
                                            } else {
                                                $arr = array('Status' => false, 'Message' => "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากจำกัดประตูหน่วยงานไว้ไม่เกินจำนวน 3 ประตู");
                                                echo json_encode($arr);
                                            }
                                        }
                                    } else {
                                    }
                                } else {
                                    return false;
                                }
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่ได้รับสิทธิในการแก้ไขข้อมูล");
                                echo json_encode($arr);
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่ได้รับสิทธิในการแก้ไขข้อมูล");
                            echo json_encode($arr);
                        }
                    }
                }
            }
        } else {
            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลประตูหน่วยงานได้มากกว่า 3 ประตู");
            echo json_encode($arr);
        }
    }

    //จัดการข้อมูลเวลาของบุคลาการ
    public function INSERTPERSONTIME($TimeN, $TimeO, $TimeHN, $TimeHO, $DoorID, $Perid, $DepCode)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");
        $dateResult = date("d-m-Y");
        $valueSql = '';
        if ((count($DoorID) - 1) <= 3) {
            for ($i = 0; $i <= count($DoorID) - 1; $i++) {
                if ($i == count($DoorID) - 1) {
                    $valueSql .= "(NULL, '" . $Perid . "', '" . $DepCode . "', '" . $dateNow . "', '" . $TimeN . "', '" . $TimeO . "', '" . $TimeHN . "', '" . $TimeHO . "', '" . $DoorID[$i]->doorId . "', '" . $Perid . "', '" . $Perid . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
                } else {
                    $valueSql .= "(NULL, '" . $Perid . "', '" . $DepCode . "', '" . $dateNow . "', '" . $TimeN . "', '" . $TimeO . "', '" . $TimeHN . "', '" . $TimeHO . "', '" . $DoorID[$i]->doorId . "', '" . $Perid . "', '" . $Perid . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ,";
                }
            }
            if ($this->db->hostDB) { //date("Y-m-d") == $Date อย่าลิมเงื่อนไขให้เช็ควัน กับห้ามอัพเดทย้อนหลัง

                $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " . $_SESSION['PERID']
                );
                if ($queryPermiss) {
                    if (mysqli_num_rows($queryPermiss) >= 1) {

                        $query = mysqli_query($this->db->hostDB, "SELECT * FROM `c_person_time` WHERE PERID = " . $Perid . " AND P_Time_DateTime = '" . $dateNow . "' ORDER BY P_Time_DateTime");
                        if ($query) {
                            if ($query->num_rows == 0) {
                                $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_person_time (P_Time_ID, PERID, Dep_code, P_Time_DateTime, P_Time_TimeN, P_Time_TimeO, P_Time_TimeHN , P_Time_TimeHO, P_DoorID , P_Time_CreateBY, P_Time_UpdateBY, P_Time_CreateT, P_Time_UpdateT)
            VALUES $valueSql");
                                if ($InsertQ) {
                                    $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                    echo json_encode($arr);
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                    echo json_encode($arr);
                                }
                            } else {
                                if ((count($DoorID) - 1) <= 2) {
                                    while ($data = mysqli_fetch_array($query)) {
                                        $DeleteData = mysqli_query($this->db->hostDB, "DELETE FROM c_person_time WHERE P_Time_ID = " . $data['P_Time_ID']);
                                    }

                                    if ($DeleteData) {
                                        $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_person_time (P_Time_ID, PERID, Dep_code, P_Time_DateTime, P_Time_TimeN, P_Time_TimeO, P_Time_TimeHN , P_Time_TimeHO, P_DoorID , P_Time_CreateBY, P_Time_UpdateBY, P_Time_CreateT, P_Time_UpdateT)
            VALUES $valueSql");
                                        if ($InsertQ) {
                                            $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                            echo json_encode($arr);
                                        } else {
                                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                            echo json_encode($arr);
                                        }
                                    }
                                } else {
                                    $arr = array('Status' => false, 'Message' => "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากจำกัดประตูหน่วยงานไว้ไม่เกินจำนวน 3 ประตู");
                                    echo json_encode($arr);
                                }
                            }
                        }

                    } else {
                        $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '004_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                        );

                        if ($queryPermiss) {
                            if (mysqli_num_rows($queryPermiss) >= 1) {
                                $query = mysqli_query($this->db->hostDB, "SELECT * FROM `c_person_time` WHERE PERID = " . $Perid . " AND P_Time_DateTime = '" . $dateNow . "' ORDER BY P_Time_DateTime");
                                if ($query) {
                                    if ($query->num_rows == 0) {
                                        $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_person_time (P_Time_ID, PERID, Dep_code, P_Time_DateTime, P_Time_TimeN, P_Time_TimeO, P_Time_TimeHN , P_Time_TimeHO, P_DoorID , P_Time_CreateBY, P_Time_UpdateBY, P_Time_CreateT, P_Time_UpdateT)
            VALUES $valueSql");
                                        if ($InsertQ) {
                                            $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                            echo json_encode($arr);
                                        } else {
                                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                            echo json_encode($arr);
                                        }
                                    } else {
                                        if ((count($DoorID) - 1) <= 2) {
                                            while ($data = mysqli_fetch_array($query)) {
                                                $DeleteData = mysqli_query($this->db->hostDB, "DELETE FROM c_person_time WHERE P_Time_ID = " . $data['P_Time_ID']);
                                            }

                                            if ($DeleteData) {
                                                $InsertQ = mysqli_query($this->db->hostDB, "INSERT INTO c_person_time (P_Time_ID, PERID, Dep_code, P_Time_DateTime, P_Time_TimeN, P_Time_TimeO, P_Time_TimeHN , P_Time_TimeHO, P_DoorID , P_Time_CreateBY, P_Time_UpdateBY, P_Time_CreateT, P_Time_UpdateT)
            VALUES $valueSql");
                                                if ($InsertQ) {
                                                    $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ บันทึกข้อมูลใหม่วันที่ : " . $dateResult);
                                                    echo json_encode($arr);
                                                } else {
                                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมููลได้");
                                                    echo json_encode($arr);
                                                }
                                            }
                                        } else {
                                            $arr = array('Status' => false, 'Message' => "ไม่สามารถบันทึกข้อมูลได้ เนื่องจากจำกัดประตูหน่วยงานไว้ไม่เกินจำนวน 3 ประตู");
                                            echo json_encode($arr);
                                        }
                                    }
                                }
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่ได้รับสิทธิในการแก้ไขข้อมูล");
                                echo json_encode($arr);
                            }
                        }
                    }
                }
            } else {
                return false;
            }
        } else {
            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลประตูหน่วยงานได้มากกว่า 3 ประตู");
            echo json_encode($arr);
        }
    }

    //เรียกข้อมูลบุคลากรของหน่วยงานแต่ล่ะหน่วยงาน
    public function GETPERSONDEP($DepCode)
    {
        $myArray = array();
        $dateNow = date("Y-m-d");

        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            S. NAME,
            S.SURNAME,
            S.PERID,
            S.DEP_WORK,
            D.Dep_name,
            D.Edit_code,
            P_Time_DateTime,
            P_Time_TimeN,
            P_Time_TimeO,
            P_Time_TimeHN,
            P_Time_TimeHO,
            P_DoorID,
            P_Time_UpdateBY,
            P_Time_UpdateT,
            door_name
        FROM
            STAFF.Medperson AS S
        LEFT JOIN (
            SELECT
                c1.*, c3.door_name
            FROM
                HRTIME_DB.c_person_time c1
            LEFT JOIN HRTIME_DB.c_person_time c2 ON (
                c1.PERID = c2.PERID
                AND c1.P_Time_DateTime < c2.P_Time_DateTime
            )
            LEFT JOIN HRTIME_DB.C_door c3 ON c3.door_id = c1.P_DoorID
            WHERE
                c2.P_Time_DateTime IS NULL
        ) AS H ON S.PERID = H.PERID
        LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
        WHERE
            D.Dep_Code = " . $DepCode . "
        AND S.CSTATUS != 0
        ORDER BY
            S.PERID ASC");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }

                $queryPersonDepOther = mysqli_query($this->db->hostDB, "SELECT
                    S. NAME,
                    S.SURNAME,
                    S.PERID,
                    S.DEP_WORK,
                    D.Dep_name,
                    D.Edit_code,
                    P_Time_DateTime,
                    P_Time_TimeN,
                    P_Time_TimeO,
                    P_Time_TimeHN,
                    P_Time_TimeHO,
                    P_DoorID,
                    P_Time_UpdateBY,
                    P_Time_UpdateT,
                    door_name
                FROM
                	HRTIME_DB.c_medperson AS A
                LEFT JOIN STAFF.Medperson AS S ON A.PERID = S.PERID
                LEFT JOIN (
                    SELECT
                        c1.*, c3.door_name
                    FROM
                        HRTIME_DB.c_person_time c1
                    LEFT JOIN HRTIME_DB.c_person_time c2 ON (
                        c1.PERID = c2.PERID
                        AND c1.P_Time_DateTime < c2.P_Time_DateTime
                    )
                    LEFT JOIN HRTIME_DB.C_door c3 ON c3.door_id = c1.P_DoorID
                    WHERE
                        c2.P_Time_DateTime IS NULL
                ) AS H ON S.PERID = H.PERID
                LEFT JOIN STAFF.Depart AS D ON S.DEP_WORK = D.Dep_Code
                WHERE
                    A.Dep_Code = " . $DepCode . "
                AND S.CSTATUS != 0
                ORDER BY
                    S.PERID ASC");

                if ($queryPersonDepOther) {
                    while ($data = mysqli_fetch_assoc($queryPersonDepOther)) {
                        array_push($myArray, $data);
                    }
                }

            } else {
            }
            // echo array_splice($myArray, 1 , 0,array('test' => 'test'));
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // SELECT
    //         HRTIME_DB.c_person_time.P_Time_DateTime,
    //         HRTIME_DB.c_person_time.P_Time_TimeN,
    //         HRTIME_DB.c_person_time.P_Time_TimeO,
    //         HRTIME_DB.c_person_time.P_Time_TimeHN,
    //         HRTIME_DB.c_person_time.P_Time_TimeHO,
    //         HRTIME_DB.c_person_time.P_Time_UpdateBY,
    //         HRTIME_DB.c_person_time.P_Time_UpdateT,
    //         STAFF.Medperson. NAME,
    //         STAFF.Medperson.SURNAME,
    //         STAFF.Medperson.PERID,
    //         STAFF.Medperson.DEP_WORK,
    //         STAFF.Depart.Dep_name
    //     FROM
    //         HRTIME_DB.c_person_time
    //     JOIN STAFF.Medperson ON HRTIME_DB.c_person_time.PERID = STAFF.Medperson.PERID
    //     JOIN STAFF.Depart ON HRTIME_DB.c_person_time.Dep_code = STAFF.Depart.Dep_Code
    //     WHERE
    //         STAFF.Medperson.DEP_WORK = ".$DepCode."
    //     AND STAFF.Medperson.CSTATUS != 0
    //     ORDER BY
    //         PERID ASC

}
