<?php
class MonthSummaryModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GatAllDepartment()
    {
        $myArray = array();
        $myDepart = array();
        $DataDepart = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                    B.Dep_Code,
                    B.Edit_code,
                    B.Dep_name,
                    B.Dep_Group_name,
                    B.Telephone,
                    B.Doc_In,
                    B.Doc_Out,
                    B.Doc_go,
                    B.Doc_in2,
                    B.Dep_status
                    FROM
                    c_user_permiss AS A
                    INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                    WHERE
                    A.Perid = $PERID
                    ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                        A.Dep_Code,
                        A.Dep_name,
                        A.Dep_Group_name
                        FROM
                        STAFF.Depart AS A
                        INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                        WHERE
                        B.PERID = $PERID
                        ORDER BY
                        A.Dep_name ASC
                        ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                        array_push($DataDepart, array('DepartAdmin' => array(), 'DepartUser' => $myArray));
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $myArray[] = $data;
                    }
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Edit_code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query->num_rows > 0) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myDepart[] = $data;

                        }
                    } else {

                    }

                    array_push($DataDepart, array('DepartAdmin' => $myArray, 'DepartUser' => $myDepart));
                    // while ($data = mysqli_fetch_assoc($query)) {

                    //     $myArray[] = $data;

                    // }
                }
            } else {

                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                    array_push($DataDepart, array('DepartAdmin' => $myArray, 'DepartUser' => $myDepart));
                } else {
                }
            }
            $myJSON = json_encode($DataDepart);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETREPORTWORKBYPERSONAL($Dep_Code, $Date_Cordi, $Date_Cordi2, $PERID)
    {
        $WorkLateArr = array();
        $CommentArr = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                A_transaction
                WHERE
                A_transaction.Dep_code = $Dep_Code
                AND
                PERID = $PERID
                AND
                A_transaction.A_Datetime
                BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ORDER BY A_transaction.A_Datetime DESC
                ");
            if (mysqli_num_rows($query) > 0) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $WorkLateArr[] = $data;

                }

                $queryComment = mysqli_query($this->db->hostDB, "SELECT
                A.Comment_ID,
                A.Comment_Details,
                A.Comment_Date,
                A.PERID,
                A.Dep_Code,
                A.Create_BY,
                A.Create_T,
                B.`NAME`,
                B.SURNAME,
                STAFF.Depart.Dep_name,
                STAFF.Depart.Dep_Group_name,
                STAFF.Depart.Edit_code
            FROM
                HRTIME_DB.c_comment AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
                WHERE
                A.PERID = $PERID
                AND STAFF.Depart.Edit_code = $Dep_Code
                AND A.Comment_Date
                BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                    ");

                if ($queryComment) {
                    while ($CommentData = mysqli_fetch_assoc($queryComment)) {

                        $CommentArr[] = $CommentData;

                    }
                }

            } else {
                $queryPerson = mysqli_query($this->db->hostDB, "SELECT
                A.`NAME`,
                A.SURNAME,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Edit_code
            FROM
                STAFF.Medperson AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
                WHERE
                A.PERID = $PERID");
                
                if ($queryPerson) {
                    while ($dataDep_Code = mysqli_fetch_assoc($queryPerson)) {
                        $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                A_transaction
                WHERE
                A_transaction.Dep_code = " . $dataDep_Code['Dep_Code'] . "
                AND
                PERID = $PERID
                AND
                A_transaction.A_Datetime
                BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ORDER BY A_transaction.A_Datetime DESC
                ");
                        if ($query) {
                            while ($data = mysqli_fetch_assoc($query)) {

                                $WorkLateArr[] = $data;

                            }

                            $queryComment = mysqli_query($this->db->hostDB, "SELECT
                A.Comment_ID,
                A.Comment_Details,
                A.Comment_Date,
                A.PERID,
                A.Dep_Code,
                A.Create_BY,
                A.Create_T,
                B.`NAME`,
                B.SURNAME,
                STAFF.Depart.Dep_name,
                STAFF.Depart.Dep_Group_name,
                STAFF.Depart.Edit_code
            FROM
                HRTIME_DB.c_comment AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = A.Dep_Code
                WHERE
                A.PERID = $PERID
                AND STAFF.Depart.Edit_code = " . $dataDep_Code['Dep_Code'] . "
                AND A.Comment_Date
                BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                    ");

                            if ($queryComment) {
                                while ($CommentData = mysqli_fetch_assoc($queryComment)) {

                                    $CommentArr[] = $CommentData;

                                }
                            }

                        }
                    }
                }
            }
            $myJSON = json_encode(['Work_Late' => $WorkLateArr, 'Comment' => $CommentArr]);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETREPORTLEAVEBYPERSONAL($Dep_Code, $Date_Cordi, $Date_Cordi2, $PERID)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            c_take_a_leave
        WHERE
            c_take_a_leave.Dep_code = $Dep_Code
        AND
            PERID = $PERID
        AND T_Leave_Date_End >= '$Date_Cordi'
        AND T_Leave_Date_End <= '$Date_Cordi2'
        AND T_Status_Leave = '1'
        UNION
        SELECT
            *
        FROM
            c_take_a_leave
        WHERE
            c_take_a_leave.Dep_code = $Dep_Code
        AND
            PERID = $PERID
        AND T_Leave_Date_Start >= '$Date_Cordi'
        AND T_Leave_Date_Start <= '$Date_Cordi2'
        AND T_Status_Leave = '1'
            ");
            if (mysqli_num_rows($query) == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                *
            FROM
                c_take_a_leave
            WHERE
                c_take_a_leave.Dep_code = $Dep_Code
            AND
            PERID = $PERID
            AND T_Leave_Date_End = '$Date_Cordi'
            AND T_Status_Leave = '1'
            ");
                if(mysqli_num_rows($query) > 0){
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                    $myJSON = json_encode($myArray);
                } else {
                    $queryPerson = mysqli_query($this->db->hostDB, "SELECT
                A.`NAME`,
                A.SURNAME,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Edit_code
            FROM
                STAFF.Medperson AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
                WHERE
                A.PERID = $PERID");

                    if ($queryPerson) {
                        while ($dataDep_Code = mysqli_fetch_assoc($queryPerson)) {
                            $query = mysqli_query($this->db->hostDB, "SELECT
            *
        FROM
            c_take_a_leave
        WHERE
            c_take_a_leave.Dep_code = " . $dataDep_Code['Dep_Code'] . "
        AND
            PERID = $PERID
        AND T_Leave_Date_End >= '$Date_Cordi'
        AND T_Leave_Date_End <= '$Date_Cordi2'
        AND T_Status_Leave = '1'
        UNION
        SELECT
            *
        FROM
            c_take_a_leave
        WHERE
            c_take_a_leave.Dep_code = " . $dataDep_Code['Dep_Code'] . "
        AND
            PERID = $PERID
        AND T_Leave_Date_Start >= '$Date_Cordi'
        AND T_Leave_Date_Start <= '$Date_Cordi2'
        AND T_Status_Leave = '1'
            ");
                            if ($query->num_rows == 0) {
                                $query = mysqli_query($this->db->hostDB, "SELECT
                *
            FROM
                c_take_a_leave
            WHERE
                c_take_a_leave.Dep_code = " . $dataDep_Code['Dep_Code'] . "
            AND
            PERID = $PERID
            AND T_Leave_Date_End = '$Date_Cordi'
            AND T_Status_Leave = '1'
            ");
                                if ($query) {
                                    while ($data = mysqli_fetch_assoc($query)) {

                                        $myArray[] = $data;

                                    }
                                } else {
                                }
                                $myJSON = json_encode($myArray);
                            } else {
                                if ($query) {
                                    while ($data = mysqli_fetch_assoc($query)) {

                                        $myArray[] = $data;

                                    }
                                } else {
                                }
                                $myJSON = json_encode($myArray);
                            }
                        }
                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
                $myJSON = json_encode($myArray);
            }
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลบุคลากรภายในหน่วยงาน
    public function GETPERSONFROMDEPART($DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                A.`NAME`,
                A.SURNAME,
                A.PERID,
                A.POS_LEVEL,
                A.Date_In,
                B.Dep_name,
                B.Dep_Code,
                C.PosName
                FROM
                STAFF.Medperson AS A
                LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
                LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
                WHERE
                B.Dep_Code = " . $DepCode . " AND
                A.CSTATUS != 0
                ORDER BY
                A.PERID ASC
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    array_push($myArray, $data);

                }

                $queryPersonDepOther = mysqli_query($this->db->hostDB, "SELECT
                A.`NAME`,
                A.SURNAME,
                A.PERID,
                A.POS_LEVEL,
                A.Date_In,
                B.Dep_name,
                B.Dep_Code,
                C.PosName
                FROM HRTIME_DB.c_medperson AS D
                LEFT JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
                LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
                LEFT JOIN STAFF.Positions AS C ON C.PosCode = A.NewPos
                WHERE
                D.Dep_Code = " . $DepCode . " AND
                A.CSTATUS != 0
                ORDER BY
                A.PERID ASC
                ");

                if ($queryPersonDepOther) {
                    while ($data = mysqli_fetch_assoc($queryPersonDepOther)) {

                        array_push($myArray, $data);

                    }
                }

            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                        *
                        FROM
                        D_holiday
                        ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    public function GETDOCTORSUMMARYBY_P($Dep_Code, $Date_Cordi, $Date_Cordi2, $PERID){
        $myArray = array();
        if ($this->db->hostDB) {
                $query = mysqli_query($this->db->hostDB, "
                SELECT
                A.`no`,
                A.perid,
                A.day_of_month,
                A.count,
                B.`NAME`,
                B.SURNAME,
                C.Dep_Code,
                C.Edit_code,
                C.Dep_name,
                C.Dep_Group_name
                FROM
                HRTIME_DB.doctor_summary AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.perid
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                WHERE
                B.PERID = " . $PERID . " AND
                C.Dep_Code = " . $Dep_Code . " AND
                A.day_of_month BETWEEN '$Date_Cordi' AND '$Date_Cordi2'
                ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

//                        $myArray[] = $data;
                        array_push($myArray, $data);

                    }
                } else {
                }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }
}
