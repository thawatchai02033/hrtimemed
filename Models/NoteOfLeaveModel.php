<?php

class NoteOfLeaveModel extends Model
{

    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function GETALLDEPARTMENT()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกมูลใบลาทั้งหมดภายในหน่วยงาน
    public function GETALLDATANOTEOFLEAVE($DepCode)
    {

        $myArray = array();
        $strDepQuery = '';
        $arr = [];
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {

            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {

                } else {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $queryLeaveData = mysqli_query($this->db->hostDB, "SELECT
                A.*,
                B.`NAME`,
                B.SURNAME,
                B.SEX,
                B.TITLE,
                C.Dep_Code,
                C.Dep_name,
                C.Dep_Group_name,
                D.Leave_Detail,
                E.PosName
                FROM
                HRTIME_DB.c_take_a_leave AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                WHERE
                A.Dep_Code = " . $data['Dep_Code'] . "
                AND
                A.T_Status_Leave = 0
                ORDER BY
                A.No_Id DESC");
                        if ($query) {
                            while ($Leavedata = mysqli_fetch_assoc($queryLeaveData)) {
                                $myArray[] = $Leavedata;
                            }
                            $CheckPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*
                                FROM
                                HRTIME_DB.c_medperson AS A
                                WHERE
                                A.PERID = " . $PERID);

                            if (mysqli_num_rows($CheckPersonalOther) > 0) {
                                while ($CheckPerson_O = mysqli_fetch_assoc($CheckPersonalOther)) {
                                    $QueryPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*,
                                B.`NAME`,
                                B.SURNAME,
                                B.SEX,
                                B.TITLE,
                                C.Dep_Code,
                                C.Dep_name,
                                C.Dep_Group_name,
                                D.Leave_Detail,
                                E.PosName
                                FROM
                                HRTIME_DB.c_medperson AS F 
                                INNER JOIN HRTIME_DB.c_take_a_leave AS A ON A.PERID = F.PERID
                                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                                WHERE
                                F.Dep_code = " . $CheckPerson_O['Dep_Code'] . " 
                                AND
                                A.T_Status_Leave = 0
                                ORDER BY
                                A.No_Id DESC");

                                    if (mysqli_num_rows($QueryPersonalOther) > 0) {
                                        while ($QueryCheckPerson_O = mysqli_fetch_assoc($QueryPersonalOther)) {
                                            array_push($myArray, $QueryCheckPerson_O);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $queryLeaveData = mysqli_query($this->db->hostDB, "SELECT
                A.*,
                B.`NAME`,
                B.SURNAME,
                B.SEX,
                B.TITLE,
                C.Dep_Code,
                C.Dep_name,
                C.Dep_Group_name,
                D.Leave_Detail,
                E.PosName
                FROM
                HRTIME_DB.c_take_a_leave AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                WHERE
                A.Dep_Code = " . $data['Dep_Code'] . "
                AND
                A.T_Status_Leave = 0
                ORDER BY
                A.No_Id DESC");
                        if ($queryLeaveData) {
                            while ($Leavedata = mysqli_fetch_assoc($queryLeaveData)) {
                                $myArray[] = $Leavedata;
                            }

                            $CheckPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*
                                FROM
                                HRTIME_DB.c_medperson AS A
                                WHERE
                                A.PERID = " . $PERID);

                            if (mysqli_num_rows($CheckPersonalOther) > 0) {
                                while ($CheckPerson_O = mysqli_fetch_assoc($CheckPersonalOther)) {
                                    $QueryPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*,
                                B.`NAME`,
                                B.SURNAME,
                                B.SEX,
                                B.TITLE,
                                C.Dep_Code,
                                C.Dep_name,
                                C.Dep_Group_name,
                                D.Leave_Detail,
                                E.PosName
                                FROM
                                HRTIME_DB.c_medperson AS F 
                                INNER JOIN HRTIME_DB.c_take_a_leave AS A ON A.PERID = F.PERID
                                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                                WHERE
                                F.Dep_code = " . $CheckPerson_O['Dep_Code'] . " 
                                AND
                                A.T_Status_Leave = 0
                                ORDER BY
                                A.No_Id DESC");

                                    if (mysqli_num_rows($QueryPersonalOther) > 0) {
                                        while ($QueryCheckPerson_O = mysqli_fetch_assoc($QueryPersonalOther)) {
                                            array_push($myArray, $QueryCheckPerson_O);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลชนิดของการลา
    public function GETLEAVETYPE()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave.Leave_Type,
            HRTIME_DB.c_leave.Leave_Detail
            FROM
            HRTIME_DB.c_leave
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกข้อมูลประเภทวันลา
    public function GETDAYTYPE()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave_day.L_Day_Type,
            HRTIME_DB.c_leave_day.L_Day_Detail
            FROM
            HRTIME_DB.c_leave_day
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลประเภทวันลา
    public function SELECTLEAVEPERSON($PerId)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            HRTIME_DB.c_leave_day.L_Day_Type,
            HRTIME_DB.c_leave_day.L_Day_Detail
            FROM
            HRTIME_DB.c_leave_day
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //แก้ไขผลการอนุมติใบลา
    public function UPDATEDATANOTEOFLEAVE($No_Id, $Status, $PerId, $DayOff, $Dep_Code)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $Dep_Code
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    if ($DayOff == '0') {
                        $querySelectData = mysqli_query($this->db->hostDB, "SELECT * FROM c_take_a_leave
    WHERE No_Id = " . $No_Id);
                        if (mysqli_num_rows($querySelectData) >= 1) {
                            $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = '" . $Status . "',
                T_Work_Summary = 0,
                T_Leave_UpdateBY = '" . $PerId . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $No_Id);
                            if ($query) {
                                $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถพิจารณาใบลาได้ เนื่องจากไม่พบข้อมูลในระบบ");
                        }
                    } else {
                        $querySelectData = mysqli_query($this->db->hostDB, "SELECT * FROM c_take_a_leave
    WHERE No_Id = " . $No_Id);
                        if (mysqli_num_rows($querySelectData) >= 1) {
                            $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = '" . $Status . "',
                T_Leave_UpdateBY = '" . $PerId . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $No_Id);
                            if ($query) {
                                $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถพิจารณาใบลาได้ เนื่องจากไม่พบข้อมูลในระบบ");
                        }
                    }
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $Dep_Code
                    AND
                    D.Opt_C_Code = '006_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            if ($DayOff == '0') {
                                $querySelectData = mysqli_query($this->db->hostDB, "SELECT * FROM c_take_a_leave
    WHERE No_Id = " . $No_Id);
                                if (mysqli_num_rows($querySelectData) >= 1) {
                                    $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = '" . $Status . "',
                T_Work_Summary = 0,
                T_Leave_UpdateBY = '" . $PerId . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $No_Id);
                                    if ($query) {
                                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                                    } else {
                                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                                    }
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถพิจารณาใบลาได้ เนื่องจากไม่พบข้อมูลในระบบ");
                                }
                            } else {
                                $querySelectData = mysqli_query($this->db->hostDB, "SELECT * FROM c_take_a_leave
    WHERE No_Id = " . $No_Id);
                                if (mysqli_num_rows($querySelectData) >= 1) {
                                    $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = '" . $Status . "',
                T_Leave_UpdateBY = '" . $PerId . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $No_Id);
                                    if ($query) {
                                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                                    } else {
                                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                                    }
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถพิจารณาใบลาได้ เนื่องจากไม่พบข้อมูลในระบบ");
                                }
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้");
                        }
                    }
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    //แก้ไขผลการอนุมติใบลาแบบติ๊กเลือกอนุมัติ
    // ปลดล็อคเงื่อนไข DepCode ออก 27/08/63
    public function LISTUPDATEDATANOTEOFLEAVE($Data_Consider, $PerId_Update, $DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    foreach ($Data_Consider as $key => $value) {
                        if ($value->IsChecked == '1') {
                            if ($value->DayOff == '0') {
                                $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = 1,
                T_Work_Summary = 0,
                T_Leave_UpdateBY = '" . $PerId_Update . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $value->No_Id);
                            } else {
                                $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = 1,
                T_Leave_UpdateBY = '" . $PerId_Update . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $value->No_Id);
                            }
                        }
                    }
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                    }
                    echo json_encode($arr);
                } else {
//                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
//                    A.*,
//                    C.Permiss_ID,
//                    C.Permiss_Details,
//                    D.Opt_C_Code,
//                    D.Opt_C_Details
//                    FROM
//                    c_user_permiss AS A
//                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
//                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
//                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
//                    WHERE
//                    A.Dep_Code = $DepCode
//                    AND
//                    D.Opt_C_Code = '006_A'
//                    AND
//                    A.PERID = " . $_SESSION['PERID']
//                    );
//                    if ($queryPermiss) {
//                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            foreach ($Data_Consider as $key => $value) {
                                if ($value->IsChecked == '1') {
                                    if ($value->DayOff == '0') {
                                        $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = 1,
                T_Work_Summary = 0,
                T_Leave_UpdateBY = '" . $PerId_Update . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $value->No_Id);
                                    } else {
                                        $query = mysqli_query($this->db->hostDB, "UPDATE c_take_a_leave
                SET T_Status_Leave = 1,
                T_Leave_UpdateBY = '" . $PerId_Update . "',
                T_Leave_UpdateT = CURRENT_TIMESTAMP
                WHERE No_Id = " . $value->No_Id);
                                    }
                                }
                            }
                            if ($query) {
                                $arr = array('Status' => true, 'Message' => "บันทึกข้อมูลสำเร็จ");
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้");
                            }
                            echo json_encode($arr);
//                        } else {
//                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
//                        }
//                    } else {
//                        echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
//                    }
                }
            }
        } else {
            return false;
        }
    }

    public function DELETEDATA($No_Id, $DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                    }
                    echo json_encode($arr);
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '006_B'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
                            if ($query) {
                                $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                            }
                            echo json_encode($arr);
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                        }
                    } else {
                        echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                    }
                }
            }
        } else {
            return false;
        }
    }

    // ลบใบลาบุคลากรในหน่วยงาน
    public function DELETEDATANOTEOFLEAVE($No_Id)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
            if ($query) {
                $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    // เรียกมูลใบลาทั้งหมดโดยการนับ count
    public function GETCOUNTDATANOTEOFLEAVE($DepCode)
    {
        $strDepQuery = '';
        $arr = [];
        $PERID = $_SESSION['PERID'];
        $countData = 0;
        if ($this->db->hostDB) {

            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {

                } else {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $queryLeaveData = mysqli_query($this->db->hostDB, "SELECT
                A.*,
                B.`NAME`,
                B.SURNAME,
                B.SEX,
                B.TITLE,
                C.Dep_Code,
                C.Dep_name,
                C.Dep_Group_name,
                D.Leave_Detail,
                E.PosName
                FROM
                HRTIME_DB.c_take_a_leave AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                WHERE
                A.Dep_Code = " . $data['Dep_Code'] . "
                AND
                A.T_Status_Leave = 0
                ORDER BY
                A.No_Id DESC");
                        if ($queryLeaveData) {
                            $countData += mysqli_num_rows($queryLeaveData);

                            $CheckPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*
                                FROM
                                HRTIME_DB.c_medperson AS A
                                WHERE
                                A.PERID = " . $PERID);

                            if (mysqli_num_rows($CheckPersonalOther) > 0) {
                                while ($CheckPerson_O = mysqli_fetch_assoc($CheckPersonalOther)) {
                                    $QueryPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*,
                                B.`NAME`,
                                B.SURNAME,
                                B.SEX,
                                B.TITLE,
                                C.Dep_Code,
                                C.Dep_name,
                                C.Dep_Group_name,
                                D.Leave_Detail,
                                E.PosName
                                FROM
                                HRTIME_DB.c_medperson AS F 
                                INNER JOIN HRTIME_DB.c_take_a_leave AS A ON A.PERID = F.PERID
                                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                                WHERE
                                F.Dep_code = " . $CheckPerson_O['Dep_Code'] . " 
                                AND
                                A.T_Status_Leave = 0
                                ORDER BY
                                A.No_Id DESC");

                                    if (mysqli_num_rows($QueryPersonalOther) > 0) {
                                        $countData += mysqli_num_rows($QueryPersonalOther);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {
                        $queryLeaveData = mysqli_query($this->db->hostDB, "SELECT
                A.*,
                B.`NAME`,
                B.SURNAME,
                B.SEX,
                B.TITLE,
                C.Dep_Code,
                C.Dep_name,
                C.Dep_Group_name,
                D.Leave_Detail,
                E.PosName
                FROM
                HRTIME_DB.c_take_a_leave AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                WHERE
                A.Dep_Code = " . $data['Dep_Code'] . "
                AND
                A.T_Status_Leave = 0
                ORDER BY
                A.No_Id DESC");
                        if ($queryLeaveData) {
                            $countData += mysqli_num_rows($queryLeaveData);

                            $CheckPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*
                                FROM
                                HRTIME_DB.c_medperson AS A
                                WHERE
                                A.PERID = " . $PERID);

                            if (mysqli_num_rows($CheckPersonalOther) > 0) {
                                while ($CheckPerson_O = mysqli_fetch_assoc($CheckPersonalOther)) {
                                    $QueryPersonalOther = mysqli_query($this->db->hostDB, "
                                SELECT
                                A.*,
                                B.`NAME`,
                                B.SURNAME,
                                B.SEX,
                                B.TITLE,
                                C.Dep_Code,
                                C.Dep_name,
                                C.Dep_Group_name,
                                D.Leave_Detail,
                                E.PosName
                                FROM
                                HRTIME_DB.c_medperson AS F 
                                INNER JOIN HRTIME_DB.c_take_a_leave AS A ON A.PERID = F.PERID
                                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.PERID
                                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                                INNER JOIN HRTIME_DB.c_leave AS D ON D.Leave_Type = A.T_Leave_Type
                                INNER JOIN STAFF.Positions AS E ON E.PosCode = B.NewPos
                                WHERE
                                F.Dep_code = " . $CheckPerson_O['Dep_Code'] . " 
                                AND
                                A.T_Status_Leave = 0
                                ORDER BY
                                A.No_Id DESC");

                                    if (mysqli_num_rows($QueryPersonalOther) > 0) {
                                        $countData += mysqli_num_rows($QueryPersonalOther);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $arr = array('Status' => true, 'Message' => $countData);
            $myJSON = json_encode($arr);
            echo $myJSON;
        } else {
            return false;
        }
    }

    //เรียกมูลบุคลากรหน่วยงานที่เกี่ยวข้อง
    public function GETALLDATAPERSON()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            Medperson.PERID,
            Medperson.`NAME`,
            Medperson.SURNAME,
            Medperson.SEX,
            Medperson.TITLE,
            Medperson.DEP_WORK,
            Medperson.NewPos,
            Positions.PosName
            FROM
            Medperson
            INNER JOIN Positions ON Medperson.NewPos = Positions.PosCode
            WHERE
            Medperson.CSTATUS != 0
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
        // $myArray = array();
        // $strDepQuery = '';
        // $arr = [];
        // $PERID = $_SESSION['PERID'];
        // if ($this->db->hostDB) {
        //     $query = mysqli_query($this->db->hostDB, "SELECT
        //                 C_admin.PERID
        //             FROM
        //                 C_admin
        //             WHERE
        //                 C_admin.PERID = $PERID");
        //     if ($query->num_rows == 0) {
        //         $query = mysqli_query($this->db->hostDB, "SELECT
        //                     B.Dep_Code,
        //                     B.Edit_code,
        //                     B.Dep_name,
        //                     B.Dep_Group_name,
        //                     B.Telephone,
        //                     B.Doc_In,
        //                     B.Doc_Out,
        //                     B.Doc_go,
        //                     B.Doc_in2,
        //                     B.Dep_status
        //                     FROM
        //                     STAFF.D_admin AS A
        //                     INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
        //                     WHERE
        //                     A.Perid = $PERID
        //                     ORDER BY B.Dep_name ASC");
        //         if ($query->num_rows == 0) {
        //             $query = mysqli_query($this->db->hostDB, "SELECT
        //                         A.Dep_Code,
        //                         A.Dep_name,
        //                         A.Dep_Group_name
        //                         FROM
        //                         STAFF.Depart AS A
        //                         INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
        //                         WHERE
        //                         B.PERID = $PERID
        //                         ORDER BY
        //                         A.Dep_name ASC
        //                         ");
        //             if ($query) {
        //                 // while ($data = mysqli_fetch_assoc($query)) {
        //                 //     $myArray[] = $data;
        //                 // }

        //             } else {
        //             }
        //         } else {
        //             while ($data = mysqli_fetch_assoc($query)) {
        //                 $query2 = mysqli_query($this->db->hostDB, "SELECT
        //                             STAFF.Medperson.`NAME`,
        //                             STAFF.Medperson.SURNAME,
        //                             STAFF.Medperson.SEX,
        //                             STAFF.Medperson.TITLE,
        //                             STAFF.Depart.Dep_Code,
        //                             STAFF.Depart.Dep_name,
        //                             STAFF.Depart.Dep_Group_name,
        //                             STAFF.Positions.PosName
        //                         FROM
        //                             STAFF.Medperson
        //                             INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
        //                             INNER JOIN STAFF.Positions ON STAFF.Positions.PosCode = STAFF.Medperson.NewPos
        //                         WHERE
        //                             STAFF.Depart.Dep_Code =  = " . $data['Dep_Code']);

        //                 if ($query2) {
        //                     while ($data = mysqli_fetch_assoc($query2)) {
        //                         array_push($arr, $data);
        //                     }
        //                 } else {
        //                 }
        //             }
        //             rsort($arr);
        //             $myJSON = json_encode($arr);
        //             echo $myJSON;
        //         }
        //     } else {
        //         $query = mysqli_query($this->db->hostDB, "SELECT
        //                     STAFF.Medperson.`NAME`,
        //                     STAFF.Medperson.SURNAME,
        //                     STAFF.Medperson.SEX,
        //                     STAFF.Medperson.TITLE,
        //                     STAFF.Depart.Dep_Code,
        //                     STAFF.Depart.Dep_name,
        //                     STAFF.Depart.Dep_Group_name,
        //                     STAFF.Positions.PosName
        //                 FROM
        //                     STAFF.Medperson
        //                     INNER JOIN STAFF.Depart ON STAFF.Depart.Dep_Code = STAFF.Medperson.DEP_WORK
        //                     INNER JOIN STAFF.Positions ON STAFF.Positions.PosCode = STAFF.Medperson.NewPos");
        //         if ($query) {
        //             while ($data = mysqli_fetch_assoc($query)) {

        //                 $myArray[] = $data;

        //             }
        //         } else {
        //         }
        //         $myJSON = json_encode($myArray);
        //         echo $myJSON;
        //     }
        // } else {
        //     return false;
        // }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                    *
                    FROM
                    D_holiday
                    ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

}
