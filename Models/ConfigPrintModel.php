<?php

class ConfigPrintModel extends Model
{

    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    //เรียกข้อมูลหน่วยงานทั้งหมด
    public function GETALLDEPARTMENT()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // ลบใบลาบุคลากรในหน่วยงาน
    public function DELETEDATANOTEOFLEAVE($No_Id)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "DELETE FROM c_take_a_leave
            WHERE No_Id = " . $No_Id);
            if ($query) {
                $arr = array('Status' => true, 'Message' => "ลบข้อมูลสำเร็จ");
            } else {
                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
            }
            echo json_encode($arr);
        } else {
            return false;
        }
    }

    // เรียกมูลใบลาทั้งหมดโดยการนับ count
    public function GETCOUNTDATANOTEOFLEAVE($DepCode)
    {

        $myArray = array();
        $strDepQuery = '';
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            C_admin.PERID
        FROM
            C_admin
        WHERE
            C_admin.PERID = $PERID");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Edit_code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                STAFF.D_admin AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        // while ($data = mysqli_fetch_assoc($query)) {
                        //     $myArray[] = $data;
                        // }

                    } else {
                    }
                } else {
                    $counter = 0;
                    while ($data = mysqli_fetch_assoc($query)) {
                        $query2 = mysqli_query($this->db->hostDB, "SELECT
                    *
                FROM
                    `c_take_a_leave`
                WHERE
                    c_take_a_leave.T_Status_Leave = 0
                AND c_take_a_leave.Dep_code = " . $data['Dep_Code'] . "
                    ");
                        if ($query2) {
                            $countData = mysqli_num_rows($query2);
                            $counter += $countData;
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดใช้งานคำสั่งได้");
                        }
                        // echo json_encode($arr);
                        // if (++$counter == $numResults) {
                        //     $strDepQuery .= " c_take_a_leave.Dep_Code = " . $data['Dep_Code'];
                        // } else {
                        //     $strDepQuery .= " c_take_a_leave.Dep_Code = " . $data['Dep_Code'] . " OR";
                        // }
                    }
                    $arr = array('Status' => true, 'Message' => $counter);
                    echo json_encode($arr);
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "SELECT
                *
            FROM
                `c_take_a_leave`
            WHERE
                c_take_a_leave.T_Status_Leave = 0
            ");
                if ($query) {
                    $countData = mysqli_num_rows($query);
                    $arr = array('Status' => true, 'Message' => $countData);
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
                }
                echo json_encode($arr);
            }
        } else {
            return false;
        }

        // $myArray = array();
        // if ($this->db->hostDB) {
        //     $query = mysqli_query($this->db->hostDB, "SELECT
        //     *
        // FROM
        //     `c_take_a_leave`
        // WHERE
        //     c_take_a_leave.T_Status_Leave = 0
        // AND
        //     c_take_a_leave.Dep_Code = $DepCode
        //     ");
        //     if ($query) {
        //         $countData = mysqli_num_rows($query);
        //         $arr = array('Status' => true, 'Message' => $countData);
        //     } else {
        //         $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถลบข้อมูลได้");
        //     }
        //     echo json_encode($arr);
        // } else {
        //     return false;
        // }
    }

    //เรียกมูลบุคลากรหน่วยงานที่เกี่ยวข้อง
    public function GETALLDATAPERSON()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            STAFF.Medperson.PERID,
            STAFF.Medperson.`NAME`,
            STAFF.Medperson.SURNAME,
            STAFF.Medperson.SEX,
            STAFF.Medperson.TITLE,
            STAFF.Medperson.DEP_WORK,
            STAFF.Medperson.NewPos,
            STAFF.Depart.Dep_name,
            STAFF.Positions.PosName
            FROM
            STAFF.Medperson
            INNER JOIN STAFF.Positions ON STAFF.Medperson.NewPos = STAFF.Positions.PosCode
            INNER JOIN STAFF.Depart ON STAFF.Medperson.DEP_WORK = STAFF.Depart.Dep_Code
            WHERE
            STAFF.Medperson.CSTATUS != 0
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //เรียกมูลบุคคลที่มีสิทธิอนุมัติใบลา
    public function GETALLAPPROVEPERSONLEAVE($DepCode)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                c_config_print
                WHERE
                Dep_Code = " . $DepCode);
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // บันทึกการตั้งค่าผู้มีสิทธิอนุมัติใบลาหรือลงลายเซ็นต์
    public function SAVECONFIGFORPRINT($A_M, $A_L, $A_P, $A_I, $Perid, $DepCode)
    {
        $myArray = array();
        date_default_timezone_set("Asia/Bangkok");
        $dateNow = date("Y-m-d H:i:s");
        $count = 0;
        $sqlQuery = '';

        if ($this->db->hostDB) {

            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    A.PERID = " .$_SESSION['PERID']
            );
            if($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {
                    for ($i = 0; $i < count($A_M); $i++) {
                        if ($i == count($A_M) - 1) {
                            $sqlQuery .= "('" . $A_M[$i]->PERID . "', '" . $DepCode . "',0, '" . $A_M[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                        } else {
                            $sqlQuery .= "('" . $A_M[$i]->PERID . "', '" . $DepCode . "',0, '" . $A_M[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                        }
                    }
                    for ($i = 0; $i < count($A_L); $i++) {
                        if ($i == count($A_L) - 1) {
                            $sqlQuery .= "('" . $A_L[$i]->PERID . "', '" . $DepCode . "',1, '" . $A_L[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                        } else {
                            $sqlQuery .= "('" . $A_L[$i]->PERID . "', '" . $DepCode . "',1, '" . $A_L[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                        }
                    }
                    for ($i = 0; $i < count($A_P); $i++) {
                        if ($i == count($A_P) - 1) {
                            $sqlQuery .= "('" . $A_P[$i]->PERID . "', '" . $DepCode . "',2, '" . $A_P[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                        } else {
                            $sqlQuery .= "('" . $A_P[$i]->PERID . "', '" . $DepCode . "',2, '" . $A_P[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                        }
                    }
                    for ($i = 0; $i < count($A_I); $i++) {
                        if ($i == count($A_I) - 1) {
                            $sqlQuery .= "('" . $A_I[$i]->PERID . "', '" . $DepCode . "',3, '" . $A_I[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP)";
                        } else {
                            $sqlQuery .= "('" . $A_I[$i]->PERID . "', '" . $DepCode . "',3, '" . $A_I[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                        }
                    }

                    $q_DeleteData = mysqli_query($this->db->hostDB, "DELETE
                    FROM c_config_print
                    WHERE
                    Dep_Code = $DepCode
                    ");

                    if ($q_DeleteData) {
                        $query = mysqli_query($this->db->hostDB, "INSERT INTO c_config_print (PERID, Dep_Code, P_STATUS, POSI_STATUS, CREATE_BY, CREATE_TIME, UPDATE_BY, UPDATE_TIME)
                            VALUES $sqlQuery");
                        if ($query) {
                            $arr = array('Status' => true, 'Message' => 'บันทึกข้อมูลใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้โปรดลองอีกครั้ง");
                        }
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้โปรดลองอีกครั้ง");
                    }
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $DepCode
                    AND
                    D.Opt_C_Code = '007_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            for ($i = 0; $i < count($A_M); $i++) {
                                if ($i == count($A_M) - 1) {
                                    $sqlQuery .= "('" . $A_M[$i]->PERID . "', '" . $DepCode . "',0, '" . $A_M[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                                } else {
                                    $sqlQuery .= "('" . $A_M[$i]->PERID . "', '" . $DepCode . "',0, '" . $A_M[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                                }
                            }
                            for ($i = 0; $i < count($A_L); $i++) {
                                if ($i == count($A_L) - 1) {
                                    $sqlQuery .= "('" . $A_L[$i]->PERID . "', '" . $DepCode . "',1, '" . $A_L[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                                } else {
                                    $sqlQuery .= "('" . $A_L[$i]->PERID . "', '" . $DepCode . "',1, '" . $A_L[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                                }
                            }
                            for ($i = 0; $i < count($A_P); $i++) {
                                if ($i == count($A_P) - 1) {
                                    $sqlQuery .= "('" . $A_P[$i]->PERID . "', '" . $DepCode . "',2, '" . $A_P[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                                } else {
                                    $sqlQuery .= "('" . $A_P[$i]->PERID . "', '" . $DepCode . "',2, '" . $A_P[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                                }
                            }
                            for ($i = 0; $i < count($A_I); $i++) {
                                if ($i == count($A_I) - 1) {
                                    $sqlQuery .= "('" . $A_I[$i]->PERID . "', '" . $DepCode . "',3, '" . $A_I[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP)";
                                } else {
                                    $sqlQuery .= "('" . $A_I[$i]->PERID . "', '" . $DepCode . "',3, '" . $A_I[$i]->Position . "', '" . $Perid . "', CURRENT_TIMESTAMP, '" . $Perid . "', CURRENT_TIMESTAMP),";
                                }
                            }

                            $q_DeleteData = mysqli_query($this->db->hostDB, "DELETE
                    FROM c_config_print
                    WHERE
                    Dep_Code = $DepCode
                    ");

                            if ($q_DeleteData) {
                                $query = mysqli_query($this->db->hostDB, "INSERT INTO c_config_print (PERID, Dep_Code, P_STATUS, POSI_STATUS, CREATE_BY, CREATE_TIME, UPDATE_BY, UPDATE_TIME)
                            VALUES $sqlQuery");
                                if ($query) {
                                    $arr = array('Status' => true, 'Message' => 'บันทึกข้อมูลใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                                } else {
                                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้โปรดลองอีกครั้ง");
                                }
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้โปรดลองอีกครั้ง");
                            }
                        } else {
                            $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้");
                        }
                    }
                }
            }
            echo json_encode($arr);
        } else {
            return false;
        }

    }

}
