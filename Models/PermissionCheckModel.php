<?php


class PermissionCheckModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    public function CHECKPERMISSIONMENU($MENU_CODE)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
                SELECT
                A.*,
                C.Permiss_ID,
                C.Permiss_Details,
                D.Opt_C_Code,
                D.Opt_C_Details
                FROM
                c_admin_permiss AS A
                LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                WHERE
                A.PERID = " . $_SESSION['PERID']);
            if (mysqli_num_rows($query) > 0) {
                if(mysqli_num_rows($query) > 0){
                    echo json_encode(array('Status' => true));
                } else {
                    echo json_encode(array('Status' => false));
                }
            } else {
                $query = mysqli_query($this->db->hostDB, "
                SELECT
                A.*,
                C.Permiss_ID,
                C.Permiss_Details,
                D.Opt_C_Code,
                D.Opt_C_Details
                FROM
                c_user_permiss AS A
                LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                WHERE
                C.Permiss_ID = $MENU_CODE
                AND
                A.PERID = " . $_SESSION['PERID']);
                if ($query) {
                    if(mysqli_num_rows($query) > 0){
                        echo json_encode(array('Status' => true));
                    } else {
                        echo json_encode(array('Status' => false));
                    }
                } else {
                    echo json_encode(array('Status' => false));
                }
            }
        } else {
            return false;
        }
    }
}