<?php


class SummaryWorkingDayModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    // เรียกข้อมูลทั้งหมดจากฐานข้อมูลHRTIME_DB->D_transaction เงื่อนไข PERID != 0
    public function GETAllDEPART()
    {
        $myArray = array();
        $PERID = $_SESSION['PERID'];
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_admin_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                B.Dep_Code,
                B.Dep_Code,
                B.Dep_name,
                B.Dep_Group_name,
                B.Telephone,
                B.Doc_In,
                B.Doc_Out,
                B.Doc_go,
                B.Doc_in2,
                B.Dep_status
                FROM
                c_user_permiss AS A
                INNER JOIN STAFF.Depart AS B ON B.Dep_Code = A.Dep_code
                WHERE
                A.Perid = $PERID
                ORDER BY B.Dep_name ASC");
                if ($query->num_rows == 0) {
                    $query = mysqli_query($this->db->hostDB, "SELECT
                    A.Dep_Code,
                    A.Dep_name,
                    A.Dep_Code,
                    A.Dep_Group_name
                    FROM
                    STAFF.Depart AS A
                    INNER JOIN STAFF.Medperson AS B ON B.DEP_WORK = A.Dep_Code
                    WHERE
                    B.PERID = $PERID
                    ORDER BY
                    A.Dep_name ASC
                    ");
                    if ($query) {
                        while ($data = mysqli_fetch_assoc($query)) {

                            $myArray[] = $data;

                        }
                    } else {
                    }
                } else {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                }
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    // เรียกข้อมูลวันหยุดทั้งหมดจากฐานข้อมูล D_holiday
    public function GETHOLIDAY()
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                *
                FROM
                D_holiday
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            return $myJSON;
        } else {
            return false;
        }
    }

    //  เรียกข้อมูลบุคลากรในหน่วยงานโดยใช้เงื่อนไข Dep_Code
    public function GETPERSONFORMDEP($Dep_Code)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.PERID,
            A.`NAME`,
            A.SURNAME,
            A.POS_WORK,
            A.NewPos,
            B.Dep_Code,
            B.Edit_code,
            B.Dep_name
            FROM
            STAFF.Depart AS B
            INNER JOIN STAFF.Medperson AS A ON B.Dep_Code = A.DEP_WORK
            WHERE
            B.Dep_Code = $Dep_Code
            AND A.CSTATUS != 0
            ORDER BY A.PERID ASC
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    array_push($myArray, $data);

                }

                $queryPersonOther = mysqli_query($this->db->hostDB, "SELECT
            A.`NAME`,
            A.SURNAME,
            A.PERID,
            A.POS_WORK,
             A.NewPos,
            B.Dep_Code,
            B.Edit_code,
            B.Dep_name
            FROM
            HRTIME_DB.c_medperson AS D
            LEFT JOIN STAFF.Medperson AS A ON A.PERID = D.PERID
            LEFT JOIN STAFF.Depart AS B ON B.Dep_Code = A.DEP_WORK
            WHERE
            D.Dep_Code = " . $Dep_Code . " AND
            A.CSTATUS != 0
            ORDER BY
            A.PERID ASC
            ");
                if ($queryPersonOther) {
                    while ($data = mysqli_fetch_assoc($queryPersonOther)) {
                        array_push($myArray, $data);
                    }
                }

            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function GETDATAWORKING($Dep_Code, $Date_Start, $Date_End)
    {
        $WorkingData = array();
        $LeaveData = array();
        $LeaveData2 = array();
        $SummaryWorkingData = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
                A_transaction.*
                FROM
                A_transaction
                WHERE
                A_transaction.Dep_Code = $Dep_Code AND
                A_transaction.A_Datetime BETWEEN '$Date_Start' AND '$Date_End'
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $WorkingData[] = $data;

                }
            } else {
            }

            // -------------------------------------------------- //

            $query = mysqli_query($this->db->hostDB, "SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.Dep_Code = $Dep_Code AND
                c_take_a_leave.T_Leave_Date_Start BETWEEN '$Date_Start' AND '$Date_End'
                ORDER BY c_take_a_leave.T_Leave_Date_Start ASC
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $LeaveData[] = $data;

                }
            } else {
            }

            // -------------------------------------------------- //

            $query = mysqli_query($this->db->hostDB, "SELECT
                c_take_a_leave.*
                FROM
                c_take_a_leave
                WHERE
                c_take_a_leave.Dep_Code = $Dep_Code AND
                c_take_a_leave.T_Leave_Date_Start < '$Date_Start' AND c_take_a_leave.T_Leave_Date_End >= '$Date_Start'
                ORDER BY c_take_a_leave.T_Leave_Date_Start ASC
            ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $LeaveData2[] = $data;

                }
            } else {
            }
            array_push($SummaryWorkingData, array('WorkingData' => $WorkingData, 'LeaveData' => $LeaveData, 'LeaveData2' => $LeaveData2));
            $myJSON = json_encode($SummaryWorkingData);
            echo $myJSON;
        } else {
            return false;
        }
    }

    public function SUBMITREPORTSUMMARY($Report_Data, $Dep_Code)
    {
        $Query_Insert = '';
        $command = '';
        $Count = count($Report_Data);
        for ($i = 0; $i < count($Report_Data); $i++) {
            if ($i === $Count) {
                if ($Report_Data[$i]->CheckApprove) {
                    $Query_Insert .= '(' . $Report_Data[$i]->PERID . ', "' . $Report_Data[$i]->Date_Submit . '", 3, ' . $Report_Data[$i]->WorkDaySum . ')';
                }
            } else {
                if ($Report_Data[$i]->CheckApprove) {
                    if($Query_Insert == ''){
                        $Query_Insert .= '(' . $Report_Data[$i]->PERID . ', "' . $Report_Data[$i]->Date_Submit . '", 3, ' . $Report_Data[$i]->WorkDaySum . ')';
                    } else {
                        $Query_Insert .= ', (' . $Report_Data[$i]->PERID . ', "' . $Report_Data[$i]->Date_Submit . '", 3, ' . $Report_Data[$i]->WorkDaySum . ') ';
                    }
                }
            }
        }

        if ($this->db->hostDB) {

            $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_admin_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $Dep_Code
                    AND
                    A.PERID = " . $_SESSION['PERID']
            );
            if ($queryPermiss) {
                if (mysqli_num_rows($queryPermiss) >= 1) {

//                    $ApproveData = mysqli_query()

                    $query75 = mysqli_query($this->db->hostDB, "INSERT INTO HR_CENTER.`working_summary_tb` (
                    `perid`,
                    `monthly`,
                    `classify`,
                    `record`
                )
                VALUES
                    $Query_Insert
                ");
                    $query = mysqli_query($this->db->hostDB2, "INSERT INTO HR_CENTER.`working_summary_tb` (
                                `perid`,
                                `monthly`,
                                `classify`,
                                `record`
                            )
                            VALUES
                                $Query_Insert
                            ");
                    $command = "INSERT INTO HR_CENTER.`working_summary_tb` (
                                `perid`,
                                `monthly`,
                                `classify`,
                                `record`
                            )
                            VALUES
                                $Query_Insert
                            ";
                    $command = trim(preg_replace('/\s\s+/', ' ', $command));
                    mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`log_hrtime` (
                                `process`,
                                `perid`,
                                `type`,
                                `command`
                            )
                            VALUES
                                ('SummaryWorking', '" . $_SESSION['PERID'] . "' , 'insert', '" . $command . "')
                            ");
                    if ($query) {
                        $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                    } else {
                        $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                    }
                    echo json_encode($arr);
                } else {
                    $queryPermiss = mysqli_query($this->db->hostDB, "SELECT
                    A.*,
                    C.Permiss_ID,
                    C.Permiss_Details,
                    D.Opt_C_Code,
                    D.Opt_C_Details
                    FROM
                    c_user_permiss AS A
                    LEFT JOIN c_user_option AS B ON B.PERID = A.PERID
                    LEFT JOIN c_option_permiss AS D ON D.Opt_C_Code = B.Option_Permiss
                    LEFT JOIN c_permission AS C ON C.Permiss_ID = B.Menu_Permiss
                    WHERE
                    A.Dep_Code = $Dep_Code
                    AND
                    D.Opt_C_Code = '010_A'
                    AND
                    A.PERID = " . $_SESSION['PERID']
                    );
                    if ($queryPermiss) {
                        if (mysqli_num_rows($queryPermiss) >= 1) {
                            $query75 = mysqli_query($this->db->hostDB, "INSERT INTO HR_CENTER.`working_summary_tb` (
                                `perid`,
                                `monthly`,
                                `classify`,
                                `record`
                            )
                            VALUES
                                $Query_Insert
                            ");
                            $query = mysqli_query($this->db->hostDB2, "INSERT INTO HR_CENTER.`working_summary_tb` (
                                `perid`,
                                `monthly`,
                                `classify`,
                                `record`
                            )
                            VALUES
                                $Query_Insert
                            ");
                            $command = "INSERT INTO HR_CENTER.`working_summary_tb` (
                                `perid`,
                                `monthly`,
                                `classify`,
                                `record`
                            )
                            VALUES
                                $Query_Insert
                            ";
                            $command = trim(preg_replace('/\s\s+/', ' ', $command));
                            mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.`log_hrtime` (
                                `process`,
                                `perid`,
                                `type`,
                                `command`
                            )
                            VALUES
                                ('SummaryWorking', '" . $_SESSION['PERID'] . "' , 'insert', '" . $command . "')
                            ");
                            if ($query) {
                                $arr = array('Status' => true, 'Message' => "ยืนยันข้อมูลสำเร็จ");
                            } else {
                                $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถยืนยันข้อมูลได้");
                            }
                            echo json_encode($arr);
                        } else {
                            echo json_encode(array('Status' => false, 'Message' => "เกิดข้อผิดพลาดคุณไม่มีได้รับสิทธิในการจัดการข้อมูลส่วนนี้"));
                        }
                    }
                }
            } else {

            }
        } else {
            return false;
        }
    }

    //  เรียกบุคลากรทุกคนภายในหน่วยงาน
    public function GETPERSONALDEPART($Dep_Code)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "SELECT
            A.PERID,
            A.`NAME`,
            A.SURNAME,
            B.Dep_Code,
            B.Edit_code,
            B.Dep_name
            FROM
            STAFF.Depart AS B
            INNER JOIN STAFF.Medperson AS A ON B.Dep_Code = A.DEP_WORK
            WHERE
            B.Dep_Code = $Dep_Code
            AND A.CSTATUS != 0
            ");

            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    $myArray[] = $data;

                }
            } else {

            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }

    // สรุปการลาของบุคลากรในหน่วยงานที่รับผิดชอบ
    public function GETREPORTLEAVESUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2)
    {
        $myArray = array();
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
            SELECT
            A.*,
            B.Edit_code,
            B.Dep_Code
            FROM
            HRTIME_DB.c_take_a_leave AS A
            INNER JOIN STAFF.Depart AS B ON A.Dep_code = B.Dep_Code
            WHERE
            B.Dep_Code = $Dep_Code AND
            A.T_Leave_Date_Start <= '$Date_Cordi2' AND
            A.T_Leave_Date_End >= '$Date_Cordi' AND
            A.T_Status_Leave = '1'
            ");
            if ($query->num_rows == 0) {
                $query = mysqli_query($this->db->hostDB, "SELECT
                *
            FROM
                c_take_a_leave
            WHERE
                c_take_a_leave.Dep_code = $Dep_Code
            AND T_Leave_Date_End = '$Date_Cordi'
            AND T_Status_Leave = '1'
            ");
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
                $myJSON = json_encode($myArray);
            } else {
                if ($query) {
                    while ($data = mysqli_fetch_assoc($query)) {

                        $myArray[] = $data;

                    }
                } else {
                }
                $myJSON = json_encode($myArray);
            }
            echo $myJSON;
        } else {
            return false;
        }
    }

    function GETAPPROVEDATASUMMARY($Dep_Code, $Date_Cordi)
    {
        $myArray = array();
        if ($this->db->hostDB2) {
            $query = mysqli_query($this->db->hostDB2, "SELECT
                A.id,
                A.perid,
                A.monthly,
                A.classify,
                A.record,
                C.Dep_Code
                FROM
                HR_CENTER.working_summary_tb AS A
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.perid
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                WHERE
                C.Dep_Code = $Dep_Code
                AND
                A.classify = '3'
                AND
                A.monthly = '$Date_Cordi'
                ");
            if ($query) {
                while ($data = mysqli_fetch_assoc($query)) {

                    array_push($myArray,$data);

                }

                $queryOhter = mysqli_query($this->db->hostDB2, "SELECT
                A.id,
                A.perid,
                A.monthly,
                A.classify,
                A.record,
                C.Dep_Code
                FROM
                HRTIME_DB.c_medperson AS D
                INNER JOIN HR_CENTER.working_summary_tb AS A ON A.perid = D.PERID
                INNER JOIN STAFF.Medperson AS B ON B.PERID = A.perid
                INNER JOIN STAFF.Depart AS C ON C.Dep_Code = B.DEP_WORK
                WHERE
                D.Dep_Code = $Dep_Code
                AND
                A.classify = '3'
                AND
                A.monthly = '$Date_Cordi'
                ");

                while ($data = mysqli_fetch_assoc($queryOhter)) {

                    array_push($myArray,$data);

                }
            } else {
            }
            $myJSON = json_encode($myArray);
            echo $myJSON;
        } else {
            return false;
        }
    }
}
