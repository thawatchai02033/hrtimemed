<?php


class lineNotifyModel extends Model
{
    public function __construct()
    {
        parent::__construct(constant("DB"));
    }

    public function SAVELINENOTIFY($Token)
    {
        $myArray = array();
        date_default_timezone_set("Asia/Bangkok");
        $dateNow = date("Y-m-d H:i:s");
        if ($this->db->hostDB) {
            $query = mysqli_query($this->db->hostDB, "
                SELECT
                *
                FROM
                HRTIME_DB.line_notify
                WHERE
                HRTIME_DB.line_notify.PERID = ". $_SESSION['PERID']);
            if (mysqli_num_rows($query) == 0) {
                $query = mysqli_query($this->db->hostDB, "INSERT INTO HRTIME_DB.line_notify(PERID, Line_Token)
                VALUES (". $_SESSION['PERID'] .", '" . $Token . "')");
                if ($query) {
                    $arr = array('Status' => true, 'Message' => 'บันทึกข้อมูล line token ใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่สามารถบันทึกข้อมูลได้โปรดลองอีกครั้ง");
                }
                echo json_encode($arr);
            } else {
                $query = mysqli_query($this->db->hostDB, "UPDATE HRTIME_DB.line_notify
                    SET Line_Token = '" . $Token . "'
                    WHERE PERID = " . $_SESSION['PERID']);
                if ($query) {
                    $arr = array('Status' => true, 'Message' => 'แก้ไขข้อมูล line token ใหม่วันที่ ' . $dateNow . ' สำเร็จ');
                } else {
                    $arr = array('Status' => false, 'Message' => "เกิดข้อผิดพลาดไม่แก้ไขข้อมูลได้");
                }
                echo json_encode($arr);
            }
        }
    }
}