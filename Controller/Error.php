<?php

class ErrorPage extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->views->msg = 'ฝ่ายเทคโนโลยีสารสนเทศ';
        $this->views->render('Page/ErrorPage');
    }
}
