<?php

class ManageTime extends Controller
{
    public function __construct()
    {
        parent::__construct('ManageTime');
        $this->views->Department = $this->model->GatAllDepartment();
        $this->views->Door = $this->model->GatAllDoor();
        $this->views->render('Page/ManageTimePage');
    }
}
