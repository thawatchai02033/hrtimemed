<?php

class TakeALeave extends Controller
{
    public function __construct()
    {
        parent::__construct('TakeALeave');
        $this->views->Department = $this->model->GETALLDEPARTMENT();
        $this->views->LeaveType = $this->model->GETLEAVETYPE();
        $this->views->DayType = $this->model->GETDAYTYPE();
        $this->views->PersonCommit = $this->model->GETALLDATAPERSONDep();
        $this->views->AllPerson = $this->model->GETALLDATAPERSON();
        $this->views->Country = $this->model->GETALLCOUNTRY();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/TakeALeavePage');
        // $this->views->render('Page/ComingPage');
    }
}
