<?php

    class More extends Controller
    {
        function __construct()
        {
            parent::__construct('More');

            $this->views->Department = $this->model->GatAllDepartment();
            $this->views->Holiday = $this->model->GETHOLIDAY();
            $this->views->render('Page/MorePage');
        }
    }
    
?>