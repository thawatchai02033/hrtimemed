<?php

class LeaveRegis extends Controller
{
    public function __construct()
    {
        parent::__construct('LeaveRegis');
        // $this->views->msg = $this->model->DateAllData();
        $this->views->Department = $this->model->GatAllDepartment();
        $this->views->render('Page/LeaveRegisPage');
    }
}
