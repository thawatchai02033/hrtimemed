<?php

class ConfigPrint extends Controller
{
    public function __construct()
    {
        parent::__construct('ConfigPrint');
        $this->views->Department = $this->model->GETALLDEPARTMENT();
        $this->views->PersonCommit = $this->model->GETALLDATAPERSON();
        $this->views->render('Page/ConfigPrintPage');
    }

}
