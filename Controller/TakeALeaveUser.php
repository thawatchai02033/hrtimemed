<?php

class TakeALeaveUser extends Controller
{
    public function __construct()
    {
        parent::__construct('TakeALeaveUser');
        $this->views->Department = $this->model->GETALLDEPARTMENT();
        $this->views->LeaveType = $this->model->GETLEAVETYPE();
        $this->views->DayType = $this->model->GETDAYTYPE();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->DataPersonal = $this->model->GETDATAPERSONAL();
        $this->views->PersonCommit = $this->model->GETALLDATAPERSONDep();
        $this->views->AllPerson = $this->model->GETALLDATAPERSON();
        $this->views->Country = $this->model->GETALLCOUNTRY();
        $this->views->render('Page/TakeALeaveUserPage');
        // $this->views->render('Page/ComingPage');
    }
}
