<?php

class Report extends Controller
{
    public function __construct()
    {
        parent::__construct('Report');
        $this->views->Department = $this->model->GatAllDepartment();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/ReportPage');
    }

}
