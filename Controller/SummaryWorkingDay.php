<?php


class SummaryWorkingDay extends Controller
{
    public function __construct()
    {
        parent::__construct('SummaryWorkingDay');
        $this->views->Department = $this->model->GETAllDEPART();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/SummaryWorkingDayPage');
    }
}