<?php

class WorkTime extends Controller
{
    public function __construct()
    {
        parent::__construct('WorkTime');
        $this->views->Department = $this->model->GatAllDepartment();
        $this->views->DoorArray = $this->model->GETALLDOORHOS();
        $this->views->DoorArrOutDepart = $this->model->GETALLDOORHOSOUTDEPART();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/WorkTimePage');
    }
}
