<?php

class DepartSum extends Controller
{
    public function __construct()
    {
        parent::__construct('DepartSum');
        $this->views->Department = $this->model->GatAllDepartment();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/DepartSumPage');
    }
}
