<?php

class ReportOfDay extends Controller
{
    public function __construct()
    {
        parent::__construct('ReportOfDay');
        $this->views->Department = $this->model->GatAllDepartment();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/ReportOfDayPage');
    }

}
