<?php

class Date extends Controller
{
    public function __construct()
    {
        parent::__construct('Date');
        // $this->views->msg = $this->model->DateAllData();
        $this->views->Department = $this->model->GatAllDepartment();
        $this->views->DoorArray = $this->model->GETALLDOORHOS();
        $this->views->DoorArrOutDepart = $this->model->GETALLDOORHOSOUTDEPART();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/DatePage');

        //require './Models/DateModel.php';
        //$this->model->GetAllData();
        // if(isset($this->views)){
        //     echo 'yes';
        // }else{
        //     echo 'on';
        // }
        // if(isset($this->model)){
        //     $this->model->GetAllData();
        // }else{
        //     echo 'on';
        // }
    }

    public function Index()
    {
        $this->model->GetAllData();
    }
}
