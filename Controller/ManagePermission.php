<?php

class ManagePermission extends Controller
{
    public function __construct()
    {
        parent::__construct('ManagePermission2');
        $this->views->Department = $this->model->GETALLDEPARTMENT();
        $this->views->PersonCommit = $this->model->GETALLDATAPERSON();
        $this->views->PersonCommit2 = $this->model->GETALLDATAPERSON2();
        $this->views->Personal_Ref = $this->model->GETALLPERSONAL_REF();
        $this->views->Depart_Ref = $this->model->GETALLDEPART_REF();
        $this->views->Menu_Func = $this->model->GETALLMENU_FUNC();
        $this->views->Option_Func = $this->model->GETALLOPTION_FUNC();
        $this->views->render('Page/ManagePermissionPage');
    }
}
