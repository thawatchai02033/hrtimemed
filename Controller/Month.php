<?php

    class Month extends Controller
    {
        function __construct()
        {
            parent::__construct('Month');

            $this->views->Department = $this->model->GatAllDepartment();
            $this->views->Holiday = $this->model->GETHOLIDAY();
            $this->views->render('Page/MonthPage');
        }
    }
    
?>