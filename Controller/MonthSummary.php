<?php

    class MonthSummary extends Controller
    {
        function __construct()
        {
            parent::__construct('MonthSummary');

            $this->views->Department = $this->model->GatAllDepartment();
            $this->views->Holiday = $this->model->GETHOLIDAY();
            $this->views->render('Page/MonthSummaryPage');
        }
    }
    
?>