<?php

class Login extends Controller
{
    private $CheckAlertMessage = false;
    public function __construct($pages = false)
    {
        parent::__construct('Login');
        // $this->views->Result = $this->model->TestServiceModel();
        $this->views->render('Page/LoginPage');
    }

    public function Index($pages = false)
    {
        // $this->views->datamsg = $this->model->GetAllData();
        // $this->views->render('Page/LoginPage');
        header("location: " . constant(URL) . "MedMisProject/MVCworkTime/login");
    }

    public function CheckLogin()
    {
        // $this->views->datamsg = $this->model->GetAllData();
        // $this->views->render('Page/LoginPage');
        $username = $_POST["Username"];
        $password = $_POST["Password"];
        return $this->model->CheckLoginModel($username, $password);
    }
}
