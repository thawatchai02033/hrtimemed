<?php

class ReportLoadUnit extends Controller
{
    public function __construct()
    {
        parent::__construct('ReportLoadUnit');
        $this->views->Department = $this->model->GETAllDEPART();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/ReportLoadUnitPage');
    }
}
