<?php

class NoteOfLeave extends Controller
{
    public function __construct()
    {
        parent::__construct('NoteOfLeave');
        $this->views->Department = $this->model->GETALLDEPARTMENT();
        $this->views->PersonCommit = $this->model->GETALLDATAPERSON();
        $this->views->LeaveType = $this->model->GETLEAVETYPE();
        $this->views->DayType = $this->model->GETDAYTYPE();
        $this->views->Holiday = $this->model->GETHOLIDAY();
        $this->views->render('Page/NoteOfLeavePage');
    }

}
