<?php

require './libs/Router.php';
require './libs/Controller.php';
require './libs/Model.php';
require './libs/Views.php';

require './libs/Database.php';

require './Config/Const.php';
require './Config/Database.php';
$app = new Router();
