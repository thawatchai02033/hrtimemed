<aside class="main-sidebar" style="z-index: unset;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="z-index: 10;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="./Image/psu.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><span class="hidden-xs">
                        <?php echo($PersonalData["DataPerson"]["NAME"] . " " . $PersonalData["DataPerson"]["SURNAME"]); ?></span>
                </p>
                <a>ฝ่าย :
                    <?php echo $PersonalData["DataPerson"]["Dep_name"]; ?></a>
                <br/>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header" style="color: turquoise;font-weight: bold">เมนูสำหรับบุคลากรในหน่วยงาน</li>
            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>การเข้า-ออกงาน</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($_GET['url'] == "WorkTime") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./WorkTime"><i class="fa fa-circle-o"></i> รายละเอียดการเข้า-ออกงาน</a></li>
                    <li class="<?php if ($_GET['url'] == "MonthSummary") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./MonthSummary"><i class="fa fa-circle-o"></i> สรุปการปฏิบัติงาน</a></li>
                    <li class="<?php if ($_GET['url'] == "TakeALeaveUser") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./TakeALeaveUser"><i class="fa fa-circle-o"></i> ใบลา</a></li>
                    <!-- <li class="<?php if ($_GET['url'] == "individual") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./individual"><i
                                class="fa fa-circle-o"></i> รายบุคคล</a></li> -->
                    <li class="<?php if ($_GET['url'] == "LineNotify") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>">
                    <a href="./lineNotify"><i class="fa fa-circle-o"></i> แจ้งเตือนผ่านไลน์ <span style="color: #00FFFF">(Beta)</span></a></li>
                    <!-- <li class="<?php if ($_GET['url'] == "more") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./more"><i
                                class="fa fa-circle-o"></i> ภาพรวมการปฏิบัติงาน</a></li> -->
                    <!-- <li><a href="./moveEmployee"><i class="fa fa-circle-o"></i>
                            รายชื่อเคลื่อนย้ายบุคลากร</a></li> -->
                </ul>
            </li>
            <?php
            if ($PersonalData["Status"] == "Administrator" || $PersonalData["Status"] == "Supervisor") {
                $this->model->CheckMenu = 'true';
            } else {
                $this->model->CheckMenu = 'false';
            }
            ?>
            <li class="header" style="color: turquoise;font-weight: bold" ng-if="<?php echo $this->model->CheckMenu ?>">
                เมนูสำหรับหัวหน้าหน่วยงาน
            </li>
            <li class="treeview active" ng-if="<?php echo $this->model->CheckMenu ?>">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>จัดการข้อมูลการเข้า-ออกงาน</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($_GET['url'] == "Date") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./Date"><i class="fa fa-circle-o"></i> รายละเอียดการเข้า-ออกงาน</a></li>
                    <li class="<?php if ($_GET['url'] == "Month") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./Month"><i class="fa fa-circle-o"></i> รายงานประจำเดือน</a></li>
                    <li class="<?php if ($_GET['url'] == "More") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./More"><i class="fa fa-circle-o"></i> ภาพรวมการปฏิบัติงาน</a></li>
                    <li class="<?php if ($_GET['url'] == "ManageTime") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./ManageTime"><i class="fa fa-circle-o"></i> ตั้งค่าข้อมูลเวลา</a></li>
                </ul>
            </li>
            <li class="treeview active" ng-if="<?php echo $this->model->CheckMenu ?>">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>สรุปวันทำการ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($_GET['url'] == "SummaryWorkingDay") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./SummaryWorkingDay"><i class="fa fa-circle-o"></i> สรุปวันทำการ การปฏิบัติงาน</a></li>
                </ul>
            </li>
            <li class="treeview active" ng-if="<?php echo $this->model->CheckMenu ?>">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>จัดการ การลา</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($_GET['url'] == "TakeALeave") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./TakeALeave"><i class="fa fa-circle-o"></i> จัดการข้อมูลใบลา</a></li>
                    <li class="<?php if ($_GET['url'] == "NoteOfLeave") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>">
                        <a href="./NoteOfLeave">
                            <i class="fa fa-circle-o"></i>
                            อนุมัติใบลา
                            <span class="pull-right-container">
                                <small class="label pull-right bg-red" style="font-weight: bold"
                                       id="showCountLeave"></small>
                            </span>
                        </a>
                    </li>
                    <li class="<?php if ($_GET['url'] == "ConfigPrint") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./ConfigPrint"><i class="fa fa-circle-o"></i> ตั้งค่าการลา</a></li>
                    <li class="<?php if ($_GET['url'] == "LeaveRegis") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./LeaveRegis"><i class="fa fa-circle-o"></i> ตั้งค่าเริ่มต้นวันลาสะสม</a></li>
                    <li class="<?php if ($_GET['url'] == "RegisMail") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./RegisMail"><i class="fa fa-circle-o"></i> ตั้งค่าแจ้งเตือนผ่านไลน์</a></li>
                </ul>
            </li>
            <li class="treeview active" ng-if="<?php echo $this->model->CheckMenu ?>">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>จัดการข้อมูลสิทธิ์การใช้งาน</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($_GET['url'] == "ManagePermission") {
                        echo "active";
                    } else {
                        echo "";
                    } ?>"><a href="./ManagePermission"><i class="fa fa-circle-o"></i> จัดการข้อมูลสิทธิ์การใช้งาน</a></li>
                </ul>
            </li>
            <!--<li class="header" style="color: turquoise;font-weight: bold" ng-if="<?php /*echo $PersonalData["Status"] == "Administrator" */?>">
                เมนูสำหรับผู้บริหาร
            </li>
            <li class="treeview active" ng-if="<?php /*echo $PersonalData["Status"] == "Administrator" */?>">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>รายงาน</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php /*if ($_GET['url'] == "ReportOfDay") {
                        echo "active";
                    } else {
                        echo "";
                    } */?>"><a href="./ReportOfDay"><i class="fa fa-circle-o"></i> ภาพรวมการปฎิบัติงานประจำวัน</a></li>
                </ul>
                <ul class="treeview-menu">
                    <li class="<?php /*if ($_GET['url'] == "Report") {
                        echo "active";
                    } else {
                        echo "";
                    } */?>"><a href="./Report"><i class="fa fa-circle-o"></i> ภาพรวมการปฎิบัติงานประจำเดือน</a></li>
                </ul>
            </li>
            <li class="header" ng-if="<?php /*echo $this->model->CheckMenu */?>">Report Summary</li>-->
            <!--<li class="treeview active" ng-if="<?php /*echo $this->model->CheckMenu */?>">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>รายงาน</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php /*if ($_GET['url'] == "DepartSum") {echo "active" ;} else {echo "" ;}*/?>"><a href="./DepartSum"><i class="fa fa-circle-o"></i> รายงานระดับหน่วยงาน</a></li>
                </ul>
            </li>-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>