<?php
$path = './Models/ManageLoginModel.php';
if(file_exists($path)){
    require './Models/ManageLoginModel.php';
    $Modelsname = 'ManageLoginModel';
    $this->model = new $Modelsname;
    $PersonalData = $this->model->CheckLoginModel();
}
?>
<header class="main-header" ng-controller="myHeader" style="z-index: 1;">
    <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>HT</b>S</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>เวลา เข้า/ออก </b>งาน</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu" id="drop_Menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="./Image/psu.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo ($PersonalData["DataPerson"]["NAME"] ." ".  $PersonalData["DataPerson"]["SURNAME"]);?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="./Image/psu.gif" class="img-circle" alt="User Image">
                            <?php
                                if($PersonalData["Status"] == "Administrator"){
                                    $this->model->Status = "ผู้ดูแลระบบ";
                                    $this->model->CheckUser = true;
                                    $this->model->CheckStatus = 'true';
                                    $this->model->CheckStatusUser();
                                }else if($PersonalData["Status"] == "Supervisor"){
                                    $this->model->Status = "หัวหน้าหน่วยงาน";
                                    $this->model->CheckUser = false;
                                    $this->model->CheckStatus = 'true';
                                    $this->model->CheckStatusUser();
                                }else{
                                    $this->model->Status = "ผู้ใช้งานระบบ";
                                    $this->model->CheckUser = false;
                                    $this->model->CheckStatus = 'false';
                                    $this->model->CheckStatusUser();
                                }
                            ?>
                            <p>
                                <?php echo $this->model->Status ?>
                                <small>ฝ่าย : <?php echo $PersonalData["DataPerson"]["Dep_name"] ?></small>
                                <small>คณะแพทยศาสตร์ มหาวิทยาลัยสงขลานครินทร์</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" ng-click="LogOut()">ออกจากระบบ</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
