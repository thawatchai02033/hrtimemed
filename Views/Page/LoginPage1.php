<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HR - Time Summary</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="./tools/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="./tools/plugins/iCheck/square/blue.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

  <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    .swal2-popup {
      font-size: 1.6rem !important;
    }
</style>
</head>

<body class="hold-transition login-page" ng-app="myApp">
  <div class="login-box">
    <div class="login-logo" style="">
      <a href=""><b>ระบบ เข้า/ออก งานคณะแพทยศาสตร์ </b></a>
    </div>
    <div class="login-box-body" ng-controller="myCtrl">
      <p class="login-box-msg">กรุณาเข้าสู่ระบบก่อนเข้าใช้งาน</p>
      <!-- แก้ไข div จาก form -->
      <div>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="PERID" name="Username" ng-model="Username" ng-keyup="LoginKeyEnterCheck(event=$event)">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="PIN Number" name="Password" ng-model="Password" ng-keyup="LoginKeyEnterCheck(event=$event)">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <!-- <span class="	glyphicon glyphicon-info-sign" style="color: red" ng-hide="StatusLogin['Status']"> {{StatusLogin["Message"]}}</span> -->
            <!-- แก้ไขรายงานแสดงผลด้ายซ้ายมือปุ่มเข้าสู่ระบบ -->
          </div>
          <div class="col-xs-4">
            <button class="btn btn-primary btn-block btn-flat" ng-click="Login()">เข้าสู่ระบบ</button>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- jQuery 3 -->
  <script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="./tools/plugins/iCheck/icheck.min.js"></script>
  <!-- SweetDialog JS-->
  <script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>
  <!--AngularJS-->
  <script src="./tools/Js/angular.min.js"></script>
  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  </script>
  <script type="text/javascript">
    var app = angular.module("myApp", []);

    app.controller("myCtrl", function ($scope, $http, $window, $timeout) {
      $scope.Username = "";
      $scope.Password = "";
      $scope.StatusLogin = {
        'Status': false,
        'Message': ''
      };
      $scope.LoginKeyEnterCheck = function(event){
        if(event.keyCode == 13){
          $scope.Login();
        }
      }
      $scope.Login = function () {
        // $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        if ($scope.Username == '' || $scope.Password == '') {
          const toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            width: 300,
            timer: 3000
          });

          toast({
            type: 'error',
            title: 'โปรดกรอกข้อมูลให้ครบถ้วน'
          });
          return;
        }
        data = {
          'Username': $scope.Username,
          'Password': $scope.Password
        };

        $http.post('./ApiService/CheckLogin', data, {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then(function successCallback(response) {
            $scope.StatusLogin = response.data;
            // $scope.$apply();
            if ($scope.StatusLogin['Status'] == true) {
              const toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                width: 300,
                timer: 3000
              });

              toast({
                type: 'success',
                title: 'ยินดีต้อนรับเข้าสู่ระบบตรวจสอบเวลาเข้างาน'
              });
              $timeout(function () {
                $window.location.href = './Date';
              }, 3000);
              // $window.location.href = '../MVCworkTime/Date';
            } else {
              const toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                width: 300,
                timer: 3000
              });

              toast({
                type: 'error',
                title: $scope.StatusLogin['Message']
              });
            }
          })
      };
    });
  </script>
</body>

</html>