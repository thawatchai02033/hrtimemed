<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" />
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif" />

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive>div.dataTables_wrapper>div.row>div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive>div.dataTables_wrapper>div.row>div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td,
        .table>caption+thead>tr:first-child>th,
        .table>colgroup+thead>tr:first-child>td,
        .table>colgroup+thead>tr:first-child>th,
        .table>thead:first-child>tr:first-child>td,
        .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
    <div id="wrapper" class="wrapper">

        <?php require './Views/Header.php'?>
        <!-- Left side column. contains the logo and sidebar -->
        <?php require './Views/Menu.php'?>

        <div class="content-wrapper" ng-controller="myCtrl">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    ตั้งค่าแจ้งเตือนผ่านไลน์
                    <small>Line Application</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> จัดการข้อมูลการเข้า-ออกงาน</a></li>
                    <li class="active">ตั้งค่าแจ้งเตือนผ่านไลน์</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content" ng-show="CheckPermiss">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">แบบฟอร์มลงทะเบียนตั้งค่าการแจ้งเตือนผ่าน Line Application</h3>
                                <div class="box-tools">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding" style="height: 100%;">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div align="center" style="font-weight: bold">
                                                <h4 class="box-title" style="color:green">ขั้นตอนวิธีการสมัครการแจ้งเตือนผ่าน Line Notify</h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12" style="font-size: 16px">
                                                    1. เข้าไปยังบริการ Line Notify <a href="https://notify-bot.line.me/my/" target="_new">https://notify-bot.line.me/my/</a><br>
                                                    2. เข้าสู่ระบบ Line<br>
                                                    3. กดปุ่ม <b>"ออก Token"</b><br>
                                                    4. ใส่ชื่อ Token ตามต้องการ เช่น<b>"แจ้งเดือนการลาหน่วยงาน....."</b><br>
                                                    5. เลือกห้องแชทที่ต้องการส่งข้อความแจ้งเตือน และกดปุ่ม <b>"ออก Token"</b><br>
                                                    6. กดปุ่ม <b>"คัดลอก"</b><br>
                                                    7. นำ Token ที่ได้วางในช่อง Access Token ด้านล่างและกดปุ่มส่ง
                                                </div>

                                                <div class="fg2 tc p5">
                                                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/DxIyXY0zfsI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="margin-top: 80px"></iframe>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row" style="margin-top: 15px">
                                                <div class="col-md-4">
                                                    <p style="text-align: center;color: brown;font-weight: bold;text-align: right;margin-top: 10px">กรุณากรอกรหัส : </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control" ng-model="Token" placeholder="รหัส Token" />
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="number" class="form-control" ng-model="PerID" placeholder="รหัสบุคลากร" />
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <div class="col-md-12" align="center">
                                                        <a class="btn btn-app" style="width: 150px;background-color: #64FFDA;color: #D81B60" ng-click="SaveData()">
                                                            <i class=" fa fa-save"></i> <span style="font-weight: bold">บันทึกข้อมูล</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /. box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <section class="content" ng-show="!CheckPermiss">
                <div style="position: fixed;top: 50%;left: 30%;" align="center">
                    <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
                </div>
            </section>
            <!-- /.content -->
            <div class="loader" align="center" ng-if="Loading">
                <img class="loaderImg" src="./Image/Preloader_2.gif" />
            </div>
        </div>
        <!-- /.content-wrapper -->
        <?php require './Views/Footer.php'?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
    <script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
    <!-- Demo scripts for this page-->
    <script src="./tools/vendor/js/demo/datatables-demo.js"></script>
    <!-- SlimScroll -->
    <script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="./tools/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="./tools/dist/js/demo.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- date-range-picker -->
    <script src="./tools/bower_components/moment/min/moment.min.js"></script>
    <script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap color picker -->
    <script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- Select2 -->
    <script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="./tools/plugins/iCheck/icheck.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="./tools/dist/js/sb-admin-2.js"></script>
    <!-- SweetDialog JS-->
    <script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


    <script src="./tools/Js/angular.min.js"></script>
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
    <script src="./tools/Js/angular-datatables.min.js"></script>

    <!--   Core JS Files   -->
    <script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

    <script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

    <!-- Sharrre libray -->
    <script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
            if (<?php echo $this->model->CheckStatus ?>) {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
                setInterval(function () {
                    $.ajax({
                        type: "POST",
                        url: './ApiService/T_GetCountDataNoteOfLeave',
                        contentType: "application/json",
                        data: JSON.stringify({
                            DepCode: "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>
                        }),
                        error: function (data) {
                            console.log(data);
                        },
                        success: function (data) {
                            var ObjResult = JSON.parse(data);
                            if (ObjResult.Status) {
                                if (ObjResult.Message > 0) {
                                    md.showNotification('bottom', 'right', ObjResult.Message);
                                    $('#showCountLeave').html(ObjResult.Message);
                                } else {
                                    $('#showCountLeave').html('');
                                }
                            }
                        },
                    });
                }, 30000);
            }
        });
    </script>
    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', {
                'placeholder': 'dd/mm/yyyy'
            })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', {
                'placeholder': 'mm/dd/yyyy'
            })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                            'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                        'MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: true,
                showMeridian: false,
                minuteStep: 1
            })
        })
    </script>
    <script>
        var app = angular.module("myApp", ["datatables"]);
        app.controller("myHeader", function ($scope, $http, $window) {
            $scope.LogOut = function () {
                $http.post('./ApiService/LogOut', {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        if (response.data) {
                            $window.location.href = './Login';
                        } else {
                            Swal({
                                type: 'error',
                                title: 'ออกจากระบบไม่สำเร็จ'
                            });
                        };
                    });
            }
        });
    </script>
    <script>
        app.controller("myCtrl", function ($scope, $http, $window, $timeout, $interval, DTOptionsBuilder, $sce) {
            $scope.Token = '';
            $scope.PerID = '';
            $scope.CheckPermiss = true;
            $scope.init = function () {

                data = {
                    'MENU_CODE': '009',
                };

                $http.post('./ApiService/CheckPermissMenu', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        if (response.data.Status) {
                            $scope.CheckPermiss = true;
                        } else {
                            $scope.CheckPermiss = false;
                        }
                    });
            }

            $timeout($scope.init);

            $scope.SaveData = function () {
                Swal({
                    title: 'ยืนยันการทำรายการ',
                    text: "คุณต้องการบันทึกหมายเลข Token สำหรับการแจ้งเตือนผ่านไลน์ใช้หรือไม่ ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'ยกเลิก',
                    confirmButtonText: 'ตกลง'
                }).then((result) => {
                    if (result.value) {
                        if ($scope.Token == '' && $scope.PerID == '') {
                            Swal({
                                type: 'error',
                                title: 'โปรดกรอกข้อมูลให้ครบถ้วน'
                            });
                        } else {
                            data = {
                                'Token': $scope.Token,
                                'PerID': $scope.PerID,
                                'DepCode': "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                            };
                            $http.post('./ApiService/SaveDataRegis', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                .then(function successCallback(response) {
                                    var result = response.data;
                                    if (result.Status) {
                                        Swal({
                                            type: 'success',
                                            title: result.Message
                                        });
                                        $scope.Token = '';
                                        $scope.PerID = '';
                                    } else {
                                        Swal({
                                            type: 'error',
                                            title: result.Message
                                        });
                                    }
                                });
                        }
                    }
                });
            }
        });

        app.filter('timeDate', function () {
            return function (value) {
                // value = new Date(value);
                var Hour = value.split(':')[0];
                var Minutes = value.split(':')[1];
                var ampm = Hour >= 12 ? 'PM' : 'AM';
                Hour = Hour % 12;
                Hour = Hour ? Hour : 12;
                var strTime = Hour + ':' + Minutes + ' ' +
                    ampm;
                return (
                    strTime
                    // value.getHours() >= 13 ? (value.getHours() - 12) : (value.getHours())) + ":" + (
                    // value.getMinutes() < 10 ? '0' : '') + value.getMinutes() + (value.getHours() > 11 ?
                    // 'pm' : 'am'
                );
            }
        });

        app.filter('timeFormat2Digit', function () {
            return function (value) {
                // value = new Date(value);
                var Hour = value.split(':')[0];
                var Minutes = value.split(':')[1];
                var strTime = Hour + ':' + Minutes
                return (
                    strTime
                );
            }
        });
    </script>
</body>

</html>