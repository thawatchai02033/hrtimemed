<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <link href="./tools/customTemplate/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="stylesheet" href="./tools/ui-select/dist/select.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- vuetify   -->
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table > caption + thead > tr:first-child > td,
        .table > caption + thead > tr:first-child > th,
        .table > colgroup + thead > tr:first-child > td,
        .table > colgroup + thead > tr:first-child > th,
        .table > thead:first-child > tr:first-child > td,
        .table > thead:first-child > tr:first-child > th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                สรุปการปฏิบัติงาน
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> การเข้า-ออกงาน</a></li>
                <li class="active">สรุปการปฏิบัติงาน</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div id="app">
                <v-app>
                    <v-content>
                        <div class="text-center">
                            <v-snackbar
                                    v-model="snackbar"
                                    :multi-line="multiLine"
                                    :timeout="timeout"
                                    bottom
                                    left
                                    vertical
                            >
                                <div v-html="text">
                                    {{ text }}
                                </div>
                                <v-btn
                                        color="red"
                                        text
                                        @click="closeDetailsTimEx"
                                >
                                    Close
                                </v-btn>
                            </v-snackbar>
                        </div>
                        <v-container>
                            <v-row>
                                <v-col
                                        cols="12"
                                >
                                    <v-card>
                                        <v-app-bar
                                                color="primary"
                                                dense
                                                dark
                                        >
                                            <v-toolbar-title>สรุปการปฏิบัติงาน</v-toolbar-title>
                                        </v-app-bar>
                                        <v-list-item>
                                            <v-container>
                                                <v-card>
                                                    <v-card-title>
                                                        เลือกช่วงวันที่
                                                    </v-card-title>
                                                    <v-list-item>
                                                        <v-row class="d-flex justify-center">
                                                            <v-col
                                                                    cols="12"
                                                                    md="4">
                                                                <v-select
                                                                        :items="yearData"
                                                                        item-text="year"
                                                                        item-value="year"
                                                                        v-model="yearSelect"
                                                                    label="สรุปการปฏิบัติงานประจำปี"
                                                                >

                                                                </v-select>
                                                            </v-col>
                                                        </v-row>
                                                    </v-list-item>
                                                    <v-list-item class="d-flex">
                                                        <v-row class="d-flex justify-center">
                                                            <v-col
                                                                cols="12"
                                                                md="8">
                                                                <v-data-table
                                                                        :headers="headers"
                                                                        :items="desserts"
                                                                        :items-per-page="5"
                                                                        class="d-block"
                                                                ></v-data-table>
                                                            </v-col>
                                                        </v-row>
                                                    </v-list-item>
                                                </v-card>
                                            </v-container>
                                        </v-list-item>
                                    </v-card>
                                </v-col>
                            </v-row>
                        </v-container>
                    </v-content>
                </v-app>
            </div>
        </section>
        <!-- /.content -->
        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/Js/angular-sanitize.min.js"></script>
<script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>
<script src="./tools/ui-select/dist/select2.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //Date picker
        $('#datepicker2').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    })
</script>

<script>
    new Vue({
        el: '#app',
        vuetify: new Vuetify(),
        data: () => ({
            yearSelect: '2019',
            yearData: [],
            multiLine: true,
            snackbar: false,
            timeout: 5000,
            text: '',
            dates: [],
            MONTHOFYEAR: [
                {
                    name: 'มกราคม',
                    value: '1',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'กุมภาพันธ์',
                    value: '2',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'มีนาคม',
                    value: '3',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'เมษายน',
                    value: '4',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'พฤษภาคม',
                    value: '5',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'มิถุนายน',
                    value: '6',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'กรกฎาคม',
                    value: '7',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'สิงหาคม',
                    value: '8',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'กันยายน',
                    value: '9',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'ตุลาคม',
                    value: '10',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'พฤศจิกายน',
                    value: '11',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                },
                {
                    name: 'ธันวาคม',
                    value: '12',
                    nodata: 0,
                    late: 0,
                    before: 0,
                    absence: 0,
                    leave: 0,
                    data: []
                }
            ],
            DATEOFYEAR: [],
            leaveReport: [],
            Depart: <?php echo $this->Department; ?>,
            Holiday: <?php echo $this->Holiday; ?>,
            headers: [
                {
                    text: '',
                    value: '#',
                },
                { text: 'ปี', value: 'year' },
                { text: 'เดือน', value: 'month' },
                { text: 'ไม่มีข้อมูล', value: 'nodata' },
                { text: 'สาย', value: 'late' },
                { text: 'ขาดงาน', value: 'absence' },
                { text: 'ออกก่อน', value: 'before' },
                { text: 'ลา', value: 'iron' },
                { text: 'ราชการ', value: 'iron' },
            ],
            desserts: [],
            Depart: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>,
        }),
        methods: {
            init() {
                this.CallSocket()
                this.Work_Calulate()
            },
            CallSocket() {
                var socket = io.connect('http://172.29.30.171:3000/');
                // $("#add_status").click(function(){
                //     socket.emit('status added',$("#comment").val());
                // });
                var that = this
                var msg = ''
                var token = ''
                socket.on('refresh feed',function(dataObj){
                    setTimeout(() => {
                        // msg = ''
                        // token = ''
                        that.showDetailsTimEx(dataObj)
                        // msg = obj.NAME + ' ' + obj.SURNAME + ' สแกนบัตร : ' + obj.event_time + ' ประตู : ' + obj.door_name
                        // token = 'eynFYAauCmFe4kgPxdjkYSwv78TvRya23qzvmh53vuM'
                        // socket.emit('Request Notify',{msg: msg, Line_Token: token});
                    }, 5000)
                });
            },
            Work_Calulate() {
                var DateNow = new Date();
                var check_Map_Dep = false;
                this.YearSelect = '' + this.yearSelect;
                this.MonthSelect = '' + (DateNow.getMonth() + 1);

                var date = new Date(this.YearSelect + "-" + this.MonthSelect + "-" + "01");
                var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                var firstDay2 = new Date(date.getFullYear(), date.getMonth(), 1);
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                var options = {
                    weekday: 'long'
                };

                this.MONTHOFYEAR.map((month_I) => {
                    firstDay2 = new Date(date.getFullYear(), 0, 1);
                    lastDay = new Date(date.getFullYear(), 12, 0);
                    for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                        var checkHoliday = false;
                        if (month_I.value == (d.getMonth() + 1)) {
                            this.Holiday.map((dayOff, idx) => {
                                if (dayOff.Date_stop == this.GetTimeToDb(d)) {
                                    month_I.data.push({
                                        'DAY': this.GetTimeToDb(d),
                                        'DAY_NAME': d.toLocaleDateString('en-US', options),
                                        'STATUS': '-',
                                        'Holiday': dayOff,
                                        'Holiday_Status': true,
                                        'Leave_Status': false,
                                        'leave_Detail': null,
                                        'OPD': false,
                                        'OPD_COUNT': 0,
                                        'LeaveCondi': false,
                                        'Work': false,
                                        'Late': false,
                                        'Before': false,
                                        'Nodata': false,
                                        'Absence': false,
                                        'Comment': ''
                                    })
                                    checkHoliday = true;
                                }
                            })

                            if (!checkHoliday) {
                                month_I.data.push({
                                    'DAY': this.GetTimeToDb(d),
                                    'DAY_NAME': d.toLocaleDateString('en-US', options),
                                    'STATUS': '-',
                                    'Holiday': [],
                                    'Holiday_Status': false,
                                    'Leave_Status': false,
                                    'leave_Detail': null,
                                    'OPD': false,
                                    'OPD_COUNT': 0,
                                    'LeaveCondi': false,
                                    'Work': false,
                                    'Late': false,
                                    'Before': false,
                                    'Nodata': false,
                                    'Absence': false,
                                    'Comment': ''
                                })
                            }
                        }
                    }
                })


                console.log(this.MONTHOFYEAR)

                this.GetOPD().then((res) => {
                    if(res){
                        this.GetReportPersonal().then((res) => {
                            console.log(res)
                            if(res) {
                                this.GetLeavePersonal()
                            }
                        }, (reject) => {

                        })
                    }
                },(error) => {

                })
            },
            GetLeavePersonal() {
                return new Promise((success,reject) => {
                    const param = {
                        'Dep_Code': 35210,
                        'Date_Cordi': '2019-01-01',
                        'Date_Cordi2': '2019-12-31',
                        'PERID' : 1040
                    };

                    const headerAxios = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }

                    var that = this

                    axios.post('./ApiService/GetLeaveReportBYPersonal', param, headerAxios)
                        .then((response) => {
                            console.log(response.data)
                            that.leaveReport = response.data;
                            that.MONTHOFYEAR.map((MOY_I) => {
                                MOY_I.data.map((DataI) => {
                                    that.leaveReport.map((Leave_Arr) => {
                                       if (DataI.DAY >= Leave_Arr.T_Leave_Date_Start && DataI.DAY <= Leave_Arr.T_Leave_Date_End) {
                                           switch (Leave_Arr.T_Leave_Type) {
                                               case '0':
                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ลากิจส่วนตัว',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               case '1':
                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ลาป่วย',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               case '2':

                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ลาพักผ่อน',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               case '3':
                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ลาศึกษาต่อ',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               case '4':
                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               case '5':
                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ลาคลอดบุตร',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               case '6':
                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               case '7':
                                                   MOY_I.leave += 1;
                                                   DataI.Leave_Status = true;;
                                                   DataI.leave_Detail = {
                                                       'PERID': Leave_Arr.PERID,
                                                       'T_Leave_Type': 'ไปราชการ',
                                                       'No_Id': Leave_Arr.No_Id,
                                                       'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                       'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                       'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                       'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                       'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                       'T_All_Summary': Leave_Arr.T_All_Summary,
                                                       'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                   }
                                                   break;
                                               default:
                                                   break;
                                           }
                                       }
                                    });
                                });
                            });

                            console.log(that.MONTHOFYEAR)

                            /*angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                workNoDataOut = 0;
                                workNoDataIn = 0;

                                angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                    if (Date_I.DAY >= $scope.GetTimeToDb(new Date(date.getFullYear(), date.getMonth(), date.getDate()))) {
                                        workNoDataIn += 1;
                                        $scope.workNoDataInSum += 1;
                                        Report_Arr.workNoDataIn = workNoDataIn;
                                        Report_Arr.StatusDetails.push({
                                            'Day': Date_I.DAY,
                                            'Time': '',
                                            'Status': 'ไม่มีเวลาเข้างาน',
                                            'Status2': 'In',
                                            'Comment': '',
                                            'Comment_Date': ''
                                        });
                                    }
                                });

                                angular.forEach($scope.workReport, function (Work_Arr, idx) {
                                    if (Work_Arr.PERID == Report_Arr.PERID) {

                                        if ((parseInt(Report_Arr.POS_WORK)) >= 551 && ((parseInt(Report_Arr.POS_WORK)) <= 558)
                                            || (parseInt(Report_Arr.POS_WORK) == 800) || (parseInt(Report_Arr.POS_WORK) == 221)
                                            || (parseInt(Report_Arr.POS_WORK) == 211) || (parseInt(Report_Arr.POS_WORK) == 212)) {
                                            if (Work_Arr.A_Time_in2 == '' && Work_Arr.A_Time_ho == '' &&
                                                Work_Arr.A_Time_hn == '' && Work_Arr.A_Time_out2 == '') {
                                                angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                    if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                        if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                        } else if (Date_I.Holiday_Status) {
                                                        } else if (Date_I.Leave_Status || Date_I.leave_Detail != null) {
                                                        } else {
                                                            if (!Date_I.OPD) {
                                                                workNoDataIn += 1;
                                                                $scope.workNoDataInSum += 1;
                                                                Report_Arr.workNoDataIn = workNoDataIn;
                                                                Report_Arr.StatusDetails.push({
                                                                    'Day': Work_Arr.A_Datetime,
                                                                    'Time': Work_Arr.A_Time_in2,
                                                                    'Status': 'ไม่มีเวลาเข้างาน',
                                                                    'Status2': 'In',
                                                                    'Comment': '',
                                                                    'Comment_Date': ''
                                                                });
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        } else {
                                            if (Work_Arr.A_Time_in2 == '') {
                                                angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                    if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                        if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                        } else if (Date_I.Holiday_Status) {
                                                        } else if (Date_I.Leave_Status || Date_I.leave_Detail != null) {
                                                        } else {
                                                            workNoDataIn += 1;
                                                            $scope.workNoDataInSum += 1;
                                                            Report_Arr.workNoDataIn = workNoDataIn;
                                                            Report_Arr.StatusDetails.push({
                                                                'Day': Work_Arr.A_Datetime,
                                                                'Time': Work_Arr.A_Time_in2,
                                                                'Status': 'ไม่มีเวลาเข้างาน',
                                                                'Status2': 'In',
                                                                'Comment': '',
                                                                'Comment_Date': ''
                                                            });
                                                        }
                                                    }
                                                });
                                            } else {
                                                if (Work_Arr.A_Status_in2 != 'สาย') {
                                                    Report_Arr.StatusDetails.push({
                                                        'Day': Work_Arr.A_Datetime,
                                                        'Time': $scope.CheckDocMaster(Report_Arr, Work_Arr.A_Time_in2, Work_Arr.A_Status_in2),
                                                        'Status': '',
                                                        'Status2': 'In',
                                                        'Comment': '',
                                                        'Comment_Date': ''
                                                    });
                                                }
                                            }
                                        }
                                    }
                                });
                            });

                            angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                angular.forEach(Report_Arr.StatusDetails, function (status_item) {
                                    angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                        if (Date_I.DAY == status_item.Day) {
                                            if (Date_I.OPD) {
                                                if (status_item.Time.indexOf('OPD') < 0) {
                                                    status_item.Time += ', OPD'
                                                }
                                            }
                                        }
                                    });
                                });
                            });

                            data = {
                                'Dep_Code': $scope.DepartSelect,
                                'Date_Cordi': DateStart,
                                'Date_Cordi2': DateEnd,
                            };

                            $http.post('./ApiService/GetCommentReport', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                                .then(function successCallback(response) {
                                    $scope.CommentReport = response.data;
                                    if ($scope.CommentReport.length > 0) {
                                        angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                            angular.forEach(Report_Arr.StatusDetails, function (statusDetail) {
                                                angular.forEach($scope.CommentReport, function (CommentArr) {
                                                    if (Report_Arr.PERID == CommentArr.PERID) {
                                                        if (statusDetail.Day == CommentArr.Comment_Date) {
                                                            statusDetail.Comment = CommentArr.Comment_Details;
                                                            statusDetail.Comment_Date = CommentArr.Comment_Date;
                                                            Report_Arr.CheckComment = true;
                                                        }
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });

                            angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                angular.forEach($scope.More_Leave_Data, function (Leave_Item) {
                                    if (Report_Arr.PERID == Leave_Item.PERID) {
                                        Report_Arr.LeaveStatusDetails2 = Leave_Item.L_DATA;
                                    }
                                });
                            });

                            angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                var i = 0;
                                // if (Report_Arr.PERID == '1638') {
                                if (Report_Arr.LeaveStatusDetails2.length > 0) {

                                    angular.forEach(Report_Arr.LeaveStatusDetails2, function (Leave_Item) {
                                        var date1 = new Date(Leave_Item.Data.leave1.T_Leave_Date_Start);
                                        var date2 = new Date(Leave_Item.Data.leave2.T_Leave_Date_End);

                                        for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                            angular.forEach(Report_Arr.DATEOFYEAR, function (dayItem) {
                                                if (dayItem.DAY == $scope.GetTimeToDb(d)) {
                                                    dayItem.LeaveCondi = true;
                                                }
                                            })
                                        }

                                        i += parseFloat(Leave_Item.Count)
                                    });

                                    $scope.ArrayLeaveOther = [];

                                    angular.forEach(Report_Arr.DATEOFYEAR, function (dayItem) {
                                        if (dayItem.Leave_Status && !dayItem.LeaveCondi) {
                                            angular.forEach(Report_Arr.LeaveStatusDetails, function (Leave_ItemAll, idx_L_all) {
                                                if (dayItem.leave_Detail.No_Id == Leave_ItemAll.No_Id) {
                                                    $scope.ArrayLeaveOther.push(Leave_ItemAll)
                                                }
                                            })
                                        }
                                    })

                                    $scope.ArrayLeaveOther = UniqueArraybyId($scope.ArrayLeaveOther,
                                        "No_Id");

                                    function UniqueArraybyId(collection, keyname) {
                                        var output = [],
                                            keys = [];

                                        angular.forEach(collection, function (item) {
                                            var key = item[keyname];
                                            if (keys.indexOf(key) === -1) {
                                                keys.push(key);
                                                output.push(item);
                                            }
                                        });
                                        return output;
                                    };

                                    if ($scope.ArrayLeaveOther.length > 0) {
                                        angular.forEach($scope.ArrayLeaveOther, function (Leave_Item) {
                                            var CheckCondiFrist = true;
                                            if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY && Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                            } else if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY) {
                                                var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                            } else if (Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                            } else {
                                                var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                            }

                                            date1.setHours(0, 0, 0, 0);
                                            date2.setHours(0, 0, 0, 0);

                                            if (parseFloat(Leave_Item.T_Work_Summary) > 0) {
                                                if (date1.getTime() == date2.getTime()) {
                                                    i++;
                                                    if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                        i -= 0.5;
                                                    } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                        i -= 0.5;
                                                    }
                                                } else {
                                                    for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                        angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I) {
                                                                if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                                    if ($scope.GetTimeToDb(d) == $scope.GetTimeToDb(date2)) {
                                                                        if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (เช้า)') {
                                                                            i += 0.5;
                                                                        } else if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {
                                                                            i += 0.5;
                                                                        } else {
                                                                            i += 1;
                                                                        }
                                                                    } else {
                                                                        if (CheckCondiFrist) {
                                                                            if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                                i += 0.5;
                                                                            } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                                i += 0.5;
                                                                            } else {
                                                                                i += 1;
                                                                            }
                                                                            CheckCondiFrist = false;
                                                                        } else {
                                                                            i += 1;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        );
                                                    }
                                                }
                                            }
                                        })
                                    }

                                } else {
                                    if (Report_Arr.LeaveStatusDetails.length > 0) {
                                        angular.forEach(Report_Arr.LeaveStatusDetails, function (Leave_Item) {
                                            var CheckCondiFrist = true;
                                            if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY && Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                            } else if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY) {
                                                var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                            } else if (Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                            } else {
                                                var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                            }

                                            date1.setHours(0, 0, 0, 0);
                                            date2.setHours(0, 0, 0, 0);

                                            if (parseFloat(Leave_Item.T_Work_Summary) > 0) {
                                                if (date1.getTime() == date2.getTime()) {
                                                    i++;
                                                    if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                        i -= 0.5;
                                                    } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                        i -= 0.5;
                                                    }
                                                } else {
                                                    for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                        angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I) {
                                                                if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                                    if ($scope.GetTimeToDb(d) == $scope.GetTimeToDb(date2)) {
                                                                        if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (เช้า)') {
                                                                            i += 0.5;
                                                                        } else if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {
                                                                            i += 0.5;
                                                                        } else {
                                                                            i += 1;
                                                                        }
                                                                    } else {
                                                                        if (CheckCondiFrist) {
                                                                            if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                                i += 0.5;
                                                                            } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                                i += 0.5;
                                                                            } else {
                                                                                i += 1;
                                                                            }
                                                                            CheckCondiFrist = false;
                                                                        } else {
                                                                            i += 1;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        );
                                                    }
                                                }
                                            }
                                        })
                                    }
                                }
                                // }
                                Report_Arr.LeaveSum = i;
                                Report_Arr.DateOfMonth = Report_Arr.DATEOFYEAR.length;
                                Report_Arr.WorkDaySum = parseFloat(Report_Arr.DATEOFYEAR.length) - (i + parseFloat(Report_Arr.workNoDataIn) + parseFloat(Report_Arr.workAbsence));
                                Report_Arr.WorkDaySum_PER = ((parseFloat(Report_Arr.WorkDaySum) / parseFloat(Report_Arr.DateOfMonth)) * 100);
                            });

                            // $scope.Loading = false;
                            // $scope.ShowLeaveCheck($scope.ReportSummaryCopy[0]);

                            $scope.AddWorkingHour = [];

                            angular.forEach($scope.ReportSummaryCopy, function (Report, idf_Ri) {
                                if ((parseInt(Report.POS_WORK)) >= 551 && ((parseInt(Report.POS_WORK)) <= 558)
                                    || (parseInt(Report.POS_WORK) == 800) || (parseInt(Report.POS_WORK) == 221)
                                    || (parseInt(Report.POS_WORK) == 211) || (parseInt(Report.POS_WORK) == 212)) {
                                    $scope.AddWorkingHour.push($scope.funcAddWorking(Report, idf_Ri, $scope.ReportSummaryCopy.length - 1))
                                }
                            });

                            data = {
                                'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                            };
                            $http.post('./ApiService/GetRefPersonal', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                                .then(function successCallback(response) {
                                    if (response.data.length > 0) {
                                        angular.forEach($scope.ReportSummaryCopy, function (data_I, idx) {
                                            angular.forEach(response.data, function (Ref_I) {
                                                if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == $scope.DepartSelect) {
                                                } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == $scope.DepartSelect) {
                                                    $scope.ReportSummaryCopy.splice(idx, 1)
                                                }
                                            })
                                        })
                                    }
                                });

                            angular.forEach($scope.ReportSummaryCopy, function (report_i) {
                                report_i.StatusDetails = UniqueArraybyId(report_i.StatusDetails,
                                    "Day");
                            })

                            function UniqueArraybyId(collection, keyname) {
                                var output = [],
                                    keys = [];

                                angular.forEach(collection, function (item) {
                                    var key = item[keyname];
                                    if (keys.indexOf(key) === -1) {
                                        keys.push(key);
                                        output.push(item);
                                    }
                                });
                                return output;
                            };

                            $scope.ReportSummary = $scope.ReportSummaryCopy
                            $scope.CheckApprove();*/

                            // console.log($scope.ReportSummary)
                        });
                })
            },
            GetReportPersonal() {
                return new Promise((success,reject) => {
                    const param = {
                        DepCode: 35210,
                        Date_Cordi: '2019-01-01',
                        Date_Cordi2: '2019-12-31',
                        PERID: 1040
                    }
                    const headerAxios = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }
                    axios.post('./ApiService/GetWorkReportBYPersonal', param, headerAxios).then(res => {
                        if (res.data) {
                            res.data.Work_Late.map((workPerson_I) => {
                                if (workPerson_I.A_Status_in2 == 'สาย') {
                                    this.MONTHOFYEAR.map((MOY_I) => {
                                        MOY_I.data.map((DataI) => {
                                            if(DataI.DAY == workPerson_I.A_Datetime) {
                                                if (DataI.DAY_NAME == 'Saturday' || DataI.DAY_NAME == 'Sunday') {
                                                } else if (DataI.Holiday_Status) {
                                                } else if (DataI.Leave_Status || DataI.leave_Detail != null) {
                                                } else {
                                                    MOY_I.late++
                                                    DataI.Late = true
                                                }
                                            }
                                        })
                                    })
                                } else if (workPerson_I.A_Status_in2 == 'ขาดงาน') {
                                    this.MONTHOFYEAR.map((MOY_I) => {
                                        MOY_I.data.map((DataI) => {
                                            if(DataI.DAY == workPerson_I.A_Datetime) {
                                                if (DataI.DAY_NAME == 'Saturday' || DataI.DAY_NAME == 'Sunday') {
                                                } else if (DataI.Holiday_Status) {
                                                } else if (DataI.Leave_Status || DataI.leave_Detail != null) {
                                                } else {
                                                    MOY_I.absence++
                                                    DataI.Absence = true
                                                }
                                            }
                                        })
                                    })
                                } else if(workPerson_I.A_Status_in2 == 'ออกก่อน') {
                                    this.MONTHOFYEAR.map((MOY_I) => {
                                        MOY_I.data.map((DataI) => {
                                            if(DataI.DAY == workPerson_I.A_Datetime) {
                                                if (DataI.DAY_NAME == 'Saturday' || DataI.DAY_NAME == 'Sunday') {
                                                } else if (DataI.Holiday_Status) {
                                                } else if (DataI.Leave_Status || DataI.leave_Detail != null) {
                                                } else {
                                                    MOY_I.before++
                                                    DataI.Before = true
                                                }
                                            }
                                        })
                                    })
                                }
                                // else if(workPerson_I.A_Status_in2 == '') {
                                //     this.MONTHOFYEAR.map((MOY_I) => {
                                //         MOY_I.data.map((DataI) => {
                                //             if(DataI.DAY == workPerson_I.A_Datetime) {
                                //                 if (DataI.DAY_NAME == 'Saturday' || DataI.DAY_NAME == 'Sunday') {
                                //                 } else if (DataI.Holiday_Status) {
                                //                 } else if (DataI.Leave_Status || DataI.leave_Detail != null) {
                                //                 } else {
                                //                     DataI.Nodata = true
                                //                 }
                                //             }
                                //         })
                                //     })
                                // }
                            })

                            success(true)
                        } else {
                            reject(false)
                        }
                    })
                })
            },
            GetOPD() {
                return new Promise((success, reject) => {
                    const param = {
                        Dep_Code: 35210,
                        Date_Cordi: '2019-01-01',
                        Date_Cordi2: '2019-12-31',
                        PERID: 1040
                    }
                    const headerAxios = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }
                    axios.post('./ApiService/GetDocTorSummaryBy_P', param, headerAxios).then(response => {
                        if (response.data.length > 0) {
                            console.log(response.data)
                            this.MONTHOFYEAR.map((MOY_I) => {
                                MOY_I.data.map((DataI) => {
                                    response.data.map((DocTor_Arr) => {
                                        if (MOY_I.DAY == DocTor_Arr.day_of_month) {
                                            MOY_I.OPD = true;
                                            MOY_I.OPD_COUNT = DocTor_Arr.count;
                                        }
                                    });
                                })
                            });
                            success(true)
                        } else {
                            success(true)
                        }
                    })
                })
            },
            GetTimeToDb(date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1;

                var yyyy = date.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return date = yyyy + '-' + mm + '-' + dd;
            },
            showDetailsTimEx(dataObj) {
                this.snackbar = true
                this.text = "<div align='center' style='width: 100%'>" +
                    "<img src='https://mednet.psu.ac.th/Personal/ShowPictNew.php?pid=" + dataObj.PERID + "' style='border-radius: 40%;height: 120px;border: 3px solid;color: tomato' onerror=\"this.onerror=null;this.src='https://w7.pngwing.com/pngs/876/963/png-transparent-silhouette-human-head-person-shadow-face-animals-logo.png';\" /><br />" +
                    "<span>" +dataObj.NAME+ " " + dataObj.SURNAME+ " ( " + dataObj.PERID + " )</span><br />" +
                    "<span>ตำแนห่ง : " +dataObj.PosName+ "</span><br />" +
                    "<span>หน่วยงาน : " +dataObj.Dep_name+ "</span><br />" +
                    "<span>วัน/เวลา : " +dataObj.event_time+"</span><br />" +
                    "<span>พื้นที่ : " +dataObj.area_name+ "</span><br />" +
                    "<span>ประตู : " +dataObj.door_name+ "</span><br />" +
                    "</div>"
            },
            closeDetailsTimEx() {
                this.snackbar = false
            },
            getYear() {
                for (var i = (parseInt(new Date().getFullYear()) - 1); i <= parseInt(new Date().getFullYear()); i++) {
                    this.yearData.push({
                        id: this.yearData.length + 1,
                        year: i
                    })
                }
            }
        },
        mounted() {
            this.getYear()
            this.init()
        }
    })
</script>

<script>
    var app = angular.module("myApp", ["datatables", "ui.bootstrap", 'ngAnimate']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<!--<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, DTOptionsBuilder, $uibModal) {
        $scope.Depart = <?php /*echo $this->Department; */?>;
        $scope.Holiday = <?php /*echo $this->Holiday; */?>;
        $scope.personDep = [];
        $scope.StatusAbsense = []; // ขาดงาน
        $scope.StatusLate = []; // สาย
        $scope.StatusOut = []; // ออกก่อน
        $scope.StatusLeave = []; // ลา
        $scope.StatusNoData = []; // ไม่มีข้อมูล
        $scope.StatusNoApprove = []; // ไม่มีการยืนยันข้อมูล
        $scope.StatusApprove = []; // ยืนยันข้อมูล
        $scope.DepartData = [];
        $scope.CheckSelect = false;
        $scope.CheckDepDupe = false;
        $scope.CheckCompare = false;

        $scope.init = function () {
            var dateNow = new Date();
            $scope.PrsonSelect = "" + <?php /*echo $_SESSION['PERID'] */?>;
            $scope.DepartSelect = "" + <?php /*echo($PersonalData["DataPerson"]["Dep_Code"]); */?>;
            $scope.MonthSelect = '' + (dateNow.getMonth() + 1);
            $scope.YearSelect = '' + dateNow.getFullYear();
            if ($scope.Depart[0].DepartAdmin.length > 0) {
                angular.forEach($scope.Depart[0].DepartAdmin, function (Depart_I) {
                    $scope.DepartData.push({
                        'Dep_Code': Depart_I.Dep_Code,
                        'Edit_code': Depart_I.Edit_code,
                        'Dep_name': Depart_I.Dep_name,
                        'Dep_Group_name': Depart_I.Dep_Group_name,
                        'Type': 'Admin'
                    });
                });
            }
            if ($scope.Depart[0].DepartUser.length > 0) {
                angular.forEach($scope.Depart[0].DepartUser, function (Depart_I) {
                    $scope.DepartData.push({
                        'Dep_Code': Depart_I.Dep_Code,
                        'Edit_code': Depart_I.Edit_code,
                        'Dep_name': Depart_I.Dep_name,
                        'Dep_Group_name': Depart_I.Dep_Group_name,
                        'Type': 'User'
                    });
                });
            }

            $scope.update();
            $scope.getReportOfMonth();
        }

        $scope.update = function () {
            $scope.CheckCompare = false;
            data = {
                'DepCode': $scope.DepartSelect,
            };
            $http.post('./ApiService/GetPerson_MonthSum', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.personDep = response.data;
                    $scope.Loading = false;
                    $scope.CheckSelect = false;
                    $scope.CheckDepDupe = false;
                    var checkCordi = false;
                    angular.forEach($scope.personDep, function (per_Item, idx) {
                        if (per_Item.PERID == "" + <?php /*echo $_SESSION['PERID'] */?>) {
                            $scope.PrsonSelect = "" + <?php /*echo $_SESSION['PERID'] */?>;
                            $scope.CheckSelect = true;
                            angular.forEach($scope.Depart[0].DepartUser, function (DepartU_I) {
                                if ($scope.Depart[0].DepartAdmin.length > 0) {
                                    angular.forEach($scope.Depart[0].DepartAdmin, function (DepartA_I, idx_Admin) {
                                        if (DepartU_I.Dep_Code == DepartA_I.Dep_Code) {
                                            checkCordi = true;
                                        } else {
                                            if (idx_Admin === $scope.Depart[0].DepartAdmin.length - 1 && !checkCordi) {
                                                $scope.CheckDepDupe = true;
                                            }
                                        }
                                    });
                                } else {
                                    $scope.CheckDepDupe = true;
                                }
                            });
                        } else {
                            if (idx === $scope.personDep.length - 1 && !$scope.CheckSelect) {
                                $scope.PrsonSelect = '';
                            }
                        }
                    })
                    $scope.CheckCompare = true;
                });
        }

        $scope.updatePersonToDB = function () {
            var PERID = angular.element(document.querySelector('#PersonCode'));
            $scope.PrsonSelect = PERID[0].options[PERID[0].selectedIndex].value;
        }

        $timeout($scope.init)

        $scope.getReportOfMonth = function () {

            $scope.daysOfYear = [];
            $scope.ReportSummary = [];
            $scope.StatusAbsense = []; // ขาดงาน
            $scope.StatusLate = []; // สาย
            $scope.StatusOut = []; // ออกก่อน
            $scope.StatusLeave = []; // ลา
            $scope.StatusNoData = []; // ไม่มีข้อมูล
            $scope.StatusNoApprove = []; // ไม่มีการยืนยันข้อมูล
            $scope.StatusApprove = []; // ยืนยันข้อมูล

            $scope.MonthShow = $scope.MonthSelect;
            $scope.YearShow = $scope.YearSelect;
            monthSelect = '';
            if ($scope.MonthSelect < 10) {
                monthSelect = '0' + $scope.MonthSelect;
            } else {
                monthSelect = $scope.MonthSelect;
            }

            var date = new Date($scope.YearSelect + "-" + monthSelect + "-" + "01");
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var firstDay2 = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            var options = {
                weekday: 'long'
            };
            for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                $scope.daysOfYear.push({
                    'DAY': $scope.GetTimeToDb(d),
                    'DAY_NAME': d.toLocaleDateString('en-US', options),
                    'STATUS': '-'
                });
            }


            var DateStart = $scope.GetTimeToDb(firstDay);
            var DateEnd = $scope.GetTimeToDb(lastDay);

            data = {
                'DepCode': $scope.DepartSelect,
                'Date_Cordi': DateStart,
                'Date_Cordi2': DateEnd,
                'PERID': $scope.PrsonSelect
            };

            $http.post('./ApiService/GetWorkReportBYPersonal', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.reportWorkPer = response.data;
                    var name = '';
                    if ($scope.reportWorkPer.length <= 0) {
                        Swal({
                            type: 'error',
                            title: 'ไม่พบข้อมูล',
                            text: 'การยืนยันข้อมูลการปฏิบัติงานในเดือนนี้'
                        })
                    } else {
                        angular.forEach($scope.personDep, function (PerDataItem) {
                            if (PerDataItem.PERID == $scope.PrsonSelect) {
                                name = PerDataItem.NAME + ' ' + PerDataItem.SURNAME;
                            }
                        });
                        angular.forEach($scope.reportWorkPer.Work_Late, function (WorkItem) {

                            if (WorkItem.A_Time_in2 != '' && WorkItem.A_Time_in2 != null) {
                                if (WorkItem.A_Status_in2 == 'สาย') {
                                    var checkDayOff = true;
                                    if ((new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options) != 'Saturday') && (new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options) != 'Sunday')) {
                                        angular.forEach($scope.Holiday, function (dayOff, idx) {
                                            if (dayOff.Date_stop == WorkItem.A_Datetime) {
                                                checkDayOff = false;
                                            } else {
                                                if (idx === $scope.Holiday.length - 1 && checkDayOff) {
                                                    $scope.StatusLate.push({
                                                        'PERID': $scope.PrsonSelect,
                                                        'NAME': name,
                                                        'DATE': WorkItem.A_Datetime,
                                                        'TIME': WorkItem.A_Time_in2,
                                                        'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options),
                                                        'Comment': ''
                                                    });
                                                }
                                            }
                                        })

                                    }

                                } else if (WorkItem.A_Status_in2 == 'ออกก่อน') {
                                    $scope.StatusOut.push({
                                        'PERID': $scope.PrsonSelect,
                                        'NAME': name,
                                        'DATE': WorkItem.A_Datetime,
                                        'TIME': WorkItem.A_Time_in2,
                                        'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options),
                                        'Comment': ''
                                    });
                                } else if (WorkItem.A_Status_in2 == 'ขาดงาน') {
                                    var checkDayOff = true;
                                    if ((new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options) != 'Saturday') && (new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options) != 'Sunday')) {
                                        angular.forEach($scope.Holiday, function (dayOff, idx) {
                                            if (dayOff.Date_stop == WorkItem.A_Datetime) {
                                                checkDayOff = false;
                                            } else {
                                                if (idx === $scope.Holiday.length - 1 && checkDayOff) {
                                                    $scope.StatusAbsense.push({
                                                        'PERID': $scope.PrsonSelect,
                                                        'NAME': name,
                                                        'DATE': WorkItem.A_Datetime,
                                                        'TIME': WorkItem.A_Time_in2,
                                                        'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options),
                                                        'Comment': ''
                                                    });
                                                }
                                            }
                                        })
                                    }

                                } else {

                                }
                            } else {
                                var checkDayOff = true;
                                if ((new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options) != 'Saturday') && (new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options) != 'Sunday')) {
                                    angular.forEach($scope.Holiday, function (dayOff, idx) {
                                        if (dayOff.Date_stop == WorkItem.A_Datetime) {
                                            checkDayOff = false;
                                        } else {
                                            if (idx === $scope.Holiday.length - 1 && checkDayOff) {
                                                $scope.StatusNoData.push({
                                                    'PERID': $scope.PrsonSelect,
                                                    'NAME': name,
                                                    'DATE': WorkItem.A_Datetime,
                                                    'TIME': 'ไม่มีข้อมูลเวลางานเช้า',
                                                    'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options),
                                                    'Comment': ''
                                                });
                                            }
                                        }
                                    })
                                }
                                // if (WorkItem.A_Status_in2 == 'ขาดงาน') {
                                //     $scope.StatusAbsense.push({
                                //         'PERID': $scope.PrsonSelect,
                                //         'NAME': name,
                                //         'DATE': WorkItem.A_Datetime,
                                //         'TIME': WorkItem.A_Time_in2,
                                //         'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options),
                                //         'Comment': ''
                                //     });
                                // } else {
                                //     $scope.StatusNoData.push({
                                //         'PERID': $scope.PrsonSelect,
                                //         'NAME': name,
                                //         'DATE': WorkItem.A_Datetime,
                                //         'TIME': 'ไม่มีข้อมูลเวลางานเช้า',
                                //         'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options),
                                //         'Comment': ''
                                //     });
                                // }
                            }

                            // if (WorkItem.A_Time_ho != '' && WorkItem.A_Time_ho != null) {
                            //     if (WorkItem.A_Status_ho == 'สาย') {
                            //         $scope.StatusLate.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': WorkItem.A_Time_ho
                            //         });
                            //     } else if (WorkItem.A_Status_ho == 'ออกก่อน') {
                            //         $scope.StatusOut.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': WorkItem.A_Time_ho
                            //         });
                            //     } else if (WorkItem.A_Status_ho == 'ขาดงาน') {

                            //     } else {

                            //     }
                            // } else {
                            //     if (WorkItem.A_Status_ho == 'ขาดงาน') {

                            //     } else {
                            //         $scope.StatusNoData.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': 'ไม่มีข้อมูลเวลาออกพักเที่ยง'
                            //         });
                            //     }
                            // }

                            // if (WorkItem.A_Time_hn != '' && WorkItem.A_Time_hn != null) {
                            //     if (WorkItem.A_Status_hn == 'สาย') {
                            //         $scope.StatusLate.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': WorkItem.A_Time_hn
                            //         });
                            //     } else if (WorkItem.A_Status_hn == 'ออกก่อน') {
                            //         $scope.StatusOut.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': WorkItem.A_Time_hn
                            //         });
                            //     } else if (WorkItem.A_Status_hn == 'ขาดงาน') {

                            //     } else {

                            //     }
                            // } else {
                            //     if (WorkItem.A_Status_hn == 'ขาดงาน') {

                            //     } else {
                            //         $scope.StatusNoData.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': 'ไม่มีข้อมูลเวลาเข้าพักเที่ยง'
                            //         });
                            //     }
                            // }

                            // if (WorkItem.A_Time_out2 != '' && WorkItem.A_Time_out2 != null) {
                            //     if (WorkItem.A_Status_out2 == 'สาย') {
                            //         $scope.StatusLate.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': WorkItem.A_Time_out2,
                            //             'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options)
                            //         });
                            //     } else if (WorkItem.A_Status_out2 == 'ออกก่อน') {
                            //         $scope.StatusOut.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': WorkItem.A_Time_out2,
                            //             'DAY_NAME': new Date(WorkItem.A_Datetime).toLocaleDateString('en-US', options)
                            //         });
                            //     } else if (WorkItem.A_Status_out2 == 'ขาดงาน') {

                            //     } else {

                            //     }
                            // } else {
                            //     if (WorkItem.A_Status_out2 == 'ขาดงาน') {

                            //     } else {
                            //         $scope.StatusNoData.push({
                            //             'PERID': $scope.PrsonSelect,
                            //             'NAME': name,
                            //             'DATE': WorkItem.A_Datetime,
                            //             'TIME': 'ไม่มีข้อมูลเวลาออกงาน'
                            //         });
                            //     }
                            // }
                        });

                        if ($scope.reportWorkPer.Comment.length > 0 && $scope.StatusLate.length > 0) {
                            angular.forEach($scope.reportWorkPer.Comment, function (Comment_Item) {
                                angular.forEach($scope.StatusLate, function (Late_Item) {
                                    if (Late_Item.DATE == Comment_Item.Comment_Date) {
                                        Late_Item.Comment = Comment_Item.Comment_Details;
                                    }
                                });
                                angular.forEach($scope.StatusNoData, function (Nodata_Item) {
                                    if (Nodata_Item.DATE == Comment_Item.Comment_Date) {
                                        Nodata_Item.Comment = Comment_Item.Comment_Details;
                                    }
                                });
                            });
                        }

                        data = {
                            'DepCode': $scope.DepartSelect,
                            'Date_Cordi': DateStart,
                            'Date_Cordi2': DateEnd,
                            'PERID': $scope.PrsonSelect
                        };


                        $http.post('./ApiService/GetLeaveReportBYPersonal', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.LeavePersonal = response.data;
                                var leaveStatus = ''
                                angular.forEach($scope.LeavePersonal, function (LeaveItem) {
                                    switch (LeaveItem.T_Leave_Type) {
                                        case "0":
                                            leaveStatus = "ลากิจส่วนตัว"
                                            break;
                                        case "1":
                                            leaveStatus = "ลาป่วย"
                                            break;
                                        case "2":
                                            leaveStatus = "ลาพักผ่อน"
                                            break;
                                        case "3":
                                            leaveStatus = "ลาศึกษาต่อ"
                                            break;
                                        case "4":
                                            leaveStatus = "ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย"
                                            break;
                                        case "5":
                                            leaveStatus = "ลาคลอดบุตร"
                                            break;
                                        case "6":
                                            leaveStatus = "ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์"
                                            break;
                                        default:
                                            leaveStatus = "อื่นๆ"
                                    }
                                    $scope.StatusLeave.push({
                                        'PERID': $scope.PrsonSelect,
                                        'NAME': name,
                                        'DATE_START': LeaveItem.T_Leave_Date_Start,
                                        'DATE_START_S': LeaveItem.T_Day_Type_Start,
                                        'DATE_END': LeaveItem.T_Leave_Date_End,
                                        'DATE_END_S': LeaveItem.T_Day_Type_End,
                                        'L_TIME': LeaveItem.T_Leave_Time,
                                        'WORK_SUM': LeaveItem.T_Work_Summary,
                                        'LEAVE_TYPE': leaveStatus,
                                    });
                                })

                                angular.forEach($scope.StatusLeave, function (LeaveItem, idx_L) {
                                    if ($scope.StatusLate.length > 0) {
                                        var checkLate = true;
                                        while (checkLate) {
                                            if ($scope.StatusLate.length > 0) {
                                                angular.forEach($scope.StatusLate, function (LeteItem, idx_W) {
                                                    if (LeteItem.DATE >= LeaveItem.DATE_START &&
                                                        LeteItem.DATE <= LeaveItem.DATE_END) {
                                                        $scope.StatusLate.splice(idx_W, 1);
                                                    }
                                                    if (idx_W === $scope.StatusLate.length - 1) {
                                                        checkLate = false;
                                                    }
                                                });
                                            } else {
                                                checkLate = false;
                                            }
                                        }
                                    }

                                    if ($scope.StatusAbsense.length > 0) {
                                        var checkAbsense = true;
                                        while (checkAbsense) {
                                            if ($scope.StatusAbsense.length > 0) {
                                                angular.forEach($scope.StatusAbsense, function (AbsenseItem, idx_W) {
                                                    if (AbsenseItem.DATE >= LeaveItem.DATE_START &&
                                                        AbsenseItem.DATE <= LeaveItem.DATE_END) {
                                                        $scope.StatusAbsense.splice(idx_W, 1);
                                                        return;
                                                    }
                                                    if (idx_W === $scope.StatusAbsense.length - 1) {
                                                        checkAbsense = false;
                                                    }
                                                });
                                            } else {
                                                checkAbsense = false;
                                            }
                                        }
                                    }

                                    if ($scope.StatusNoData.length > 0) {
                                        var checkNodata = true;
                                        while (checkNodata) {
                                            if ($scope.StatusNoData.length > 0) {
                                                angular.forEach($scope.StatusNoData, function (NoDataItem, idx_W) {
                                                    if (NoDataItem.DATE >= LeaveItem.DATE_START &&
                                                        NoDataItem.DATE <= LeaveItem.DATE_END) {
                                                        $scope.StatusNoData.splice(idx_W, 1);
                                                        return;
                                                    }
                                                    if (idx_W === $scope.StatusNoData.length - 1) {
                                                        checkNodata = false;
                                                    }
                                                });
                                            } else {
                                                checkNodata = false;
                                            }
                                        }
                                    }
                                });
                            });
                        var dateCheck = '';
                        angular.forEach($scope.daysOfYear, function (dayOfyear) {
                            dateCheck = '';
                            angular.forEach($scope.reportWorkPer, function (WorkItem, idx) {
                                if (dayOfyear.DAY == WorkItem.A_Datetime) {
                                    dateCheck = WorkItem.A_Datetime;
                                }
                                if (idx === $scope.reportWorkPer.length - 1 && dateCheck != '') {
                                    $scope.StatusApprove.push({
                                        'PERID': $scope.PrsonSelect,
                                        'NAME': name,
                                        'DATE': dayOfyear.DAY
                                    });
                                } else if (idx === $scope.reportWorkPer.length - 1 && dateCheck == '') {
                                    $scope.StatusNoApprove.push({
                                        'PERID': $scope.PrsonSelect,
                                        'NAME': name,
                                        'DATE': dayOfyear.DAY
                                    });
                                }
                            });
                        });
                    }
                });
        }

        $scope.GetTimeToDb = function (date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1;

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return date = yyyy + '-' + mm + '-' + dd;
        }
    });

    app.filter('NumDeci', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var num1 = value.split('.')[0];
                var num2 = value.split('.')[1];
                if (num2 == 0) {
                    DateTrans = num1;
                } else {
                    return value
                }
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('-')[2];
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    Year;
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('NameTh', function () {
        return function (value) {
            var DateTh = "";
            if (value != undefined) {
                switch (value) {
                    case "Sunday":
                        DateTh = "อาทิตย์"
                        break;
                    case "Monday":
                        DateTh = "จันทร์"
                        break;
                    case "Tuesday":
                        DateTh = "อังคาร"
                        break;
                    case "Wednesday":
                        DateTh = "พุธ"
                        break;
                    case "Thursday":
                        DateTh = "พฤหัสบดี"
                        break;
                    case "Friday":
                        DateTh = "ศุกร์"
                        break;
                    case "Saturday":
                        DateTh = "เสาร์"
                        break;
                    default:
                        DateTh = value
                    // code block
                }
            }
            return (
                DateTh
            );
        }
    });
</script>-->
</body>

</html>