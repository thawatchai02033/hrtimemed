<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HR - Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered {
            border: 1px solid #64FFDA;
            margin-top: 20px;
        }

        table.table-bordered > thead > tr > th {
            border: 1px solid #64FFDA;
        }

        table.table-bordered > tbody > tr > td {
            border: 1px solid #64FFDA;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .pic {
            margin: 50px;
            /* demo spacing */
            display: inline-block;
            vertical-align: middle;
            width: 0;
            height: 0;
        }

        .arrow-left {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-right: 30px solid #737373;
        }

        .arrow-right {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-left: 30px solid #737373;
        }

        .arrow-up {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-bottom: 30px solid #737373;
        }

        .arrow-down {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-top: 30px solid #737373;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        .myButton {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #44c767), color-stop(1, #5cbf2a));
            background: -moz-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -webkit-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -o-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -ms-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: linear-gradient(to bottom, #44c767 5%, #5cbf2a 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#44c767', endColorstr='#5cbf2a', GradientType=0);
            background-color: #44c767;
            -moz-border-radius: 28px;
            -webkit-border-radius: 28px;
            border-radius: 28px;
            border: 1px solid #18ab29;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: Arial;
            font-size: 17px;
            padding: 16px 31px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #2f6627;
        }

        .myButton:hover {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cbf2a), color-stop(1, #44c767));
            background: -moz-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -webkit-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -o-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -ms-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: linear-gradient(to bottom, #5cbf2a 5%, #44c767 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cbf2a', endColorstr='#44c767', GradientType=0);
            background-color: #5cbf2a;
        }

        .myButton:active {
            position: relative;
            top: 1px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div id="page-wrapper" class="content-wrapper" window-size my-directive ng-controller="myCtrl">
        <section class="content-header">
            <h1>
                จัดการสิทธิ์การเข้าใช้งาน
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-pie-chart"></i> จัดการสิทธิ์การเข้าใช้งาน</a></li>
                <li class="active">จัดการสิทธิ์การเข้าใช้งาน</li>
            </ol>
        </section>
        <section class="content" ng-if="CheckPermiss">
            <div class="box-body">
                <div class="box box-primary">
                    <ul class="nav nav-tabs">
                        <li class="active" ng-if="<?php echo $_SESSION['PERID'];?> == '46792'"><a href="#Admin" data-toggle="tab" aria-expanded="true"
                                              style="font-weight: bold">ผู้ดูแลระบบ</a></li>
                        <li class=""><a href="#User" data-toggle="tab" aria-expanded="false"
                                        style="font-weight: bold">ผู้ใช้งาน</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Admin"  ng-if="<?php echo $_SESSION['PERID'];?> == '46792'">
                            <div class="" style="margin-top: 20px" align="center">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#Permiss_Admin" data-toggle="tab" aria-expanded="true"
                                                          style="font-weight: bold">เพิ่มสิทธิ์การใช้งาน</a></li>
                                    <li class=""><a href="#Change_Position" data-toggle="tab" aria-expanded="false"
                                                    style="font-weight: bold">โยกย้ายหน่วยงาน/ภาควิชา</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="Permiss_Admin">
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <select ui-select2 class="form-control select2"
                                                        style="width: 100%;" id="PersonChoose">
                                                    <option ng-repeat="person in PersonMed"
                                                            ng-model="PersonChoose"
                                                            ng-selected="PersonChoose == person.PERID"
                                                            value="{{person.PERID}}">{{person.PERID}} :
                                                        {{person.NAME}}
                                                        {{person.SURNAME}} : {{person.Dep_name}}
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4" align="center">
                                                <button class="btn btn-info" ng-click="AddPerson()">เพิ่มข้อมูล</button>
                                            </div>
                                        </div>
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-4">
                                                <div class="table-responsive">
                                                    <!-- <table datatable="ng" dt-options="vm.dtOptions" dt-instance="dtInstance" class="table table-bordered" width="100%" cellspacing="0"> -->
                                                    <table datatable="ng" dt-options="dtOptions"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>รายชื่อสิทธิ์</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" ng-repeat="item in Person_Permiss"
                                                            style="cursor: pointer;" ng-click="ChoosePerson(item)">
                                                            <td ng-style="item.CssStyles">
                                                                <div>รหัสบุคลากร <span
                                                                            style="color: #2C009F;font-weight: bold">{{item.PERID}}</span>
                                                                    : {{item.NAME}} {{item.SURNAME}}
                                                                    <i class="fa fa-window-close" aria-hidden="true" style="position: absolute;right: 20px;transform: scale(1.5);color: red;" ng-click="DeleteItem(item)"></i>
                                                                </div>
                                                                <p style="font-weight: bold;color: #ff934c">
                                                                    {{item.Dep_name}}</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div align="center">
                                                    <button class="btn btn-success"
                                                            style="font-weight: bold;padding: 15px;font-size: 16px"
                                                            ng-click="saveData()">บันทึก
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="table-responsive">
                                                    <!-- <table datatable="ng" dt-options="vm.dtOptions" dt-instance="dtInstance" class="table table-bordered" width="100%" cellspacing="0"> -->
                                                    <table datatable="ng" dt-options="dtOptionsDepart"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>เลือกหน่วยงาน/ภาควิชา ในการจัดการสิทธิ์</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" style="cursor: pointer;">
                                                            <td style="font-weight: bold;text-align: center">
                                                                <span style="color:blue;padding-right: 10px" ng-click="selectAdminAll()">( เลือกทั้งหมด )</span>
                                                                <span style="color:red;padding-left: 10px" ng-click="cancelAdminAll()">( ยกเลิกทั้งหมด )</span>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd gradeX" ng-repeat="dep in Depart2"
                                                            style="cursor: pointer;">
                                                            <td style="font-weight: bold;text-align: left">
                                                                <span style="color: #2C009F;font-weight: bold">{{dep.Dep_name}}</span>
                                                                <p style="font-weight: bold;color: #ff934c">
                                                                    {{dep.Dep_Group_name}}</p>
                                                                <div class="checkbox" align="right" style="height: 0px">
                                                                    <label ng-click="CheckedDep(dep)">
                                                                        <input type="checkbox"
                                                                               style="transform: scale(2);position: absolute;margin-top: -31px"
                                                                               ng-checked="dep.IsChecked">
                                                                    </label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Change_Position">
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-4" align="center">
                                                <span>ชื่อ - สกุล หน่วยงาน</span>
                                            </div>
                                            <div class="col-md-4">
                                                <select ui-select2 class="form-control select2"
                                                        style="width: 100%;" id="Person_ChangeDep">
                                                    <option ng-repeat="person in PersonMed"
                                                            ng-model="PersonChange"
                                                            ng-selected="PersonChange == person.PERID"
                                                            value="{{person.PERID}}">{{person.PERID}} :
                                                        {{person.NAME}}
                                                        {{person.SURNAME}} : {{person.Dep_name}}
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4" align="center">
                                            </div>
                                        </div>
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-4" align="center">
                                                <span>หน่วยงาน</span>
                                            </div>
                                            <div class="col-md-4">
                                                <select ui-select2 class="form-control select2"
                                                        style="width: 100%;" id="DepartChoose">
                                                    <option ng-repeat="depart in Depart"
                                                            ng-model="DepartChoose"
                                                            ng-selected="DepartChoose == depart.Dep_Code"
                                                            value="{{depart.Dep_Code}}">{{depart.Dep_name}} :
                                                        {{depart.Dep_Group_name}}
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4" align="center">
                                            </div>
                                        </div>
                                        <div align="center" style="margin-top: 15px">
                                            <button class="btn btn-success"
                                                    style="font-weight: bold;padding: 15px;font-size: 16px"
                                                    ng-click="AddPersonChangeDep()">บันทึก
                                            </button>
                                        </div>
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <!-- <table datatable="ng" dt-options="vm.dtOptions" dt-instance="dtInstance" class="table table-bordered" width="100%" cellspacing="0"> -->
                                                    <table datatable="ng" dt-options="dtOptions"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>รายชื่อผู้ย้ายสังกัด</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" ng-repeat="item in PersonChangeDepData"
                                                            style="cursor: pointer;" ng-click="ChoosePerson(item)">
                                                            <td style="font-weight: bold;text-align: left">
                                                                <div>รหัสบุคลากร <span
                                                                            style="color: #2C009F;font-weight: bold">{{item.PERID}}</span>
                                                                    : {{item.NAME}} {{item.SURNAME}}
                                                                    <i class="fa fa-window-close" aria-hidden="true" style="position: absolute;right: 20px;transform: scale(1.5);color: red;" ng-click="DeleteItemChangePerson(item)"></i>
                                                                </div>
                                                                <p style="font-weight: bold;color: #ff934c">
                                                                    สังกัดเดิม : <span style="color: green">{{item.Dep_name_Old}}</span> >>>> ย้ายไปสังกัด : <span style="color: #0000ee">{{item.Dep_name}}</span></p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="User">
                            <div class="" style="margin-top: 20px" align="center">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#Permiss_User" data-toggle="tab" aria-expanded="true"
                                                          style="font-weight: bold">เพิ่มสิทธิ์การจัดการข้อมูล
                                            ภาควิชา/หน่วยงาน</a></li>
                                    <li class=""><a href="#Permiss_Option" data-toggle="tab" aria-expanded="false"
                                                    style="font-weight: bold">เพิ่มสิทธิ์ในการใช้งานเมนู</a></li>
                                    <li class=""><a href="#Change_Position_User" data-toggle="tab" aria-expanded="false"
                                                    style="font-weight: bold">โอนย้ายข้อมูลบุคลากร</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="Permiss_User">
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <select ui-select2 class="form-control select2"
                                                        style="width: 100%;" id="PersonChoose_Ref_Dep">
                                                    <option ng-repeat="personData in Personal_Ref_Dep"
                                                            ng-model="P_Ref_Choose"
                                                            ng-selected="P_Ref_Choose == personData.PERID"
                                                            value="{{personData.PERID}}">{{personData.PERID}} :
                                                        {{personData.NAME}}
                                                        {{personData.SURNAME}} : {{personData.Dep_name}}
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4" align="center">
                                                <button class="btn btn-info" ng-click="AddPersonVice()">เพิ่มข้อมูล</button>
                                            </div>
                                        </div>
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-4">
                                                <div class="table-responsive">
                                                    <!-- <table datatable="ng" dt-options="vm.dtOptions" dt-instance="dtInstance" class="table table-bordered" width="100%" cellspacing="0"> -->
                                                    <table datatable="ng" dt-options="dtOptions"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>รายชื่อสิทธิ์</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" ng-repeat="item in Person_User_Permiss track by $index"
                                                            style="cursor: pointer;" ng-click="ChooseVicePerson(item)">
                                                            <td ng-style="item.CssStyles" >
                                                                <div>รหัสบุคลากร <span
                                                                            style="color: #2C009F;font-weight: bold">{{item.PERID}}</span>
                                                                    : {{item.NAME}} {{item.SURNAME}}
                                                                    <i class="fa fa-window-close" aria-hidden="true" style="position: absolute;right: 20px;transform: scale(1.5);color: red;" ng-click="DeleteViceItem(item)"></i>
                                                                </div>
                                                                <p style="font-weight: bold;color: #ff934c">
                                                                    {{item.Dep_name}}</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div align="center">
                                                    <button class="btn btn-success"
                                                            style="font-weight: bold;padding: 15px;font-size: 16px"
                                                            ng-click="saveDataVicePermiss()">บันทึก
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="table-responsive">
                                                    <!-- <table datatable="ng" dt-options="vm.dtOptions" dt-instance="dtInstance" class="table table-bordered" width="100%" cellspacing="0"> -->
                                                    <table datatable="ng" dt-options="dtOptionsDepart"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>เลือกหน่วยงาน/ภาควิชา ในการจัดการสิทธิ์</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" style="cursor: pointer;">
                                                            <td style="font-weight: bold;text-align: center">
                                                                <span style="color:blue;padding-right: 10px" ng-click="selectViceAll()">( เลือกทั้งหมด )</span>
                                                                <span style="color:red;padding-left: 10px" ng-click="cancelViceAll()">( ยกเลิกทั้งหมด )</span>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd gradeX" ng-repeat="dep in Depart_Ref2"
                                                            style="cursor: pointer;">
                                                            <td style="font-weight: bold;text-align: left">
                                                                <span style="color: #2C009F;font-weight: bold">{{dep.Dep_name}}</span>
                                                                <p style="font-weight: bold;color: #ff934c">
                                                                    {{dep.Dep_Group_name}}</p>
                                                                <div class="checkbox" align="right" style="height: 0px">
                                                                    <label ng-click="CheckedDep(dep)">
                                                                        <input type="checkbox"
                                                                               style="transform: scale(2);position: absolute;margin-top: -31px"
                                                                               ng-checked="dep.IsChecked">
                                                                    </label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Permiss_Option">
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-4">
                                                <div class="table-responsive">
                                                    <!-- <table datatable="ng" dt-options="vm.dtOptions" dt-instance="dtInstance" class="table table-bordered" width="100%" cellspacing="0"> -->
                                                    <table datatable="ng" dt-options="dtOptions"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>รายชื่อสิทธิ์</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" ng-repeat="item in Person_User_Permiss track by $index"
                                                            style="cursor: pointer;" ng-click="ChooseVicePerson(item)">
                                                            <td ng-style="item.CssStyles" >
                                                                <div>รหัสบุคลากร <span
                                                                            style="color: #2C009F;font-weight: bold">{{item.PERID}}</span>
                                                                    : {{item.NAME}} {{item.SURNAME}}
                                                                    <i class="fa fa-window-close" aria-hidden="true" style="position: absolute;right: 20px;transform: scale(1.5);color: red;" ng-click="DeleteViceItem(item)"></i>
                                                                </div>
                                                                <p style="font-weight: bold;color: #ff934c">
                                                                    {{item.Dep_name}}</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div align="center">
                                                    <button class="btn btn-success"
                                                            style="font-weight: bold;padding: 15px;font-size: 16px"
                                                            ng-click="saveDataViceFunc()">บันทึก
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="table-responsive">
                                                    <table datatable="ng" dt-options="dtOptionsDepart"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>เลือกสิทธิ์ในการเข้าถึงเมนูการทำงาน</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" style="cursor: pointer;">
                                                            <td style="font-weight: bold;text-align: center">
                                                                <span style="color:blue;padding-right: 10px" ng-click="FuncSelectAll()">( เลือกทั้งหมด )</span>
                                                                <span style="color:red;padding-left: 10px" ng-click="FuncCancelAll()">( ยกเลิกทั้งหมด )</span>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd gradeX" ng-repeat="Menu_item in Menu_Func"
                                                            style="cursor: pointer;">
                                                            <td style="font-weight: bold;text-align: left" ng-click="Func(Menu_item)">
                                                                <span style="color: #2C009F;font-weight: bold">{{Menu_item.Permiss_Details}}</span>
                                                                <div class="checkbox" align="right" style="height: 0px">
                                                                    <label>
                                                                        <input type="checkbox"
                                                                               style="transform: scale(2);position: absolute;margin-top: -25px"
                                                                               ng-checked="Menu_item.IsChecked">
                                                                    </label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="table-responsive" style="margin-top: 15px">
                                                    <table datatable="ng" dt-options="dtOptionsDepart"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>เลือกสิทธิ์ในการเข้าถึงฟังก์ชันเมนูการทำงาน</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" ng-repeat="Option_item in Option_Func"
                                                            style="cursor: pointer;">
                                                            <td style="font-weight: bold;text-align: left" ng-show="Option_item.ShowItem" ng-click="Option_Check(Option_item)">
                                                                <span style="color: #2C009F;font-weight: bold">{{Option_item.Opt_C_Details}}</span>
                                                                <div class="checkbox" align="right" style="height: 0px">
                                                                    <label>
                                                                        <input type="checkbox"
                                                                               style="transform: scale(2);position: absolute;margin-top: -25px"
                                                                               ng-checked="Option_item.IsChecked">
                                                                    </label>
                                                                </div>
                                                                <p style="color: rgba(185,119,65,0.99)">ฟังก์ชัน : {{Option_item.Detail}}</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="Change_Position_User">
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-4" align="center">
                                                <span>ชื่อ - สกุล หน่วยงาน</span>
                                            </div>
                                            <div class="col-md-4">
                                                <select ui-select2 class="form-control select2"
                                                        style="width: 100%;" id="Person_ChangeDep2">
                                                    <option ng-repeat="person in PersonMed2"
                                                            ng-model="PersonChange2"
                                                            ng-selected="PersonChange2 == person.PERID"
                                                            value="{{person.PERID}}">{{person.PERID}} :
                                                        {{person.NAME}}
                                                        {{person.SURNAME}} : {{person.Dep_name}}
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4" align="center">
                                            </div>
                                        </div>
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-4" align="center">
                                                <span>หน่วยงาน</span>
                                            </div>
                                            <div class="col-md-4">
                                                <select ui-select2 class="form-control select2"
                                                        style="width: 100%;" id="DepartChoose2">
                                                    <option ng-repeat="depart in Depart"
                                                            ng-model="DepartChoose2"
                                                            ng-selected="DepartChoose2 == depart.Dep_Code"
                                                            value="{{depart.Dep_Code}}">{{depart.Dep_name}} :
                                                        {{depart.Dep_Group_name}}
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-md-4" align="center">
                                            </div>
                                        </div>
                                        <div align="center" style="margin-top: 15px">
                                            <button class="btn btn-success"
                                                    style="font-weight: bold;padding: 15px;font-size: 16px"
                                                    ng-click="AddPersonChangeDep2()">บันทึก
                                            </button>
                                        </div>
                                        <div class="row" align="center" style="margin-top: 25px">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <!-- <table datatable="ng" dt-options="vm.dtOptions" dt-instance="dtInstance" class="table table-bordered" width="100%" cellspacing="0"> -->
                                                    <table datatable="ng" dt-options="dtOptions"
                                                           dt-instance="dtInstanceApprove"
                                                           class="table table-bordered"
                                                           width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: center">
                                                                <h4>รายชื่อผู้ย้ายสังกัด</h4>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" ng-repeat="item in PersonChangeDepData2"
                                                            style="cursor: pointer;" ng-click="ChoosePerson(item)">
                                                            <td style="font-weight: bold;text-align: left">
                                                                <div>รหัสบุคลากร <span
                                                                        style="color: #2C009F;font-weight: bold">{{item.PERID}}</span>
                                                                    : {{item.NAME}} {{item.SURNAME}}
                                                                    <i class="fa fa-window-close" aria-hidden="true" style="position: absolute;right: 20px;transform: scale(1.5);color: red;" ng-click="DeleteItemChangePerson(item)"></i>
                                                                </div>
                                                                <p style="font-weight: bold;color: #ff934c">
                                                                    สังกัดเดิม : <span style="color: green">{{item.Dep_name_Old}}</span> >>>> ย้ายไปสังกัด : <span style="color: #0000ee">{{item.Dep_name}}</span></p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="content" ng-if="!CheckPermiss">
            <div style="position: fixed;top: 50%;left: 30%;" align="center">
                <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
            </div>
        </section>
    </div>
    </section>
    <div class="loader" align="center" ng-if="Loading">
        <img class="loaderImg" src="./Image/Preloader_2.gif"/>
    </div>
</div>
<!-- /.content-wrapper -->
<?php require './Views/Footer.php' ?>
<!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>
<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap datepicker thai-->
<script src="./tools/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.th.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>

<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/Js/angular-sanitize.min.js"></script>
<script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="./tools/notiTools/assets/js/core/popper.min.js"></script>
<script src="./tools/notiTools/assets/js/core/bootstrap-material-design.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script>
    $(document).ready(function () {
        $().ready(function () {

            $('#drop_Menu').click(function () {
                $('#drop_Menu').addClass("open1");
            });

            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });

        if ( <?php echo $this->model->CheckStatus?> ) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]);?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]);?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>

<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker
        $('#reservation2').daterangepicker({
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "ตกลง",
                "cancelLabel": "ยกเลิก",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    "อา",
                    "จ",
                    "อ",
                    "พ",
                    "พฤ",
                    "ศ",
                    "ส"
                ],
                "monthNames": [
                    'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ],
                "firstDay": 1
            }
        })

        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            // format: 'dd/mm/yyyy',
            language: 'th'
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false,
            showMeridian: false,
            minuteStep: 1
        })

    })
</script>
<script>
    var app = angular.module("myApp", ["datatables", "ui.bootstrap", 'ngAnimate']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, $sce, DTOptionsBuilder, $uibModal) {

        $scope.Depart = <?php echo $this->Department; ?>;
        $scope.PersonMed = <?php echo $this->PersonCommit; ?>;
        $scope.PersonMed2 = <?php echo $this->PersonCommit2; ?>;
        $scope.Personal_Ref = <?php echo $this->Personal_Ref; ?>;
        $scope.Depart_Ref = <?php echo $this->Depart_Ref; ?>;
        $scope.Menu_Func = <?php echo $this->Menu_Func; ?>;
        $scope.Option_Func = <?php echo $this->Option_Func; ?>;
        $scope.Person_Permiss = [];
        $scope.Person_ChangeDep = [];
        $scope.Personal_Ref_Dep = [];
        $scope.Depart2 = [];
        $scope.Depart_Ref2 = [];
        $scope.PersonChangeDepData = [];
        $scope.PersonChangeDepData2 = [];
        $scope.PersonChoose = '0';
        $scope.PersonChange = '0';
        $scope.PersonChange2 = '0';
        $scope.Checked_Dep = false;
        $scope.Perid_Choose = '';
        $scope.Perid_Vice_Choose = '';
        $scope.CheckPermiss = true;
        $scope.loading = true;


        $scope.GetCurrentTime = function () {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return today = yyyy + '-' + mm + '-' + dd;
        }

        $scope.init = function () {

            <?php
            if ($PersonalData["Status"] == 'Administrator'){
            ?>
            $scope.CheckPermiss = true;
            <?php
            } else {
            ?>
            $scope.CheckPermiss = false;
            <?php
            }
            ?>

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [5, 10])
            $scope.dtOptionsDepart = DTOptionsBuilder.newOptions().withOption('lengthMenu', [10, 20, 30, 50, 100, 500])
            // $scope.Loading = false;

            angular.forEach($scope.Depart, function (item, idx) {
                $scope.Depart2.push({
                    Dep_Code: item.Dep_Code,
                    Dep_name: item.Dep_name,
                    Dep_Group_name: item.Dep_Group_name,
                    IsChecked: false
                });
            })

            angular.forEach($scope.Depart_Ref, function (item, idx) {
                $scope.Depart_Ref2.push({
                    Dep_Code: item.Dep_Code,
                    Dep_name: item.Dep_name,
                    Dep_Group_name: item.Dep_Group_name,
                    IsChecked: false
                });
            })

            angular.forEach($scope.Menu_Func, function (item, idx) {
                item.IsChecked = false
                angular.forEach($scope.Option_Func, function (item_Opt, idx) {
                    if (item.Permiss_ID == item_Opt.Permiss_ID){
                        item_Opt.Detail = item.Permiss_Details;
                    }
                })
            })

            angular.forEach($scope.Option_Func, function (item, idx) {
                item.IsChecked = false
                item.ShowItem = false
            })

            angular.forEach($scope.Personal_Ref, function (item, idx) {
                angular.forEach(item.Data, function (item_Ref) {
                    $scope.Personal_Ref_Dep.push(item_Ref);
                })
            })

            $scope.getDataPersonChangeDep();
            $scope.getDataPersonChangeDep2();
            $scope.GetMasterData();
            $scope.GetViceData();
        }

        $timeout($scope.init)

        $scope.selectAdminAll = function(){
            angular.forEach($scope.Depart2, function (item, idx) {
                item.IsChecked = true;
            })
        }

        $scope.cancelAdminAll = function(){
            angular.forEach($scope.Depart2, function (item, idx) {
                item.IsChecked = false;
            })
        }

        $scope.selectViceAll = function(){
            angular.forEach($scope.Depart_Ref2, function (item, idx) {
                item.IsChecked = true;
            })
        }

        $scope.cancelViceAll = function(){
            angular.forEach($scope.Depart_Ref2, function (item, idx) {
                item.IsChecked = false;
            })
        }

        $scope.GetMasterData = function(){
            $http.post('./ApiService/GETMasterData', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if(response.data.length > 0){
                        $scope.Person_Permiss = response.data
                        angular.forEach($scope.Person_Permiss, function (item) {
                            item.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "white"};
                            item.Select = false;
                        })
                    }
                });
        }

        $scope.GetViceData = function(){
            $http.post('./ApiService/GETViceData', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if(response.data.length > 0){
                        $scope.Person_User_Permiss = [];
                        angular.forEach(response.data, function (item) {
                            angular.forEach($scope.Personal_Ref_Dep, function (item2) {
                                if (item.PERID == item2.PERID){
                                    $scope.Person_User_Permiss.push(item)
                                }
                            })
                        })
                        angular.forEach($scope.Person_User_Permiss, function (item) {
                            item.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "white"};
                            item.Select = false;
                        })
                    } else {
                        $scope.Person_User_Permiss = [];
                    }
                });
        }

        $scope.AddPerson = function () {
            var Person_Choose = angular.element(document.querySelector('#PersonChoose'));
            var perid_Med = Person_Choose[0].options[Person_Choose[0].selectedIndex].value;
            // angular.forEach($scope.PersonMed, function (item) {
            //     if (item.PERID == perid_Med) {
            //         $scope.Person_Permiss.push(item)
            //     }
            // })

            data = {
                'PERID': perid_Med
            };
            $http.post('./ApiService/SaveMasterData', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if(response.data.Status){
                        $scope.Person_Permiss = [];
                        $scope.Person_Permiss = response.data.Master_Data
                        angular.forEach($scope.Person_Permiss, function (item) {
                            item.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "white"};
                            item.Select = false;
                        })
                    } else {
                        Swal({
                            type: 'error',
                            title: response.data.Message
                        });
                    }
                });
        }

        $scope.AddPersonVice = function(){
            var Person_Choose = angular.element(document.querySelector('#PersonChoose_Ref_Dep'));
            var perid_Med = Person_Choose[0].options[Person_Choose[0].selectedIndex].value;

            data = {
                'PERID': perid_Med
            };
            $http.post('./ApiService/SaveViceData', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if(response.data.Status){
                        $scope.Person_User_Permiss = [];
                        // $scope.Person_User_Permiss = response.data.Master_Data
                        // angular.forEach($scope.Person_User_Permiss, function (item) {
                        //     item.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "white"};
                        //     item.Select = false;
                        // })
                        angular.forEach(response.data.Master_Data, function (item) {
                            angular.forEach($scope.Personal_Ref_Dep, function (item2) {
                                if (item.PERID == item2.PERID){
                                    $scope.Person_User_Permiss.push(item)
                                }
                            })
                        })
                        angular.forEach($scope.Person_User_Permiss, function (item) {
                            item.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "white"};
                            item.Select = false;
                        })
                    } else {
                        Swal({
                            type: 'error',
                            title: response.data.Message
                        });
                    }
                });
        }

        $scope.AddPersonChangeDep = function () {
            var Person_Change = angular.element(document.querySelector('#Person_ChangeDep'));
            var perid_Med = Person_Change[0].options[Person_Change[0].selectedIndex].value;
            var Depart_Change = angular.element(document.querySelector('#DepartChoose'));
            var depart_Med = Depart_Change[0].options[Depart_Change[0].selectedIndex].value;
            angular.forEach($scope.PersonMed, function (item) {
                if (item.PERID == perid_Med) {
                    if(item.DEP_WORK == depart_Med){
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถย้ายหน่วยงานที่สังกัดอยู่เดิมได้'
                        });
                    } else {
                        data = {
                            PERID: perid_Med,
                            Dep_Code: depart_Med,
                            Dep_Code_Old: item.DEP_WORK,
                            PERID_CREATE: <?php echo $_SESSION['PERID'];?>
                        }

                        $http.post('./ApiService/SaveDataPersonChangeDep', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {

                                if(response.data.Status){
                                    Swal({
                                        type: 'success',
                                        title: response.data.Message
                                    });
                                    $scope.getDataPersonChangeDep2();
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: response.data.Message
                                    });
                                }
                            });
                    }
                }
            })
        }


        $scope.AddPersonChangeDep2 = function () {
            var Person_Change = angular.element(document.querySelector('#Person_ChangeDep2'));
            var perid_Med = Person_Change[0].options[Person_Change[0].selectedIndex].value;
            var Depart_Change = angular.element(document.querySelector('#DepartChoose2'));
            var depart_Med = Depart_Change[0].options[Depart_Change[0].selectedIndex].value;
            angular.forEach($scope.PersonMed, function (item) {
                if (item.PERID == perid_Med) {
                    if(item.DEP_WORK == depart_Med){
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถย้ายหน่วยงานที่สังกัดอยู่เดิมได้'
                        });
                    } else {
                        data = {
                            PERID: perid_Med,
                            Dep_Code: depart_Med,
                            Dep_Code_Old: item.DEP_WORK,
                            PERID_CREATE: <?php echo $_SESSION['PERID'];?>
                        }

                        $http.post('./ApiService/SaveDataPersonChangeDep', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {

                                if(response.data.Status){
                                    Swal({
                                        type: 'success',
                                        title: response.data.Message
                                    });
                                    $scope.getDataPersonChangeDep2();
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: response.data.Message
                                    });
                                }
                            });
                    }
                }
            })
        }

        $scope.getDataPersonChangeDep = function(){
            $http.post('./ApiService/getDataPersonChangeDep', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.PersonChangeDepData = [];
                    if (response.data.length > 0){

                        $scope.PersonChangeDepData = response.data;

                    } else {
                        $scope.PersonChangeDepData = [];
                    }
                });
        }

        $scope.getDataPersonChangeDep2 = function(){
            $http.post('./ApiService/getDataPersonChangeDep', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.PersonChangeDepData2 = [];
                    if (response.data.length > 0){

                        // $scope.PersonChangeDepData = response.data;
                        angular.forEach($scope.Depart, function (Depart_item) {
                            angular.forEach(response.data, function (Person_Item) {
                                if (Depart_item.Dep_Code == Person_Item.Dep_Code) {
                                    $scope.PersonChangeDepData2.push(Person_Item);
                                }
                            })
                        })

                    } else {
                        $scope.PersonChangeDepData2 = [];
                    }
                });
        }

        $scope.saveData = function () {
            var CheckSelect = false;
            angular.forEach($scope.Person_Permiss, function (item, idx) {
                if(item.Select){
                    CheckSelect = true;
                    data = {
                        'PERID': <?php echo $_SESSION['PERID'];?>,
                        'PERID_PERSON': $scope.Perid_Choose,
                        'Permiss_Dep': $scope.Depart2
                    };
                    $http.post('./ApiService/SaveDataPermissAdmin', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            console.log(response.data)
                            if(response.data.Status){
                                Swal({
                                    type: 'success',
                                    title: response.data.Message
                                });
                                $scope.GetMasterData();
                            } else {
                                Swal({
                                    type: 'error',
                                    title: response.data.Message
                                });
                            }
                        });
                } else {
                    if(idx === $scope.Person_Permiss.length - 1 && !CheckSelect){
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถบันทึกข้อมูลได้สำเร็จ โปรดเลือกรายชื่อบุคลากรก่อนทำการยืนยันบันทึกสิทธิ'
                        });
                    }
                }
            })
        }

        $scope.saveDataVicePermiss = function () {
            var CheckSelect = false;
            angular.forEach($scope.Person_User_Permiss, function (item, idx) {
                if(item.Select){
                    CheckSelect = true;
                    data = {
                        'PERID': <?php echo $_SESSION['PERID'];?>,
                        'PERID_PERSON': $scope.Perid_Vice_Choose,
                        'Permiss_Dep': $scope.Depart_Ref2
                    };
                    $http.post('./ApiService/SaveDataPermissVice', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            if(response.data.Status){
                                Swal({
                                    type: 'success',
                                    title: response.data.Message
                                });
                                $scope.GetViceData();
                            } else {
                                Swal({
                                    type: 'error',
                                    title: response.data.Message
                                });
                            }
                        });
                } else {
                    if(idx === $scope.Person_User_Permiss.length - 1 && !CheckSelect){
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถบันทึกข้อมูลได้สำเร็จ โปรดเลือกรายชื่อบุคลากรก่อนทำการยืนยันบันทึกสิทธิ'
                        });
                    }
                }
            })
        }

        $scope.saveDataVice = function () {
            var CheckSelect = false;
            angular.forEach($scope.Person_User_Permiss, function (item, idx) {
                if(item.Select){
                    CheckSelect = true;
                    data = {
                        'PERID': <?php echo $_SESSION['PERID'];?>,
                        'PERID_PERSON': $scope.Perid_Vice_Choose,
                        'Permiss_Dep': $scope.Depart_Ref2
                    };
                    $http.post('./ApiService/SaveDataPermissVice', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            if(response.data.Status){
                                Swal({
                                    type: 'success',
                                    title: response.data.Message
                                });
                                // $scope.GetViceData();
                            } else {
                                Swal({
                                    type: 'error',
                                    title: response.data.Message
                                });
                            }
                        });
                } else {
                    if(idx === $scope.Person_User_Permiss.length - 1 && !CheckSelect){
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถบันทึกข้อมูลได้สำเร็จ โปรดเลือกรายชื่อบุคลากรก่อนทำการยืนยันบันทึกสิทธิ'
                        });
                    }
                }
            })
        }

        $scope.saveDataViceFunc = function () {
            var CheckSelect = false;
            angular.forEach($scope.Person_User_Permiss, function (item, idx) {
                if(item.Select){
                    CheckSelect = true;
                    data = {
                        'PERID': <?php echo $_SESSION['PERID'];?>,
                        'PERID_PERSON': $scope.Perid_Vice_Choose,
                        'Func_Permiss': $scope.Menu_Func,
                        'Option_Permiss': $scope.Option_Func
                    };

                    $http.post('./ApiService/SaveDataFuncVice', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            if(response.data.Status){
                                Swal({
                                    type: 'success',
                                    title: response.data.Message
                                });
                                // $scope.GetViceData();
                            } else {
                                Swal({
                                    type: 'error',
                                    title: response.data.Message
                                });
                            }
                        });
                } else {
                    if(idx === $scope.Person_User_Permiss.length - 1 && !CheckSelect){
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถบันทึกข้อมูลได้สำเร็จ โปรดเลือกรายชื่อบุคลากรก่อนทำการยืนยันบันทึกสิทธิ'
                        });
                    }
                }
            })
        }

        $scope.DeleteItem = function (item) {
            data = {
                'PERID': item.PERID
            };
            $http.post('./ApiService/DeleteDataPermissAdmin', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if(response.data.Status){
                        Swal({
                            type: 'success',
                            title: response.data.Message
                        });
                        $scope.GetMasterData();
                    } else {
                        Swal({
                            type: 'error',
                            title: response.data.Message
                        });
                    }
                });
        }

        $scope.DeleteItemChangePerson = function (item) {

            data = {
                'PERID': item.PERID
            };
            $http.post('./ApiService/DeletePersonChangeDep', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if(response.data.Status){
                        Swal({
                            type: 'success',
                            title: response.data.Message
                        });
                        $scope.getDataPersonChangeDep();
                        $scope.getDataPersonChangeDep2();
                    } else {
                        Swal({
                            type: 'error',
                            title: response.data.Message
                        });
                    }
                });

        }

        $scope.DeleteViceItem = function (item) {
            data = {
                'PERID': item.PERID
            };
            $http.post('./ApiService/DeleteDataPermissVice', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    console.log(response.data)
                    if(response.data.Status){
                        Swal({
                            type: 'success',
                            title: response.data.Message
                        });
                        $scope.GetViceData();
                    } else {
                        Swal({
                            type: 'error',
                            title: response.data.Message
                        });
                    }
                });
        }

        $scope.saveDataPersonChange = function(){
            data = {
                'PERID': <?php echo $_SESSION['PERID'];?>,
                'PERID_PERSON': $scope.Perid_Choose,
                'Permiss_Dep': $scope.Depart2
            };
            $http.post('./ApiService/SaveDataPermissAdmin', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                });
        }

        $scope.CheckedDep = function (DepItem) {
            angular.forEach($scope.Depart2, function (item, idx) {
                if (DepItem.Dep_Code == item.Dep_Code) {
                    if (!DepItem.IsChecked) {
                        DepItem.IsChecked = true;
                    } else {
                        DepItem.IsChecked = false;
                    }
                }
            })
        }

        $scope.Func = function (FuncItem) {
            angular.forEach($scope.Option_Func, function (item_Opt, idx) {
                item_Opt.ShowItem = false;
            });
            angular.forEach($scope.Menu_Func, function (item, idx) {
                if (FuncItem.Permiss_ID == item.Permiss_ID) {
                    if (!FuncItem.IsChecked) {
                        FuncItem.IsChecked = true;
                        angular.forEach($scope.Option_Func, function (item_Opt, idx) {
                            if (FuncItem.Permiss_ID == item_Opt.Permiss_ID) {
                                if (!item_Opt.ShowItem) {
                                    item_Opt.ShowItem = true;
                                } else {
                                    item_Opt.ShowItem = false;
                                }
                            }
                        });
                    } else {
                        FuncItem.IsChecked = false;
                    }
                }
            })

            // angular.forEach($scope.Option_Func, function (item_Opt, idx) {
            //     if (FuncItem.Permiss_ID == item_Opt.Permiss_ID) {
            //         if (!item_Opt.ShowItem) {
            //             item_Opt.ShowItem = true;
            //         } else {
            //             item_Opt.ShowItem = false;
            //         }
            //     }
            // });
        }

        $scope.Option_Check = function (OptItem) {
            angular.forEach($scope.Option_Func, function (item, idx) {
                if (OptItem.Opt_C_Code == item.Opt_C_Code) {
                    if (!OptItem.IsChecked) {
                        OptItem.IsChecked = true;
                    } else {
                        OptItem.IsChecked = false;
                    }
                }
            })
        }

        $scope.FuncSelectAll = function (){
            angular.forEach($scope.Menu_Func, function (item, idx) {
                item.IsChecked = true;
            })
            angular.forEach($scope.Option_Func, function (item_Opt, idx) {
                item_Opt.IsChecked = true;
            });
        }

        $scope.FuncCancelAll = function (){
            angular.forEach($scope.Menu_Func, function (item, idx) {
                item.IsChecked = false;
            })
            angular.forEach($scope.Option_Func, function (item_Opt, idx) {
                item_Opt.IsChecked = false;
            });
        }

        $scope.ChoosePerson = function (item) {
            $scope.Perid_Choose = item.PERID;

            angular.forEach($scope.Person_Permiss, function (item_P) {
                if(item_P.PERID == item.PERID){
                    item_P.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "skyblue"};
                    item_P.Select = true;
                } else {
                    item_P.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "white"};
                    item_P.Select = false;
                }
            })

            data = {
                'PERID': $scope.Perid_Choose,
            };
            $http.post('./ApiService/GetPermissMaster', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    angular.forEach($scope.Depart2, function (item, idx) {
                        item.IsChecked = false;
                    })
                    angular.forEach($scope.Depart2, function (item, idx) {
                        angular.forEach(response.data, function (item_Permiss, idx) {
                            if (item_Permiss.Dep_Code == item.Dep_Code) {
                                item.IsChecked = true;
                            }
                        })
                    })
                });
        }

        $scope.ChooseVicePerson = function (item) {
            $scope.loading = true;
            $scope.Perid_Vice_Choose = item.PERID;

            angular.forEach($scope.Person_User_Permiss, function (item_P) {
                if(item_P.PERID == item.PERID){
                    item_P.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "skyblue"};
                    item_P.Select = true;
                } else {
                    item_P.CssStyles = {"font-weight": "bold","text-align": "left","background-color": "white"};
                    item_P.Select = false;
                }
            })

            data = {
                'PERID': $scope.Perid_Vice_Choose,
            };
            $http.post('./ApiService/GetPermissVice', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.loading = false;
                    if(response.data.length == 0){
                        angular.forEach($scope.Depart_Ref2, function (item, idx) {
                            item.IsChecked = false;
                        })
                    } else {
                        if (response.data[0].ViceData.length > 0) {
                            angular.forEach($scope.Depart_Ref2, function (item, idx) {
                                item.IsChecked = false;
                            })
                            angular.forEach($scope.Menu_Func, function (item, idx) {
                                item.IsChecked = false;
                            })
                            angular.forEach($scope.Option_Func, function (item, idx) {
                                item.IsChecked = false;
                            })
                            angular.forEach($scope.Depart_Ref2, function (item, idx) {
                                angular.forEach(response.data[0].ViceData, function (item_Permiss, idx) {
                                    if (item_Permiss.Dep_Code == item.Dep_Code) {
                                        item.IsChecked = true;
                                    }
                                })
                            })
                            angular.forEach($scope.Menu_Func, function (item, idx) {
                                angular.forEach(response.data[0].ViceOpt, function (item_Func, idx) {
                                    if (item_Func.Menu_Permiss == item.Permiss_ID) {
                                        item.IsChecked = true;
                                    }
                                })
                            })
                            angular.forEach($scope.Option_Func, function (item, idx) {
                                angular.forEach(response.data[0].ViceOpt, function (item_Opt, idx) {
                                    if (item_Opt.Option_Permiss == item.Opt_C_Code) {
                                        item.IsChecked = true;
                                    }
                                })
                            })


                        } else {

                        }
                    }
                });
        }

        $scope.htmlTrusted = function (html) {
            return $sce.trustAsHtml(html);
        }

        $scope.SetDateToDB = function (DateTpDB) {
            $scope.DateSelect = DateTpDB;
            $scope.Day = $scope.DateSelect.split('/')[1];
            $scope.Month = $scope.DateSelect.split('/')[0];
            $scope.Year = $scope.DateSelect.split('/')[2];
            return $scope.Year + "-" + $scope.Month + "-" +
                $scope.Day;
        }
    });

    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('/')[1];
                var Month = value.split('/')[0];
                var Year = value.split('/')[2];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    Year;
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateTimeLine', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('-')[2];
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateNameCut', function () {
        return function (value) {
            if (value != undefined) {
                if (value.length > 20) {
                    value = value.substring(0, 20);
                    value += "...";
                }
            }
            return (
                value
            );
        }
    })

    app.filter('NameTh', function () {
        return function (value) {
            var DateTh = "";
            if (value != undefined) {
                switch (value) {
                    case "Sunday":
                        DateTh = "อาทิตย์"
                        break;
                    case "Monday":
                        DateTh = "จันทร์"
                        break;
                    case "Tuesday":
                        DateTh = "อังคาร"
                        break;
                    case "Wednesday":
                        DateTh = "พุธ"
                        break;
                    case "Thursday":
                        DateTh = "พฤหัสบดี"
                        break;
                    case "Friday":
                        DateTh = "ศุกร์"
                        break;
                    case "Saturday":
                        DateTh = "เสาร์"
                        break;
                    default:
                        DateTh = value
                    // code block
                }
            }
            return (
                DateTh
            );
        }
    });

    app.directive('myDirective', ['$window', function ($window) {
        return function (scope, element) {
            var w = angular.element($window);
            scope.getWindowDimensions = function () {
                return {
                    'h': w.height(),
                    'w': w.width()
                };
            };
            scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
                scope.windowHeight = newValue.h;
                scope.windowWidth = newValue.w;
                scope.style = function () {
                    return {
                        'height': (newValue.h - 100) + 'px',
                        'width': (newValue.w - 100) + 'px'
                    };
                };
            }, true);

            w.bind('resize', function () {
                scope.$apply();
            });
        }
    }]);
</script>
</body>

</html>
