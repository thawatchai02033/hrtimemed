<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="./tools/ui-select/dist/select.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">

    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <div class="content-wrapper" ng-controller="myCtrl">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ตั้งค่าข้อมูลเวลา
                <small>ภายใต้หน่วยงานที่รับผิดชอบ</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> จัดการข้อมูลการเข้า-ออกงาน</a></li>
                <li class="active">ตั้งค่าข้อมูลเวลา</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" ng-if="CheckPermiss">
            <div class="row">
                <div class="col-md-3">

                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">หน่วยงาน</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <div style="padding: 20px">
                                <!-- <select class="form-control select2" style="width: 100%;" ng-model="DepartSelect" ng-change="update()" id="DepartCode" ng-disabled="<?php echo !$this->model->CheckUser ?>"> -->
                                <select class="form-control select2" style="width: 100%;" ng-model="DepartSelect"
                                        ng-change="update()" id="DepartCode">
                                    <option ng-repeat="depart in Depart" ng-selected="DepartSelect == depart.Dep_Code"
                                            value="{{depart.Dep_Code}}">{{depart.Dep_name}} :
                                        {{depart.Dep_Group_name}}
                                    </option>
                                </select>
                            </div>
                            <!-- /.Select group -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-body no-padding">
                            <div style="padding: 20px;" align="center">
                                <button style="width:40%;" class="btn btn-block btn-info" ng-click="update()">ค้นหา
                                </button>
                            </div>
                            <!-- /.Select group -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">เวลาการเข้าปฏิบัติงานของหน่วยงาน :</h3>
                            <h3 class="box-title" style="color:red">{{DepartHeader}}</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-6" style="text-align: center">
                                        <label style="color: green;" ng-show="TimeSave">เวลาหน่วยงานถูกบันทึกล่าสุดเมื่อวันที่
                                            : {{Date | DateThai2}}</label>
                                        <label style="color: red;" ng-show="!TimeSave">ไม่มีข้อมูลการบันทึกเวลาของหน่วยงาน</label>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-3" style="text-align: center">
                                        <label>เวลาการปฏิบัติงานวันที่:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                   style="text-align: center" autocomplete="off">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                            <div class="box-body">
                                <!-- time Picker -->
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center">
                                            <label>เวลาการเข้าปฏิบัติงานเช้า:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker"
                                                       style="text-align: center" id="TimeN" value="8:30"
                                                       autocomplete="off">

                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                    <!-- /.form group -->
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-3" style="text-align: center">
                                        <label>เวลาออกจากการปฏิบัติงาน:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker"
                                                   style="text-align: center" id="TimeO" value="16:30">

                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-3" style="text-align: center">
                                        <label>เวลาออก (เที่ยง):</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker"
                                                   style="text-align: center" id="TimeHO" value="12:30">

                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="col-md-3">
                                        <label style="color:blue">ค่าปกติ 12:30 เท่ากับ 12.00 (เวลาจะถูกอ่านค่าลดลง 30
                                            นาที)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-3" style="text-align: center">
                                        <label>เวลาเข้า (เที่ยง):</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker"
                                                   style="text-align: center" id="TimeHN" value="13:00"
                                                   ng-disabled="true">

                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                            <div class="box-body" ng-repeat="Door_Item in DoorData">
                                <div class="form-group">
                                    <div class="col-md-3" style="text-align: center">
                                        <label>นับเวลาโดยอ้างอิงจากการสแกนที่ประตู:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select ui-select2 class="form-control select2" style="width: 100%;"
                                                ng-model="DoorSelect[Door_Item.Count]"
                                                ng-init="DoorSelect[Door_Item.Count] = ''+Door_Item.door_id"
                                                id="doorDepart{{Door_Item.Count}}">
                                            <option ng-repeat="door in Door"
                                                    ng-selected="DoorSelect[Door_Item.Count] == door.door_id"
                                                    value="{{door.door_id}}">{{door.door_name}}
                                            </option>
                                        </select>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="col-md-3" align="center">
                                        <button class="btn btn-success" style="font-weight: bold"
                                                ng-click="insertDoor()" ng-if="$index < 1">เพิ่ม
                                        </button>
                                        <button class="btn btn-danger" style="font-weight: bold"
                                                ng-click="DeleteDoor($index)" ng-if="$index >= 1">ลบ
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body no-padding">
                                <div style="padding: 20px;" align="center">
                                    <button style="width:40%;" class="btn btn-block btn-success" ng-click="SaveData()">
                                        บันทึกข้อมูล
                                    </button>
                                </div>
                                <!-- /.Select group -->
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row" ng-if="DepartPersonResult.length > 0">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="panel panel-default">
                            <div class="box-header">
                                <h3 class="box-title">ตารางรายชื่อข้อมูลการตั้งค่าเวลารายบุคคล</h3>
                            </div>

                            <div class="box-body">
                                <p class="table-responsive">
                                    <table datatable="ng" dt-options="dtOptions" class="table table-bordered"
                                           width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>รหัสบุคลากร</th>
                                            <th>หน่วยงาน</th>
                                            <th>ชื่อ - สกุล</th>
                                            <th>เวลาการเข้าปฏิบัติงานเช้า</th>
                                            <th>เวลาออก (เที่ยง)</th>
                                            <th>เวลาเข้า (เที่ยง)</th>
                                            <th>เวลาออกจากการปฏิบัติงาน</th>
                                            <th>ประตูหน่วยงาน</th>
                                            <th>ทำรายการ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="odd gradeX" ng-repeat="PersonDep in DepartPersonResult">
                                            <td style="text-align: center; vertical-align: middle;">
                                                {{PersonDep.PERID}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;">
                                                {{PersonDep.Dep_name}}
                                            </td>
                                            <td style="vertical-align: middle;font-weight: bold;">{{PersonDep.NAME}}
                                                {{PersonDep.SURNAME}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;"
                                                ng-if="PersonDep.P_Time_TimeN != null">{{PersonDep.P_Time_TimeN |
                                                timeFormat2Digit}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:red;font-weight: bold;"
                                                ng-if="PersonDep.P_Time_TimeN == null">-
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;"
                                                style="color:green" ng-if="PersonDep.P_Time_TimeHO != null">
                                                {{PersonDep.P_Time_TimeHO | timeFormat2Digit}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:red;font-weight: bold;"
                                                ng-if="PersonDep.P_Time_TimeHO == null">-
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;"
                                                ng-if="PersonDep.P_Time_TimeHN != null">{{PersonDep.P_Time_TimeHN |
                                                timeFormat2Digit}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:red;font-weight: bold;"
                                                ng-if="PersonDep.P_Time_TimeHN == null">-
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;"
                                                ng-if="PersonDep.P_Time_TimeO != null">{{PersonDep.P_Time_TimeO |
                                                timeFormat2Digit}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:red;font-weight: bold;"
                                                ng-if="PersonDep.P_Time_TimeO == null">-
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;"
                                                ng-if="PersonDep.Door.length > 0">
                                <p ng-repeat="DoorItem in PersonDep.Door">
                                    {{DoorItem.door_name}}
                                </p>
                                <td style="text-align: center; vertical-align: middle;color:red;font-weight: bold;"
                                    ng-if="PersonDep.Door.length <= 0">-
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <a class="btn btn-app" data-toggle="modal" data-target="#modal-default"
                                       ng-click="ModalShow(PersonDep)">
                                        <i class="fa fa-edit" style="color: #ff840c;"></i> <span
                                                style="font-weight: bold">แก้ไข</span>
                                    </a>
                                </td>
                                </tr>
                                </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
            <div class="modal fade" id="modal-default">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">แก้ไขข้อมูล คุณ : {{Modalname}}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body no-padding">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center">
                                            <label>วันที่บันทึกข้อมูลล่าสุด:</label>
                                        </div>
                                        <div class="col-md-9" align="center">
                                            <label style="font-size: 20px;color: green;"
                                                   ng-if="DataPerson.P_Time_DateTime != ''">{{DataPerson.P_Time_DateTime
                                                | DateThai}}</label>
                                            <label style="font-size: 20px;color: red;"
                                                   ng-if="DataPerson.P_Time_DateTime == '' || DataPerson.P_Time_DateTime == null">ไม่มีข้อมูลการบันทึกเวลาปฏิบัติงาน</label>
                                            <!-- <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker"
                                                    style="text-align: center">
                                            </div> -->
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="col-md-3" style="text-align: center">
                                                <label>เวลาการเข้าปฏิบัติงานเช้า:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control timepicker"
                                                           style="text-align: center" id="TimeN2">

                                                    <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center">
                                            <label>เวลาออกจากการปฏิบัติงาน:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker"
                                                       style="text-align: center" id="TimeO2">

                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="col-md-3" style="text-align: center">
                                                <label>เวลาออก (เที่ยง):</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control timepicker"
                                                           style="text-align: center" id="TimeHO2">

                                                    <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                </div>
                                <div class="box-body">
                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="col-md-3" style="text-align: center">
                                                <label>เวลาเข้า (เที่ยง):</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <input type="text" class="form-control timepicker"
                                                           style="text-align: center" id="TimeHN2" ng-disabled="true">

                                                    <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                </div>
                                <div class="box-body" ng-repeat="Door_Item in DoorDataPerson">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center">
                                            <label>นับเวลาโดยอ้างอิงจากการสแกนที่ประตู:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select ui-select2 class="form-control select2" style="width: 100%;"
                                                    ng-model="DoorSelectPerson[Door_Item.Count]"
                                                    ng-init="DoorSelectPerson[Door_Item.Count] = ''+Door_Item.door_id"
                                                    id="doorDepartPerson{{Door_Item.Count}}">
                                                <option ng-repeat="door in Door"
                                                        ng-selected="DoorSelectPerson[Door_Item.Count] == door.door_id"
                                                        value="{{door.door_id}}">{{door.door_name}}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-3" align="center">
                                            <button class="btn btn-success" style="font-weight: bold"
                                                    ng-click="insertDoorPerson()" ng-if="$index < 1">เพิ่ม
                                            </button>
                                            <button class="btn btn-danger" style="font-weight: bold"
                                                    ng-click="DeleteDoorPerson($index)" ng-if="$index >= 1">ลบ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center">
                                            <label>นับเวลาโดยอ้างอิงจากการสแกนที่ตำแหน่ง:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <select class="form-control" style="width: 100%;text-align-last:center;">
                                                <option selected="selected">ภายใต้อาณาเขตของโรงพยาบาล</option>
                                                <option>หน้าหน่วยงาน</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">ยกเลิก</button>
                            <button type="button" class="btn btn-info" ng-click="SaveDataTimePerson()">บันทึกข้อมูล
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </section>
        <section class="content" ng-if="!CheckPermiss">
            <div style="position: fixed;top: 50%;left: 30%;" align="center">
                <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
            </div>
        </section>
        <!-- /.content -->
        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap datepicker thai-->
<script src="./tools/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.th.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/ui-select/dist/select2.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>
<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            language: 'th'
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: true,
            showMeridian: false,
            minuteStep: 1
        })
    })
</script>
<script>
    var app = angular.module("myApp", ["datatables", 'ui.select2']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, $interval, DTOptionsBuilder) {
        $scope.Depart = <?php echo $this->Department; ?>;
        $scope.Door = <?php echo $this->Door; ?>;
        $scope.DepartTimeResult = [];
        $scope.DepartPersonResult = [];
        $scope.TimeResult = [];
        $scope.DataPerson = [];
        $scope.DoorData = [];
        $scope.DoorDataPerson = [];
        $scope.DoorSubmit = [];
        $scope.Count_D = 0;
        $scope.Count_D_Person = 0;
        $scope.timein = '';
        $scope.timein2 = '';
        $scope.timehalfin = '';
        $scope.timehalfout = '';
        $scope.timeout = '';
        $scope.timeout2 = '';
        $scope.Date = '';
        $scope.DateTime = '';
        $scope.TimeSave = false;
        $scope.DepartShowTable = '';
        $scope.Loading = true;
        $scope.CheckPermiss = true;

        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250]);

        $scope.init = function () {

            data = {
                'MENU_CODE': '004',
            };

            $http.post('./ApiService/CheckPermissMenu', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    if (response.data.Status) {
                        $scope.CheckPermiss = true;
                        var check_Map_Dep = false;

                        $scope.Timestart_N = document.getElementById("TimeN").value.split(':')[0];
                        $scope.TimeEnd_N = document.getElementById("TimeN").value.split(':')[1];

                        angular.forEach($scope.Depart, function (item, idx) {
                            if (item.Dep_Code == <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>) {
                                $scope.DepartSelect = "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>;
                                check_Map_Dep = true;
                                $scope.update();
                            } else {
                                if(idx == $scope.Depart.length - 1 && !check_Map_Dep){
                                    $scope.DepartSelect = '';
                                    $scope.Loading = false;
                                }
                            }
                        })

                        $scope.Loading = false;
                        $interval(callAtInterval, 50);
                    } else {
                        $scope.CheckPermiss = false;
                        $scope.Loading = false;
                    }
                });
        }

        $scope.insertDoor = function () {
            $scope.DoorData.push({
                Count: ++$scope.Count_D,
            });
        }

        $scope.DeleteDoor = function (idx) {
            $scope.DoorData.splice(idx, 1);
            $scope.Count_D--;
        }

        $scope.insertDoorPerson = function () {
            $scope.DoorDataPerson.push({
                Count: ++$scope.Count_D_Person,
            });
        }

        $scope.DeleteDoorPerson = function (idx) {
            $scope.DoorDataPerson.splice(idx, 1);
            $scope.Count_D_Person--;
        }

        $timeout($scope.init)

        var ChangeTime = function (Hour, Minute) {
            var ampm = Hour >= 12 ? 'PM' : 'AM';
            Hour = Hour % 12;
            Hour = Hour ? Hour : 12; // the hour '0' should be '12'
            // minutes = minutes < 10 ? '0' + minutes :
            //     minutes;
            var strTime = Hour + ':' + Minute + ' ' +
                ampm;
            $scope.TimeResult.push(strTime);
            // return strTime;
        }

        var ChangeTime24H = function (date) {

            var Date24 = date.split(' ');
            var Hour = Date24[0].split(':')[0];
            var Minute = Date24[0].split(':')[1];
            var Status = Date24[1];
            if (Status == 'PM') {
                if (Hour != 12) {
                    Hour = Hour * 1 + 12;
                }

            } else if (Status == 'AM' && Hour == '12') {
                Hour = Hour - 12;
            } else {
                Hour = Hour;
            }

            return Hour + ':' + Minute + ':00';
        }

        var ChangeTime12H = function (date) {

            var Date24 = date.split(' ');
            var Hour = date.split(':')[0];
            var Minute = date.split(':')[1];
            var ampm = Hour >= 12 ? 'PM' : 'AM';
            Hour = Hour % 12;
            Hour = Hour ? Hour : 12;

            return Hour + ':' + Minute + ' ' +
                ampm;
        }

        function callAtInterval() {


            if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 4) {
                // document.getElementById("TimeHO").value = '4' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '4' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '8' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 3) {
                // document.getElementById("TimeHO").value = '3' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '3' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '7' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 2) {
                // document.getElementById("TimeHO").value = '2' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '2' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '6' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 1) {
                // document.getElementById("TimeHO").value = '1' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '1' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '5' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 0) {
                // document.getElementById("TimeHO").value = '0' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '0' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '4' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 23) {
                // document.getElementById("TimeHO").value = '23' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '23' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '3' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 22) {
                // document.getElementById("TimeHO").value = '22' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '22' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '2' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 21) {
                // document.getElementById("TimeHO").value = '21' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '21' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '1' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 20) {
                // document.getElementById("TimeHO").value = '20' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '20' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '0' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 19) {
                // document.getElementById("TimeHO").value = '19' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '19' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '23' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 18) {
                // document.getElementById("TimeHO").value = '18' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '18' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '22' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 17) {
                // document.getElementById("TimeHO").value = '17' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '17' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '21' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 16) {
                // document.getElementById("TimeHO").value = '16' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '16' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '20' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 15) {
                // document.getElementById("TimeHO").value = '15' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '15' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '19' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 14) {
                // document.getElementById("TimeHO").value = '14' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '14' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '18' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 13) {
                // document.getElementById("TimeHO").value = '13' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '13' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '17' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 12) {
                // document.getElementById("TimeHO").value = '12' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '12' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '16' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 11) {
                // document.getElementById("TimeHO").value = '11' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '11' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '15' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 10) {
                // document.getElementById("TimeHO").value = '10' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '10' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '14' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 9) {
                // document.getElementById("TimeHO").value = '9' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '9' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '13' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 8) {
                // document.getElementById("TimeHO").value = '8' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '8' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '12' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 7) {
                // document.getElementById("TimeHO").value = '7' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '7' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '11' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 6) {
                // document.getElementById("TimeHO").value = '6' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '6' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '10' + ':' + document.getElementById("TimeO").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[0]) == 5) {
                // document.getElementById("TimeHO").value = '5' + ':' + document.getElementById("TimeHO").value.split(':')[1];
                document.getElementById("TimeHN").value = '5' + ':' + document.getElementById("TimeHN").value.split(':')[1];
                // document.getElementById("TimeO").value = '9' + ':' + document.getElementById("TimeO").value.split(':')[1];
            }

            if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 30) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '30';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '30';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '30';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 31) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '31';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '31';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '31';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 32) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '32';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '32';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '32';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 33) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '33';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '33';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '33';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 34) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '34';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '34';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '34';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 35) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '35';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '35';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '35';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 36) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '36';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '36';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '36';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 37) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '37';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '37';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '37';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 38) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '38';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '38';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '38';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 39) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '39';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '39';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '39';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 40) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '40';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '40';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '40';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 41) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '41';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '41';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '41';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 42) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '42';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '42';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '42';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 43) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '43';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '43';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '43';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 44) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '44';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '44';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '44';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 45) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '45';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '45';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '45';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 46) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '46';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '46';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '46';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 47) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '47';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '47';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '47';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 48) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '48';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '48';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '48';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 49) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '49';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '49';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '49';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 50) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '50';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '50';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '50';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 51) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '51';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '51';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '51';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 52) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '52';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '52';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '52';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 53) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '53';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '53';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '53';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 54) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '54';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '54';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '54';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 55) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '55';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '55';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '55';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 56) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '56';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '56';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '56';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 57) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '57';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '57';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '57';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 58) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '58';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '58';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '58';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 59) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '59';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '59';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '59';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 0) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '00';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '00';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '00';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 1) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '01';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '01';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '01';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 2) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '02';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '02';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '02';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 3) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '03';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '03';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '03';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 4) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '04';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '04';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '04';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 5) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '05';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '05';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '05';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 6) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '06';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '06';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '06';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 7) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '07';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '07';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '07';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 8) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '08';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '08';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '08';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 9) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '09';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '09';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '09';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 10) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '10';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '10';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '10';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 11) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '11';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '11';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '11';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 12) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '12';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '12';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '12';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 13) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '13';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '13';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '13';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 14) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '14';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '14';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '14';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 15) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '15';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '15';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '15';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 16) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '16';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '16';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '16';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 17) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '17';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '17';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '17';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 18) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '18';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '18';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '18';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 19) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '19';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '19';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '19';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 20) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '20';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '20';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '20';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 21) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '21';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '21';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '21';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 22) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '22';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '22';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '22';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 23) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '23';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '23';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '23';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 24) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '24';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '24';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '24';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 25) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '25';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '25';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '25';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 26) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '26';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '26';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '26';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 27) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '27';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '27';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '27';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 28) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '28';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '28';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '28';
            } else if (parseInt(document.getElementById("TimeHO").value.split(':')[1]) == 29) {
                // document.getElementById("TimeHO").value = document.getElementById("TimeHO").value.split(':')[0] + ':' + '29';
                document.getElementById("TimeHN").value = document.getElementById("TimeHN").value.split(':')[0] + ':' + '29';
                // document.getElementById("TimeO").value = document.getElementById("TimeO").value.split(':')[0] + ':' + '29';
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////  Modal ////////////////////////////////////////////////////////////////////////////////////

            if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 4) {
                // document.getElementById("TimeHO2").value = '4' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '4' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '8' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 3) {
                // document.getElementById("TimeHO2").value = '3' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '3' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '7' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 2) {
                // document.getElementById("TimeHO2").value = '2' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '2' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '6' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 1) {
                // document.getElementById("TimeHO2").value = '1' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '1' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '5' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 0) {
                // document.getElementById("TimeHO2").value = '0' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '0' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '4' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 23) {
                // document.getElementById("TimeHO2").value = '23' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '23' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '3' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 22) {
                // document.getElementById("TimeHO2").value = '22' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '22' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '2' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 21) {
                // document.getElementById("TimeHO2").value = '21' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '21' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '1' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 20) {
                // document.getElementById("TimeHO2").value = '20' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '20' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '0' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 19) {
                // document.getElementById("TimeHO2").value = '19' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '19' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '23' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 18) {
                // document.getElementById("TimeHO2").value = '18' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '18' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '22' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 17) {
                // document.getElementById("TimeHO2").value = '17' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '17' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '21' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 16) {
                // document.getElementById("TimeHO2").value = '16' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '16' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '20' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 15) {
                // document.getElementById("TimeHO2").value = '15' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '15' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '19' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 14) {
                // document.getElementById("TimeHO2").value = '14' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '14' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '18' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 13) {
                // document.getElementById("TimeHO2").value = '13' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '13' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '17' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 12) {
                // document.getElementById("TimeHO2").value = '12' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '12' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '16' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 11) {
                // document.getElementById("TimeHO2").value = '11' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '11' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '15' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 10) {
                // document.getElementById("TimeHO2").value = '10' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '10' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '14' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 9) {
                // document.getElementById("TimeHO2").value = '9' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '9' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '13' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 8) {
                // document.getElementById("TimeHO2").value = '8' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '8' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '12' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 7) {
                // document.getElementById("TimeHO2").value = '7' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '7' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '11' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 6) {
                // document.getElementById("TimeHO2").value = '6' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '6' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '10' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[0]) == 5) {
                // document.getElementById("TimeHO2").value = '5' + ':' + document.getElementById("TimeHO2").value.split(':')[1];
                document.getElementById("TimeHN2").value = '5' + ':' + document.getElementById("TimeHN2").value.split(':')[1];
                // document.getElementById("TimeO2").value = '9' + ':' + document.getElementById("TimeO2").value.split(':')[1];
            }

            if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 30) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '30';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '30';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '30';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 31) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '31';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '31';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '31';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 32) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '32';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '32';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '32';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 33) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '33';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '33';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '33';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 34) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '34';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '34';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '34';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 35) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '35';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '35';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '35';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 36) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '36';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '36';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '36';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 37) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '37';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '37';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '37';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 38) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '38';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '38';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '38';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 39) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '39';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '39';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '39';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 40) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '40';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '40';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '40';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 41) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '41';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '41';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '41';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 42) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '42';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '42';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '42';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 43) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '43';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '43';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '43';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 44) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '44';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '44';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '44';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 45) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '45';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '45';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '45';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 46) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '46';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '46';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '46';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 47) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '47';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '47';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '47';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 48) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '48';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '48';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '48';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 49) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '49';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '49';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '49';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 50) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '50';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '50';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '50';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 51) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '51';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '51';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '51';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 52) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '52';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '52';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '52';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 53) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '53';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '53';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '53';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 54) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '54';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '54';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '54';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 55) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '55';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '55';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '55';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 56) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '56';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '56';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '56';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 57) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '57';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '57';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '57';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 58) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '58';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '58';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '58';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 59) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '59';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '59';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '59';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 0) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '00';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '00';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '00';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 1) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '01';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '01';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '01';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 2) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '02';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '02';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '02';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 3) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '03';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '03';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '03';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 4) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '04';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '04';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '04';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 5) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '05';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '05';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '05';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 6) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '06';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '06';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '06';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 7) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '07';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '07';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '07';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 8) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '08';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '08';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '08';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 9) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '09';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '09';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '09';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 10) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '10';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '10';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '10';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 11) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '11';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '11';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '11';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 12) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '12';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '12';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '12';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 13) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '13';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '13';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '13';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 14) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '14';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '14';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '14';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 15) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '15';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '15';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '15';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 16) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '16';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '16';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '16';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 17) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '17';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '17';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '17';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 18) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '18';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '18';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '18';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 19) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '19';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '19';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '19';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 20) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '20';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '20';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '20';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 21) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '21';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '21';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '21';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 22) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '22';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '22';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '22';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 23) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '23';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '23';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '23';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 24) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '24';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '24';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '24';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 25) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '25';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '25';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '25';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 26) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '26';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '26';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '26';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 27) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '27';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '27';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '27';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 28) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '28';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '28';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '28';
            } else if (parseInt(document.getElementById("TimeHO2").value.split(':')[1]) == 29) {
                // document.getElementById("TimeHO2").value = document.getElementById("TimeHO2").value.split(':')[0] + ':' + '29';
                document.getElementById("TimeHN2").value = document.getElementById("TimeHN2").value.split(':')[0] + ':' + '29';
                // document.getElementById("TimeO2").value = document.getElementById("TimeO2").value.split(':')[0] + ':' + '29';
            }
        }

        $scope.ModalShow = function (PersonDep) {

            $scope.DataPerson = PersonDep;
            $scope.Modalname = PersonDep.NAME + " " + PersonDep.SURNAME;
            $scope.DoorDataPerson = [];

            if (PersonDep.P_Time_TimeN != null) {
                document.getElementById("TimeN2").value = PersonDep.P_Time_TimeN.split(':')[0] + ":" + PersonDep.P_Time_TimeN.split(':')[1];
            } else {
                document.getElementById("TimeN2").value = '8:30';
            }
            if (PersonDep.P_Time_TimeHO != null) {
                document.getElementById("TimeHO2").value = PersonDep.P_Time_TimeHO.split(':')[0] + ":" + PersonDep.P_Time_TimeHO.split(':')[1];
            } else {
                document.getElementById("TimeHO2").value = '12:30';
            }
            if (PersonDep.P_Time_TimeHN != null) {
                document.getElementById("TimeHN2").value = PersonDep.P_Time_TimeHN.split(':')[0] + ":" + PersonDep.P_Time_TimeHN.split(':')[1];
            } else {
                document.getElementById("TimeHN2").value = '12:30';
            }
            if (PersonDep.P_Time_TimeO != null) {
                document.getElementById("TimeO2").value = PersonDep.P_Time_TimeO.split(':')[0] + ":" + PersonDep.P_Time_TimeO.split(':')[1];
            } else {
                document.getElementById("TimeO2").value = '16:30';
            }

            if (PersonDep.Door.length > 0) {
                angular.forEach(PersonDep.Door, function (item_Door) {
                    angular.forEach($scope.Door, function (DoorArr) {
                        if (item_Door.P_DoorID == DoorArr.door_id) {
                            $scope.DoorDataPerson.push({
                                Count: ++$scope.Count_D_Person,
                                door_id: '' + DoorArr.door_id
                            });
                        }
                    });
                })
            } else {
                $scope.DoorDataPerson = [];
                $scope.Count_D_Person = 0;
                $scope.insertDoorPerson();
            }
            $scope.DoorSelectPerson = PersonDep.P_DoorID;
        }


        $scope.update = function () {
            var DepartIsNull = angular.element(document.querySelector('#DepartCode'));
            if (DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != null && DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.DepartSelect = DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value
            }

            angular.forEach($scope.Depart, function (arr) {
                if (arr.Dep_Code == $scope.DepartSelect) {
                    $scope.DepartHeader = arr.Dep_name;
                    data = {
                        'DepCode': $scope.DepartSelect,
                    };
                    $http.post('./ApiService/GetDepartTimeSet', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            $scope.DepartTimeResult = response.data;
                            $scope.TimeResult = [];
                            $scope.DoorData = [];
                            if ($scope.DepartTimeResult.length > 0) {
                                angular.forEach($scope.DepartTimeResult, function (arr) {
                                    $scope.DateSelect = arr.D_Time_DateTime;
                                    $scope.Month = $scope.DateSelect.split('-')[1];
                                    $scope.Year = $scope.DateSelect.split('-')[0];
                                    $scope.Day = $scope.DateSelect.split('-')[2];
                                    $scope.Date = $scope.Month + "/" + $scope.Day +
                                        "/" + $scope.Year;
                                    document.getElementById("datepicker").value =
                                        $scope.Date;
                                    $scope.TimeSave = true;
                                    // if (arr.door_id == '0') {
                                    //     $scope.DoorSelect = '0';
                                    // } else {
                                    //     // angular.forEach($scope.Door, function (DoorArr) {
                                    //     //     if (arr.door_id == DoorArr.door_id) {
                                    //     //         $scope.DoorData.push({
                                    //     //             Count: ++$scope.Count_D,
                                    //     //             door_id: '' + DoorArr.door_id
                                    //     //         });
                                    //     //         // $scope.DoorSelect = '' + DoorArr.door_id;
                                    //     //     } else {
                                    //     //
                                    //     //     }
                                    //     // });
                                    // }

                                    document.getElementById("TimeN").value = arr.D_Time_TimeN.split(':')[0] + ":" + arr.D_Time_TimeN.split(':')[1];
                                    document.getElementById("TimeHO").value = arr.D_Time_TimeHO.split(':')[0] + ":" + arr.D_Time_TimeHO.split(':')[1];
                                    document.getElementById("TimeHN").value = arr.D_Time_TimeHN.split(':')[0] + ":" + arr.D_Time_TimeHN.split(':')[1];
                                    document.getElementById("TimeO").value = arr.D_Time_TimeO.split(':')[0] + ":" + arr.D_Time_TimeO.split(':')[1];

                                });

                                angular.forEach($scope.DepartTimeResult, function (arr) {
                                    angular.forEach($scope.Door, function (DoorArr) {
                                        if (arr.door_id == DoorArr.door_id && $scope.DateSelect == arr.D_Time_DateTime) {
                                            $scope.DoorData.push({
                                                Count: ++$scope.Count_D,
                                                door_id: '' + DoorArr.door_id
                                            });
                                        }
                                    });
                                });
                            } else {
                                $scope.DoorData = [];
                                $scope.Count_D = 0;
                                $scope.insertDoor();
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!

                                var yyyy = today.getFullYear();
                                if (dd < 10) {
                                    dd = '0' + dd;
                                }
                                if (mm < 10) {
                                    mm = '0' + mm;
                                }

                                var today = mm + '/' + dd + '/' + yyyy;

                                document.getElementById("datepicker").value = today;

                                $scope.TimeSave = false;
                                document.getElementById("TimeN").value = '8:30';
                                document.getElementById("TimeHO").value = '12:30';
                                document.getElementById("TimeHN").value = '13:00';
                                document.getElementById("TimeO").value = '16:30';
                                // document.getElementById("doorDepart").value = '';
                                // document.getElementById("datepicker").value = '';
                            }
                        });
                    data = {
                        'DepCode': $scope.DepartSelect,
                    };
                    $http.post('./ApiService/GetPersonDepart', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            if (response.data.length > 0) {
                                $scope.DepartPersonResult = [];
                                var DepartPersonResultCopy = []
                                angular.forEach(response.data, function (item) {
                                    var DoorPersonArr = [];
                                    angular.forEach(response.data, function (item_dupe) {
                                        if (item.PERID == item_dupe.PERID) {
                                            if (item_dupe.P_DoorID != null) {
                                                if(item_dupe.P_DoorID != 0){
                                                    DoorPersonArr.push({
                                                        P_DoorID: item_dupe.P_DoorID,
                                                        door_name: item_dupe.door_name
                                                    })
                                                } else {
                                                    DoorPersonArr.push({
                                                        P_DoorID: 0,
                                                        door_name: 'ประตูทั้งหมด'
                                                    })
                                                }
                                            }
                                        }
                                    })

                                    DepartPersonResultCopy.push({
                                        DEP_WORK: item.DEP_WORK,
                                        Dep_name: item.Dep_name,
                                        Edit_code: item.Edit_code,
                                        NAME: item.NAME,
                                        SURNAME: item.SURNAME,
                                        PERID: item.PERID,
                                        P_Time_DateTime: item.P_Time_DateTime,
                                        P_Time_TimeHN: item.P_Time_TimeHN,
                                        P_Time_TimeHO: item.P_Time_TimeHO,
                                        P_Time_TimeN: item.P_Time_TimeN,
                                        P_Time_TimeO: item.P_Time_TimeO,
                                        Door: DoorPersonArr
                                    })
                                })

                                DepartPersonResultCopy = UniqueArraybyId(DepartPersonResultCopy,
                                    "PERID");

                                function UniqueArraybyId(collection, keyname) {
                                    var output = [],
                                        keys = [];

                                    angular.forEach(collection, function (item) {
                                        var key = item[keyname];
                                        if (keys.indexOf(key) === -1) {
                                            keys.push(key);
                                            output.push(item);
                                        }
                                    });
                                    return output;
                                };

                                if ($scope.CheckAll) {
                                    data = {
                                        'DepCode': $scope.Depart
                                    };
                                } else {
                                    data = {
                                        'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                                    };
                                }
                                $http.post('./ApiService/GetRefPersonal', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                    .then(function successCallback(response) {
                                        if (response.data.length > 0) {
                                            var Copies = []
                                            angular.extend(Copies, DepartPersonResultCopy)
                                            angular.forEach(DepartPersonResultCopy, function (data_I, idx) {
                                                angular.forEach(response.data, function (Ref_I) {
                                                    if ($scope.CheckAll) {
                                                        angular.forEach($scope.Depart, function (depart_I, idx_Depart) {
                                                            if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == depart_I.Dep_Code) {
                                                            } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == depart_I.Dep_Code) {
                                                                var CheckDep = false
                                                                angular.forEach($scope.Depart, function (depart_I2, idx_Dep) {
                                                                    if (Ref_I.Dep_Code == depart_I2.Dep_Code) {
                                                                        CheckDep = true
                                                                    }
                                                                })
                                                                if (!CheckDep) {
                                                                    if(Copies.length > 0){
                                                                        angular.forEach(Copies, function (data_Copy, idx_c) {
                                                                            if(data_I.PERID == data_Copy.PERID){
                                                                                Copies.splice(idx_c, 1)
                                                                            }
                                                                        })
                                                                    }
                                                                } else {
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == $scope.DepartSelect) {
                                                        } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == $scope.DepartSelect) {
                                                            if(Copies.length > 0){
                                                                angular.forEach(Copies, function (data_Copy, idx_c) {
                                                                    if(data_I.PERID == data_Copy.PERID){
                                                                        Copies.splice(idx_c, 1)
                                                                    }
                                                                })
                                                            }
                                                        }
                                                    }
                                                })
                                            })
                                            $scope.DepartPersonResult = []
                                            $scope.DepartPersonResult = Copies
                                        } else {
                                            $scope.DepartPersonResult = []
                                            $scope.DepartPersonResult = DepartPersonResultCopy
                                        }
                                    });
                            }
                        });
                }
            });
        }

        $scope.SaveData = function () {
            angular.forEach()
            if (document.getElementById("datepicker").value != "" &&
                // document.getElementById("doorDepart").value != "? undefined:undefined ?" &&
                document.getElementById("DepartCode").value != "? undefined:undefined ?" &&
                document.getElementById("TimeN").value != "" &&
                document.getElementById("TimeHO").value != "" &&
                document.getElementById("TimeHN").value != "" &&
                document.getElementById("TimeO").value != "") {
                var MonthInsert = document.getElementById("datepicker").value.split('/')[0];
                var YearInsert = document.getElementById("datepicker").value.split('/')[2];
                var DayInsert = document.getElementById("datepicker").value.split('/')[1];
                $scope.Date2 = YearInsert + "-" + MonthInsert +
                    "-" + DayInsert;
                var TimeN = document.getElementById("TimeN").value;
                var TimeHO = document.getElementById("TimeHO").value;
                var TimeHN = document.getElementById("TimeHN").value;
                var TimeO = document.getElementById("TimeO").value;
                // if (TimeN > TimeHO) {
                //     Swal({
                //         type: 'error',
                //         title: "1"
                //     })
                // }
                // if (TimeHO < TimeN || TimeHO > TimeHN || TimeHO > TimeO) {
                //     Swal({
                //         type: 'error',
                //         title: "2"
                //     })
                // }
                // if (TimeHN < TimeN || TimeHN < TimeHN || TimeHN > TimeO) {
                //     Swal({
                //         type: 'error',
                //         title: "3"
                //     })
                // }
                // if (TimeO < TimeHO || TimeO < TimeHN || TimeO < TimeN) {
                //     Swal({
                //         type: 'error',
                //         title: "4"
                //     })
                // }


                // document.getElementById("TimeN").value = '';
                // document.getElementById("TimeHO").value = '';
                // document.getElementById("TimeHN").value = '';
                // document.getElementById("TimeO").value = '';
                // document.getElementById("doorDepart").value = '';
                // document.getElementById("datepicker").value = '';


                Swal({
                    title: 'ยืนยันการทำรายการ',
                    text: "คุณต้องการเพิ่มหรือแก้ไขข้อมูลใช่หรือไม่?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'ยกเลิก',
                    confirmButtonText: 'ตกลง'
                }).then((result) => {
                    if (result.value) {
                        $scope.DoorSubmit = [];
                        // angular.forEach($scope.DoorData, function (item) {
                        //     if (angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value != '? string: ?') {
                        //         $scope.DoorSubmit.push({
                        //             doorId: angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value
                        //         })
                        //     }
                        // });

                        var CheckAllDoor = false;
                        angular.forEach($scope.DoorData, function (item) {
                            if (angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value != '? string: ?' &&
                                angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value == '0') {
                                $scope.DoorSubmit.push({
                                    doorId: angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value
                                })
                                CheckAllDoor = true;
                            }
                        });
                        if (!CheckAllDoor) {
                            angular.forEach($scope.DoorData, function (item) {
                                if (angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value != '? string: ?') {
                                    var CheckDoor_Dupe = false;
                                    if ($scope.DoorSubmit.length > 0) {
                                        angular.forEach($scope.DoorSubmit, function (door_Item, idx) {
                                            if (door_Item.doorId == angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value) {
                                                CheckDoor_Dupe = true;
                                            } else {
                                                if ((idx == $scope.DoorSubmit.length - 1) && !CheckDoor_Dupe) {
                                                    $scope.DoorSubmit.push({
                                                        doorId: angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value
                                                    })
                                                }
                                            }
                                        })
                                    } else {
                                        $scope.DoorSubmit.push({
                                            doorId: angular.element(document.querySelector('#doorDepart' + item.Count))[0].options[angular.element(document.querySelector('#doorDepart' + item.Count))[0].selectedIndex].value
                                        })
                                    }
                                }
                            });
                        }

                        data = {
                            'Date': $scope.Date2,
                            'TimeN': TimeN,
                            'TimeO': TimeO,
                            'TimeHN': TimeHN,
                            'TimeHO': TimeHO,
                            'Perid': <?php echo $_SESSION['PERID'] ?>,
                            'DepCode': document.getElementById("DepartCode").value,
                            'door_Data': $scope.DoorSubmit
                        };

                        $http.post('./ApiService/InsertDapartTime', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                var result = response.data;
                                if (result.Status) {
                                    Swal({
                                        type: 'success',
                                        title: result.Message
                                    })
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: result.Message
                                    })
                                }
                            });
                    }
                });
            } else {
                Swal({
                    type: 'error',
                    title: 'ไม่สามารถบันทึกข้อมูลได้',
                    text: 'กรุณาระบุข้อมูลให้ครบถ้วน'
                })
            }
        }

        $scope.SaveDataTimePerson = function () {
            var TimeN2 = document.getElementById("TimeN2").value;
            var TimeHO2 = document.getElementById("TimeHO2").value;
            var TimeHN2 = document.getElementById("TimeHN2").value;
            var TimeO2 = document.getElementById("TimeO2").value;
            var DOORID = $scope.DoorSelectPerson;
            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "คุณต้องการเพิ่มหรือแก้ไขข้อมูลใช่หรือไม่?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'ยกเลิก',
                confirmButtonText: 'ตกลง'
            }).then((result) => {
                if (result.value) {

                    $scope.DoorPersonSubmit = [];
                    var CheckAllDoor = false;
                    angular.forEach($scope.DoorDataPerson, function (item) {
                        if (angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].options[angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].selectedIndex].value != '? string: ?' &&
                            angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].options[angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].selectedIndex].value == '0') {
                            $scope.DoorPersonSubmit.push({
                                doorId: angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].options[angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].selectedIndex].value
                            })
                            CheckAllDoor = true;
                        }
                    });
                    if (!CheckAllDoor) {
                        angular.forEach($scope.DoorDataPerson, function (item) {
                            if (angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].options[angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].selectedIndex].value != '? string: ?') {
                                var CheckDoor_Dupe = false;
                                if ($scope.DoorPersonSubmit.length > 0) {
                                    angular.forEach($scope.DoorPersonSubmit, function (door_Item, idx) {
                                        if (door_Item.doorId == angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].options[angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].selectedIndex].value) {
                                            CheckDoor_Dupe = true;
                                        } else {
                                            if ((idx == $scope.DoorPersonSubmit.length - 1) && !CheckDoor_Dupe) {
                                                $scope.DoorPersonSubmit.push({
                                                    doorId: angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].options[angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].selectedIndex].value
                                                })
                                            }
                                        }
                                    })
                                } else {
                                    $scope.DoorPersonSubmit.push({
                                        doorId: angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].options[angular.element(document.querySelector('#doorDepartPerson' + item.Count))[0].selectedIndex].value
                                    })
                                }
                            }
                        });
                    }


                    data = {
                        'TimeN': TimeN2,
                        'TimeO': TimeO2,
                        'TimeHN': TimeHN2,
                        'TimeHO': TimeHO2,
                        'DoorID': $scope.DoorPersonSubmit,
                        'Perid': $scope.DataPerson.PERID,
                        'Perid_Update': <?php echo $_SESSION['PERID'] ?>,
                        'DepCode': $scope.DataPerson.DEP_WORK,
                    };

                    $http.post('./ApiService/InsertPersonTime', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            var result = response.data;
                            if (result.Status) {
                                Swal({
                                    type: 'success',
                                    title: result.Message
                                });
                                data = {
                                    'DepCode': $scope.DepartSelect,
                                };
                                $http.post('./ApiService/GetPersonDepart',
                                    data, {
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        }
                                    })
                                    .then(function successCallback(response) {
                                        $scope.DepartPersonResult = [];
                                        if (response.data.length > 0) {
                                            angular.forEach(response.data, function (item) {
                                                var DoorPersonArr = [];
                                                angular.forEach(response.data, function (item_dupe) {
                                                    if (item.PERID == item_dupe.PERID) {
                                                        if (item_dupe.P_DoorID != null) {
                                                            if(item_dupe.P_DoorID != 0){
                                                                DoorPersonArr.push({
                                                                    P_DoorID: item_dupe.P_DoorID,
                                                                    door_name: item_dupe.door_name
                                                                })
                                                            } else {
                                                                DoorPersonArr.push({
                                                                    P_DoorID: 0,
                                                                    door_name: 'ประตูทั้งหมด'
                                                                })
                                                            }
                                                        }
                                                    }
                                                })

                                                $scope.DepartPersonResult.push({
                                                    DEP_WORK: item.DEP_WORK,
                                                    Dep_name: item.Dep_name,
                                                    Edit_code: item.Edit_code,
                                                    NAME: item.NAME,
                                                    SURNAME: item.SURNAME,
                                                    PERID: item.PERID,
                                                    P_Time_DateTime: item.P_Time_DateTime,
                                                    P_Time_TimeHN: item.P_Time_TimeHN,
                                                    P_Time_TimeHO: item.P_Time_TimeHO,
                                                    P_Time_TimeN: item.P_Time_TimeN,
                                                    P_Time_TimeO: item.P_Time_TimeO,
                                                    Door: DoorPersonArr
                                                })
                                            })

                                            $scope.DepartPersonResult = UniqueArraybyId($scope.DepartPersonResult,
                                                "PERID");

                                            function UniqueArraybyId(collection, keyname) {
                                                var output = [],
                                                    keys = [];

                                                angular.forEach(collection, function (item) {
                                                    var key = item[keyname];
                                                    if (keys.indexOf(key) === -1) {
                                                        keys.push(key);
                                                        output.push(item);
                                                    }
                                                });
                                                return output;
                                            };
                                        }
                                    });
                            } else {
                                Swal({
                                    type: 'error',
                                    title: result.Message
                                });
                            }
                        });
                }
            });

        }
    });

    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('-')[2];
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateThai2', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('/')[1];
                var Month = value.split('/')[0];
                var Year = value.split('/')[2];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('timeDate', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var ampm = Hour >= 12 ? 'PM' : 'AM';
            Hour = Hour % 12;
            Hour = Hour ? Hour : 12;
            var strTime = Hour + ':' + Minutes + ' ' +
                ampm;
            return (
                strTime
                // value.getHours() >= 13 ? (value.getHours() - 12) : (value.getHours())) + ":" + (
                // value.getMinutes() < 10 ? '0' : '') + value.getMinutes() + (value.getHours() > 11 ?
                // 'pm' : 'am'
            );
        }
    });

    app.filter('timeFormat2Digit', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var strTime = Hour + ':' + Minutes
            return (
                strTime
            );
        }
    });
</script>
</body>

</html>
