<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบ เข้า-ออก งาน</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="./tools/ui-select/dist/select.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table > caption + thead > tr:first-child > td,
        .table > caption + thead > tr:first-child > th,
        .table > colgroup + thead > tr:first-child > td,
        .table > colgroup + thead > tr:first-child > th,
        .table > thead:first-child > tr:first-child > td,
        .table > thead:first-child > tr:first-child > th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <div class="content-wrapper" ng-controller="myCtrl">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ตั้งค่าการลา
                <small>ข้อมูลสำหรับการออกใบลา</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> การจัดการ การลา</a></li>
                <li class="active">ตั้งค่าการลา</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" ng-if="CheckPermiss">
            <div class="row">
                <div class="col-md-3">

                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">หน่วยงาน</h3>
                        </div>
                        <div class="box-body no-padding">
                            <div style="padding: 20px">
                                <!-- <select class="form-control select2" style="width: 100%;" ng-model="DepartSelect" ng-change="update()" id="DepartCode" ng-disabled="<?php echo !$this->model->CheckUser ?>"> -->
                                <select class="form-control select2" style="width: 100%;" ng-model="DepartSelect"
                                        ng-change="getAllApproveLeave()" id="DepartCode">
                                    <option ng-repeat="depart in Depart" ng-selected="DepartSelect == depart.Dep_Code"
                                            value="{{depart.Dep_Code}}">{{depart.Dep_name}} :
                                        {{depart.Dep_Group_name}}
                                    </option>
                                </select>
                            </div>
                            <!-- /.Select group -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-body no-padding">
                            <div style="padding: 20px;" align="center">
                                <button style="width:40%;" class="btn btn-block btn-info" ng-click="update()">ค้นหา
                                </button>
                            </div>
                            <!-- /.Select group -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="box box-primary" ng-if="DepartSelect != ''">
                        <div class="box-header with-border">
                            <h3 class="box-title">ตั้งค่าข้อมูลสำหรับการพิพม์ออกใบลา :</h3>
                            <h3 class="box-title" style="color:red">{{DepartHeader}}</h3>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="row" align="center">
                                <div class="col-md-12" align="center">
                                    <div ng-bind-html="htmlTrusted(ShowConfigText)"></div>
                                </div>
                            </div>
                            <div style="border-bottom: 2px solid">
                                <div ng-repeat="ManageData in M_Manager">
                                    <div id="Count{{ManageData.Count}}">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: teal;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ผู้มีอำนาจอนุมัติ</p>
                                                            <p style="text-align: center;color: deeppink;font-weight: bold;text-align: right;margin-top: 10px">
                                                                (ระดับผู้บริหาร)</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select ui-select2 class="form-control select2"
                                                                    style="width: 100%;"
                                                                    ng-model="approverSelect_M[ManageData.Count]" ng-init="approverSelect_M[ManageData.Count] = ''+ManageData.PERID"
                                                                    id="approverManager{{ManageData.Count}}">
                                                                <option ng-repeat="person in PersonCommit"
                                                                        ng-selected="approverSelect_M[ManageData.Count] == person.PERID"
                                                                        value="{{person.PERID}}">{{person.NAME}}
                                                                    {{person.SURNAME}} : {{person.Dep_name}}
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2" align="center">
                                                            <button class="btn btn-success" style="font-weight: bold"
                                                                    ng-click="insertM()" ng-if="$index < 1">เพิ่ม
                                                            </button>
                                                            <button class="btn btn-danger" style="font-weight: bold"
                                                                    ng-click="DeleteM($index)" ng-if="$index >= 1">ลบ
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: brown;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ตำแหน่ง</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control"
                                                                   id="approverManager{{ManageData.Count}}_p"
                                                                   value="{{ManageData.P_STATUS}}"/>
                                                        </div>
                                                        <div class="col-md-2">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="border-bottom: 2px solid">
                                <div ng-repeat="ManageData in L_Manager">
                                    <div id="Count{{ManageData.Count}}">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: teal;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ผู้มีอำนาจอนุมัติ</p>
                                                            <p style="text-align: center;color: deeppink;font-weight: bold;text-align: right;margin-top: 10px">
                                                                (ระดับหัวหน้า)</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select ui-select2 class="form-control select2"
                                                                    style="width: 100%;"
                                                                    ng-model="approverSelect_L[ManageData.Count]" ng-init="approverSelect_L[ManageData.Count] = ''+ManageData.PERID"
                                                                    id="approverLeader{{ManageData.Count}}">
                                                                <option ng-repeat="person in PersonCommit"
                                                                        ng-selected="approverSelect_L[ManageData.Count] == person.PERID"
                                                                        value="{{person.PERID}}">{{person.NAME}}
                                                                    {{person.SURNAME}}
                                                                    : {{person.Dep_name}}
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2" align="center">
                                                            <button class="btn btn-success" style="font-weight: bold"
                                                                    ng-click="insertL()" ng-if="$index < 1">เพิ่ม
                                                            </button>
                                                            <button class="btn btn-danger" style="font-weight: bold"
                                                                    ng-click="DeleteL($index)" ng-if="$index >= 1">ลบ
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: brown;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ตำแหน่ง</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control"
                                                                   id="approverLeader{{ManageData.Count}}_p"
                                                                   value="{{ManageData.P_STATUS}}"/>
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="border-bottom: 2px solid">
                                <div ng-repeat="ManageData in P_Manager">
                                    <div id="Count{{ManageData.Count}}">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: teal;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ผู้มีอำนาจอนุมัติขั้นต้น</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select ui-select2 class="form-control select2"
                                                                    style="width: 100%;"
                                                                    ng-model="approverSelect_P[ManageData.Count]" ng-init="approverSelect_P[ManageData.Count] = ''+ManageData.PERID"
                                                                    id="approverPerson{{ManageData.Count}}">
                                                                <option ng-repeat="person in PersonCommit"
                                                                        ng-selected="approverSelect_P[ManageData.Count] == person.PERID"
                                                                        value="{{person.PERID}}">{{person.NAME}}
                                                                    {{person.SURNAME}}
                                                                    : {{person.Dep_name}}
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2" align="center">
                                                            <button class="btn btn-success" style="font-weight: bold"
                                                                    ng-click="insertP()" ng-if="$index < 1">เพิ่ม
                                                            </button>
                                                            <button class="btn btn-danger" style="font-weight: bold"
                                                                    ng-click="DeleteP($index)" ng-if="$index >= 1">ลบ
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: brown;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ตำแหน่ง</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control"
                                                                   id="approverPerson{{ManageData.Count}}_p"
                                                                   value="{{ManageData.P_STATUS}}"/>
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="border-bottom: 2px solid">
                                <div ng-repeat="ManageData in I_Manager">
                                    <div id="Count{{ManageData.Count}}">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: teal;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ผู้ตรวจสอบข้อมูล</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select ui-select2 class="form-control select2"
                                                                    style="width: 100%;"
                                                                    ng-model="approverSelect[ManageData.Count]" ng-init="approverSelect[ManageData.Count] = ''+ManageData.PERID" ng-change=""
                                                                    id="approverInvest{{ManageData.Count}}">
                                                                <option ng-repeat="person in PersonCommit"
                                                                        ng-selected="approverSelect[ManageData.Count] == person.PERID"
                                                                        value="{{person.PERID}}">{{person.NAME}}
                                                                    {{person.SURNAME}}
                                                                    : {{person.Dep_name}}
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2" align="center">
                                                            <button class="btn btn-success" style="font-weight: bold"
                                                                    ng-click="insertI()" ng-if="$index < 1">เพิ่ม
                                                            </button>
                                                            <button class="btn btn-danger" style="font-weight: bold"
                                                                    ng-click="DeleteI($index)" ng-if="$index >= 1">ลบ
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin-top: 15px">
                                                        <div class="col-md-6">
                                                            <p style="text-align: center;color: brown;font-weight: bold;text-align: right;margin-top: 10px">
                                                                ตำแหน่ง</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control"
                                                                   id="approverInvest{{ManageData.Count}}_p"
                                                                   value="{{ManageData.P_STATUS}}"/>
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body no-padding">
                                <div style="padding: 20px;" align="center" ng-if="ApproveP.length == 0">
                                    <button class="btn btn-info" ng-click="settingNote('บันทึก')">
                                        <h5 style="color: white;font-weight: bold">บันทึกข้อมูลผู้อนุมัติใบลา</h5>
                                    </button>
                                </div>
                                <div style="padding: 20px;" align="center" ng-if="ApproveP.length > 0">
                                    <button class="btn btn-warning" ng-click="settingNote('แก้ไข')">
                                        <h5 style="color: white;font-weight: bold">แก้ไขข้อมูลผู้อนุมัติใบลา</h5>
                                    </button>
                                </div>
                                <!-- /.Select group -->
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <section class="content" ng-if="!CheckPermiss">
            <div style="position: fixed;top: 50%;left: 30%;" align="center">
                <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
            </div>
        </section>
        <!-- /.content -->
        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/ui-select/dist/select2.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>
<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: true,
            showMeridian: false,
            minuteStep: 1
        })
    })
</script>
<script>
    var app = angular.module("myApp", ["datatables", 'ui.select2']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, $interval, DTOptionsBuilder, $sce) {
        $scope.Depart = <?php echo $this->Department; ?>;
        $scope.PersonCommit = <?php echo $this->PersonCommit; ?>;
        $scope.Loading = true;
        $scope.ApproveP = [];
        $scope.M_Manager = [];
        $scope.L_Manager = [];
        $scope.P_Manager = [];
        $scope.I_Manager = [];
        $scope.Count_M = 0;
        $scope.Count_L = 0;
        $scope.Count_P = 0;
        $scope.Count_I = 0;
        $scope.Position1 = '';
        $scope.Position2 = '';
        $scope.Position3 = '';
        $scope.Position4 = '';
        $scope.CheckPermiss = true;

        $scope.init = function () {

            data = {
                'MENU_CODE': '007',
            };

            $http.post('./ApiService/CheckPermissMenu', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    if (response.data.Status) {
                        $scope.CheckPermiss = true;
                        var check_Map_Dep = false;
                        angular.forEach($scope.Depart, function (item, idx) {
                            if (item.Dep_Code == <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>) {
                                $scope.DepartSelect = "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>;
                                check_Map_Dep = true;
                                $scope.getAllApproveLeave();
                            } else {
                                if(idx == $scope.Depart.length - 1 && !check_Map_Dep){
                                    $scope.DepartSelect = '';
                                    $scope.Loading = false;
                                }
                            }
                        })

                    } else {
                        $scope.CheckPermiss = false;
                        $scope.Loading = false;
                    }
                });

        }

        $timeout($scope.init)

        $scope.insertM = function () {
            $scope.M_Manager.push({
                Count: ++$scope.Count_M,
            });
        }

        $scope.DeleteM = function (idx) {
            $scope.M_Manager.splice(idx, 1);
            $scope.Count_M--;
        }

        $scope.insertL = function () {
            $scope.L_Manager.push({
                Count: ++$scope.Count_L,
            });
        }

        $scope.DeleteL = function (idx) {
            $scope.L_Manager.splice(idx, 1);
            $scope.Count_L--;
        }

        $scope.insertP = function () {
            $scope.P_Manager.push({
                Count: ++$scope.Count_P,
            });
        }

        $scope.DeleteP = function (idx) {
            $scope.P_Manager.splice(idx, 1);
            $scope.Count_P--;
        }

        $scope.insertI = function () {
            $scope.I_Manager.push({
                Count: ++$scope.Count_I,
            });
        }

        $scope.DeleteI = function (idx) {
            $scope.I_Manager.splice(idx, 1);
            $scope.Count_I--;
        }

        $scope.getAllApproveLeave = function () {
            $scope.Loading = true;
            // var DepartIsNull = angular.element(document.querySelector('#DepartCode'));
            //if (DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value == null || DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value == '? undefined:undefined ?') {
            //    if ('<?php //echo $this->model->Status; ?>//' == 'ผู้ดูแลระบบ') {
            //
            //    } else {
            //        $scope.DepartSelect = $scope.Depart[0].Dep_Code;
            //    }
            //}
            var DepartIsNull = angular.element(document.querySelector('#DepartCode'));

            if (DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != null && DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.DepartSelect = DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value;
            }

            data = {
                'DepCode': $scope.DepartSelect
            };
            $http.post('./ApiService/GetConfigLeave', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    var result = response.data;
                    $scope.ApproveP = [];
                    $scope.ApproveP = response.data;
                    if (response.data.length > 0) {

                        $scope.M_Manager = [];
                        $scope.L_Manager = [];
                        $scope.P_Manager = [];
                        $scope.I_Manager = [];
                        $scope.Count_M = 0;
                        $scope.Count_L = 0;
                        $scope.Count_P = 0;
                        $scope.Count_I = 0;

                        angular.forEach($scope.ApproveP, function (item) {
                            switch (parseInt(item.P_STATUS)) {
                                case 0 :
                                    $scope.M_Manager.push({
                                        Count: ++$scope.Count_M,
                                        PERID: item.PERID,
                                        P_STATUS: item.POSI_STATUS
                                    });
                                    break;
                                case 1 :
                                    $scope.L_Manager.push({
                                        Count: ++$scope.Count_L,
                                        PERID: item.PERID,
                                        P_STATUS: item.POSI_STATUS
                                    });
                                    break;
                                case 2 :
                                    $scope.P_Manager.push({
                                        Count: ++$scope.Count_P,
                                        PERID: item.PERID,
                                        P_STATUS: item.POSI_STATUS
                                    });
                                    break;
                                case 3 :
                                    $scope.I_Manager.push({
                                        Count: ++$scope.Count_I,
                                        PERID: item.PERID,
                                        P_STATUS: item.POSI_STATUS
                                    });
                                    break;
                                default:
                            }
                        })

                        $scope.ShowConfigText = "<h2  style='color: green;font-weight: bold'>ข้อมูลถูกบันทึกเมื่อวันที่ " + $scope.TimeToShow(result[0].CREATE_TIME.split(' ')[0]) + " </h2>"

                    } else {

                        $scope.M_Manager = [];
                        $scope.L_Manager = [];
                        $scope.P_Manager = [];
                        $scope.I_Manager = [];
                        $scope.Count_M = 0;
                        $scope.Count_L = 0;
                        $scope.Count_P = 0;
                        $scope.Count_I = 0;

                        $scope.M_Manager.push({
                            Count: ++$scope.Count_M,
                        });
                        $scope.L_Manager.push({
                            Count: ++$scope.Count_L,
                        });
                        $scope.P_Manager.push({
                            Count: ++$scope.Count_P,
                        });
                        $scope.I_Manager.push({
                            Count: ++$scope.Count_I,
                        });

                        $scope.ShowConfigText = "<h2  style='color: red;font-weight: bold'>ไม่พบการตั้งค่าข้อมูลใบลา</h2>"
                    }
                    $scope.Loading = false;
                });
        }

        // ตั้งค่าข้อมูลใบลาสำหรับการพิมพ์
        $scope.settingNote = function (textHeader) {
            $scope.PersonApproveData_M = [];
            $scope.PersonApproveData_L = [];
            $scope.PersonApproveData_P = [];
            $scope.PersonApproveData_I = [];
            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "คุณต้องการ" + textHeader + "ข้อมูลบุคคลการอนุมัติหรือตรวจสอบข้อมูลใช่หรือไม่",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'ยกเลิก',
                confirmButtonText: 'ตกลง'
            }).then((result) => {
                if (result.value) {

                    angular.forEach($scope.M_Manager, function (item) {
                        if(angular.element(document.querySelector('#approverManager' + item.Count))[0].options[angular.element(document.querySelector('#approverManager' + item.Count))[0].selectedIndex].value > 0){
                            $scope.PersonApproveData_M.push({
                                'PERID': angular.element(document.querySelector('#approverManager' + item.Count))[0].options[angular.element(document.querySelector('#approverManager' + item.Count))[0].selectedIndex].value,
                                'Position': document.getElementById('approverManager' + item.Count + '_p').value
                            });
                        }
                    })
                    angular.forEach($scope.L_Manager, function (item) {
                        if(angular.element(document.querySelector('#approverLeader' + item.Count))[0].options[angular.element(document.querySelector('#approverLeader' + item.Count))[0].selectedIndex].value > 0){
                            $scope.PersonApproveData_L.push({
                                'PERID': angular.element(document.querySelector('#approverLeader' + item.Count))[0].options[angular.element(document.querySelector('#approverLeader' + item.Count))[0].selectedIndex].value,
                                'Position': document.getElementById('approverLeader' + item.Count + '_p').value
                            });
                        }
                    })
                    angular.forEach($scope.P_Manager, function (item) {
                        if(angular.element(document.querySelector('#approverPerson' + item.Count))[0].options[angular.element(document.querySelector('#approverPerson' + item.Count))[0].selectedIndex].value > 0){
                            $scope.PersonApproveData_P.push({
                                'PERID': angular.element(document.querySelector('#approverPerson' + item.Count))[0].options[angular.element(document.querySelector('#approverPerson' + item.Count))[0].selectedIndex].value,
                                'Position': document.getElementById('approverPerson' + item.Count + '_p').value
                            });
                        }
                    })
                    angular.forEach($scope.I_Manager, function (item) {
                        if(angular.element(document.querySelector('#approverInvest' + item.Count))[0].options[angular.element(document.querySelector('#approverInvest' + item.Count))[0].selectedIndex].value > 0){
                            $scope.PersonApproveData_I.push({
                                'PERID': angular.element(document.querySelector('#approverInvest' + item.Count))[0].options[angular.element(document.querySelector('#approverInvest' + item.Count))[0].selectedIndex].value,
                                'Position': document.getElementById('approverInvest' + item.Count + '_p').value
                            });
                        }
                    })

                    if (($scope.PersonApproveData_M.length > 0) && ($scope.PersonApproveData_L.length > 0) &&
                        ($scope.PersonApproveData_P.length > 0) && ($scope.PersonApproveData_I.length > 0)) {
                            data = {
                            'A_M': $scope.PersonApproveData_M,
                            'A_L': $scope.PersonApproveData_L,
                            'A_P': $scope.PersonApproveData_P,
                            'A_I': $scope.PersonApproveData_I,
                            'Perid': <?php echo $_SESSION['PERID'] ?>,
                            'DepCode': document.getElementById("DepartCode").value
                        };
                        $http.post('./ApiService/SaveConfigLeave', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                var result = response.data;
                                if (result.Status) {
                                    Swal({
                                        type: 'success',
                                        title: result.Message
                                    })
                                    $scope.update();
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: result.Message
                                    })
                                }
                            });
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถบันทึกข้อมูลได้ กรถณากรอกข้อมูลให้ครบถ้วน'
                        })
                    }
                }
            });
        }


        $scope.update = function () {
            data = {
                'DepCode': document.getElementById("DepartCode").value
            };
            $http.post('./ApiService/GetConfigLeave', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    var result = response.data;
                    $scope.ApproveP = [];
                    $scope.ApproveP = response.data;
                    if (response.data.length > 0) {
                        $scope.approverSelect1 = result[0].PERID;
                        $scope.approverSelect2 = result[1].PERID;
                        $scope.approverSelect3 = result[2].PERID;
                        $scope.approverSelect4 = result[3].PERID;
                        $scope.Position1 = result[0].POSI_STATUS;
                        $scope.Position2 = result[1].POSI_STATUS;
                        $scope.Position3 = result[2].POSI_STATUS;
                        $scope.Position4 = result[3].POSI_STATUS;
                        $scope.ShowConfigText = "<h2  style='color: green;font-weight: bold'>ข้อมูลถูกบันทึกเมื่อวันที่ " + result[0].CREATE_TIME.split(' ')[0] + " </h2>"
                    } else {
                        $scope.approverSelect1 = 0;
                        $scope.approverSelect2 = 0;
                        $scope.approverSelect3 = 0;
                        $scope.approverSelect4 = 0;
                        $scope.Position1 = '';
                        $scope.Position2 = '';
                        $scope.Position3 = '';
                        $scope.Position4 = '';
                        $scope.ShowConfigText = "<h2  style='color: red;font-weight: bold'>ไม่พบการตั้งค่าข้อมูลใบลา</h2>"
                    }
                });
        }

        $scope.htmlTrusted = function (html) {
            return $sce.trustAsHtml(html);
        }

        // แปลงค่าข้อมูลเวลา ให้อยู่ในรูปแบบ วัน เดือน ปี ตย. 12 ธันวาคม 2561
        $scope.TimeToShow = function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var Day = value.split('-')[2];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return DateTrans;
        }

    });

    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('/')[1];
                var Month = value.split('/')[0];
                var Year = value.split('/')[2];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    Year;
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('timeDate', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var ampm = Hour >= 12 ? 'PM' : 'AM';
            Hour = Hour % 12;
            Hour = Hour ? Hour : 12;
            var strTime = Hour + ':' + Minutes + ' ' +
                ampm;
            return (
                strTime
                // value.getHours() >= 13 ? (value.getHours() - 12) : (value.getHours())) + ":" + (
                // value.getMinutes() < 10 ? '0' : '') + value.getMinutes() + (value.getHours() > 11 ?
                // 'pm' : 'am'
            );
        }
    });

    app.filter('timeFormat2Digit', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var strTime = Hour + ':' + Minutes
            return (
                strTime
            );
        }
    });
</script>
</body>

</html>