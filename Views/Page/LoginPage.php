<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- <link rel="apple-touch-icon" sizes="76x76" href="./tools/creative-tim/assets/img/apple-icon.png"> -->
    <link rel="icon" type="image/png" href="./tools/creative-tim/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>ระบบ เข้า/ออก งานคณะแพทยศาสตร์</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="./tools/creative-tim/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="./tools/creative-tim/assets/css/light-bootstrap-dashboard.css?v=2.0.1" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="./tools/creative-tim/assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif" />
</head>

<body ng-app="myApp">
    <div class="wrapper wrapper-full-page" ng-controller="myCtrl">
        <div class="full-page  section-image" data-color="black" data-image="./tools/creative-tim/assets/img/medhos.jpg" ;>
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                        <div class="card card-login card-hidden">
                            <div class="card-header ">
                                <h3 class="header text-center"><b>ระบบ เข้า/ออก งานคณะแพทยศาสตร์</b></h3>
                            </div>
                            <div class="card-body ">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>PERID</label>
                                        <input type="text" placeholder="PERID" id="Username" class="form-control" name="Username" ng-model="Username" ng-keyup="LoginKeyEnterCheck(event=$event)">
                                    </div>
                                    <div class="form-group">
                                        <label>PIN</label>
                                        <input type="password" placeholder="PIN Number" id="Password" class="form-control" name="Password" ng-model="Password" ng-keyup="LoginKeyEnterCheck(event=$event)">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto" align="center">
                                <button type="submit" class="btn btn-info btn-wd" ng-click="Login()">เข้าสู่ระบบ</button>
                                <p style="cursor: pointer;color: red" ng-click="GetPin()">ลืมรหัส pin</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 ml-auto mr-auto" align="center">
                        <p style="color:white;font-weight: bold">จัดทำโดย</p>
                        <p style="color:burlywood;font-weight: bold">ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์มหาวิทยาลัยสงขลานครินทร์</p>
                        <p style="color:white;font-weight: bold">พบปัญหาการใช้งาน</p>
                        <p style="color:burlywood;font-weight: bold">ติดต่อ ธวัชชัย จันทรทิพย์(ตั้ม) โทร. 1951</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SweetDialog JS-->
    <script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>
    <!--AngularJS-->
    <script src="./tools/Js/angular.min.js"></script>
    <script type="text/javascript">
        var app = angular.module("myApp", []);

        app.controller("myCtrl", function ($scope, $http, $window, $timeout) {
            $scope.Username = "";
            $scope.Password = "";
            $scope.StatusLogin = {
                'Status': false,
                'Message': ''
            };

            $scope.init = function () {
                document.getElementById('Username').focus();
            }

            $timeout($scope.init)

            $scope.GetPin = function () {
                Swal.mixin({
                    input: 'text',
                    confirmButtonText: 'ถัดไป &rarr;',
                    showCancelButton: true,
                    cancelButtonText: 'ยกเลิก',
                    progressSteps: ['1', '2']
                }).queue([{
                        title: 'รหัสบุคลากรของคุณคือ ??',
                        text: 'โปรดกรอกรหัสบุคลากร'
                    },
                    'รหัสบัตรประชาชน'
                ]).then((result) => {
                    if (result.value) {
                        data = {
                            'PERID': result.value[0],
                            'PERSONALID': result.value[1]
                        };

                        $http.post('./ApiService/GetPinNumber', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                            .then(function successCallback(response) {
                                if (response.data.length > 0) {
                                    Swal({
                                        type: 'success',
                                        title: 'รหัส PIN Number คือ',
                                        html: '<h3 style="color:red;font-weight:bold">' + response.data[0].PIN + '</h3>'
                                    })
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: 'พบข้อผิดพลาด...',
                                        text: 'ข้อมูลไม่ถูกต้องกรุณาลองใหม่อีกครั้ง'
                                    })
                                }
                            });
                    }
                })
            }

            $scope.LoginKeyEnterCheck = function (event) {
                if (event.keyCode == 13) {
                    if (document.getElementById('Username').value != "" && document.getElementById('Password').value == "") {
                        document.getElementById('Password').focus();
                    } else if (document.getElementById('Username').value == "" && document.getElementById('Password').value != "") {
                        document.getElementById('Username').focus();
                    } else if (document.getElementById('Username').value == "" && document.getElementById('Password').value == "") {
                        document.getElementById('Username').focus();
                    } else {
                        $scope.Login();
                    }
                }
            }
            $scope.Login = function () {
                // $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
                if ($scope.Username == '' || $scope.Password == '') {
                    const toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        width: 300,
                        timer: 3000
                    });

                    toast({
                        type: 'error',
                        title: 'โปรดกรอกข้อมูลให้ครบถ้วน'
                    });
                    return;
                }
                data = {
                    'Username': $scope.Username,
                    'Password': $scope.Password
                };

                $http.post('./ApiService/CheckLogin', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        $scope.StatusLogin = response.data;
                        // $scope.$apply();
                        if ($scope.StatusLogin['Status'] == true) {
                            const toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                width: 300,
                                timer: 3000
                            });

                            toast({
                                type: 'success',
                                title: 'ยินดีต้อนรับเข้าสู่ระบบตรวจสอบเวลาเข้างาน'
                            });
                            if($scope.StatusLogin['License'] == "Administrator" || $scope.StatusLogin['License'] == "Supervisor"){
                                $timeout(function () {
                                    $window.location.href = './Date';
                                }, 3000);
                            }else {
                                $timeout(function () {
                                    $window.location.href = './WorkTime';
                                }, 3000);
                            }
                            // $window.location.href = '../MVCworkTime/Date';
                        } else {
                            const toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                width: 300,
                                timer: 3000
                            });

                            toast({
                                type: 'error',
                                title: $scope.StatusLogin['Message']
                            });
                        }
                    })
            };
        });
    </script>
</body>
<!--   Core JS Files   -->
<script src="./tools/creative-tim/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="./tools/creative-tim/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="./tools/creative-tim/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: https://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="./tools/creative-tim/assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
<!--  Chartist Plugin  -->
<script src="./tools/creative-tim/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/creative-tim/assets/js/plugins/bootstrap-notify.js"></script>
<!--  Share Plugin -->
<script src="./tools/creative-tim/assets/js/plugins/jquery.sharrre.js"></script>
<!--  jVector Map  -->
<script src="./tools/creative-tim/assets/js/plugins/jquery-jvectormap.js" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="./tools/creative-tim/assets/js/plugins/moment.min.js"></script>
<!--  DatetimePicker   -->
<script src="./tools/creative-tim/assets/js/plugins/bootstrap-datetimepicker.js"></script>
<!--  Sweet Alert  -->
<script src="./tools/creative-tim/assets/js/plugins/sweetalert2.min.js" type="text/javascript"></script>
<!--  Tags Input  -->
<script src="./tools/creative-tim/assets/js/plugins/bootstrap-tagsinput.js" type="text/javascript"></script>
<!--  Sliders  -->
<script src="./tools/creative-tim/assets/js/plugins/nouislider.js" type="text/javascript"></script>
<!--  Bootstrap Select  -->
<script src="./tools/creative-tim/assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<!--  jQueryValidate  -->
<script src="./tools/creative-tim/assets/js/plugins/jquery.validate.min.js" type="text/javascript"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="./tools/creative-tim/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--  Bootstrap Table Plugin -->
<script src="./tools/creative-tim/assets/js/plugins/bootstrap-table.js"></script>
<!--  DataTable Plugin -->
<script src="./tools/creative-tim/assets/js/plugins/jquery.dataTables.min.js"></script>
<!--  Full Calendar   -->
<script src="./tools/creative-tim/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/creative-tim/assets/js/light-bootstrap-dashboard.js?v=2.0.1" type="text/javascript"></script>
<!-- Light Dashboard DEMO methods, don't include it in your project! -->
<script src="./tools/creative-tim/assets/js/demo.js"></script>
<script>
    $(document).ready(function () {
        demo.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>


</html>