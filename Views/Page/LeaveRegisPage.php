<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        table.table-bordered {
            border: 1px solid rgb(255, 123, 0);
            margin-top: 20px;
        }

        table.table-bordered > thead > tr > th {
            border: 1px solid rgb(255, 123, 0);
        }

        table.table-bordered > tbody > tr > td {
            border: 1px solid rgb(255, 123, 0);
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table > caption + thead > tr:first-child > td,
        .table > caption + thead > tr:first-child > th,
        .table > colgroup + thead > tr:first-child > td,
        .table > colgroup + thead > tr:first-child > th,
        .table > thead:first-child > tr:first-child > td,
        .table > thead:first-child > tr:first-child > th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        th {
            white-space: nowrap;
        }

        td {
            white-space: nowrap;
        }

        tr {
            white-space: nowrap;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <div class="content-wrapper" ng-controller="myCtrl">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                ตั้งค่าเริ่มต้นวันลาสะสม
                <small>บันทึกสรุปข้อมูลวันลาสะสมของของบุคลากรในหน่วยงาน</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> จัดการ การลา</a></li>
                <li class="active">ตั้งค่าเริ่มต้นวันลาสะสม</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" ng-if="CheckPermiss">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="panel panel-default">
                            <div class="box-body no-padding">
                                <div align="center">
                                    <label style="margin-top: 15px;font-weight: bold;font-size: 26px">หน่วยงาน</label>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4">
                                        <div style="padding: 20px">
                                            <select class="form-control select2" style="width: 100%;"
                                                    ng-model="DepartSelect" ng-change="update()" id="DepartCode">
                                                <option value="0">แสดงข้อมูลทั้งหมด</option>
                                                <option ng-repeat="depart in Depart"
                                                        ng-selected="DepartSelect == depart.Dep_Code"
                                                        value="{{depart.Dep_Code}}">{{depart.Dep_name}} :
                                                    {{depart.Dep_Group_name}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4"></div>
                                </div>
                                <!-- /.Select group -->
                            </div>
                            <div class="box-header">
                                <h3 class="box-title">ตารางแสดงข้อมูลรายชื่อการตั้งค่าเริ่มต้นวันลาสะสมผ่านระบบ</h3>
                            </div>

                            <div class="box-body">
                                <div class="table-responsive">
                                    <table datatable="ng" dt-options="dtOptions" class="table table-bordered"
                                           width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>รหัสบุคลากร</th>
                                            <th>ชื่อ - สกุล</th>
                                            <th>ลากิจ</th>
                                            <th>ลาป่วย</th>
                                            <th>ลาพักผ่อน</th>
                                            <th>ปีงบประมาณ</th>
                                            <th>ทำรายการ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="odd gradeX" ng-repeat="Person in PersonInDepartToShow"
                                            style="background-color: antiquewhite;" id="{{Person.PERID}}_Header">
                                            <td style="padding: 8px;text-align: center; vertical-align: middle;"><span
                                                        style="font-weight: bold">{{Person.PERID}}</span></td>
                                            <td style="padding: 8px;text-align: center; vertical-align: middle;"><span
                                                        style="font-weight: bold">{{Person.NAME}} {{Person.SURNAME}}</span>
                                            </td>
                                            <td style="padding: 8px;text-align: center; vertical-align: middle;">
                                                <div align="center" style="text-align: right">
                                                    <span style="font-weight: bold">ลามาแล้ว</span>
                                                    <input type="number"
                                                           style="text-align: center;margin-left: 15px;margin-right: 15px;width: 70px;margin-top: 10px"
                                                           id="{{Person.PERID}}_TimeLeave0"/>
                                                    <span style="font-weight: bold">ครั้ง</span>
                                                </div>
                                                <div align="center" style="text-align: right">
                                                    <span style="font-weight: bold">จำนวน</span>
                                                    <input type="number"
                                                           style="text-align: center;margin-left: 15px;margin-right: 15px;width: 70px;margin-top: 10px"
                                                           id="{{Person.PERID}}_DayLeave0"/>
                                                    <span style="font-weight: bold">วัน</span>
                                                </div>
                                                <div align="center" style="text-align: right">
                                                    <span style="font-weight: bold">เป็นวันทำการ</span>
                                                    <input type="number"
                                                           style="text-align: center;margin-left: 15px;margin-right: 15px;width: 70px;margin-top: 10px"
                                                           id="{{Person.PERID}}_DayWorkLeave0"/>
                                                    <span style="font-weight: bold">วัน</span>
                                                </div>
                                            </td>
                                            <td style="padding: 8px;text-align: center; vertical-align: middle;">
                                                <div align="center" style="text-align: right">
                                                    <span style="font-weight: bold">ลามาแล้ว</span>
                                                    <input type="number"
                                                           style="text-align: center;margin-left: 15px;margin-right: 15px;width: 70px;margin-top: 10px"
                                                           id="{{Person.PERID}}_TimeLeave1"/>
                                                    <span style="font-weight: bold">ครั้ง</span>
                                                </div>
                                                <div align="center" style="text-align: right">
                                                    <span style="font-weight: bold">จำนวน</span>
                                                    <input type="number"
                                                           style="text-align: center;margin-left: 15px;margin-right: 15px;width: 70px;margin-top: 10px"
                                                           id="{{Person.PERID}}_DayLeave1"/>
                                                    <span style="font-weight: bold">วัน</span>
                                                </div>
                                                <div align="center" style="text-align: right">
                                                    <span style="font-weight: bold">เป็นวันทำการ</span>
                                                    <input type="number"
                                                           style="text-align: center;margin-left: 15px;margin-right: 15px;width: 70px;margin-top: 10px"
                                                           id="{{Person.PERID}}_DayWorkLeave1"/>
                                                    <span style="font-weight: bold">วัน</span>
                                                </div>
                                            </td>
                                            <td style="padding: 8px;text-align: center; vertical-align: middle;">
                                                <div align="center" style="text-align: center">
                                                    <span style="font-weight: bold">วันลาพักผ่อนสะสม</span>
                                                    <input type="number"
                                                           style="text-align: center;margin-left: 15px;margin-right: 15px;width: 70px;margin-top: 10px"
                                                           id="{{Person.PERID}}_DayRestLeave2"/>
                                                    <span style="font-weight: bold">วัน</span>
                                                </div>
                                            </td>
                                            <td style="padding: 8px;text-align: center; vertical-align: middle;">
                                               <select class="form-control" id="{{Person.PERID}}_Year">
                                                   <option ng-repeat="fiscal in fiscalYear">{{fiscal.year}}</option>
                                               </select>
                                            </td>
                                            <td style="padding: 8px;text-align: center; vertical-align: middle;">
                                                <a class="btn btn-app" data-toggle="modal" data-target="#modal-default"
                                                   ng-click="SaveData(Person)">
                                                    <i class="fa fa-save" style="color:lightseagreen"></i> บันทึก
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="content" ng-if="!CheckPermiss">
            <div style="position: fixed;top: 50%;left: 30%;" align="center">
                <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
            </div>
        </section>

        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <?php require './Views/Footer.php' ?>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>
<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: true,
            showMeridian: false,
            minuteStep: 1
        })
    })
</script>
<script>
    var app = angular.module("myApp", ["datatables"]);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, $interval, DTOptionsBuilder) {
        $scope.Depart = <?php echo $this->Department; ?>;
        $scope.PersonInDepart = [];
        $scope.PersonInDepartToShow = [];
        $scope.His_Leave = [];
        $scope.fiscalYear = []
        $scope.CheckPermiss = true;

        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 500, 1000, 2500]);

        $scope.init = function () {
            //$scope.PrsonSelect = "" + <?php //echo $_SESSION['PERID'] ?>//;
            //$scope.DepartSelect = "" + <?php //echo($PersonalData["DataPerson"]["Edit_code"]); ?>//;
            //$scope.update();

            data = {
                'MENU_CODE': '008',
            };

            $http.post('./ApiService/CheckPermissMenu', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    if (response.data.Status) {
                        $scope.CheckPermiss = true;
                        var check_Map_Dep = false;

                        $scope.PrsonSelect = "" + <?php echo $_SESSION['PERID'] ?>;

                        angular.forEach($scope.Depart, function (item, idx) {
                            if (item.Dep_Code == <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>) {
                                $scope.DepartSelect = "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>;
                                check_Map_Dep = true;
                                $scope.update();
                            } else {
                                if(idx == $scope.Depart.length - 1 && !check_Map_Dep){
                                    $scope.DepartSelect = '';
                                    $scope.Loading = false;
                                }
                            }
                        })

                        $scope.GetYear()

                    } else {
                        $scope.CheckPermiss = false;

                    }
                });

        }

        $timeout($scope.init)

        $scope.update = function () {
            // $scope.Loading = true;
            $scope.PersonInDepart = []
            $scope.PersonInDepartToShow = []
            var DepartIsNull = angular.element(document.querySelector('#DepartCode'));

            if (DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != null && DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.DepartSelect = DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value;
            }
            // data = {
            //     'DepCode': $scope.DepartSelect,
            // };

            if($scope.DepartSelect == '0'){
                $scope.CheckAll = true;
            } else {
                $scope.CheckAll = false;
            }

            if($scope.CheckAll){
                data = {
                    'DepCode': $scope.Depart
                };
            } else {
                data = {
                    'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                };
            }

            $http.post('./ApiService/T_GetPersonDepart', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.PersonInDepart = response.data;

                    if ($scope.PersonInDepart.length > 0) {
                        // data = {
                        //     'DepCode': $scope.DepartSelect,
                        // };

                        if($scope.CheckAll){
                            data = {
                                'DepCode': $scope.Depart
                            };
                        } else {
                            data = {
                                'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                            };
                        }
                        $http.post('./ApiService/GetRefPersonal', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                if (response.data.length > 0) {
                                    var Copies = []
                                    angular.extend(Copies, $scope.PersonInDepart)
                                    angular.forEach($scope.PersonInDepart, function (data_I, idx) {
                                        angular.forEach(response.data, function (Ref_I) {
                                            if ($scope.CheckAll) {
                                                angular.forEach($scope.Depart, function (depart_I, idx_Depart) {
                                                    if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == depart_I.Dep_Code) {
                                                    } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == depart_I.Dep_Code) {
                                                        var CheckDep = false
                                                        angular.forEach($scope.Depart, function (depart_I2, idx_Dep) {
                                                            if (Ref_I.Dep_Code == depart_I2.Dep_Code) {
                                                                CheckDep = true
                                                            }
                                                        })
                                                        if (!CheckDep) {
                                                            if(Copies.length > 0){
                                                                angular.forEach(Copies, function (data_Copy, idx_c) {
                                                                    if(data_I.PERID == data_Copy.PERID){
                                                                        Copies.splice(idx_c, 1)
                                                                    }
                                                                })
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                });
                                            } else {
                                                if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == $scope.DepartSelect) {
                                                } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == $scope.DepartSelect) {
                                                    if(Copies.length > 0){
                                                        angular.forEach(Copies, function (data_Copy, idx_c) {
                                                            if(data_I.PERID == data_Copy.PERID){
                                                                Copies.splice(idx_c, 1)
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        })
                                    })
                                    $scope.PersonInDepart = []
                                    $scope.PersonInDepart = Copies
                                    $scope.PersonInDepartToShow = $scope.PersonInDepart

                                    if($scope.CheckAll){
                                        data = {
                                            'DepCode': $scope.Depart
                                        };
                                    } else {
                                        data = {
                                            'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                                        };
                                    }
                                    $http.post('./ApiService/L_GetDataRegis', data, {
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        }
                                    })
                                        .then(function successCallback(response) {
                                            $scope.His_Leave = response.data;
                                            angular.forEach($scope.His_Leave, function (His_item) {
                                                if(document.getElementById(His_item.PERID + '_Header') != null){
                                                    document.getElementById(His_item.PERID + '_Header').style.backgroundColor = 'honeydew';
                                                }

                                                if (His_item.Leave_Type == 0) {
                                                    if(document.getElementById(His_item.PERID + '_TimeLeave0') != null){
                                                        document.getElementById(His_item.PERID + '_TimeLeave0').value = His_item.Time_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayLeave0') != null){
                                                        document.getElementById(His_item.PERID + '_DayLeave0').value = His_item.Day_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayWorkLeave0') != null){
                                                        document.getElementById(His_item.PERID + '_DayWorkLeave0').value = His_item.DayWork_Leave;
                                                    }

                                                } else if (His_item.Leave_Type == 1) {
                                                    if(document.getElementById(His_item.PERID + '_TimeLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_TimeLeave1').value = His_item.Time_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_DayLeave1').value = His_item.Day_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_DayWorkLeave1').value = His_item.DayWork_Leave;
                                                    }

                                                } else if (His_item.Leave_Type == 2) {
                                                    if(document.getElementById(His_item.PERID + '_DayLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_DayRestLeave2').value = His_item.DayRest_Leave;
                                                    }

                                                }
                                            })
                                        });
                                } else {
                                    $scope.Loading = false;

                                    $scope.PersonInDepartToShow = $scope.PersonInDepart

                                    if($scope.CheckAll){
                                        data = {
                                            'DepCode': $scope.Depart
                                        };
                                    } else {
                                        data = {
                                            'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                                        };
                                    }
                                    $http.post('./ApiService/L_GetDataRegis', data, {
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        }
                                    })
                                        .then(function successCallback(response) {
                                            $scope.His_Leave = response.data;
                                            angular.forEach($scope.His_Leave, function (His_item) {
                                                if(document.getElementById(His_item.PERID + '_Header') != null){
                                                    document.getElementById(His_item.PERID + '_Header').style.backgroundColor = 'honeydew';
                                                }

                                                if (His_item.Leave_Type == 0) {
                                                    if(document.getElementById(His_item.PERID + '_TimeLeave0') != null){
                                                        document.getElementById(His_item.PERID + '_TimeLeave0').value = His_item.Time_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayLeave0') != null){
                                                        document.getElementById(His_item.PERID + '_DayLeave0').value = His_item.Day_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayWorkLeave0') != null){
                                                        document.getElementById(His_item.PERID + '_DayWorkLeave0').value = His_item.DayWork_Leave;
                                                    }

                                                } else if (His_item.Leave_Type == 1) {
                                                    if(document.getElementById(His_item.PERID + '_TimeLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_TimeLeave1').value = His_item.Time_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_DayLeave1').value = His_item.Day_Leave;
                                                    }
                                                    if(document.getElementById(His_item.PERID + '_DayLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_DayWorkLeave1').value = His_item.DayWork_Leave;
                                                    }

                                                } else if (His_item.Leave_Type == 2) {
                                                    if(document.getElementById(His_item.PERID + '_DayLeave1') != null){
                                                        document.getElementById(His_item.PERID + '_DayRestLeave2').value = His_item.DayRest_Leave;
                                                    }

                                                }
                                            })
                                        });
                                }
                            });
                    }

                });
        }

        $scope.GetYear = function() {
            $scope.fiscalYear = []
            $scope.fiscalYear.push({
                year : new Date().getFullYear() + 1
            })
            $scope.fiscalYear.push({
                year : new Date().getFullYear()
            })
            $scope.fiscalYear.push({
                year : new Date().getFullYear() - 1
            })
        }

        $scope.SaveData = function (PersonObj) {
            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "กรุณาตรวจสอบข้อมูลให้ครบถ้วนก่อนบันทึกข้อมูล",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {
                    var myElement = document.getElementById(PersonObj.PERID + '_Year');
                    data = {
                        'PERID': PersonObj.PERID,
                        'DepCode': PersonObj.Dep_Code,
                        '_TimeLeave0': document.getElementById(PersonObj.PERID + '_TimeLeave0').value,
                        '_DayLeave0': document.getElementById(PersonObj.PERID + '_DayLeave0').value,
                        '_DayWorkLeave0': document.getElementById(PersonObj.PERID + '_DayWorkLeave0').value,
                        '_TimeLeave1': document.getElementById(PersonObj.PERID + '_TimeLeave1').value,
                        '_DayLeave1': document.getElementById(PersonObj.PERID + '_DayLeave1').value,
                        '_DayWorkLeave1': document.getElementById(PersonObj.PERID + '_DayWorkLeave1').value,
                        '_DayRestLeave2': document.getElementById(PersonObj.PERID + '_DayRestLeave2').value,
                        '_Year': myElement.options[myElement.selectedIndex].value,
                        'PERID_Update': $scope.PrsonSelect
                    };
                    $http.post('./ApiService/L_RegisSaveData', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            var result = response.data;
                            if (result.Status) {
                                Swal({
                                    type: 'success',
                                    title: result.Message
                                });
                                data = {
                                    'DepCode': $scope.DepartSelect,
                                };
                                $http.post('./ApiService/L_GetDataRegis', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                    .then(function successCallback(response) {
                                        if ($scope.His_Leave.length > 0) {
                                            if(document.getElementById(PersonObj.PERID + '_Header') != null){
                                                document.getElementById(PersonObj.PERID + '_Header').style.backgroundColor = 'honeydew';
                                            }
                                        } else {
                                            $scope.His_Leave = response.data;
                                        }
                                    });
                            } else {
                                Swal({
                                    type: 'error',
                                    title: result.Message
                                });
                            }
                        });
                }
            });
        }

    });

    app.filter('timeDate', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var ampm = Hour >= 12 ? 'PM' : 'AM';
            Hour = Hour % 12;
            Hour = Hour ? Hour : 12;
            var strTime = Hour + ':' + Minutes + ' ' +
                ampm;
            return (
                strTime
                // value.getHours() >= 13 ? (value.getHours() - 12) : (value.getHours())) + ":" + (
                // value.getMinutes() < 10 ? '0' : '') + value.getMinutes() + (value.getHours() > 11 ?
                // 'pm' : 'am'
            );
        }
    });

    app.filter('timeFormat2Digit', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var strTime = Hour + ':' + Minutes
            return (
                strTime
            );
        }
    });
</script>
</body>

</html>
