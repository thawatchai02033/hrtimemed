<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HR - Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered {
            border: 1px solid #64FFDA;
            margin-top: 20px;
        }

        table.table-bordered > thead > tr > th {
            border: 1px solid #64FFDA;
        }

        table.table-bordered > tbody > tr > td {
            border: 1px solid #64FFDA;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .pic {
            margin: 50px;
            /* demo spacing */
            display: inline-block;
            vertical-align: middle;
            width: 0;
            height: 0;
        }

        .arrow-left {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-right: 30px solid #737373;
        }

        .arrow-right {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-left: 30px solid #737373;
        }

        .arrow-up {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-bottom: 30px solid #737373;
        }

        .arrow-down {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-top: 30px solid #737373;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        .myButton {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #44c767), color-stop(1, #5cbf2a));
            background: -moz-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -webkit-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -o-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -ms-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: linear-gradient(to bottom, #44c767 5%, #5cbf2a 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#44c767', endColorstr='#5cbf2a', GradientType=0);
            background-color: #44c767;
            -moz-border-radius: 28px;
            -webkit-border-radius: 28px;
            border-radius: 28px;
            border: 1px solid #18ab29;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: Arial;
            font-size: 17px;
            padding: 16px 31px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #2f6627;
        }

        .myButton:hover {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cbf2a), color-stop(1, #44c767));
            background: -moz-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -webkit-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -o-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -ms-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: linear-gradient(to bottom, #5cbf2a 5%, #44c767 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cbf2a', endColorstr='#44c767', GradientType=0);
            background-color: #5cbf2a;
        }

        .myButton:active {
            position: relative;
            top: 1px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div id="page-wrapper" class="content-wrapper" window-size my-directive ng-controller="myCtrl">
        <div class="fixed-plugin">
            <div class="dropdown show-dropdown" style="height: 60px;">
                <a href="#" data-toggle="dropdown1" aria-expanded="false" ng-click="GetPerson(CheckComment)">
                    <i class="fa fa-commenting fa-2x" style="margin-top: 15px;"> </i>
                </a>
                <ul class="dropdown-menu-slider" x-placement="bottom-start"
                    style="position: absolute; top: 41px; left: 8px; will-change: top, left; background-color: wheat;">
                    <li class="header-title"> Comment</li>
                    </hr>
                    <li class="header-title">เพิ่มหมายเหตุ : วันที่ {{DateSelect | DateThai}}</li>
                    <li class="adjustments-line">
                        <a href="javascript:void(0)" class="switch-trigger background-color">
                            <select class="form-control select2" style="width: 100%;" ng-model="PrsonSelect"
                                    ng-change="updatePerson()" id="PersonCode">
                                <option ng-repeat="perDep in personToComment" value="{{perDep.PERID}}">{{perDep.NAME}}
                                    {{perDep.SURNAME}}
                                </option>
                            </select>
                            <div class="clearfix"></div>
                        </a>
                    </li>
                    <li class="adjustments-line" style="margin-top: 25px">
                        <a href="javascript:void(0)" class="switch-trigger">
                            <textarea class="form-control" rows="5" placeholder="ข้อความ..." id="txtCommect"></textarea>
                            <div class="clearfix"></div>
                        </a>
                        <br>
                        <br>
                    </li>
                    <li class="adjustments-line" style="margin-top: 80px">
                        <a href="javascript:void(0)" class="switch-trigger">
                            <p style="color: red;text-align: center">***กรณีต้องการระบุเวลา เพื่อให้ผู้ดูแล/หัวหน้า
                                แก้ไขข้อมูล***</p>
                            <button class="btn btn-success" style="font-weight: bold;font-size: 14px;border-radius: 5px"
                                    ng-click="SettingTime()">ระบุเวลา
                            </button>
                            <div class="input-group" style="margin-top: 20px"
                                 ng-show="Check_TimeRange_I && CheckEditTime">
                                <input type="text" class="form-control timepicker" style="text-align: center"
                                       id="Comment_Time_I" value="00:00">

                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <div class="input-group" style="margin-top: 20px"
                                 ng-show="Check_TimeRange_O && CheckEditTime">
                                <input type="text" class="form-control timepicker" style="text-align: center"
                                       id="Comment_Time_O" value="00:00">

                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="adjustments-line" style="margin-top: 180px" ng-show="CheckEditTime">
                        <a href="javascript:void(0)" class="switch-trigger">
                            <button ng-class="{'btn btn-default' : !Check_TimeRange_I , 'btn btn-info' : Check_TimeRange_I}"
                                    style="font-weight: bold;font-size: 14px;border-radius: 5px;padding: 6px"
                                    ng-click="ChooseTimeRangeIn()">เวลาเข้างาน
                            </button>
                            <button ng-class="{'btn btn-default' : !Check_TimeRange_O , 'btn btn-info' : Check_TimeRange_O}"
                                    style="font-weight: bold;font-size: 14px;border-radius: 5px;padding: 6px"
                                    ng-click="ChooseTimeRangeOut()">เวลาออกงาน
                            </button>
                        </a>
                    </li>
                    <li class="adjustments-line" style="margin-top: 50px;margin-bottom: 25px">
                        <a href="javascript:void(0)" class="switch-trigger">
                            <button class="btn btn-rose btn-block btn-fill" style="font-size: 16px"
                                    ng-click="SaveComment()">บันทึก
                            </button>
                            <div class="clearfix"></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="fixed-plugin" style="margin-top: 70px">
            <div class="dropdown show-dropdown" style="height: 60px;">
                <a href="#" data-toggle="dropdown1" aria-expanded="false" ng-click="GetComment(CheckGetComment)">
                    <i class="fa fa-book fa-2x" style="margin-top: 15px;"> </i>
                </a>
                <ul class="dropdown-menu-slider" x-placement="bottom-start"
                    style="position: absolute; top: 41px; left: 8px; will-change: top, left; background-color: wheat;">
                    <li class="header-title">
                        <a href="javascript:void(0)" class="switch-trigger background-color">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span style="font-weight: bold;color: rgba(86,176,185,0.99);cursor: pointer" ng-click="GetComment()">(ข้อมูลประจำวัน)</span>
                                </div>
                                <div class="col-sm-6">
                                    <span style="font-weight: bold;color: rgba(177,99,185,0.99);cursor: pointer" ng-click="GetAllComment()">(ข้อมูลทั้งหมด)</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </a>
                    </li>
                    <li class="header-title"> Comment</li>
                    <li class="header-title" ng-if="ShowTitleComment">สรุปหมายเหตุ : วันที่ {{DateSelect | DateThai}}</li>
                    <hr style="border: 1px solid black;">
                    <div class="col-md-12" style="overflow-y: auto;overflow-x: hidden;height: 450px;">
                        <div ng-repeat="commentData in GetCommentData" ng-if="GetCommentData.length != 0"
                             align="center">
                            <label style="color: black;text-align: left;font-size: 13px;">หมายเหตุประจำวันที่ :
                                {{commentData.Comment_Date | DateTimeLine}}
                            </label>
                            <p style="font-weight: bold;color:indianred">คุณ: {{commentData.NAME}}
                                {{commentData.SURNAME}}</p>
                            <p style="color:navy;text-align: justify;text-justify: inter-word;">
                                หมายเหตุ*** : <span style="font-weight: bold;color: #005a76">{{commentData.Comment_Details}}</span>
                            </p>
                            <h5 style="color:navy;text-align: left;text-align: justify;text-justify: inter-word;"
                                ng-if="commentData.Time_I != null || commentData.Time_O != null">
                                ขอแก้ไขข้อมูลการปฏิบัติงาน:
                                <p style="font-weight: bold;color: #005a76" ng-if="commentData.Time_I != null">
                                    เวลาเข้างาน</p>
                                <p style="color:#ff3714" ng-if="commentData.Time_I != null">{{commentData.Time_I}} น.
                                    <span style="color:#ff39ee;font-weight: bold"
                                          ng-if="commentData.Time_Status == '0'">( รอพิจารณา )</span><span
                                            style="color:#00b92d;font-weight: bold"
                                            ng-if="commentData.Time_Status == '1'">( อนุมัติ )</span><span
                                            style="color:red;font-weight: bold" ng-if="commentData.Time_Status == '2'">( ไม่อนุมัติ )</span>
                                </p>
                                <p style="font-weight: bold;color: #005a76" ng-if="commentData.Time_O != null">
                                    เวลาออกงาน</p>
                                <p style="color:#ff3714" ng-if="commentData.Time_O != null">{{commentData.Time_O}} น.
                                    <span style="color:#ff39ee;font-weight: bold"
                                          ng-if="commentData.Time_Status == '0'">( รอพิจารณา )</span><span
                                            style="color:#00b92d;font-weight: bold"
                                            ng-if="commentData.Time_Status == '1'">( อนุมัติ )</span><span
                                            style="color:red;font-weight: bold" ng-if="commentData.Time_Status == '2'">( ไม่อนุมัติ )</span>
                                </p>
                            </h5>
                            <label style="color:green;text-align: left;font-size: 12px;margin-left: -54px">บันทึกเมื่อ:
                                {{commentData.Create_T}}
                            </label>
                            <hr style="border: 0.5px solid hotpink;"/>
                        </div>
                        <div ng-if="GetCommentData.length == 0">
                            <h5 style="font-weight: bold;color:indianred;text-align: center;margin-top: 50%;">
                                ไม่มีข้อมูล</h5>
                        </div>
                    </div>
                    <li class="adjustments-line">
                        <a href="javascript:void(0)" class="switch-trigger background-color">
                            <div class="clearfix"></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="fixed-plugin" style="margin-top: 140px">
            <div class="dropdown show-dropdown" style="height: 60px;cursor: pointer">
                <p ng-style="ChangeColor" ng-click="GetWFH(CheckGetComment)" data-toggle="modal"
                   data-target="#noticeModal">
                    WORK FROM HOME
                </p>
            </div>
        </div>
        <section class="content-header">
            <h1>
                รายละเอียดการเข้า-ออกงานประจำวัน
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-pie-chart"></i> การเข้า-ออกงาน</a></li>
                <li class="active">รายละเอียดการเข้า-ออกงาน</li>
            </ol>
        </section>
        <!-- /.row -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="panel panel-default">
                            <div class="box-header">
                                <h3 class="box-title">ตารางปฏิทินแสดงรายละเอียดการเข้า-ออกงานประจำวัน</h3>
                            </div>

                            <div class="box-body" style="display: none;">
                                <!-- Date -->
                                <div class="form-group">
                                    <div class="col-lg-3" style="text-align: center">
                                        <label>เรียกดูประจำวันที่:</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                   autocomplete="off">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="col-lg-3" align="center">
                                        <button type="button" class="btn btn-block btn-info"
                                                style="width:40%;align-content: center" ng-click="update()">เรียกดู
                                        </button>
                                    </div>
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="box-body" style="display: none;">
                                <!-- Date -->
                                <div class="form-group">
                                    <div class="col-lg-3" style="text-align: center">
                                        <label>เรียกดูตามช่วงวันที่:</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="reservation">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="col-lg-3" align="center">
                                        <button type="button" class="btn btn-block btn-info"
                                                style="width:40%;align-content: center" ng-click="updateRange()">เรียกดู
                                        </button>
                                    </div>
                                    <hr/>
                                </div>
                                <!-- /.form group -->
                            </div>

                            <div class="row" ng-if="windowWidth > 950">
                                <div class="col-md-1" align="center">
                                    <i class="fa fa-arrow-circle-left"
                                       style="font-size: 55px;margin-top: 60px;cursor: pointer;"
                                       ng-click="previousDay()" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-2" ng-repeat="time in DataTimeDepShow track by $index">
                                    <div class="card " ng-click="ShowDateSelect(time.DateShow)"
                                         style="cursor: pointer;background-color: wheat;"
                                         ng-if="time.DateName != 'Saturday' && time.DateName != 'Sunday'">
                                        <div class="card-header card-header-rose card-header-icon"
                                             ng-if="time.DateOff != ''">
                                            <div class="card-icon">
                                                <i class="material-icons">today</i>
                                            </div>
                                            <h4 class="card-title"
                                                style="text-align: center;font-size: 18px;font-weight: bold;color: #E53935;padding-top: 20px">
                                                {{time.DateName | NameTh}}</h4>
                                            <br/>
                                            <h4 class="card-title"
                                                style="text-align: center;font-size: 18px;font-weight: bold;color: #E53935;padding-bottom: 20px;">
                                                {{time.DateShow | DateThai}}</h4>
                                        </div>
                                        <div class="card-header card-header-success card-header-icon"
                                             ng-if="time.DateOff == ''">
                                            <div class="card-icon">
                                                <i class="material-icons">today</i>
                                            </div>
                                            <h4 class="card-title"
                                                style="text-align: center;font-size: 18px;font-weight: bold;color: green;padding-top: 20px">
                                                {{time.DateName | NameTh}}</h4>
                                            <br/>
                                            <h4 class="card-title"
                                                style="text-align: center;font-size: 18px;font-weight: bold;color: #9C27B0;padding-bottom: 20px;">
                                                {{time.DateShow | DateThai}}</h4>
                                        </div>
                                    </div>
                                    <div class="card " ng-click="ShowDateSelect(time.DateShow)"
                                         style="cursor: pointer;background-color: wheat;"
                                         ng-if="time.DateName == 'Saturday' || time.DateName == 'Sunday'">
                                        <div class="card-header card-header-rose card-header-icon">
                                            <div class="card-icon">
                                                <i class="material-icons">today</i>
                                            </div>
                                            <h4 class="card-title"
                                                style="text-align: center;font-size: 18px;font-weight: bold;color: #E53935;padding-top: 20px">
                                                {{time.DateName | NameTh}}</h4>
                                            <br/>
                                            <h4 class="card-title"
                                                style="text-align: center;font-size: 18px;font-weight: bold;color: #E53935;padding-bottom: 20px;">
                                                {{time.DateShow | DateThai}}</h4>
                                        </div>
                                    </div>
                                    <span class="info-box-text"
                                          style="font-size: 18px;text-align: center;font-weight: bold;color: #E53935"
                                          ng-if="time.DateOff != ''">{{time.DateOffName | DateNameCut}}</span>
                                </div>
                                <div class="col-md-1" align="center">
                                    <i class="fa fa-arrow-circle-right"
                                       style="font-size: 55px;margin-top: 60px;cursor: pointer;" ng-click="nextDay()"
                                       aria-hidden="true"></i>
                                </div>
                            </div>

                            <div class="box-header" align="center">
                                <label class="box-title">แสดงข้อมูลประจำวันที่ <h1
                                            style="color: green;font-weight: bold">{{DateSelect | DateThai}}</h1>
                                </label>
                            </div>
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">ตารางก่อนการยืนยันข้อมูล</a>
                                    </li>
                                    <li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">ตารางหลังการยืนยันข้อมูล</a>
                                    </li>
                                    <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">แสดงข้อมูลแบบ
                                            Timeline</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="activity">
                                        <div align="center">
                                            <div class="col-lg-6 col-xs-6">
                                                <label class="box-title" styles="border-color: coral">
                                                    <h1 style="color: cadetblue;font-weight: bold">เวลาเข้างาน <span
                                                                style="color: darkblue">(หน่วยงาน.)</span></h1>
                                                </label>
                                            </div>
                                            <div class="col-lg-6 col-xs-6">
                                                <label class="box-title" styles="border-color: coral">
                                                    <h1 style="color: cadetblue;font-weight: bold">เวลาออกงาน <span
                                                                style="color: darkblue">(หน่วยงาน.)</span></h1>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-xs-6" ng-if="uniqueStandards2[0].TIME_IN2 != ''" ng-click="modalShow()" style="cursor: pointer">
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueStandards2[0].TIME_IN2.indexOf('ลา') >= 0">
                                                            {{uniqueStandards2[0].TIME_IN2}}</h3>
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueStandards2[0].TIME_IN2.indexOf('ลา') == -1">
                                                            {{uniqueStandards2[0].TIME_IN2}} น. <span
                                                                    style="color: blueviolet"> {{statusIn}} </span>
                                                            <span style="margin-left: 10px;color: #0a2b1d" ng-if="EditTime.length > 0 && EditTime[0].status == '1' && EditTime[0].time_in != null">
                                                                ({{EditTime[0].time_in.split(' ')[0] | DateTimeLine}})
                                                            </span>
                                                        </h3>

                                                        <p style="color: black;font-weight: blod">เวลาเข้างาน ( <span>{{S_TimeIn}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod" ng-if="EditTime.length > 0 && EditTime[0].time_in != null">เวลาเข้างาน : (
                                                            <span style="color:coral;font-weight: bold">{{EditTime[0].time_in.split(' ')[1]}}</span>
                                                            ) <span style="font-weight: bold;color: #ffda00">(แก้ไขใหม่)</span>  สถานะ : <span style="font-weight: bold;color: #00b3ff" ng-if="EditTime[0].status == '0'">(รออนุมัติ)</span> <span style="font-weight: bold;color: #0a2b1d" ng-if="EditTime[0].status == '1'">(อนุมัติ)</span> <span style="font-weight: bold;color: red" ng-if="EditTime[0].status == '2'">(ไม่อนุมัติ)</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xs-6" ng-if="uniqueStandards2[0].TIME_IN2 == ''" ng-click="modalShow()" style="cursor: pointer">
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3><span style="color: deeppink"> - </span></h3>

                                                        <p style="color: black;font-weight: blod">เวลาเข้างาน ( <span>{{S_TimeIn}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod"
                                                           ng-if="GetCommentData.length > 0">**หมายเหตุ** : <span
                                                                    style="color:darkmagenta;font-weight: bold">{{GetCommentData[0].Comment_Details}}</span>
                                                        </p>
                                                        <p style="color: black;font-weight: blod" ng-if="EditTime.length > 0 && EditTime[0].time_in != null">เวลาเข้างาน : (
                                                            <span style="color:coral;font-weight: bold">{{EditTime[0].time_in.split(' ')[1]}}</span>
                                                            ) <span style="font-weight: bold;color: #ffda00">(แก้ไขใหม่)</span>  สถานะ : <span style="font-weight: bold;color: #00b3ff" ng-if="EditTime[0].status == '0'">(รออนุมัติ)</span> <span style="font-weight: bold;color: #0a2b1d" ng-if="EditTime[0].status == '1'">(อนุมัติ)</span> <span style="font-weight: bold;color: red" ng-if="EditTime[0].status == '2'">(ไม่อนุมัติ)</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xs-6" ng-if="uniqueStandards2[0].TIME_OUT2 != ''" ng-click="modalShow()" style="cursor: pointer">
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueStandards2[0].TIME_OUT2.indexOf('ลา') >= 0">
                                                            {{uniqueStandards2[0].TIME_OUT2}}</h3>
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueStandards2[0].TIME_OUT2.indexOf('ลา') == -1">
                                                            {{uniqueStandards2[0].TIME_OUT2}} น. <span
                                                                    style="color: blueviolet"> {{statusOut}} </span>
                                                            <span style="margin-left: 10px;color: #0a2b1d" ng-if="EditTime.length > 0 && EditTime[0].status == '1' && EditTime[0].time_out != null">
                                                                ({{EditTime[0].time_out.split(' ')[0] | DateTimeLine}})
                                                            </span>
                                                        </h3>
                                                        <p style="color: black;font-weight: blod"
                                                           ng-if="GetCommentData.length > 0">**หมายเหตุ** : <span
                                                                    style="color:darkmagenta;font-weight: bold">{{GetCommentData[0].Comment_Details}}</span>
                                                        </p>

                                                        <p style="color: black;font-weight: blod">เวลาออกงาน ( <span>{{S_TimeOut}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod" ng-if="EditTime.length > 0 && EditTime[0].time_out != null">เวลาออกงาน : (
                                                            <span style="color:coral;font-weight: bold">{{EditTime[0].time_out.split(' ')[1]}}</span>
                                                            ) <span style="font-weight: bold;color: #ffda00">(แก้ไขใหม่)</span>  สถานะ : <span style="font-weight: bold;color: #00b3ff" ng-if="EditTime[0].status == '0'">(รออนุมัติ)</span> <span style="font-weight: bold;color: #0a2b1d" ng-if="EditTime[0].status == '1'">(อนุมัติ)</span> <span style="font-weight: bold;color: red" ng-if="EditTime[0].status == '2'">(ไม่อนุมัติ)</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xs-6" ng-if="uniqueStandards2[0].TIME_OUT2 == ''" ng-click="modalShow()" style="cursor: pointer">
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3><span style="color: deeppink"> - </span></h3>

                                                        <p style="color: black;font-weight: blod">เวลาออกงาน ( <span>{{S_TimeOut}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod"
                                                           ng-if="GetCommentData.length > 0">**หมายเหตุ** : <span
                                                                    style="color:darkmagenta;font-weight: bold">{{GetCommentData[0].Comment_Details}}</span>
                                                        </p>
                                                        <p style="color: black;font-weight: blod" ng-if="EditTime.length > 0 && EditTime[0].time_out != null">เวลาออกงาน : (
                                                            <span style="color:coral;font-weight: bold">{{EditTime[0].time_out.split(' ')[1]}}</span>
                                                            ) <span style="font-weight: bold;color: #ffda00">(แก้ไขใหม่)</span>  สถานะ : <span style="font-weight: bold;color: #00b3ff" ng-if="EditTime[0].status == '0'">(รออนุมัติ)</span> <span style="font-weight: bold;color: #0a2b1d" ng-if="EditTime[0].status == '1'">(อนุมัติ)</span> <span style="font-weight: bold;color: red" ng-if="EditTime[0].status == '2'">(ไม่อนุมัติ)</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="timeline">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-lg-4" style="text-align: center">
                                                    <label>ช่วงระหว่างวันที่:</label>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right"
                                                               id="reservation2" data-date-language="th-th"
                                                               autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4" align="center">
                                                    <button type="button" class="btn btn-block btn-info"
                                                            style="width:40%;align-content: center"
                                                            ng-click="CalTimeLine()">เรียกดู
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <!-- The time line -->
                                                <ul class="timeline" ng-repeat="DateItem in DataTimeLine">
                                                    <!-- timeline time label -->
                                                    <li class="time-label">
                                                          <span class="bg-red" style="margin-top: 18px">
                                                            {{DateItem.DateDB | DateTimeLine}}
                                                          </span>
                                                        <span>
                                                            {{DateItem.DateName | NameTh}}
                                                          </span>
                                                        <span style="color: red;font-weight: bold"
                                                              ng-if="DateItem.DateOffName != ''">
                                                            ({{DateItem.DateOffName}})
                                                          </span>
                                                    </li>
                                                    <!-- /.timeline-label -->
                                                    <!-- timeline item -->
                                                    <li ng-repeat="TimeLine in DateItem.TimeLine">
                                                        <i class="fa fa-clock-o bg-aqua"></i>

                                                        <div class="timeline-item">
                                                            <span class="time"><i class="fa fa-clock-o"></i></span>

                                                            <h3 class="timeline-header"><a
                                                                        class="btn btn-primary btn-xs"
                                                                        style="font-size: 16px;background-color: azure;"><i
                                                                            class="fa fa-clock-o" style="color:red"></i>
                                                                    <span style="color: black"> {{TimeLine.event_time.split(' ')[1]}} น.</span></a>
                                                            </h3>
                                                            <h3 class="timeline-header"><a
                                                                        class="btn btn-primary btn-xs"
                                                                        style="font-size: 16px;background-color: azure;"><i
                                                                            class="fa fa-building"
                                                                            style="color:orangered"></i> <span
                                                                            style="color: black">ประตู : {{FindDoor(TimeLine.door_id)}}</span></a>
                                                            </h3>
                                                        </div>
                                                    </li>
                                                    <!-- END timeline item -->
                                                    <!-- timeline item -->

                                                    <li>
                                                        <i class="fa fa-stop bg-red"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3"></div>
                                            <!-- /.col -->
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->

                                    <div class="tab-pane" id="settings">
                                        <div align="center">
                                            <div class="col-lg-6 col-xs-6">
                                                <label class="box-title" styles="border-color: coral">
                                                    <h1 style="color: cadetblue;font-weight: bold">เวลาเข้างาน <span
                                                                style="color: darkblue">(หน่วยงาน.)</span></h1>
                                                </label>
                                            </div>
                                            <div class="col-lg-6 col-xs-6">
                                                <label class="box-title" styles="border-color: coral">
                                                    <h1 style="color: cadetblue;font-weight: bold">เวลาออกงาน <span
                                                                style="color: darkblue">(หน่วยงาน.)</span></h1>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-xs-6"
                                                 ng-if="uniqueDataApprove[0].A_Time_in2 != ''"
                                            >
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueDataApprove[0].A_Time_in2.indexOf('ลา') >= 0">
                                                            {{uniqueDataApprove[0].A_Time_in2}}</h3>
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueDataApprove[0].A_Time_in2.indexOf('ลา') == -1 && uniqueDataApprove[0].A_Status_in2 == ''">
                                                            {{uniqueDataApprove[0].A_Time_in2}} น.</h3>
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueDataApprove[0].A_Time_in2.indexOf('ลา') == -1 && uniqueDataApprove[0].A_Status_in2 != ''">
                                                            {{uniqueDataApprove[0].A_Time_in2}} น. <span
                                                                    style="color: blueviolet"> (สาย) </span></h3>
                                                        <h3 ng-if="uniqueDataApprove[0] == null"><span
                                                                    style="color: deeppink"> - </span></h3>

                                                        <p style="color: black;font-weight: blod">เวลาเข้างาน ( <span>{{S_TimeIn_A}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod"
                                                           ng-if="GetCommentData.length > 0">**หมายเหตุ** : <span
                                                                    style="color:darkmagenta;font-weight: bold">{{GetCommentData[0].Comment_Details}}</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xs-6"
                                                 ng-if="uniqueDataApprove[0].A_Time_in2 == ''">
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3><span style="color: deeppink"> - </span></h3>

                                                        <p style="color: black;font-weight: blod">เวลาเข้างาน ( <span>{{S_TimeIn_A}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod"
                                                           ng-if="GetCommentData.length > 0">**หมายเหตุ** : <span
                                                                    style="color:darkmagenta;font-weight: bold">{{GetCommentData[0].Comment_Details}}</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xs-6"
                                                 ng-if="uniqueDataApprove[0].A_Time_out2 != ''">
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueDataApprove[0].A_Time_out2.indexOf('ลา') >= 0">
                                                            {{uniqueDataApprove[0].A_Time_out2}}</h3>
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueDataApprove[0].A_Time_out2.indexOf('ลา') == -1 && uniqueDataApprove[0].A_Status_out2 == ''">
                                                            {{uniqueDataApprove[0].A_Time_out2}} น.</h3>
                                                        <h3 style="color: orangered"
                                                            ng-if="uniqueDataApprove[0].A_Time_out2.indexOf('ลา') == -1 && uniqueDataApprove[0].A_Status_out2 != ''">
                                                            {{uniqueDataApprove[0].A_Time_out2}} น. <span
                                                                    style="color: blueviolet"> (ออกก่อน) </span></h3>
                                                        <h3 ng-if="uniqueDataApprove[0] == null"><span
                                                                    style="color: deeppink"> - </span></h3>

                                                        <p style="color: black;font-weight: blod">เวลาออกงาน ( <span>{{S_TimeOut_A}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod"
                                                           ng-if="GetCommentData.length > 0">**หมายเหตุ** : <span
                                                                    style="color:darkmagenta;font-weight: bold">{{GetCommentData[0].Comment_Details}}</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xs-6"
                                                 ng-if="uniqueDataApprove[0].A_Time_out2 == ''">
                                                <!-- small box -->
                                                <div class="small-box" style="background-color: mintcream">
                                                    <div class="inner">
                                                        <h3><span style="color: deeppink"> - </span></h3>

                                                        <p style="color: black;font-weight: blod">เวลาออกงาน ( <span>{{S_TimeOut_A}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod">ประตูหน่วยงาน : (
                                                            <span style="color:coral;font-weight: bold">{{doorInName}}</span>
                                                            )</p>
                                                        <p style="color: black;font-weight: blod"
                                                           ng-if="GetCommentData.length > 0">**หมายเหตุ** : <span
                                                                    style="color:darkmagenta;font-weight: bold">{{GetCommentData[0].Comment_Details}}</span>
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="ion ion-ios-timer"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">ข้อมูลการเข้าปฏิบัติงาน</h4>
                        <h4 class="modal-title">คุณ : <span style="font-weight:bold">{{Modalname}}</span></h4>
                        <h4 class="modal-title" ng-repeat="CardID in CardNo">ข้อมูลหมายเลขบัตร : <span
                                    style="font-weight:bold;color: red">{{CardID.CARD_NO}}</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="box-body no-padding">
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-5" style="text-align: center">
                                        <label>วันที่:</label>
                                    </div>
                                    <div class="col-md-7" align="center">
                                        <label style="font-size: 20px;color: green;">{{DateSelect | DateThai}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>เวลาเข้างานเขต รพ.:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_TIN)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>สแกน ณ ประตู:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_DIN)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border: 1px solid black;"/>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>เวลาเข้างาน หน่วยงาน.:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_TIN2)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>สแกน ณ ประตู:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_DIN2)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border: 1px solid black;"/>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-5" style="text-align: center">
                                        <label>เวลาออก (พักเที่ยง):</label>
                                    </div>
                                    <div class="col-md-7" align="center">
                                        <label style="font-size: 16px;color: green;">
                                            <div ng-bind-html="htmlTrusted(Date_P_THO)"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>สแกน ณ ประตู:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_DHO)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border: 1px solid black;"/>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>เวลาเข้า (พักเที่ยง):</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_THN)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>สแกน ณ ประตู:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_DHN)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border: 1px solid black;"/>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>เวลาออกงานเขต รพ.:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_TO)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>สแกน ณ ประตู:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_DO)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border: 1px solid black;"/>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>เวลาออกงาน หน่วยงาน.:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_TO2)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group">
                                        <div class="col-md-5" style="text-align: center">
                                            <label>สแกน ณ ประตู:</label>
                                        </div>
                                        <div class="col-md-7" align="center">
                                            <label style="font-size: 16px;color: green;">
                                                <div ng-bind-html="htmlTrusted(Date_P_DO2)"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border: 1px solid black;"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">ปิดการแสดงข้อมูล
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-notice">
                <div class="modal-content" ng-show="ShowWorkFromHome">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel"
                            style="font-weight: bold;color: rgba(185,140,130,0.99);font-size: 20px">
                            บันทึกข้อมูลการปฏิบัติงาน Work From Home</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="material-icons">close</i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p style="text-align: center;color: teal;font-weight: bold">เวลาเริ่มปฏิบัติงาน</p>
                        <div class="row" align="center">
                            <div class="col-md-1 col-sm-0"></div>
                            <div class="col-md-10 col-sm-12">
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" style="text-align: center"
                                           id="WFH_Time_I" value="8:30" readonly="readonly">
                                    <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"
                            style="background-color: antiquewhite" ng-click="GetTimeCurrent_In()"><i
                                class="fa fa-clock-o"><span
                                    style="font-weight: bold;color: rgba(103,131,185,0.99);margin-left: 10px">เวลาปัจจุบัน</span></i>
                    </button>
                </span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-0"></div>
                        </div>
                        <p style="text-align: center;color: teal;font-weight: bold;margin-top: 20px">
                            เวลาสิ้นสุดการปฏิบัติงาน</p>
                        <div class="row" align="center">
                            <div class="col-md-1 col-sm-0"></div>
                            <div class="col-md-10 col-sm-12">
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" style="text-align: center"
                                           id="WFH_Time_O" value="16:30" readonly="readonly">
                                    <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"
                            style="background-color: antiquewhite" ng-click="GetTimeCurrent_Out()"><i
                                class="fa fa-clock-o"><span
                                    style="font-weight: bold;color: rgba(103,131,185,0.99);margin-left: 10px">เวลาปัจจุบัน</span></i>
                    </button>
                </span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-0"></div>
                        </div>
                    </div>
                    <hr/>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-info btn-round" style="margin: 10px" ng-click="SaveWFH()">
                            บันทึกข้อมูลการปฏิบัติงาน ณ {{DateSelect | DateThai}}
                        </button>
                        <button type="button" class="btn btn-danger btn-round" style="margin: 10px"
                                data-dismiss="modal">ปิดหน้าต่าง
                        </button>
                    </div>
                </div>
                <div class="modal-content" ng-show="!ShowWorkFromHome">
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel"
                            style="font-weight: bold;color: rgba(185,140,130,0.99);font-size: 20px">
                            บันทึกข้อมูลการปฏิบัติงาน Work From Home</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="material-icons">close</i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div align="center">
                            <h1 style="font-weight: bold;color: brown">ข้อมูลการปฏิบัติงานประจำวันที่ {{DateSelect | DateThai}}
                                ถูกบันทึกข้อมูลเรียบร้อยแล้ว</h1>
                        </div>
                    </div>
                    <hr/>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-danger btn-round" style="margin: 10px"
                                data-dismiss="modal">ปิดหน้าต่าง
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>
<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap datepicker thai-->
<script src="./tools/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.th.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>

<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/Js/angular-sanitize.min.js"></script>
<script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="./tools/notiTools/assets/js/core/popper.min.js"></script>
<script src="./tools/notiTools/assets/js/core/bootstrap-material-design.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script>
    $(document).ready(function () {
        $().ready(function () {

            $('#drop_Menu').click(function () {
                $('#drop_Menu').addClass("open1");
            });

            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });

        if ( <?php echo $this->model->CheckStatus?> ) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]);?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]);?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>

<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker
        $('#reservation2').daterangepicker({
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "ตกลง",
                "cancelLabel": "ยกเลิก",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "daysOfWeek": [
                    "อา",
                    "จ",
                    "อ",
                    "พ",
                    "พฤ",
                    "ศ",
                    "ส"
                ],
                "monthNames": [
                    'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ],
                "firstDay": 1
            }
        })

        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            // format: 'dd/mm/yyyy',
            language: 'th'
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false,
            showMeridian: false,
            minuteStep: 1
        })

    })
</script>
<script>
    var app = angular.module("myApp", ["datatables", "ui.bootstrap", 'ngAnimate']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, $sce, DTOptionsBuilder, $uibModal, $interval) {
        $scope.Depart = <?php echo $this->Department;?> ;
        $scope.DoorArr = <?php echo $this->DoorArray;?> ;
        $scope.DoorArrOutDepart = <?php echo $this->DoorArrOutDepart;?> ;
        $scope.Holiday = <?php echo $this->Holiday;?> ;
        $scope.EditTime = [];
        $scope.DataResult = [];
        $scope.uniqueStandards = [];
        $scope.uniqueStandards2 = [];
        $scope.unique_dataRange = [];
        $scope.uniqueStandards_R = [];
        $scope.uniqueStandardsRange = [];
        $scope.uniqueDataApprove = [];
        $scope.DataTimeDepShow = [];
        $scope.MixTimeArrRange = [];
        $scope.ObjDataItem = [];
        $scope.CheckName = '';
        $scope.timein = '';
        $scope.timein2 = '';
        $scope.timehn = '';
        $scope.timeho = '';
        $scope.timeout = '';
        $scope.timeout2 = '';
        $scope.DateTime1 = '';
        $scope.dtInstance = {};
        $scope.dtInstanceApprove = {};
        $scope.dateMax = '';
        $scope.Loading = true;
        $scope.EditDataPerson = false;
        $scope.ShowTable = true;
        $scope.DataApp = false;
        $scope.CheckComment = true;
        $scope.CheckGetComment = true;
        $scope.CheckEditTime = false;
        $scope.Check_TimeRange_I = false;
        $scope.Check_TimeRange_O = false;
        $scope.S_TimeIn = '00:00:00 - 08:30:00 (ปกติ)';
        $scope.S_TimeOut = '16:30:00 - 23:59:59 (ปกติ)';

        $scope.S_TimeIn_A = '00:00:00 - 08:30:00 (ปกติ)';
        $scope.S_TimeOut_A = '16:30:00 - 23:59:59 (ปกติ)';

        $scope.statusIn = '';
        $scope.statusOut = '';
        $scope.doorInName = 'ประตูทั้งหมด';

        $scope.ChangeColor = {
            'font-weight': 'bold',
            'color': 'red'
        }

        // ตัวแปรเก็บค่าเวลาเพื่อแสดงใน table header และเงื่อนไข การแสดงหัวตารางเมื่อเรียกดูข้อมูลสำเร็จ

        $scope.T_time_in = '';
        $scope.T_time_in2 = '';
        $scope.T_time_out = '';
        $scope.T_time_out2 = '';
        $scope.T_time_hn = '';
        $scope.T_time_ho = '';
        $scope.DataSuccess = false;
        $scope.ShowWorkFromHome = true;
        $scope.ShowTitleComment = true;

        $scope.CheckEditTime = function () {
            return new Promise((resolve, reject) => {
                data = {
                    'PERID': <?php echo $_SESSION['PERID']?> ,
                    'date': $scope.SetDateToDB($scope.DateSelect),
                };
                $http.post('./ApiService/CheckEditTime', data,{
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        if(response.data.Status) {
                            resolve(response.data.Status)
                        } else {
                            resolve(response.data.Status)
                        }
                    });
            })
        }

        $scope.modalShow = function () {
            $scope.Loading = true
            $scope.CalTimeLine2().then(res => {
                if (res) {
                    $scope.CheckEditTime().then(res => {
                        if(!res){
                            $scope.openModelEdit(res)
                        } else {
                            $scope.Loading = false
                            Swal({
                                type: 'error',
                                title: 'พบข้อผิดพลาดไม่สามารถแก้ไขข้อมูลเวลาได้ เนื่องจากวันที่ทำรายการ มีข้อมูลการแก้ไขเวลาอยู่แล้ว'
                            });
                        }
                    })
                } else {
                    $scope.Loading = false
                }
            })
        }

        $scope.openModelEdit = function (res) {
            var modalInstance = $uibModal.open({
                templateUrl: './Views/Page/modalEditTime.html',
                controller: 'ModalEditTime',
                animation: true,
                resolve: {
                    params: function () {
                        return {
                            PERID: $scope.uniqueStandards2[0].PERID,
                            DataTimeLine2: $scope.DataTimeLine2,
                            DoorArr: $scope.DoorArr,
                            DateSelect: $scope.DateSelect,
                            CheckEdit: res
                        };
                    }
                }
            });
            modalInstance.result.then(
                function (result) {
                    if(result) {
                        $scope.init()
                    }
                },
                function (result) {
                    console.log('called $modalInstance.dismiss()');
                    // console.log(result);
                    // alert(result);
                }
            );
            $scope.Loading = false
        };

        $scope.CalTimeLine2 = function () {
            return new Promise((resolve, reject) => {
                var DateBefore = new Date($scope.SetDateToDB($scope.DateSelect))
                var DateAfter = new Date($scope.SetDateToDB($scope.DateSelect))
                DateBefore.setDate(DateBefore.getDate() - 1)
                DateAfter.setDate(DateAfter.getDate() + 1)
                var dateStart = ''
                var monthStart = ''
                var dateEnd = ''
                var monthEnd = ''

                if (DateBefore.getDate() < 10) {
                    dateStart = '0' + DateBefore.getDate()
                } else {
                    dateStart = DateBefore.getDate()
                }

                if (DateAfter.getDate() < 10) {
                    dateEnd = '0' + DateAfter.getDate()
                } else {
                    dateEnd = DateAfter.getDate()
                }

                if ((DateBefore.getMonth() + 1 ) < 10) {
                    monthStart = '0' + (DateBefore.getMonth() + 1 )
                } else {
                    monthStart = (DateBefore.getMonth() + 1 )
                }

                if ((DateAfter.getMonth() + 1 ) < 10) {
                    monthEnd = '0' + (DateAfter.getMonth() + 1 )
                } else {
                    monthEnd = (DateAfter.getMonth() + 1 )
                }

                S_Date = DateBefore.getFullYear() + '-' + monthStart + '-' + dateStart
                E_Date = DateAfter.getFullYear() + '-' + monthEnd + '-' + dateEnd
                data = {
                    'DepCode': $scope.DepartSelect,
                    'DateStart': S_Date,
                    'DateEnd': E_Date,
                    'PERID': <?php echo $_SESSION['PERID']?>
                };
                $http.post('./ApiService/GETTIMELINEDATA_USER', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        if (response.data != null || response.data.length > 0) {
                            $scope.DataAllTimeLine = [];
                            $scope.DataTimeLine2 = [];

                            var RangeDate = '';
                            var RangeDateEnd = '';

                            var d1 = new Date(S_Date);
                            var d2 = new Date(E_Date);

                            while (d1 <= d2) {

                                if (d1.getDate() <= 9 && (d1.getMonth() + 1) <= 9) {
                                    RangeDate = "0" + (d1.getMonth() + 1) + '/' + "0" + d1.getDate() + '/' + d1.getFullYear();
                                    RangeDateEnd = "0" + (d2.getMonth() + 1) + '/' + "0" + d2.getDate() + '/' + d2.getFullYear();
                                } else if (d1.getDate() <= 9 && (d1.getMonth() + 1) > 9) {
                                    RangeDate = (d1.getMonth() + 1) + '/0' + d1.getDate() + '/' + d1.getFullYear();
                                    RangeDateEnd = (d2.getMonth() + 1) + '/0' + d2.getDate() + '/' + d2.getFullYear();
                                } else if (d1.getDate() > 9 && (d1.getMonth() + 1) <= 9) {
                                    RangeDate = "0" + (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
                                    RangeDateEnd = "0" + (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
                                } else {
                                    RangeDate = (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
                                    RangeDateEnd = (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
                                }

                                var Day = RangeDate.split('/')[1];
                                var Month = RangeDate.split('/')[0];
                                var Year = RangeDate.split('/')[2];
                                var RangeDateToDB = Year + "-" + Month + "-" + Day;
                                var options = {
                                    weekday: 'long'
                                };
                                $scope.DataTimeLine2.push({
                                    'DateShow': RangeDate,
                                    'DateName': d1.toLocaleDateString('en-US', options),
                                    'DateDB': RangeDateToDB,
                                    'DateOff': '',
                                    'DateOffName': '',
                                    'TimeLine': []
                                });
                                angular.forEach($scope.DataTimeLine2, function (DataTimeItem) {
                                    angular.forEach($scope.Holiday, function (hoItem) {
                                        if (hoItem.Date_stop == DataTimeItem.DateDB) {
                                            DataTimeItem.DateOff = hoItem.Date_stop,
                                                DataTimeItem.DateOffName = hoItem.Date_name
                                        } else {
                                        }
                                    });
                                });
                                d1.setDate(d1.getDate() + 1);
                            }
                        }
                        angular.forEach($scope.DataTimeLine2, function (DataTimeItem) {
                            angular.forEach(response.data, function (timeLineItem) {
                                if (DataTimeItem.DateDB == timeLineItem.event_time.split(' ')[0]) {
                                    DataTimeItem.TimeLine.push({
                                        ...timeLineItem,
                                        'CheckIn': false,
                                        'CheckOut': false,
                                    });
                                }
                            });
                        });
                        resolve(true)
                    });
            })
        }

        $scope.ChooseTimeRangeIn = function () {
            if (!$scope.Check_TimeRange_I) {
                $scope.Check_TimeRange_I = true;
            } else {
                $scope.Check_TimeRange_I = false;
            }
        }

        $scope.ChooseTimeRangeOut = function () {
            if (!$scope.Check_TimeRange_O) {
                $scope.Check_TimeRange_O = true;
            } else {
                $scope.Check_TimeRange_O = false;
            }
        }

        $scope.SettingTime = function () {
            if (!$scope.CheckEditTime) {
                $scope.CheckEditTime = true;
            } else {
                $scope.CheckEditTime = false;
            }
        };

        $scope.GetCurrentTime = function () {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return today = yyyy + '-' + mm + '-' + dd;
        }

        $scope.open = function () {
            var modalInstance = $uibModal.open({
                templateUrl: './Views/Page/modalDate.html',
                controller: 'ModalInstanceCtrl',
                animation: true,
                resolve: {
                    params: function () {
                        return {
                            item_D: $scope.ObjDataItem,
                            Date: $scope.DateSelect,
                            ObjDataPerson: $scope.uniqueStandards2,
                            ObjDoorArr: $scope.DoorArr
                        };
                    }
                }
            });
            modalInstance.result.then(
                function (result) {
                    console.log('called $modalInstance.close()');
                    // alert(result);
                },
                function (result) {
                    console.log('called $modalInstance.dismiss()');
                    // alert(result);
                }
            );
        };

        $scope.open2 = function () {
            var modalInstance = $uibModal.open({
                templateUrl: './Views/Page/modalDate2.html',
                controller: 'ModalInstanceCtrl',
                animation: true,
                resolve: {
                    params: function () {
                        return {
                            item_D: $scope.ObjDataItem,
                            Date: $scope.DateSelect,
                            ObjDataPerson: $scope.uniqueDataApprove,
                            ObjDoorArr: $scope.DoorArr,
                            CondiStatus: <?php echo $this->model->CheckMenu?>
                        };
                    }
                }
            });
            modalInstance.result.then(
                function (result) {
                    // console.log('called $modalInstance.close()');
                    // console.log(result);
                    // angular.forEach(result, function (itemArr) {
                    //     angular.forEach($scope.uniqueDataApprove, function (D_Approve) {
                    //         if (D_Approve.PERID == itemArr.PERID) {
                    //             D_Approve.A_Time_in2 = itemArr.TIME_IN2;
                    //             D_Approve.A_Time_ho = itemArr.TIME_HO;
                    //             D_Approve.A_Time_hn = itemArr.TIME_HN;
                    //             D_Approve.A_Time_out2 = itemArr.TIME_OUT2;
                    //             D_Approve.A_Door_in2 = itemArr.DOOR_IN2;
                    //             D_Approve.A_Door_ho = itemArr.DOOR_HO;
                    //             D_Approve.A_Door_hn = itemArr.DOOR_HN;
                    //             D_Approve.A_Door_out2 = itemArr.DOOR_OUT2;
                    //         }
                    //     });
                    // });
                    $timeout(function () {
                        $scope.getApproveData();
                    }, 1500);
                    // $scope.getApproveData();
                },
                function (result) {
                    console.log('called $modalInstance.dismiss()');
                    // console.log(result);
                    // alert(result);
                }
            );
        };

        // $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250]);

        $scope.init = function () {
            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250])
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            $scope.dateMax = today;

            var today = mm + '/' + dd + '/' + yyyy;

            var p_day = new Date();
            p_day = new Date(p_day.getTime() - ((24 * 60 * 60 * 1000) * 4));
            var dd = p_day.getDate();
            var mm = p_day.getMonth() + 1; //January is 0!

            var yyyy = p_day.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            var p_day = mm + '/' + dd + '/' + yyyy;

            document.getElementById("datepicker").value = today;
            $scope.DepartSelect = "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]);?> ;
            $scope.update();
            document.getElementById("reservation").value = p_day + " - " + today;
            // $scope.updateRange();
            // $scope.Loading = false;
            $scope.checkComment()
            $scope.getEditTime().then(res => {
                $scope.EditTime = res
            })

            var colorCordi = true
            $interval(function () {
                if (colorCordi) {
                    $scope.ChangeColor = {
                        'font-weight': 'bold',
                        'color': 'red'
                    }
                    colorCordi = false
                } else {
                    $scope.ChangeColor = {
                        'font-weight': 'bold',
                        'color': 'white'
                    }
                    colorCordi = true
                }


            }, 500)
        }

        $timeout($scope.init)

        $scope.getEditTime = function() {
            return new Promise((resolve, reject) => {
                data = {
                    'PERID': <?php echo $_SESSION['PERID']?> ,
                    'date': $scope.SetDateToDB($scope.DateSelect),
                };
                $http.post('./ApiService/GetEditTime', data,{
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        if(response.data) {
                            resolve(response.data)
                        } else {
                            reject(true)
                        }
                    });
            })
        }

        $scope.checkComment = function () {
            data = {
                'PerId': <?php echo $_SESSION['PERID'] ?>,
                'DateConrdi': $scope.Date,
            };

            $http.post('./ApiService/CheckComment', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data.Status) {
                        $scope.ShowWorkFromHome = false
                    } else {
                        $scope.ShowWorkFromHome = true
                    }
                })
        }


        // ฟังก์ชัน บันทึกผลการปฏิบัติงานที่บ้าน
        $scope.SaveWFH = async function () {
            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "กรุณาตรวจสอบข้อมูลให้ครบถ้วนก่อนยืนยันข้อมูล",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {
                    data = {
                        'DepCode': $scope.DepartSelect,
                        'PerId': <?php echo $_SESSION['PERID'] ?>,
                        'DateConrdi': $scope.Date,
                        'Comment': 'Work From Home!!',
                        'Time_I': document.getElementById('WFH_Time_I').value,
                        'Time_O': document.getElementById('WFH_Time_O').value,
                        'PerId_Create': <?php echo $_SESSION['PERID']?>
                    };

                    $http.post('./ApiService/SaveComment', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            var result = response.data;
                            if (result.Status) {
                                Swal({
                                    type: 'success',
                                    title: result.Message
                                });
                                document.getElementById('txtCommect').value = '';
                                $scope.PrsonSelect = 0;
                                $scope.checkComment()
                            } else {
                                Swal({
                                    type: 'error',
                                    title: result.Message
                                });
                            }
                        });
                }
            });
        }

        // ฟังก์ชัน เรียกข้อมูลเวลาปัจจุบัน
        $scope.GetTimeCurrent_In = async function () {
            var DateNow = new Date()
            if (DateNow.getMinutes() < 10) {
                document.getElementById('WFH_Time_I').value = DateNow.getHours() + ':0' + DateNow.getMinutes()
            } else {
                document.getElementById('WFH_Time_I').value = DateNow.getHours() + ':' + DateNow.getMinutes()
            }

        }

        $scope.GetTimeCurrent_Out = async function () {
            var DateNow = new Date()
            if (DateNow.getMinutes() < 10) {
                document.getElementById('WFH_Time_O').value = DateNow.getHours() + ':0' + DateNow.getMinutes()
            } else {
                document.getElementById('WFH_Time_O').value = DateNow.getHours() + ':' + DateNow.getMinutes()
            }
        }


        // ฟังก์ชั่น เลื่อนวันที่ถัดไปหรือไปข้างหน้า 4 วันเพื่อแสดงให้ผู้ใช้งานเห็น
        $scope.nextDay = function (DataResult) {

            var p_day = new Date((document.getElementById("reservation").value).split(" - ")[0]);
            var today = new Date((document.getElementById("reservation").value).split(" - ")[1]);

            var dateLast = today.getDate() + " " + (today.getMonth() + 1) + " " + today.getFullYear();
            var dateSele = $scope.dateMax.getDate() + " " + ($scope.dateMax.getMonth() + 1) + " " + $scope.dateMax.getFullYear();
            if (dateLast == dateSele) {
                Swal({
                    type: 'error',
                    title: 'พบข้อผิดพลาด',
                    text: 'ข้อมูลแสดงผลถึงวันที่ล่าสุดแล้ว'
                })
            } else {

                var dd = today.setDate(today.getDate() + 5);
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!

                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }

                var today = mm + '/' + dd + '/' + yyyy;

                var dd = p_day.setDate(p_day.getDate() + 5);
                var dd = p_day.getDate();
                var mm = p_day.getMonth() + 1; //January is 0!

                var yyyy = p_day.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }

                var p_day = mm + '/' + dd + '/' + yyyy;

                document.getElementById("reservation").value = p_day + " - " + today;
                $scope.updateRange();

                $scope.getApproveData();
            }
        };
        /////////////////////////////////////////////////////////////////////

        // ฟังก์ชั่น เลือนวันที่ไปเป็นก่อนหน้านี้ โดยจะเลื่อนครั้งล่ะ 4 วัน เพื่อแสดงให้ผู้ใช้งานเห็น
        $scope.previousDay = function (DataResult) {

            var p_day = new Date((document.getElementById("reservation").value).split(" - ")[0]);
            var today = new Date((document.getElementById("reservation").value).split(" - ")[1]);

            var dd = today.setDate(today.getDate() - 5);
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            var today = mm + '/' + dd + '/' + yyyy;

            var dd = p_day.setDate(p_day.getDate() - 5);
            var dd = p_day.getDate();
            var mm = p_day.getMonth() + 1; //January is 0!

            var yyyy = p_day.getFullYear();
            if (dd < 10) {
                // if(){}
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            var p_day = mm + '/' + dd + '/' + yyyy;

            document.getElementById("reservation").value = p_day + " - " + today;
            $scope.updateRange();
        };
        /////////////////////////////////////////////////////////////////////

        // ฟังก์ชั่นการยืนยันข้อมูล หรือ Approve ข้อมูลจากตารางแสดงผล D_transection เพื่อสร้างตาราง Approve ใหม่ สำหรับออกรายงานหรือ
        // ต้องการปรับเปลี่ยนแก้ไข
        $scope.ApproveData = function () {
            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "กรุณาตรวจสอบข้อมูลให้ครบถ้วนก่อนยืนยันข้อมูล",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {

                    var CheckStopDate = new Date($scope.Date);
                    var CheckHoliday = false;
                    var options = {
                        weekday: 'long'
                    };
                    if (CheckStopDate.toLocaleDateString('en-US', options) == "Saturday" || CheckStopDate.toLocaleDateString('en-US', options) == "Sunday") {
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถยืนยันข้อมูลได้สำเร็จ...',
                            text: 'เนื่องจากวันที่ดังกล่าวเป็นวันหยุดการปฏิบัติงาน'
                        })
                    } else {
                        angular.forEach($scope.Holiday, function (hoItem) {
                            if (hoItem.Date_stop == $scope.Date) {
                                CheckHoliday = true
                            } else {
                            }
                        });
                        if (CheckHoliday) {
                            Swal({
                                type: 'error',
                                title: 'ไม่สามารถยืนยันข้อมูลได้สำเร็จ...',
                                text: 'เนื่องจากวันที่ดังกล่าวเป็นวันหยุดการปฏิบัติงาน'
                            })
                        } else {
                            data = {
                                'OBJDATA': $scope.uniqueStandards2,
                                'DATENOW': $scope.Date,
                                'C_PERID': <?php echo $_SESSION['PERID']?> ,
                                'U_PERID': <?php echo $_SESSION['PERID']?>
                            };

                            $http.post('./ApiService/ApproveData', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                                .then(function successCallback(response) {
                                    if (response.data) {
                                        $scope.DataApp = true;
                                        Swal(
                                            'ยืนยันข้อมูลการปฏิบัติงาน',
                                            'สำเร็จ',
                                            'success'
                                        )
                                    } else {
                                        Swal(
                                            'ยืนยันข้อมูลการปฏิบัติงาน',
                                            'ไม่สำเร็จ',
                                            'error'
                                        )
                                    }
                                });
                        }
                    }
                }
            });
        }
        /////////////////////////////////////////////////////////////////////

        // ฟังก์ชั่นค้นหาชื่อประตู โดยส่ง door_id เข้ามา จะรีเทิร์น ชื่อของประตูรพ. ออกไป
        $scope.FindDoor = function (door_idData) {
            var doorname = '';
            angular.forEach($scope.DoorArr, function (Door_A) {
                if (Door_A.door_id == door_idData) {
                    doorname = "" + Door_A.door_name;
                }
            });
            return doorname;
        }
        /////////////////////////////////////////////////////////////////////

        // ฟังก์ชั่น โชว์ Modal รายล่ะเอียดของแต่ล่ะคน ในตาราง html
        $scope.showDetails = function (item) {
            // $scope.ObjDataItem = item;
            // $scope.open();
            $scope.Modalname = item.NAME + " " + item.SURNAME.split('<br>')[0];
            angular.forEach($scope.uniqueStandards2, function (ArrayDataPerson) {
                if (ArrayDataPerson.PERID == item.PERID) {
                    if (ArrayDataPerson.TIME_IN != null && ArrayDataPerson.TIME_IN != '') {
                        $scope.Date_P_TIN = ArrayDataPerson.TIME_IN;
                    } else {
                        $scope.Date_P_TIN = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.TIME_IN2 != null && ArrayDataPerson.TIME_IN2 != '') {
                        $scope.Date_P_TIN2 = ArrayDataPerson.TIME_IN2;
                    } else {
                        $scope.Date_P_TIN2 = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.DOOR_IN != null && ArrayDataPerson.DOOR_IN != '') {
                        $scope.Date_P_DIN = $scope.FindDoor(ArrayDataPerson.DOOR_IN);
                    } else {
                        $scope.Date_P_DIN = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.DOOR_IN2 != null && ArrayDataPerson.DOOR_IN2 != '') {
                        $scope.Date_P_DIN2 = $scope.FindDoor(ArrayDataPerson.DOOR_IN2);
                    } else {
                        $scope.Date_P_DIN2 = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.TIME_HO != null && ArrayDataPerson.TIME_HO != '') {
                        $scope.Date_P_THO = ArrayDataPerson.TIME_HO;
                    } else {
                        $scope.Date_P_THO = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.TIME_HN != null && ArrayDataPerson.TIME_HN != '') {
                        $scope.Date_P_THN = ArrayDataPerson.TIME_HN;
                    } else {
                        $scope.Date_P_THN = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.DOOR_HO != null && ArrayDataPerson.DOOR_HO != '') {
                        $scope.Date_P_DHO = $scope.FindDoor(ArrayDataPerson.DOOR_HO);
                    } else {
                        $scope.Date_P_DHO = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.DOOR_HN != null && ArrayDataPerson.DOOR_HN != '') {
                        $scope.Date_P_DHN = $scope.FindDoor(ArrayDataPerson.DOOR_HN);
                    } else {
                        $scope.Date_P_DHN = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.TIME_OUT != null && ArrayDataPerson.TIME_OUT != '') {
                        $scope.Date_P_TO = ArrayDataPerson.TIME_OUT;
                    } else {
                        $scope.Date_P_TO = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.TIME_OUT2 != null && ArrayDataPerson.TIME_OUT2 != '') {
                        $scope.Date_P_TO2 = ArrayDataPerson.TIME_OUT2;
                    } else {
                        $scope.Date_P_TO2 = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.DOOR_OUT != null && ArrayDataPerson.DOOR_OUT != '') {
                        $scope.Date_P_DO = $scope.FindDoor(ArrayDataPerson.DOOR_OUT);
                    } else {
                        $scope.Date_P_DO = "<span style=\"color:#E040FB\"> - </span>";
                    }
                    if (ArrayDataPerson.DOOR_OUT2 != null && ArrayDataPerson.DOOR_OUT2 != '') {
                        $scope.Date_P_DO2 = $scope.FindDoor(ArrayDataPerson.DOOR_OUT2);
                    } else {
                        $scope.Date_P_DO2 = "<span style=\"color:#E040FB\"> - </span>";
                    }
                }
            });
            data = {
                'PERID': item.PERID
            };

            $http.post('./ApiService/GetCardNo', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.CardNo = response.data;
                });
        }

        $scope.showDetails2 = function (item) {
            $scope.ObjDataItem = item;
            $scope.open2();
        }
        /////////////////////////////////////////////////////////////////////

        // ยังไม่ใช้งาน
        $scope.EditData = function (CheckEdit) {
            if (!$scope.EditDataPerson) {
                $scope.EditDataPerson = CheckEdit;
            } else {
                $scope.EditDataPerson = CheckEdit;
            }
            document.getElementById("E_TimeIN1").value = $scope.ObjDataItem.TIME_IN.split(":")[0] + ":" + $scope.ObjDataItem.TIME_IN.split(":")[1];
        }
        /////////////////////////////////////////////////////////////////////

        $scope.CalTimeLine = function () {

            // var p_day = new Date((document.getElementById("reservation").value).split(" - ")[0]);
            // var today = new Date((document.getElementById("reservation").value).split(" - ")[1]);
            var S_Date = (document.getElementById("reservation2").value).split(" - ")[0];
            var E_Date = (document.getElementById("reservation2").value).split(" - ")[1];
            S_Date = S_Date.split("/")[2] + '-' + S_Date.split("/")[1] + '-' + S_Date.split("/")[0]
            E_Date = E_Date.split("/")[2] + '-' + E_Date.split("/")[1] + '-' + E_Date.split("/")[0]
            data = {
                'DepCode': $scope.DepartSelect,
                'DateStart': S_Date,
                'DateEnd': E_Date,
                'PERID': <?php echo $_SESSION['PERID']?>
            };
            $http.post('./ApiService/GETTIMELINEDATA_USER', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data != null || response.data.length > 0) {
                        $scope.DataAllTimeLine = [];
                        $scope.DataTimeLine = [];

                        var RangeDate = '';
                        var RangeDateEnd = '';

                        var d1 = new Date(S_Date);
                        var d2 = new Date(E_Date);

                        while (d1 <= d2) {

                            if (d1.getDate() <= 9 && (d1.getMonth() + 1) <= 9) {
                                RangeDate = "0" + (d1.getMonth() + 1) + '/' + "0" + d1.getDate() + '/' + d1.getFullYear();
                                RangeDateEnd = "0" + (d2.getMonth() + 1) + '/' + "0" + d2.getDate() + '/' + d2.getFullYear();
                            } else if (d1.getDate() <= 9 && (d1.getMonth() + 1) > 9) {
                                RangeDate = (d1.getMonth() + 1) + '/0' + d1.getDate() + '/' + d1.getFullYear();
                                RangeDateEnd = (d2.getMonth() + 1) + '/0' + d2.getDate() + '/' + d2.getFullYear();
                            } else if (d1.getDate() > 9 && (d1.getMonth() + 1) <= 9) {
                                RangeDate = "0" + (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
                                RangeDateEnd = "0" + (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
                            } else {
                                RangeDate = (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
                                RangeDateEnd = (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
                            }

                            var Day = RangeDate.split('/')[1];
                            var Month = RangeDate.split('/')[0];
                            var Year = RangeDate.split('/')[2];
                            var RangeDateToDB = Year + "-" + Month + "-" + Day;
                            var options = {
                                weekday: 'long'
                            };
                            $scope.DataTimeLine.push({
                                'DateShow': RangeDate,
                                'DateName': d1.toLocaleDateString('en-US', options),
                                'DateDB': RangeDateToDB,
                                'DateOff': '',
                                'DateOffName': '',
                                'TimeLine': []
                            });
                            angular.forEach($scope.DataTimeLine, function (DataTimeItem) {
                                angular.forEach($scope.Holiday, function (hoItem) {
                                    if (hoItem.Date_stop == DataTimeItem.DateDB) {
                                        DataTimeItem.DateOff = hoItem.Date_stop,
                                            DataTimeItem.DateOffName = hoItem.Date_name
                                    } else {
                                    }
                                });
                            });
                            d1.setDate(d1.getDate() + 1);
                        }
                    }
                    angular.forEach($scope.DataTimeLine, function (DataTimeItem) {
                        angular.forEach(response.data, function (timeLineItem) {
                            if (DataTimeItem.DateDB == timeLineItem.event_time.split(' ')[0]) {
                                DataTimeItem.TimeLine.push(timeLineItem);
                            }
                        });
                    });
                });
        }

        $scope.getBeforeApprove = function () {
            $scope.ShowTable = true;
        }

        $scope.getApproveData = function () {

            $scope.ShowTable = false;

            data = {
                'DepCode': $scope.DepartSelect,
                'DateConrdi': $scope.Date,
                'PERID': <?php echo $_SESSION['PERID']?>
                // 'DateConrdi': $scope.Date
            };

            $http.post('./ApiService/GetDataApprove_USER', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.uniqueDataApprove = [];
                    $scope.uniqueDataApprove = response.data;
                    if ($scope.uniqueDataApprove.length > 0) {
                        data = {
                            'DepCode': [{'Dep_Code': $scope.DepartSelect}],
                            'DateConrdi': $scope.Date,
                            'PERID': <?php echo $_SESSION['PERID']?>
                        };
                        $http.post('./ApiService/GetPersonDate_USER', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.PersonDepartTime = response.data;
                                var keepGoing = true;

                                angular.forEach($scope.PersonDepartTime, function (arr1, idx2) {
                                    keepGoing = true;
                                    if (arr1.P_Time_TimeN != null) {
                                        $scope.Ap_timein2 = '';
                                        $scope.Ap_timehn = '';
                                        $scope.Ap_timeho = '';
                                        $scope.Ap_timeout2 = '';
                                        $scope.doorInName = '';
                                        //คำนวณเวลาเข้าประตูบริเวณ รพ ทั้งหมด
                                        angular.forEach($scope.uniqueDataApprove, function (arr2, idx_Item) {
                                            if (arr1.PERID == arr2.PERID) {
                                                angular.forEach(arr1.DoorData, function (DoorItem, Door_Idx) {
                                                    if (Door_Idx == arr1.DoorData.length - 1) {
                                                        $scope.doorInName += $scope.FindDoor(DoorItem.door_id);
                                                    } else {
                                                        $scope.doorInName += $scope.FindDoor(DoorItem.door_id) + '  ,  ';
                                                    }
                                                })

                                                $scope.S_TimeOut_A = arr1.P_Time_TimeO + ' - 23:59:59 (ปกติ)';
                                                $scope.S_TimeIn_A = '00:00:00 - ' + arr1.P_Time_TimeN + ' (ปกติ)';

                                                if (arr2.A_Time_in2.split(' ')[0] != '' && arr2.A_Time_in2.split(' ')[0] != null) {
                                                    if (arr2.A_Time_in2.split(' ')[0] <= $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]) {
                                                        if ($scope.Ap_timein2 == null || $scope.Ap_timein2 == '') {
                                                            arr2.A_Time_in2 = $scope.Ap_timein2 = arr2.A_Time_in2;
                                                            return false;
                                                        }
                                                    }
                                                        //  else if (arr2.A_Time_in2.split(' ')[0] > $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1] &&
                                                        //     arr2.A_Time_in2.split(' ')[0] < $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]) {
                                                        //     var DateHo = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[0];
                                                        //     var TimeHo = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1];
                                                        //     var Hour = TimeHo.split(':')[0];
                                                        //     var Minute = TimeHo.split(':')[1];
                                                        //     var Sec = TimeHo.split(':')[2];
                                                        //     Hour = parseInt(Hour) - 1;
                                                        //     var newDateTime = Hour + ":" + Minute + ":" + Sec;
                                                        //     if (arr2.A_Time_in2.split(' ')[0] <= newDateTime) {
                                                        //         console.log('test2');
                                                        //         if ($scope.Ap_timein2 == null || $scope.Ap_timein2 == '') {
                                                        //             arr2.A_Time_in2 = $scope.Ap_timein2 = arr2.A_Time_in2 + arr1.P_Time_TimeN.split(':')[0] + ':' + arr1.P_Time_TimeN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[0] +
                                                        //                 ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                        //             return false;
                                                        //         }
                                                        //     }
                                                    // }
                                                    else {
                                                        arr2.A_Time_in2 = $scope.Ap_timein2 = arr2.A_Time_in2;
                                                    }
                                                }
                                            }
                                        });
                                        //////////////////////////////////////////

                                        //คำนวณเวลาออก (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                                        angular.forEach($scope.uniqueDataApprove, function (arr2, idx_Item) {
                                            if (arr1.PERID == arr2.PERID) {
                                                if (arr2.A_Time_ho.split(' ')[0] != '' && arr2.A_Time_ho.split(' ')[0] != null) {
                                                    if (arr2.A_Time_ho.split(' ')[0] >= $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1] && arr2.A_Time_ho.split(' ')[0] < arr1.P_Time_TimeHN) {
                                                        if ($scope.Ap_timeho == null || $scope.Ap_timeho == '') {
                                                            arr2.A_Time_ho = $scope.Ap_timeho = arr2.A_Time_ho + " <br /> <span style=\"color:#00E5FF\">" +
                                                                ($scope.SetTimeBeforeHour($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]).split(':')[0] +
                                                                ':' + ($scope.SetTimeBeforeHour($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]).split(':')[1] +
                                                                ' น. - ' + arr1.P_Time_TimeHN.split(':')[0] + ':' + arr1.P_Time_TimeHN.split(':')[1] + " น.</span>";
                                                        }
                                                    }
                                                        //  else if (arr2.A_Time_ho.split(' ')[0] < $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]) {
                                                        //     var DateHo = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[0];
                                                        //     var TimeHo = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1];
                                                        //     var Hour = TimeHo.split(':')[0];
                                                        //     var Minute = TimeHo.split(':')[1];
                                                        //     var Sec = TimeHo.split(':')[2];
                                                        //     Hour = parseInt(Hour) - 1;
                                                        //     var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                                        //     if (arr2.A_Time_ho.split(' ')[0] > newDateTime && arr2.A_Time_ho.split(' ')[0] < $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]) {
                                                        //         if ($scope.Ap_timeho == null || $scope.Ap_timeho == '') {
                                                        //             arr2.A_Time_ho = $scope.Ap_timeho = arr2.A_Time_ho + ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]).split(':')[0] +
                                                        //                 ':' + ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]).split(':')[1] +
                                                        //                 ' น. - ' + arr1.P_Time_TimeHN.split(':')[0] + ':' + arr1.P_Time_TimeHN.split(':')[1] + " น.</span>";
                                                        //         }
                                                        //     }else{

                                                        //     }
                                                    // }
                                                    else {
                                                        if ($scope.Ap_timeho == null || $scope.Ap_timeho == '') {
                                                            arr2.A_Time_ho = $scope.Ap_timeho = arr2.A_Time_ho + ($scope.SetTimeBeforeHour($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]).split(':')[0] +
                                                                ':' + ($scope.SetTimeBeforeHour($scope.Date, arr1.P_Time_TimeHO).split(' ')[1]).split(':')[1] +
                                                                ' น. - ' + arr1.P_Time_TimeHN.split(':')[0] + ':' + arr1.P_Time_TimeHN.split(':')[1] + " น.</span>";
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                        //////////////////////////////////////////

                                        //คำนวณเวลาเข้า (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                                        angular.forEach($scope.uniqueDataApprove, function (arr2, idx_Item) {
                                            if (arr1.PERID == arr2.PERID) {
                                                if (arr2.A_Time_hn.split(' ')[0] != '' && arr2.A_Time_hn.split(' ')[0] != null) {
                                                    if (arr2.A_Time_hn.split(' ')[0] >= arr1.P_Time_TimeHN &&
                                                        arr2.A_Time_hn.split(' ')[0] < $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[1]) {
                                                        if ($scope.Ap_timehn == null || $scope.Ap_timehn == '') {
                                                            arr2.A_Time_hn = $scope.Ap_timehn = arr2.A_Time_hn + " <br /> <span style=\"color:#00E5FF\">" +
                                                                arr1.P_Time_TimeHN.split(':')[0] + ':' + arr1.P_Time_TimeHN.split(':')[1] + ' น. - ' + ($scope.SetTimeLateHours($scope.Date, arr1.P_Time_TimeHN).split(' ')[1]).split(':')[0] +
                                                                ':' + ($scope.SetTimeLateHours($scope.Date, arr1.P_Time_TimeHN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                        }
                                                    }
                                                        //  else if (arr2.A_Time_hn.split(' ')[0] < arr1.P_Time_TimeHN) {
                                                        //     var DateHn = $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[0];
                                                        //     var TimeHn = $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[1];
                                                        //     var Hour = TimeHn.split(':')[0];
                                                        //     var Minute = TimeHn.split(':')[1];
                                                        //     var Sec = TimeHn.split(':')[2];
                                                        //     Hour = parseInt(Hour) + 1;
                                                        //     var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                                        //     if (arr2.A_Time_hn.split(' ')[0] > $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[1] &&
                                                        //         arr2.A_Time_hn.split(' ')[0] <= newDateTime) {
                                                        //         if ($scope.Ap_timehn == null || $scope.Ap_timehn == '') {
                                                        //             arr2.A_Time_hn = $scope.Ap_timehn = arr2.A_Time_hn + arr1.P_Time_TimeHN.split(':')[0] + ':' + arr1.P_Time_TimeHN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[1]).split(':')[0] +
                                                        //                 ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[1]).split(':')[1] + " น.</span>";

                                                        //         }
                                                        //     }
                                                    // }
                                                    else {
                                                        if ($scope.Ap_timehn == null || $scope.Ap_timehn == '') {
                                                            arr2.A_Time_hn = $scope.Ap_timehn = arr2.A_Time_hn + arr1.P_Time_TimeHN.split(':')[0] + ':' + arr1.P_Time_TimeHN.split(':')[1] + ' น. - ' + ($scope.SetTimeLateHours($scope.Date, arr1.P_Time_TimeHN).split(' ')[1]).split(':')[0] +
                                                                ':' + ($scope.SetTimeLateHours($scope.Date, arr1.P_Time_TimeHN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                        //////////////////////////////////////////

                                        //คำนวณเวลาออกประตูบริเวณ รพ ทั้งหมด
                                        angular.forEach($scope.uniqueDataApprove, function (arr2, idx_Item) {
                                            if (arr1.PERID == arr2.PERID) {
                                                if (arr2.A_Time_out2.split(' ')[0] != '' && arr2.A_Time_out2.split(' ')[0] != null) {
                                                    if (arr2.A_Time_out2.split(' ')[0] >= $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1] &&
                                                        arr2.A_Time_out2.split(' ')[0] <= "24:59:59"
                                                    ) {
                                                        if ($scope.Ap_timeout2 == null || $scope.Ap_timeout2 == '') {
                                                            arr2.A_Time_out2 = $scope.Ap_timeout2 = arr2.A_Time_out2;
                                                        }
                                                    }
                                                        //  else if (arr2.A_Time_out2.split(' ')[0] < $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1] && arr2.A_Time_out2.split(' ')[0] > $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[1] &&
                                                        //     ($scope.timehn == '' || $scope.timehn == null)) {
                                                        //     var DateO = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[0];
                                                        //     var TimeO = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1];
                                                        //     var Hour = TimeO.split(':')[0];
                                                        //     var Minute = TimeO.split(':')[1];
                                                        //     var Sec = TimeO.split(':')[2];
                                                        //     Hour = parseInt(Hour) - 1;
                                                        //     var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                                        //     if (arr2.A_Time_out2.split(' ')[0] >= newDateTime) {
                                                        //         if ($scope.Ap_timeout2 == null || $scope.Ap_timeout2 == '') {
                                                        //             arr2.A_Time_out2 = $scope.Ap_timeout2 = arr2.A_Time_out2 + ($scope.SetTimeBefore(_dateRange, arr1.P_Time_TimeO).split(' ')[1]).split(':')[0] +
                                                        //                 ':' + ($scope.SetTimeBefore(_dateRange, arr1.P_Time_TimeO).split(' ')[1]).split(':')[1] +
                                                        //                 ' น. - ' + arr1.P_Time_TimeO.split(':')[0] + ':' + arr1.P_Time_TimeO.split(':')[1] + " น.</span>";
                                                        //         }
                                                        //     } else {

                                                        //     }
                                                    // }
                                                    else {
                                                        if ($scope.Ap_timeout2 == null || $scope.Ap_timeout2 == '') {
                                                            arr2.A_Time_out2 = $scope.Ap_timeout2 = arr2.A_Time_out2;
                                                        }
                                                    }
                                                }
                                            }
                                        });

                                        //////////////////////////////////////////
                                    } else {
                                    }
                                });
                            });


                        data = {
                            'DepCode': $scope.DepartSelect,
                            'DateConrdi': $scope.Date,
                            'PERID': <?php echo $_SESSION['PERID']?>
                        };

                        $http.post('./ApiService/GetLeavePerson_USER', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.LeaveArrData_A = response.data;
                                angular.forEach($scope.LeaveArrData_A, function (leaveItem) {
                                    angular.forEach($scope.uniqueDataApprove, function (dataItem) {
                                        if (leaveItem.PERID == dataItem.PERID) {
                                            if (leaveItem.T_Leave_Date_Start == $scope.Date) {
                                                if (leaveItem.T_Day_Type_Start == "เต็มวัน") {
                                                    dataItem.A_Time_in2 = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                    dataItem.A_Time_ho = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                    dataItem.A_Time_hn = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                    dataItem.A_Time_out2 = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                } else if (leaveItem.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                    dataItem.A_Time_in2 = leaveItem.Leave_Detail + " ครึ่งวัน (เช้า)"
                                                    dataItem.A_Time_ho = leaveItem.Leave_Detail + " ครึ่งวัน (เช้า)"
                                                } else {
                                                    dataItem.A_Time_hn = leaveItem.Leave_Detail + " ครึ่งวัน (บ่าย)"
                                                    dataItem.A_Time_out2 = leaveItem.Leave_Detail + " ครึ่งวัน (บ่าย)"
                                                }
                                            } else if (leaveItem.T_Leave_Date_End == $scope.Date) {
                                                if (leaveItem.T_Day_Type_End == "เต็มวัน") {
                                                    dataItem.A_Time_in2 = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                    dataItem.A_Time_ho = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                    dataItem.A_Time_hn = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                    dataItem.A_Time_out2 = leaveItem.Leave_Detail + " (เต็มวัน)"
                                                } else if (leaveItem.T_Day_Type_End == "ครึ่งวัน (เช้า)") {
                                                    dataItem.A_Time_in2 = leaveItem.Leave_Detail + " ครึ่งวัน (เช้า)"
                                                    dataItem.A_Time_ho = leaveItem.Leave_Detail + " ครึ่งวัน (เช้า)"
                                                } else {
                                                    dataItem.A_Time_hn = leaveItem.Leave_Detail + " ครึ่งวัน (บ่าย)"
                                                    dataItem.A_Time_out2 = leaveItem.Leave_Detail + " ครึ่งวัน (บ่าย)"
                                                }
                                            } else if (leaveItem.T_Leave_Date_Start <= $scope.Date && leaveItem.T_Leave_Date_End >= $scope.Date) {
                                                dataItem.A_Time_in2 = leaveItem.Leave_Detail;
                                                dataItem.A_Time_ho = leaveItem.Leave_Detail;
                                                dataItem.A_Time_hn = leaveItem.Leave_Detail;
                                                dataItem.A_Time_out2 = leaveItem.Leave_Detail;
                                            } else {

                                            }
                                        }
                                    });
                                });
                            });
                    } else {
                        $scope.S_TimeOut_A = '-';
                        $scope.S_TimeIn_A = '-';
                    }
                });
        }

        $scope.FilterWord = function (value) {
            $scope.dtInstance.DataTable.search(value);

            $scope.dtInstance.DataTable.search(value).draw();
        };

        $scope.FilterWordApprove = function (value) {
            $scope.dtInstanceApprove.DataTable.search(value);

            $scope.dtInstanceApprove.DataTable.search(value).draw();
        };

        $scope.htmlTrusted = function (html) {
            return $sce.trustAsHtml(html);
        }

        $scope.SetTimeBefore = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() - 30);
            return DateT + " " + d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeBeforeHour = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() - 60);
            return DateT + " " + d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeBeforeCustom = function (DateT, Time, Minute) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() - Minute);
            return d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeLate = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() + 30);
            return DateT + " " + d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeLateHours = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setHours(d1.getHours() + 1);
            return DateT + " " + d1.toLocaleTimeString('en-GB')
        }

        $scope.ShowDateSelect = function (DateResult) {
            // นำค่าวันที่ปัจจุบันเก็บใส่ตัวแปร
            var today = $scope.GetCurrentTime();

            var Day = DateResult.split('/')[1];
            var Month = DateResult.split('/')[0];
            var Year = DateResult.split('/')[2];
            var RangeDateToShow = Year + "-" + Month + "-" + Day;
            $scope.Date = RangeDateToShow;

            document.getElementById("datepicker").value = DateResult;
            $scope.checkComment()
            $scope.update();
            $scope.getEditTime().then(res => {
                $scope.EditTime = res
            })
        }

        $scope.SetTimeShowTable = function (TimeS, TimeE) {
            var T_hourS = TimeS.split(":")[0];
            var T_MinuteS = TimeS.split(":")[1];
            var T_hourS2 = TimeE.split(":")[0];
            var T_MinuteS2 = TimeE.split(":")[1];
            return T_hourS + ":" + T_MinuteS + " น. - " + T_hourS2 + ":" + T_MinuteS2 + " น.";
        }

        $scope.SetDateToDB = function (DateTpDB) {
            $scope.DateSelect = DateTpDB;
            $scope.Day = $scope.DateSelect.split('/')[1];
            $scope.Month = $scope.DateSelect.split('/')[0];
            $scope.Year = $scope.DateSelect.split('/')[2];
            return $scope.Year + "-" + $scope.Month + "-" +
                $scope.Day;
        }

        // เรียกข้อมูลบุคลากรภายในหน่วยงานทั้งหมดภายใต้เงื่อนไข depcode จาก select html รหัสหน่วยงาน
        $scope.GetPerson = function (comment) {
            if (!comment) {
                $scope.CheckComment = true;
            } else {
                data = {
                    'DepCode': $scope.DepartSelect,
                    'PERID': <?php echo $_SESSION['PERID']?>
                };
                $http.post('./ApiService/T_GetPersonDepart_USER', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        $scope.personToComment = response.data;
                    });
                $scope.CheckComment = false;
            }
        }

        // เรียกหมายเหตุทั้งหมดในน่วยงานภายใต้เงื่อนไข วันที่และ depcode
        $scope.GetComment = function (comment) {
            $scope.ShowTitleComment = true;
            // if (!comment) {
            //     $scope.CheckGetComment = true;
            // } else {
            $scope.GetCommentData = [];
            data = {
                'DepCode': $scope.DepartSelect,
                'DateConrdi': $scope.Date,
                'PERID': <?php echo $_SESSION['PERID']?>
            };

            $http.post('./ApiService/GetComment_USER', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.GetCommentData = response.data;
                });
            $scope.CheckGetComment = false;
            // }
        }

        // เรียกหมายเหตุทั้งหมดในน่วยงาน
        $scope.GetAllComment = function (comment) {
            $scope.ShowTitleComment = false;
            // if (!comment) {
            //     $scope.CheckGetComment = true;
            // } else {
            $scope.GetCommentData = [];
            data = {
                'DepCode': $scope.DepartSelect,
                'PERID': <?php echo $_SESSION['PERID']?>
            };

            $http.post('./ApiService/GetAllComment_USER', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.GetCommentData = response.data;
                });
            $scope.CheckGetComment = false;
            // }
        }

        $scope.SaveComment = function () {
            var myElement = angular.element(document.querySelector('#PersonCode'));
            var name = '';
            var data = '';
            var TimeSet_I = '';
            var TimeSet_O = '';
            $scope.DateSelect = document.getElementById("datepicker").value;
            $scope.Day = $scope.DateSelect.split('/')[1];
            $scope.Month = $scope.DateSelect.split('/')[0];
            $scope.Year = $scope.DateSelect.split('/')[2];
            $scope.Date = $scope.Year + "-" + $scope.Month + "-" +
                $scope.Day;
            if ($scope.Date != "undefined--undefined" && document.getElementById('txtCommect').value != '' && myElement[0].options[myElement[0].selectedIndex].value != "? undefined:undefined ?") {
                angular.forEach($scope.personToComment, function (person) {
                    if (myElement[0].options[myElement[0].selectedIndex].value == person.PERID) {
                        name = person.NAME + ' ' + person.SURNAME;
                    }
                });
                Swal({
                    title: 'ยืนยันการทำรายการ',
                    text: "ต้องการลงบันทึกหมายเหตุการปฏิบัติงาน คุณ : " + name + " ใช่หรือไม่",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ตกลง',
                    cancelButtonText: 'ยกเลิก'
                }).then((result) => {
                    if (result.value) {
                        if ($scope.CheckEditTime) {
                            if ($scope.Check_TimeRange_I && !$scope.Check_TimeRange_O) {
                                if (document.getElementById("Comment_Time_I").value != null && document.getElementById("Comment_Time_I").value != '') {
                                    TimeSet_I = document.getElementById("Comment_Time_I").value + ":00";
                                    if (document.getElementById("Comment_Time_I").value.split(":")[0].length == 1 && document.getElementById("Comment_Time_I").value.split(":")[0] < 10) {
                                        TimeSet_I = "0" + TimeSet_I;
                                    }

                                    data = {
                                        'DepCode': $scope.DepartSelect,
                                        'PerId': myElement[0].options[myElement[0].selectedIndex].value,
                                        'DateConrdi': $scope.Date,
                                        'Comment': document.getElementById('txtCommect').value,
                                        'Time_I': TimeSet_I,
                                        'Time_O': '',
                                        'PerId_Create': <?php echo $_SESSION['PERID']?>
                                    };
                                }
                            } else if (!$scope.Check_TimeRange_I && $scope.Check_TimeRange_O) {
                                if (document.getElementById("Comment_Time_O").value != null && document.getElementById("Comment_Time_O").value != '') {
                                    TimeSet_O = document.getElementById("Comment_Time_O").value + ":00";
                                    if (document.getElementById("Comment_Time_O").value.split(":")[0].length == 1 && document.getElementById("Comment_Time_O").value.split(":")[0] < 10) {
                                        TimeSet_O = "0" + TimeSet_O;
                                    }

                                    data = {
                                        'DepCode': $scope.DepartSelect,
                                        'PerId': myElement[0].options[myElement[0].selectedIndex].value,
                                        'DateConrdi': $scope.Date,
                                        'Comment': document.getElementById('txtCommect').value,
                                        'Time_I': '',
                                        'Time_O': TimeSet_O,
                                        'PerId_Create': <?php echo $_SESSION['PERID']?>
                                    };
                                }
                            } else if ($scope.Check_TimeRange_I && $scope.Check_TimeRange_O) {
                                if ((document.getElementById("Comment_Time_I").value != null && document.getElementById("Comment_Time_I").value != '') && (document.getElementById("Comment_Time_O").value != null && document.getElementById("Comment_Time_O").value != '')) {
                                    TimeSet_I = document.getElementById("Comment_Time_I").value + ":00";
                                    TimeSet_O = document.getElementById("Comment_Time_O").value + ":00";
                                    if (document.getElementById("Comment_Time_I").value.split(":")[0].length == 1 && document.getElementById("Comment_Time_I").value.split(":")[0] < 10) {
                                        TimeSet_I = "0" + TimeSet_I;
                                    }
                                    if (document.getElementById("Comment_Time_O").value.split(":")[0].length == 1 && document.getElementById("Comment_Time_O").value.split(":")[0] < 10) {
                                        TimeSet_O = "0" + TimeSet_O;
                                    }

                                    data = {
                                        'DepCode': $scope.DepartSelect,
                                        'PerId': myElement[0].options[myElement[0].selectedIndex].value,
                                        'DateConrdi': $scope.Date,
                                        'Comment': document.getElementById('txtCommect').value,
                                        'Time_I': TimeSet_I,
                                        'Time_O': TimeSet_O,
                                        'PerId_Create': <?php echo $_SESSION['PERID']?>
                                    };
                                }
                            } else {

                            }
                        } else {
                            data = {
                                'DepCode': $scope.DepartSelect,
                                'PerId': myElement[0].options[myElement[0].selectedIndex].value,
                                'DateConrdi': $scope.Date,
                                'Comment': document.getElementById('txtCommect').value,
                                'Time_I': '',
                                'Time_O': '',
                                'PerId_Create': <?php echo $_SESSION['PERID']?>
                            };
                        }

                        $http.post('./ApiService/SaveComment', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                var result = response.data;
                                if (result.Status) {
                                    Swal({
                                        type: 'success',
                                        title: result.Message
                                    });
                                    document.getElementById('txtCommect').value = '';
                                    $scope.PrsonSelect = 0;
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: result.Message
                                    });
                                }
                            });
                    }
                });
            } else {
                Swal({
                    type: 'error',
                    title: 'โปรดกรอกข้อมูลให้ครบถ้วน'
                });
            }
        }

        $scope.funcRangeCal = function (TimeDep) {
            data = {
                'DepCode': $scope.DepartSelect,
                'DateConrdi': TimeDep
            };

            $http.post('./ApiService/GetTimeSetDep', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    var timer = response.data;
                    if (timer.length != 0) {

                        manageTimeRange(timer[0].D_Time_TimeN, timer[0].D_Time_TimeO, timer[0].D_Time_TimeHO, timer[0].D_Time_TimeHN, timer[0].door_id, true, TimeDep);
                    } else {

                        manageTimeRange("08:30:00", "16:30:00", "12:00:00", "12:30:00", $scope.DoorArr, false, TimeDep);
                    }
                });
        }

        $scope.updateRange = function () {
            $scope.DataAllPerson = [];
            $scope.DataTimeDepCheck = [];
            $scope.DataTimeDepShow = [];
            $scope.MixTimeArrRange = [];

            var ArrFunc_Range = [];
            var RangeDate = '';
            var RangeDateEnd = '';

            var d1 = new Date((document.getElementById("reservation").value).split(" - ")[0]);
            var d2 = new Date((document.getElementById("reservation").value).split(" - ")[1]);

            while (d1 <= d2) {

                if (d1.getDate() <= 9 && (d1.getMonth() + 1) <= 9) {
                    RangeDate = "0" + (d1.getMonth() + 1) + '/' + "0" + d1.getDate() + '/' + d1.getFullYear();
                    RangeDateEnd = "0" + (d2.getMonth() + 1) + '/' + "0" + d2.getDate() + '/' + d2.getFullYear();
                } else if (d1.getDate() <= 9 && (d1.getMonth() + 1) > 9) {
                    RangeDate = (d1.getMonth() + 1) + '/0' + d1.getDate() + '/' + d1.getFullYear();
                    RangeDateEnd = (d2.getMonth() + 1) + '/0' + d2.getDate() + '/' + d2.getFullYear();
                } else if (d1.getDate() > 9 && (d1.getMonth() + 1) <= 9) {
                    RangeDate = "0" + (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
                    RangeDateEnd = "0" + (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
                } else {
                    RangeDate = (d1.getMonth() + 1) + '/' + d1.getDate() + '/' + d1.getFullYear();
                    RangeDateEnd = (d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear();
                }

                var Day = RangeDate.split('/')[1];
                var Month = RangeDate.split('/')[0];
                var Year = RangeDate.split('/')[2];
                var RangeDateToDB = Year + "-" + Month + "-" + Day;
                var options = {
                    weekday: 'long'
                };
                $scope.DataTimeDepShow.push({
                    'DateShow': RangeDate,
                    'DateName': d1.toLocaleDateString('en-US', options),
                    'DateDB': RangeDateToDB,
                    'DateOff': '',
                    'DateOffName': ''
                });
                angular.forEach($scope.DataTimeDepShow, function (DataTimeItem) {
                    angular.forEach($scope.Holiday, function (hoItem) {
                        if (hoItem.Date_stop == DataTimeItem.DateDB) {
                            DataTimeItem.DateOff = hoItem.Date_stop,
                                DataTimeItem.DateOffName = hoItem.Date_name
                        } else {
                        }
                    });
                });
                $scope.DataTimeDepCheck.push(RangeDateToDB);
                d1.setDate(d1.getDate() + 1);
            }
        }

        $scope.update = function () {
            if (document.getElementById("datepicker").value == '') {
                Swal({
                    type: 'error',
                    title: 'ไม่สามารถทำรายการได้สำเร็จ...',
                    text: 'โปรดระบุวันที่ต้องการดูข้อมูล'
                })
            } else if ($scope.DepartSelect == null || $scope.DepartSelect == '') {
                Swal({
                    type: 'error',
                    title: 'ไม่สามารถทำรายการได้สำเร็จ...',
                    text: 'โปรดระบุหน่วยงานที่ต้องการดูข้อมูล'
                })
            } else {
                // นำค่าวันที่ปัจจุบันเก็บใส่ตัวแปร
                var today = $scope.GetCurrentTime();
                $scope.Loading = true;
                $scope.ShowTable = true;
                $scope.doorInName = '';
                $scope.DateSelect = document.getElementById("datepicker").value;
                $scope.Day = $scope.DateSelect.split('/')[1];
                $scope.Month = $scope.DateSelect.split('/')[0];
                $scope.Year = $scope.DateSelect.split('/')[2];
                $scope.Date = $scope.Year + "-" + $scope.Month + "-" +
                    $scope.Day;

                if ($scope.Date < today) {
                    $scope.CheckApprove = true;
                } else {
                    $scope.CheckApprove = false;
                }
                ///////////////////////////////////////////////////////////////////////
                ////////////////เรียกจากการApproveมาตรวจสอบว่าเคยมีการApprove //////////////
                ////////////////ตารางมาก่อนหรือไม่/////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////
                data = {
                    'DepCode': [{'Dep_Code': $scope.DepartSelect}],
                    'DateConrdi': $scope.Date
                };

                $http.post('./ApiService/GetDataApprove', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        var DataApprove = response.data;
                        if (DataApprove.length <= 0) {
                            $scope.DataApp = false;
                        } else {
                            $scope.DataApp = true;
                        }
                    });


                ///////////////////////////////////////////////////////////////////////
                ////////////////เรียกข้อมูลเวลาจากหน่วยงานเพื่อนำค่ามาคำนวณ///////////////////
                ///////////////////////////////////////////////////////////////////////
                data = {
                    'DepCode': [{'Dep_Code': $scope.DepartSelect}],
                    'DateConrdi': $scope.Date,
                    'PERID': <?php echo $_SESSION['PERID']?>
                };

                $http.post('./ApiService/GetTimeSetDepUser', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        var timesetDep = response.data;
                        $scope.uniqueStandards2 = [];
                        $scope.uniqueStandards = [];
                        /////////////////////////////////////////////////////////////////////
                        //////////////ถ้าสำเร็จเรียกค่าเวลาจากบุคลากรในหน่วยงานมาคำนวณ///////////////////
                        /////////////////////////////////////////////////////////////////////
                        if (timesetDep.length != 0) {
                            // $scope.T_time_in = $scope.SetTimeShowTable(timesetDep[0].D_Time_TimeN, $scope.SetTimeLate($scope.Date, timesetDep[0].D_Time_TimeN).split(" ")[1]);
                            // $scope.T_time_in2 = $scope.SetTimeShowTable(timesetDep[0].D_Time_TimeN, $scope.SetTimeLate($scope.Date, timesetDep[0].D_Time_TimeN).split(" ")[1]);
                            // $scope.T_time_out = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, timesetDep[0].D_Time_TimeO).split(" ")[1], timesetDep[0].D_Time_TimeO);
                            // $scope.T_time_out2 = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, timesetDep[0].D_Time_TimeO).split(" ")[1], timesetDep[0].D_Time_TimeO);
                            // $scope.T_time_hn = $scope.SetTimeShowTable(timesetDep[0].D_Time_TimeHN, $scope.SetTimeLateHours($scope.Date, timesetDep[0].D_Time_TimeHN).split(" ")[1]);
                            // $scope.T_time_ho = $scope.SetTimeShowTable($scope.SetTimeBeforeHour($scope.Date, timesetDep[0].D_Time_TimeHO).split(" ")[1], timesetDep[0].D_Time_TimeHO);
                            // $scope.DataSuccess = true;
                            // $scope.doorInName = $scope.FindDoor(timesetDep[0].door_id);
                            angular.forEach(timesetDep[0].DoorData, function (DoorItem, door_idx) {
                                if (door_idx == timesetDep[0].DoorData.length - 1) {
                                    $scope.doorInName += $scope.FindDoor(DoorItem.door_id);
                                } else {
                                    $scope.doorInName += $scope.FindDoor(DoorItem.door_id) + '  ,  ';
                                }
                            })

                            manageTime(timesetDep);
                            $scope.updateRange();
                            $scope.S_TimeIn = '00:00:00 - ' + timesetDep[0].D_Time_TimeN + ' (ปกติ)';
                            $scope.S_TimeOut = timesetDep[0].D_Time_TimeO + ' - 23:59:59 (ปกติ)';
                            $scope.S_TimeIn_A = '00:00:00 - ' + timesetDep[0].D_Time_TimeN + ' (ปกติ)';
                            $scope.S_TimeOut_A = timesetDep[0].D_Time_TimeO + ' - 23:59:59 (ปกติ)';
                            $scope.getApproveData();
                        } else {
                            // $scope.T_time_in = $scope.SetTimeShowTable("08:30:00", $scope.SetTimeLate($scope.Date, "08:30:00").split(" ")[1]);
                            // $scope.T_time_in2 = $scope.SetTimeShowTable("08:30:00", $scope.SetTimeLate($scope.Date, "08:30:00").split(" ")[1]);
                            // $scope.T_time_out = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "16:30:00").split(" ")[1], "16:30:00");
                            // $scope.T_time_out2 = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "16:30:00").split(" ")[1], "16:30:00");
                            // $scope.T_time_hn = $scope.SetTimeShowTable("12:30:00", $scope.SetTimeLateHours($scope.Date, "12:30:00").split(" ")[1]);
                            // $scope.T_time_ho = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "12:00:00").split(" ")[1], $scope.SetTimeLate($scope.Date, "12:00:00").split(" ")[1]);
                            // $scope.DataSuccess = true;
                            // $scope.doorInName = 'ประตูทั้งหมด';
                            manageTime(timesetDep);
                            $scope.updateRange();
                            $scope.S_TimeIn = '00:00:00 - 08:30:00 (ปกติ)';
                            $scope.S_TimeOut = '16:30:00 - 23:59:00 (ปกติ)';
                            $scope.S_TimeIn_A = '00:00:00 - 08:30:00 (ปกติ)';
                            $scope.S_TimeOut_A = '16:30:00 - 23:59:00 (ปกติ)';
                            $scope.getApproveData();
                        }
                        $scope.GetComment();
                    });
            }
        }

        var manageTime = function (TimeSetObj) {
            $scope.CheckName = '';
            var Old_timeIn = "08:30:00";
            var Old_timeOut = "16:30:00";
            var Old_timeHo = "12:30:00";
            var Old_timeHn = "12:30:00";
            var BeforeH_timeHo = $scope.SetTimeBeforeHour($scope.Date, "12:30:00");
            var LateH_timeHn = $scope.SetTimeLateHours($scope.Date, "12:30:00");
            var _timeIn = $scope.SetTimeLate($scope.Date, "08:30:00");
            var _timeHnOld = "12:30:00";
            var _timeHn = $scope.SetTimeLate($scope.Date, "12:30:00");
            var _timeHo = $scope.SetTimeBefore($scope.Date, "12:30:00");
            var _timeOut = $scope.SetTimeBefore($scope.Date, "16:30:00");
            var _doorId = $scope.DoorArr;
            var _condition = false;

            var CheckTimeSet = false;

            $scope.statusIn = '';
            $scope.statusOut = '';

            data = {
                'DepCode': $scope.DepartSelect,
                'DateConrdi': $scope.Date,
                'PERID': <?php echo $_SESSION['PERID']?>
            };
            $http.post('./ApiService/GETDATADATEDEP_USER', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.ItemData = response.data;
                    $scope.DataResult = [];
                    if ($scope.ItemData.length > 0) {
                        angular.forEach($scope.ItemData, function (arr) {
                            if ($scope.CheckName != arr.PERID) {
                                $scope.timein = '';
                                $scope.timein2 = '';
                                $scope.timehn = '';
                                $scope.timeho = '';
                                $scope.timeout = '';
                                $scope.timeout2 = '';
                                $scope.door_in = '';
                                $scope.door_in2 = '';
                                $scope.door_hn = '';
                                $scope.door_ho = '';
                                $scope.door_out = '';
                                $scope.door_out2 = '';

                                if (TimeSetObj.length > 0 && !TimeSetObj[0].Cordi) {
                                    angular.forEach(TimeSetObj, function (TimeSetI, time_Idx) {
                                        if (arr.Dep_Code == TimeSetI.Dep_code) {
                                            Old_timeIn = TimeSetI.D_Time_TimeN;
                                            Old_timeOut = TimeSetI.D_Time_TimeO;
                                            Old_timeHo = TimeSetI.D_Time_TimeHO;
                                            Old_timeHn = TimeSetI.D_Time_TimeHN;
                                            BeforeH_timeHo = $scope.SetTimeBeforeHour($scope.Date, TimeSetI.D_Time_TimeHO);
                                            LateH_timeHn = $scope.SetTimeLateHours($scope.Date, TimeSetI.D_Time_TimeHN);
                                            _timeIn = $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeN);
                                            _timeHnOld = TimeSetI.D_Time_TimeHN;
                                            _timeHn = $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeHN);
                                            _timeHo = $scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeHO);
                                            _timeOut = $scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeO);
                                            CheckTimeSet = true;
                                            _doorId = TimeSetI.DoorData;
                                            _condition = true;

                                            $scope.T_time_in = $scope.SetTimeShowTable(TimeSetI.D_Time_TimeN, $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeN).split(" ")[1]);
                                            $scope.T_time_in2 = $scope.SetTimeShowTable(TimeSetI.D_Time_TimeN, $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeN).split(" ")[1]);
                                            $scope.T_time_out = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeO).split(" ")[1], TimeSetI.D_Time_TimeO);
                                            $scope.T_time_out2 = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeO).split(" ")[1], TimeSetI.D_Time_TimeO);
                                            $scope.T_time_hn = $scope.SetTimeShowTable(TimeSetI.D_Time_TimeHN, $scope.SetTimeLateHours($scope.Date, TimeSetI.D_Time_TimeHN).split(" ")[1]);
                                            $scope.T_time_ho = $scope.SetTimeShowTable($scope.SetTimeBeforeHour($scope.Date, TimeSetI.D_Time_TimeHO).split(" ")[1], TimeSetI.D_Time_TimeHO);
                                        } else {
                                            if (time_Idx === TimeSetObj.length - 1 && !CheckTimeSet) {
                                                Old_timeIn = "08:30:00";
                                                Old_timeOut = "16:30:00";
                                                Old_timeHo = "12:30:00";
                                                Old_timeHn = "12:30:00";
                                                BeforeH_timeHo = $scope.SetTimeBeforeHour($scope.Date, "12:30:00");
                                                LateH_timeHn = $scope.SetTimeLateHours($scope.Date, "12:30:00");
                                                _timeIn = $scope.SetTimeLate($scope.Date, "08:30:00");
                                                _timeHnOld = "12:30:00";
                                                _timeHn = $scope.SetTimeLate($scope.Date, "12:30:00");
                                                _timeHo = $scope.SetTimeBefore($scope.Date, "12:30:00");
                                                _timeOut = $scope.SetTimeBefore($scope.Date, "16:30:00");
                                                CheckTimeSet = false;
                                                _doorId = $scope.DoorArr;
                                                _condition = false;

                                                $scope.T_time_in = $scope.SetTimeShowTable("08:30:00", $scope.SetTimeLate($scope.Date, "08:30:00").split(" ")[1]);
                                                $scope.T_time_in2 = $scope.SetTimeShowTable("08:30:00", $scope.SetTimeLate($scope.Date, "08:30:00").split(" ")[1]);
                                                $scope.T_time_out = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "16:30:00").split(" ")[1], "16:30:00");
                                                $scope.T_time_out2 = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "16:30:00").split(" ")[1], "16:30:00");
                                                $scope.T_time_hn = $scope.SetTimeShowTable("12:30:00", $scope.SetTimeLateHours($scope.Date, "12:30:00").split(" ")[1]);
                                                $scope.T_time_ho = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "12:00:00").split(" ")[1], $scope.SetTimeLate($scope.Date, "12:00:00").split(" ")[1]);
                                            }
                                        }
                                    })
                                } else {
                                    angular.forEach(TimeSetObj, function (TimeSetI, time_Idx) {
                                        if (arr.Dep_Code == TimeSetI.Dep_Code_Old) {
                                            Old_timeIn = TimeSetI.D_Time_TimeN;
                                            Old_timeOut = TimeSetI.D_Time_TimeO;
                                            Old_timeHo = TimeSetI.D_Time_TimeHO;
                                            Old_timeHn = TimeSetI.D_Time_TimeHN;
                                            BeforeH_timeHo = $scope.SetTimeBeforeHour($scope.Date, TimeSetI.D_Time_TimeHO);
                                            LateH_timeHn = $scope.SetTimeLateHours($scope.Date, TimeSetI.D_Time_TimeHN);
                                            _timeIn = $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeN);
                                            _timeHnOld = TimeSetI.D_Time_TimeHN;
                                            _timeHn = $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeHN);
                                            _timeHo = $scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeHO);
                                            _timeOut = $scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeO);
                                            CheckTimeSet = true;
                                            _doorId = TimeSetI.DoorData;
                                            _condition = true;

                                            $scope.T_time_in = $scope.SetTimeShowTable(TimeSetI.D_Time_TimeN, $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeN).split(" ")[1]);
                                            $scope.T_time_in2 = $scope.SetTimeShowTable(TimeSetI.D_Time_TimeN, $scope.SetTimeLate($scope.Date, TimeSetI.D_Time_TimeN).split(" ")[1]);
                                            $scope.T_time_out = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeO).split(" ")[1], TimeSetI.D_Time_TimeO);
                                            $scope.T_time_out2 = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, TimeSetI.D_Time_TimeO).split(" ")[1], TimeSetI.D_Time_TimeO);
                                            $scope.T_time_hn = $scope.SetTimeShowTable(TimeSetI.D_Time_TimeHN, $scope.SetTimeLateHours($scope.Date, TimeSetI.D_Time_TimeHN).split(" ")[1]);
                                            $scope.T_time_ho = $scope.SetTimeShowTable($scope.SetTimeBeforeHour($scope.Date, TimeSetI.D_Time_TimeHO).split(" ")[1], TimeSetI.D_Time_TimeHO);
                                        } else {
                                            if (time_Idx === TimeSetObj.length - 1 && !CheckTimeSet) {
                                                Old_timeIn = "08:30:00";
                                                Old_timeOut = "16:30:00";
                                                Old_timeHo = "12:30:00";
                                                Old_timeHn = "12:30:00";
                                                BeforeH_timeHo = $scope.SetTimeBeforeHour($scope.Date, "12:30:00");
                                                LateH_timeHn = $scope.SetTimeLateHours($scope.Date, "12:30:00");
                                                _timeIn = $scope.SetTimeLate($scope.Date, "08:30:00");
                                                _timeHnOld = "12:30:00";
                                                _timeHn = $scope.SetTimeLate($scope.Date, "12:30:00");
                                                _timeHo = $scope.SetTimeBefore($scope.Date, "12:30:00");
                                                _timeOut = $scope.SetTimeBefore($scope.Date, "16:30:00");
                                                CheckTimeSet = false;
                                                _doorId = $scope.DoorArr;
                                                _condition = false;

                                                $scope.T_time_in = $scope.SetTimeShowTable("08:30:00", $scope.SetTimeLate($scope.Date, "08:30:00").split(" ")[1]);
                                                $scope.T_time_in2 = $scope.SetTimeShowTable("08:30:00", $scope.SetTimeLate($scope.Date, "08:30:00").split(" ")[1]);
                                                $scope.T_time_out = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "16:30:00").split(" ")[1], "16:30:00");
                                                $scope.T_time_out2 = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "16:30:00").split(" ")[1], "16:30:00");
                                                $scope.T_time_hn = $scope.SetTimeShowTable("12:30:00", $scope.SetTimeLateHours($scope.Date, "12:30:00").split(" ")[1]);
                                                $scope.T_time_ho = $scope.SetTimeShowTable($scope.SetTimeBefore($scope.Date, "12:00:00").split(" ")[1], $scope.SetTimeLate($scope.Date, "12:00:00").split(" ")[1]);
                                            }
                                        }
                                    })
                                }

                                //คำนวณเวลาเข้าประตูบริเวณ รพ ทั้งหมด
                                angular.forEach($scope.ItemData, function (arr2) {
                                    if (arr.PERID == arr2.PERID) {

                                        if (arr2.event_time <= $scope.Date + " " + Old_timeIn) {

                                            if (_condition) {

                                                var CheckAllDoor = false;
                                                angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                    if (doorIdItem.door_id == '0') {
                                                        CheckAllDoor = true;
                                                    }
                                                })

                                                if (CheckAllDoor) {
                                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                                        $scope.timein2 = arr2.event_time.split(' ')[1];
                                                        $scope.statusIn = '';
                                                        $scope.door_in2 = arr2.door_id;
                                                    }
                                                } else {
                                                    var CheckDoor = false;
                                                    angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                        if (arr2.door_id == doorIdItem.door_id) {
                                                            CheckDoor = true;
                                                        }
                                                    })
                                                    if (!CheckDoor) {
                                                        var checkdooZone = true;
                                                        angular.forEach($scope.DoorArrOutDepart, function (
                                                            doorAllArrOut, idx) {
                                                            if (checkdooZone) {
                                                                if (arr2.door_id == doorAllArrOut.door_id) {
                                                                    checkdooZone = false;
                                                                    return false;
                                                                } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                    if ($scope.timein == null || $scope.timein == '') {
                                                                        $scope.timein = arr2.event_time.split(' ')[1];
                                                                        $scope.door_in = arr2.door_id;
                                                                        return false;
                                                                    }
                                                                    checkdooZone = false;
                                                                }
                                                            }
                                                        });
                                                        // if ($scope.timein == null || $scope.timein == '') {
                                                        //     $scope.timein = arr2.event_time.split(' ')[1];
                                                        //     $scope.door_in = arr2.door_id;
                                                        // }
                                                    } else {
                                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                                            $scope.timein2 = arr2.event_time.split(' ')[1];
                                                            $scope.statusIn = '';
                                                            $scope.door_in2 = arr2.door_id;
                                                        }
                                                    }
                                                }
                                            } else {
                                                angular.forEach($scope.DoorArr, function (
                                                    doorAllArr, idx) {
                                                    if (arr2.door_id == doorAllArr.door_id) {
                                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                                            $scope.timein2 = arr2.event_time.split(' ')[1];
                                                            $scope.statusIn = '';
                                                            $scope.door_in2 = arr2.door_id;
                                                            return false;
                                                        }
                                                    }
                                                });
                                                // angular.forEach($scope.DoorArrOutDepart, function (
                                                //     doorAllArrOut, idx) {
                                                //     if (arr2.door_id == doorAllArrOut.door_id) {
                                                //         if ($scope.timein == null || $scope.timein == '') {
                                                //             $scope.timein = arr2.event_time.split(' ')[1];
                                                //             $scope.door_in = arr2.door_id;
                                                //             return false;
                                                //         }
                                                //     }
                                                // });
                                            }
                                        } else if (arr2.event_time > $scope.Date + " " + Old_timeIn && arr2.event_time <= _timeIn) {
                                            // var DateHo = _timeHo.split(' ')[0];
                                            // var TimeHo = _timeHo.split(' ')[1];
                                            // var Hour = TimeHo.split(
                                            //     ':')[0];
                                            // var Minute = TimeHo.split(':')[1];
                                            // var Sec = TimeHo.split(':')[2];
                                            // Hour = parseInt(Hour) - 1;
                                            // var newDateTime = Hour + ":" + Minute + ":" + Sec;
                                            // if (arr2.event_time <= DateHo + " " + newDateTime) {
                                            if (_condition) {
                                                var CheckAllDoor = false;
                                                angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                    if (doorIdItem.door_id == '0') {
                                                        CheckAllDoor = true;
                                                    }
                                                })
                                                if (CheckAllDoor) {
                                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                                        $scope.timein2 = arr2.event_time.split(' ')[1];
                                                        $scope.statusIn = '(สาย)';
                                                        $scope.door_in2 = arr2.door_id;
                                                    }
                                                } else {
                                                    var CheckDoor = false;
                                                    angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                        if (arr2.door_id == doorIdItem.door_id) {
                                                            CheckDoor = true;
                                                        }
                                                    })
                                                    if (!CheckDoor) {
                                                        var checkdooZone = true;
                                                        angular.forEach($scope.DoorArrOutDepart, function (
                                                            doorAllArrOut, idx) {
                                                            if (checkdooZone) {
                                                                if (arr2.door_id == doorAllArrOut.door_id) {
                                                                    checkdooZone = false;
                                                                    return false;
                                                                } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                    if ($scope.timein == null || $scope.timein == '') {
                                                                        $scope.timein = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                                                        $scope.door_in = arr2.door_id;
                                                                    }
                                                                    if ($scope.timein2 != null && $scope.timein2 != '') {
                                                                        $scope.timein = arr2.event_time.split(' ')[1];
                                                                        $scope.door_in = arr2.door_id;
                                                                    }
                                                                    checkdooZone = false;
                                                                }
                                                            }
                                                        });
                                                        // if ($scope.timein == null || $scope.timein == '') {
                                                        //     $scope.timein = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                                        //     $scope.door_in = arr2.door_id;
                                                        // }
                                                    } else {
                                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                                            $scope.timein2 = arr2.event_time.split(' ')[1];
                                                            $scope.statusIn = '(สาย)';
                                                            $scope.door_in2 = arr2.door_id;
                                                        }
                                                        if ($scope.timein != null && $scope.timein != '') {
                                                            $scope.timein = $scope.timein.replace('<span style=\"color:#E040FB\"> ( สาย ) </span>', '');
                                                        }
                                                    }
                                                }
                                            } else {
                                                angular.forEach($scope.DoorArr, function (
                                                    doorAllArr, idx) {
                                                    if (arr2.door_id == doorAllArr.door_id) {
                                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                                            $scope.timein2 = arr2.event_time.split(' ')[1];
                                                            $scope.statusIn = '(สาย)';
                                                            $scope.door_in2 = arr2.door_id;
                                                            return false;
                                                        }
                                                    }
                                                });

                                                // angular.forEach($scope.DoorArrOutDepart, function (
                                                //     doorAllArrOut, idx) {
                                                //     if (arr2.door_id == doorAllArrOut.door_id) {
                                                //         if ($scope.timein == null || $scope.timein == '') {
                                                //             $scope.timein = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                                //             $scope.door_in = arr2.door_id;
                                                //             return false;
                                                //         }
                                                //     }
                                                // });
                                            }
                                            // }
                                        } else {

                                        }
                                    }
                                });
                                //////////////////////////////////////////

                                //คำนวณเวลาออก (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                                angular.forEach($scope.ItemData, function (
                                    arr2) {

                                    if (arr.PERID == arr2.PERID) {
                                        if (($scope.timein != null && $scope.timein != '') || ($scope.timein2 != null && $scope.timein2 != '')) {
                                            if (arr2.event_time >= _timeHo && arr2.event_time <= $scope.Date + " " + _timeHnOld) {
                                                if ($scope.timeho == null || $scope.timeho == '') {
                                                    $scope.timeho = arr2.event_time.split(' ')[1];
                                                    $scope.door_ho = arr2.door_id;
                                                }
                                            } else if (arr2.event_time >= BeforeH_timeHo && arr2.event_time < _timeHo) {
                                                // var DateHo = _timeHo.split(' ')[0];
                                                // var TimeHo = _timeHo.split(' ')[1];
                                                // var Hour = TimeHo.split(
                                                //     ':')[0];
                                                // var Minute = TimeHo.split(':')[1];
                                                // var Sec = TimeHo.split(':')[2];
                                                // Hour = parseInt(Hour) - 1;
                                                // var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                                // if (arr2.event_time > DateHo + " " + newDateTime && arr2.event_time < _timeHo) {
                                                if ($scope.timeho == null || $scope.timeho == '') {
                                                    $scope.timeho = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( ออกก่อน ) </span>";
                                                    $scope.door_ho = arr2.door_id;
                                                }
                                                // }
                                            } else {
                                            }
                                        } else {
                                            if (arr2.event_time < _timeHn) {
                                                if (_condition) {
                                                    var CheckAllDoor = false;
                                                    angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                        if (doorIdItem.door_id == '0') {
                                                            CheckAllDoor = true;
                                                        }
                                                    })
                                                    if (CheckAllDoor) {
                                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                                            $scope.timein2 = arr2.event_time.split(' ')[1];
                                                            $scope.statusIn = '(สาย)';
                                                            $scope.door_in2 = arr2.door_id;
                                                        }
                                                    } else {
                                                        var CheckDoor = false;
                                                        angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                            if (arr2.door_id == doorIdItem.door_id) {
                                                                CheckDoor = true;
                                                            }
                                                        })
                                                        if (!CheckDoor) {
                                                            var checkdooZone = true;
                                                            angular.forEach($scope.DoorArrOutDepart, function (
                                                                doorAllArrOut, idx) {
                                                                if (checkdooZone) {
                                                                    if (arr2.door_id == doorAllArrOut.door_id) {
                                                                        checkdooZone = false;
                                                                        return false;
                                                                    } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                        if ($scope.timein == null || $scope.timein == '') {
                                                                            $scope.timein = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                                                            $scope.door_in = arr2.door_id;
                                                                        }
                                                                        if ($scope.timein2 != null && $scope.timein2 != '') {
                                                                            $scope.timein = arr2.event_time.split(' ')[1];
                                                                            $scope.door_in = arr2.door_id;
                                                                        }
                                                                        checkdooZone = false;
                                                                    }
                                                                }
                                                            });
                                                            // if ($scope.timein == null || $scope.timein == '') {
                                                            //     $scope.timein = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                                            //     $scope.door_in = arr2.door_id;
                                                            // }
                                                        } else {
                                                            if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                $scope.statusIn = '(สาย)';
                                                                $scope.door_in2 = arr2.door_id;
                                                            }
                                                            if ($scope.timein != null && $scope.timein != '') {
                                                                $scope.timein = $scope.timein.replace('<span style=\"color:#E040FB\"> ( สาย ) </span>', '');
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    angular.forEach($scope.DoorArr, function (
                                                        doorAllArr, idx) {
                                                        if (arr2.door_id == doorAllArr.door_id) {
                                                            if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                $scope.statusIn = '(สาย)';
                                                                $scope.door_in2 = arr2.door_id;
                                                                return false;
                                                            }
                                                        }
                                                    });

                                                    // angular.forEach($scope.DoorArrOutDepart, function (
                                                    //     doorAllArrOut, idx) {
                                                    //     if (arr2.door_id == doorAllArrOut.door_id) {
                                                    //         if ($scope.timein == null || $scope.timein == '') {
                                                    //             $scope.timein = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                                    //             $scope.door_in = arr2.door_id;
                                                    //             return false;
                                                    //         }
                                                    //     }
                                                    // });
                                                }
                                            }
                                        }
                                    }
                                });

                                //////////////////////////////////////////

                                //คำนวณเวลาเข้า (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                                angular.forEach($scope.ItemData, function (
                                    arr2) {

                                    if (arr.PERID == arr2.PERID) {

                                        if (arr2.event_time > $scope.Date + " " + _timeHnOld &&
                                            arr2.event_time <= _timeHn
                                        ) {
                                            if ($scope.timehn == null || $scope.timehn == '') {
                                                $scope.timehn = arr2.event_time.split(' ')[1];
                                                $scope.door_hn = arr2.door_id;
                                            }

                                        } else if (arr2.event_time > _timeHn && arr2.event_time <= LateH_timeHn) {
                                            // var DateHn = _timeHn.split(' ')[0];
                                            // var TimeHn = _timeHn.split(' ')[1];
                                            // var Hour = TimeHn.split(
                                            //     ':')[0];
                                            // var Minute = TimeHn.split(':')[1];
                                            // var Sec = TimeHn.split(':')[2];
                                            // Hour = parseInt(Hour) + 1;
                                            // var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                            // if (arr2.event_time > _timeHn && arr2.event_time <= DateHn + " " + newDateTime) {
                                            if ($scope.timehn == null || $scope.timehn == '') {
                                                $scope.timehn = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                                $scope.door_hn = arr2.door_id;
                                            }
                                            // }
                                        }
                                    } else {

                                    }
                                });

                                //////////////////////////////////////////

                                //คำนวณเวลาออกประตูบริเวณ รพ ทั้งหมด
                                angular.forEach($scope.ItemData, function (arr2) {

                                    if (arr.PERID == arr2.PERID) {

                                        if (arr2.event_time >= $scope.Date + " " + Old_timeOut &&
                                            arr2.event_time <= $scope.Date + " 24:59:59"
                                        ) {
                                            if (_condition) {
                                                var CheckAllDoor = false;
                                                angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                    if (doorIdItem.door_id == '0') {
                                                        CheckAllDoor = true;
                                                    }
                                                })
                                                if (CheckAllDoor) {
                                                    $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                    $scope.statusOut = '';
                                                    $scope.door_out2 = arr2.door_id;
                                                } else {
                                                    var CheckDoor = false;
                                                    angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                        if (arr2.door_id == doorIdItem.door_id) {
                                                            CheckDoor = true;
                                                        }
                                                    })
                                                    if (!CheckDoor) {
                                                        var checkdooZone = true;
                                                        angular.forEach($scope.DoorArrOutDepart, function (
                                                            doorAllArrOut, idx) {
                                                            if (checkdooZone) {
                                                                if (arr2.door_id == doorAllArrOut.door_id) {
                                                                    checkdooZone = false;
                                                                    return false;
                                                                } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                    $scope.timeout = arr2.event_time.split(' ')[1];
                                                                    $scope.door_out = arr2.door_id;
                                                                    checkdooZone = false;
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                        $scope.statusOut = '';
                                                        $scope.door_out2 = arr2.door_id;
                                                    }
                                                }
                                            } else {
                                                angular.forEach($scope.DoorArr, function (
                                                    doorAllArr) {
                                                    if (arr2.door_id == doorAllArr.door_id) {
                                                        $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                        $scope.statusOut = '';
                                                        $scope.door_out2 = arr2.door_id;
                                                        return false;
                                                    }
                                                });
                                            }

                                        } else if (arr2.event_time >= _timeOut &&
                                            arr2.event_time < $scope.Date + " " + Old_timeOut
                                        ) {
                                            if (_condition) {
                                                var CheckAllDoor = false;
                                                angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                    if (doorIdItem.door_id == '0') {
                                                        CheckAllDoor = true;
                                                    }
                                                })
                                                if (CheckAllDoor) {
                                                    $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                    $scope.statusOut = '(ออกก่อน)';
                                                    $scope.door_out2 = arr2.door_id;
                                                } else {
                                                    var CheckDoor = false;
                                                    angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                        if (arr2.door_id == doorIdItem.door_id) {
                                                            CheckDoor = true;
                                                        }
                                                    })
                                                    if (!CheckDoor) {
                                                        // if ($scope.timeout == null || $scope.timeout == '') {
                                                        $scope.timeout = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( ออกก่อน ) </span>";
                                                        $scope.door_out = arr2.door_id;
                                                        // }
                                                    } else {
                                                        // if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                                        $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                        $scope.statusOut = '(ออกก่อน)';
                                                        $scope.door_out2 = arr2.door_id;
                                                        // }
                                                    }
                                                }
                                            } else {
                                                angular.forEach($scope.DoorArr, function (
                                                    doorAllArr) {
                                                    if (arr2.door_id == doorAllArr.door_id) {
                                                        var checkdooZone = true;
                                                        angular.forEach($scope.DoorArrOutDepart, function (
                                                            doorAllArrOut, idx) {
                                                            if (checkdooZone) {
                                                                if (arr2.door_id == doorAllArrOut.door_id) {
                                                                    checkdooZone = false;
                                                                    return false;
                                                                } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                    $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                    $scope.statusOut = '(ออกก่อน)';
                                                                    $scope.door_out2 = arr2.door_id;
                                                                    checkdooZone = false;
                                                                    return false;
                                                                }
                                                            }
                                                        });
                                                        // if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                                        // $scope.timeout2 = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( ออกก่อน ) </span>";
                                                        // $scope.door_out2 = arr2.door_id;
                                                        // return false;
                                                        // }
                                                    }
                                                });


                                            }
                                        } else {
                                            if (arr2.event_time >= ($scope.Date + ' ' + $scope.SetTimeBeforeCustom($scope.Date, Old_timeOut, 150)) && arr2.event_time <= _timeOut) {
                                                if (_condition) {
                                                    var CheckAllDoor = false;
                                                    angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                        if (doorIdItem.door_id == '0') {
                                                            CheckAllDoor = true;
                                                        }
                                                    })
                                                    if (CheckAllDoor) {
                                                        $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                        $scope.statusOut = '(ออกก่อน)';
                                                        $scope.door_out2 = arr2.door_id;
                                                    } else {
                                                        var CheckDoor = false;
                                                        angular.forEach(_doorId, function (doorIdItem, door_idx) {
                                                            if (arr2.door_id == doorIdItem.door_id) {
                                                                CheckDoor = true;
                                                            }
                                                        })
                                                        if (!CheckDoor) {
                                                            $scope.timeout = arr2.event_time.split(' ')[1] + "<span style=\"color:#E040FB\"> ( ออกก่อน ) </span>";
                                                            $scope.door_out = arr2.door_id;
                                                        } else {
                                                            $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                            $scope.statusOut = '(ออกก่อน)';
                                                            $scope.door_out2 = arr2.door_id;
                                                        }
                                                    }
                                                } else {
                                                    angular.forEach($scope.DoorArr, function (
                                                        doorAllArr) {
                                                        if (arr2.door_id == doorAllArr.door_id) {
                                                            var checkdooZone = true;
                                                            angular.forEach($scope.DoorArrOutDepart, function (
                                                                doorAllArrOut, idx) {
                                                                if (checkdooZone) {
                                                                    if (arr2.door_id == doorAllArrOut.door_id) {
                                                                        checkdooZone = false;
                                                                        return false;
                                                                    } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                        $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                        $scope.statusOut = '(ออกก่อน)';
                                                                        $scope.door_out2 = arr2.door_id;
                                                                        checkdooZone = false;
                                                                        return false;
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            }

                                        }
                                    }
                                });

                                // $scope.DataResult.push({'PERID':arr2.PERID,'TIME_IN':$scope.timein,'TIME_IN2':$scope.timein2,'TIME_OUT':$scope.timeout});

                                //////////////////////////////////////////


                                // angular.forEach(arr, function (value, key) {
                                //     // console.log("2"+key + ': ' + value);
                                // })

                                $scope.DataResult.push({
                                    'PERID': arr.PERID,
                                    'DEP_CODE': arr.Dep_Code,
                                    'NAME': arr.NAME,
                                    'SURNAME': arr.SURNAME,
                                    'CARD_NO': arr.card_no,
                                    'TIME_IN': $scope.timein,
                                    'TIME_IN2': $scope.timein2,
                                    'TIME_HO': $scope.timeho,
                                    'TIME_HN': $scope.timehn,
                                    'TIME_OUT': $scope.timeout,
                                    'TIME_OUT2': $scope.timeout2,
                                    'DOOR_IN': $scope.door_in,
                                    'DOOR_IN2': $scope.door_in2,
                                    'DOOR_HO': $scope.door_ho,
                                    'DOOR_HN': $scope.door_hn,
                                    'DOOR_OUT': $scope.door_out,
                                    'DOOR_OUT2': $scope.door_out2
                                });
                            }
                            $scope.CheckName = arr.PERID;
                            // console.log($scope.DataResult);
                        });

                        $scope.uniqueStandards = UniqueArraybyId($scope.DataResult,
                            "PERID");

                        function UniqueArraybyId(collection, keyname) {
                            var output = [],
                                keys = [];

                            angular.forEach(collection, function (item) {
                                var key = item[keyname];
                                if (keys.indexOf(key) === -1) {
                                    keys.push(key);
                                    output.push(item);
                                }
                            });
                            return output;
                        };

                        // $scope.uniqueStandards2 = $scope.uniqueStandards;
                        // console.log($scope.uniqueStandards);

                        // เรียกข้อมูลบุคลากรในฝ่ายเพื่อนำมาเปรียบเทียบข้อมูลเวลาที่ได้จากข้างบนพร้อมบันทึกผล

                        data = {
                            'DepCode': [{'Dep_Code': $scope.DepartSelect}],
                            'DateConrdi': $scope.Date,
                            'PERID': <?php echo $_SESSION['PERID']?>
                        };
                        $http.post('./ApiService/GetPersonDate_USER', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.PersonDepartTime = response.data;
                                var keepGoing = true;
                                $scope.statusIn = '';
                                $scope.statusOut = '';

                                angular.forEach($scope.PersonDepartTime, function (arr1, idx2) {
                                    keepGoing = true;
                                    angular.forEach($scope.uniqueStandards, function (arr_Item, idx) {
                                        if (arr_Item.PERID == arr1.PERID) {
                                            keepGoing = false;
                                        } else if (idx === $scope.uniqueStandards.length - 1 && keepGoing != false) {
                                            $scope.uniqueStandards.push({
                                                'PERID': arr1.PERID,
                                                'DEP_CODE': arr1.Dep_Code,
                                                'NAME': arr1.NAME,
                                                'SURNAME': arr1.SURNAME,
                                                'CARD_NO': '',
                                                'TIME_IN': '',
                                                'TIME_IN2': '',
                                                'TIME_HO': '',
                                                'TIME_HN': '',
                                                'TIME_OUT': '',
                                                'TIME_OUT2': '',
                                                'DOOR_IN': '',
                                                'DOOR_IN2': '',
                                                'DOOR_HO': '',
                                                'DOOR_HN': '',
                                                'DOOR_OUT': '',
                                                'DOOR_OUT2': '',
                                            });
                                        }
                                    });
                                });

                                angular.forEach($scope.PersonDepartTime, function (arr1, idx2) {
                                    keepGoing = true;
                                    if (arr1.P_Time_TimeN != null) {
                                        angular.forEach($scope.uniqueStandards, function (arr_Item, idx) {
                                            if (arr1.PERID == arr_Item.PERID) {
                                                $scope.doorInName = '';
                                                $scope.timein = '';
                                                $scope.timein2 = '';
                                                $scope.timehn = '';
                                                $scope.timeho = '';
                                                $scope.timeout = '';
                                                $scope.timeout2 = '';
                                                $scope.door_in = '';
                                                $scope.door_in2 = '';
                                                $scope.door_hn = '';
                                                $scope.door_ho = '';
                                                $scope.door_out = '';
                                                $scope.door_out2 = '';
                                                $scope.S_TimeIn = '00:00:00 - ' + arr1.P_Time_TimeN + ' (ปกติ)';
                                                $scope.S_TimeOut = arr1.P_Time_TimeO + ' - 23:59:59 (ปกติ)';

                                                angular.forEach(arr1.DoorData, function (DoorItem, Door_Idx) {
                                                    if (Door_Idx == arr1.DoorData.length - 1) {
                                                        $scope.doorInName += $scope.FindDoor(DoorItem.door_id);
                                                    } else {
                                                        $scope.doorInName += $scope.FindDoor(DoorItem.door_id) + '  ,  ';
                                                    }
                                                })

                                                //คำนวณเวลาเข้าประตูบริเวณ รพ ทั้งหมด
                                                angular.forEach($scope.ItemData, function (arr2, idx_Item) {
                                                    if (arr_Item.PERID == arr2.PERID) {

                                                        if (arr2.event_time <= $scope.Date + " " + arr1.P_Time_TimeN) {

                                                            // if (_condition) {

                                                            var CheckAllDoor = false;
                                                            angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                if (doorIdItem.door_id == '0') {
                                                                    CheckAllDoor = true;
                                                                }
                                                            })

                                                            if (CheckAllDoor) {
                                                                if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                    $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                    $scope.statusIn = '';
                                                                    $scope.door_in2 = arr2.door_id;
                                                                }
                                                            } else {
                                                                var CheckDoor = false;
                                                                angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                    if (arr2.door_id == doorIdItem.door_id) {
                                                                        CheckDoor = true;
                                                                    }
                                                                })
                                                                if (!CheckDoor) {
                                                                    var checkdooZone = true;
                                                                    angular.forEach($scope.DoorArrOutDepart, function (
                                                                        doorAllArrOut, idx) {
                                                                        if (checkdooZone) {
                                                                            if (arr2.door_id == doorAllArrOut.door_id) {
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                                if ($scope.timein == null || $scope.timein == '') {
                                                                                    $scope.timein = arr2.event_time.split(' ')[1];
                                                                                    $scope.door_in = arr2.door_id;
                                                                                }
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            }
                                                                        }
                                                                    });
                                                                    // if ($scope.timein == null || $scope.timein == '') {
                                                                    //     $scope.timein = arr2.event_time.split(' ')[1] + " <br /> <span style=\"color:#00E5FF\">" +
                                                                    //         arr1.P_Time_TimeN.split(':')[0] + ':' + arr1.P_Time_TimeN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[0] +
                                                                    //         ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                                    //     $scope.door_in = arr2.door_id;
                                                                    // }
                                                                } else {
                                                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                        $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                        $scope.statusIn = '';
                                                                        $scope.door_in2 = arr2.door_id;
                                                                    }
                                                                }
                                                            }
                                                            // } else {
                                                            //     angular.forEach($scope.DoorArr, function (
                                                            //         doorAllArr, idx) {
                                                            //         if (arr2.door_id == doorAllArr.door_id) {
                                                            //             if ($scope.timein2 == null || $scope.timein2 == '') {
                                                            //                 $scope.timein2 = arr2.event_time.split(' ')[1] + " <br /> <span style=\"color:#00E5FF\">" +
                                                            //                     arr1.P_Time_TimeN.split(':')[0] + ':' + arr1.P_Time_TimeN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[0] +
                                                            //                     ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                            //                 $scope.door_in2 = arr2.door_id;
                                                            //                 return false;
                                                            //             }
                                                            //         }
                                                            //     });

                                                            // angular.forEach($scope.DoorArrOutDepart, function (
                                                            //     doorAllArrOut, idx) {
                                                            //     if (arr2.door_id == doorAllArrOut.door_id) {
                                                            //         if ($scope.timein == null || $scope.timein == '') {
                                                            //             $scope.timein = arr2.event_time.split(' ')[1] + " <br /> <span style=\"color:#00E5FF\">" +
                                                            //                 arr1.P_Time_TimeN.split(':')[0] + ':' + arr1.P_Time_TimeN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[0] +
                                                            //                 ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                            //             $scope.door_in = arr2.door_id;
                                                            //             return false;
                                                            //         }
                                                            //     }
                                                            // });
                                                            // }
                                                        } else if (arr2.event_time > $scope.Date + " " + arr1.P_Time_TimeN &&
                                                            arr2.event_time <= $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN)) {
                                                            // var DateHo = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[0];
                                                            // var TimeHo = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO).split(' ')[1];
                                                            // var Hour = TimeHo.split(':')[0];
                                                            // var Minute = TimeHo.split(':')[1];
                                                            // var Sec = TimeHo.split(':')[2];
                                                            // Hour = parseInt(Hour) - 1;
                                                            // var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                                            // if (arr2.event_time <= DateHo + " " + newDateTime) {
                                                            // if (_condition) {
                                                            var CheckAllDoor = false;
                                                            angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                if (doorIdItem.door_id == '0') {
                                                                    CheckAllDoor = true;
                                                                }
                                                            })
                                                            if (CheckAllDoor) {
                                                                if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                    $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                    $scope.statusIn = '';
                                                                    $scope.door_in2 = arr2.door_id;
                                                                }
                                                            } else {
                                                                var CheckDoor = false;
                                                                angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                    if (arr2.door_id == doorIdItem.door_id) {
                                                                        CheckDoor = true;
                                                                    }
                                                                })
                                                                if (!CheckDoor) {
                                                                    var checkdooZone = true;
                                                                    angular.forEach($scope.DoorArrOutDepart, function (
                                                                        doorAllArrOut, idx) {
                                                                        if (checkdooZone) {
                                                                            if (arr2.door_id == doorAllArrOut.door_id) {
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                                if ($scope.timein == null || $scope.timein == '') {
                                                                                    $scope.timein = arr2.event_time.split(' ')[1] + " (สาย) ";
                                                                                    $scope.door_in = arr2.door_id;
                                                                                }
                                                                                if ($scope.timein2 != null && $scope.timein2 != '') {
                                                                                    $scope.timein = arr2.event_time.split(' ')[1];
                                                                                    $scope.door_in = arr2.door_id;
                                                                                }
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            }
                                                                        }
                                                                    });
                                                                    // if ($scope.timein == null || $scope.timein == '') {
                                                                    //     $scope.timein = arr2.event_time.split(' ')[1] + " <span style=\"color:#E040FB\"> (สาย) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                                    //         arr1.P_Time_TimeN.split(':')[0] + ':' + arr1.P_Time_TimeN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[0] +
                                                                    //         ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                                    //     $scope.door_in = arr2.door_id;
                                                                    // }
                                                                } else {
                                                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                        $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                        $scope.statusIn = '(สาย)';
                                                                        $scope.door_in2 = arr2.door_id;
                                                                    }
                                                                    if ($scope.timein != null || $scope.timein != '') {
                                                                        $scope.timein = $scope.timein.replace(' <span style=\"color:#E040FB\"> (สาย) </span>', '');
                                                                    }
                                                                }
                                                            }
                                                            // } else {
                                                            //     angular.forEach($scope.DoorArr, function (
                                                            //         doorAllArr, idx) {
                                                            //         if (arr2.door_id == doorAllArr.door_id) {
                                                            //             if (arr1.PERID == '46269') {
                                                            //                 console.log(arr1);
                                                            //                 console.log(arr2);
                                                            //                 console.log(arr1.PERID + ' ' + arr2.door_id + ' ' + doorAllArr.door_id);
                                                            //             }
                                                            //             if ($scope.timein2 == null || $scope.timein2 == '') {
                                                            //                 $scope.timein2 = arr2.event_time.split(' ')[1] + " <span style=\"color:#E040FB\"> (สาย) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                            //                     arr1.P_Time_TimeN.split(':')[0] + ':' + arr1.P_Time_TimeN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[0] +
                                                            //                     ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                            //                 $scope.door_in2 = arr2.door_id;
                                                            //                 return false;
                                                            //             }
                                                            //         }
                                                            //     });

                                                            // angular.forEach($scope.DoorArrOutDepart, function (
                                                            //     doorAllArrOut, idx) {
                                                            //     if (arr2.door_id == doorAllArrOut.door_id) {
                                                            //         if ($scope.timein == null || $scope.timein == '') {
                                                            //             $scope.timein = arr2.event_time.split(' ')[1] + " <span style=\"color:#E040FB\"> (สาย) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                            //                 arr1.P_Time_TimeN.split(':')[0] + ':' + arr1.P_Time_TimeN.split(':')[1] + ' น. - ' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[0] +
                                                            //                 ':' + ($scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN).split(' ')[1]).split(':')[1] + " น.</span>";
                                                            //             $scope.door_in = arr2.door_id;
                                                            //             return false;
                                                            //         }
                                                            //     }
                                                            // });
                                                            // }
                                                            // }
                                                        } else {

                                                        }
                                                    }
                                                    if (idx_Item === $scope.ItemData.length - 1) {
                                                        arr_Item.TIME_IN = $scope.timein;
                                                        arr_Item.DOOR_IN = $scope.door_in;
                                                    }
                                                    if (idx_Item === $scope.ItemData.length - 1) {
                                                        arr_Item.TIME_IN2 = $scope.timein2;
                                                        arr_Item.DOOR_IN2 = $scope.door_in2;
                                                    }
                                                });
                                                //////////////////////////////////////////

                                                //คำนวณเวลาออก (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                                                angular.forEach($scope.ItemData, function (arr2, idx_Item) {
                                                    if (arr_Item.PERID == arr2.PERID) {
                                                        if (($scope.timein != null && $scope.timein != '') || ($scope.timein2 != null && $scope.timein2 != '')) {
                                                            if (($scope.timein2 != null && $scope.timein2 == '')) {
                                                                if (arr2.event_time < $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN)) {
                                                                    var CheckAllDoor = false;
                                                                    angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                        if (doorIdItem.door_id == '0') {
                                                                            CheckAllDoor = true;
                                                                        }
                                                                    })
                                                                    if (CheckAllDoor) {
                                                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                            $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                            $scope.statusIn = '(สาย)';
                                                                            $scope.door_in2 = arr2.door_id;
                                                                            arr_Item.TIME_IN2 = $scope.timein2;
                                                                            arr_Item.DOOR_IN2 = $scope.door_in2;
                                                                        }
                                                                    } else {
                                                                        var CheckDoor = false;
                                                                        angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                            if (arr2.door_id == doorIdItem.door_id) {
                                                                                CheckDoor = true;
                                                                            }
                                                                        })
                                                                        if (!CheckDoor) {
                                                                            var checkdooZone = true;
                                                                            angular.forEach($scope.DoorArrOutDepart, function (
                                                                                doorAllArrOut, idx) {
                                                                                if (checkdooZone) {
                                                                                    if (arr2.door_id == doorAllArrOut.door_id) {
                                                                                        checkdooZone = false;
                                                                                        return false;
                                                                                    } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                                        if ($scope.timein == null || $scope.timein == '') {
                                                                                            $scope.timein = arr2.event_time.split(' ')[1] + " ( สาย ) ";
                                                                                            $scope.door_in = arr2.door_id;
                                                                                            arr_Item.TIME_IN = $scope.timein;
                                                                                            arr_Item.DOOR_IN = $scope.door_in;
                                                                                        }
                                                                                        if ($scope.timein2 != null && $scope.timein2 != '') {
                                                                                            $scope.timein = arr2.event_time.split(' ')[1];
                                                                                            $scope.door_in = arr2.door_id;
                                                                                            arr_Item.TIME_IN = $scope.timein;
                                                                                            arr_Item.DOOR_IN = $scope.door_in;
                                                                                        }
                                                                                        checkdooZone = false;
                                                                                        return false;
                                                                                    }
                                                                                }

                                                                            });
                                                                        } else {
                                                                            if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                                $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                                $scope.statusIn = '(สาย)';
                                                                                $scope.door_in2 = arr2.door_id;
                                                                                arr_Item.TIME_IN2 = $scope.timein2;
                                                                                arr_Item.DOOR_IN2 = $scope.door_in2;
                                                                            }
                                                                            if ($scope.timein != null || $scope.timein != '') {
                                                                                $scope.timein = $scope.timein.replace('<span style=\"color:#E040FB\"> ( สาย ) </span>', '');
                                                                                arr_Item.TIME_IN = $scope.timein;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                if (arr2.event_time >= $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO) && arr2.event_time <= $scope.Date + " " + arr1.P_Time_TimeHN) {
                                                                    if ($scope.timeho == null || $scope.timeho == '') {
                                                                        $scope.timeho = arr2.event_time.split(' ')[1];
                                                                        $scope.door_ho = arr2.door_id;
                                                                        arr_Item.TIME_HO = $scope.timeho;
                                                                        arr_Item.DOOR_HO = $scope.door_ho;
                                                                    }
                                                                } else if (arr2.event_time >= $scope.SetTimeBeforeHour($scope.Date, arr1.P_Time_TimeHO) && arr2.event_time < $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO)) {
                                                                    if ($scope.timeho == null || $scope.timeho == '') {
                                                                        $scope.timeho = arr2.event_time.split(' ')[1] + " (ออกก่อน) ";
                                                                        $scope.door_ho = arr2.door_id;
                                                                        arr_Item.TIME_HO = $scope.timeho;
                                                                        arr_Item.DOOR_HO = $scope.door_ho;
                                                                    }
                                                                } else {
                                                                    arr_Item.TIME_HO = '';
                                                                    arr_Item.DOOR_HO = '';
                                                                }
                                                            }

                                                        } else {
                                                            if (arr2.event_time < $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN)) {
                                                                var CheckAllDoor = false;
                                                                angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                    if (doorIdItem.door_id == '0') {
                                                                        CheckAllDoor = true;
                                                                    }
                                                                })
                                                                if (CheckAllDoor) {
                                                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                        $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                        $scope.statusIn = '(สาย)';
                                                                        $scope.door_in2 = arr2.door_id;
                                                                        arr_Item.TIME_IN2 = $scope.timein2;
                                                                        arr_Item.DOOR_IN2 = $scope.door_in2;
                                                                    }
                                                                } else {
                                                                    var CheckDoor = false;
                                                                    angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                        if (arr2.door_id == doorIdItem.door_id) {
                                                                            CheckDoor = true;
                                                                        }
                                                                    })
                                                                    if (!CheckDoor) {
                                                                        var checkdooZone = true;
                                                                        angular.forEach($scope.DoorArrOutDepart, function (
                                                                            doorAllArrOut, idx) {
                                                                            if (checkdooZone) {
                                                                                if (arr2.door_id == doorAllArrOut.door_id) {
                                                                                    checkdooZone = false;
                                                                                    return false;
                                                                                } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                                    if ($scope.timein == null || $scope.timein == '') {
                                                                                        $scope.timein = arr2.event_time.split(' ')[1] + " ( สาย ) ";
                                                                                        $scope.door_in = arr2.door_id;
                                                                                        arr_Item.TIME_IN = $scope.timein;
                                                                                        arr_Item.DOOR_IN = $scope.door_in;
                                                                                    }
                                                                                    checkdooZone = false;
                                                                                    return false;
                                                                                }
                                                                            }
                                                                        });
                                                                    } else {
                                                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                                                            $scope.timein2 = arr2.event_time.split(' ')[1];
                                                                            $scope.statusIn = '(สาย)';
                                                                            $scope.door_in2 = arr2.door_id;
                                                                            arr_Item.TIME_IN2 = $scope.timein2;
                                                                            arr_Item.DOOR_IN2 = $scope.door_in2;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                });
                                                //////////////////////////////////////////

                                                //คำนวณเวลาเข้า (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                                                angular.forEach($scope.ItemData, function (arr2, idx_Item) {
                                                    if (arr_Item.PERID == arr2.PERID) {
                                                        if (arr2.event_time > $scope.Date + " " + arr1.P_Time_TimeHN &&
                                                            arr2.event_time <= $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN)) {
                                                            if ($scope.timehn == null || $scope.timehn == '') {
                                                                $scope.timehn = arr2.event_time.split(' ')[1];
                                                                $scope.door_hn = arr2.door_id;
                                                            }
                                                        } else if (arr2.event_time > $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN) && arr2.event_time <= $scope.SetTimeLateHours($scope.Date, arr1.P_Time_TimeHN)) {
                                                            // var DateHn = $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[0];
                                                            // var TimeHn = $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN).split(' ')[1];
                                                            // var Hour = TimeHn.split(':')[0];
                                                            // var Minute = TimeHn.split(':')[1];
                                                            // var Sec = TimeHn.split(':')[2];
                                                            // Hour = parseInt(Hour) + 1;
                                                            // var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                                            // if (arr2.event_time > $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN) &&
                                                            //     arr2.event_time <= DateHn + " " + newDateTime) {
                                                            if ($scope.timehn == null || $scope.timehn == '') {
                                                                $scope.timehn = arr2.event_time.split(' ')[1] + " (สาย) ";
                                                                $scope.door_hn = arr2.door_id;
                                                            }
                                                            // }
                                                        }
                                                    }
                                                    if (idx_Item === $scope.ItemData.length - 1) {
                                                        arr_Item.TIME_HN = $scope.timehn;
                                                        arr_Item.DOOR_HN = $scope.door_ho;
                                                    }
                                                });
                                                //////////////////////////////////////////

                                                //คำนวณเวลาออกประตูบริเวณ รพ ทั้งหมด
                                                angular.forEach($scope.ItemData, function (arr2, idx_Item) {
                                                    if (arr_Item.PERID == arr2.PERID) {

                                                        if (arr2.event_time >= $scope.Date + " " + arr1.P_Time_TimeO &&
                                                            arr2.event_time <= $scope.Date + " 24:59:59"
                                                        ) {
                                                            // if (_condition) {
                                                            var CheckAllDoor = false;
                                                            angular.forEach(arr1.DoorData, function (door_item, idx_door) {
                                                                if (door_item.door_id == '0') {
                                                                    CheckAllDoor = true;
                                                                }
                                                            });
                                                            if (CheckAllDoor) {
                                                                $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                $scope.statusOut = '';
                                                                $scope.door_out2 = arr2.door_id;
                                                            } else {
                                                                var CheckDoor = false;
                                                                angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                    if (arr2.door_id == doorIdItem.door_id) {
                                                                        CheckDoor = true;
                                                                    }
                                                                })
                                                                if (!CheckDoor) {
                                                                    var checkdooZone = true;
                                                                    angular.forEach($scope.DoorArrOutDepart, function (
                                                                        doorAllArrOut, idx) {
                                                                        if (checkdooZone) {
                                                                            if (arr2.door_id == doorAllArrOut.door_id) {
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                                $scope.timeout = arr2.event_time.split(' ')[1];
                                                                                $scope.door_out = arr2.door_id;
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            }
                                                                        }
                                                                    });
                                                                    // if ($scope.timeout == null || $scope.timeout == '') {
                                                                    // $scope.timeout = arr2.event_time.split(' ')[1] + " <br /> <span style=\"color:#00E5FF\">" +
                                                                    //     ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[0] +
                                                                    //     ':' + ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[1] +
                                                                    //     ' น. - ' + arr1.P_Time_TimeO.split(':')[0] + ':' + arr1.P_Time_TimeO.split(':')[1] + " น.</span>";
                                                                    // $scope.door_out = arr2.door_id;
                                                                    // }
                                                                } else {
                                                                    // if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                                                    $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                    $scope.statusOut = '';
                                                                    $scope.door_out2 = arr2.door_id;
                                                                    // }
                                                                }
                                                            }
                                                            // } else {
                                                            //     angular.forEach($scope.DoorArr, function (
                                                            //         doorAllArr) {
                                                            //         if (arr2.door_id == doorAllArr.door_id) {
                                                            //             // if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                                            //             $scope.timeout2 = arr2.event_time.split(' ')[1] + " <br /> <span style=\"color:#00E5FF\">" +
                                                            //                 ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[0] +
                                                            //                 ':' + ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[1] +
                                                            //                 ' น. - ' + arr1.P_Time_TimeO.split(':')[0] + ':' + arr1.P_Time_TimeO.split(':')[1] + " น.</span>";
                                                            //             $scope.door_out2 = arr2.door_id;
                                                            //             return false;
                                                            //             // }
                                                            //         }
                                                            //     });


                                                            // angular.forEach($scope.DoorArrOutDepart, function (
                                                            //     doorAllArrOut, idx) {
                                                            //     if (arr2.door_id == doorAllArrOut.door_id) {
                                                            //         if ($scope.timeout == null || $scope.timeout == '') {
                                                            //             $scope.timeout = arr2.event_time.split(' ')[1] + " <br /> <span style=\"color:#00E5FF\">" +
                                                            //                 ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[0] +
                                                            //                 ':' + ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[1] +
                                                            //                 ' น. - ' + arr1.P_Time_TimeO.split(':')[0] + ':' + arr1.P_Time_TimeO.split(':')[1] + " น.</span>";
                                                            //             $scope.door_out = arr2.door_id;
                                                            //             return false;
                                                            //         }
                                                            //     }
                                                            // });
                                                            // }

                                                        } else if (arr2.event_time >= $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO) && arr2.event_time < $scope.Date + " " + arr1.P_Time_TimeO) {
                                                            // var DateO = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[0];
                                                            // var TimeO = $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1];
                                                            // var Hour = TimeO.split(':')[0];
                                                            // var Minute = TimeO.split(':')[1];
                                                            // var Sec = TimeO.split(':')[2];
                                                            // Hour = parseInt(Hour) - 1;
                                                            // var newDateTime = Hour + ":" + Minute + ":" + Sec;

                                                            // if (arr2.event_time >= DateO + " " + newDateTime) {
                                                            // if (_condition) {
                                                            var CheckAllDoor = false;
                                                            angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                if (doorIdItem.door_id == '0') {
                                                                    CheckAllDoor = true;
                                                                }
                                                            })
                                                            if (CheckAllDoor) {
                                                                $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                $scope.statusOut = '(ออกก่อน)';
                                                                $scope.door_out2 = arr2.door_id;
                                                            } else {
                                                                var CheckDoor = false;
                                                                angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                    if (arr2.door_id == doorIdItem.door_id) {
                                                                        CheckDoor = true;
                                                                    }
                                                                })
                                                                if (!CheckDoor) {
                                                                    var checkdooZone = true;
                                                                    angular.forEach($scope.DoorArrOutDepart, function (
                                                                        doorAllArrOut, idx) {
                                                                        if (checkdooZone) {
                                                                            if (arr2.door_id == doorAllArrOut.door_id) {
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                                $scope.timeout = arr2.event_time.split(' ')[1] + " (ออกก่อน) ";
                                                                                $scope.door_out = arr2.door_id;
                                                                                checkdooZone = false;
                                                                                return false;
                                                                            }
                                                                        }
                                                                    });
                                                                    // if ($scope.timeout == null || $scope.timeout == '') {
                                                                    // $scope.timeout = arr2.event_time.split(' ')[1] + " <span style=\"color:#E040FB\"> (ออกก่อน) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                                    //     ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[0] +
                                                                    //     ':' + ($scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO).split(' ')[1]).split(':')[1] +
                                                                    //     ' น. - ' + arr1.P_Time_TimeO.split(':')[0] + ':' + arr1.P_Time_TimeO.split(':')[1] + " น.</span>";
                                                                    // $scope.door_out = arr2.door_id;
                                                                    // }
                                                                } else {
                                                                    // if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                                                    $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                    $scope.statusOut = '(ออกก่อน)';
                                                                    $scope.door_out2 = arr2.door_id;
                                                                    // }
                                                                }
                                                            }
                                                        } else {
                                                            if (arr2.event_time >= ($scope.Date + ' ' + $scope.SetTimeBeforeCustom($scope.Date, Old_timeOut, 150)) && arr2.event_time <= _timeOut) {
                                                                var CheckAllDoor = false;
                                                                angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                    if (doorIdItem.door_id == '0') {
                                                                        CheckAllDoor = true;
                                                                    }
                                                                })
                                                                if (CheckAllDoor) {
                                                                    $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                    $scope.statusOut = '(ออกก่อน)';
                                                                    $scope.door_out2 = arr2.door_id;
                                                                } else {
                                                                    var CheckDoor = false;
                                                                    angular.forEach(arr1.DoorData, function (doorIdItem, door_idx) {
                                                                        if (arr2.door_id == doorIdItem.door_id) {
                                                                            CheckDoor = true;
                                                                        }
                                                                    })
                                                                    if (!CheckDoor) {
                                                                        var checkdooZone = true;
                                                                        angular.forEach($scope.DoorArrOutDepart, function (
                                                                            doorAllArrOut, idx) {
                                                                            if (checkdooZone) {
                                                                                if (arr2.door_id == doorAllArrOut.door_id) {
                                                                                    checkdooZone = false;
                                                                                    return false;
                                                                                } else if (idx === $scope.DoorArrOutDepart.length - 1) {
                                                                                    $scope.timeout = arr2.event_time.split(' ')[1] + " (ออกก่อน) ";
                                                                                    $scope.door_out = arr2.door_id;
                                                                                    checkdooZone = false;
                                                                                    return false;
                                                                                }
                                                                            }
                                                                        });
                                                                    } else {
                                                                        $scope.timeout2 = arr2.event_time.split(' ')[1];
                                                                        $scope.statusOut = '(ออกก่อน)';
                                                                        $scope.door_out2 = arr2.door_id;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (idx_Item === $scope.ItemData.length - 1) {
                                                        arr_Item.TIME_OUT = $scope.timeout;
                                                        arr_Item.DOOR_OUT = $scope.door_out;
                                                    }
                                                    if (idx_Item === $scope.ItemData.length - 1) {
                                                        arr_Item.TIME_OUT2 = $scope.timeout2;
                                                        arr_Item.DOOR_OUT2 = $scope.door_out2;
                                                    }
                                                });

                                                //////////////////////////////////////////
                                            }
                                        });
                                    } else {
                                        $scope.doorInName = 'ประตูทั้งหมด';
                                    }
                                });

                                angular.forEach($scope.uniqueStandards, function (Del_Date) {
                                    if (Del_Date.TIME_IN != '') {
                                        Del_Date.TIME_IN = Del_Date.TIME_IN.replace($scope.Date, '');
                                    }
                                    if (Del_Date.TIME_IN2 != '') {
                                        Del_Date.TIME_IN2 = Del_Date.TIME_IN2.replace($scope.Date, '');
                                    }
                                    if (Del_Date.TIME_HO != '') {
                                        Del_Date.TIME_HO = Del_Date.TIME_HO.replace($scope.Date, '');
                                    }
                                    if (Del_Date.TIME_HN != '') {
                                        Del_Date.TIME_HN = Del_Date.TIME_HN.replace($scope.Date, '');
                                    }
                                    if (Del_Date.TIME_OUT != '') {
                                        Del_Date.TIME_OUT = Del_Date.TIME_OUT.replace($scope.Date, '');
                                    }
                                    if (Del_Date.TIME_IN2 != '') {
                                        Del_Date.TIME_OUT2 = Del_Date.TIME_OUT2.replace($scope.Date, '');
                                    }
                                })

                                data = {
                                    'DepCode': $scope.DepartSelect,
                                    'DateConrdi': $scope.Date,
                                    'PERID': <?php echo $_SESSION['PERID']?>
                                };

                                $http.post('./ApiService/GetLeavePerson_USER', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                    .then(function successCallback(response) {
                                        $scope.LeaveArrData = response.data;
                                        if($scope.LeaveArrData.length > 0) {
                                            angular.forEach($scope.LeaveArrData, function (leaveItem) {
                                                angular.forEach($scope.uniqueStandards, function (dataItem) {
                                                    if (leaveItem.PERID == dataItem.PERID) {
                                                        if (leaveItem.T_Leave_Date_Start == $scope.Date) {
                                                            if (leaveItem.T_Day_Type_Start == "เต็มวัน") {
                                                                dataItem.TIME_IN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_IN2 = leaveItem.Leave_Detail + " (เต็มวัน)";
                                                                dataItem.TIME_HO = leaveItem.Leave_Detail;
                                                                dataItem.TIME_HN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT2 = leaveItem.Leave_Detail + " (เต็มวัน)";
                                                            } else if (leaveItem.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                dataItem.TIME_IN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_IN2 = leaveItem.Leave_Detail + ' ครึ่งวัน (เช้า)';
                                                                dataItem.TIME_HO = leaveItem.Leave_Detail;
                                                            } else {
                                                                dataItem.TIME_HN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT2 = leaveItem.Leave_Detail + ' ครึ่งวัน (บ่าย)';
                                                            }
                                                        } else if (leaveItem.T_Leave_Date_End == $scope.Date) {
                                                            if (leaveItem.T_Day_Type_End == "เต็มวัน") {
                                                                dataItem.TIME_IN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_IN2 = leaveItem.Leave_Detail + " (เต็มวัน)";
                                                                dataItem.TIME_HO = leaveItem.Leave_Detail;
                                                                dataItem.TIME_HN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT2 = leaveItem.Leave_Detail + " (เต็มวัน)";
                                                            } else if (leaveItem.T_Day_Type_End == "ครึ่งวัน (เช้า)") {
                                                                dataItem.TIME_IN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_IN2 = leaveItem.Leave_Detail + ' ครึ่งวัน (เช้า)';
                                                                dataItem.TIME_HO = leaveItem.Leave_Detail;
                                                            } else {
                                                                dataItem.TIME_HN = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT = leaveItem.Leave_Detail;
                                                                dataItem.TIME_OUT2 = leaveItem.Leave_Detail + ' ครึ่งวัน (บ่าย)';
                                                            }
                                                        } else if (leaveItem.T_Leave_Date_Start <= $scope.Date && leaveItem.T_Leave_Date_End >= $scope.Date) {
                                                            dataItem.TIME_IN = leaveItem.Leave_Detail;
                                                            dataItem.TIME_IN2 = leaveItem.Leave_Detail;
                                                            dataItem.TIME_HO = leaveItem.Leave_Detail;
                                                            dataItem.TIME_HN = leaveItem.Leave_Detail;
                                                            dataItem.TIME_OUT = leaveItem.Leave_Detail;
                                                            dataItem.TIME_OUT2 = leaveItem.Leave_Detail;
                                                        } else {

                                                        }
                                                    }
                                                });
                                            });
                                        } else {
                                            if($scope.EditTime.length > 0) {
                                                $scope.EditTime.map(itemEdit => {
                                                    $scope.uniqueStandards.map(itemUni => {
                                                        if(itemEdit.PERID == itemUni.PERID) {
                                                            if(itemEdit.status == '1') {
                                                                if(itemEdit.time_in != null) {
                                                                    itemUni.TIME_IN2 = itemEdit.time_in.split(' ')[1]
                                                                }
                                                                if(itemEdit.time_out != null) {
                                                                    itemUni.TIME_OUT2 = itemEdit.time_out.split(' ')[1]
                                                                }
                                                            }
                                                        }
                                                    })
                                                })
                                            }
                                        }
                                        $scope.uniqueStandards2 = $scope.uniqueStandards;
                                        $scope.Loading = false;
                                    });


                                // $scope.uniqueStandards2 = $scope.uniqueStandards;
                                // $scope.Loading = false;
                                // $scope.uniqueStandards2 = $scope.uniqueStandards;
                                // $scope.Loading = false;
                            });
                        ////////////////////////////////////////////////////////////
                        // console.log('1');
                        // console.log($scope.uniqueStandards);
                        // $scope.uniqueStandards2 = $scope.uniqueStandards;
                        // $scope.$apply();
                        // $window.location.reload();
                    } else {
                        data = {
                            'DepCode': $scope.DepartSelect,
                            'DateConrdi': $scope.Date,
                            'PERID': <?php echo $_SESSION['PERID']?>
                        };
                        $http.post('./ApiService/GetPersonDate_USER', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.PersonDepartTime = response.data;
                                $scope.uniqueStandards = [];
                                $scope.uniqueStandards2 = [];
                                angular.forEach($scope.PersonDepartTime, function (arr1, idx2) {
                                    $scope.uniqueStandards.push({
                                        'PERID': arr1.PERID,
                                        'DEP_CODE': arr1.Dep_Code,
                                        'NAME': arr1.NAME,
                                        'SURNAME': arr1.SURNAME,
                                        'CARD_NO': '',
                                        'TIME_IN': '',
                                        'TIME_IN2': '',
                                        'TIME_HO': '',
                                        'TIME_HN': '',
                                        'TIME_OUT': '',
                                        'TIME_OUT2': '',
                                        'DOOR_IN': '',
                                        'DOOR_IN2': '',
                                        'DOOR_HO': '',
                                        'DOOR_HN': '',
                                        'DOOR_OUT': '',
                                        'DOOR_OUT2': '',
                                    });
                                });
                                $scope.uniqueStandards2 = $scope.uniqueStandards;
                                $scope.Loading = false;
                            });
                    }
                });

        }
    })
    ;

    app.controller('ModalInstanceCtrl', ['$scope', '$http', '$sce', '$uibModalInstance', 'params', function ($scope, $http, $sce, $uibModalInstance, params) {
        $scope.Modalname = params.item_D.NAME + " " + params.item_D.SURNAME.split('<br>')[0];
        $scope.Date_Person = params.Date;
        $scope.EditDataPerson = false;
        $scope.DoorArr = params.ObjDoorArr;
        $scope.DataPersonTime = [];
        $scope.uniqueStandards2 = [];
        $scope.statusAdmin = params.CondiStatus;
        var timein = '';
        var timein2 = '';
        var timeho = '';
        var timehn = '';
        var timeout = '';
        var timeout2 = '';

        $scope.SetTimeBefore = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() - 30);
            return d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeBeforeHour = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() - 60);
            return d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeBeforeCustom = function (DateT, Time, Minute) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() - Minute);
            return d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeLate = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setMinutes(d1.getMinutes() + 30);
            return d1.toLocaleTimeString('en-GB')
        }

        $scope.SetTimeLateHours = function (DateT, Time) {
            var dateString = DateT + " " + Time;

            var d1 = new Date(dateString.replace(/-/g, '/'));
            d1.setHours(d1.getHours() + 1);
            return d1.toLocaleTimeString('en-GB')
        }

        $scope.GetCurrentTime = function () {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return today = yyyy + '-' + mm + '-' + dd;
        }

        $scope.UpdateAbsenceData = function () {
            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "ต้องการลงบันทึกเวลาขาดงาน คุณ : " + params.item_D.NAME + " " + params.item_D.SURNAME + " ใช่หรือไม่",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {
                    data = {
                        'No_Id': params.item_D.No_Id,
                        'PERID': <?php echo $_SESSION['PERID']?>
                    };
                    $http.post('./ApiService/UpdateAbsence', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            var result = response.data;
                            if (result.Status) {
                                Swal({
                                    type: 'success',
                                    title: result.Message
                                });
                                $uibModalInstance.close(result.Status);
                            } else {
                                Swal({
                                    type: 'error',
                                    title: result.Message
                                });
                                $uibModalInstance.close();
                            }
                        });
                }
            });
        }

        $scope.UpdateApproveData = function () {

            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "กรุณาตรวจสอบข้อมูลให้ครบถ้วนก่อนแก้ไขข้อมูล",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {
                    $scope.DateSelect = params.Date;
                    $scope.Day = $scope.DateSelect.split('/')[1];
                    $scope.Month = $scope.DateSelect.split('/')[0];
                    $scope.Year = $scope.DateSelect.split('/')[2];
                    $scope.Date = $scope.Year + "-" + $scope.Month + "-" +
                        $scope.Day;
                    var CurDate = $scope.GetCurrentTime();
                    $scope.DataPersonTime = [];

                    data = {
                        'DateStart': CurDate,
                        'DateEnd': $scope.Date
                    };
                    $http.post('./ApiService/CalDateDiff', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            if (response.data[0].DateDiff > 30) {
                                Swal({
                                    type: 'error',
                                    title: 'พบข้อผิดพลาด',
                                    text: 'ไม่สามารถแก้ไขข้อมูลย้อนหลังได้เกิน 30 วัน'
                                })
                            } else {
                                if (document.getElementById("E_TimeIN2").value != null && document.getElementById("E_TimeIN2").value != '') {
                                    timein2 = document.getElementById("E_TimeIN2").value + ":00";
                                    if (document.getElementById("E_TimeIN2").value.split(":")[0].length == 1 && document.getElementById("E_TimeIN2").value.split(":")[0] < 10) {
                                        timein2 = "0" + timein2;
                                    }
                                }
                                if (document.getElementById("E_TimeHO").value != null && document.getElementById("E_TimeHO").value != '') {
                                    timeho = document.getElementById("E_TimeHO").value + ":00";
                                    if (document.getElementById("E_TimeHO").value.split(":")[0].length == 1 && document.getElementById("E_TimeHO").value.split(":")[0] < 10) {
                                        timeho = "0" + timeho;
                                    }
                                }
                                if (document.getElementById("E_TimeHN").value != null && document.getElementById("E_TimeHN").value != '') {
                                    timehn = document.getElementById("E_TimeHN").value + ":00";
                                    if (document.getElementById("E_TimeHN").value.split(":")[0].length == 1 && document.getElementById("E_TimeHN").value.split(":")[0] < 10) {
                                        timehn = "0" + timehn;
                                    }
                                }
                                if (document.getElementById("E_TimeOUT2").value != null && document.getElementById("E_TimeOUT2").value != '') {
                                    timeout2 = document.getElementById("E_TimeOUT2").value + ":00";
                                    if (document.getElementById("E_TimeOUT2").value.split(":")[0].length == 1 && document.getElementById("E_TimeOUT2").value.split(":")[0] < 10) {
                                        timeout2 = "0" + timeout2;
                                    }
                                }

                                $scope.DataPersonTime.push({
                                    'PERID': params.item_D.PERID,
                                    'DEP_CODE': params.item_D.Dep_Code,
                                    'NAME': params.item_D.NAME,
                                    'SURNAME': params.item_D.SURNAME,
                                    'TIME_IN2': timein2,
                                    'TIME_HO': timeho,
                                    'TIME_HN': timehn,
                                    'TIME_OUT2': timeout2,
                                    'DOOR_IN2': $scope.DoorS_Timein2,
                                    'DOOR_HO': $scope.DoorS_Timeho,
                                    'DOOR_HN': $scope.DoorS_Timehn,
                                    'DOOR_OUT2': $scope.DoorS_Timeout2,
                                });

                                $scope.Loading = true;
                                // $scope.DateSelect = params.Date;
                                // $scope.Day = $scope.DateSelect.split('/')[1];
                                // $scope.Month = $scope.DateSelect.split('/')[0];
                                // $scope.Year = $scope.DateSelect.split('/')[2];
                                // $scope.Date = $scope.Year + "-" + $scope.Month + "-" +
                                //     $scope.Day;
                                data = {
                                    'DepCode': params.item_D.Dep_Code,
                                    'DateConrdi': $scope.Date
                                };
                                $http.post('./ApiService/GetTimeSetDep', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                    .then(function successCallback(response) {
                                        var timesetDep = response.data;
                                        // $scope.uniqueStandards2 = [];
                                        if (timesetDep.length != 0) {
                                            $scope.DataSuccess = true;
                                            manageTime(timesetDep[0].D_Time_TimeN, timesetDep[0].D_Time_TimeO, timesetDep[0].D_Time_TimeHO, timesetDep[0].D_Time_TimeHN, timesetDep[0].door_id, true);
                                        } else {
                                            $scope.DataSuccess = true;
                                            manageTime("08:30:00", "16:30:00", "12:00:00", "12:30:00", $scope.DoorArr, false);
                                        }
                                    });
                            }
                        });
                }
            });
        }

        $scope.FindDoor = function (door_idData) {
            var doorname = '';
            angular.forEach(params.ObjDoorArr, function (Door_A) {
                if (Door_A.door_id == door_idData) {
                    doorname = "" + Door_A.door_name;
                }
            });

            return doorname;
        }

        $scope.htmlTrusted = function (html) {
            return $sce.trustAsHtml(html);
        }

        $scope.ApproveData = function () {
            data = {
                'PERID': params.item_D.PERID,
                'DEPCODE': params.item_D.Dep_Code,
                'TIME_IN': params.item_D.TIME_IN,
                'TIME_IN2': params.item_D.TIME_IN2,
                'TIME_HO': params.item_D.TIME_HO,
                'TIME_HN': params.item_D.TIME_HN,
                'TIME_OUT': params.item_D.TIME_OUT,
                'TIME_OUT2': params.item_D.TIME_OUT2,
                'C_PERID': <?php echo $_SESSION['PERID']?> ,
                'U_PERID': <?php echo $_SESSION['PERID']?>
            };

            $http.post('./ApiService/ApproveData', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.CardNo = response.data;
                });
        }


        $scope.EditData = function (CheckEdit) {
            if (!$scope.EditDataPerson) {
                $scope.EditDataPerson = CheckEdit;
            } else {
                $scope.EditDataPerson = CheckEdit;
            }

            if (params.item_D.A_Time_in2 != null && params.item_D.A_Time_in2 != '') {
                if (params.item_D.A_Time_in2.indexOf("อื่นๆ") >= 0) {
                    document.getElementById("E_TimeIN2").value = '';
                    $scope.LeaveCheckIn = true;
                } else if (params.item_D.A_Time_in2.indexOf("ลา") >= 0) {
                    document.getElementById("E_TimeIN2").value = '';
                    $scope.LeaveCheckIn = true;
                } else if (params.item_D.A_Time_in2.indexOf("ขาดงาน") >= 0) {
                    document.getElementById("E_TimeIN2").value = '';
                    $scope.LeaveCheckIn = true;
                } else {
                    $scope.LeaveCheckIn = false;
                    document.getElementById("E_TimeIN2").value = params.item_D.A_Time_in2.split(":")[0] + ":" + params.item_D.A_Time_in2.split(":")[1];
                    $scope.DoorS_Timein2 = params.item_D.A_Door_in2;
                }
            } else {
                document.getElementById("E_TimeIN2").value = '';
            }
            if (params.item_D.A_Time_ho != null && params.item_D.A_Time_ho != '') {
                if (params.item_D.A_Time_ho.indexOf("อื่นๆ") >= 0) {
                    document.getElementById("E_TimeHO").value = '';
                    $scope.LeaveCheckHo = true;
                } else if (params.item_D.A_Time_ho.indexOf("ลา") >= 0) {
                    document.getElementById("E_TimeHO").value = '';
                    $scope.LeaveCheckHo = true;
                } else if (params.item_D.A_Time_ho.indexOf("ขาดงาน") >= 0) {
                    document.getElementById("E_TimeHO").value = '';
                    $scope.LeaveCheckHo = true;
                } else {
                    $scope.LeaveCheckHo = false;
                    document.getElementById("E_TimeHO").value = params.item_D.A_Time_ho.split(":")[0] + ":" + params.item_D.A_Time_ho.split(":")[1];
                    $scope.DoorS_Timeho = params.item_D.A_Door_ho;
                }
            } else {
                document.getElementById("E_TimeHO").value = '';
            }
            if (params.item_D.A_Time_hn != null && params.item_D.A_Time_hn != '') {
                if (params.item_D.A_Time_hn.indexOf("อื่นๆ") >= 0) {
                    document.getElementById("E_TimeHN").value = '';
                    $scope.LeaveCheckHn = true;
                } else if (params.item_D.A_Time_hn.indexOf("ลา") >= 0) {
                    document.getElementById("E_TimeHN").value = '';
                    $scope.LeaveCheckHn = true;
                } else if (params.item_D.A_Time_hn.indexOf("ขาดงาน") >= 0) {
                    document.getElementById("E_TimeHN").value = '';
                    $scope.LeaveCheckHn = true;
                } else {
                    $scope.LeaveCheckHn = false;
                    document.getElementById("E_TimeHN").value = params.item_D.A_Time_hn.split(":")[0] + ":" + params.item_D.A_Time_hn.split(":")[1];
                    $scope.DoorS_Timehn = params.item_D.A_Door_hn;
                }
            } else {
                document.getElementById("E_TimeHN").value = '';
            }
            if (params.item_D.A_Time_out2 != null && params.item_D.A_Time_out2 != '') {
                if (params.item_D.A_Time_out2.indexOf("อื่นๆ") >= 0) {
                    $scope.LeaveCheckOut = true;
                    document.getElementById("E_TimeOUT2").value = '';
                } else if (params.item_D.A_Time_out2.indexOf("ลา") >= 0) {
                    $scope.LeaveCheckOut = true;
                    document.getElementById("E_TimeOUT2").value = '';
                } else if (params.item_D.A_Time_out2.indexOf("ขาดงาน") >= 0) {
                    $scope.LeaveCheckOut = true;
                    document.getElementById("E_TimeOUT2").value = '';
                } else {
                    $scope.LeaveCheckOut = false;
                    document.getElementById("E_TimeOUT2").value = params.item_D.A_Time_out2.split(":")[0] + ":" + params.item_D.A_Time_out2.split(":")[1];
                    $scope.DoorS_Timeout2 = params.item_D.A_Door_out2;
                }
            } else {
                document.getElementById("E_TimeOUT2").value = '';
            }
        }

        angular.forEach(params.ObjDataPerson, function (ArrayDataPerson) {
            if (ArrayDataPerson.PERID == params.item_D.PERID) {

                if (ArrayDataPerson.A_Time_in2 != null && ArrayDataPerson.A_Time_in2 != '') {
                    $scope.Date_P_TIN2 = ArrayDataPerson.A_Time_in2;
                } else {
                    $scope.Date_P_TIN2 = "<span style=\"color:#E040FB\"> - </span>";
                }
                if (ArrayDataPerson.A_Door_in2 != null && ArrayDataPerson.A_Door_in2 != '') {
                    $scope.Date_P_DIN2 = $scope.FindDoor(ArrayDataPerson.A_Door_in2);
                } else {
                    $scope.Date_P_DIN2 = "<span style=\"color:#E040FB\"> - </span>";
                }
                if (ArrayDataPerson.A_Time_ho != null && ArrayDataPerson.A_Time_ho != '') {
                    $scope.Date_P_THO = ArrayDataPerson.A_Time_ho;
                } else {
                    $scope.Date_P_THO = "<span style=\"color:#E040FB\"> - </span>";
                }
                if (ArrayDataPerson.A_Time_hn != null && ArrayDataPerson.A_Time_hn != '') {
                    $scope.Date_P_THN = ArrayDataPerson.A_Time_hn;
                } else {
                    $scope.Date_P_THN = "<span style=\"color:#E040FB\"> - </span>";
                }
                if (ArrayDataPerson.A_Door_ho != null && ArrayDataPerson.A_Door_ho != '') {
                    $scope.Date_P_DHO = $scope.FindDoor(ArrayDataPerson.A_Door_ho);
                } else {
                    $scope.Date_P_DHO = "<span style=\"color:#E040FB\"> - </span>";
                }
                if (ArrayDataPerson.A_Door_hn != null && ArrayDataPerson.A_Door_hn != '') {
                    $scope.Date_P_DHN = $scope.FindDoor(ArrayDataPerson.A_Door_hn);
                } else {
                    $scope.Date_P_DHN = "<span style=\"color:#E040FB\"> - </span>";
                }
                if (ArrayDataPerson.A_Time_out2 != null && ArrayDataPerson.A_Time_out2 != '') {
                    $scope.Date_P_TO2 = ArrayDataPerson.A_Time_out2;
                } else {
                    $scope.Date_P_TO2 = "<span style=\"color:#E040FB\"> - </span>";
                }
                if (ArrayDataPerson.A_Door_out2 != null && ArrayDataPerson.A_Door_out2 != '') {
                    $scope.Date_P_DO2 = $scope.FindDoor(ArrayDataPerson.A_Door_out2);
                } else {
                    $scope.Date_P_DO2 = "<span style=\"color:#E040FB\"> - </span>";
                }
            }
        });

        data = {
            'PERID': params.item_D.PERID
        };

        $http.post('./ApiService/GetCardNo', data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
            .then(function successCallback(response) {
                $scope.CardNo = response.data;
            });


        $scope.ok = function (ResultData) {
            // $uibModalInstance.close(ResultData);
            $uibModalInstance.close(true);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('this is result for dismiss');
        };

        var manageTime = function (_timeIn, _timeOut, _timeHo, _timeHn, _doorId, _condition) {
            var Old_timeIn = _timeIn;
            var Old_timeOut = _timeOut;
            var Old_timeHo = _timeHo;
            var Old_timeHn = _timeHn;
            var BeforeH_timeHo = $scope.SetTimeBeforeHour($scope.Date, _timeHo);
            var LateH_timeHn = $scope.SetTimeLateHours($scope.Date, _timeHn);
            _timeIn = $scope.SetTimeLate($scope.Date, _timeIn);
            _timeHnOld = _timeHn;
            _timeHn = $scope.SetTimeLate($scope.Date, _timeHn);
            _timeHo = $scope.SetTimeBefore($scope.Date, _timeHo);
            _timeOut = $scope.SetTimeBefore($scope.Date, _timeOut);

            $scope.DataResult = [];
            $scope.timein2 = '';
            $scope.timehn = '';
            $scope.timeho = '';
            $scope.timeout2 = '';
            $scope.door_in2 = '';
            $scope.door_hn = '';
            $scope.door_ho = '';
            $scope.door_out2 = '';

            // $scope.uniqueStandards2 = $scope.uniqueStandards;
            // console.log('$scope.uniqueStandards');

            // เรียกข้อมูลบุคลากรในฝ่ายเพื่อนำมาเปรียบเทียบข้อมูลเวลาที่ได้จากข้างบนพร้อมบันทึกผล

            data = {
                'DepCode': params.item_D.Dep_Code,
                'DateConrdi': $scope.Date,
                'PERID': params.item_D.PERID
            };
            $http.post('./ApiService/GetPersonDatePersonal', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.PersonDepartTime = response.data;
                    var keepGoing = true;

                    if ($scope.PersonDepartTime.length <= 0) {
                        //คำนวณเวลาเข้าประตูบริเวณ รพ ทั้งหมด
                        angular.forEach($scope.DataPersonTime, function (arr2) {

                            if (arr2.TIME_IN2 <= Old_timeIn) {
                                if (_condition) {
                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                        $scope.timein2 = arr2.TIME_IN2;
                                        $scope.door_in2 = arr2.DOOR_IN2;
                                    }
                                } else {
                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                        $scope.timein2 = arr2.TIME_IN2;
                                        $scope.door_in2 = arr2.DOOR_IN2;
                                    }
                                }
                            } else if (arr2.TIME_IN2 > Old_timeIn) {
                                if (_condition) {
                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                        $scope.timein2 = arr2.TIME_IN2 + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                        $scope.door_in2 = arr2.DOOR_IN2;
                                    }

                                } else {
                                    if ($scope.timein2 == null || $scope.timein2 == '') {
                                        $scope.timein2 = arr2.TIME_IN2 + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                        $scope.door_in2 = arr2.DOOR_IN2;
                                    }
                                }
                            } else {
                            }


                        });
                        //////////////////////////////////////////

                        //คำนวณเวลาออก (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                        angular.forEach($scope.DataPersonTime, function (arr2) {
                            if (arr2.TIME_HO >= _timeHo && arr2.TIME_HO <= Old_timeHo) {
                                if ($scope.timeho == null || $scope.timeho == '') {
                                    $scope.timeho = arr2.TIME_HO;
                                    $scope.door_ho = arr2.DOOR_HO;
                                }
                            } else if (arr2.TIME_HO != '' && arr2.TIME_HO < _timeHo) {
                                if ($scope.timeho == null || $scope.timeho == '') {
                                    $scope.timeho = arr2.TIME_HO + "<span style=\"color:#E040FB\"> ( ออกก่อน ) </span>";
                                    $scope.door_ho = arr2.DOOR_HO;
                                }
                            } else {

                            }
                        });

                        //////////////////////////////////////////

                        //คำนวณเวลาเข้า (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                        angular.forEach($scope.DataPersonTime, function (arr2) {
                            if (arr2.TIME_HN > Old_timeHo && arr2.TIME_HN <= _timeHn) {
                                if ($scope.timehn == null || $scope.timehn == '') {
                                    $scope.timehn = arr2.TIME_HN;
                                    $scope.door_hn = arr2.DOOR_HN;
                                }

                            } else {
                                if (arr2.TIME_HN > _timeHn) {
                                    if ($scope.timehn == null || $scope.timehn == '') {
                                        $scope.timehn = arr2.TIME_HN + "<span style=\"color:#E040FB\"> ( สาย ) </span>";
                                        $scope.door_hn = arr2.DOOR_HN;
                                    }
                                }
                            }
                        });

                        //////////////////////////////////////////

                        //คำนวณเวลาออกประตูบริเวณ รพ ทั้งหมด
                        angular.forEach($scope.DataPersonTime, function (arr2) {
                            if (arr2.TIME_OUT2 >= Old_timeOut && arr2.TIME_OUT2 <= "24:59:59") {
                                if (_condition) {
                                    if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                        $scope.timeout2 = arr2.TIME_OUT2;
                                        $scope.door_out2 = arr2.DOOR_OUT2;
                                    }
                                } else {
                                    if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                        $scope.timeout2 = arr2.TIME_OUT2;
                                        $scope.door_out2 = arr2.DOOR_OUT2;
                                        return false;
                                    }
                                }

                            } else if (arr2.TIME_OUT2 != '' && arr2.TIME_OUT2 < Old_timeOut) {
                                if (_condition) {
                                    if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                        $scope.timeout2 = arr2.TIME_OUT2 + "<span style=\"color:#E040FB\"> ( ออกก่อน ) </span>";
                                        $scope.door_out2 = arr2.DOOR_OUT2;
                                    }
                                } else {
                                    if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                        $scope.timeout2 = arr2.TIME_OUT2 + "<span style=\"color:#E040FB\"> ( ออกก่อน ) </span>";
                                        $scope.door_out2 = arr2.DOOR_OUT2;
                                        return false;
                                    }
                                }
                            } else {
                            }
                        });


                        //////////////////////////////////////////
                    } else {
                        angular.forEach($scope.PersonDepartTime, function (arr1, idx2) {
                            keepGoing = true;
                            angular.forEach($scope.DataPersonTime, function (arr_Item, idx) {
                                //คำนวณเวลาเข้าประตูบริเวณ รพ ทั้งหมด
                                if (arr_Item.TIME_IN2 != '' && arr_Item.TIME_IN2 <= arr1.P_Time_TimeN) {

                                    if (_condition) {
                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                            $scope.timein2 = arr_Item.TIME_IN2 + " <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN) + "</span>";
                                            $scope.door_in2 = arr_Item.DOOR_IN2;
                                        }
                                    } else {
                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                            $scope.timein2 = arr_Item.TIME_IN2 + " <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN) + "</span>";
                                            $scope.door_in2 = arr_Item.DOOR_IN2;
                                            return false;
                                        }
                                    }
                                } else if (arr_Item.TIME_IN2 != '' && arr_Item.TIME_IN2 > arr1.P_Time_TimeN) {
                                    if (_condition) {
                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                            $scope.timein2 = arr_Item.TIME_IN2 + " <span style=\"color:#E040FB\"> (สาย) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN) + "</span>";
                                            $scope.door_in2 = arr_Item.DOOR_IN2;
                                        }
                                    } else {
                                        if ($scope.timein2 == null || $scope.timein2 == '') {
                                            $scope.timein2 = arr_Item.TIME_IN2 + " <span style=\"color:#E040FB\"> (สาย) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeN) + "</span>";
                                            $scope.door_in2 = arr_Item.DOOR_IN2;
                                            return false;
                                        }
                                    }
                                } else {

                                }
                            });
                            //////////////////////////////////////////

                            //คำนวณเวลาออก (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                            angular.forEach($scope.DataPersonTime, function (arr_Item, idx_Item) {
                                if (arr_Item.TIME_HO != '' && arr_Item.TIME_HO >= $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO) && arr_Item.TIME_HO <= arr1.P_Time_TimeHN) {
                                    if ($scope.timeho == null || $scope.timeho == '') {
                                        $scope.timeho = arr_Item.TIME_HO + " <br /> <span style=\"color:#00E5FF\">" +
                                            $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO) + "</span>";
                                        $scope.door_ho = arr_Item.DOOR_HO;
                                    }
                                } else if (arr_Item.TIME_HO != '' && arr_Item.TIME_HO >= $scope.SetTimeBeforeHour($scope.Date, arr1.P_Time_TimeHO) && arr_Item.TIME_HO < $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO)) {
                                    if ($scope.timeho == null || $scope.timeho == '') {
                                        $scope.timeho = arr_Item.TIME_HO + " <span style=\"color:#E040FB\"> (ออกก่อน) </span> <br /> <span style=\"color:#00E5FF\">" +
                                            $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO) + "</span>";
                                        $scope.door_ho = arr_Item.DOOR_HO;
                                    }
                                } else {
                                    if ($scope.timeho == null || $scope.timeho == '') {
                                        $scope.timeho = arr_Item.TIME_HO + " <span style=\"color:#E040FB\"> (สาย) </span> <br /> <span style=\"color:#00E5FF\">" +
                                            $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeHO) + "</span>";
                                        $scope.door_ho = arr_Item.DOOR_HO;
                                    }
                                }
                            });
                            //////////////////////////////////////////


                            //คำนวณเวลาเข้า (เที่ยง) ประตูบริเวณ รพ ทั้งหมด
                            angular.forEach($scope.DataPersonTime, function (arr_Item, idx_Item) {
                                if (arr_Item.TIME_HN != '' && arr_Item.TIME_HN > arr1.P_Time_TimeHN && arr_Item.TIME_HN <= $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN)) {
                                    if ($scope.timehn == null || $scope.timehn == '') {
                                        $scope.timehn = arr_Item.TIME_HN + " <br /> <span style=\"color:#00E5FF\">" +
                                            $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN) + "</span>";
                                        $scope.door_hn = arr_Item.DOOR_HN;
                                    }
                                } else {
                                    if (arr_Item.TIME_HN != '' && arr_Item.TIME_HN > $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN)) {
                                        if ($scope.timehn == null || $scope.timehn == '') {
                                            $scope.timehn = arr_Item.TIME_HN + " <span style=\"color:#E040FB\"> (สาย) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeLate($scope.Date, arr1.P_Time_TimeHN) + "</span>";
                                            $scope.door_hn = arr_Item.DOOR_HN;
                                        }
                                    } else {
                                    }
                                }
                            });
                            //////////////////////////////////////////

                            //คำนวณเวลาออกประตูบริเวณ รพ ทั้งหมด
                            angular.forEach($scope.DataPersonTime, function (arr_Item, idx_Item) {
                                if (arr_Item.TIME_OUT2 != '' && arr_Item.TIME_OUT2 >= arr1.P_Time_TimeO && arr_Item.TIME_OUT2 <= "24:59:59") {
                                    if (_condition) {
                                        if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                            $scope.timeout2 = arr_Item.TIME_OUT2 + " <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO) + "</span>";
                                            $scope.door_out = arr_Item.DOOR_OUT2;
                                        }
                                    } else {
                                        if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                            $scope.timeout2 = arr_Item.TIME_OUT2 + " <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO) + "</span>";
                                            $scope.door_out2 = arr_Item.DOOR_OUT2;
                                            return false;
                                        }
                                    }

                                } else if (arr_Item.TIME_OUT2 != '' && arr_Item.TIME_OUT2 < arr1.P_Time_TimeO) {
                                    if (_condition) {
                                        if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                            $scope.timeout2 = arr_Item.TIME_OUT2 + " <span style=\"color:#E040FB\"> (ออกก่อน) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO) + "</span>";
                                            $scope.door_out2 = arr_Item.DOOR_OUT2;
                                        }
                                    } else {
                                        if ($scope.timeout2 == null || $scope.timeout2 == '') {
                                            $scope.timeout2 = arr_Item.TIME_OUT2 + " <span style=\"color:#E040FB\"> (ออกก่อน) </span> <br /> <span style=\"color:#00E5FF\">" +
                                                $scope.SetTimeBefore($scope.Date, arr1.P_Time_TimeO) + "</span>";
                                            $scope.door_out2 = arr_Item.DOOR_OUT2;
                                            return false;
                                        }
                                    }
                                } else {
                                }
                            });

                            //////////////////////////////////////////
                        });

                    }

                    $scope.DataResult.push({
                        'PERID': params.item_D.PERID,
                        'DEP_CODE': params.item_D.Dep_Code,
                        'NAME': params.item_D.NAME,
                        'SURNAME': params.item_D.SURNAME,
                        'TIME_IN2': $scope.timein2,
                        'TIME_HO': $scope.timeho,
                        'TIME_HN': $scope.timehn,
                        'TIME_OUT2': $scope.timeout2,
                        'DOOR_IN2': $scope.door_in2,
                        'DOOR_HO': $scope.door_ho,
                        'DOOR_HN': $scope.door_hn,
                        'DOOR_OUT2': $scope.door_out2,
                    });

                    $scope.uniqueStandards = UniqueArraybyId($scope.DataResult,
                        "PERID");

                    function UniqueArraybyId(collection, keyname) {
                        var output = [],
                            keys = [];

                        angular.forEach(collection, function (item) {
                            var key = item[keyname];
                            if (keys.indexOf(key) === -1) {
                                keys.push(key);
                                output.push(item);
                            }
                        });
                        return output;
                    };

                    data = {
                        'DepCode': params.item_D.Dep_Code,
                        'DateConrdi': $scope.Date,
                        'PERID': <?php echo $_SESSION['PERID']?> ,
                        'OBJDATA': $scope.uniqueStandards
                    };

                    $http.post('./ApiService/UpdateDataApprove', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            if (response.data) {
                                Swal(
                                    'ผลการแก้ไขข้อมูลการปฏิบัติงาน',
                                    response.data.Message,
                                    'success'
                                )
                            } else {
                                Swal(
                                    'ผลการแก้ไขข้อมูลการปฏิบัติงาน',
                                    response.data.Message,
                                    'error'
                                )
                            }
                        });

                    $scope.Loading = false;
                    $scope.ok($scope.uniqueStandards);
                });

            ////////////////////////////////////////////////////////////
            // console.log($scope.uniqueStandards);
            // $scope.uniqueStandards2 = $scope.uniqueStandards;
            // $scope.$apply();
            // $window.location.reload();

        }


    }]);

    app.controller('ModalEditTime', ['$scope', '$timeout', '$http', '$sce', '$uibModalInstance', 'params', function ($scope,$timeout, $http, $sce, $uibModalInstance, params) {
        $scope.PERID = params.PERID
        $scope.DataTimeLine2 = params.DataTimeLine2
        $scope.DoorArr = params.DoorArr
        $scope.DateSelect = params.DateSelect
        $scope.CheckInData = false
        $scope.CheckOutData = false
        $scope.CheckOnclick = false

        $scope.FindDoor = function (door_idData) {
            var doorname = '';
            angular.forEach($scope.DoorArr, function (Door_A) {
                if (Door_A.door_id == door_idData) {
                    doorname = "" + Door_A.door_name;
                }
            });
            return doorname;
        }

        $scope.switchCheckIn = function () {
                $scope.CheckOutData = false
                if(!$scope.CheckInData){
                    $scope.CheckInData = true
                } else {
                    $scope.CheckInData = false
                }
        }

        $scope.switchCheckOut = function () {
                $scope.CheckInData = false
                if(!$scope.CheckOutData){
                    $scope.CheckOutData = true
                } else {
                    $scope.CheckOutData = false
                }
        }

        $scope.CheckIn = function(item, idx) {
            if($scope.CheckInData) {
                $scope.DataTimeLine2.map(TimeL_i => {
                    if(TimeL_i.DateDB == item.DateDB) {
                        TimeL_i.TimeLine.map((item_TL,idx_c) => {
                            if (idx == idx_c) {
                                if (!item_TL.CheckIn) {
                                    if(item_TL.CheckOut) {
                                        item_TL.CheckOut = false
                                    }
                                    item_TL.CheckIn = true
                                } else {
                                    item_TL.CheckIn = false
                                }
                            } else {
                                item_TL.CheckIn = false
                            }
                        })
                    } else {
                        TimeL_i.TimeLine.map((item_TL,idx_c) => {
                            item_TL.CheckIn = false
                        })
                    }
                })
            } else if($scope.CheckOutData) {
                $scope.DataTimeLine2.map(TimeL_i => {
                    if(TimeL_i.DateDB == item.DateDB) {
                        TimeL_i.TimeLine.map((item_TL,idx_c) => {
                            if (idx == idx_c) {
                                if (!item_TL.CheckOut) {
                                    if(item_TL.CheckIn) {
                                        item_TL.CheckIn = false
                                    }
                                    item_TL.CheckOut = true
                                } else {
                                    item_TL.CheckOut = false
                                }
                            } else {
                                item_TL.CheckOut = false
                            }
                        })
                    } else {
                        TimeL_i.TimeLine.map((item_TL,idx_c) => {
                            item_TL.CheckOut = false
                        })
                    }
                })
            }
        }

        $scope.submitEditTime = function() {
            var timein = ''
            var timeout = ''
            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "คุณต้องการบันทึกข้อมูลการปฏิบัติงานใหม่หรือไม่ ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {
                    $scope.CheckOnclick = true
                    $scope.DataTimeLine2.map((item) => {
                        item.TimeLine.map((timeL_i) => {
                            if (timeL_i.CheckIn){
                                timein = timeL_i.event_time
                            } else if(timeL_i.CheckOut) {
                                timeout = timeL_i.event_time
                            }
                        })
                    })
                    if(timein == '' && timeout == ''){
                        $scope.CheckOnclick = false
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถบันทึกข้อมูลได้ โปรดเลือกเวลาการปฏิบัติงาน'
                        });
                    } else {
                        data = {
                            'PERID': <?php echo $_SESSION['PERID']?> ,
                            'date': $scope.SetDateToDB($scope.DateSelect),
                            'time_in': timein,
                            'time_out': timeout,
                            'Create_By': <?php echo $_SESSION['PERID']?> ,
                            'Create_Time': 'NOW()'
                        };
                        $scope.submitData(data)
                    }
                }
            })

        }

        $scope.submitData = function (data) {
            $http.post('./ApiService/SaveEditTIme', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if(response.data.Status){
                        Swal({
                            type: 'success',
                            title: 'บันทึกข้อมูลสำเร็จ'
                        });
                        $scope.ok()
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถบันทึกข้อมูลได้ โปรดเลือกเวลาการปฏิบัติงาน'
                        });
                    }
                    $scope.CheckOnclick = false
                });
        }

        $scope.queryData = function (path, data) {
            $http.post('./ApiService/' + path, data,{
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    return response.data
                });
        }

        $scope.SetDateToDB = function (DateTpDB) {
            $scope.DateSelect = DateTpDB;
            $scope.Day = $scope.DateSelect.split('/')[1];
            $scope.Month = $scope.DateSelect.split('/')[0];
            $scope.Year = $scope.DateSelect.split('/')[2];
            return $scope.Year + "-" + $scope.Month + "-" +
                $scope.Day;
        }

        $scope.ok = function () {
            $uibModalInstance.close(true);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('this is result for dismiss');
        };


    }]);


    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('/')[1];
                var Month = value.split('/')[0];
                var Year = value.split('/')[2];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateTimeLine', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('-')[2];
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateNameCut', function () {
        return function (value) {
            if (value != undefined) {
                if (value.length > 20) {
                    value = value.substring(0, 20);
                    value += "...";
                }
            }
            return (
                value
            );
        }
    })

    app.filter('NameTh', function () {
        return function (value) {
            var DateTh = "";
            if (value != undefined) {
                switch (value) {
                    case "Sunday":
                        DateTh = "อาทิตย์"
                        break;
                    case "Monday":
                        DateTh = "จันทร์"
                        break;
                    case "Tuesday":
                        DateTh = "อังคาร"
                        break;
                    case "Wednesday":
                        DateTh = "พุธ"
                        break;
                    case "Thursday":
                        DateTh = "พฤหัสบดี"
                        break;
                    case "Friday":
                        DateTh = "ศุกร์"
                        break;
                    case "Saturday":
                        DateTh = "เสาร์"
                        break;
                    default:
                        DateTh = value
                    // code block
                }
            }
            return (
                DateTh
            );
        }
    });

    app.directive('myDirective', ['$window', function ($window) {
        return function (scope, element) {
            var w = angular.element($window);
            scope.getWindowDimensions = function () {
                return {
                    'h': w.height(),
                    'w': w.width()
                };
            };
            scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
                scope.windowHeight = newValue.h;
                scope.windowWidth = newValue.w;
                scope.style = function () {
                    return {
                        'height': (newValue.h - 100) + 'px',
                        'width': (newValue.w - 100) + 'px'
                    };
                };
            }, true);

            w.bind('resize', function () {
                scope.$apply();
            });
        }
    }]);
</script>
</body>

</html>
