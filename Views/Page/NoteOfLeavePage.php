<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            width: 1%;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            width: 1%;
        }

        .table > caption + thead > tr:first-child > td,
        .table > caption + thead > tr:first-child > th,
        .table > colgroup + thead > tr:first-child > td,
        .table > colgroup + thead > tr:first-child > th,
        .table > thead:first-child > tr:first-child > td,
        .table > thead:first-child > tr:first-child > th {
            border-top: 0;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <div class="content-wrapper" ng-controller="myCtrl">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                อนุมัติใบลา
                <small>ข้อมูลการลาของบุคลากรในหน่วยงาน</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> การจัดการ การลา</a></li>
                <li class="active">อนุมัติใบลา</li>
            </ol>
        </section>

        <button class="btn btn-info btn-round" data-toggle="modal" data-target="#noticeModal" ng-show="false"
                id="showModal">
            Notice modal
        </button>

        <!-- Main content -->
        <section class="content" ng-if="CheckPermiss">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="panel panel-default">
                            <div class="box-header">
                                <h3 class="box-title">ตารางแสดงข้อมูลใบลาของบุคลากรในหน่วยงาน</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table datatable="ng" dt-options="dtOptions" class="table table-bordered"
                                           width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <!-- <th>ลำดับที่</th> -->
                                            <th>อนุมัติ</th>
                                            <th>ไม่นับเป็นวันปฏิบัติงานฯ</th>
                                            <th>หน่วยงาน</th>
                                            <th>ชื่อ - สกุล</th>
                                            <th>ประเภทการลา</th>
                                            <th>เหตุผล</th>
                                            <th>วันที่เริ่มต้น</th>
                                            <th>วันที่สิ้นสุด</th>
                                            <th>ทำรายการ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="odd gradeX" ng-repeat="leaveData in LeaveDataPerson">
                                            <!-- <td style="text-align: center; vertical-align: middle;">{{$index + 1}}</td>
                                            <td style="text-align: center; vertical-align: middle;">{{leaveData.PERID}}</td> -->
                                            <td style="text-align: center; vertical-align: middle;">
                                                <div class="checkbox" ng-click="CheckNoId(leaveData.No_Id)">
                                                    <label>
                                                        <input type="checkbox" style="transform: scale(2);">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;">
                                                <div class="checkbox" ng-click="CheckDayOffNoId(leaveData.No_Id)">
                                                    <label>
                                                        <input type="checkbox" style="transform: scale(2);"
                                                               ng-checked="CheckLeaveType(leaveData.T_Leave_Type)"
                                                               ng-disabled="CheckLeaveType(leaveData.T_Leave_Type)">
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;">
                                                {{leaveData.Dep_name}}
                                            </td>
                                            <td style="vertical-align: middle;font-weight: bold;">{{leaveData.NAME}}
                                                {{leaveData.SURNAME}} ({{leaveData.PERID}})
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:red;font-weight: bold;">
                                                {{leaveData.Leave_Detail}}
                                            </td>
                                            <td style="text-align: left; vertical-align: middle;color:royalblue;font-weight: bold;text-align: justify;text-justify: inter-word;">
                                                {{leaveData.T_Leave_Reason}}
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">
                                                {{leaveData.T_Leave_Date_Start | DateThai}}
                                                ({{leaveData.T_Day_Type_Start}})
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">
                                                {{leaveData.T_Leave_Date_End | DateThai}} ({{leaveData.T_Day_Type_End}})
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;">
                                                <a class="btn btn-app" ng-click="showDetails2(leaveData)">
                                                    <i class="fa fa-edit" style="color:gold"></i> พิจารณา
                                                </a>
                                                <a class="btn btn-app" ng-click="DeleteData(leaveData)">
                                                    <i class="fa fa-window-close-o" style="color:maroon"></i> ลบใบลา
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div align="center">
                                    <button class="btn btn-success" style="transform: scale(1)"
                                            ng-click="Consider_Leave()">
                                        <i class="fa fa-save" style="color:gold;margin-right: 10px"></i> <span
                                                style="font-size: 16px;font-weight: bold;color: white;">อนุมัติเฉพาะรายการที่ถูกเลือก</span>
                                    </button>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <section class="content" ng-if="!CheckPermiss">
            <div style="position: fixed;top: 50%;left: 30%;" align="center">
                <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
            </div>
        </section>
        <section id="printSectionId" ng-if="false">

            <div class="row" style="margin-top: 50px">
                <div align="center">
                    <h4 style="text-decoration: underline;font-weight: bold">ใบลาป่วย ลาคลอดบุตร ลากิจส่วนตัว</h4>
                </div>
                <div align="center" style="margin-right: 80px">
                    <p style="text-align: right">เขียนที่ คณะแพทยศาสตร์</p>
                </div>
                <div align="center" style="margin-right: 80px">
                    <p style="text-align: right">วันที่ {{DataPrint.T_Leave_CreateT.split(' ')[0] | DateThai}}</p>
                </div>
                <br/>
                <div style="margin-left: 80px;margin-right: 80px;font-size: 14px">
                    <div style="margin-top: 15px">
                        <span style="font-weight: bold;">เรื่อง</span><span style="text-align: left;margin-left: 20px">{{DataPrint.Leave_Detail}}</span>
                    </div>
                    <div style="margin-top: 15px">
                        <span style="font-weight: bold;">เรียน</span><span style="text-align: left;margin-left: 20px">หัวหน้าหน่วยงาน</span>
                    </div>
                    <div style="margin-top: 15px">
                        <p style="margin-left: 40px" ng-if="DataPrint.SEX == '1' && DataPrint.TITLE == '2'">ข้าพเจ้า นาง
                            {{DataPrint.NAME}} {{DataPrint.SURNAME}}</p>
                        <p style="margin-left: 40px" ng-if="DataPrint.SEX == '1' && DataPrint.TITLE != '2'">ข้าพเจ้า
                            น.ส. {{DataPrint.NAME}} {{DataPrint.SURNAME}}</p>
                        <p style="margin-left: 40px" ng-if="DataPrint.SEX == '2'">ข้าพเจ้า <span
                                    style="margin-left: 20px">นาย {{DataPrint.NAME}} {{DataPrint.SURNAME}}</span><span
                                    style="margin-left: 30px">ตำแหน่ง</span><span style="margin-left: 20px">{{DataPrint.PosName}}</span>
                        </p>
                        <p style="margin-left: 40px"><span>สังกัด</span><span style="margin-left: 20px">{{DataPrint.Dep_name}}</span>
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-sm-2" style="margin-top: 20px">
                            <span style="margin-left: 40px;margin-top: 25px;text-align: right">ขอลา</span>
                        </div>
                        <div class="col-sm-3">
                            <div ng-if="DataPrint.T_Leave_Type != '1'">
                                <i class="fa fa-square-o" aria-hidden="true"></i><span
                                        style="margin-left: 10px">ป่วย</span>
                            </div>
                            <div ng-if="DataPrint.T_Leave_Type == '1'">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">ป่วย</span>
                            </div>
                            <div ng-if="DataPrint.T_Leave_Type != '0'">
                                <i class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">กิจส่วนตัว</span>
                            </div>
                            <div ng-if="DataPrint.T_Leave_Type == '0'">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">กิจส่วนตัว</span>
                            </div>
                            <div ng-if="DataPrint.T_Leave_Type != '5'">
                                <i class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">คลอดบุตร</span>
                            </div>
                            <div ng-if="DataPrint.T_Leave_Type == '5'">
                                <i class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">คลอดบุตร</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <span>เนื่องจาก</span><span style="margin-left: 30px">{{DataPrint.T_Leave_Reason}}</span>
                        </div>
                    </div>
                    <div style="margin-top: 15px">
                        <span style="font-weight: bold;">ตั้งแต่วันที่</span><span
                                style="text-align: left;margin-left: 20px">{{DataPrint.T_Leave_Date_Start | DateThai}}</span><span
                                style="font-weight: bold;margin-left: 10px">ถึงวันที่</span><span
                                style="text-align: left;margin-left: 20px">{{DataPrint.T_Leave_Date_End | DateThai}}</span><span
                                style="font-weight: bold;margin-left: 10px">มีกำหนด</span><span
                                style="text-align: left;margin-left: 20px">{{timeDiff1}} วัน</span>
                    </div>
                    <!-- if check ประเภทการลา -->
                    <div style="margin-top: 15px" ng-if="DataPrint.T_Leave_Type == '5'">
                        <span>ข้าพเจ้าได้ลา</span><i style="margin-left: 20px" class="fa fa-square-o"
                                                     aria-hidden="true"></i><span
                                style="margin-left: 10px">ป่วย</span><i style="margin-left: 20px" class="fa fa-square-o"
                                                                        aria-hidden="true"></i><span
                                style="margin-left: 10px">กิจส่วนตัว</span><i style="margin-left: 20px"
                                                                              class="fa fa-check-square-o"
                                                                              aria-hidden="true"></i><span
                                style="margin-left: 10px">คลอดบุตร</span><span style="margin-left: 30px">ครั้งสุดท้ายตั้งแต่</span>
                    </div>
                    <div style="margin-top: 15px" ng-if="DataPrint.T_Leave_Type == '0'">
                        <span>ข้าพเจ้าได้ลา</span><i style="margin-left: 20px" class="fa fa-square-o"
                                                     aria-hidden="true"></i><span
                                style="margin-left: 10px">ป่วย</span><i style="margin-left: 20px"
                                                                        class="fa fa-check-square-o"
                                                                        aria-hidden="true"></i><span
                                style="margin-left: 10px">กิจส่วนตัว</span><i style="margin-left: 20px"
                                                                              class="fa fa-square-o"
                                                                              aria-hidden="true"></i><span
                                style="margin-left: 10px">คลอดบุตร</span><span style="margin-left: 30px">ครั้งสุดท้ายตั้งแต่</span>
                    </div>
                    <div style="margin-top: 15px" ng-if="DataPrint.T_Leave_Type == '1'">
                        <span>ข้าพเจ้าได้ลา</span><i style="margin-left: 20px" class="fa fa-check-square-o"
                                                     aria-hidden="true"></i><span
                                style="margin-left: 10px">ป่วย</span><i style="margin-left: 20px" class="fa fa-square-o"
                                                                        aria-hidden="true"></i><span
                                style="margin-left: 10px">กิจส่วนตัว</span><i style="margin-left: 20px"
                                                                              class="fa fa-square-o"
                                                                              aria-hidden="true"></i><span
                                style="margin-left: 10px">คลอดบุตร</span><span style="margin-left: 30px">ครั้งสุดท้ายตั้งแต่</span>
                    </div>
                    <!--  -->
                    <!-- if check ประวัติการลา  -->
                    <div style="margin-top: 15px" ng-if="HistoryLeave.length > 0">
                        <span style="font-weight: bold;">วันที่</span><span style="text-align: left;margin-left: 20px">{{DateHisS | DateThai}}</span><span
                                style="font-weight: bold;margin-left: 10px">ถึงวันที่</span><span
                                style="text-align: left;margin-left: 20px">{{DateHisE | DateThai}}</span><span
                                style="font-weight: bold;margin-left: 10px">มีกำหนด</span><span
                                style="text-align: left;margin-left: 20px">{{timeDiff2}} วัน</span>
                    </div>
                    <div style="margin-top: 15px" ng-if="HistoryLeave.length == 0">
                        <span style="font-weight: bold;">วันที่</span><span style="text-align: left;margin-left: 20px">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span><span
                                style="font-weight: bold;margin-left: 10px">ถึงวันที่</span><span
                                style="text-align: left;margin-left: 20px">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span><span
                                style="font-weight: bold;margin-left: 10px">มีกำหนด</span><span
                                style="text-align: left;margin-left: 20px">- &nbsp วัน</span>
                    </div>
                    <!--  -->
                    <div style="margin-top: 15px">
                        <span style="font-weight: bold;">ในระหว่างที่ลาจะติดต่อข้าพเจ้าได้ที่</span><span
                                style="text-align: left;margin-left: 30px">เบอร์ 081126345699</span>
                    </div>
                    <div style="margin-top: 15px" class="row">
                        <div class="col-sm-6">
                            <div>
                                <span style="text-decoration: underline;font-weight: bold;font-size: 14px">สถิติการลาในปีงบประมาณนี้</span>
                            </div>
                            <table class="table table-bordered" border="3" style="border-color: black">
                                <thead style="border-color: black">
                                <tr>
                                    <th scope="col"><span style="font-size: 14px">ประเภทลา</span></th>
                                    <th scope="col"><span style="font-size: 14px">ลามาแล้ว</span></th>
                                    <th scope="col"><span style="font-size: 14px">ลาครั้งนี้</span></th>
                                    <th scope="col"><span style="font-size: 14px">รวมเป็น</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row"><span style="font-size: 14px">ป่วย</span></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row"><span style="font-size: 14px">กิจส่วนตัว</span></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th scope="row"><span style="font-size: 14px">คลอดบุตร</span></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="margin-left: 15px;margin-top: 15px">
                                <span style="margin-top: 15px">(ลงชื่อ)............................................ผู้ตรวจสอบ</span>
                            </div>
                            <div style="margin-left: 15px;margin-top: 15px">
                                <span style="margin-top: 15px">(ตำแหน่ง).................................................</span>
                            </div>
                            <div style="margin-left: 50px;margin-top: 15px">
                                <span style="margin-top: 15px">วันที่................/............../.............</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div style="margin-top: 15px" align="center">
                                <span>ขอแสดงความนับถือ</span>
                            </div>
                            <div style="margin-top: 15px" align="center">
                                <span>(ลงชื่อ)..............................................</span>
                            </div>
                            <div style="margin-top: 15px" align="center">
                                <p style="margin-left: 40px" ng-if="DataPrint.SEX == '1' && DataPrint.TITLE == '2'">(
                                    นาง {{DataPrint.NAME}} {{DataPrint.SURNAME}} )</p>
                                <p style="margin-left: 40px" ng-if="DataPrint.SEX == '1' && DataPrint.TITLE != '2'">(
                                    นางสาว {{DataPrint.NAME}} {{DataPrint.SURNAME}} )</p>
                                <p style="margin-left: 40px" ng-if="DataPrint.SEX == '2'">( นาย {{DataPrint.NAME}}
                                    {{DataPrint.SURNAME}} )</span></p>
                            </div>
                            <div style="margin-top: 15px">
                                <span style="text-decoration: underline;font-weight: bold;font-size: 14px">ความเห็นของผู้บังคับบัญชา</span>
                            </div>
                            <div style="margin-top: 15px">
                                .........................................................................
                            </div>
                            <div style="margin-top: 15px">
                                .........................................................................
                            </div>
                            <div style="margin-left: 25px;margin-top: 15px">
                                <span style="margin-top: 15px">(ลงชื่อ)........................................................</span>
                            </div>
                            <div style="margin-left: 15px;margin-top: 15px">
                                <span style="margin-top: 15px">(ตำแหน่ง)......................................................</span>
                            </div>
                            <div style="margin-left: 50px;margin-top: 15px">
                                <span style="margin-top: 15px">วันที่................../................../.................</span>
                            </div>
                            <div style="margin-top: 15px">
                                <span style="text-decoration: underline;font-weight: bold;font-size: 14px">คำสั่ง</span>
                            </div>
                            <div style="margin-top: 8px" align="center">
                                <i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span
                                        style="margin-left: 10px">อนุญาติ</span><i style="margin-left: 20px"
                                                                                   class="fa fa-square-o"
                                                                                   aria-hidden="true"></i><span
                                        style="margin-left: 10px">ไม่อนุญาติ</span>
                            </div>
                            <div style="margin-left: 25px;margin-top: 15px">
                                <span style="margin-top: 15px">(ลงชื่อ)........................................................</span>
                            </div>
                            <div style="margin-left: 15px;margin-top: 15px">
                                <span style="margin-top: 15px">(ตำแหน่ง)......................................................</span>
                            </div>
                            <div style="margin-left: 50px;margin-top: 15px">
                                <span style="margin-top: 15px">วันที่................../................../.................</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/Js/angular-sanitize.min.js"></script>
<script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Edit_code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Edit_code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>
<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //Date picker
        $('#datepicker2').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    })
</script>
<script>
    var app = angular.module("myApp", ["datatables", "ui.bootstrap", 'ngAnimate']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, DTOptionsBuilder, $uibModal, $interval) {
        $scope.Depart = <?php echo $this->Department; ?>;
        $scope.LeaveType = <?php echo $this->LeaveType; ?>;
        $scope.DayType = <?php echo $this->DayType; ?>;
        $scope.PersonCommit = <?php echo $this->PersonCommit; ?>;
        $scope.Holiday = <?php echo $this->Holiday; ?>;
        $scope.LeaveTSelect = '';
        $scope.personDep - [];
        $scope.Loading = true;
        $scope.CheckPermiss = true;
        $scope.DataPrint = [];
        $scope.DataTimeDepShow = [];
        $scope.DataTimeDepCheck = [];
        $scope.CheckListNoId = [];

        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250])
        // $scope.personPERID

        //ฟังก์ชั่นที่ทำก่อนเมื่อเริ่มหน้าเพจ โดยการดึงค่า Dep_code จากฟังก์ชั่นของ PHP นำมาเก็บไว้ในตัวแปล เพื่อใช้เป็นค่าพารามิเตอร์สำหรับการเลือกข้อมูลหน่วยงาน
        $scope.init = function () {
            //$scope.DepartSelect = "" + <?php //echo($PersonalData["DataPerson"]["Edit_code"]); ?>//;
            //$scope.update();
            // $interval(alert('test'), 100);
            data = {
                'MENU_CODE': '006',
            };

            $http.post('./ApiService/CheckPermissMenu', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    if (response.data.Status) {
                        $scope.CheckPermiss = true;

                        $scope.DepartSelect = "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>;
                        $scope.update();

                    } else {
                        $scope.CheckPermiss = false;
                        $scope.Loading = false;
                    }
                });
        }

        $timeout($scope.init)

        var DateToDb = function (DatePicker) {
            var MonthInsert = document.getElementById(DatePicker).value.split('/')[0];
            var YearInsert = document.getElementById(DatePicker).value.split('/')[2];
            var DayInsert = document.getElementById(DatePicker).value.split('/')[1];
            return YearInsert + "-" + MonthInsert +
                "-" + DayInsert;
        }

        //โชว์ Modal หน้าต่างสำหรับแก้ไขข้อมูลบุคลากรรายบุคคล
        $scope.ModalShow = function (ItemData) {
            $scope.Name = ItemData.NAME + " " + ItemData.SURNAME;
            $scope.LeaveTSelect = ItemData.T_Leave_Type;
            // console.log($scope.LeaveTSelect);
        }

        //พิจารณาใบลาทั้งหมดที่ได้จากการติ๊กเลือก Check box ทางซ้าย
        $scope.Consider_Leave = function () {
            var CheckChooseChoice = false;
            if ($scope.CheckListNoId.length > 0) {
                angular.forEach($scope.CheckListNoId, function (item) {
                    if (item.IsChecked) {
                        CheckChooseChoice = true;
                    }
                })
                if (CheckChooseChoice) {
                    Swal({
                        title: 'ยืนยันการทำรายการ',
                        text: "ต้องการอนุมัติใบลาตามรายการที่เลือก ใช่หรือไม่ !!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'ตกลง',
                        cancelButtonText: 'ยกเลิก'
                    }).then((result) => {
                        if (result.value) {
                            data = {
                                'Data_Consider': $scope.CheckListNoId,
                                'PerId_Update': <?php echo $_SESSION['PERID'] ?>,
                                'DepCode': $scope.DepartSelect
                            };
                            $http.post('./ApiService/T_ListUpdateDataNoteOfLeave', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                                .then(function successCallback(response) {
                                    var result = response.data;
                                    if (result.Status) {
                                        Swal({
                                            type: 'success',
                                            title: result.Message
                                        });
                                        $scope.update();
                                    } else {
                                        Swal({
                                            type: 'error',
                                            title: result.Message
                                        });
                                    }
                                });
                        }
                    });

                } else {
                    Swal({
                        type: 'error',
                        title: 'กรุณาเลือกใบลาที่ต้องการอนุมัติ'
                    });
                }
            } else {
                Swal({
                    type: 'error',
                    title: 'ไม่พบข้อมูลใบลา'
                });
            }


        }

        $scope.CheckNoId = function (id) {
            if ($scope.CheckListNoId.length > 0) {
                angular.forEach($scope.CheckListNoId, function (item, idx) {
                    if (item.No_Id == id) {
                        if (item.IsChecked) {
                            item.IsChecked = false;
                        } else {
                            item.IsChecked = true;
                        }
                        checkDup = true;
                    } else {
                    }
                })
            } else {
            }
        }

        $scope.CheckDayOffNoId = function (id) {
            if ($scope.CheckListNoId.length > 0) {
                angular.forEach($scope.CheckListNoId, function (item, idx) {
                    if (item.No_Id == id) {
                        if (item.DayOff) {
                            item.DayOff = false;
                        } else {
                            item.DayOff = true;
                        }
                    } else {
                    }
                })
            } else {
            }
        }

        $scope.CheckLeaveType = function (type) {
            if (type == '0' || type == '1' || type == '2' || type == '5') {
                return true;
            } else {
                return false;
            }
        }

        //ลบข้อมูลการลาของบุคลากรที่เลือกจาก select
        $scope.DeleteData = function (ItemData) {

            Swal({
                title: 'ยืนยันการทำรายการ',
                text: "ต้องการลบข้อมูลการลา ของคุณ " + ItemData.NAME + " " + ItemData.SURNAME + " ใช่หรือไม่",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then((result) => {
                if (result.value) {
                    data = {
                        'No_Id': ItemData.No_Id,
                        'DepCode': $scope.DepartSelect
                    };
                    $http.post('./ApiService/T_DeleteDataNoteOfLeave', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            var result = response.data;
                            if (result.Status) {
                                Swal({
                                    type: 'success',
                                    title: result.Message
                                });
                                data = {
                                    'DepCode': $scope.DepartSelect,
                                    'PerId': <?php echo $_SESSION['PERID'] ?>
                                };
                                $http.post('./ApiService/T_GetDataNoteOfLeave', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                    .then(function successCallback(response) {
                                        $scope.LeaveDataPerson = [];
                                        $scope.LeaveDataPerson = response.data;
                                    });
                            } else {
                                Swal({
                                    type: 'error',
                                    title: result.Message
                                });
                            }
                        });
                }
            });
        }

        // เรียกข้อมูลบุคลากรภายในหน่วยงานทั้งหมดภายใต้เงื่อนไข depcode จาก select html รหัสหน่วยงาน
        $scope.update = function () {
            // var myElement = angular.element(document.querySelector('#DepartCode'));
            // 'DepCode': myElement[0].options[myElement[0].selectedIndex].value,
            data = {
                'DepCode': $scope.DepartSelect,
                'PerId': <?php echo $_SESSION['PERID'] ?>
            };
            $http.post('./ApiService/T_GetDataNoteOfLeave', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.LeaveDataPerson = response.data;
                    angular.forEach($scope.LeaveDataPerson, function (leave_I) {
                        if (leave_I.T_Leave_Type == '0' || leave_I.T_Leave_Type == '1' || leave_I.T_Leave_Type == '2' || leave_I.T_Leave_Type == '5') {
                            $scope.CheckListNoId.push({'No_Id': leave_I.No_Id, 'IsChecked': false, 'DayOff': true});
                        } else {
                            $scope.CheckListNoId.push({'No_Id': leave_I.No_Id, 'IsChecked': false, 'DayOff': false});
                        }

                    })
                    $scope.LeaveDataPerson = UniqueArraybyId($scope.LeaveDataPerson,
                        "No_Id");

                    function UniqueArraybyId(collection, keyname) {
                        var output = [],
                            keys = [];

                        angular.forEach(collection, function (item) {
                            var key = item[keyname];
                            if (keys.indexOf(key) === -1) {
                                keys.push(key);
                                output.push(item);
                            }
                        });
                        return output;
                    };
                    // $scope.LeaveDataPerson = UniqueArraybyId($scope.LeaveDataPerson,
                    //         "PERID");

                    //     function UniqueArraybyId(collection, keyname) {
                    //         var output = [],
                    //             keys = [];

                    //         angular.forEach(collection, function (item) {
                    //             var key = item[keyname];
                    //             if (keys.indexOf(key) === -1) {
                    //                 keys.push(key);
                    //                 output.push(item);
                    //             }
                    //         });
                    //         return output;
                    //     };
                    // console.log($scope.CheckListNoId)
                    $scope.Loading = false;
                });
        }

        //เรียกดูข้อมูลการลาของบุคลากรที่เลือก จาก select html
        $scope.updatePerson = function () {
            var myElement = angular.element(document.querySelector('#PersonCode'));
            data = {
                'DepCode': $scope.DepartSelect,
                'PerId': myElement[0].options[myElement[0].selectedIndex].value
            };
            $http.post('./ApiService/T_GetLeavePersonData', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.LeaveDataPerson = response.data;
                });
        }

        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        async function demo() {
            await sleep(1000);
            var innerContents = document.getElementById("printSectionId").innerHTML;
            // var popupWinindow = window.open('', '_blank', 'width=1280,height=768,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            // window.document.open('', '_blank', 'width=1280,height=768,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            window.document.write('<html><head><link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css"><link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css"><link rel="stylesheet" type="text/css" href="style.css" /><style>@media print {.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {float: left;}.col-sm-12 {width: 100%;}.col-sm-11 {width: 91.66666667%;}.col-sm-10 {width: 83.33333333%;}.col-sm-9 {width: 75%;}.col-sm-8 {width: 66.66666667%;}.col-sm-7 {width: 58.33333333%;}.col-sm-6 {width: 50%;}.col-sm-5 {width: 41.66666667%;}.col-sm-4 {width: 33.33333333%;}.col-sm-3 {width: 25%;}.col-sm-2 {width: 16.66666667%;}.col-sm-1 {width: 8.33333333%;}}@page {size: A4;margin: 0;}</style></head><body onload="window.print()">' + innerContents + '<script src="./tools/bower_components/jquery/dist/jquery.min.js" /><script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js" /></body></html>');
            window.document.close();
        }

        $scope.GetTimeToDb = function (date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1;

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return date = yyyy + '-' + mm + '-' + dd;
        }

        $scope.calArrayDate = function (fday, lday) {
            var firstDay2 = new Date(fday);
            var lastDay = new Date(lday);
            var daysOfYear = [];
            var options = {
                weekday: 'long'
            };
            for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                daysOfYear.push({
                    'DAY': $scope.GetTimeToDb(d),
                    'DAY_NAME': d.toLocaleDateString('en-US', options),
                    'STATUS': '-'
                });
            }
            return daysOfYear;
        }

        $scope.GetCurrentTime = function () {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return today = yyyy + '-' + mm + '-' + dd;
        }

        $scope.getAge = function (date_1, date_2) {

            //convert to UTC
            var arrayWorkSummary = [];
            var date2_UTC = new Date(Date.UTC(date_2.getUTCFullYear(), date_2.getUTCMonth(), date_2.getUTCDate()));
            var date1_UTC = new Date(Date.UTC(date_1.getUTCFullYear(), date_1.getUTCMonth(), date_1.getUTCDate()));


            var yAppendix, mAppendix, dAppendix;


            //--------------------------------------------------------------
            var days = date2_UTC.getDate() - date1_UTC.getDate();
            if (days < 0) {

                date2_UTC.setMonth(date2_UTC.getMonth() - 1);
                days += $scope.DaysInMonth(date2_UTC);
            }
            //--------------------------------------------------------------
            var months = date2_UTC.getMonth() - date1_UTC.getMonth();
            if (months < 0) {
                date2_UTC.setFullYear(date2_UTC.getFullYear() - 1);
                months += 12;
            }
            //--------------------------------------------------------------
            var years = date2_UTC.getFullYear() - date1_UTC.getFullYear();


            if (years > 1) yAppendix = " years";
            else yAppendix = " year";
            if (months > 1) mAppendix = " months";
            else mAppendix = " month";
            if (days > 1) dAppendix = " days";
            else dAppendix = " day";

            arrayWorkSummary.push(years);
            arrayWorkSummary.push(months);
            arrayWorkSummary.push(days);

            return arrayWorkSummary;
            // return years + yAppendix + ", " + months + mAppendix + ", and " + days + dAppendix + " old.";
        }


        $scope.DaysInMonth = function (date2_UTC) {
            var monthStart = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth(), 1);
            var monthEnd = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth() + 1, 1);
            var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
            return monthLength;
        }

        $scope.open2 = function () {
            var modalInstance = $uibModal.open({
                templateUrl: './Views/Page/modalNoteOfLeave.html',
                controller: 'ModalInstanceCtrl',
                animation: true,
                resolve: {
                    params: function () {
                        return {
                            item_D: $scope.ObjDataItem
                        };
                    }
                }
            });
            modalInstance.result.then(
                function (result) {
                    if (result) {
                        data = {
                            'DepCode': $scope.DepartSelect,
                            'PerId': <?php echo $_SESSION['PERID'] ?>
                        };
                        $http.post('./ApiService/T_GetDataNoteOfLeave', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.LeaveDataPerson = [];
                                $scope.LeaveDataPerson = response.data;
                            });
                    }
                    if (result.Print == 'Print') {
                        $scope.DataPrint = result.DataPrams;
                        // console.log($scope.DataPrint);

                        var Date1 = new Date($scope.DataPrint.T_Leave_Date_Start);
                        var Date2 = new Date($scope.DataPrint.T_Leave_Date_End);

                        $scope.timeDiff1 = $scope.DiffDate(Date2, Date1);
                        //     $timeout(function () {
                        // }, 1500);
                        data = {
                            'PerId': $scope.DataPrint.PERID,
                            'TypeLeave': $scope.DataPrint.T_Leave_Type,
                            'DateCondi': $scope.DataPrint.T_Leave_Date_Start,
                        };
                        $http.post('./ApiService/T_GetHistoryLeaveUers', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.HistoryLeave = response.data;
                                $scope.CheckEndDate = '';
                                $scope.sumALlDayOff = 0;
                                $scope.MaxDateOff = 0;
                                $scope.YearOtc = null;
                                var d1 = new Date($scope.HistoryLeave.Profile[0].Date_In);
                                var d2 = new Date($scope.GetCurrentTime());
                                $scope.GetHisWork = $scope.getAge(new Date($scope.HistoryLeave.Profile[0].Date_In), new Date($scope.GetCurrentTime()));
                                if ($scope.GetHisWork[0] >= 10) {
                                    $scope.MaxDateOff = 30;
                                } else {
                                    $scope.MaxDateOff = 20;
                                }

                                d1.setMonth(d1.getMonth() + 6);
                                var options = {
                                    weekday: 'long'
                                };
                                // console.log(d1.toLocaleDateString('en-US'));
                                // console.log(d2.toLocaleDateString('en-US'));
                                while (d1 <= d2) {

                                    // var yearEnd = '' + d1.getUTCFullYear() + "-10-01";
                                    // var d3 = new Date(yearEnd);
                                    // while (d1 <= d2) {
                                    //     console.log(d1.toLocaleDateString('en-US'));
                                    //     d1.setDate(d1.getDate() + 1);
                                    // }
                                    // var i = 0;
                                    if (d1.getDate() == 1 && (d1.getMonth() + 1) == 10) {
                                        // console.log(d1.toLocaleDateString('en-US'));
                                        $scope.YearOtc = new Date(d1);
                                        angular.forEach($scope.HistoryLeave.Leave2, function (Leaveitem, idx) {
                                            $scope.DataTimeDepCheck = [];
                                            $scope.DataTimeDepShow = [];
                                            $scope.CountDayOff = 0;
                                            $scope.ChecktDayOff = false;
                                            var CountLeave = 0;
                                            var date = new Date(Leaveitem.T_Leave_Date_Start);
                                            // $scope.dateInMonth = $scope.calArrayDate(new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0));

                                            var date_S = new Date(Leaveitem.T_Leave_Date_Start);
                                            var date_E = new Date(Leaveitem.T_Leave_Date_End);

                                            if (date_E.getUTCFullYear() == $scope.YearOtc.getUTCFullYear()) {
                                                if (date_S < $scope.YearOtc && date_E < $scope.YearOtc) {
                                                    if (date_S < ($scope.YearOtc.getUTCFullYear() - 1) && date_E > ($scope.YearOtc.getUTCFullYear() - 1)) {
                                                        while (date_S < ($scope.YearOtc.getUTCFullYear() - 1)) {

                                                            date_S.setDate(date_S.getDate() + 1);
                                                        }
                                                    } else {

                                                    }
                                                    console.log(date_S.toLocaleDateString('en-US'));
                                                    while (date_S <= date_E) {
                                                        if (date_S.getDate() <= 9 && (date_S.getMonth() + 1) <= 9) {
                                                            RangeDate = "0" + (date_S.getMonth() + 1) + '/' + "0" + date_S.getDate() + '/' + date_S.getFullYear();
                                                            RangeDateEnd = "0" + (date_E.getMonth() + 1) + '/' + "0" + date_E.getDate() + '/' + date_E.getFullYear();
                                                        } else if (date_S.getDate() <= 9 && (date_S.getMonth() + 1) > 9) {
                                                            RangeDate = (date_S.getMonth() + 1) + '/' + date_S.getDate() + '/' + date_S.getFullYear();
                                                            RangeDateEnd = (date_E.getMonth() + 1) + '/' + date_E.getDate() + '/' + date_E.getFullYear();
                                                        } else if (date_S.getDate() > 9 && (date_S.getMonth() + 1) <= 9) {
                                                            RangeDate = "0" + (date_S.getMonth() + 1) + '/' + date_S.getDate() + '/' + date_S.getFullYear();
                                                            RangeDateEnd = "0" + (date_E.getMonth() + 1) + '/' + date_E.getDate() + '/' + date_E.getFullYear();
                                                        } else {
                                                            RangeDate = (date_S.getMonth() + 1) + '/' + date_S.getDate() + '/' + date_S.getFullYear();
                                                            RangeDateEnd = (date_E.getMonth() + 1) + '/' + date_E.getDate() + '/' + date_E.getFullYear();
                                                        }

                                                        var Day = RangeDate.split('/')[1];
                                                        var Month = RangeDate.split('/')[0];
                                                        var Year = RangeDate.split('/')[2];
                                                        var RangeDateToDB = Year + "-" + Month + "-" + Day;
                                                        var options = {
                                                            weekday: 'long'
                                                        };
                                                        date_S.setDate(date_S.getDate() + 1);
                                                        CountLeave++;
                                                    }
                                                    $scope.sumALlDayOff += CountLeave;

                                                    if ($scope.HistoryLeave.Leave2[idx + 1] != null) {

                                                        if (Leaveitem.T_Leave_Date_End > $scope.HistoryLeave.Leave2[idx + 1].T_Leave_Date_Start) {
                                                        } else {

                                                            var date_E1 = new Date(Leaveitem.T_Leave_Date_End);
                                                            var date_S1 = new Date($scope.HistoryLeave.Leave2[idx + 1].T_Leave_Date_Start);
                                                            date_E1.setDate(date_E1.getDate() + 1);
                                                            date_S1.setDate(date_S1.getDate() - 1);

                                                            $scope.dateInMonth = $scope.calArrayDate(new Date(date_E1.getFullYear(), date_E1.getMonth(), 1), new Date(date_E1.getFullYear(), date_E1.getMonth() + 1, 0));

                                                            while (date_E1 <= date_S1) {

                                                                // $scope.CountDayOff = 0;
                                                                // $scope.ChecktDayOff = false;

                                                                if (date_E1.getDate() <= 9 && (date_E1.getMonth() + 1) <= 9) {
                                                                    RangeDate = "0" + (date_E1.getMonth() + 1) + '/' + "0" + date_E1.getDate() + '/' + date_E1.getFullYear();
                                                                    RangeDateEnd = "0" + (date_S1.getMonth() + 1) + '/' + "0" + date_S1.getDate() + '/' + date_S1.getFullYear();
                                                                } else if (date_E1.getDate() <= 9 && (date_E1.getMonth() + 1) > 9) {
                                                                    RangeDate = (date_E1.getMonth() + 1) + '/' + date_E1.getDate() + '/' + date_E1.getFullYear();
                                                                    RangeDateEnd = (date_S1.getMonth() + 1) + '/' + date_S1.getDate() + '/' + date_S1.getFullYear();
                                                                } else if (date_E1.getDate() > 9 && (date_E1.getMonth() + 1) <= 9) {
                                                                    RangeDate = "0" + (date_E1.getMonth() + 1) + '/' + date_E1.getDate() + '/' + date_E1.getFullYear();
                                                                    RangeDateEnd = "0" + (date_S1.getMonth() + 1) + '/' + date_S1.getDate() + '/' + date_S1.getFullYear();
                                                                } else {
                                                                    RangeDate = (date_E1.getMonth() + 1) + '/' + date_E1.getDate() + '/' + date_E1.getFullYear();
                                                                    RangeDateEnd = (date_S1.getMonth() + 1) + '/' + date_S1.getDate() + '/' + date_S1.getFullYear();
                                                                }

                                                                var Day = RangeDate.split('/')[1];
                                                                var Month = RangeDate.split('/')[0];
                                                                var Year = RangeDate.split('/')[2];
                                                                var RangeDateToDB = Year + "-" + Month + "-" + Day;
                                                                var options = {
                                                                    weekday: 'long'
                                                                };

                                                                angular.forEach($scope.dateInMonth, function (dateItem) {
                                                                    if (dateItem.DAY == RangeDateToDB) {
                                                                        if (dateItem.DAY_NAME == "Saturday" || dateItem.DAY_NAME == "Sunday") {
                                                                            $scope.CountDayOff++;
                                                                        } else {
                                                                            var checkDayOff2 = false;
                                                                            angular.forEach($scope.Holiday, function (hoItem, ho_idx) {
                                                                                if (hoItem.Date_stop == RangeDateToDB) {
                                                                                    $scope.CountDayOff++;
                                                                                    checkDayOff2 = true;
                                                                                }
                                                                            });

                                                                            if (checkDayOff2) {
                                                                            } else {
                                                                                $scope.ChecktDayOff = true;
                                                                            }

                                                                            // $scope.CountDayOff++;
                                                                        }

                                                                    } else {

                                                                    }
                                                                })
                                                                date_E1.setDate(date_E1.getDate() + 1);

                                                                if (date_E1 > date_S1) {
                                                                    if (!$scope.ChecktDayOff) {
                                                                        $scope.sumALlDayOff += $scope.CountDayOff;
                                                                    } else {
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    } else {
                                                    }

                                                }

                                            }

                                        });
                                    } else {
                                        // while (d3 <= d1) {
                                        //     console.log(d3.toLocaleDateString('en-US'));
                                        //     d3.setDate(d3.getDate() + 1);
                                        // }
                                        if (d1 >= d2) {

                                            angular.forEach($scope.HistoryLeave.Leave2, function (Leaveitem, idx) {
                                                var date_S = new Date(Leaveitem.T_Leave_Date_Start);
                                                var date_E = new Date(Leaveitem.T_Leave_Date_End);

                                                if (date_E > $scope.YearOtc && date_S < $scope.YearOtc) {
                                                    while (date_S < $scope.YearOtc) {
                                                        if (date_S.getDate() == 30 && (date_S.getMonth() + 1) == 9) {
                                                            date_S.setDate(date_S.getDate() + 1);
                                                            if (date_S < date_E) {
                                                                while (date_S <= date_E) {
                                                                    console.log('หลัง 1 ต ค ' + date_S.toLocaleDateString('en-US'));
                                                                    date_S.setDate(date_S.getDate() + 1);
                                                                }
                                                            }
                                                        }
                                                        date_S.setDate(date_S.getDate() + 1);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                    d1.setDate(d1.getDate() + 1);
                                }

                                // demo();
                            });
                        // demo();
                    }
                    // console.log('called $modalInstance.close()');
                    // console.log(result);
                },
                function (result) {
                    // console.log('called $modalInstance.dismiss()');
                    // console.log(result);
                    // alert(result);
                }
            );
        };

        $scope.DiffDate = function (date1, date2) {
            var difference = date1.getTime() - date2.getTime();

            // var daysDifferencem = Math.floor(difference / 1000 / 60 / 60 / 24 / 30 / 12);
            // difference -= daysDifferencem * 1000 * 60 * 60 * 24 * 30 * 12

            var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
            difference -= daysDifference * 1000 * 60 * 60 * 24

            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            difference -= hoursDifference * 1000 * 60 * 60

            var minutesDifference = Math.floor(difference / 1000 / 60);
            difference -= minutesDifference * 1000 * 60

            var secondsDifference = Math.floor(difference / 1000);

            return daysDifference + 1;
        }

        $scope.SetDateToSHOW = function (DateTpDB) {
            $scope.DateSelect = DateTpDB;
            $scope.Year = $scope.DateSelect.split('-')[1];
            $scope.Month = $scope.DateSelect.split('-')[0];
            $scope.Day = $scope.DateSelect.split('-')[2];
            return $scope.Year + "-" + $scope.Month + "-" +
                $scope.Day;
        }

        $scope.showDetails2 = function (item) {
            $scope.ObjDataItem = item;
            $scope.open2();
        }
    });

    app.controller('ModalInstanceCtrl', ['$scope', '$http', '$sce', '$uibModalInstance', 'params', function ($scope, $http, $sce, $uibModalInstance, params) {
        $scope.LeaveType = <?php echo $this->LeaveType; ?>;
        $scope.DayType = <?php echo $this->DayType; ?>;
        $scope.Name = params.item_D.NAME + " " + params.item_D.SURNAME;
        $scope.LeaveTSelect = params.item_D.T_Leave_Type;
        var Year = params.item_D.T_Leave_Date_Start.split('-')[1];
        var Day = params.item_D.T_Leave_Date_Start.split('-')[0];
        var Month = params.item_D.T_Leave_Date_Start.split('-')[2];
        var startDate = Year + "/" + Month + "/" +
            Day;
        Year = params.item_D.T_Leave_Date_End.split('-')[1];
        Day = params.item_D.T_Leave_Date_End.split('-')[0];
        Month = params.item_D.T_Leave_Date_End.split('-')[2];
        var endDate = Year + "/" + Month + "/" +
            Day;
        $scope.DateStart = startDate;
        $scope.DateEnd = endDate;
        $scope.Details = params.item_D.T_Leave_Reason;
        $scope.Status = params.item_D.T_Status_Leave;
        $scope.DateCreate = params.item_D.T_Leave_CreateT;
        $scope.LeaveS_Select = params.item_D.T_Day_Type_Start;
        $scope.LeaveE_Select = params.item_D.T_Day_Type_End;
        $scope.DayAll = params.item_D.T_All_Summary;
        $scope.DayWork = params.item_D.T_Work_Summary;
        $scope.PERID = params.item_D.PERID;
        $scope.Dep_Code = params.item_D.Dep_code;
        // console.log(params.item_D);

        if (params.item_D.T_Leave_Type == '0' || params.item_D.T_Leave_Type == '1' || params.item_D.T_Leave_Type == '2' || params.item_D.T_Leave_Type == '5') {
            $scope.DayOff = '1';
        } else {
            $scope.DayOff = '1';
        }

        $scope.CloseModal = function () {
            $uibModalInstance.close();
        }

        $scope.PrintLeaveData = function () {
            $uibModalInstance.close({
                'Print': 'Print',
                'DataPrams': params.item_D
            });
        }

        $scope.UpdateLeaveData = function () {

            if ($scope.ChangeDate($scope.DateStart) > $scope.ChangeDate($scope.DateEnd)) {
                Swal({
                    type: 'error',
                    title: 'พบข้อผิดพลาด',
                    text: 'โปรดตรวจสอบข้อมูลวันที่อีกครั้ง'
                })
            } else {
                if ($scope.Status == '0') {
                    Swal({
                        type: 'error',
                        title: 'ไม่สามารถบันทึกผลการอนุมัติได้',
                        text: 'กรุณาเลือกผลการอนุมัติให้ถูกต้อง'
                    })
                } else {
                    data = {
                        'PERID_PERSON': params.item_D.PERID,
                        'No_Id': params.item_D.No_Id,
                        'Status': $scope.Status,
                        'DayOff': $scope.DayOff,
                        'Dep_Code': $scope.Dep_Code,
                        'PerId_Update': <?php echo $_SESSION['PERID'] ?>
                    };
                    $http.post('./ApiService/T_UpdateDataNoteOfLeave', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            var result = response.data;
                            if (result.Status) {
                                Swal({
                                    type: 'success',
                                    title: result.Message
                                });
                                $uibModalInstance.close(result.Status);
                            } else {
                                Swal({
                                    type: 'error',
                                    title: result.Message
                                });
                                $uibModalInstance.close();
                            }
                        });
                }
            }
        }

        $scope.ChangeDate = function (DateUpdate) {
            var Day = DateUpdate.split('/')[1];
            var Month = DateUpdate.split('/')[0];
            var Year = DateUpdate.split('/')[2];
            return Year + "-" + Month + "-" + Day;
        }
    }]);

    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Year = value.split('-')[0];
                var Month = value.split('-')[1];
                var Day = value.split('-')[2];
                Year = parseInt(Year) + 543;
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    Year;
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('NumDeci', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var num1 = value.split('.')[0];
                var num2 = value.split('.')[1];
                if (num2 == 0) {
                    DateTrans = num1;
                } else {
                    return value
                }
            }
            return (
                DateTrans
            );
        }
    });
</script>
</body>

</html>
