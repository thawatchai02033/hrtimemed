<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif" />

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" />
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">



    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        @import url(./tools/fonts/thsarabunnew.css);

        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive>div.dataTables_wrapper>div.row>div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive>div.dataTables_wrapper>div.row>div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td,
        .table>caption+thead>tr:first-child>th,
        .table>colgroup+thead>tr:first-child>td,
        .table>colgroup+thead>tr:first-child>th,
        .table>thead:first-child>tr:first-child>td,
        .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
    <div id="wrapper" class="wrapper">

        <?php require './Views/Header.php'?>
        <!-- Left side column. contains the logo and sidebar -->
        <?php require './Views/Menu.php'?>

        <div class="content-wrapper" ng-controller="myCtrl">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    ใบลา
                    <small>จัดการข้อมูลการลาคุณ : <span style="color: blue;font-weight: bold">
                            <?php echo ($PersonalData["DataPerson"]["NAME"] . " " . $PersonalData["DataPerson"]["SURNAME"]); ?></span></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> สถิติการมาทำงาน</a></li>
                    <li class="active">ใบลา</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title" style="font-weight: bold">การลาปฏิบัติงาน</h3>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-12" style="text-align: center">
                                            <h4 style="font-weight: bold">ประเภทการลา:</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <select class="form-control select2" style="width: 100%;" ng-model="LeaveTypeSelect" ng-change="updateLeaveType()" id="LeaveT">
                                                <option ng-repeat="leaveT in LeaveType" value="{{leaveT.Leave_Type}}">{{leaveT.Leave_Detail}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: center">
                                                <h4 style="font-weight: bold">วันที่เริ่มต้นการลา:</h4>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-8">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control pull-right" id="datepicker" style="text-align: center" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="form-control select2" style="width: 100%;text-align-last:center;" id="DayTypeS" ng-model="DayTypeStart">
                                                        <option selected="selected" value="เต็มวัน">เต็มวัน</option>
                                                        <option value="ครึ่งวัน (เช้า)">ครึ่งวัน (เช้า)</option>
                                                        <option value="ครึ่งวัน (บ่าย)">ครึ่งวัน (บ่าย)</option>
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                        <!-- /.form group -->
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-12" style="text-align: center">
                                            <h4 style="font-weight: bold">วันที่สิ้นสุดการลา:</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-8">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="datepicker2" style="text-align: center" autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control select2" style="width: 100%;text-align-last:center;" id="DayTypeE" ng-model="DayTypeEnd">
                                                    <option selected="selected" value="เต็มวัน">เต็มวัน</option>
                                                    <option value="ครึ่งวัน (เช้า)">ครึ่งวัน (เช้า)</option>
                                                    <option value="ครึ่งวัน (บ่าย)">ครึ่งวัน (บ่าย)</option>
                                                </select>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-12" style="text-align: center">
                                            <h4 style="font-weight: bold">เหตุผล:</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="5" placeholder="เหตุผลการลา..." id="LeaveR"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div align="center">
                                    <div class="checkbox">
                                        <label ng-click="CheckDayOff()">
                                            <input type="checkbox" style="transform: scale(2);"><span style="font-weight: bold;font-size: 14px;margin-left: 5px;color: red" ng-click="CheckDayOff()">ต้องการลาในวันหยุดราชการ</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="box-body" ng-show="!C_DayOff">
                                    <div class="form-group">
                                        <div class="col-md-12" style="text-align: center">
                                            <h4 style="color:red;font-weight: bold">ต้องการลาในวันหยุดราชการ:</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-12" align="right">
                                                <div class="col-md-4" align="center">
                                                    <h5 style="font-weight: bold">มีกำหนด : </h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="LeaveDayOff" />
                                                </div>
                                                <div class="col-md-4" align="center">
                                                    <h5 style="font-weight: bold">วัน</h5>
                                                </div>
                                            </div>
                                            <div class="col-md-12" align="left" style="margin-top: 20px">
                                                <div class="col-md-4" align="center">
                                                    <h5 style="font-weight: bold">เป็นวันทำการ : </h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="LeaveDayOffWork" />
                                                </div>
                                                <div class="col-md-4" align="center">
                                                    <h5 style="font-weight: bold">วัน</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="text-align: center;margin-top: 15px">
                                            <span style="color:red;font-weight: bold">หากต้องการลาครึ่งวัน ให้ระบุเป็น .5</span>
                                        </div>
                                    </div>
                                </div>
                                <div style="padding: 20px;" align="center">
                                    <button style="width:40%;" class="btn btn-block btn-success" ng-click="SaveLeaveData()">บันทึกข้อมูล</button>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /. box -->
                    </div>
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true" style="font-weight: bold">ข้อมูลทั่วไป</a></li>
                                <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false" style="font-weight: bold">ประวัติการลา</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="activity">
                                    <div class="box-header with-border" align="center">
                                        <label class="box-title" style="font-weight: bold;font-size: 14px">ประวัติการลาประจำปีงบประมาณ : <span style="color:indianred;font-weight: bold">{{YearShow}}</span></label>
                                        <label class="box-title" style="font-weight: bold;font-size: 14px">ระหว่างวันที่ : <span style="color:indianred;font-weight: bold">{{DateShowStart}}</span> ถึงวันที่ : <span style="color:indianred;font-weight: bold">{{DateShowEnd}}</span></label>
                                    </div>
                                    <div class="col-md-12" style="text-align: center">
                                        <label style="font-weight: bold">ประเภทการลา: <span style="color:hotpink;font-weight: bold">{{LeaveHistroeyShow}}</span></label>
                                    </div>
                                    <div class="box-body box-profile" style="height: 540px;">
                                        <img class="profile-user-img img-responsive img-circle" ng-src="http://mednet.psu.ac.th/Personal/ShowfPict.php?id={{txtPerid}}" onerror="this.src='./Image/personal.png'">

                                        <h3 class="profile-username text-center">{{DataPersonal.NAME}} {{DataPersonal.SURNAME}}</h3>

                                        <p class="text-muted text-center">สังกัด <span style="color: green;font-weight: bold">{{DataPersonal.Dep_name}}</span> ตำแหน่ง <span style="color: green;font-weight: bold">{{DataPersonal.PosName}}</span></p>
                                        <p class="text-muted text-center">อายุงาน <span style="color: indigo;font-weight: bold">{{AgePerson[0]}}</span> ปี <span style="color: indigo;font-weight: bold">{{AgePerson[1]}}</span> เดือน <span style="color: indigo;font-weight: bold">{{AgePerson[2]}} วัน</span></p>
                                        <ul class="list-group list-group-unbordered" style="margin-top: 95px;" ng-if="CheckLeave_T == '2'">
                                            <li class="list-group-item" ng-if="LeaveMaxHis.length > 1">
                                                <b>มีวันลาสะสมพักผ่อน </b> <a class="pull-right"><span style="color:hotpink;font-weight: bold">{{LeaveMaxHis[LeaveMaxHis.length - 2].Count}}</span> วัน</a>
                                            </li>
                                            <li class="list-group-item" ng-if="LeaveMaxHis.length == 1">
                                                <b>มีวันลาสะสมพักผ่อน </b> <a class="pull-right"><span style="color:red;font-weight: bold">รอข้อมูล</span></a>
                                            </li>
                                            <li class="list-group-item" ng-if="LeaveMaxHis.length == 0">
                                                <b>มีวันลาสะสมพักผ่อน </b> <a class="pull-right"><span style="color:red;font-weight: bold">ไม่มีข้อมูล</span></a>
                                            </li>
                                            <li class="list-group-item" ng-if="LeaveMaxHis.length > 1">
                                                <b>มีสิทธิลาสะสมพักผ่อนประจำปีนี้อีก 10 วัน รวมเป็น </b> <a class="pull-right"><span style="color:hotpink;font-weight: bold">{{LeaveMaxHis[LeaveMaxHis.length - 1].Count2}}</span> วัน</a>
                                            </li>
                                            <li class="list-group-item" ng-if="LeaveMaxHis.length == 1">
                                                <b>มีสิทธิลาสะสมพักผ่อนประจำปีนี้อีก 10 วัน รวมเป็น </b> <a class="pull-right"><span style="color:red;font-weight: bold">รอข้อมูล</span></a>
                                            </li>
                                            <li class="list-group-item" ng-if="LeaveMaxHis.length == 0">
                                                <b>มีสิทธิลาสะสมพักผ่อนประจำปีนี้อีก 10 วัน รวมเป็น </b> <a class="pull-right"><span style="color:red;font-weight: bold">ไม่มีข้อมูล</span></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>ลาพักผ่อนในปีงบประมาณ <span style="color:indianred;font-weight: bold">{{YearShow}}</span> ไปแล้ว </b> <a class="pull-right"><span style="color:hotpink;font-weight: bold">{{LeaveMaxHis[LeaveMaxHis.length - 1].LeaveHit}}</span> วัน</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>เหลือวันลาพักผ่อน </b> <a class="pull-right"><span style="color:hotpink;font-weight: bold">{{count_DaySum}}</span> วัน</a>
                                            </li>
                                        </ul>
                                        <ul class="list-group list-group-unbordered" ng-if="CheckLeave_T != '2' && CheckLeave_T != '7' && CheckLeave_T != null">
                                            <li class="list-group-item" style="margin-top: 95px;">
                                                <b>มีประวัติการ <span style="color:hotpink;font-weight: bold">{{LeaveHistroeyShow}}</span> ในปีงบประมาณ </b> <a class="pull-right">ลามาแล้ว <span style="color:hotpink;font-weight: bold">{{count_TimeLeave}}</span> ครั้ง คิดเป็น <span style="color:hotpink;font-weight: bold">{{count_WorkSum}}</span> วันทำการ</a>
                                            </li>
                                        </ul>
                                        <ul class="list-group list-group-unbordered" ng-if="CheckLeave_T == '7' && CheckLeave_T != null">
                                            <li class="list-group-item" style="margin-top: 95px;">
                                                <b>มีประวัติการ <span style="color:hotpink;font-weight: bold">ลา{{LeaveHistroeyShow}}</span> ในปีงบประมาณ </b> <a class="pull-right">ลามาแล้ว <span style="color:hotpink;font-weight: bold">{{count_TimeLeave}}</span> ครั้ง คิดเป็น <span style="color:hotpink;font-weight: bold">{{count_WorkSum}}</span> วันทำการ</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane" id="timeline">
                                    <div class="box-body no-padding" ng-if="LeaveTypeSelect != null">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <!-- <div class="col-md-12" style="text-align: center" ng-if="CheckLeave_T == '2'">
                                                                    <label style="font-weight: bold">มีวันลาสะสมพักผ่อน : <span style="color:hotpink;font-weight: bold">{{LeaveMaxHis[LeaveMaxHis.length - 2].Count}}</span> วัน มีสิทธิลาสะสมพักผ่อนประจำปีนี้อีก 10 วัน รวมเป็น <span style="color:hotpink;font-weight: bold">{{LeaveMaxHis[LeaveMaxHis.length - 1].Count2}}</span> วัน</label>
                                                                    <label style="font-weight: bold">ลาพักผ่อนในปีงบประมาณ <span style="color:indianred;font-weight: bold">{{YearShow}}</span> ไปแล้ว <span style="color:hotpink;font-weight: bold">{{LeaveMaxHis[LeaveMaxHis.length - 1].LeaveHit}}</span> วัน เหลือวันลาพักผ่อน <span style="color:hotpink;font-weight: bold">{{count_DaySum}}</span> วัน</label>
                                                                </div> -->
                                                <div class="col-md-12" style="text-align: center" ng-if="OpenTable && dataLeaveResult.length > 0">
                                                    <label style="font-weight: bold">ตารางประวัติการลา : <span style="color:hotpink;font-weight: bold">{{LeaveHistroeyShow}}</span></label>
                                                </div>
                                            </div>
                                            <div class="box-body" ng-if="OpenTable && dataLeaveResult.length > 0">
                                                <div class="table-responsive">
                                                    <table datatable="ng" dt-options="dtOptions2" class="table table-bordered" width="100%" cellspacing="0">
                                                        <thead>
                                                            <tr>
                                                                <th>ลาครั้งที่</th>
                                                                <th>วันที่เริ่มต้น</th>
                                                                <th>วันที่สิ้นสุด</th>
                                                                <th>จำนวนวันทำการ</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="odd gradeX" style="cursor: pointer;" ng-repeat="leaveData in dataLeaveResult | orderBy:'leaveData.T_Leave_CreateT'">
                                                                <!-- <td style="text-align: center; vertical-align: middle;color:lightcoral;font-weight: bold;">{{$index + 1}}</td> -->
                                                                <td style="text-align: center; vertical-align: middle;color:lightcoral;font-weight: bold;">ลาครั้งที่ <span style="color:green">{{leaveData.T_Leave_Time}}</span></td>
                                                                <td style="text-align: center; vertical-align: middle;color:lightcoral;font-weight: bold;">{{leaveData.T_Leave_Date_Start | DateThai}}</td>
                                                                <td style="text-align: center; vertical-align: middle;color:lightcoral;font-weight: bold;">{{leaveData.T_Leave_Date_End | DateThai}}</td>
                                                                <td style="text-align: center; vertical-align: middle;color:lightcoral;font-weight: bold;">{{leaveData.T_Work_Summary | NumDeci}} วัน</td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th colspan="3" style="text-align: center;font-size: 20px">รวม</th>
                                                                <th style="text-align: center; vertical-align: middle;color:lightcoral;font-weight: bold;font-size: 20px">{{count_WorkSum}} วัน</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body no-padding" ng-if="LeaveTypeSelect == null">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12" style="text-align: center;height: 378px;margin-top: 180px;">
                                                    <h4 style="font-weight: bold"><span style="color:hotpink;font-weight: bold">กรุณาเลือกประเภทการลา</span></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-body no-padding" ng-if="LeaveTypeSelect != null && dataLeaveResult.length == 0">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <div class="col-md-12" style="text-align: center;height: 345px;margin-top: 180px;" ng-if="LeaveTypeSelect != 7">
                                                    <h4 style="font-weight: bold"><span style="color:red;font-weight: bold">ไม่มีข้อมูลการ{{LeaveHistroeyShow}}</span></h4>
                                                </div>
                                                <div class="col-md-12" style="text-align: center;height: 345px;margin-top: 180px;" ng-if="LeaveTypeSelect == 7">
                                                    <h4 style="font-weight: bold"><span style="color:red;font-weight: bold">ไม่มีข้อมูลการลา{{LeaveHistroeyShow}}</span></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /. box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row" align="center">
                    <div class="box box-primary" style="width:95%">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#CodiLeave" data-toggle="tab" aria-expanded="true" style="font-weight: bold">เงื่อนไขการลา</a></li>
                            <li class=""><a href="#LeaveData" data-toggle="tab" aria-expanded="false" style="font-weight: bold">ประวัติการลาทั้งหมด</a></li>
                            <li class=""><a href="#LeaveSummary" data-toggle="tab" aria-expanded="false" style="font-weight: bold">สรุปการลา</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="CodiLeave">
                                <div class="box-body" align="center">
                                    <div class="box box-danger box-solid" style="width:90%">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">เงื่อนไขการลาหยุดราชการ (ตามหลักเกณฑ์การพิจารณาเลื่อนขั้น/เพิ่มค่าจ้างประจำปี)</h3>

                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="box box-danger box-solid">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">ข้าราชการ และลูกจ้างประจำ</h3>

                                                            <div class="box-tools pull-right">
                                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <p style="font-size: 20px;color:red;font-weight: bold">ลาป่วยและลากิจส่วนตัว</p>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">      - ลาได้ไม่ถึง 9 ครั้ง (ภายในครึ่งปีงบประมาณ)</pre>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">      - ลาได้ไม่ถึง 23 ครั้ง (ภายในครึ่งปีงบประมาณ)</pre>
                                                            <p style="font-size: 20px;color:red;font-weight: bold">ลาพักผ่อน</p>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">      - ลาได้ไม่เกินจำนวนวันลาพักผ่อนสะสม</pre>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">      - ไม่อนุญาติให้ลากรณีอายุการทำงานไม่ถึง 6 เดือน</pre>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="box box-danger box-solid">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">พนักงานมหาวิทยาลัยและพนักงานคณะแพทยศาสตร์</h3>

                                                            <div class="box-tools pull-right">
                                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <p style="font-size: 20px;color:red;font-weight: bold">ลาป่วยและลากิจส่วนตัว</p>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">       - ลาได้ไม่ถึง 18 ครั้ง (ภายในปีงบประมาณ)</pre>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">       - ลาได้ไม่ถึง 45 ครั้ง (ภายในปีงบประมาณ)</pre>
                                                            <p style="font-size: 20px;color:red;font-weight: bold">ลาพักผ่อน</p>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">       - ลาได้ไม่เกินจำนวนวันลาพักผ่อนสะสม</pre>
                                                            <pre style="font-size: 16px;color:blue;font-weight: bold">       - ไม่อนุญาติให้ลากรณีอายุการทำงานไม่ถึง 6 เดือน</pre>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="LeaveData">
                                <div class="box-header" style="text-align: center">
                                    <h3 class="box-title">ตารางปฏิทินการลาของคุณ : <span style="color: blue;font-weight: bold">
                                            <?php echo ($PersonalData["DataPerson"]["NAME"] . " " . $PersonalData["DataPerson"]["SURNAME"]); ?></span>
                                    </h3>
                                    <h4 class="text-muted text-center">
                                        <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 9'>( พนักงานมหาลัย )</span>
                                        <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 4'>( พนักงานเงินรายได้ )</span>
                                        <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 3'>( พนักงานมหาลัย )</span>
                                        <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 2'>( ลูกจ้างประจำ )</span>
                                        <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 1'>( ข้าราชการ )</span>
                                    </h4>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table datatable="ng" dt-options="dtOptions" class="table table-bordered" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <!-- <th>ลำดับที่</th> -->
                                                    <th>ชื่อ - สกุล</th>
                                                    <th>ปีงบประมาณ</th>
                                                    <th>ลาครั้งที่</th>
                                                    <th>ประเภทการลา</th>
                                                    <th>วันที่เริ่มต้น</th>
                                                    <th>วันที่สิ้นสุด</th>
                                                    <th>จำนวนวัน</th>
                                                    <th>วันทำการ</th>
                                                    <th>ผลการอนุมัติ</th>
                                                    <th>ทำรายการ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="odd gradeX" style="cursor: pointer;font-size: 16px" ng-repeat="leaveData in LeaveDataPerson">
                                                    <!-- <td style="text-align: center; vertical-align: middle;">{{$index + 1}}</td> -->
                                                    <td style="vertical-align: middle;font-weight: bold;">{{leaveData.NAME}} {{leaveData.SURNAME}}</td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">
                                                        <p>{{leaveData.Fiscal_Year | yearToTh}}</p>
                                                        <p style="color: orange" ng-if="leaveData.Fiscal_Year_Status == '1'">(ครึ่งปีแรก)</p>
                                                        <p style="color: orangered" ng-if="leaveData.Fiscal_Year_Status == '2'">(ครึ่งปีหลัง)</p>
                                                        <p style="color: slateblue" ng-if="leaveData.Fiscal_Year_Status == '0'">(ตลอดปีงบประมาณ)</p>
                                                    </td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;" ng-if="leaveData.T_Leave_Time != null"><span style="color:red">{{leaveData.T_Leave_Time}}</span></td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;" ng-if="leaveData.T_Leave_Time == null"></td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">{{leaveData.Leave_Detail}}</td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">{{leaveData.T_Leave_Date_Start | DateThai}}</td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">{{leaveData.T_Leave_Date_End | DateThai}}</td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">{{leaveData.T_All_Summary}} วัน</td>
                                                    <td style="text-align: center; vertical-align: middle;color:green;font-weight: bold;">{{leaveData.T_Work_Summary | NumDeci}} วันทำการ</td>
                                                    <td style="text-align: center; vertical-align: middle;color:royalblue;font-weight: bold;" ng-if="leaveData.T_Status_Leave == '0'">รอการพิจารณา</td>
                                                    <td style="text-align: center; vertical-align: middle;color:#00E5FF;font-weight: bold;" ng-if="leaveData.T_Status_Leave == '1'">อนุมติ</td>
                                                    <td style="text-align: center; vertical-align: middle;color:red;font-weight: bold;" ng-if="leaveData.T_Status_Leave == '2'">ไม่อนุมติ</td>
                                                    <td style="text-align: center; vertical-align: middle;">
                                                        <a class="btn btn-app" ng-click="showDetails2(leaveData)">
                                                            <i class="fa fa-edit" style="color:lightseagreen"></i> รายละเอียด
                                                        </a>
                                                        <!-- <a class="btn btn-app" ng-click="PrintForm(leaveData)" data-toggle="modal" data-target="#modal-default"> -->
                                                        <a class="btn btn-app" data-toggle="modal" ng-click="PrintForm(leaveData)" data-target="#modal-default" ng-if="leaveData.T_Leave_Type == '0' || leaveData.T_Leave_Type == '1' || leaveData.T_Leave_Type == '2' || leaveData.T_Leave_Type == '5'">
                                                            <i class="fa fa-print" style="color:maroon"></i> พิมพ์
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                            </div>
                            <div class="tab-pane" id="LeaveSummary">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-12" style="text-align: center">
                                            <h4 class="box-title">สรุปการลา : <span style="color: blue;font-weight: bold">
                                                    <?php echo ($PersonalData["DataPerson"]["NAME"] . " " . $PersonalData["DataPerson"]["SURNAME"]); ?></span>
                                            </h4>
                                            <h4 class="text-muted text-center">
                                                <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 9'>( พนักงานมหาลัย )</span>
                                                <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 4'>( พนักงานเงินรายได้ )</span>
                                                <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 3'>( พนักงานมหาลัย )</span>
                                                <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 2'>( ลูกจ้างประจำ )</span>
                                                <span style="color: red;font-weight: bold" ng-if='<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 1'>( ข้าราชการ )</span>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="box box-widget widget-user" style="margin-top: 55px">
                                            <!-- Add the bg color to the header using any of the bg-* classes -->
                                            <div class="widget-user-header bg-black" style="background: lightblue url('./Image/header.jpg') no-repeat;background-size: cover;height: 218px;">
                                                <!-- <h3 class="widget-user-username">Elizabeth Pierce</h3>
                                                <h5 class="widget-user-desc">Web Designer</h5> -->
                                            </div>
                                            <div class="widget-user-image">
                                                <img class="img-circle" ng-src="http://mednet.psu.ac.th/Personal/ShowfPict.php?id={{txtPerid}}" onerror="this.src='./Image/personal.png'" alt="User Avatar">
                                                <!-- <h3 class="profile-username text-center">{{DataPersonal.NAME}} {{DataPersonal.SURNAME}}</h3> -->
                                            </div>
                                            <div class="box-header with-border" align="center">
                                                <label class="box-title" style="font-weight: bold;font-size: 18px">ประวัติการลาประจำปีงบประมาณ : <span style="color:indianred;font-weight: bold">{{YearShow}}</span></label>
                                                <label class="box-title" style="font-weight: bold;font-size: 18px">ระหว่างวันที่ : <span style="color:indianred;font-weight: bold">{{DateShowStart}}</span> ถึงวันที่ : <span style="color:indianred;font-weight: bold">{{DateShowEnd}}</span></label>
                                            </div>
                                            <div class="box-footer">
                                                <div class="row">
                                                    <div class="col-sm-4 border-right" ng-repeat="LeaveMax in LeaveHistoryMax">
                                                        <div class="description-block">
                                                            <p style="font-weight: bold;color:saddlebrown;font-size: 18px">{{LeaveMax[0].Leave_Detail}}</p>
                                                            <p class="description-text" style="font-weight: bold;color:orangered;font-size: 18px" ng-if="LeaveMax[0].MaxLeave != null">{{LeaveMax[0].MaxLeave}} ครั้ง</p>
                                                            <span class="description-text" style="font-weight: bold;color:orangered;font-size: 18px" ng-if="LeaveMax[0].MaxLeave == null">-</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>
            <section id="printSectionId" ng-show="false">

                <div class="row" style="margin-top: 50px">
                    <div align="center">
                        <h4 style="text-decoration: underline;font-weight: bold;font-family: 'THSarabunNew', sans-serif">ใบลาป่วย ลาคลอดบุตร ลากิจส่วนตัว</h4>
                    </div>
                    <div align="center" style="margin-right: 80px">
                        <p style="text-align: right;font-family: 'THSarabunNew', sans-serif;font-size: 14px">เขียนที่ คณะแพทยศาสตร์</p>
                    </div>
                    <div align="center" style="margin-right: 80px">
                        <p style="text-align: right;font-family: 'THSarabunNew', sans-serif;font-size: 14px">วันที่ {{DataForPrint.T_Leave_CreateT.split(' ')[0] | DateThai}}</p>
                    </div>
                    <div style="margin-left: 80px;margin-right: 80px;font-family: 'THSarabunNew', sans-serif;font-size: 14px">
                        <div style="margin-top: 5px">
                            <span>เรื่อง</span><span style="text-align: left;margin-left: 20px">{{DataForPrint.Leave_Detail}}</span>
                        </div>
                        <div style="margin-top: 15px">
                            <span>เรียน</span><span style="text-align: left;margin-left: 20px">{{ManagerAppr.POSI_STATUS}}</span>
                        </div>
                        <div style="margin-top: 15px">
                            <p style="margin-left: 40px" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE == '2'">ข้าพเจ้า นาง {{DataForPrint.NAME}} {{DataForPrint.SURNAME}}</span><span style="margin-left: 30px">ตำแหน่ง</span><span style="margin-left: 20px">{{DataForPrint.PosName}}</span></p>
                            <p style="margin-left: 40px" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE != '2'">ข้าพเจ้า น.ส. {{DataForPrint.NAME}} {{DataForPrint.SURNAME}}</span><span style="margin-left: 30px">ตำแหน่ง</span><span style="margin-left: 20px">{{DataForPrint.PosName}}</span></p>
                            <p style="margin-left: 40px" ng-if="DataForPrint.SEX == '2'">ข้าพเจ้า <span style="margin-left: 20px">นาย {{DataForPrint.NAME}} {{DataForPrint.SURNAME}}</span><span style="margin-left: 30px">ตำแหน่ง</span><span style="margin-left: 20px">{{DataForPrint.PosName}}</span></p>
                            <p style="margin-left: 40px"><span>สังกัด</span><span style="margin-left: 20px">{{DataForPrint.Dep_name}}</span></p>
                        </div>
                        <div class="row">
                            <div class="col-sm-2" style="margin-top: 20px">
                                <span style="margin-left: 40px;margin-top: 25px;text-align: right">ขอลา</span>
                            </div>
                            <div class="col-sm-3">
                                <div ng-if="DataForPrint.T_Leave_Type != '1'">
                                    <i class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">ป่วย</span>
                                </div>
                                <div ng-if="DataForPrint.T_Leave_Type == '1'">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">ป่วย</span>
                                </div>
                                <div ng-if="DataForPrint.T_Leave_Type != '0'">
                                    <i class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">กิจส่วนตัว</span>
                                </div>
                                <div ng-if="DataForPrint.T_Leave_Type == '0'">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">กิจส่วนตัว</span>
                                </div>
                                <div ng-if="DataForPrint.T_Leave_Type != '5'">
                                    <i class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">คลอดบุตร</span>
                                </div>
                                <div ng-if="DataForPrint.T_Leave_Type == '5'">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">คลอดบุตร</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <span>เนื่องจาก</span><span style="margin-left: 30px">{{DataForPrint.T_Leave_Reason}}</span>
                            </div>
                        </div>
                        <div style="margin-top: 15px">
                            <span>ตั้งแต่วันที่</span><span style="text-align: left;margin-left: 20px">{{DataForPrint.T_Leave_Date_Start | DateThai}}</span><span style="margin-left: 42px">ถึงวันที่</span><span style="text-align: left;margin-left: 20px">{{DataForPrint.T_Leave_Date_End | DateThai}}</span><span style="margin-left: 34px">มีกำหนด</span><span style="text-align: left;margin-left: 20px">{{DataForPrint.T_All_Summary}}&nbsp&nbsp&nbsp&nbsp&nbsp วัน</span>
                        </div>
                        <!-- if check ประเภทการลา -->
                        <div style="margin-top: 15px" ng-if="DataForPrint.T_Leave_Type == '5'">
                            <span>ข้าพเจ้าได้ลา</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">ป่วย</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">กิจส่วนตัว</span><i style="margin-left: 20px" class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">คลอดบุตร</span><span style="margin-left: 30px">ครั้งสุดท้ายตั้งแต่</span>
                        </div>
                        <div style="margin-top: 15px" ng-if="DataForPrint.T_Leave_Type == '0'">
                            <span>ข้าพเจ้าได้ลา</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">ป่วย</span><i style="margin-left: 20px" class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">กิจส่วนตัว</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">คลอดบุตร</span><span style="margin-left: 30px">ครั้งสุดท้ายตั้งแต่</span>
                        </div>
                        <div style="margin-top: 15px" ng-if="DataForPrint.T_Leave_Type == '1'">
                            <span>ข้าพเจ้าได้ลา</span><i style="margin-left: 20px" class="fa fa-check-square-o" aria-hidden="true"></i><span style="margin-left: 10px">ป่วย</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">กิจส่วนตัว</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">คลอดบุตร</span><span style="margin-left: 30px">ครั้งสุดท้ายตั้งแต่</span>
                        </div>
                        <!--  -->
                        <!-- if check ประวัติการลา  -->
                        <div style="margin-top: 15px" ng-if="HistoryLeave2.LeaveHis.length > 0">
                            <span>วันที่</span><span style="text-align: left;margin-left: 55px">{{HistoryLeave2.LeaveHis[0].T_Leave_Date_Start | DateThai}}</span><span style="margin-left: 50px">ถึงวันที่</span><span style="text-align: left;margin-left: 20px">{{HistoryLeave2.LeaveHis[0].T_Leave_Date_End | DateThai}}</span><span style="margin-left: 45px">มีกำหนด</span><span style="text-align: left;margin-left: 20px">{{HistoryLeave2.LeaveHis[0].T_All_Summary}}</span><span style="text-align: left;margin-left: 20px">วัน</span>
                        </div>
                        <div style="margin-top: 15px" ng-if="HistoryLeave2.LeaveHis.length == 0">
                            <span>วันที่</span><span style="text-align: left;margin-left: 20px">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span><span style="margin-left: 10px">ถึงวันที่</span><span style="text-align: left;margin-left: 20px">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp-&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span><span style="margin-left: 10px">มีกำหนด</span><span style="text-align: left;margin-left: 20px"> - </span><span style="text-align: left;margin-left: 20px">วัน</span>
                        </div>
                        <!--  -->
                        <div style="margin-top: 15px">
                            <span>ในระหว่างที่ลาจะติดต่อข้าพเจ้าได้ที่</span><span style="text-align: left;margin-left: 30px">{{ContactMe}}</span>
                        </div>
                        <div style="margin-top: 15px" class="row">
                            <div class="col-sm-7">
                                <div>
                                    <span style="text-decoration: underline;font-weight: bold">สถิติการลาในปีงบประมาณนี้</span>
                                </div>
                                <table border="3" style="border-color: black;font-family: 'THSarabunNew', sans-serif;font-size: 14px">
                                    <thead style="border-color: black">
                                        <tr>
                                            <th style="padding-left: 5px;padding-right: 5px;" scope="col"><span style="text-align: center;">ประเภทลา</span></th>
                                            <th style="padding-left: 5px;padding-right: 5px;" scope="col">
                                                <p style="text-align: center">ลามาแล้ว</p>
                                                <p>( วันทำการ )</p>
                                            </th>
                                            <th style="padding-left: 5px;padding-right: 5px;" scope="col">
                                                <p style="text-align: center">ลาครั้งนี้</p>
                                                <p>( วันทำการ )</p>
                                            </th>
                                            <th style="padding-left: 5px;padding-right: 5px;" scope="col">
                                                <p style="text-align: center">รวมเป็น</span>
                                                    <p>( วันทำการ )</p>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody ng-if="DataForPrint.T_Leave_Type == '1'">
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">ป่วย</span></td>
                                            <td style="text-align: center; vertical-align: middle;">{{count_WorkSum2}}</td>
                                            <td style="text-align: center; vertical-align: middle;">{{DataForPrint.T_Work_Summary | NumDeci}}</td>
                                            <td style="text-align: center; vertical-align: middle;">{{sumLeaveDay}}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">กิจส่วนตัว</span></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">คลอดบุตร</span></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tbody ng-if="DataForPrint.T_Leave_Type == '0'">
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">ป่วย</span></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">กิจส่วนตัว</span></td>
                                            <td style="text-align: center; vertical-align: middle;font-size: 13px">{{count_WorkSum2}}</td>
                                            <td style="text-align: center; vertical-align: middle;font-size: 13px">{{DataForPrint.T_Work_Summary | NumDeci}}</td>
                                            <td style="text-align: center; vertical-align: middle;font-size: 13px">{{sumLeaveDay}}</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">คลอดบุตร</span></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tbody ng-if="DataForPrint.T_Leave_Type == '5'">
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">ป่วย</span></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8" scope="row"><span style="text-align: center;">กิจส่วนตัว</span></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; vertical-align: middle;padding: 8px" scope="row"><span style="text-align: center;">คลอดบุตร</span></td>
                                            <td style="text-align: center; vertical-align: middle;">{{count_WorkSum2}}</td>
                                            <td style="text-align: center; vertical-align: middle;">{{DataForPrint.T_Work_Summary | NumDeci}}</td>
                                            <td style="text-align: center; vertical-align: middle;">{{sumLeaveDay}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div style="margin-left: 15px;margin-top: 20px">
                                    <span style="margin-top: 15px">(ลงชื่อ)........................................................ผู้ตรวจสอบ</span>
                                </div>
                                <div style="width: 312px" align="center">
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[3].SEX == '1' && HistoryLeave2.PersonApprove[3].TITLE == '2'">(นาง {{HistoryLeave2.PersonApprove[3].NAME}} {{HistoryLeave2.PersonApprove[3].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[3].SEX == '1' && HistoryLeave2.PersonApprove[3].TITLE != '2'">(นางสาว {{HistoryLeave2.PersonApprove[3].NAME}} {{HistoryLeave2.PersonApprove[3].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[3].SEX == '2'">(นาย {{HistoryLeave2.PersonApprove[3].NAME}} {{HistoryLeave2.PersonApprove[3].SURNAME}})</p>
                                    <p style="margin-top: 15px">ตำแหน่ง {{HistoryLeave2.PersonApprove[3].POSI_STATUS}}</p>
                                    <p style="margin-top: 15px">วันที่............/............../.............</p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12" style="left: -30px;">
                                        <div style="margin-top: 66px;" align="center">
                                            <span style="margin-top: 15px">โดยมอบหมายให้ {{AgentPerson.NAME}} {{AgentPerson.SURNAME}} ปฏิบัติหน้าที่แทน</span>
                                        </div>
                                        <div style="margin-top: 20px" align="center">
                                            <span style="margin-top: 15px">(ลงชื่อ)..........................................................</span>
                                        </div>
                                        <div align="center">
                                            <div align="center">
                                                <p style="margin-top: 15px" ng-if="AgentPerson.SEX == '1' && AgentPerson.TITLE == '2'">(นาง {{AgentPerson.NAME}} {{AgentPerson.SURNAME}})</p>
                                                <p style="margin-top: 15px" ng-if="AgentPerson.SEX == '1' && AgentPerson.TITLE != '2'">(นางสาว {{AgentPerson.NAME}} {{AgentPerson.SURNAME}})</p>
                                                <p style="margin-top: 15px" ng-if="AgentPerson.SEX == '2'">(นาย {{AgentPerson.NAME}} {{AgentPerson.SURNAME}})</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-7"></div> -->
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div style="margin-top: 0px" align="center">
                                    <span>ขอแสดงความนับถือ</span>
                                </div>
                                <div style="margin-top: 15px" align="center">
                                    <span>(ลงชื่อ)..............................................</span>
                                </div>
                                <div style="margin-top: 15px" align="center">
                                    <p style="margin-left: 40px" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE == '2'">( นาง {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} )</p>
                                    <p style="margin-left: 40px" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE != '2'">( นางสาว {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} )</p>
                                    <p style="margin-left: 40px" ng-if="DataForPrint.SEX == '2'">( นาย {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} )</span></p>
                                </div>
                                <div style="margin-top: 15px">
                                    <span style="text-decoration: underline;font-weight: bold;">ความเห็น</span>
                                </div>
                                <div style="margin-top: 5px">
                                    <i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px;">เห็นสมควร</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px;">ไม่เห็นสมควร</span>
                                </div>
                                <div style="margin-top: 15px">.........................................................................
                                </div>
                                <div style="margin-top: 15px">.........................................................................
                                </div>
                                <div style="margin-left: 15px;margin-top: 20px">
                                    <span style="margin-top: 15px">(ลงชื่อ).........................................................</span>
                                </div>
                                <div style="width: 270px" align="center">
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[2].SEX == '1' && HistoryLeave2.PersonApprove[2].TITLE == '2'">(นาง {{HistoryLeave2.PersonApprove[2].NAME}} {{HistoryLeave2.PersonApprove[2].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[2].SEX == '1' && HistoryLeave2.PersonApprove[2].TITLE != '2'">(นางสาว {{HistoryLeave2.PersonApprove[2].NAME}} {{HistoryLeave2.PersonApprove[2].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[2].SEX == '2'">(นาย {{HistoryLeave2.PersonApprove[2].NAME}} {{HistoryLeave2.PersonApprove[2].SURNAME}})</p>
                                    <p style="margin-top: 15px">ตำแหน่ง {{HistoryLeave2.PersonApprove[2].POSI_STATUS}}</p>
                                    <p style="margin-top: 15px">วันที่............/............../.............</p>
                                </div>
                                <div style="margin-top: 15px">
                                    <span style="text-decoration: underline;font-weight: bold;font-size: 14px">คำสั่ง</span>
                                </div>
                                <div style="margin-top: 8px" align="center">
                                    <i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">อนุญาติ</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px">ไม่อนุญาติ</span>
                                </div>
                                <div style="margin-left: 15px;margin-top: 20px">
                                    <span style="margin-top: 15px">(ลงชื่อ).........................................................</span>
                                </div>
                                <d0iv style="width: 270px" align="center">
                                    <p style="margin-top: 15px" ng-if="ManagerAppr.SEX == '1' && ManagerAppr.TITLE == '2'">(นาง {{ManagerAppr.NAME}} {{ManagerAppr.SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="ManagerAppr.SEX == '1' && ManagerAppr.TITLE != '2'">(นางสาว {{ManagerAppr.NAME}} {{ManagerAppr.SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="ManagerAppr.SEX == '2'">(นาย {{ManagerAppr.NAME}} {{ManagerAppr.SURNAME}})</p>
                                    <p style="margin-top: 15px">ตำแหน่ง {{ManagerAppr.POSI_STATUS}}</p>
                                    <p style="margin-top: 15px">วันที่............/............../.............</p>
                                </d0iv>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <section id="printSectionId2" ng-show="false">

                <div class="row" style="margin-top: 50px" style="font-family: 'THSarabunNew', sans-serif;font-size: 14px;">
                    <div align="center">
                        <h4 style="text-decoration: underline;font-weight: bold;font-family: 'THSarabunNew', sans-serif;">ใบลาพักผ่อน</h4>
                    </div>
                    <div align="center" style="margin-right: 80px">
                        <p style="text-align: right;font-family: 'THSarabunNew', sans-serif;font-size: 14px;">เขียนที่ คณะแพทยศาสตร์</p>
                    </div>
                    <div align="center" style="margin-right: 80px">
                        <p style="text-align: right;font-family: 'THSarabunNew', sans-serif;font-size: 14px;">วันที่ {{DataForPrint.T_Leave_CreateT.split(' ')[0] | DateThai}}</p>
                    </div>
                    <br />
                    <div style="margin-left: 80px;margin-right: 80px;font-family: 'THSarabunNew', sans-serif;font-size: 14px;">
                        <div style="margin-top: 15px">
                            <span style="font-family: 'THSarabunNew', sans-serif;font-size: 14px;">เรื่อง</span><span style="text-align: left;margin-left: 20px;">{{DataForPrint.Leave_Detail}}</span>
                        </div>
                        <div style="margin-top: 15px">
                            <span style="font-family: 'THSarabunNew', sans-serif;font-size: 14px;">เรียน</span><span style="text-align: left;margin-left: 20px;">{{ManagerAppr.POSI_STATUS}}</span>
                        </div>
                        <div style="margin-top: 15px" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE == '2'">
                            <p style="margin-left: 0px;line-height: 23pt;" ng-if="LeaveMaxHis2[LeaveMaxHis2.length - 2] != null">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า นาง {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} คณะแพทยศาสตร์ มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 1].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary | NumDeci}} วัน</p>
                            <p style="margin-left: 0px;line-height: 23pt;" ng-if="LeaveMaxHis2[LeaveMaxHis2.length - 2] == null">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า นาง {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} คณะแพทยศาสตร์ มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 1].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary | NumDeci}} วัน</p>
                        </div>
                        <div style="margin-top: 15px" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE != '2'">
                            <p style="margin-left: 0px;line-height: 23pt;" ng-if="LeaveMaxHis2[LeaveMaxHis2.length - 2] != null">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า น.ส. {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} คณะแพทยศาสตร์ มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 1].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary | NumDeci}} วัน</p>
                            <p style="margin-left: 0px;line-height: 23pt;" ng-if="LeaveMaxHis2[LeaveMaxHis2.length - 2] == null">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า น.ส. {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} คณะแพทยศาสตร์ มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 1].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary | NumDeci}} วัน</p>
                        </div>
                        <div style="margin-top: 15px" ng-if="DataForPrint.SEX == '2'">
                            <p style="margin-left: 0px;line-height: 23pt;text-align:justify;" ng-if="LeaveMaxHis2[LeaveMaxHis2.length - 2] != null">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า นาย {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} คณะแพทยศาสตร์ มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 1].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary | NumDeci}} วัน</p>
                            <p style="margin-left: 0px;line-height: 23pt;text-align:justify;" ng-if="LeaveMaxHis2[LeaveMaxHis2.length - 2] == null">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า นาย {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} คณะแพทยศาสตร์ มีวันลาพักผ่อนสะสม 0 วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 1].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary | NumDeci}} วัน</p>
                        </div>
                        <!-- <div style="margin-top: 15px">
                            <p style="margin-left: 0px;" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE == '2'">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า นาง {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary}} วัน</p>
                            <p style="margin-left: 0px;" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE != '2'">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า น.ส. {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary}} วัน</p>
                            <p style="margin-left: 0px;" ng-if="DataForPrint.SEX == '2'">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspข้าพเจ้า นาย {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} ตำแหน่ง {{DataForPrint.PosName}} สังกัด {{DataForPrint.Dep_name}} มีวันลาพักผ่อนสะสม {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count}} วันทำการ มีสิทธิลาพักผ่อนประจำปีนี้อีก 10 วันทำการ รวมเป็น {{LeaveMaxHis2[LeaveMaxHis2.length - 2].Count2}} วันทำการ ขอลาพักผ่อน ตั้งแต่วันที่ {{DataForPrint.T_Leave_Date_Start | DateThai}} {{DataForPrint.T_Day_Type_Start}} ถึงวันที่ {{DataForPrint.T_Leave_Date_End | DateThai}} {{DataForPrint.T_Day_Type_End}} มีกำหนด {{DataForPrint.T_All_Summary}} วัน เป็นวันทำการ {{DataForPrint.T_Work_Summary}} วัน</p>
                        </div> -->
                        <div style="margin-top: 35px" class="row">
                            <div class="col-sm-7">
                                <div>
                                    <span style="text-decoration: underline;font-weight: bold;">สถิติการลาในปีงบประมาณนี้</span>
                                </div>
                                <table border="3" style="border-color: black;font-family: 'THSarabunNew', sans-serif;font-size: 14px;">
                                    <thead style="border-color: black">
                                        <tr>
                                            <th scope="col" style="padding-left: 10px;padding-right: 10px;"><span style="text-align: center;">ประเภทลา</span></th>
                                            <th scope="col" align="center">
                                                <p style="text-align: center">ลามาแล้ว</p>
                                                <p style="text-align: center">( วันทำการ )</p>
                                            </th>
                                            <th scope="col" style="padding-left: 10px;padding-right: 10px;" align="center">
                                                <p style="text-align: center">ลาครั้งนี้</p>
                                                <p style="text-align: center">( วันทำการ )</p>
                                            </th>
                                            <th scope="col" style="padding-left: 10px;padding-right: 10px;" align="center">
                                                <p style="text-align: center">รวมเป็น</p>
                                                <p style="text-align: center">( วันทำการ )</p>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody ng-if="DataForPrint.T_Leave_Type == '2'">
                                        <tr style="padding: 20px">
                                            <td scope="row" style="padding: 8px"><span style="text-align: center;">พักผ่อน</span></td>
                                            <td style="text-align: center; vertical-align: middle;">{{count_WorkSum2}}</td>
                                            <td style="text-align: center; vertical-align: middle;">{{DataForPrint.T_Work_Summary | NumDeci}}</td>
                                            <td style="text-align: center; vertical-align: middle;">{{sumLeaveDay}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div style="margin-left: 35px;margin-top: 20px">
                                    <span style="margin-top: 15px;">(ลงชื่อ)......................................................ผู้ตรวจสอบ</span>
                                </div>
                                <div style="width: 312px" align="center">
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[3].SEX == '1' && HistoryLeave2.PersonApprove[3].TITLE == '2'">(นาง {{HistoryLeave2.PersonApprove[3].NAME}} {{HistoryLeave2.PersonApprove[3].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[3].SEX == '1' && HistoryLeave2.PersonApprove[3].TITLE != '2'">(นางสาว {{HistoryLeave2.PersonApprove[3].NAME}} {{HistoryLeave2.PersonApprove[3].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[3].SEX == '2'">(นาย {{HistoryLeave2.PersonApprove[3].NAME}} {{HistoryLeave2.PersonApprove[3].SURNAME}})</p>
                                    <p style="margin-top: 15px">ตำแหน่ง {{HistoryLeave2.PersonApprove[3].POSI_STATUS}}</p>
                                    <p style="margin-top: 15px">วันที่............/............../.............</p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12" style="left: -30px;">
                                        <div style="margin-top: 90px" align="center">
                                            <span style="margin-top: 15px">โดยมอบหมายให้ {{AgentPerson.NAME}} {{AgentPerson.SURNAME}} ปฏิบัติหน้าที่แทน</span>
                                        </div>
                                        <div style="margin-top: 20px" align="center">
                                            <span style="margin-top: 15px">(ลงชื่อ)..........................................................</span>
                                        </div>
                                        <div style="margin-top: 20px" align="center">
                                            <div align="center">
                                                <p style="margin-top: 15px" ng-if="AgentPerson.SEX == '1' && AgentPerson.TITLE == '2'">(นาง {{AgentPerson.NAME}} {{AgentPerson.SURNAME}})</p>
                                                <p style="margin-top: 15px" ng-if="AgentPerson.SEX == '1' && AgentPerson.TITLE != '2'">(นางสาว {{AgentPerson.NAME}} {{AgentPerson.SURNAME}})</p>
                                                <p style="margin-top: 15px" ng-if="AgentPerson.SEX == '2'">(นาย {{AgentPerson.NAME}} {{AgentPerson.SURNAME}})</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div style="margin-top: 15px" align="center">
                                    <span style="">ขอแสดงความนับถือ</span>
                                </div>
                                <div style="margin-top: 15px" align="center">
                                    <span style="">(ลงชื่อ)...........................................................</span>
                                </div>
                                <div style="margin-top: 15px" align="center">
                                    <p style="margin-left: 30px;" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE == '2'">( นาง {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} )</p>
                                    <p style="margin-left: 30px;" ng-if="DataForPrint.SEX == '1' && DataForPrint.TITLE != '2'">( นางสาว {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} )</p>
                                    <p style="margin-left: 30px;" ng-if="DataForPrint.SEX == '2'">( นาย {{DataForPrint.NAME}} {{DataForPrint.SURNAME}} )</span></p>
                                </div>
                                <div style="margin-top: 15px">
                                    <span style="text-decoration: underline;font-weight: bold;">ความเห็น</span>
                                </div>
                                <div style="margin-top: 5px">
                                    <i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px;">เห็นสมควร</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px;">ไม่เห็นสมควร</span>
                                </div>
                                <div style="margin-top: 15px">.........................................................................
                                </div>
                                <div style="margin-top: 15px">.........................................................................
                                </div>
                                <div style="margin-left: 25px;margin-top: 15px">
                                    <span style="margin-top: 15px;">(ลงชื่อ)........................................................</span>
                                </div>
                                <div style="width: 270px" align="center">
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[2].SEX == '1' && HistoryLeave2.PersonApprove[2].TITLE == '2'">(นาง {{HistoryLeave2.PersonApprove[2].NAME}} {{HistoryLeave2.PersonApprove[2].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[2].SEX == '1' && HistoryLeave2.PersonApprove[2].TITLE != '2'">(นางสาว {{HistoryLeave2.PersonApprove[2].NAME}} {{HistoryLeave2.PersonApprove[2].SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="HistoryLeave2.PersonApprove[2].SEX == '2'">(นาย {{HistoryLeave2.PersonApprove[2].NAME}} {{HistoryLeave2.PersonApprove[2].SURNAME}})</p>
                                    <p style="margin-top: 15px">ตำแหน่ง {{HistoryLeave2.PersonApprove[2].POSI_STATUS}}</p>
                                    <p style="margin-top: 15px">วันที่............/............../.............</p>
                                </div>
                                <div style="margin-top: 15px">
                                    <span style="text-decoration: underline;font-weight: bold;">คำสั่ง</span>
                                </div>
                                <div style="margin-top: 8px" align="center">
                                    <i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px;">อนุญาติ</span><i style="margin-left: 20px" class="fa fa-square-o" aria-hidden="true"></i><span style="margin-left: 10px;">ไม่อนุญาติ</span>
                                </div>
                                <div style="margin-left: 25px;margin-top: 15px">
                                    <span style="margin-top: 15px;">(ลงชื่อ)........................................................</span>
                                </div>
                                <div style="width: 270px" align="center">
                                    <p style="margin-top: 15px" ng-if="ManagerAppr.SEX == '1' && ManagerAppr.TITLE == '2'">(นาง {{ManagerAppr.NAME}} {{ManagerAppr.SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="ManagerAppr.SEX == '1' && ManagerAppr.TITLE != '2'">(นางสาว {{ManagerAppr.NAME}} {{ManagerAppr.SURNAME}})</p>
                                    <p style="margin-top: 15px" ng-if="ManagerAppr.SEX == '2'">(นาย {{ManagerAppr.NAME}} {{ManagerAppr.SURNAME}})</p>
                                    <p style="margin-top: 15px">ตำแหน่ง {{ManagerAppr.POSI_STATUS}}</p>
                                    <p style="margin-top: 15px">วันที่............/............../.............</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>

            <div class="modal fade" id="modal-default">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" style="text-align: center;font-weight: bold;color: dodgerblue">กรุณาระบุข้อมูลให้สมบูรณ์</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body no-padding">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center;margin-top: 25px;">
                                            <label>ผู้อนุมัติ:</label>
                                        </div>
                                        <div class="col-md-7" align="center" style="margin-top: 18px;">
                                            <select class="form-control select2" style="width: 100%;" ng-model="PersonArSelect" id="PersonAr">
                                                <option ng-repeat="PersonAr in PersonAppr" value="{{PersonAr.PERID}}">{{PersonAr.NAME}} {{PersonAr.SURNAME}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2" style="text-align: center">
                                            <a class="btn btn-app" data-toggle="modal" ng-click="PrintForm('')">
                                                <i class="fa fa-print" style="color:maroon"></i> พิมพ์
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body no-padding">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center;margin-top: 25px;">
                                            <label>ผู้ปฏิบัติหน้าที่แทน:</label>
                                        </div>
                                        <div class="col-md-7" align="center" style="margin-top: 18px;">
                                            <select class="form-control select2" style="width: 100%;" ng-model="approverSelect" ng-change="" id="approverPerson">
                                                <option ng-repeat="person in PersonCommit" ng-selected="approverSelect == person.PERID" value="{{person.PERID}}">{{person.NAME}} {{person.SURNAME}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body no-padding" ng-show="DataForPrint.T_Leave_Type == '0' || DataForPrint.T_Leave_Type == '1' || DataForPrint.T_Leave_Type == '5'">
                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-md-3" style="text-align: center;margin-top: 25px;">
                                            <label>ติดต่อข้าพเจ้าได้ที่:</label>
                                        </div>
                                        <div class="col-md-7" align="center" style="margin-top: 18px;">
                                            <input type="text" class="form-control" ng-model="ContactMe" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.content -->
            <div class="loader" align="center" ng-if="Loading">
                <img class="loaderImg" src="./Image/Preloader_2.gif" />
            </div>
        </div>
        <!-- /.content-wrapper -->
        <?php require './Views/Footer.php'?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
    <script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
    <!-- Demo scripts for this page-->
    <script src="./tools/vendor/js/demo/datatables-demo.js"></script>
    <!-- SlimScroll -->
    <script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="./tools/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="./tools/dist/js/demo.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- date-range-picker -->
    <script src="./tools/bower_components/moment/min/moment.min.js"></script>
    <script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap color picker -->
    <script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- Select2 -->
    <script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="./tools/plugins/iCheck/icheck.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="./tools/dist/js/sb-admin-2.js"></script>
    <!-- SweetDialog JS-->
    <script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


    <script src="./tools/Js/angular.min.js"></script>
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
    <script src="./tools/Js/angular-datatables.min.js"></script>
    <script src="./tools/Js/angular-sanitize.min.js"></script>
    <script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <script src="./tools/Js/angular-animate.min.js"></script>

    <!--   Core JS Files   -->
    <script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

    <script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

    <!-- Sharrre libray -->
    <script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
            if (<?php echo $this->model->CheckStatus ?>) {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
                setInterval(function () {
                    $.ajax({
                        type: "POST",
                        url: './ApiService/T_GetCountDataNoteOfLeave',
                        contentType: "application/json",
                        data: JSON.stringify({
                            DepCode: "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>
                        }),
                        error: function (data) {
                            console.log(data);
                        },
                        success: function (data) {
                            var ObjResult = JSON.parse(data);
                            if (ObjResult.Status) {
                                if (ObjResult.Message > 0) {
                                    md.showNotification('bottom', 'right', ObjResult.Message);
                                    $('#showCountLeave').html(ObjResult.Message);
                                } else {
                                    $('#showCountLeave').html('');
                                }
                            }
                        },
                    });
                }, 30000);
            }
        });
    </script>

    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', {
                'placeholder': 'dd/mm/yyyy'
            })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', {
                'placeholder': 'mm/dd/yyyy'
            })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                            'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                        'MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            //Date picker
            $('#datepicker2').datepicker({
                autoclose: true
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })
    </script>
    <script>
        var app = angular.module("myApp", ["datatables", "ui.bootstrap", 'ngAnimate']);
        app.controller("myHeader", function ($scope, $http, $window) {
            $scope.LogOut = function () {
                $http.post('./ApiService/LogOut', {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        if (response.data) {
                            $window.location.href = './Login';
                        } else {
                            Swal({
                                type: 'error',
                                title: 'ออกจากระบบไม่สำเร็จ'
                            });
                        };
                    });
            }
        });
    </script>
    <script>
        app.controller("myCtrl", function ($scope, $http, $window, $timeout, DTOptionsBuilder, $uibModal) {
            $scope.Depart = <?php echo $this->Department; ?>;
            $scope.LeaveType = <?php echo $this->LeaveType; ?>;
            $scope.DayType = <?php echo $this->DayType; ?>;
            $scope.Holiday = <?php echo $this->Holiday; ?>;
            $scope.PersonCommit = <?php echo $this->PersonCommit; ?>;
            $scope.LeaveTSelect = '';
            $scope.txtPerid = '';
            $scope.DataPersonal = [];
            $scope.personDep = <?php echo $this->DataPersonal; ?>;
            $scope.funcCalculateLeave = [];
            $scope.dataLeaveResult = [];
            $scope.TimetoCount = [];
            $scope.LeaveMaxHis = [];
            $scope.LeaveHistoryMax = [];
            $scope.Loading = true;
            $scope.C_DayOff = true;
            $scope.LeaveHistroeyShow = '';
            $scope.count_DaySum = 0;
            $scope.count_WorkSum = 0;
            $scope.count_DaySum2 = 0;
            $scope.count_WorkSum2 = 0;
            $scope.count_TimeLeave = 0;
            $scope.LeaveSum_T2 = 0;
            $scope.LeaveStart_T2 = 0;
            $scope.OpenTable = false;
            $scope.CheckUpdateDayWork = false;

            $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [5, 150, 200, 250])
            $scope.dtOptions2 = DTOptionsBuilder.newOptions().withOption('lengthMenu', [6, 10, 20])
            // $scope.personPERID

            //ฟังก์ชั่นที่ทำก่อนเมื่อเริ่มหน้าเพจ โดยการดึงค่า Dep_code จากฟังก์ชั่นของ PHP นำมาเก็บไว้ในตัวแปล เพื่อใช้เป็นค่าพารามิเตอร์สำหรับการเลือกข้อมูลหน่วยงาน
            $scope.init = function () {
                $scope.DepartSelect = "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>;
                $scope.EditCode = "" + <?php echo ($PersonalData["DataPerson"]["Edit_code"]); ?>;
                $scope.update();
                var d2 = new Date($scope.GetCurrentTime());
                if ((d2.getMonth() + 1) < 10) {
                    $scope.YearShow = (d2.getUTCFullYear() + 543);
                    $scope.DateShowStart = '01 ' + ' ตุลาคม ' + ((d2.getUTCFullYear() - 1) + 543);
                    $scope.DateShowEnd = '30 ' + ' กันยายน ' + ((d2.getUTCFullYear()) + 543);
                } else {
                    $scope.YearShow = ((d2.getUTCFullYear() + 1) + 543);
                    $scope.DateShowStart = '01 ' + ' ตุลาคม ' + (d2.getUTCFullYear() + 543);
                    $scope.DateShowEnd = '30 ' + ' กันยายน ' + ((d2.getUTCFullYear() + 1) + 543);
                }
                angular.forEach($scope.personDep, function (personItem) {
                    if (<?php echo $_SESSION['PERID'] ?> == personItem.PERID) {
                        $scope.DataPersonal = personItem;
                    }
                });
                $scope.txtPerid = <?php echo $_SESSION['PERID'] ?>;
                $scope.AgePerson = $scope.getAge(new Date($scope.DataPersonal.Date_In), new Date());
                $scope.GetLeaveSummary();
            }

            $timeout($scope.init)

            var DateToDb = function (DatePicker) {
                var MonthInsert = document.getElementById(DatePicker).value.split('/')[0];
                var YearInsert = document.getElementById(DatePicker).value.split('/')[2];
                var DayInsert = document.getElementById(DatePicker).value.split('/')[1];
                return YearInsert + "-" + MonthInsert +
                    "-" + DayInsert;
            }

            //โชว์ Modal หน้าต่างสำหรับแก้ไขข้อมูลบุคลากรรายบุคคล
            $scope.ModalShow = function (ItemData) {
                $scope.Name = ItemData.NAME + " " + ItemData.SURNAME;
                $scope.LeaveTSelect = ItemData.T_Leave_Type;
                // console.log($scope.LeaveTSelect);
            }

            $scope.CheckDayOff = function () {
                if (!$scope.C_DayOff) {
                    $scope.C_DayOff = true;
                } else {
                    $scope.C_DayOff = false;
                }
            }

            $scope.GetLeaveSummary = function () {
                data = {
                    'Year': (parseInt($scope.YearShow) - 543),
                    'PerId': <?php echo $_SESSION['PERID'] ?>
                };

                $http.post('./ApiService/GetMaxHistoryLeave', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        $scope.LeaveHistoryMax = response.data;
                    });
            }

            $scope.calLeave = function (Date_Start, Date_End, perId, Dep_code, Leave_T, num_dayOff, max) {
                var d2 = new Date($scope.GetCurrentTime());
                if ((d2.getMonth() + 1) < 10) {
                    $scope.DateShowStart2 = (d2.getUTCFullYear() - 1) + '-10-01';
                    $scope.DateShowEnd2 = (d2.getUTCFullYear()) + '-09-30';
                } else {
                    $scope.DateShowStart2 = d2.getUTCFullYear() + '-10-01';
                    $scope.DateShowEnd2 = (d2.getUTCFullYear() + 1) + '-09-30';
                }
                data = {
                    'Date_Start': Date_Start,
                    'Date_End': Date_End,
                    'PERID': perId,
                    'Dep_Code': Dep_code,
                    'Leave_Type': Leave_T
                };

                $http.post('./ApiService/GetHistoryLeaveYear', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        var ArrayData = response.data;
                        $scope.count_WorkSum = 0;
                        $scope.CheckLeave_T = Leave_T;
                        if (Leave_T == 2) {
                            $scope.count_DaySum += num_dayOff;
                            if ($scope.count_DaySum > $scope.MaxDateOff) {
                                $scope.count_DaySum = $scope.MaxDateOff;
                            }
                            angular.forEach(ArrayData, function (Leaveitem) {
                                $scope.count_DaySum -= Leaveitem.T_Work_Summary;
                                // console.log(parseInt(Leaveitem.T_Work_Summary) + ' ' + $scope.count_DaySum);
                                $scope.count_WorkSum += parseInt(Leaveitem.T_Work_Summary);
                                if (Date_Start >= $scope.DateShowStart2) {
                                    $scope.dataLeaveResult.push(Leaveitem);
                                }
                            })
                        } else {
                            $scope.count_DaySum = 0;
                            angular.forEach(ArrayData, function (Leaveitem) {
                                $scope.count_WorkSum += parseInt(Leaveitem.T_Work_Summary);
                                if (Date_Start >= $scope.DateShowStart2) {
                                    $scope.dataLeaveResult.push(Leaveitem);
                                }
                            })
                        }
                        // console.log($scope.count_DaySum);
                    });
            }

            $scope.updateLeaveType = function () {
                $scope.OpenTable = false;
                var LeaveT = angular.element(document.querySelector('#LeaveT'));
                $scope.count_DaySum = 0;
                $scope.count_TimeLeave = 0;
                $scope.dataLeaveResult = [];
                $scope.LeaveMaxHis = [];
                $scope.LeaveHistroeyShow = LeaveT[0].options[LeaveT[0].selectedIndex].innerText;
                var DateNow = new Date();
                var Date_Start = '' + (DateNow.getUTCFullYear() - 1) + '-10-01';
                var Date_End = '' + DateNow.getUTCFullYear() + '-09-30';
                var CountBefore = 0;

                var d2 = new Date($scope.GetCurrentTime());
                if ((d2.getMonth() + 1) < 10) {
                    $scope.DateShowStart2 = (d2.getUTCFullYear() - 1) + '-10-01';
                    $scope.DateShowEnd2 = (d2.getUTCFullYear()) + '-09-30';
                } else {
                    $scope.DateShowStart2 = d2.getUTCFullYear() + '-10-01';
                    $scope.DateShowEnd2 = (d2.getUTCFullYear() + 1) + '-09-30';
                }

                $scope.CheckLeave_T = LeaveT[0].options[LeaveT[0].selectedIndex].value;

                data = {
                    'PerId': <?php echo $_SESSION['PERID'] ?>,
                    'TypeLeave': LeaveT[0].options[LeaveT[0].selectedIndex].value,
                };
                $http.post('./ApiService/T_GetLeveStart', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        $scope.leaveStart = response.data;
                        data = {
                            'PerId': <?php echo $_SESSION['PERID'] ?>,
                            'TypeLeave': LeaveT[0].options[LeaveT[0].selectedIndex].value,
                            'DateCondi': Date_Start,
                        };
                        $http.post('./ApiService/T_GetHistoryLeaveUers', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                            .then(function successCallback(response) {
                                $scope.HistoryLeave = response.data;
                                // console.log($scope.HistoryLeave);
                                var d1 = new Date($scope.HistoryLeave.Profile[0].Date_In);
                                var d2 = new Date($scope.GetCurrentTime());
                                // var d1 = new Date('2008-02-14');
                                // var d2 = new Date('2019-10-05');

                                $scope.GetHisWork = $scope.getAge(new Date($scope.HistoryLeave.Profile[0].Date_In), new Date($scope.GetCurrentTime()));
                                // $scope.GetHisWork = $scope.getAge(d1, d2);
                                if ($scope.GetHisWork[0] >= 10) {
                                    $scope.MaxDateOff = 30;
                                } else {
                                    $scope.MaxDateOff = 20;
                                }
                                d1.setMonth(d1.getMonth() + 6);


                                if (d2 > new Date(d2.getUTCFullYear() + '-10-01')) {
                                    while (d1.getUTCFullYear() <= d2.getUTCFullYear() + 1) {
                                        $scope.dataLeaveResult = [];
                                        if ((d1.getMonth() + 1) < 10) {
                                            $scope.count_WorkSum = 0;
                                            $scope.count_DaySum += 10;
                                            CountBefore = $scope.count_DaySum;
                                            if ($scope.count_DaySum > $scope.MaxDateOff) {
                                                $scope.count_DaySum = $scope.MaxDateOff;
                                                CountBefore = $scope.count_DaySum;
                                                // $scope.count_DaySum -= 10;
                                            }
                                            if (d1.getFullYear() == '2019') {
                                                if ($scope.leaveStart.length > 0) {
                                                    if (LeaveT[0].options[LeaveT[0].selectedIndex].value == '2') {
                                                        $scope.count_DaySum = parseFloat($scope.leaveStart[0].DayRest_Leave);
                                                        CountBefore = parseFloat($scope.leaveStart[0].DayRest_Leave);
                                                        if ((parseFloat($scope.leaveStart[0].DayRest_Leave) - 10) > 0) {
                                                            if ($scope.LeaveMaxHis.length == 0) {
                                                                $scope.LeaveMaxHis.push({
                                                                    'Year': d1.getFullYear(),
                                                                    'Count': (parseFloat($scope.leaveStart[0].DayRest_Leave) - 10),
                                                                    'Count2': CountBefore,
                                                                    'LeaveHit': $scope.count_WorkSum,
                                                                    'Max': $scope.MaxDateOff
                                                                });
                                                            } else {
                                                                $scope.LeaveMaxHis[$scope.LeaveMaxHis.length - 1].Count = (parseFloat($scope.leaveStart[0].DayRest_Leave) - 10);
                                                            }
                                                        } else {
                                                            if ($scope.LeaveMaxHis.length == 0) {
                                                                $scope.LeaveMaxHis.push({
                                                                    'Year': d1.getFullYear(),
                                                                    'Count': 0,
                                                                    'Count2': CountBefore,
                                                                    'LeaveHit': $scope.count_WorkSum,
                                                                    'Max': $scope.MaxDateOff
                                                                });
                                                            } else {
                                                                $scope.LeaveMaxHis[$scope.LeaveMaxHis.length - 1].Count = 0;
                                                            }
                                                            // $scope.LeaveMaxHis[$scope.LeaveMaxHis.length - 1].Count = 0;
                                                        }
                                                    } else {
                                                        $scope.count_WorkSum += parseFloat($scope.leaveStart[0].DayWork_Leave);
                                                    }
                                                }
                                            } else {
                                                $scope.count_WorkSum = 0;
                                            }
                                            $scope.count_TimeLeave = 0;
                                            angular.forEach($scope.HistoryLeave.Leave2, function (Leave_Item) {
                                                if (Leave_Item.T_Leave_Type == LeaveT[0].options[LeaveT[0].selectedIndex].value) {
                                                    if (Leave_Item.T_Leave_Date_Start >= (d1.getFullYear() - 1) + '-10-01' && Leave_Item.T_Leave_Date_Start <= d1.getFullYear() + '-09-30') {
                                                        if (LeaveT[0].options[LeaveT[0].selectedIndex].value == 2) {
                                                            $scope.count_DaySum -= parseFloat(Leave_Item.T_Work_Summary);
                                                            $scope.count_WorkSum += parseFloat(Leave_Item.T_Work_Summary);
                                                            if (Leave_Item.T_Leave_Date_Start >= $scope.DateShowStart2) {
                                                                $scope.dataLeaveResult.push(Leave_Item);
                                                            }
                                                        } else {
                                                            $scope.count_WorkSum += parseFloat(Leave_Item.T_Work_Summary);
                                                            if ($scope.count_TimeLeave < parseFloat(Leave_Item.T_Leave_Time)) {
                                                                $scope.count_TimeLeave = parseFloat(Leave_Item.T_Leave_Time);
                                                            } else {}
                                                            if (Leave_Item.T_Leave_Date_Start >= $scope.DateShowStart2) {
                                                                $scope.dataLeaveResult.push(Leave_Item);
                                                            }
                                                        }
                                                    } else {
                                                        // $scope.count_TimeLeave = 0;
                                                        // $scope.dataLeaveResult = [];
                                                    }
                                                }
                                            })
                                            $scope.LeaveMaxHis.push({
                                                'Year': d1.getFullYear(),
                                                'Count': $scope.count_DaySum,
                                                'Count2': CountBefore,
                                                'LeaveHit': $scope.count_WorkSum,
                                                'Max': $scope.MaxDateOff
                                            });
                                        }
                                        d1.setUTCFullYear(d1.getUTCFullYear() + 1);
                                    }

                                } else {
                                    while (d1.getUTCFullYear() <= d2.getUTCFullYear()) {
                                        $scope.count_WorkSum = 0;
                                        $scope.count_DaySum += 10;
                                        CountBefore = $scope.count_DaySum;
                                        if ($scope.count_DaySum > $scope.MaxDateOff) {
                                            $scope.count_DaySum = $scope.MaxDateOff;
                                            CountBefore = $scope.count_DaySum;
                                            // $scope.count_DaySum -= 10;
                                        }
                                        if (d1.getFullYear() == '2019') {
                                            if ($scope.leaveStart.length > 0) {
                                                if (LeaveT[0].options[LeaveT[0].selectedIndex].value == '2') {
                                                    $scope.count_DaySum = parseFloat($scope.leaveStart[0].DayRest_Leave);
                                                    CountBefore = parseFloat($scope.leaveStart[0].DayRest_Leave);
                                                    if ((parseFloat($scope.leaveStart[0].DayRest_Leave) - 10) > 0) {
                                                        if ($scope.LeaveMaxHis.length == 0) {
                                                            $scope.LeaveMaxHis.push({
                                                                'Year': d1.getFullYear(),
                                                                'Count': (parseFloat($scope.leaveStart[0].DayRest_Leave) - 10),
                                                                'Count2': CountBefore,
                                                                'LeaveHit': $scope.count_WorkSum,
                                                                'Max': $scope.MaxDateOff
                                                            });
                                                        } else {
                                                            $scope.LeaveMaxHis[$scope.LeaveMaxHis.length - 1].Count = (parseFloat($scope.leaveStart[0].DayRest_Leave) - 10);
                                                        }
                                                    } else {
                                                        if ($scope.LeaveMaxHis.length == 0) {
                                                            $scope.LeaveMaxHis.push({
                                                                'Year': d1.getFullYear(),
                                                                'Count': 0,
                                                                'Count2': CountBefore,
                                                                'LeaveHit': $scope.count_WorkSum,
                                                                'Max': $scope.MaxDateOff
                                                            });
                                                        } else {
                                                            $scope.LeaveMaxHis[$scope.LeaveMaxHis.length - 1].Count = 0;
                                                        }
                                                    }
                                                } else {
                                                    $scope.count_WorkSum += parseFloat($scope.leaveStart[0].DayWork_Leave);
                                                }

                                            }
                                        } else {
                                            $scope.count_WorkSum = 0;
                                        }
                                        $scope.count_TimeLeave = 0;
                                        angular.forEach($scope.HistoryLeave.Leave2, function (Leave_Item) {
                                            if (Leave_Item.T_Leave_Type == LeaveT[0].options[LeaveT[0].selectedIndex].value) {
                                                if (Leave_Item.T_Leave_Date_Start >= (d1.getFullYear() - 1) + '-10-01' && Leave_Item.T_Leave_Date_Start <= d1.getFullYear() + '-09-30') {
                                                    if (LeaveT[0].options[LeaveT[0].selectedIndex].value == 2) {
                                                        $scope.count_DaySum -= parseFloat(Leave_Item.T_Work_Summary);
                                                        $scope.count_WorkSum += parseFloat(Leave_Item.T_Work_Summary);
                                                        if (Leave_Item.T_Leave_Date_Start >= $scope.DateShowStart2) {
                                                            $scope.dataLeaveResult.push(Leave_Item);
                                                        }
                                                    } else {
                                                        $scope.count_WorkSum += parseFloat(Leave_Item.T_Work_Summary);
                                                        if ($scope.count_TimeLeave < parseFloat(Leave_Item.T_Leave_Time)) {
                                                            $scope.count_TimeLeave = parseFloat(Leave_Item.T_Leave_Time);
                                                        } else {}
                                                        if (Leave_Item.T_Leave_Date_Start >= $scope.DateShowStart2) {
                                                            $scope.dataLeaveResult.push(Leave_Item);
                                                        }
                                                    }
                                                } else {
                                                    // console.log(d2.getFullYear());
                                                    // $scope.count_TimeLeave = parseFloat(Leave_Item.T_Leave_Time);
                                                    // if(Leave_Item.T_Leave_Date_Start.split('-')[0] == d1.getFullYear()){

                                                    // }
                                                    // $scope.count_TimeLeave = 0;
                                                    // $scope.dataLeaveResult = [];
                                                }

                                            }
                                        })
                                        $scope.LeaveMaxHis.push({
                                            'Year': d1.getFullYear(),
                                            'Count': $scope.count_DaySum,
                                            'Count2': CountBefore,
                                            'LeaveHit': $scope.count_WorkSum,
                                            'Max': $scope.MaxDateOff
                                        });
                                        // console.log($scope.LeaveMaxHis);
                                        d1.setUTCFullYear(d1.getUTCFullYear() + 1);
                                    }
                                }

                                $scope.OpenTable = true;
                            });
                    });
            }

            $scope.CheckLeaveHis = function (LeaveType, DateStart, DateEnd) {
                $scope.OpenTable = false;
                var LeaveT = LeaveType;
                $scope.count_DaySum2 = 0;
                $scope.dataLeaveResult2 = [];
                $scope.LeaveMaxHis2 = [];

                var DateNow = new Date();
                var Date_Start = '' + (DateNow.getUTCFullYear() - 1) + '-10-01';
                var Date_End = '' + DateNow.getUTCFullYear() + '-09-30';
                var CountBefore = 0;
                data = {
                    'PerId': <?php echo $_SESSION['PERID'] ?>,
                    'TypeLeave': LeaveType,
                };
                $http.post('./ApiService/T_GetLeveStart', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        $scope.leaveStart2 = response.data;
                        data = {
                            'PerId': <?php echo $_SESSION['PERID'] ?>,
                            'TypeLeave': LeaveType,
                            'DateCondi': Date_Start,
                        };
                        $http.post('./ApiService/T_GetHistoryLeaveUers', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                            .then(function successCallback(response) {
                                $scope.HistoryLeave3 = response.data;
                                var d1 = new Date($scope.HistoryLeave3.Profile[0].Date_In);
                                var d2 = new Date(DateStart);

                                $scope.GetHisWork2 = $scope.getAge(new Date($scope.HistoryLeave3.Profile[0].Date_In), new Date($scope.GetCurrentTime()));
                                if ($scope.GetHisWork2[0] >= 10) {
                                    $scope.MaxDateOff2 = 30;
                                } else {
                                    $scope.MaxDateOff2 = 20;
                                }
                                d1.setMonth(d1.getMonth() + 6);
                                if (d2 > new Date(d2.getUTCFullYear() + '-10-01')) {
                                    while (d1.getUTCFullYear() <= d2.getUTCFullYear() + 1) {
                                        $scope.count_WorkSum2 = 0;
                                        $scope.count_DaySum2 += 10;
                                        CountBefore = $scope.count_DaySum2;

                                        if ($scope.count_DaySum2 > $scope.MaxDateOff2) {
                                            $scope.count_DaySum2 = $scope.MaxDateOff2;
                                            CountBefore = $scope.count_DaySum2;
                                            // $scope.count_DaySum2 -= 10;
                                        }
                                        if (d1.getFullYear() == '2019') {
                                            if ($scope.leaveStart2.length > 0) {
                                                if (LeaveType == '2') {
                                                    $scope.count_DaySum2 = parseFloat($scope.leaveStart2[0].DayRest_Leave);
                                                    CountBefore = parseFloat($scope.leaveStart2[0].DayRest_Leave);
                                                    if ((parseFloat($scope.leaveStart2[0].DayRest_Leave) - 10) > 0) {
                                                        $scope.LeaveMaxHis2.push({
                                                            'Year': d1.getFullYear(),
                                                            'Count': (parseFloat($scope.leaveStart2[0].DayRest_Leave) - 10),
                                                            'Count2': CountBefore,
                                                            'LeaveHit': $scope.count_WorkSum2,
                                                            'Max': $scope.MaxDateOff2
                                                        });
                                                    } else {
                                                        $scope.LeaveMaxHis2.push({
                                                            'Year': d1.getFullYear(),
                                                            'Count': 0,
                                                            'Count2': CountBefore,
                                                            'LeaveHit': $scope.count_WorkSum2,
                                                            'Max': $scope.MaxDateOff2
                                                        });
                                                    }
                                                } else {
                                                    $scope.count_WorkSum2 += parseFloat($scope.leaveStart2[0].DayWork_Leave);
                                                }
                                            }
                                        } else {
                                            $scope.count_WorkSum2 = 0;
                                        }
                                        angular.forEach($scope.HistoryLeave3.Leave2, function (Leave_Item) {
                                            if (Leave_Item.T_Leave_Type == LeaveType) {
                                                if (Leave_Item.T_Leave_Date_Start >= (d1.getFullYear() - 1) + '-10-01' && Leave_Item.T_Leave_Date_Start <= d1.getFullYear() + '-09-30') {
                                                    if (LeaveType == 2) {
                                                        if (Leave_Item.T_Leave_Date_Start == DateStart && Leave_Item.T_Leave_Date_End == DateEnd) {} else {
                                                            if (Leave_Item.T_Leave_Date_Start < DateStart) {
                                                                $scope.count_DaySum2 -= parseFloat(Leave_Item.T_Work_Summary);
                                                                $scope.count_WorkSum2 += parseFloat(Leave_Item.T_Work_Summary);
                                                                $scope.dataLeaveResult2.push(Leave_Item);
                                                            } else {}
                                                        }
                                                    } else {
                                                        if (Leave_Item.T_Leave_Date_Start == DateStart && Leave_Item.T_Leave_Date_End == DateEnd) {} else {
                                                            if (Leave_Item.T_Leave_Date_Start < DateStart) {
                                                                $scope.count_WorkSum2 += parseFloat(Leave_Item.T_Work_Summary);
                                                                $scope.dataLeaveResult2.push(Leave_Item);
                                                            } else {}
                                                        }
                                                    }
                                                }

                                            }
                                        })
                                        $scope.LeaveMaxHis2.push({
                                            'Year': d1.getFullYear(),
                                            'Count': $scope.count_DaySum2,
                                            'Count2': CountBefore,
                                            'LeaveHit': $scope.count_WorkSum2,
                                            'Max': $scope.MaxDateOff2
                                        });
                                        $scope.sumLeaveDay = parseFloat($scope.DataForPrint.T_Work_Summary) + $scope.count_WorkSum2;
                                        d1.setUTCFullYear(d1.getUTCFullYear() + 1);
                                    }
                                } else {
                                    while (d1.getUTCFullYear() <= d2.getUTCFullYear()) {
                                        $scope.count_WorkSum2 = 0;
                                        $scope.count_DaySum2 += 10;
                                        CountBefore = $scope.count_DaySum2;

                                        if ($scope.count_DaySum2 > $scope.MaxDateOff2) {
                                            $scope.count_DaySum2 = $scope.MaxDateOff2;
                                            CountBefore = $scope.count_DaySum2;
                                            $scope.count_DaySum2 -= 10;
                                        }
                                        if (d1.getFullYear() == '2019') {
                                            if ($scope.leaveStart2.length > 0) {
                                                if (LeaveType == '2') {
                                                    $scope.count_DaySum2 = parseFloat($scope.leaveStart2[0].DayRest_Leave);
                                                    CountBefore = parseFloat($scope.leaveStart2[0].DayRest_Leave);
                                                    if ((parseFloat($scope.leaveStart2[0].DayRest_Leave) - 10) > 0) {
                                                        $scope.LeaveMaxHis2.push({
                                                            'Year': d1.getFullYear(),
                                                            'Count': (parseFloat($scope.leaveStart2[0].DayRest_Leave) - 10),
                                                            'Count2': CountBefore,
                                                            'LeaveHit': $scope.count_WorkSum2,
                                                            'Max': $scope.MaxDateOff2
                                                        });
                                                    } else {
                                                        $scope.LeaveMaxHis2.push({
                                                            'Year': d1.getFullYear(),
                                                            'Count': 0,
                                                            'Count2': CountBefore,
                                                            'LeaveHit': $scope.count_WorkSum2,
                                                            'Max': $scope.MaxDateOff2
                                                        });
                                                    }
                                                } else {
                                                    $scope.count_WorkSum2 += parseFloat($scope.leaveStart2[0].DayWork_Leave);
                                                }
                                            }
                                        } else {
                                            $scope.count_WorkSum2 = 0;
                                        }
                                        angular.forEach($scope.HistoryLeave3.Leave2, function (Leave_Item) {
                                            if (Leave_Item.T_Leave_Type == LeaveType) {
                                                if (Leave_Item.T_Leave_Date_Start >= (d1.getFullYear() - 1) + '-10-01' && Leave_Item.T_Leave_Date_Start <= d1.getFullYear() + '-09-30') {
                                                    if (LeaveType == 2) {
                                                        if (Leave_Item.T_Leave_Date_Start == DateStart && Leave_Item.T_Leave_Date_End == DateEnd) {} else {
                                                            if (Leave_Item.T_Leave_Date_Start < DateStart) {
                                                                $scope.count_DaySum2 -= parseFloat(Leave_Item.T_Work_Summary);
                                                                $scope.count_WorkSum2 += parseFloat(Leave_Item.T_Work_Summary);
                                                                $scope.dataLeaveResult2.push(Leave_Item);
                                                            } else {}
                                                        }
                                                    } else {
                                                        if (Leave_Item.T_Leave_Date_Start == DateStart && Leave_Item.T_Leave_Date_End == DateEnd) {} else {
                                                            if (Leave_Item.T_Leave_Date_Start < DateStart) {
                                                                $scope.count_WorkSum2 += parseFloat(Leave_Item.T_Work_Summary);
                                                                $scope.dataLeaveResult2.push(Leave_Item);
                                                            } else {}
                                                        }
                                                    }
                                                }

                                            }
                                        })
                                        $scope.LeaveMaxHis2.push({
                                            'Year': d1.getFullYear(),
                                            'Count': $scope.count_DaySum2,
                                            'Count2': CountBefore,
                                            'LeaveHit': $scope.count_WorkSum2,
                                            'Max': $scope.MaxDateOff2
                                        });
                                        // console.log($scope.LeaveMaxHis2);
                                        $scope.sumLeaveDay = parseFloat($scope.DataForPrint.T_Work_Summary) + $scope.count_WorkSum2;
                                        d1.setUTCFullYear(d1.getUTCFullYear() + 1);
                                    }

                                }
                                $scope.OpenTable = true;
                            });
                    });
            }

            //ลบข้อมูลการลาของบุคลากรที่เลือกจาก select
            $scope.DeleteData = function (ItemData) {

                Swal({
                    title: 'ยืนยันการทำรายการ',
                    text: "ต้องการลบข้อมูลการลา ของคุณ " + ItemData.NAME + " " + ItemData.SURNAME + " ใช่หรือไม่",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ตกลง',
                    cancelButtonText: 'ยกเลิก'
                }).then((result) => {
                    if (result.value) {
                        data = {
                            'No_Id': ItemData.No_Id,
                        };
                        $http.post('./ApiService/T_DeleteDataLeave', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                            .then(function successCallback(response) {
                                var result = response.data;
                                if (result.Status) {
                                    Swal({
                                        type: 'success',
                                        title: result.Message
                                    });
                                    var myElement = angular.element(document.querySelector('#PersonCode'));
                                    data = {
                                        'DepCode': $scope.DepartSelect,
                                        'PerId': myElement[0].options[myElement[0].selectedIndex].value
                                    };
                                    $http.post('./ApiService/T_GetLeavePersonData', data, {
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            }
                                        })
                                        .then(function successCallback(response) {
                                            $scope.LeaveDataPerson = response.data;
                                        });
                                } else {
                                    Swal({
                                        type: 'error',
                                        title: result.Message
                                    });
                                }
                            });
                    }
                });
            }

            // เรียกข้อมูลบุคลากรภายในหน่วยงานทั้งหมดภายใต้เงื่อนไข depcode จาก select html รหัสหน่วยงาน
            $scope.update = function () {
                // var myElement = angular.element(document.querySelector('#DepartCode'));
                // 'DepCode': myElement[0].options[myElement[0].selectedIndex].value,
                $scope.UpdateLeaveDate = [];
                $scope.TimetoCount = [];
                $scope.TimeSetPerson = [];
                var checkDayOff = false;
                var i0 = 0;
                var i1 = 0;
                var i0_N = 0;
                var i1_N = 0;
                var i2 = 0;
                var i3 = 0;
                var i4 = 0;
                var i5 = 0;
                var i6 = 0;
                var i7 = 0;
                data = {
                    'DepCode': $scope.DepartSelect,
                    'PerId': <?php echo $_SESSION['PERID'] ?>
                };
                $http.post('./ApiService/T_GetLeaveUserPersonData', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        $scope.LeaveDataPerson = response.data;
                        data = {
                            'DepCode': $scope.DepartSelect,
                            'PerId': <?php echo $_SESSION['PERID'] ?>
                        };
                        if ($scope.LeaveDataPerson.length > 0) {
                            $http.post('./ApiService/T_GetLeaveUserSet', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }).then(function successCallback(response) {
                                $scope.TimeSetPerson = response.data;
                                $scope.LeaveDataPerson.reverse();
                                var CheckI0 = true;
                                var CheckI1 = true;
                                angular.forEach($scope.LeaveDataPerson, function (Leave_Item) {

                                    $scope.CountDayOff = 0;
                                    $scope.CountDayOffAll = 0;
                                    if (Leave_Item.T_All_Summary == 0 || Leave_Item.T_All_Summary == null) {
                                        var Date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                        var Date2 = new Date(Leave_Item.T_Leave_Date_End);
                                        while (Date1 <= Date2) {
                                            var checkDayOff = false;
                                            if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) <= 9) {
                                                RangeDate = "0" + (Date1.getMonth() + 1) + '/' + "0" + Date1.getDate() + '/' + Date1.getFullYear();
                                                RangeDateEnd = "0" + (Date2.getMonth() + 1) + '/' + "0" + Date2.getDate() + '/' + Date2.getFullYear();
                                            } else if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) > 9) {
                                                RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                                RangeDateEnd = (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                            } else if (Date1.getDate() > 9 && (Date1.getMonth() + 1) <= 9) {
                                                RangeDate = "0" + (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                                RangeDateEnd = "0" + (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                            } else {
                                                RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                                RangeDateEnd = (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                            }

                                            var Day = RangeDate.split('/')[1];
                                            var Month = RangeDate.split('/')[0];
                                            var Year = RangeDate.split('/')[2];
                                            var RangeDateToDB = Year + "-" + Month + "-" + Day;
                                            var options = {
                                                weekday: 'long'
                                            };
                                            if (Date1.toLocaleDateString('en-US', options) == "Saturday" || Date1.toLocaleDateString('en-US', options) == "Sunday") {

                                            } else {
                                                angular.forEach($scope.Holiday, function (hoItem, ho_idx) {
                                                    if (hoItem.Date_stop == RangeDateToDB) {
                                                        checkDayOff = true;
                                                    }
                                                });
                                                if (!checkDayOff) {
                                                    $scope.CountDayOff++;
                                                }
                                            }
                                            $scope.CountDayOffAll++;
                                            Date1.setDate(Date1.getDate() + 1);
                                        }

                                        if (Leave_Item.T_Day_Type_Start == "ครึ่งวัน (เช้า)" && Leave_Item.T_Day_Type_End == "ครึ่งวัน (เช้า)") {
                                            $scope.CountDayOff -= 0.5;
                                        } else if (Leave_Item.T_Day_Type_Start == "ครึ่งวัน (บ่าย)" && Leave_Item.T_Day_Type_End == "ครึ่งวัน (บ่าย)") {
                                            $scope.CountDayOff -= 0.5;
                                        } else if (Leave_Item.T_Day_Type_Start == "ครึ่งวัน (เช้า)" && Leave_Item.T_Day_Type_End == "ครึ่งวัน (บ่าย)") {
                                            $scope.CountDayOff -= 1;
                                        } else if (Leave_Item.T_Day_Type_End == "ครึ่งวัน (เช้า)" && Leave_Item.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                            $scope.CountDayOff -= 1;
                                        } else if (Leave_Item.T_Day_Type_Start == "ครึ่งวัน (เช้า)" && Leave_Item.T_Day_Type_End == "เต็มวัน") {
                                            $scope.CountDayOff -= 0.5;
                                        } else if (Leave_Item.T_Day_Type_Start == "ครึ่งวัน (บ่าย)" && Leave_Item.T_Day_Type_End == "เต็มวัน") {
                                            $scope.CountDayOff -= 0.5;
                                        } else if (Leave_Item.T_Day_Type_End == "ครึ่งวัน (เช้า)" && Leave_Item.T_Day_Type_Start == "เต็มวัน") {
                                            $scope.CountDayOff -= 0.5;
                                        } else if (Leave_Item.T_Day_Type_End == "ครึ่งวัน (บ่าย)" && Leave_Item.T_Day_Type_Start == "เต็มวัน") {
                                            $scope.CountDayOff -= 0.5;
                                        }

                                        $scope.UpdateLeaveDate.push({
                                            'LeaveData': Leave_Item,
                                            'CountDayOff': $scope.CountDayOffAll,
                                            'CountDayWork': $scope.CountDayOff
                                        });
                                    }

                                    if ($scope.TimeSetPerson.length > 0) {
                                        if (Leave_Item.T_Status_Leave != 0) {
                                            // if (Leave_Item.T_Leave_Time == 0 || Leave_Item.T_Leave_Time == null) {
                                            if (<?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 1 || <?php echo $PersonalData["DataPerson"]["POS_LEVEL"]; ?> == 2) {
                                                if (Leave_Item.T_Leave_Date_Start > (parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0] - 1) + '-10-01') && Leave_Item.T_Leave_Date_Start < (Leave_Item.T_Leave_Date_Start.split('-')[0] + '-10-01')) {
                                                    ////////////////////////////////////////////////////
                                                    // เช็คจำนวนเดือน ไม่เกิน ครึ่งปีงบประมาณ ลาป่วย กับ ลากิจ
                                                    ///////////////////////////////////////////////////

                                                    if (parseInt(Leave_Item.T_Leave_Date_Start.split('-')[1]) < 4) {
                                                        switch (Leave_Item.T_Leave_Type) {
                                                            case '0':
                                                                if (CheckI0) {
                                                                    angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                        if (TimeItem.Leave_Type == 0) {
                                                                            if (Leave_Item.Fiscal_Year == '2019') {
                                                                                i0 = TimeItem.Time_Leave;
                                                                                CheckI0 = false;
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                                i0++;
                                                                if ($scope.TimetoCount.length > 0) {
                                                                    var countTime = 0;
                                                                    var CheckType = false;
                                                                    angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                        if (CountItem.LeaveType == 0 && CountItem.Fiscal_Year == parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) && CountItem.Fiscal_Year_Status == 1) {
                                                                            countTime = CountItem.LeaveTime
                                                                        } else {
                                                                            if (idx === $scope.TimetoCount.length - 1) {
                                                                                CheckType = true;
                                                                                $scope.TimetoCount.push({
                                                                                    'No_Id': Leave_Item.No_Id,
                                                                                    'LeaveTime': i0,
                                                                                    'LeaveType': 0,
                                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                    'Fiscal_Year_Status': 1
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                                    if (!CheckType) {
                                                                        $scope.TimetoCount.push({
                                                                            'No_Id': Leave_Item.No_Id,
                                                                            'LeaveTime': countTime + 1,
                                                                            'LeaveType': 0,
                                                                            'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                            'Fiscal_Year_Status': 1
                                                                        });
                                                                    }
                                                                } else {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': i0,
                                                                        'LeaveType': 0,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                        'Fiscal_Year_Status': 1
                                                                    });
                                                                }
                                                                break;
                                                            case '1':
                                                                if (CheckI1) {
                                                                    angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                        if (TimeItem.Leave_Type == 1) {
                                                                            if (Leave_Item.Fiscal_Year == '2019') {
                                                                                i1 = TimeItem.Time_Leave;
                                                                                CheckI1 = false;
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                                i1++;
                                                                if ($scope.TimetoCount.length > 0) {
                                                                    var countTime = 0;
                                                                    var CheckType = false;
                                                                    angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                        if (CountItem.LeaveType == 1 && CountItem.Fiscal_Year == parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) && CountItem.Fiscal_Year_Status == 1) {
                                                                            countTime = CountItem.LeaveTime
                                                                        } else {
                                                                            if (idx === $scope.TimetoCount.length - 1) {
                                                                                CheckType = true;
                                                                                $scope.TimetoCount.push({
                                                                                    'No_Id': Leave_Item.No_Id,
                                                                                    'LeaveTime': i1,
                                                                                    'LeaveType': 1,
                                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                    'Fiscal_Year_Status': 1
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                                    if (!CheckType) {
                                                                        $scope.TimetoCount.push({
                                                                            'No_Id': Leave_Item.No_Id,
                                                                            'LeaveTime': countTime + 1,
                                                                            'LeaveType': 1,
                                                                            'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                            'Fiscal_Year_Status': 1
                                                                        });
                                                                    }
                                                                } else {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': i1,
                                                                        'LeaveType': 1,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                        'Fiscal_Year_Status': 1
                                                                    });
                                                                }
                                                                break;
                                                        }
                                                    } else {
                                                        switch (Leave_Item.T_Leave_Type) {
                                                            case '0':
                                                                if (CheckI0) {
                                                                    angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                        if (TimeItem.Leave_Type == 0) {
                                                                            if (Leave_Item.Fiscal_Year == '2019') {
                                                                                i0 = TimeItem.Time_Leave;
                                                                                CheckI0 = false;
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                                i0++;

                                                                if ($scope.TimetoCount.length > 0) {
                                                                    var countTime = 0;
                                                                    var CheckType = false;
                                                                    angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                        if (CountItem.LeaveType == 0 && CountItem.Fiscal_Year == parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) && CountItem.Fiscal_Year_Status == 2) {
                                                                            countTime = CountItem.LeaveTime
                                                                        } else {
                                                                            if (idx === $scope.TimetoCount.length - 1) {
                                                                                CheckType = true;
                                                                                if (!CheckI0) {
                                                                                    $scope.TimetoCount.push({
                                                                                        'No_Id': Leave_Item.No_Id,
                                                                                        'LeaveTime': 1,
                                                                                        'LeaveType': 0,
                                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                        'Fiscal_Year_Status': 2
                                                                                    });
                                                                                } else {
                                                                                    $scope.TimetoCount.push({
                                                                                        'No_Id': Leave_Item.No_Id,
                                                                                        'LeaveTime': i0,
                                                                                        'LeaveType': 0,
                                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                        'Fiscal_Year_Status': 2
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                    if (!CheckType) {
                                                                        $scope.TimetoCount.push({
                                                                            'No_Id': Leave_Item.No_Id,
                                                                            'LeaveTime': countTime + 1,
                                                                            'LeaveType': 0,
                                                                            'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                            'Fiscal_Year_Status': 2
                                                                        });
                                                                    }
                                                                } else {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': i0,
                                                                        'LeaveType': 0,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                        'Fiscal_Year_Status': 2
                                                                    });
                                                                }
                                                                break;
                                                            case '1':
                                                                if (CheckI1) {
                                                                    angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                        if (TimeItem.Leave_Type == 1) {
                                                                            if (Leave_Item.Fiscal_Year == '2019') {
                                                                                i1 = TimeItem.Time_Leave;
                                                                                CheckI1 = false;
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                                i1++;

                                                                if ($scope.TimetoCount.length > 0) {
                                                                    var countTime = 0;
                                                                    var CheckType = false;
                                                                    angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                        if (CountItem.LeaveType == 1 && CountItem.Fiscal_Year == parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) && CountItem.Fiscal_Year_Status == 2) {
                                                                            countTime = CountItem.LeaveTime
                                                                        } else {
                                                                            if (idx === $scope.TimetoCount.length - 1) {
                                                                                CheckType = true;
                                                                                if (!CheckI1) {
                                                                                    $scope.TimetoCount.push({
                                                                                        'No_Id': Leave_Item.No_Id,
                                                                                        'LeaveTime': 1,
                                                                                        'LeaveType': 1,
                                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                        'Fiscal_Year_Status': 2
                                                                                    });
                                                                                } else {
                                                                                    $scope.TimetoCount.push({
                                                                                        'No_Id': Leave_Item.No_Id,
                                                                                        'LeaveTime': i1,
                                                                                        'LeaveType': 1,
                                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                        'Fiscal_Year_Status': 2
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                    if (!CheckType) {
                                                                        $scope.TimetoCount.push({
                                                                            'No_Id': Leave_Item.No_Id,
                                                                            'LeaveTime': countTime + 1,
                                                                            'LeaveType': 1,
                                                                            'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                            'Fiscal_Year_Status': 2
                                                                        });
                                                                    }
                                                                } else {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': i1,
                                                                        'LeaveType': 1,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                        'Fiscal_Year_Status': 2
                                                                    });
                                                                }
                                                                break;
                                                        }
                                                    }
                                                } else {
                                                    switch (Leave_Item.T_Leave_Type) {
                                                        case '0':
                                                            if (CheckI0) {
                                                                angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                    if (TimeItem.Leave_Type == 0) {
                                                                        if (Leave_Item.Fiscal_Year == '2019') {
                                                                            i0_N = TimeItem.Time_Leave;
                                                                            CheckI0 = false;
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            i0_N++;

                                                            if ($scope.TimetoCount.length > 0) {
                                                                var countTime = 0;
                                                                var CheckType = false;
                                                                angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                    if (CountItem.LeaveType == 0 && CountItem.Fiscal_Year == (parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1) && CountItem.Fiscal_Year_Status == 1) {
                                                                        countTime = CountItem.LeaveTime
                                                                    } else {
                                                                        if (idx === $scope.TimetoCount.length - 1) {
                                                                            CheckType = true;
                                                                            $scope.TimetoCount.push({
                                                                                'No_Id': Leave_Item.No_Id,
                                                                                'LeaveTime': i0_N,
                                                                                'LeaveType': 0,
                                                                                'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                                'Fiscal_Year_Status': 1
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                                if (!CheckType) {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': countTime + 1,
                                                                        'LeaveType': 0,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                        'Fiscal_Year_Status': 1
                                                                    });
                                                                }
                                                            } else {
                                                                $scope.TimetoCount.push({
                                                                    'No_Id': Leave_Item.No_Id,
                                                                    'LeaveTime': i0_N,
                                                                    'LeaveType': 0,
                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                    'Fiscal_Year_Status': 1
                                                                });
                                                            }

                                                            // $scope.TimetoCount.push({
                                                            //     'No_Id': Leave_Item.No_Id,
                                                            //     'LeaveTime': i0_N,
                                                            //     'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                            //     'Fiscal_Year_Status': 1
                                                            // });
                                                            break;
                                                        case '1':
                                                            if (CheckI1) {
                                                                angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                    if (TimeItem.Leave_Type == 1) {
                                                                        if (Leave_Item.Fiscal_Year == '2019') {
                                                                            i1_N = TimeItem.Time_Leave;
                                                                            CheckI1 = false;
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            i1_N++;

                                                            if ($scope.TimetoCount.length > 0) {
                                                                var countTime = 0;
                                                                var CheckType = false;
                                                                angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                    if (CountItem.LeaveType == 1 && CountItem.Fiscal_Year == (parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1) && CountItem.Fiscal_Year_Status == 1) {
                                                                        countTime = CountItem.LeaveTime
                                                                    } else {
                                                                        if (idx === $scope.TimetoCount.length - 1) {
                                                                            CheckType = true;
                                                                            $scope.TimetoCount.push({
                                                                                'No_Id': Leave_Item.No_Id,
                                                                                'LeaveTime': i1_N,
                                                                                'LeaveType': 1,
                                                                                'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                                'Fiscal_Year_Status': 1
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                                if (!CheckType) {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': countTime + 1,
                                                                        'LeaveType': 1,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                        'Fiscal_Year_Status': 1
                                                                    });
                                                                }
                                                            } else {
                                                                $scope.TimetoCount.push({
                                                                    'No_Id': Leave_Item.No_Id,
                                                                    'LeaveTime': i1_N,
                                                                    'LeaveType': 1,
                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                    'Fiscal_Year_Status': 1
                                                                });
                                                            }
                                                            // $scope.TimetoCount.push({
                                                            //     'No_Id': Leave_Item.No_Id,
                                                            //     'LeaveTime': i1_N,
                                                            //     'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                            //     'Fiscal_Year_Status': 1
                                                            // });
                                                            break;
                                                    }
                                                }
                                            } else {
                                                if (Leave_Item.T_Leave_Date_Start > (parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0] - 1) + '-10-01') && Leave_Item.T_Leave_Date_Start < (Leave_Item.T_Leave_Date_Start.split('-')[0] + '-10-01')) {
                                                    ////////////////////////////////////////////////////
                                                    // เช็คจำนวนเดือน ไม่เกิน ครึ่งปีงบประมาณ ลาป่วย กับ ลากิจ
                                                    ///////////////////////////////////////////////////

                                                    switch (Leave_Item.T_Leave_Type) {
                                                        case '0':
                                                            if (CheckI0) {
                                                                angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                    if (TimeItem.Leave_Type == 0) {
                                                                        if (Leave_Item.Fiscal_Year == '2019') {
                                                                            i0 = TimeItem.Time_Leave;
                                                                            CheckI0 = false;
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            i0++;
                                                            if ($scope.TimetoCount.length > 0) {
                                                                var countTime = 0;
                                                                var CheckType = false;
                                                                angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                    if (CountItem.LeaveType == 0 && CountItem.Fiscal_Year == parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) && CountItem.Fiscal_Year_Status == 0) {
                                                                        countTime = CountItem.LeaveTime
                                                                    } else {
                                                                        if (idx === $scope.TimetoCount.length - 1) {
                                                                            CheckType = true;
                                                                            $scope.TimetoCount.push({
                                                                                'No_Id': Leave_Item.No_Id,
                                                                                'LeaveTime': i0,
                                                                                'LeaveType': 0,
                                                                                'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                'Fiscal_Year_Status': 0
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                                if (!CheckType) {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': countTime + 1,
                                                                        'LeaveType': 0,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                        'Fiscal_Year_Status': 0
                                                                    });
                                                                }
                                                            } else {
                                                                $scope.TimetoCount.push({
                                                                    'No_Id': Leave_Item.No_Id,
                                                                    'LeaveTime': i0,
                                                                    'LeaveType': 0,
                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                    'Fiscal_Year_Status': 0
                                                                });
                                                            }
                                                            break;
                                                        case '1':
                                                            if (CheckI1) {
                                                                angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                    if (TimeItem.Leave_Type == 1) {
                                                                        if (Leave_Item.Fiscal_Year == '2019') {
                                                                            i1 = TimeItem.Time_Leave;
                                                                            CheckI1 = false;
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            i1++;
                                                            if ($scope.TimetoCount.length > 0) {
                                                                var countTime = 0;
                                                                var CheckType = false;
                                                                angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                    if (CountItem.LeaveType == 1 && CountItem.Fiscal_Year == parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) && CountItem.Fiscal_Year_Status == 0) {
                                                                        countTime = CountItem.LeaveTime
                                                                    } else {
                                                                        if (idx === $scope.TimetoCount.length - 1) {
                                                                            CheckType = true;
                                                                            $scope.TimetoCount.push({
                                                                                'No_Id': Leave_Item.No_Id,
                                                                                'LeaveTime': i1,
                                                                                'LeaveType': 1,
                                                                                'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                                'Fiscal_Year_Status': 0
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                                if (!CheckType) {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': countTime + 1,
                                                                        'LeaveType': 1,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                        'Fiscal_Year_Status': 0
                                                                    });
                                                                }
                                                            } else {
                                                                $scope.TimetoCount.push({
                                                                    'No_Id': Leave_Item.No_Id,
                                                                    'LeaveTime': i1,
                                                                    'LeaveType': 1,
                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]),
                                                                    'Fiscal_Year_Status': 0
                                                                });
                                                            }
                                                            break;
                                                    }

                                                } else {
                                                    switch (Leave_Item.T_Leave_Type) {
                                                        case '0':
                                                            if (CheckI0) {
                                                                angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                    if (TimeItem.Leave_Type == 0) {
                                                                        if (Leave_Item.Fiscal_Year == '2019') {
                                                                            i0_N = TimeItem.Time_Leave;
                                                                            CheckI0 = false;
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            i0_N++;

                                                            if ($scope.TimetoCount.length > 0) {
                                                                var countTime = 0;
                                                                var CheckType = false;
                                                                angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                    if (CountItem.LeaveType == 0 && CountItem.Fiscal_Year == (parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1) && CountItem.Fiscal_Year_Status == 0) {
                                                                        countTime = CountItem.LeaveTime
                                                                    } else {
                                                                        if (idx === $scope.TimetoCount.length - 1) {
                                                                            CheckType = true;
                                                                            $scope.TimetoCount.push({
                                                                                'No_Id': Leave_Item.No_Id,
                                                                                'LeaveTime': i0_N,
                                                                                'LeaveType': 0,
                                                                                'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                                'Fiscal_Year_Status': 0
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                                if (!CheckType) {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': countTime + 1,
                                                                        'LeaveType': 0,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                        'Fiscal_Year_Status': 0
                                                                    });
                                                                }
                                                            } else {
                                                                $scope.TimetoCount.push({
                                                                    'No_Id': Leave_Item.No_Id,
                                                                    'LeaveTime': i0_N,
                                                                    'LeaveType': 0,
                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                    'Fiscal_Year_Status': 0
                                                                });
                                                            }

                                                            break;
                                                        case '1':
                                                            if (CheckI1) {
                                                                angular.forEach($scope.TimeSetPerson, function (TimeItem) {
                                                                    if (TimeItem.Leave_Type == 1) {
                                                                        if (Leave_Item.Fiscal_Year == '2019') {
                                                                            i1_N = TimeItem.Time_Leave;
                                                                            CheckI1 = false;
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            i1_N++;

                                                            if ($scope.TimetoCount.length > 0) {
                                                                var countTime = 0;
                                                                var CheckType = false;
                                                                angular.forEach($scope.TimetoCount, function (CountItem, idx) {
                                                                    if (CountItem.LeaveType == 1 && CountItem.Fiscal_Year == (parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1) && CountItem.Fiscal_Year_Status == 0) {
                                                                        countTime = CountItem.LeaveTime
                                                                    } else {
                                                                        if (idx === $scope.TimetoCount.length - 1) {
                                                                            CheckType = true;
                                                                            $scope.TimetoCount.push({
                                                                                'No_Id': Leave_Item.No_Id,
                                                                                'LeaveTime': i1_N,
                                                                                'LeaveType': 1,
                                                                                'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                                'Fiscal_Year_Status': 0
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                                if (!CheckType) {
                                                                    $scope.TimetoCount.push({
                                                                        'No_Id': Leave_Item.No_Id,
                                                                        'LeaveTime': countTime + 1,
                                                                        'LeaveType': 1,
                                                                        'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                        'Fiscal_Year_Status': 0
                                                                    });
                                                                }
                                                            } else {
                                                                $scope.TimetoCount.push({
                                                                    'No_Id': Leave_Item.No_Id,
                                                                    'LeaveTime': i1_N,
                                                                    'LeaveType': 1,
                                                                    'Fiscal_Year': parseInt(Leave_Item.T_Leave_Date_Start.split('-')[0]) + 1,
                                                                    'Fiscal_Year_Status': 0
                                                                });
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                            switch (Leave_Item.T_Leave_Type) {
                                                case '2':
                                                    i2++;
                                                    $scope.TimetoCount.push({
                                                        'No_Id': Leave_Item.No_Id,
                                                        'LeaveTime': i2,
                                                        'Fiscal_Year': parseInt($scope.YearShow) - 543,
                                                        'Fiscal_Year_Status': 0
                                                    });
                                                    break;
                                                case '3':
                                                    i3++;
                                                    $scope.TimetoCount.push({
                                                        'No_Id': Leave_Item.No_Id,
                                                        'LeaveTime': i3,
                                                        'Fiscal_Year': parseInt($scope.YearShow) - 543,
                                                        'Fiscal_Year_Status': 0
                                                    });
                                                    break;
                                                case '4':
                                                    i4++;
                                                    $scope.TimetoCount.push({
                                                        'No_Id': Leave_Item.No_Id,
                                                        'LeaveTime': i4,
                                                        'Fiscal_Year': parseInt($scope.YearShow) - 543,
                                                        'Fiscal_Year_Status': 0
                                                    });
                                                    break;
                                                case '5':
                                                    i5++;
                                                    $scope.TimetoCount.push({
                                                        'No_Id': Leave_Item.No_Id,
                                                        'LeaveTime': i5,
                                                        'Fiscal_Year': parseInt($scope.YearShow) - 543,
                                                        'Fiscal_Year_Status': 0
                                                    });
                                                    break;
                                                case '6':
                                                    i6++;
                                                    $scope.TimetoCount.push({
                                                        'No_Id': Leave_Item.No_Id,
                                                        'LeaveTime': i6,
                                                        'Fiscal_Year': parseInt($scope.YearShow) - 543,
                                                        'Fiscal_Year_Status': 0
                                                    });
                                                    break;
                                                case '7':
                                                    i7++;
                                                    $scope.TimetoCount.push({
                                                        'No_Id': Leave_Item.No_Id,
                                                        'LeaveTime': i7,
                                                        'Fiscal_Year': parseInt($scope.YearShow) - 543,
                                                        'Fiscal_Year_Status': 0
                                                    });
                                                    break;
                                                default:
                                                    break;
                                            }
                                            // }
                                        }
                                    } else {
                                        Swal({
                                            type: 'error',
                                            title: 'ไม่พบการบันทึกข้อมูลการลาเริ่มต้น โปรดติดต่อผู้ดูแลระบบของหน่วยงาน เพื่อบันทึกข้อมูล'
                                        });
                                    }

                                });

                                if ($scope.UpdateLeaveDate.length > 0) {
                                    data = {
                                        'DepCode': $scope.DepartSelect,
                                        'PerId': <?php echo $_SESSION['PERID'] ?>,
                                        'DataLeaveAll': $scope.UpdateLeaveDate
                                    };
                                    $http.post('./ApiService/T_UpdateDateCount', data, {
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            }
                                        })
                                        .then(function successCallback(response) {
                                            var result = response.data;
                                            if (result.Status) {
                                                Swal({
                                                    type: 'success',
                                                    title: result.Message
                                                });
                                                $scope.CheckUpdateDayWork = true;
                                                $scope.update();
                                            } else {
                                                Swal({
                                                    type: 'error',
                                                    title: result.Message
                                                });
                                            }
                                        });
                                }

                                if ($scope.TimeSetPerson.length > 0) {
                                    if ($scope.TimetoCount.length > 0) {
                                        data = {
                                            'ObjLeaveTime': $scope.TimetoCount
                                        };
                                        $http.post('./ApiService/T_UpdateTime', data, {
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                }
                                            })
                                            .then(function successCallback(response) {
                                                var result = response.data;
                                                if (!$scope.CheckUpdateDayWork) {
                                                    data = {
                                                        'DepCode': $scope.DepartSelect,
                                                        'PerId': <?php echo $_SESSION['PERID'] ?>
                                                    };
                                                    $http.post('./ApiService/T_GetLeaveUserPersonData', data, {
                                                            headers: {
                                                                'Content-Type': 'application/x-www-form-urlencoded'
                                                            }
                                                        })
                                                        .then(function successCallback(response) {
                                                            $scope.LeaveDataPerson = response.data;
                                                        });
                                                }
                                                // if (result.Status) {
                                                //     Swal({
                                                //         type: 'success',
                                                //         title: result.Message
                                                //     });
                                                // } else {
                                                //     Swal({
                                                //         type: 'error',
                                                //         title: result.Message
                                                //     });
                                                // }
                                            });
                                    }
                                }

                                $scope.LeaveDataPerson.reverse();

                            });
                        }
                        $scope.Loading = false;
                    });
            }

            //เรียกดูข้อมูลการลาของบุคลากรที่เลือก จาก select html
            $scope.updatePerson = function () {
                var myElement = angular.element(document.querySelector('#PersonCode'));
                data = {
                    'DepCode': $scope.DepartSelect,
                    'PerId': myElement[0].options[myElement[0].selectedIndex].value
                };
                $http.post('./ApiService/T_GetLeavePersonData', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        $scope.LeaveDataPerson = response.data;
                    });
            }

            //บันทึกข้อมูลการลาของบุคลากรในหน่วยงาน
            $scope.SaveLeaveData = function () {
                $scope.CountDayOff = 0;
                $scope.CountDayOffAll = 0;
                var checkDayOff = false;
                var DepartCode = angular.element(document.querySelector('#DepartCode'));
                var DayTypeS = angular.element(document.querySelector('#DayTypeS'));
                var DayTypeE = angular.element(document.querySelector('#DayTypeE'));
                var LeaveT = angular.element(document.querySelector('#LeaveT'));
                var PerID = angular.element(document.querySelector('#PersonCode'));
                var Date_S = DateToDb("datepicker");
                var Date_E = DateToDb("datepicker2");

                // $scope.GetHisWork = $scope.getAge(new Date($scope.HistoryLeave.Profile[0].Date_In), new Date($scope.GetCurrentTime()));
                // $scope.dateInMonth = $scope.calArrayDate(new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0));

                if (!$scope.C_DayOff) {
                    $scope.LeaveDayOffWork = document.getElementById('LeaveDayOffWork').value;
                    $scope.LeaveDayOff = document.getElementById('LeaveDayOff').value;
                    var Date1 = new Date(Date_S);
                    var Date2 = new Date(Date_E);

                    if ((Date1.getMonth() + 1) < 10 && (Date2.getMonth() + 1) >= 10) {
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถทำการลาข้ามปีงบประมาณได้'
                        });
                    } else {

                        if (Date1.toLocaleDateString('en-US') == Date2.toLocaleDateString('en-US')) {
                            if (DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (เช้า)" || DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (บ่าย)") {
                                $scope.CountDayOff = 0.5;
                            } else {
                                $scope.CountDayOff = 1;
                            }
                        } else {
                            while (Date1 <= Date2) {

                                var checkDayOff = false;
                                if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) <= 9) {
                                    RangeDate = "0" + (Date1.getMonth() + 1) + '/' + "0" + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = "0" + (Date2.getMonth() + 1) + '/' + "0" + Date2.getDate() + '/' + Date2.getFullYear();
                                } else if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) > 9) {
                                    RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                } else if (Date1.getDate() > 9 && (Date1.getMonth() + 1) <= 9) {
                                    RangeDate = "0" + (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = "0" + (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                } else {
                                    RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                }

                                var Day = RangeDate.split('/')[1];
                                var Month = RangeDate.split('/')[0];
                                var Year = RangeDate.split('/')[2];
                                var RangeDateToDB = Year + "-" + Month + "-" + Day;
                                var options = {
                                    weekday: 'long'
                                };
                                if (Date1.toLocaleDateString('en-US', options) == "Saturday" || Date1.toLocaleDateString('en-US', options) == "Sunday") {

                                } else {
                                    angular.forEach($scope.Holiday, function (hoItem, ho_idx) {
                                        if (hoItem.Date_stop == RangeDateToDB) {
                                            checkDayOff = true;
                                        }
                                    });
                                    if (!checkDayOff) {
                                        $scope.CountDayOff++;
                                        if (Date1 >= Date2) {
                                            if (DayTypeE[0].options[DayTypeE[0].selectedIndex].value == "ครึ่งวัน (เช้า)" || DayTypeE[0].options[DayTypeE[0].selectedIndex].value == "ครึ่งวัน (บ่าย)") {
                                                $scope.CountDayOff -= 0.5;
                                            }
                                        }
                                    }
                                }
                                $scope.CountDayOffAll++;
                                Date1.setDate(Date1.getDate() + 1);
                            }

                            if (DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (เช้า)" || DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (บ่าย)") {
                                $scope.CountDayOff -= 0.5;
                            }

                        }

                        if (LeaveT[0].options[LeaveT[0].selectedIndex].value != '? undefined:undefined ?' && Date_S != 'undefined--undefined' && Date_E != 'undefined--undefined' && document.getElementById('LeaveDayOff').value != '' && document.getElementById('LeaveDayOffWork').value != '' &&
                            document.getElementById('LeaveR').value != '' && DayTypeS[0].options[DayTypeS[0].selectedIndex].value != '? undefined:undefined ?' && DayTypeE[0].options[DayTypeE[0].selectedIndex].value != '? undefined:undefined ?') {
                            Swal({
                                title: 'ยืนยันการทำรายการ',
                                text: "คุณต้องการบันทึกการลาใช่หรือไม่?",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'ยกเลิก',
                                confirmButtonText: 'ตกลง'
                            }).then((result) => {
                                if (result.value) {

                                    if (Date_S > Date_E) {
                                        Swal({
                                            type: 'error',
                                            title: 'ไม่สามารถทำรายการได้สำเร็จ...',
                                            text: 'วันที่การลาไม่ถูกต้อง'
                                        })
                                    } else {
                                        if (Date_S == Date_E) {
                                            data = {
                                                'Leave_TypeS': LeaveT[0].options[LeaveT[0].selectedIndex].value,
                                                'DayTypeStart': DayTypeS[0].options[DayTypeS[0].selectedIndex].value,
                                                'DayTypeEnd': DayTypeS[0].options[DayTypeS[0].selectedIndex].value,
                                                'Leave_Date_Start': Date_S,
                                                'Leave_Date_End': Date_E,
                                                'Leave_Reason': document.getElementById('LeaveR').value,
                                                'DepCode': $scope.DepartSelect,
                                                'PerId': <?php echo $_SESSION['PERID'] ?>,
                                                'PerId_Create': <?php echo $_SESSION['PERID'] ?>,
                                                'DateSum': 1,
                                                'DateSumWork': $scope.LeaveDayOffWork
                                            };
                                        } else {
                                            data = {
                                                'Leave_TypeS': LeaveT[0].options[LeaveT[0].selectedIndex].value,
                                                'DayTypeStart': DayTypeS[0].options[DayTypeS[0].selectedIndex].value,
                                                'DayTypeEnd': DayTypeE[0].options[DayTypeE[0].selectedIndex].value,
                                                'Leave_Date_Start': Date_S,
                                                'Leave_Date_End': Date_E,
                                                'Leave_Reason': document.getElementById('LeaveR').value,
                                                'DepCode': $scope.DepartSelect,
                                                'PerId': <?php echo $_SESSION['PERID'] ?>,
                                                'PerId_Create': <?php echo $_SESSION['PERID'] ?>,
                                                'DateSum': $scope.LeaveDayOff,
                                                'DateSumWork': $scope.LeaveDayOffWork
                                            };
                                        }


                                        $http.post('./ApiService/T_InsertDataLeaveUser', data, {
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                }
                                            })
                                            .then(function successCallback(response) {
                                                var result = response.data;
                                                document.getElementById('LeaveR').value = '';
                                                document.getElementById('LeaveDayOff').value = '';
                                                document.getElementById('LeaveDayOffWork').value = '';
                                                if (result.Status) {
                                                    Swal({
                                                        type: 'success',
                                                        title: result.Message
                                                    });
                                                    data = {
                                                        'DepCode': $scope.DepartSelect,
                                                        'PerId': <?php echo $_SESSION['PERID'] ?>
                                                    };
                                                    $http.post('./ApiService/T_GetLeaveUserPersonData', data, {
                                                            headers: {
                                                                'Content-Type': 'application/x-www-form-urlencoded'
                                                            }
                                                        })
                                                        .then(function successCallback(response) {
                                                            $scope.LeaveDataPerson = [];
                                                            $scope.LeaveDataPerson = response.data;
                                                        });
                                                } else {
                                                    Swal({
                                                        type: 'error',
                                                        title: result.Message
                                                    });
                                                }
                                            });
                                    }
                                }
                            });
                        } else {
                            Swal({
                                type: 'error',
                                title: 'ไม่สามารถทำรายการได้สำเร็จ...',
                                text: 'โปรดกรอกข้อมูลให้ครบถ้วน'
                            })
                        }
                    }
                } else {
                    $scope.LeaveDayOffWork = document.getElementById('LeaveDayOffWork').value;
                    $scope.LeaveDayOff = document.getElementById('LeaveDayOff').value;
                    var Date1 = new Date(Date_S);
                    var Date2 = new Date(Date_E);

                    if ((Date1.getMonth() + 1) < 10 && (Date2.getMonth() + 1) >= 10) {
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถทำการลาข้ามปีงบประมาณได้'
                        });
                    } else {

                        if (Date1.toLocaleDateString('en-US') == Date2.toLocaleDateString('en-US')) {
                            if (DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (เช้า)" || DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (บ่าย)") {
                                $scope.CountDayOff = 0.5;
                            } else {
                                $scope.CountDayOff = 1;
                            }
                        } else {
                            while (Date1 <= Date2) {

                                var checkDayOff = false;
                                if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) <= 9) {
                                    RangeDate = "0" + (Date1.getMonth() + 1) + '/' + "0" + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = "0" + (Date2.getMonth() + 1) + '/' + "0" + Date2.getDate() + '/' + Date2.getFullYear();
                                } else if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) > 9) {
                                    RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                } else if (Date1.getDate() > 9 && (Date1.getMonth() + 1) <= 9) {
                                    RangeDate = "0" + (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = "0" + (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                } else {
                                    RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                                    RangeDateEnd = (Date2.getMonth() + 1) + '/' + Date2.getDate() + '/' + Date2.getFullYear();
                                }

                                var Day = RangeDate.split('/')[1];
                                var Month = RangeDate.split('/')[0];
                                var Year = RangeDate.split('/')[2];
                                var RangeDateToDB = Year + "-" + Month + "-" + Day;
                                var options = {
                                    weekday: 'long'
                                };
                                if (Date1.toLocaleDateString('en-US', options) == "Saturday" || Date1.toLocaleDateString('en-US', options) == "Sunday") {} else {
                                    angular.forEach($scope.Holiday, function (hoItem, ho_idx) {
                                        if (hoItem.Date_stop == RangeDateToDB) {
                                            checkDayOff = true;
                                        }
                                    });
                                    if (!checkDayOff) {
                                        $scope.CountDayOff++;
                                        if (Date1 >= Date2) {
                                            if (DayTypeE[0].options[DayTypeE[0].selectedIndex].value == "ครึ่งวัน (เช้า)" || DayTypeE[0].options[DayTypeE[0].selectedIndex].value == "ครึ่งวัน (บ่าย)") {
                                                $scope.CountDayOff -= 0.5;
                                            }
                                        }
                                    }
                                }
                                $scope.CountDayOffAll++;
                                Date1.setDate(Date1.getDate() + 1);
                            }

                            if (DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (เช้า)" || DayTypeS[0].options[DayTypeS[0].selectedIndex].value == "ครึ่งวัน (บ่าย)") {
                                $scope.CountDayOff -= 0.5;
                            }

                        }

                        if (LeaveT[0].options[LeaveT[0].selectedIndex].value != '? undefined:undefined ?' && Date_S != 'undefined--undefined' && Date_E != 'undefined--undefined' &&
                            document.getElementById('LeaveR').value != '' && DayTypeS[0].options[DayTypeS[0].selectedIndex].value != '? undefined:undefined ?' && DayTypeE[0].options[DayTypeE[0].selectedIndex].value != '? undefined:undefined ?') {
                            Swal({
                                title: 'ยืนยันการทำรายการ',
                                text: "คุณต้องการบันทึกการลาใช่หรือไม่?",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'ยกเลิก',
                                confirmButtonText: 'ตกลง'
                            }).then((result) => {
                                if (result.value) {

                                    if (Date_S > Date_E) {
                                        Swal({
                                            type: 'error',
                                            title: 'ไม่สามารถทำรายการได้สำเร็จ...',
                                            text: 'วันที่การลาไม่ถูกต้อง'
                                        })
                                    } else {
                                        if (Date_S == Date_E) {
                                            data = {
                                                'Leave_TypeS': LeaveT[0].options[LeaveT[0].selectedIndex].value,
                                                'DayTypeStart': DayTypeS[0].options[DayTypeS[0].selectedIndex].value,
                                                'DayTypeEnd': DayTypeS[0].options[DayTypeS[0].selectedIndex].value,
                                                'Leave_Date_Start': Date_S,
                                                'Leave_Date_End': Date_E,
                                                'Leave_Reason': document.getElementById('LeaveR').value,
                                                'DepCode': $scope.DepartSelect,
                                                'PerId': <?php echo $_SESSION['PERID'] ?>,
                                                'PerId_Create': <?php echo $_SESSION['PERID'] ?>,
                                                'DateSum': 1,
                                                'DateSumWork': $scope.CountDayOff
                                            };
                                        } else {
                                            data = {
                                                'Leave_TypeS': LeaveT[0].options[LeaveT[0].selectedIndex].value,
                                                'DayTypeStart': DayTypeS[0].options[DayTypeS[0].selectedIndex].value,
                                                'DayTypeEnd': DayTypeE[0].options[DayTypeE[0].selectedIndex].value,
                                                'Leave_Date_Start': Date_S,
                                                'Leave_Date_End': Date_E,
                                                'Leave_Reason': document.getElementById('LeaveR').value,
                                                'DepCode': $scope.DepartSelect,
                                                'PerId': <?php echo $_SESSION['PERID'] ?>,
                                                'PerId_Create': <?php echo $_SESSION['PERID'] ?>,
                                                'DateSum': $scope.CountDayOffAll,
                                                'DateSumWork': $scope.CountDayOff
                                            };
                                        }

                                        $http.post('./ApiService/T_InsertDataLeaveUser', data, {
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                }
                                            })
                                            .then(function successCallback(response) {
                                                var result = response.data;
                                                document.getElementById('LeaveR').value = '';
                                                document.getElementById('LeaveDayOff').value = '';
                                                document.getElementById('LeaveDayOffWork').value = '';
                                                if (result.Status) {
                                                    Swal({
                                                        type: 'success',
                                                        title: result.Message
                                                    });
                                                    data = {
                                                        'DepCode': $scope.DepartSelect,
                                                        'PerId': <?php echo $_SESSION['PERID'] ?>
                                                    };
                                                    $http.post('./ApiService/T_GetLeaveUserPersonData', data, {
                                                            headers: {
                                                                'Content-Type': 'application/x-www-form-urlencoded'
                                                            }
                                                        })
                                                        .then(function successCallback(response) {
                                                            $scope.LeaveDataPerson = [];
                                                            $scope.LeaveDataPerson = response.data;
                                                        });
                                                } else {
                                                    Swal({
                                                        type: 'error',
                                                        title: result.Message
                                                    });
                                                }
                                            });
                                    }
                                }
                            });
                        } else {
                            Swal({
                                type: 'error',
                                title: 'ไม่สามารถทำรายการได้สำเร็จ...',
                                text: 'โปรดกรอกข้อมูลให้ครบถ้วน'
                            })
                        }
                    }
                }
            }

            $scope.PrintForm = function (leaveData) {
                if (leaveData == '') {
                    if ($scope.PersonArSelect == null) {
                        Swal({
                            type: 'error',
                            title: 'กรุณาเลือกผู้อนุมัติใบลา'
                        });
                    } else {
                        angular.forEach($scope.PersonAppr, function (PersonArItem) {
                            if (PersonArItem.PERID == $scope.PersonArSelect) {
                                $scope.ManagerAppr = PersonArItem;
                            }
                        });
                        angular.forEach($scope.PersonCommit, function (PersonAllItem) {
                            if (PersonAllItem.PERID == $scope.approverSelect) {
                                $scope.AgentPerson = PersonAllItem;
                            }
                        });
                        $scope.CheckLeaveHis($scope.DataForPrint.T_Leave_Type, $scope.DataForPrint.T_Leave_Date_Start, $scope.DataForPrint.T_Leave_Date_End);
                        demo($scope.DataForPrint.T_Leave_Type);
                    }
                } else {
                    $scope.DataForPrint = leaveData;
                    $scope.HistoryLeave2 = [];
                    var Date1 = new Date(leaveData.T_Leave_Date_Start);
                    if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) <= 9) {
                        RangeDate = "0" + (Date1.getMonth() + 1) + '/' + "0" + Date1.getDate() + '/' + Date1.getFullYear();
                    } else if (Date1.getDate() <= 9 && (Date1.getMonth() + 1) > 9) {
                        RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                    } else if (Date1.getDate() > 9 && (Date1.getMonth() + 1) <= 9) {
                        RangeDate = "0" + (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                    } else {
                        RangeDate = (Date1.getMonth() + 1) + '/' + Date1.getDate() + '/' + Date1.getFullYear();
                    }

                    var Day = RangeDate.split('/')[1];
                    var Month = RangeDate.split('/')[0];
                    var Year = RangeDate.split('/')[2];
                    var RangeDateToDB = Year + "-" + Month + "-" + Day;
                    var options = {
                        weekday: 'long'
                    };
                    data = {
                        'PerId': <?php echo $_SESSION['PERID'] ?>,
                        'DepCode': $scope.DepartSelect,
                        'TypeLeave': leaveData.T_Leave_Type,
                        'DateCondi': RangeDateToDB,
                    };
                    $http.post('./ApiService/GetHistoryLeaveLast', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                        .then(function successCallback(response) {
                            $scope.HistoryLeave2 = response.data;
                            $scope.PersonAppr = [];
                            if ($scope.HistoryLeave2.PersonApprove.length == 0) {
                                Swal({
                                    type: 'error',
                                    title: 'ไม่มีข้อมูลผู้อนุมัติ โปรดตั้งค่าผู้อนุมัติข้อมูลใบลา'
                                });
                            } else {
                                // if () {
                                $scope.PersonAppr.push($scope.HistoryLeave2.PersonApprove[0]);
                                $scope.PersonAppr.push($scope.HistoryLeave2.PersonApprove[1]);
                                // } else {
                                //     $scope.CheckLeaveHis(leaveData.T_Leave_Type, leaveData.T_Leave_Date_Start, leaveData.T_Leave_Date_End);
                                //     demo(leaveData.T_Leave_Type);
                                // }
                            }
                        });
                }
            }

            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }

            async function demo(Type) {
                await sleep(1000);
                // var innerContents = document.getElementById("printSectionId2").innerHTML;
                // var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
                //     "xmlns:w='urn:schemas-microsoft-com:office:word' " +
                //     "xmlns='http://www.w3.org/TR/REC-html40'>" +
                //     "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
                // var footer = "</body></html>";
                // var sourceHTML = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word xmlns="http://www.w3.org/TR/REC-html40"><head><link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css"><link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css"><link rel="stylesheet" type="text/css" href="style.css" /><style>@import url(./tools/fonts/thsarabunnew.css);@media print {.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {float: left;}.col-sm-12 {width: 100%;}.col-sm-11 {width: 91.66666667%;}.col-sm-10 {width: 83.33333333%;}.col-sm-9 {width: 75%;}.col-sm-8 {width: 66.66666667%;}.col-sm-7 {width: 58.33333333%;}.col-sm-6 {width: 50%;}.col-sm-5 {width: 41.66666667%;}.col-sm-4 {width: 33.33333333%;}.col-sm-3 {width: 25%;}.col-sm-2 {width: 16.66666667%;}.col-sm-1 {width: 8.33333333%;}}@page {size: A4;margin: 0;}</style></head><body onload="window.print()">' + innerContents + '<script src="./tools/bower_components/jquery/dist/jquery.min.js" /><script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js" /></body></html>';

                // var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
                // var fileDownload = document.createElement("a");
                // document.body.appendChild(fileDownload);
                // fileDownload.href = source;
                // fileDownload.download = 'document.doc';
                // fileDownload.click();
                // document.body.removeChild(fileDownload);
                if (Type == 2) {
                    var innerContents = document.getElementById("printSectionId2").innerHTML;
                    // var popupWinindow = window.open('', '_blank', 'width=1280,height=768,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    // window.document.open('', '_blank', 'width=1280,height=768,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    window.document.write('<html><head><link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css"><link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css"><link rel="stylesheet" type="text/css" href="style.css" /><style>@import url(./tools/fonts/thsarabunnew.css);@media print {.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {float: left;}.col-sm-12 {width: 100%;}.col-sm-11 {width: 91.66666667%;}.col-sm-10 {width: 83.33333333%;}.col-sm-9 {width: 75%;}.col-sm-8 {width: 66.66666667%;}.col-sm-7 {width: 58.33333333%;}.col-sm-6 {width: 50%;}.col-sm-5 {width: 41.66666667%;}.col-sm-4 {width: 33.33333333%;}.col-sm-3 {width: 25%;}.col-sm-2 {width: 16.66666667%;}.col-sm-1 {width: 8.33333333%;}}@page {size: A4;margin: 0;}</style></head><body onload="window.print()">' + innerContents + '<script src="./tools/bower_components/jquery/dist/jquery.min.js" /><script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js" /></body></html>');
                    window.document.close();
                } else {
                    var innerContents = document.getElementById("printSectionId").innerHTML;
                    // var popupWinindow = window.open('', '_blank', 'width=1280,height=768,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    // window.document.open('', '_blank', 'width=1280,height=768,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    window.document.write('<html><head><link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css"><link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css"><link rel="stylesheet" type="text/css" href="style.css" /><style>@import url(./tools/fonts/thsarabunnew.css);@media print {.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {float: left;}.col-sm-12 {width: 100%;}.col-sm-11 {width: 91.66666667%;}.col-sm-10 {width: 83.33333333%;}.col-sm-9 {width: 75%;}.col-sm-8 {width: 66.66666667%;}.col-sm-7 {width: 58.33333333%;}.col-sm-6 {width: 50%;}.col-sm-5 {width: 41.66666667%;}.col-sm-4 {width: 33.33333333%;}.col-sm-3 {width: 25%;}.col-sm-2 {width: 16.66666667%;}.col-sm-1 {width: 8.33333333%;}}@page {size: A4;margin: 0;}</style></head><body onload="window.print()">' + innerContents + '<script src="./tools/bower_components/jquery/dist/jquery.min.js" /><script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js" /></body></html>');
                    window.document.close();
                }
            }

            $scope.open2 = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: './Views/Page/modalTakeALeaveUser.html',
                    controller: 'ModalInstanceCtrl',
                    animation: true,
                    resolve: {
                        params: function () {
                            return {
                                item_D: $scope.ObjDataItem
                            };
                        }
                    }
                });
                modalInstance.result.then(
                    function (result) {
                        if (result) {
                            var myElement = angular.element(document.querySelector('#PersonCode'));
                            data = {
                                'DepCode': $scope.DepartSelect,
                                'PerId': myElement[0].options[myElement[0].selectedIndex].value
                            };
                            $http.post('./ApiService/T_GetLeavePersonData', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                .then(function successCallback(response) {
                                    $scope.LeaveDataPerson = response.data;
                                });
                        }
                        // console.log('called $modalInstance.close()');
                        // console.log(result);
                    },
                    function (result) {
                        // console.log('called $modalInstance.dismiss()');
                        // console.log(result);
                        // alert(result);
                    }
                );
            };

            $scope.showDetails2 = function (item) {
                $scope.ObjDataItem = item;
                $scope.open2();
            }

            $scope.getAge = function (date_1, date_2) {

                //convert to UTC
                var arrayWorkSummary = [];
                var date2_UTC = new Date(Date.UTC(date_2.getUTCFullYear(), date_2.getUTCMonth(), date_2.getUTCDate()));
                var date1_UTC = new Date(Date.UTC(date_1.getUTCFullYear(), date_1.getUTCMonth(), date_1.getUTCDate()));


                var yAppendix, mAppendix, dAppendix;


                //--------------------------------------------------------------
                var days = date2_UTC.getDate() - date1_UTC.getDate();
                if (days < 0) {

                    date2_UTC.setMonth(date2_UTC.getMonth() - 1);
                    days += $scope.DaysInMonth(date2_UTC);
                }
                //--------------------------------------------------------------
                var months = date2_UTC.getMonth() - date1_UTC.getMonth();
                if (months < 0) {
                    date2_UTC.setFullYear(date2_UTC.getFullYear() - 1);
                    months += 12;
                }
                //--------------------------------------------------------------
                var years = date2_UTC.getFullYear() - date1_UTC.getFullYear();




                if (years > 1) yAppendix = " years";
                else yAppendix = " year";
                if (months > 1) mAppendix = " months";
                else mAppendix = " month";
                if (days > 1) dAppendix = " days";
                else dAppendix = " day";

                arrayWorkSummary.push(years);
                arrayWorkSummary.push(months);
                arrayWorkSummary.push(days);

                return arrayWorkSummary;
                // return years + yAppendix + ", " + months + mAppendix + ", and " + days + dAppendix + " old.";
            }


            $scope.DaysInMonth = function (date2_UTC) {
                var monthStart = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth(), 1);
                var monthEnd = new Date(date2_UTC.getFullYear(), date2_UTC.getMonth() + 1, 1);
                var monthLength = (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
                return monthLength;
            }

            $scope.GetTimeToDb = function (date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1;

                var yyyy = date.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return date = yyyy + '-' + mm + '-' + dd;
            }

            $scope.calArrayDate = function (fday, lday) {
                var firstDay2 = new Date(fday);
                var lastDay = new Date(lday);
                var daysOfYear = [];
                var options = {
                    weekday: 'long'
                };
                for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                    daysOfYear.push({
                        'DAY': $scope.GetTimeToDb(d),
                        'DAY_NAME': d.toLocaleDateString('en-US', options),
                        'STATUS': '-'
                    });
                }
                return daysOfYear;
            }

            $scope.GetCurrentTime = function () {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!

                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return today = yyyy + '-' + mm + '-' + dd;
            }
        });

        app.controller('ModalInstanceCtrl', ['$scope', '$http', '$sce', '$uibModalInstance', 'params', function ($scope, $http, $sce, $uibModalInstance, params) {
            $scope.LeaveType = <?php echo $this->LeaveType; ?>;
            $scope.DayType = <?php echo $this->DayType; ?>;
            $scope.Name = params.item_D.NAME + " " + params.item_D.SURNAME;
            $scope.LeaveTSelect = params.item_D.T_Leave_Type;
            var Year = params.item_D.T_Leave_Date_Start.split('-')[1];
            var Day = params.item_D.T_Leave_Date_Start.split('-')[0];
            var Month = params.item_D.T_Leave_Date_Start.split('-')[2];
            var startDate = Year + "/" + Month + "/" +
                Day;
            Year = params.item_D.T_Leave_Date_End.split('-')[1];
            Day = params.item_D.T_Leave_Date_End.split('-')[0];
            Month = params.item_D.T_Leave_Date_End.split('-')[2];
            var endDate = Year + "/" + Month + "/" +
                Day;
            $scope.DateStart = startDate;
            $scope.DateEnd = endDate;
            $scope.Details = params.item_D.T_Leave_Reason;
            $scope.Status = params.item_D.T_Status_Leave;
            $scope.DateCreate = params.item_D.T_Leave_CreateT;
            $scope.DateUpdate_M = params.item_D.T_Leave_UpdateT;
            $scope.LeaveS_Select = params.item_D.T_Day_Type_Start;
            $scope.LeaveE_Select = params.item_D.T_Day_Type_End;
            $scope.DayAllLeave = params.item_D.T_All_Summary;
            // $scope.DayWorkLeave = params.item_D.T_Work_Summary;

            if (params.item_D.T_Work_Summary != undefined) {
                var num1 = params.item_D.T_Work_Summary.split('.')[0];
                var num2 = params.item_D.T_Work_Summary.split('.')[1];
                if (num2 == 0) {
                    $scope.DayWorkLeave = num1;
                } else {
                    $scope.DayWorkLeave = params.item_D.T_Work_Summary;
                }
            }

            $scope.CloseModal = function () {
                $uibModalInstance.close();
            }

            $scope.UpdateLeaveData = function () {

                if ($scope.ChangeDate($scope.DateStart) > $scope.ChangeDate($scope.DateEnd)) {
                    Swal({
                        type: 'error',
                        title: 'พบข้อผิดพลาด',
                        text: 'โปรดตรวจสอบข้อมูลวันที่อีกครั้ง'
                    })
                } else {
                    data = {
                        'PERID_PERSON': params.item_D.PERID,
                        'No_Id': params.item_D.No_Id,
                        'LeaveTSelect': $scope.LeaveTSelect,
                        'DateStart': $scope.ChangeDate($scope.DateStart),
                        'LeaveS_Select': $scope.LeaveS_Select,
                        'DateEnd': $scope.ChangeDate($scope.DateEnd),
                        'LeaveE_Select': $scope.LeaveE_Select,
                        'Details': $scope.Details,
                        'PERID': <?php echo $_SESSION['PERID'] ?>
                    };
                    $http.post('./ApiService/T_UpdateLeavePerson', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                        .then(function successCallback(response) {
                            var result = response.data;
                            if (result.Status) {
                                Swal({
                                    type: 'success',
                                    title: result.Message
                                });
                                $uibModalInstance.close(result.Status);
                            } else {
                                Swal({
                                    type: 'error',
                                    title: result.Message
                                });
                                $uibModalInstance.close();
                            }
                        });
                }
            }

            $scope.ChangeDate = function (DateUpdate) {
                var Day = DateUpdate.split('/')[1];
                var Month = DateUpdate.split('/')[0];
                var Year = DateUpdate.split('/')[2];
                return Year + "-" + Month + "-" + Day;
            }
        }]);

        app.filter('yearToTh', function () {
            return function (value) {
                var YearTh = "";
                if (value != undefined) {
                    YearTh = parseInt(value) + 543;
                }
                return (
                    YearTh
                );
            }
        });

        app.filter('DateThai', function () {
            return function (value) {
                var DateTrans = "";
                if (value != undefined) {
                    var Year = value.split('-')[0];
                    var Month = value.split('-')[1];
                    var Day = value.split('-')[2];
                    Year = parseInt(Year) + 543;
                    var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                        'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                    ];
                    DateTrans = parseInt(Day) + " " + monthNames[parseInt(Month) - 1] + " " +
                        Year;
                }
                return (
                    DateTrans
                );
            }
        });
        app.filter('NumDeci', function () {
            return function (value) {
                var DateTrans = "";
                if (value != undefined) {
                    var num1 = value.split('.')[0];
                    var num2 = value.split('.')[1];
                    if (num2 == 0) {
                        DateTrans = num1;
                    } else {
                        return value
                    }
                }
                return (
                    DateTrans
                );
            }
        });
    </script>
</body>

</html>