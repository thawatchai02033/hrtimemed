<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="./tools/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="./tools/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .table-responsive {
        min-height: .01%;
        overflow-x: hidden;
    }
    div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
        padding-right: 0;
        overflow-x: auto;
    }
    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
    border-bottom-width: 0;
    /* width: 100px; */
    white-space: nowrap;
    width: 1%;
    }
    .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 0;
    white-space: nowrap;
    width: 1%;
    }
</style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div id="wrapper" class="wrapper">

        <?php require('./Views/Header.php') ?>
        <!-- Left side column. contains the logo and sidebar -->
        <?php require('./Views/Menu.php') ?>

        <!-- Content Wrapper. Contains page content -->
        <div id="page-wrapper" class="content-wrapper">
            <section class="content-header">
                <h1>
                    สรุปสถิติปฏิบัติงานรายบุคคล
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-pie-chart"></i> สถิติการมาทำงาน</a></li>
                    <li class="active">รายบุคคล</li>
                </ol>
            </section>
            <!-- /.row -->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box box-primary">
                            <div class="panel panel-default">
                                <div class="box-header">
                                    <h3 class="box-title">ตารางปฏิทินการปฏิบัติงานรายบุคคล</h3>
                                </div>

                                <div class="box-body">
                                    <div class="form-group">
                                        <div class="col-lg-3" style="text-align: center">
                                            <label>เรียกดูประจำรายบุคคล:</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input class="form-control" type="text" placeholder="รหัสบุคลากร" />
                                        </div>
                                        <div class="col-lg-3"></div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <!-- Date -->
                                    <div class="form-group">
                                        <div class="col-lg-3" style="text-align: center">
                                            <label>ช่วงวันที่:</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="reservation">
                                            </div>
                                        </div>
                                        <div class="col-lg-3"></div>
                                    </div>
                                    <!-- /.form group -->
                                </div>
                                <div class="box-header" align="center">
                                    <label class="box-title">แสดงข้อมูลประจำวันที่ <h3 style="color: green">12/12/2561
                                            -
                                            15/12/2561</h3>
                                    </label>
                                </div>
                                <div class="box-body">
                                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>รหัสบุคลากร</th>
                                                <th>ชื่อ - สกุล</th>
                                                <th>เวลาเข้าเขตโรงพยาบาล</th>
                                                <th>เวลาเข้าประตูหน่วยงาน</th>
                                                <th>เวลาออก (เที่ยง)</th>
                                                <th>เวลาเข้า (เที่ยง)</th>
                                                <th>เวลาออกเขตโรงพยาบาล</th>
                                                <th>เวลาออกประตูหน่วยงาน</th>
                                                <th>สาย</th>
                                                <th>ออกก่อน</th>
                                                <th>ลากิจ</th>
                                                <th>ลาพักผ่อน</th>
                                                <th>ลาป่วย</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>45121</td>
                                                <td>สมหมาย ใจดี</td>
                                                <td>8.00</td>
                                                <td>9.30</td>
                                                <td>12.00</td>
                                                <td>13.00</td>
                                                <td>16.00</td>
                                                <td>16.30</td>
                                                <td class="center" style="text-align: center">&#10003;</td>
                                                <td class="center" style="text-align: center">X</td>
                                                <td class="center" style="text-align: center">X</td>
                                                <td class="center" style="text-align: center">X</td>
                                                <td class="center" style="text-align: center">X</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>
            <!-- /.row -->
        </div>
        <!-- /.content-wrapper -->
        <?php require('./Views/Footer.php') ?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="./tools/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="./tools/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="./tools/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="./tools/dist/js/demo.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="./tools/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./tools/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="./tools/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- date-range-picker -->
    <script src="./tools/bower_components/moment/min/moment.min.js"></script>
    <script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap color picker -->
    <script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- Select2 -->
    <script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="./tools/plugins/iCheck/icheck.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="./tools/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
    <!-- page script -->
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', {
                'placeholder': 'dd/mm/yyyy'
            })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', {
                'placeholder': 'mm/dd/yyyy'
            })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                            'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                        'MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })
    </script>
</body>

</html>