<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HR - Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .red {
            background-color: red;
        }

        td {
            /*white-space: nowrap;*/
        }

        table.table-bordered {
            border: 1px solid #64FFDA;
            margin-top: 20px;
        }

        table.table-bordered > thead > tr > th {
            border: 1px solid #64FFDA;
        }

        table.table-bordered > tbody > tr > td {
            border: 1px solid #64FFDA;
        }

        th {
            /*white-space: nowrap;*/
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .pic {
            margin: 50px;
            /* demo spacing */
            display: inline-block;
            vertical-align: middle;
            width: 0;
            height: 0;
        }

        .arrow-left {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-right: 30px solid #737373;
        }

        .arrow-right {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-left: 30px solid #737373;
        }

        .arrow-up {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-bottom: 30px solid #737373;
        }

        .arrow-down {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-top: 30px solid #737373;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        .myButton {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #44c767), color-stop(1, #5cbf2a));
            background: -moz-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -webkit-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -o-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: -ms-linear-gradient(top, #44c767 5%, #5cbf2a 100%);
            background: linear-gradient(to bottom, #44c767 5%, #5cbf2a 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#44c767', endColorstr='#5cbf2a', GradientType=0);
            background-color: #44c767;
            -moz-border-radius: 28px;
            -webkit-border-radius: 28px;
            border-radius: 28px;
            border: 1px solid #18ab29;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: Arial;
            font-size: 17px;
            padding: 16px 31px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #2f6627;
        }

        .myButton:hover {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cbf2a), color-stop(1, #44c767));
            background: -moz-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -webkit-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -o-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: -ms-linear-gradient(top, #5cbf2a 5%, #44c767 100%);
            background: linear-gradient(to bottom, #5cbf2a 5%, #44c767 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cbf2a', endColorstr='#44c767', GradientType=0);
            background-color: #5cbf2a;
        }

        .myButton:active {
            position: relative;
            top: 1px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div id="page-wrapper" class="content-wrapper" ng-controller="myCtrl">
        <section class="content-header">
            <h1>
                สรุปสถิติปฏิบัติงานประจำเดือน
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-pie-chart"></i> จัดการข้อมูลการเข้า-ออกงาน</a></li>
                <li class="active">ประจำเดือน</li>
            </ol>
        </section>
        <!-- /.row -->
        <section class="content" ng-if="CheckPermiss">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="panel panel-default">
                            <div class="box-header">
                                <h3 class="box-title">ตารางปฏิทินการปฏิบัติงานประจำเดือน</h3>
                            </div>

                            <div class="box-body">
                                <!-- Date -->
                                <div class="form-group">
                                    <div class="col-lg-3" style="text-align: center">
                                        <label>หน่วยงาน:</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <!-- <select class="form-control select2" style="width: 100%;" ng-model="DepartSelect" ng-change="update()" id="Select_Depart" ng-disabled="<?php echo !$this->model->CheckUser ?>"> -->
                                        <select class="form-control select2" style="width: 100%;"
                                                ng-model="DepartSelect" ng-change="update()" id="Select_Depart">
                                            <option value="0">แสดงข้อมูลทั้งหมด</option>
                                            <option ng-repeat="depart in Depart"
                                                    ng-selected="DepartSelect == depart.Dep_Code"
                                                    value="{{depart.Dep_Code}}">{{depart.Dep_name}} :
                                                {{depart.Dep_Group_name}} (รหัสหน่วยงาน: {{depart.Dep_Code}} : {{depart.Edit_code}})
                                            </option>
                                        </select>
                                        <!-- <select class="form-control" style="width: 100%;" ng-change="SelectDataFromDepart()" ng-model="Selected">
                                                <option ng-repeat="depart in Depart" value="depart.Dep_code">{{depart.Dep_name}} : {{depart.Dep_Group_name}}</option>
                                            </select> -->
                                        <!-- /.Select group -->
                                    </div>
                                    <div class="col-lg-3"></div>
                                </div>
                                <!-- /.form group -->
                            </div>

                            <div class="box-body">
                                <!-- Date -->
                                <div class="form-group">
                                    <div class="col-lg-3" style="text-align: center">
                                        <label>เรียกดูประจำเดือน:</label>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-8">
                                            <select class="form-control select2" style="width: 100%;"
                                                    ng-change="selectMonth()" ng-model="MonthSelect" id="Select_Month">
                                                <option value="1" selected="selected">มกราคม</option>
                                                <option value="2">กุมภาพันธ์</option>
                                                <option value="3">มีนาคม</option>
                                                <option value="4">เมษายน</option>
                                                <option value="5">พฤษภาคม</option>
                                                <option value="6">มิถุนายน</option>
                                                <option value="7">กรกฎาคม</option>
                                                <option value="8">สิงหาคม</option>
                                                <option value="9">กันยายน</option>
                                                <option value="10">ตุลาคม</option>
                                                <option value="11">พฤศจิกายน</option>
                                                <option value="12">ธันวาคม</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <select class="form-control select2" style="width: 100%;"
                                                    ng-model="YearSelect" id="Select_Year">
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020" selected="selected">2020</option>
                                            </select>
                                        </div>
                                        <!-- /.Select group -->
                                    </div>
                                    <div class="col-lg-3" align="center">
                                        <button type="button" class="btn btn-block btn-info"
                                                style="width:40%;align-content: center" ng-click="getReportOfMonth()">
                                            เรียกดู
                                        </button>
                                    </div>
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div align="center" style="margin-top: 25px">
                                <label class="box-title">แสดงข้อมูลประเดือน <h1 style="color: green;font-weight: bold">
                                        {{MonthShow | DateThai}} <span style="color:red"> </span> {{DateEndShow |
                                        DateThai}} {{YearShow}}</h1></label>
                            </div>

                            <div class="form-group" align="center">
                                <label style="padding: 25px" ng-repeat="posi in FilterPosition"
                                       ng-click="FilterWord(posi)">
                                    <input type="radio" name="r3" class="form-check-input"
                                           style="transform: scale(1.5)">
                                    <span style="cursor: pointer;margin-left: 8px">{{posi.PosName}}</span>
                                </label>
                                <label style="padding: 25px" ng-click="FilterWord('000')">
                                    <input type="radio" name="r3" class="form-check-input"
                                           style="transform: scale(1.5)">
                                    <span style="cursor: pointer;margin-left: 8px">ทั้งหมด</span>
                                </label>
                            </div>

                            <div class="box-body" ng-if="CheckTable">
                                <div class="table-responsive">
                                    <table ng-if="CheckTable" datatable="ng" dt-options="dtOptions"
                                           dt-instance="dtInstance" class="table table-bordered" width="100%"
                                           cellspacing="0" style="background-color: white;">
                                        <thead>
                                        <tr>
                                            <th style="font-weight: bold" ng-repeat="item_H in HeaderMenu">
                                                {{item_H.NAME}}
                                            </th>
                                            <th style="font-weight: bold;text-align: center"
                                                ng-repeat="item_Date in daysOfYear">
                                                {{item_Date.D_1Digit}}
                                            </th>
                                            <th style="font-weight: bold">
                                                รวม
                                            </th>
                                            <th style="font-weight: bold">
                                                คิดเป็น %
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <!--<tr ng-repeat="item_R in ReportSummary | orderBy : 'POS_WORK' : true" style="font-weight: bold">-->
                                        <tr ng-repeat="item_R in ReportSummary" style="font-weight: bold">
                                            <td style="text-align: center;background-color: aquamarine;"
                                                ng-if="Check_POSWORK(item_R.POS_WORK)">{{item_R.PERID}}
                                            </td>
                                            <td style="text-align: center;" ng-if="!Check_POSWORK(item_R.POS_WORK)">
                                                {{item_R.PERID}}
                                            </td>
                                            <td style="text-align: left;background-color: aquamarine;"
                                                ng-if="Check_POSWORK(item_R.POS_WORK)">{{item_R.NAME}}
                                            </td>
                                            <td style="text-align: left;"
                                                ng-if="!Check_POSWORK(item_R.POS_WORK)">{{item_R.NAME}}
                                            </td>
                                            <td ng-style="item_DateOf_M.BG"
                                                ng-repeat="item_DateOf_M in item_R.DATEOFMONTH">
                                                <div ng-bind-html="htmlTrusted(item_DateOf_M.STATUS)"></div>
                                            </td>
                                            <td style="text-align: left">{{item_R.SUM}} / {{item_R.DATEMORE}}</td>
                                            <td style="text-align: left">{{item_R.SUM_PER | number:2}} %</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="box-body">
                                <!-- Date -->
                                <div class="form-group">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-2" align="right">
                                        <label>&#x274C : วันหยุด</label>
                                        <br/>
                                        <label><span style="color: cornflowerblue">I : ID CHECK-IN</span></label>
                                        <br/>
                                        <label><span style="color: #00b92d">OPD : แพทย์ออกตรวจ</span></label>
                                        <br/>
                                        <label><span style="color: #00E676">ป : ปกติ</span></label>
                                        <br/>
                                        <label><span style="color: #EA80FC">ส : สาย</span></label>
                                        <br/>
                                        <label><span style="color: #00796B">อ : ออกก่อน</span></label>
                                        <br/>
                                        <label><span style="color: #448AFF">ข: ขาดงาน</span></label>
                                        <br/>
                                        <label><span style="color: #FF5252">ม : ไม่มีข้อมูล</span></label>
                                        <br/>
                                        <label><span>- : ข้อมูลยังไม่ได้รับการยืนยัน</span></label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label><span style="color: #3F51B5"> ลก.: ลากิจ</span> </label>
                                        <br/>
                                        <label><span style="color: #3F51B5"> ลป.: ลาป่วย</span> </label>
                                        <br/>
                                        <label><span style="color: #3F51B5"> ลพ.: ลาพักผ่อน</span> </label>
                                        <br/>
                                        <label><span style="color: #3F51B5"> ลศ.: ลาศึกษาต่อ</span> </label>
                                        <br/>
                                        <label><span
                                                    style="color: #3F51B5"> ลฝ.: ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย</span>
                                        </label>
                                        <br/>
                                        <label><span style="color: #3F51B5"> ลค.: ลาคลอดบุตร</span> </label>
                                        <br/>
                                        <label><span
                                                    style="color: #3F51B5"> ลส.: ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์</span>
                                        </label>
                                        <br/>
                                        <label><span style="color: #3F51B5"> ร.: ไปราชการ</span> </label>
                                    </div>
                                </div>
                                <!-- /.form group -->
                            </div>
                            <!-- /.panel-body -->

                            <div class="box-body" align="center">
                                <p>สรุป(ทั้งหมด {{Personall}} คน) : {{PersonallSum | number:2}} %</p>
                                <p>สรุป(เฉพาะอาจารย์ {{Master}} คน) : {{MasterSum | number:2}} %</p>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <section class="content" ng-if="!CheckPermiss">
            <div style="position: fixed;top: 50%;left: 30%;" align="center">
                <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
            </div>
        </section>
        <!-- /.row -->
        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.buttons.min.js"></script>
<script src="./tools/vendor/datatables2/buttons.colVis.min.js"></script>
<script src="./tools/vendor/datatables2/buttons.flash.min.js"></script>
<script src="./tools/vendor/datatables2/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="./tools/vendor/datatables2/buttons.print.min.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>

<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>

<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>

<script src="./tools/Js/angular.min.js"></script>
<script src="./tools/Js/dataTables.fixedColumns.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/Js/angular-datatables.fixedcolumns.js"></script>
<script src="./tools/Js/angular-datatables.buttons.js"></script>
<script src="./tools/Js/angular-sanitize.min.js"></script>
<script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script>
    $(document).ready(function () {
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(
                        1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    })
</script>
<script>
    var app = angular.module("myApp", ["datatables", "datatables.fixedcolumns", 'datatables.buttons', "ui.bootstrap", 'ngAnimate']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, $sce, DTOptionsBuilder, $uibModal, $filter) {
        $scope.daysOfYear = [];
        $scope.DateNoChck = [];
        $scope.HeaderMenu = [];
        $scope.FilterPosition = [];
        $scope.Depart = <?php echo $this->Department; ?>;
        $scope.Holiday = <?php echo $this->Holiday; ?>;
        $scope.Loading = true;
        $scope.CheckTable = false;
        $scope.CheckPermiss = true;
        $scope.MonthShow = '';
        $scope.YearShow = '';
        $scope.dtInstance = {};
        $scope.ReportSummaryFilter = [];
        $scope.Master = 0;
        $scope.Personall = 0;
        $scope.MasterSum = 0;
        $scope.PersonallSum = 0;
        var monthSelect = '';

        $scope.init = function () {
            // $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250])
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('lengthMenu', [100, 150, 200, 250, 500])
                .withOption('paging', true)
                .withOption('searching', true)
                .withOption('ordering', false)
                .withOption('info', true)
                .withOption('scrollX', '100%')
                .withOption('sortable', false)
                .withOption('orderable', false)
                .withOption('scrollCollapse', true)
                .withButtons([
                        {
                            extend: 'copy',
                            text: '<i class="fa fa-files-o"></i> Copy',
                            titleAttr: 'Copy'
                        },
                        {
                            extend: 'print',
                            text: '<i class="fa fa-print" aria-hidden="true"></i> Print',
                            titleAttr: 'Print'
                        },
                        {
                            extend: 'excelHtml5',
                            customize: function (xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];

                                // jQuery selector to add a border to the third row
                                // $('row c[r*="3"]', sheet).attr( 's', '25' );
                                // jQuery selector to set the forth row's background gray
                                // $('row c[r*="4"]', sheet).attr( 's', '10' );
                            }
                        }
                    ]
                )
                .withFixedColumns({
                    leftColumns: 2
                });

            data = {
                'MENU_CODE': '002',
            };

            $http.post('./ApiService/CheckPermissMenu', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    if (response.data.Status) {
                        $scope.CheckPermiss = true;

                        var dateNow = new Date();
                        $scope.MonthSelect = '' + (dateNow.getMonth() + 1);
                        $scope.YearSelect = '' + dateNow.getFullYear();
                        var monthInit = dateNow.getMonth() + 1;
                        if (monthInit < 10) {
                            monthSelect = '0' + monthInit;
                        } else {
                            monthSelect = monthInit;
                        }

                        var check_Map_Dep = false;

                        angular.forEach($scope.Depart, function (item, idx) {
                            if (item.Dep_Code == <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>) {
                                $scope.DepartSelect = "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>;
                                check_Map_Dep = true;
                                $scope.getReportOfMonth();
                                $scope.Loading = false;
                            } else {
                                if (idx == $scope.Depart.length - 1 && !check_Map_Dep) {
                                    $scope.DepartSelect = '';
                                    $scope.Loading = false;
                                }
                            }
                        })

                    } else {
                        $scope.CheckPermiss = false;
                        $scope.Loading = false;
                    }
                });
        }

        $timeout($scope.init);

        $scope.selectMonth = function () {

        }

        $scope.Check_POSWORK = function (POS_WORK) {
            if ((parseInt(POS_WORK)) >= 551 && ((parseInt(POS_WORK)) <= 558)
                || (parseInt(POS_WORK) == 800) || (parseInt(POS_WORK) == 221)
                || (parseInt(POS_WORK) == 211)|| (parseInt(POS_WORK) == 212)){
                return true;
            } else {
                return false;
            }
        }

        $scope.getBGcolor = function (item, itemDate) {
            if (itemDate.STATUS.indexOf('D') >= 0) {
                item.BG = 'antiquewhite';
            } else {
                item.BG = 'white';
            }
        }

        $scope.GetTimeToDb = function (date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1;

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return date = yyyy + '-' + mm + '-' + dd;
        }

        $scope.CheckCordiDate = function (DAY) {
            var DateNow = new Date();
            var DateQuery = new Date(DAY);
            if (DateQuery > DateNow) {
                return false;
            } else {
                return true;
            }
        }

        $scope.calArrayDate = function (fday, lday) {
            var firstDay2 = new Date(fday);
            var lastDay = new Date(lday);
            var daysOfYear = [];
            var options = {
                weekday: 'long'
            };
            for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                daysOfYear.push({
                    'DAY': $scope.GetTimeToDb(d),
                    'D_1Digit': d.getDate(),
                    'DAY_NAME': d.toLocaleDateString('en-US', options),
                    'STATUS': '-'
                });
            }
            return daysOfYear;
        }

        $scope.htmlTrusted = function (html) {
            return $sce.trustAsHtml(html);
        }

        $scope.FilterWord = function (value) {
            $scope.ReportSummary = [];
            if (value == '000') {
                angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                    if($scope.Check_POSWORK(Report_Arr.POS_WORK)){
                        $scope.ReportSummary.push(Report_Arr)
                    }
                });
                angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                    if(!$scope.Check_POSWORK(Report_Arr.POS_WORK)){
                        $scope.ReportSummary.push(Report_Arr)
                    }
                });
            } else {
                angular.forEach($scope.ReportSummaryFilter, function (item) {
                    if (item.POS_WORK == value.POS_WORK) {
                        $scope.ReportSummary.push(item)
                    }
                })
            }
        };

        $scope.getReportOfMonth = function () {
            $scope.Loading = true;
            $scope.daysOfYear = [];
            $scope.ReportSummary = [];
            $scope.CheckTable = false;
            // $scope.MonthShow = $scope.MonthSelect;
            // $scope.YearShow = $scope.YearSelect;
            monthSelect = '';
            var Select_Month = angular.element(document.querySelector('#Select_Month'));
            if (Select_Month[0].options[Select_Month[0].selectedIndex].value != null && Select_Month[0].options[Select_Month[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.MonthSelect = Select_Month[0].options[Select_Month[0].selectedIndex].value;
            }
            var Select_Year = angular.element(document.querySelector('#Select_Year'));
            if (Select_Year[0].options[Select_Year[0].selectedIndex].value != null && Select_Year[0].options[Select_Year[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.YearSelect = Select_Year[0].options[Select_Year[0].selectedIndex].value;
            }
            $scope.MonthShow = $scope.MonthSelect;
            $scope.YearShow = $scope.YearSelect;
            if ($scope.MonthSelect < 10) {
                monthSelect = '0' + $scope.MonthSelect;
            } else {
                monthSelect = $scope.MonthSelect;
            }
            var options = {
                weekday: 'long'
            };

            $scope.HeaderMenu = [{
                'NAME': '#'
            }, {
                'NAME': 'ชื่อ - สกุล'
            }];

            var date = new Date($scope.YearSelect + "-" + monthSelect + "-" + "01");
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var firstDay2 = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                $scope.daysOfYear.push({
                    'DAY': $scope.GetTimeToDb(d),
                    'D_1Digit': d.getDate(),
                    'DAY_NAME': d.toLocaleDateString('en-US', options),
                    'STATUS': ''
                });
            }

            var DateStart = $scope.GetTimeToDb(firstDay);
            var DateEnd = $scope.GetTimeToDb(lastDay);
            var statusIn = '';
            var statusOut = '';
            $scope.Leave0 = '';
            $scope.Leave1 = '';
            $scope.Leave2 = '';
            $scope.Leave3 = '';
            $scope.Leave4 = '';
            $scope.Leave5 = '';
            $scope.Leave6 = '';
            $scope.Leave7 = '';

            //var DepartIsNull = angular.element(document.querySelector('#Select_Depart'));
            //if (DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value == null || DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value == '? undefined:undefined ?') {
            //    if ('<?php //echo $this->model->Status; ?>//' == 'ผู้ดูแลระบบ') {
            //
            //    } else {
            //        $scope.DepartSelect = $scope.Depart[0].Dep_Code;
            //    }
            //}

            var DepartIsNull = angular.element(document.querySelector('#Select_Depart'));

            // $scope.DepartSelect = depItem.Dep_Code;
            if (DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != null && DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != '? undefined:undefined ?') {
                //    if ('<?php //echo $this->model->Status; ?>//' == 'ผู้ดูแลระบบ') {
                //        $scope.DepartSelect = DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value;
                //    } else {
                //        // $scope.DepartSelect = $scope.Depart[0].Dep_Code;
                $scope.DepartSelect = DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value;
                //    }
            }

            if ($scope.DepartSelect == '0') {
                $scope.CheckAll = true;
            } else {
                $scope.CheckAll = false;
            }

            // data = {
            //     'Dep_Code': $scope.DepartSelect,
            // };

            if ($scope.DepartSelect == null || $scope.DepartSelect == '? string: ?' || $scope.DepartSelect == '? undefined:undefined ?') {
                Swal({
                    type: 'error',
                    title: 'กรุณาเลือกหน่วยงาน'
                });
                $scope.Loading = false;
            } else {
                if ($scope.CheckAll) {
                    data = {
                        'Dep_Code': $scope.Depart
                    };
                } else {
                    data = {
                        'Dep_Code': [{'Dep_Code': $scope.DepartSelect}]
                    };
                }

                $http.post('./ApiService/GetPersonReportOfMonth', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .then(function successCallback(response) {
                        $scope.Personal = response.data;
                        if ($scope.Personal.length > 0) {

                            if ($scope.CheckAll) {
                                data = {
                                    'DepCode': $scope.Depart
                                };
                            } else {
                                data = {
                                    'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                                };
                            }
                            $http.post('./ApiService/GetRefPersonal', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                                .then(function successCallback(response) {
                                    if (response.data.length > 0) {
                                        var Copies = []
                                        angular.extend(Copies, $scope.Personal)
                                        angular.forEach($scope.Personal, function (data_I, idx) {
                                            angular.forEach(response.data, function (Ref_I) {
                                                if ($scope.CheckAll) {
                                                    angular.forEach($scope.Depart, function (depart_I, idx_Depart) {
                                                        if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == depart_I.Dep_Code) {
                                                        } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == depart_I.Dep_Code) {
                                                            var CheckDep = false
                                                            angular.forEach($scope.Depart, function (depart_I2, idx_Dep) {
                                                                if (Ref_I.Dep_Code == depart_I2.Dep_Code) {
                                                                    CheckDep = true
                                                                }
                                                            })
                                                            if (!CheckDep) {
                                                                if(Copies.length > 0){
                                                                    angular.forEach(Copies, function (data_Copy, idx_c) {
                                                                        if(data_I.PERID == data_Copy.PERID){
                                                                            Copies.splice(idx_c, 1)
                                                                        }
                                                                    })
                                                                }
                                                            } else {
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == $scope.DepartSelect) {

                                                    } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == $scope.DepartSelect) {
                                                        if(Copies.length > 0){
                                                            angular.forEach(Copies, function (data_Copy, idx_c) {
                                                                if(data_I.PERID == data_Copy.PERID){
                                                                    Copies.splice(idx_c, 1)
                                                                }
                                                            })
                                                        }
                                                    }
                                                }
                                            })
                                        })
                                        $scope.Personal = []
                                        $scope.Personal = Copies

                                        $scope.FilterPosition = UniqueArraybyId($scope.Personal,
                                            "PosCode");

                                        function UniqueArraybyId(collection, keyname) {
                                            var output = [],
                                                keys = [];

                                            angular.forEach(collection, function (item) {
                                                var key = item[keyname];
                                                if (keys.indexOf(key) === -1) {
                                                    keys.push(key);
                                                    output.push(item);
                                                }
                                            });
                                            return output;
                                        };

                                        $scope.Personal = UniqueArraybyId($scope.Personal,
                                            "PERID");

                                        function UniqueArraybyId(collection, keyname) {
                                            var output = [],
                                                keys = [];

                                            angular.forEach(collection, function (item) {
                                                var key = item[keyname];
                                                if (keys.indexOf(key) === -1) {
                                                    keys.push(key);
                                                    output.push(item);
                                                }
                                            });
                                            return output;
                                        };

                                        // $scope.Personal = $filter('orderBy')($scope.Personal, 'PERID')

                                        angular.forEach($scope.Personal, function (PersonItem) {
                                            $scope.ReportSummary.push({
                                                'PERID': PersonItem.PERID,
                                                'NAME': PersonItem.NAME + " " + PersonItem.SURNAME,
                                                'POS_WORK': PersonItem.POS_WORK,
                                                'DATEOFMONTH': $scope.calArrayDate(new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)),
                                                'SUM': '',
                                                'SUM_PER': '',
                                                'DATEMORE': ''
                                            });
                                        })

                                        angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                            angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                date_Item.BG = {
                                                    'text-align': 'center',
                                                    'white-space': 'nowrap',
                                                    'background-color': 'white'
                                                };
                                            })
                                        })

                                        // $scope.ReportSummary = $filter('orderBy')($scope.ReportSummary, 'PERID')

                                        // data = {
                                        //     'Dep_Code': $scope.DepartSelect,
                                        //     'Date_Cordi': DateStart,
                                        //     'Date_Cordi2': DateEnd,
                                        // };

                                        if ($scope.CheckAll) {
                                            data = {
                                                'Dep_Code': $scope.Depart,
                                                'Date_Cordi': DateStart,
                                                'Date_Cordi2': DateEnd,
                                            };
                                        } else {
                                            data = {
                                                'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                                'Date_Cordi': DateStart,
                                                'Date_Cordi2': DateEnd,
                                            };
                                        }

                                        $http.post('./ApiService/GetWorkReportOfMonth', data, {
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            }
                                        })
                                            .then(function successCallback(response) {
                                                $scope.workReport = response.data;
                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                    angular.forEach($scope.workReport, function (Work_Arr, idx) {
                                                        if (Report_Arr.PERID == Work_Arr.PERID) {
                                                            angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                if (Work_Arr.A_Datetime == date_Item.DAY) {
                                                                    if (Work_Arr.A_Status_in2 == 'สาย') {
                                                                        statusIn = '<span style="color: #EA80FC">ส</span>';
                                                                    } else if (Work_Arr.A_Status_in2 == 'ออกก่อน') {
                                                                        statusIn = '<span style="color: #00796B">อ</span>';
                                                                    } else if (Work_Arr.A_Status_in2 == 'ขาดงาน') {
                                                                        statusIn = '<span style="color: #448AFF">ข</span>';
                                                                    } else if (Work_Arr.A_Status_in2 == '' && Work_Arr.A_Time_in2 == '') {
                                                                        statusIn = '<span style="color: #FF5252">ม</span>';
                                                                    } else {
                                                                        statusIn = '<span style="color: #00E676">ป</span>';
                                                                    }
                                                                    if (Work_Arr.A_Status_out2 == 'สาย') {
                                                                        statusOut = '<span style="color: #EA80FC">ส</span>';
                                                                    } else if (Work_Arr.A_Status_out2 == 'ออกก่อน') {
                                                                        statusOut = '<span style="color: #00796B">อ</span>';
                                                                    } else if (Work_Arr.A_Status_out2 == 'ขาดงาน') {
                                                                        statusOut = '<span style="color: #448AFF">ข</span>';
                                                                    } else if (Work_Arr.A_Status_out2 == '' && Work_Arr.A_Time_out2 == '') {
                                                                        statusOut = '<span style="color: #FF5252">ม</span>';
                                                                    } else {
                                                                        statusOut = '<span style="color: #00E676">ป</span>';
                                                                    }
                                                                    date_Item.STATUS = statusIn + " / " + statusOut;
                                                                } else {
                                                                    if (date_Item.STATUS != '') {

                                                                    } else {
                                                                        date_Item.STATUS = '-';
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    })

                                                })

                                                if ($scope.CheckAll) {
                                                    data = {
                                                        'Dep_Code': $scope.Depart,
                                                        'Date_Cordi': DateStart,
                                                        'Date_Cordi2': DateEnd,
                                                    };
                                                } else {
                                                    data = {
                                                        'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                                        'Date_Cordi': DateStart,
                                                        'Date_Cordi2': DateEnd,
                                                    };
                                                }

                                                $http.post('./ApiService/GetDocTorSummary', data, {
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    }
                                                })
                                                    .then(function successCallback(response) {
                                                        angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                            angular.forEach(response.data, function (DocTor_Arr) {
                                                                if (Report_Arr.PERID == DocTor_Arr.perid) {
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                        if (date_Item.DAY == DocTor_Arr.day_of_month) {
                                                                            date_Item.STATUS = '<span style="color: #00b92d">OPD</span> ';
                                                                            date_Item.BG = {
                                                                                'text-align': 'center',
                                                                                'white-space': 'nowrap',
                                                                                'background-color': 'aliceblue'
                                                                            };
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        });

                                                        if ($scope.CheckAll) {
                                                            data = {
                                                                'Dep_Code': $scope.Depart,
                                                                'Date_Cordi': DateStart,
                                                                'Date_Cordi2': DateEnd,
                                                            };
                                                        } else {
                                                            data = {
                                                                'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                                                'Date_Cordi': DateStart,
                                                                'Date_Cordi2': DateEnd,
                                                            };
                                                        }

                                                        $http.post('./ApiService/GetLeaveReportOfMonth', data, {
                                                            headers: {
                                                                'Content-Type': 'application/x-www-form-urlencoded'
                                                            }
                                                        })
                                                            .then(function successCallback(response) {
                                                                $scope.leaveReport = response.data;
                                                                var CheckHalfDay = true;
                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    angular.forEach($scope.leaveReport, function (Leave_Arr, idx_leave) {
                                                                        if (Report_Arr.PERID == Leave_Arr.PERID) {
                                                                            CheckHalfDay = true;
                                                                            angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                                if (date_Item.DAY >= Leave_Arr.T_Leave_Date_Start && date_Item.DAY <= Leave_Arr.T_Leave_Date_End) {
                                                                                    if (date_Item.STATUS != '-') {
                                                                                        switch (Leave_Arr.T_Leave_Type) {
                                                                                            case '0':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลก.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '1':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลป.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '2':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลพ.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '3':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลศ.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '4':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลฝ.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '5':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลค.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '6':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลส.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '7':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ร.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            default:
                                                                                                break;
                                                                                        }
                                                                                    } else {
                                                                                        switch (Leave_Arr.T_Leave_Type) {
                                                                                            case '0':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                                break;
                                                                                            case '1':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                                break;
                                                                                            case '2':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                                break;
                                                                                            case '3':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                                break;
                                                                                            case '4':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                                break;
                                                                                            case '5':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                                break;
                                                                                            case '6':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                                break;
                                                                                            case '7':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                                break;
                                                                                            default:
                                                                                                break;
                                                                                        }
                                                                                    }
                                                                                } else {

                                                                                }
                                                                            });
                                                                        }

                                                                    });
                                                                });
                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                        if (date_Item.DAY_NAME == 'Saturday' || date_Item.DAY_NAME == 'Sunday') {
                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> &#x274C; </span>';
                                                                        } else {
                                                                            angular.forEach($scope.Holiday, function (hoItem) {
                                                                                if (hoItem.Date_stop == date_Item.DAY) {
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> &#x274C; </span>';
                                                                                } else {
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                });

                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                            if ((parseInt(Report_Arr.POS_WORK)) >= 551 && ((parseInt(Report_Arr.POS_WORK)) <= 558)
                                                                                || (parseInt(Report_Arr.POS_WORK) == 800) || (parseInt(Report_Arr.POS_WORK) == 221)
                                                                                || (parseInt(Report_Arr.POS_WORK) == 211)|| (parseInt(Report_Arr.POS_WORK) == 212)){
                                                                            if (date_Item.STATUS != '-' && (date_Item.STATUS.indexOf('<span style="color: #3F51B5"> &#x274C; </span>') < 0)) {
                                                                                if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                                    if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                        date_Item.STATUS = '<span style="color: cornflowerblue;font-size: 12px"">I</span>';
                                                                                    }
                                                                                }
                                                                            }

                                                                        } else {
                                                                        }
                                                                    });
                                                                });

                                                                $scope.MasterSum = 0;
                                                                $scope.PersonallSum = 0;
                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    var count = 0;
                                                                    var DateLast = 0;
                                                                    var DateNow = new Date();
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item, idx_Date) {
                                                                        if ($scope.CheckCordiDate(date_Item.DAY)) {
                                                                            if (date_Item.STATUS != '-') {
                                                                                if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                                    if (date_Item.STATUS.indexOf('&#x274C;') > 0) {
                                                                                        count++;
                                                                                    } else {
                                                                                        if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                            count++;
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    if ((date_Item.STATUS.indexOf('OPD') >= 0)) {
                                                                                        count++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        if (idx_Date === $scope.ReportSummary[0].DATEOFMONTH.length - 1) {
                                                                            DateLast = parseInt(date_Item.D_1Digit);
                                                                        }

                                                                    });
                                                                    Report_Arr.SUM = count;
                                                                    Report_Arr.SUM_PER = ((count / DateLast) * 100);
                                                                    Report_Arr.DATEMORE = DateLast;
                                                                    $scope.PersonallSum += Report_Arr.SUM_PER;
                                                                    // console.log(Report_Arr.PERID +' '+ $scope.PersonallSum +' += '+ ((count / DateLast) * 100))
                                                                });
                                                                $scope.PersonallSum = ($scope.PersonallSum / $scope.ReportSummary.length) * 100

                                                                $scope.Personall = $scope.ReportSummary.length

                                                                // console.log('ทั้งหมด ' + $scope.PersonallSum)

                                                                $scope.ReportSummaryFilter = $scope.ReportSummary;
                                                                $scope.ReportSummary = [];
                                                                angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                                                                    if($scope.Check_POSWORK(Report_Arr.POS_WORK)){
                                                                        $scope.ReportSummary.push(Report_Arr)
                                                                    }
                                                                });

                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    var count = 0;
                                                                    var DateLast = 0;
                                                                    var DateNow = new Date();
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item, idx_Date) {
                                                                        if ($scope.CheckCordiDate(date_Item.DAY)) {
                                                                            if (date_Item.STATUS != '-') {
                                                                                if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                                    if (date_Item.STATUS.indexOf('&#x274C;') > 0) {
                                                                                        count++;
                                                                                    } else {
                                                                                        if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                            count++;
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    if ((date_Item.STATUS.indexOf('OPD') >= 0)) {
                                                                                        count++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        if (idx_Date === $scope.ReportSummary[0].DATEOFMONTH.length - 1) {
                                                                            DateLast = parseInt(date_Item.D_1Digit);
                                                                        }

                                                                    });
                                                                    Report_Arr.SUM = count;
                                                                    Report_Arr.SUM_PER = ((count / DateLast) * 100);
                                                                    Report_Arr.DATEMORE = DateLast;
                                                                    $scope.MasterSum += Report_Arr.SUM_PER;
                                                                    // console.log(Report_Arr.PERID +' '+ $scope.PersonallSum +' += '+ ((count / DateLast) * 100))
                                                                });

                                                                // console.log(($scope.MasterSum + ' / ' + $scope.ReportSummary.length) + ' * ' + 100)
                                                                $scope.Master = $scope.ReportSummary.length

                                                                $scope.MasterSum = ($scope.MasterSum / $scope.ReportSummary.length) * 100

                                                                // console.log('อาจารย์ ' + $scope.MasterSum)

                                                                angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                                                                    if(!$scope.Check_POSWORK(Report_Arr.POS_WORK)){
                                                                        $scope.ReportSummary.push(Report_Arr)
                                                                    }
                                                                });
                                                                // $scope.ReportSummaryFilter = $scope.ReportSummary;
                                                                $scope.Loading = false;
                                                                $scope.CheckTable = true;
                                                            });
                                                    });

                                                // console.log($scope.ReportSummary);

                                                // console.log($scope.ReportSummary);

                                                // data = {
                                                //     'Dep_Code': $scope.DepartSelect,
                                                //     'Date_Cordi': DateStart,
                                                //     'Date_Cordi2': DateEnd,
                                                // };
                                            });
                                    } else {
                                        angular.forEach($scope.Personal, function (PersonItem) {
                                            $scope.ReportSummary.push({
                                                'PERID': PersonItem.PERID,
                                                'NAME': PersonItem.NAME + " " + PersonItem.SURNAME,
                                                'POS_WORK': PersonItem.POS_WORK,
                                                'DATEOFMONTH': $scope.calArrayDate(new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)),
                                                'SUM': '',
                                                'SUM_PER': '',
                                                'DATEMORE': ''
                                            });
                                        })

                                        angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                            angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                date_Item.BG = {
                                                    'text-align': 'center',
                                                    'white-space': 'nowrap',
                                                    'background-color': 'white'
                                                };
                                            })
                                        })

                                        if ($scope.CheckAll) {
                                            data = {
                                                'Dep_Code': $scope.Depart,
                                                'Date_Cordi': DateStart,
                                                'Date_Cordi2': DateEnd,
                                            };
                                        } else {
                                            data = {
                                                'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                                'Date_Cordi': DateStart,
                                                'Date_Cordi2': DateEnd,
                                            };
                                        }

                                        $http.post('./ApiService/GetWorkReportOfMonth', data, {
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            }
                                        })
                                            .then(function successCallback(response) {
                                                $scope.workReport = response.data;
                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                    angular.forEach($scope.workReport, function (Work_Arr, idx) {
                                                        if (Report_Arr.PERID == Work_Arr.PERID) {
                                                            angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                if (Work_Arr.A_Datetime == date_Item.DAY) {
                                                                    if (Work_Arr.A_Status_in2 == 'สาย') {
                                                                        statusIn = '<span style="color: #EA80FC">ส</span>';
                                                                    } else if (Work_Arr.A_Status_in2 == 'ออกก่อน') {
                                                                        statusIn = '<span style="color: #00796B">อ</span>';
                                                                    } else if (Work_Arr.A_Status_in2 == 'ขาดงาน') {
                                                                        statusIn = '<span style="color: #448AFF">ข</span>';
                                                                    } else if (Work_Arr.A_Status_in2 == '' && Work_Arr.A_Time_in2 == '') {
                                                                        statusIn = '<span style="color: #FF5252">ม</span>';
                                                                    } else {
                                                                        statusIn = '<span style="color: #00E676">ป</span>';
                                                                    }
                                                                    if (Work_Arr.A_Status_out2 == 'สาย') {
                                                                        statusOut = '<span style="color: #EA80FC">ส</span>';
                                                                    } else if (Work_Arr.A_Status_out2 == 'ออกก่อน') {
                                                                        statusOut = '<span style="color: #00796B">อ</span>';
                                                                    } else if (Work_Arr.A_Status_out2 == 'ขาดงาน') {
                                                                        statusOut = '<span style="color: #448AFF">ข</span>';
                                                                    } else if (Work_Arr.A_Status_out2 == '' && Work_Arr.A_Time_out2 == '') {
                                                                        statusOut = '<span style="color: #FF5252">ม</span>';
                                                                    } else {
                                                                        statusOut = '<span style="color: #00E676">ป</span>';
                                                                    }
                                                                    date_Item.STATUS = statusIn + " / " + statusOut;
                                                                } else {
                                                                    if (date_Item.STATUS != '') {

                                                                    } else {
                                                                        date_Item.STATUS = '-';
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    })

                                                })

                                                if ($scope.CheckAll) {
                                                    data = {
                                                        'Dep_Code': $scope.Depart,
                                                        'Date_Cordi': DateStart,
                                                        'Date_Cordi2': DateEnd,
                                                    };
                                                } else {
                                                    data = {
                                                        'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                                        'Date_Cordi': DateStart,
                                                        'Date_Cordi2': DateEnd,
                                                    };
                                                }

                                                $http.post('./ApiService/GetDocTorSummary', data, {
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    }
                                                })
                                                    .then(function successCallback(response) {
                                                        angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                            angular.forEach(response.data, function (DocTor_Arr) {
                                                                if (Report_Arr.PERID == DocTor_Arr.perid) {
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                        if (date_Item.DAY == DocTor_Arr.day_of_month) {
                                                                            date_Item.STATUS = '<span style="color: #00b92d">OPD</span> ';
                                                                            date_Item.BG = {
                                                                                'text-align': 'center',
                                                                                'white-space': 'nowrap',
                                                                                'background-color': 'aliceblue'
                                                                            };
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        });

                                                        if ($scope.CheckAll) {
                                                            data = {
                                                                'Dep_Code': $scope.Depart,
                                                                'Date_Cordi': DateStart,
                                                                'Date_Cordi2': DateEnd,
                                                            };
                                                        } else {
                                                            data = {
                                                                'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                                                'Date_Cordi': DateStart,
                                                                'Date_Cordi2': DateEnd,
                                                            };
                                                        }

                                                        $http.post('./ApiService/GetLeaveReportOfMonth', data, {
                                                            headers: {
                                                                'Content-Type': 'application/x-www-form-urlencoded'
                                                            }
                                                        })
                                                            .then(function successCallback(response) {
                                                                $scope.leaveReport = response.data;
                                                                var CheckHalfDay = true;
                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    angular.forEach($scope.leaveReport, function (Leave_Arr, idx_leave) {
                                                                        if (Report_Arr.PERID == Leave_Arr.PERID) {
                                                                            CheckHalfDay = true;
                                                                            angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                                if (date_Item.DAY >= Leave_Arr.T_Leave_Date_Start && date_Item.DAY <= Leave_Arr.T_Leave_Date_End) {
                                                                                    if (date_Item.STATUS != '-') {
                                                                                        switch (Leave_Arr.T_Leave_Type) {
                                                                                            case '0':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลก.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '1':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลป.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '2':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลพ.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '3':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลศ.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '4':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลฝ.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '5':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลค.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '6':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลส.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            case '7':
                                                                                                if (CheckHalfDay) {
                                                                                                    if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                                        date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ร.</span>';
                                                                                                        CheckHalfDay = false;
                                                                                                    } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                                        CheckHalfDay = false;
                                                                                                    } else {
                                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                                    }
                                                                                                } else {
                                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                                }
                                                                                                break;
                                                                                            default:
                                                                                                break;
                                                                                        }
                                                                                    } else {
                                                                                        switch (Leave_Arr.T_Leave_Type) {
                                                                                            case '0':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                                break;
                                                                                            case '1':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                                break;
                                                                                            case '2':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                                break;
                                                                                            case '3':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                                break;
                                                                                            case '4':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                                break;
                                                                                            case '5':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                                break;
                                                                                            case '6':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                                break;
                                                                                            case '7':
                                                                                                date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                                break;
                                                                                            default:
                                                                                                break;
                                                                                        }
                                                                                    }
                                                                                } else {

                                                                                }
                                                                            });
                                                                        }

                                                                    });
                                                                });
                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                        if (date_Item.DAY_NAME == 'Saturday' || date_Item.DAY_NAME == 'Sunday') {
                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> &#x274C; </span>';
                                                                        } else {
                                                                            angular.forEach($scope.Holiday, function (hoItem) {
                                                                                if (hoItem.Date_stop == date_Item.DAY) {
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> &#x274C; </span>';
                                                                                } else {
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                });

                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                        if ((parseInt(Report_Arr.POS_WORK)) >= 551 && ((parseInt(Report_Arr.POS_WORK)) <= 558)
                                                                            || (parseInt(Report_Arr.POS_WORK) == 800) || (parseInt(Report_Arr.POS_WORK) == 221)
                                                                            || (parseInt(Report_Arr.POS_WORK) == 211)|| (parseInt(Report_Arr.POS_WORK) == 212)){
                                                                            if (date_Item.STATUS != '-' && (date_Item.STATUS.indexOf('<span style="color: #3F51B5"> &#x274C; </span>') < 0)) {
                                                                                if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                                    if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                        date_Item.STATUS = '<span style="color: cornflowerblue;font-size: 12px"">I</span>';
                                                                                    }
                                                                                }
                                                                            }

                                                                        } else {
                                                                        }
                                                                    });
                                                                });

                                                                $scope.MasterSum = 0;
                                                                $scope.PersonallSum = 0;
                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    var count = 0;
                                                                    var DateLast = 0;
                                                                    var DateNow = new Date();
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item, idx_Date) {
                                                                        if ($scope.CheckCordiDate(date_Item.DAY)) {
                                                                            if (date_Item.STATUS != '-') {
                                                                                if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                                    if (date_Item.STATUS.indexOf('&#x274C;') > 0) {
                                                                                        count++;
                                                                                    } else {
                                                                                        if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                            count++;
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    if ((date_Item.STATUS.indexOf('OPD') >= 0)) {
                                                                                        count++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        if (idx_Date === $scope.ReportSummary[0].DATEOFMONTH.length - 1) {
                                                                            DateLast = parseInt(date_Item.D_1Digit);
                                                                        }

                                                                    });
                                                                    Report_Arr.SUM = count;
                                                                    Report_Arr.SUM_PER = ((count / DateLast) * 100);
                                                                    Report_Arr.DATEMORE = DateLast;
                                                                    $scope.PersonallSum += Report_Arr.SUM_PER;
                                                                    // console.log(Report_Arr.PERID +' '+ $scope.PersonallSum +' += '+ ((count / DateLast) * 100))
                                                                });
                                                                $scope.PersonallSum = ($scope.PersonallSum / $scope.ReportSummary.length) * 100

                                                                $scope.Personall = $scope.ReportSummary.length

                                                                // console.log('ทั้งหมด ' + $scope.PersonallSum)

                                                                $scope.ReportSummaryFilter = $scope.ReportSummary;
                                                                $scope.ReportSummary = [];
                                                                angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                                                                    if($scope.Check_POSWORK(Report_Arr.POS_WORK)){
                                                                        $scope.ReportSummary.push(Report_Arr)
                                                                    }
                                                                });

                                                                angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                                    var count = 0;
                                                                    var DateLast = 0;
                                                                    var DateNow = new Date();
                                                                    angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item, idx_Date) {
                                                                        if ($scope.CheckCordiDate(date_Item.DAY)) {
                                                                            if (date_Item.STATUS != '-') {
                                                                                if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                                    if (date_Item.STATUS.indexOf('&#x274C;') > 0) {
                                                                                        count++;
                                                                                    } else {
                                                                                        if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                            count++;
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    if ((date_Item.STATUS.indexOf('OPD') >= 0)) {
                                                                                        count++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        if (idx_Date === $scope.ReportSummary[0].DATEOFMONTH.length - 1) {
                                                                            DateLast = parseInt(date_Item.D_1Digit);
                                                                        }

                                                                    });
                                                                    Report_Arr.SUM = count;
                                                                    Report_Arr.SUM_PER = ((count / DateLast) * 100);
                                                                    Report_Arr.DATEMORE = DateLast;
                                                                    $scope.MasterSum += Report_Arr.SUM_PER;
                                                                    // console.log(Report_Arr.PERID +' '+ $scope.PersonallSum +' += '+ ((count / DateLast) * 100))
                                                                });

                                                                // console.log(($scope.MasterSum + ' / ' + $scope.ReportSummary.length) + ' * ' + 100)
                                                                $scope.Master = $scope.ReportSummary.length

                                                                $scope.MasterSum = ($scope.MasterSum / $scope.ReportSummary.length) * 100

                                                                // console.log('อาจารย์ ' + $scope.MasterSum)

                                                                angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                                                                    if(!$scope.Check_POSWORK(Report_Arr.POS_WORK)){
                                                                        $scope.ReportSummary.push(Report_Arr)
                                                                    }
                                                                });
                                                                // $scope.ReportSummaryFilter = $scope.ReportSummary;
                                                                $scope.Loading = false;
                                                                $scope.CheckTable = true;
                                                            });
                                                    });

                                                // console.log($scope.ReportSummary);

                                                // console.log($scope.ReportSummary);

                                                // data = {
                                                //     'Dep_Code': $scope.DepartSelect,
                                                //     'Date_Cordi': DateStart,
                                                //     'Date_Cordi2': DateEnd,
                                                // };
                                            });
                                    }
                                });
                        } else {
                            $scope.FilterPosition = UniqueArraybyId($scope.Personal,
                                "PosCode");

                            function UniqueArraybyId(collection, keyname) {
                                var output = [],
                                    keys = [];

                                angular.forEach(collection, function (item) {
                                    var key = item[keyname];
                                    if (keys.indexOf(key) === -1) {
                                        keys.push(key);
                                        output.push(item);
                                    }
                                });
                                return output;
                            };

                            $scope.Personal = UniqueArraybyId($scope.Personal,
                                "PERID");

                            function UniqueArraybyId(collection, keyname) {
                                var output = [],
                                    keys = [];

                                angular.forEach(collection, function (item) {
                                    var key = item[keyname];
                                    if (keys.indexOf(key) === -1) {
                                        keys.push(key);
                                        output.push(item);
                                    }
                                });
                                return output;
                            };

                            // $scope.Personal = $filter('orderBy')($scope.Personal, 'PERID')

                            angular.forEach($scope.Personal, function (PersonItem) {
                                $scope.ReportSummary.push({
                                    'PERID': PersonItem.PERID,
                                    'NAME': PersonItem.NAME + " " + PersonItem.SURNAME,
                                    'POS_WORK': PersonItem.POS_WORK,
                                    'DATEOFMONTH': $scope.calArrayDate(new Date(date.getFullYear(), date.getMonth(), 1), new Date(date.getFullYear(), date.getMonth() + 1, 0)),
                                    'SUM': '',
                                    'SUM_PER': '',
                                    'DATEMORE': ''
                                });
                            })

                            angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                    date_Item.BG = {
                                        'text-align': 'center',
                                        'white-space': 'nowrap',
                                        'background-color': 'white'
                                    };
                                })
                            })

                            // $scope.ReportSummary = $filter('orderBy')($scope.ReportSummary, 'PERID')

                            // data = {
                            //     'Dep_Code': $scope.DepartSelect,
                            //     'Date_Cordi': DateStart,
                            //     'Date_Cordi2': DateEnd,
                            // };

                            if ($scope.CheckAll) {
                                data = {
                                    'Dep_Code': $scope.Depart,
                                    'Date_Cordi': DateStart,
                                    'Date_Cordi2': DateEnd,
                                };
                            } else {
                                data = {
                                    'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                    'Date_Cordi': DateStart,
                                    'Date_Cordi2': DateEnd,
                                };
                            }

                            $http.post('./ApiService/GetWorkReportOfMonth', data, {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            })
                                .then(function successCallback(response) {
                                    $scope.workReport = response.data;
                                    angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                        angular.forEach($scope.workReport, function (Work_Arr, idx) {
                                            if (Report_Arr.PERID == Work_Arr.PERID) {
                                                angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                    if (Work_Arr.A_Datetime == date_Item.DAY) {
                                                        if (Work_Arr.A_Status_in2 == 'สาย') {
                                                            statusIn = '<span style="color: #EA80FC">ส</span>';
                                                        } else if (Work_Arr.A_Status_in2 == 'ออกก่อน') {
                                                            statusIn = '<span style="color: #00796B">อ</span>';
                                                        } else if (Work_Arr.A_Status_in2 == 'ขาดงาน') {
                                                            statusIn = '<span style="color: #448AFF">ข</span>';
                                                        } else if (Work_Arr.A_Status_in2 == '' && Work_Arr.A_Time_in2 == '') {
                                                            statusIn = '<span style="color: #FF5252">ม</span>';
                                                        } else {
                                                            statusIn = '<span style="color: #00E676">ป</span>';
                                                        }
                                                        if (Work_Arr.A_Status_out2 == 'สาย') {
                                                            statusOut = '<span style="color: #EA80FC">ส</span>';
                                                        } else if (Work_Arr.A_Status_out2 == 'ออกก่อน') {
                                                            statusOut = '<span style="color: #00796B">อ</span>';
                                                        } else if (Work_Arr.A_Status_out2 == 'ขาดงาน') {
                                                            statusOut = '<span style="color: #448AFF">ข</span>';
                                                        } else if (Work_Arr.A_Status_out2 == '' && Work_Arr.A_Time_out2 == '') {
                                                            statusOut = '<span style="color: #FF5252">ม</span>';
                                                        } else {
                                                            statusOut = '<span style="color: #00E676">ป</span>';
                                                        }
                                                        date_Item.STATUS = statusIn + " / " + statusOut;
                                                    } else {
                                                        if (date_Item.STATUS != '') {

                                                        } else {
                                                            date_Item.STATUS = '-';
                                                        }
                                                    }
                                                });
                                            }
                                        })

                                    })

                                    if ($scope.CheckAll) {
                                        data = {
                                            'Dep_Code': $scope.Depart,
                                            'Date_Cordi': DateStart,
                                            'Date_Cordi2': DateEnd,
                                        };
                                    } else {
                                        data = {
                                            'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                            'Date_Cordi': DateStart,
                                            'Date_Cordi2': DateEnd,
                                        };
                                    }

                                    $http.post('./ApiService/GetDocTorSummary', data, {
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        }
                                    })
                                        .then(function successCallback(response) {
                                            angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                angular.forEach(response.data, function (DocTor_Arr) {
                                                    if (Report_Arr.PERID == DocTor_Arr.perid) {
                                                        angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                            if (date_Item.DAY == DocTor_Arr.day_of_month) {
                                                                date_Item.STATUS = '<span style="color: #00b92d">OPD</span> ';
                                                                date_Item.BG = {
                                                                    'text-align': 'center',
                                                                    'white-space': 'nowrap',
                                                                    'background-color': 'aliceblue'
                                                                };
                                                            }
                                                        });
                                                    }
                                                });
                                            });

                                            if ($scope.CheckAll) {
                                                data = {
                                                    'Dep_Code': $scope.Depart,
                                                    'Date_Cordi': DateStart,
                                                    'Date_Cordi2': DateEnd,
                                                };
                                            } else {
                                                data = {
                                                    'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                                                    'Date_Cordi': DateStart,
                                                    'Date_Cordi2': DateEnd,
                                                };
                                            }

                                            $http.post('./ApiService/GetLeaveReportOfMonth', data, {
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                }
                                            })
                                                .then(function successCallback(response) {
                                                    $scope.leaveReport = response.data;
                                                    var CheckHalfDay = true;
                                                    angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                        angular.forEach($scope.leaveReport, function (Leave_Arr, idx_leave) {
                                                            if (Report_Arr.PERID == Leave_Arr.PERID) {
                                                                CheckHalfDay = true;
                                                                angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                                    if (date_Item.DAY >= Leave_Arr.T_Leave_Date_Start && date_Item.DAY <= Leave_Arr.T_Leave_Date_End) {
                                                                        if (date_Item.STATUS != '-') {
                                                                            switch (Leave_Arr.T_Leave_Type) {
                                                                                case '0':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลก.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                    }
                                                                                    break;
                                                                                case '1':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลป.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                    }
                                                                                    break;
                                                                                case '2':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลพ.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                    }
                                                                                    break;
                                                                                case '3':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลศ.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                    }
                                                                                    break;
                                                                                case '4':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลฝ.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                    }
                                                                                    break;
                                                                                case '5':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลค.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                    }
                                                                                    break;
                                                                                case '6':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ลส.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                    }
                                                                                    break;
                                                                                case '7':
                                                                                    if (CheckHalfDay) {
                                                                                        if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (บ่าย)") {
                                                                                            date_Item.STATUS = date_Item.STATUS.split(' / ')[0] + '<span style="color: #3F51B5"> / ร.</span>';
                                                                                            CheckHalfDay = false;
                                                                                        } else if (Leave_Arr.T_Day_Type_Start == "ครึ่งวัน (เช้า)") {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> / ' + date_Item.STATUS.split(' / ')[1];
                                                                                            CheckHalfDay = false;
                                                                                        } else {
                                                                                            date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                        }
                                                                                    } else {
                                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                    }
                                                                                    break;
                                                                                default:
                                                                                    break;
                                                                            }
                                                                        } else {
                                                                            switch (Leave_Arr.T_Leave_Type) {
                                                                                case '0':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลก.</span> ';
                                                                                    break;
                                                                                case '1':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลป.</span> ';
                                                                                    break;
                                                                                case '2':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลพ.</span> ';
                                                                                    break;
                                                                                case '3':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลศ.</span> ';
                                                                                    break;
                                                                                case '4':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลฝ.</span> ';
                                                                                    break;
                                                                                case '5':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลค.</span> ';
                                                                                    break;
                                                                                case '6':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ลส.</span> ';
                                                                                    break;
                                                                                case '7':
                                                                                    date_Item.STATUS = '<span style="color: #3F51B5"> ร.</span> ';
                                                                                    break;
                                                                                default:
                                                                                    break;
                                                                            }
                                                                        }
                                                                    } else {

                                                                    }
                                                                });
                                                            }

                                                        });
                                                    });
                                                    angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                        angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                            if (date_Item.DAY_NAME == 'Saturday' || date_Item.DAY_NAME == 'Sunday') {
                                                                date_Item.STATUS = '<span style="color: #3F51B5"> &#x274C; </span>';
                                                            } else {
                                                                angular.forEach($scope.Holiday, function (hoItem) {
                                                                    if (hoItem.Date_stop == date_Item.DAY) {
                                                                        date_Item.STATUS = '<span style="color: #3F51B5"> &#x274C; </span>';
                                                                    } else {
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });

                                                    angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                        angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item) {
                                                            if ((parseInt(Report_Arr.POS_WORK)) >= 551 && ((parseInt(Report_Arr.POS_WORK)) <= 558)
                                                                || (parseInt(Report_Arr.POS_WORK) == 800) || (parseInt(Report_Arr.POS_WORK) == 221)
                                                                || (parseInt(Report_Arr.POS_WORK) == 211)|| (parseInt(Report_Arr.POS_WORK) == 212)){
                                                                if (date_Item.STATUS != '-' && (date_Item.STATUS.indexOf('<span style="color: #3F51B5"> &#x274C; </span>') < 0)) {
                                                                    if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                        if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                            date_Item.STATUS = '<span style="color: cornflowerblue;font-size: 12px"">I</span>';
                                                                        }
                                                                    }
                                                                }

                                                            } else {
                                                            }
                                                        });
                                                    });

                                                    $scope.MasterSum = 0;
                                                    $scope.PersonallSum = 0;
                                                    angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                        var count = 0;
                                                        var DateLast = 0;
                                                        var DateNow = new Date();
                                                        angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item, idx_Date) {
                                                            if ($scope.CheckCordiDate(date_Item.DAY)) {
                                                                if (date_Item.STATUS != '-') {
                                                                    if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                        if (date_Item.STATUS.indexOf('&#x274C;') > 0) {
                                                                            count++;
                                                                        } else {
                                                                            if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                count++;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if ((date_Item.STATUS.indexOf('OPD') >= 0)) {
                                                                            count++;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (idx_Date === $scope.ReportSummary[0].DATEOFMONTH.length - 1) {
                                                                DateLast = parseInt(date_Item.D_1Digit);
                                                            }

                                                        });
                                                        Report_Arr.SUM = count;
                                                        Report_Arr.SUM_PER = ((count / DateLast) * 100);
                                                        Report_Arr.DATEMORE = DateLast;
                                                        $scope.PersonallSum += Report_Arr.SUM_PER;
                                                        // console.log(Report_Arr.PERID +' '+ $scope.PersonallSum +' += '+ ((count / DateLast) * 100))
                                                    });
                                                    $scope.PersonallSum = ($scope.PersonallSum / $scope.ReportSummary.length) * 100

                                                    $scope.Personall = $scope.ReportSummary.length

                                                    // console.log('ทั้งหมด ' + $scope.PersonallSum)

                                                    $scope.ReportSummaryFilter = $scope.ReportSummary;
                                                    $scope.ReportSummary = [];
                                                    angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                                                        if($scope.Check_POSWORK(Report_Arr.POS_WORK)){
                                                            $scope.ReportSummary.push(Report_Arr)
                                                        }
                                                    });

                                                    angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                        var count = 0;
                                                        var DateLast = 0;
                                                        var DateNow = new Date();
                                                        angular.forEach(Report_Arr.DATEOFMONTH, function (date_Item, idx_Date) {
                                                            if ($scope.CheckCordiDate(date_Item.DAY)) {
                                                                if (date_Item.STATUS != '-') {
                                                                    if ((date_Item.STATUS.indexOf('<span style="color: #FF5252">ม</span> / <span style="color: #FF5252">ม</span>') < 0)) {
                                                                        if (date_Item.STATUS.indexOf('&#x274C;') > 0) {
                                                                            count++;
                                                                        } else {
                                                                            if ((date_Item.STATUS.indexOf('ล') < 0)) {
                                                                                count++;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if ((date_Item.STATUS.indexOf('OPD') >= 0)) {
                                                                            count++;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (idx_Date === $scope.ReportSummary[0].DATEOFMONTH.length - 1) {
                                                                DateLast = parseInt(date_Item.D_1Digit);
                                                            }

                                                        });
                                                        Report_Arr.SUM = count;
                                                        Report_Arr.SUM_PER = ((count / DateLast) * 100);
                                                        Report_Arr.DATEMORE = DateLast;
                                                        $scope.MasterSum += Report_Arr.SUM_PER;
                                                        // console.log(Report_Arr.PERID +' '+ $scope.PersonallSum +' += '+ ((count / DateLast) * 100))
                                                    });

                                                    // console.log(($scope.MasterSum + ' / ' + $scope.ReportSummary.length) + ' * ' + 100)
                                                    $scope.Master = $scope.ReportSummary.length

                                                    $scope.MasterSum = ($scope.MasterSum / $scope.ReportSummary.length) * 100

                                                    // console.log('อาจารย์ ' + $scope.MasterSum)

                                                    angular.forEach($scope.ReportSummaryFilter, function (Report_Arr) {
                                                        if(!$scope.Check_POSWORK(Report_Arr.POS_WORK)){
                                                            $scope.ReportSummary.push(Report_Arr)
                                                        }
                                                    });
                                                    // $scope.ReportSummaryFilter = $scope.ReportSummary;
                                                    $scope.Loading = false;
                                                    $scope.CheckTable = true;
                                                });
                                        });

                                    // console.log($scope.ReportSummary);

                                    // console.log($scope.ReportSummary);

                                    // data = {
                                    //     'Dep_Code': $scope.DepartSelect,
                                    //     'Date_Cordi': DateStart,
                                    //     'Date_Cordi2': DateEnd,
                                    // };
                                });
                        }
                    });
            }
        }

    });
    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = monthNames[parseInt(value) - 1]
            }
            return (
                DateTrans
            );
        }
    });
</script>
</body>

</html>
