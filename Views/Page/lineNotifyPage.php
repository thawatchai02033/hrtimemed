<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

    <link href="./tools/customTemplate/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="stylesheet" href="./tools/ui-select/dist/select.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- vuetify   -->
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table > caption + thead > tr:first-child > td,
        .table > caption + thead > tr:first-child > th,
        .table > colgroup + thead > tr:first-child > td,
        .table > colgroup + thead > tr:first-child > th,
        .table > thead:first-child > tr:first-child > td,
        .table > thead:first-child > tr:first-child > th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                สรุปการปฏิบัติงาน
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> การเข้า-ออกงาน</a></li>
                <li class="active">สรุปการปฏิบัติงาน</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div id="app">
                <v-app>
                    <v-content>
                        <v-container>
                            <v-row>
                                <v-col
                                        cols="12"
                                >
                                    <v-card>
                                        <v-app-bar
                                                dense
                                                dark
                                        >
                                            <v-toolbar-title>แจ้งเตือนผ่านไลน์</v-toolbar-title>
                                        </v-app-bar>
                                        <v-list-item>
                                            <v-container>
                                                <v-card>
                                                    <v-card-title>
                                                        <span>แจ้งเตือนข้อมูลเวลา เข้า - ออก (RealTIme)(Beta)</span>
                                                    </v-card-title>
                                                    <v-list-item>
                                                        <v-row>
                                                            <v-col
                                                                    cols="12"
                                                                    md="6">
                                                                <div align="center">
                                                                    <h2 style="font-weight: bold">
                                                                        ขั้นตอนวิธีการสมัครการแจ้งเตือนผ่าน Line Notify
                                                                    <h2/>
                                                                </div>
                                                                <v-list-item>
                                                                    <v-card>
                                                                        <v-img
                                                                                src="./Image/line0.png"
                                                                                class="white--text align-end"
                                                                                gradient="to bottom, rgba(0,0,0,.1), rgba(0,0,0,.5)"
                                                                                width="600"
                                                                        >
                                                                            <v-card-title></v-card-title>
                                                                        </v-img>

                                                                        <v-card-actions>
                                                                            <v-spacer></v-spacer>
                                                                            <p style="font-weight: bold;font-size: 16px;color: #0f192a">
                                                                                ขั้นตอนที่ 1 เข้าไปยังบริการ Line Notify
                                                                                https://notify-bot.line.me/my/
                                                                                <br/>
                                                                                ทำการเข้าสู่ระบบ
                                                                            </p>
                                                                        </v-card-actions>
                                                                    </v-card>
                                                                </v-list-item>
                                                                <v-list-item>
                                                                    <v-card>
                                                                        <v-img
                                                                                src="./Image/line1.png"
                                                                                class="white--text align-end"
                                                                                gradient="to bottom, rgba(0,0,0,.1), rgba(0,0,0,.5)"
                                                                                width="600"
                                                                        >
                                                                            <v-card-title></v-card-title>
                                                                        </v-img>

                                                                        <v-card-actions>
                                                                            <v-spacer></v-spacer>
                                                                            <p style="font-weight: bold;font-size: 16px;color: #0f192a">
                                                                                ขั้นตอนที่ 2 กดปุ่ม "ออก Token"
                                                                            </p>
                                                                        </v-card-actions>
                                                                    </v-card>
                                                                </v-list-item>
                                                                <v-list-item>
                                                                    <v-card>
                                                                        <v-img
                                                                                src="./Image/line2.png"
                                                                                class="white--text align-end"
                                                                                gradient="to bottom, rgba(0,0,0,.1), rgba(0,0,0,.5)"
                                                                                width="600"
                                                                        >
                                                                            <v-card-title></v-card-title>
                                                                        </v-img>

                                                                        <v-card-actions>
                                                                            <v-spacer></v-spacer>
                                                                            <p style="font-weight: bold;font-size: 16px;color: #0f192a">
                                                                                ขั้นตอนที่ 3.1 ใส่ชื่อ Token ตามต้องการ เช่น
                                                                                "แจ้งเตือนข้อมูลการเข้าออกงาน....."
                                                                                <br/>
                                                                                ขั้นตอนที่ 3.2 คลิกเลือกรับการแจ้งเตือนแบบตัวต่อตัวจาก LINE Notify
                                                                            </p
                                                                        </v-card-actions>
                                                                    </v-card>
                                                                </v-list-item>
                                                                <v-list-item>
                                                                    <v-card>
                                                                        <v-img
                                                                                src="./Image/line3.png"
                                                                                class="white--text align-end"
                                                                                gradient="to bottom, rgba(0,0,0,.1), rgba(0,0,0,.5)"
                                                                                width="600"
                                                                        >
                                                                            <v-card-title></v-card-title>
                                                                        </v-img>

                                                                        <v-card-actions>
                                                                            <v-spacer></v-spacer>
                                                                            <p style="font-weight: bold;font-size: 16px;color: #0f192a">
                                                                                ขั้นตอนที่ 4 กดปุ่ม "คัดลอก"
                                                                                <br/>
                                                                                นำ Token ที่ได้วางในช่อง Line Token ด้านล่างและกดปุ่มส่ง
                                                                            </p>
                                                                        </v-card-actions>
                                                                    </v-card>
                                                                </v-list-item>
                                                            </v-col>
                                                            <v-col
                                                                    cols="12"
                                                                    md="6">
                                                                <div align="center">
                                                                    <h2 style="font-weight: bold">
                                                                        บันทึกไลน์ Token
                                                                        <h2/>
                                                                </div>
                                                                <v-list-item>
                                                                        <v-text-field label="Line Token" v-model="line_token"></v-text-field>
                                                                </v-list-item>
                                                                <v-list-item class="d-flex">
                                                                    <div align="center" style="width: 100%">
                                                                        <v-btn @click="SaveToken" outlined>
                                                                            บันทึก
                                                                        </v-btn>
                                                                    </div>
                                                                </v-list-item>
                                                                <v-list-item class="d-flex justify-center">
                                                                    <v-card>
                                                                        <v-img
                                                                                src="./Image/line4.png"
                                                                                class="white--text align-end"
                                                                                gradient="to bottom, rgba(0,0,0,.1), rgba(0,0,0,.5)"
                                                                                width="600"
                                                                        >
                                                                            <v-card-title></v-card-title>
                                                                        </v-img>

                                                                        <v-card-actions>
                                                                            <v-spacer></v-spacer>
                                                                            <p style="font-weight: bold;font-size: 16px;color: #0f192a">
                                                                                ภาพตัวอย่าง เมื่อทำการแจ้งเตือนผ่านไลน์
                                                                            </p>
                                                                        </v-card-actions>
                                                                    </v-card>
                                                                </v-list-item>
                                                            </v-col>
                                                        </v-row>
                                                    </v-list-item>
                                                </v-card>
                                            </v-container>
                                        </v-list-item>
                                    </v-card>
                                </v-col>
                            </v-row>
                        </v-container>
                    </v-content>
                </v-app>
            </div>
        </section>
        <!-- /.content -->
        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/Js/angular-sanitize.min.js"></script>
<script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>
<script src="./tools/ui-select/dist/select2.js"></script>

<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //Date picker
        $('#datepicker2').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
    })
</script>

<script>
    new Vue({
        el: '#app',
        vuetify: new Vuetify(),
        data: () => ({
            Depart: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>,
            line_token: ''
        }),
        methods: {
            SaveToken() {
                const param = {
                    'Line_Token': this.line_token
                };

                const headerAxios = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }

                var that = this

                axios.post('./ApiService/saveLineNotify', param, headerAxios)
                    .then((response) => {
                        if (response.data.Status) {
                            that.line_token = ''
                            Swal({
                                type: 'success',
                                title: response.data.Message
                            });
                        } else {
                            Swal({
                                type: 'error',
                                title: response.data.Message
                            });
                        }
                    })
            },
            init() {
            },
        },
        mounted() {
            this.init()
        }
    })
</script>

<script>
    var app = angular.module("myApp", ["datatables", "ui.bootstrap", 'ngAnimate']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
</body>

</html>