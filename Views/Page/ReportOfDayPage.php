<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Work Time Summary</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif" />

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet" />
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive>div.dataTables_wrapper>div.row>div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            /* white-space: nowrap; */
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td,
        .table>caption+thead>tr:first-child>th,
        .table>colgroup+thead>tr:first-child>td,
        .table>colgroup+thead>tr:first-child>th,
        .table>thead:first-child>tr:first-child>td,
        .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            /* white-space: nowrap; */
            width: 1%;
        }

        td {
            /* white-space: nowrap; */
        }

        table.table-bordered {
            border: 1px solid #64FFDA;
            margin-top: 20px;
        }

        table.table-bordered>thead>tr>th {
            border: 1px solid #64FFDA;
        }

        table.table-bordered>tbody>tr>td {
            border: 1px solid #64FFDA;
        }

        th {
            /* white-space: nowrap; */
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .pic {
            margin: 50px;
            /* demo spacing */
            display: inline-block;
            vertical-align: middle;
            width: 0;
            height: 0;
        }

        .arrow-left {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-right: 30px solid #737373;
        }

        .arrow-right {
            border-top: 30px solid transparent;
            border-bottom: 30px solid transparent;
            border-left: 30px solid #737373;
        }

        .arrow-up {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-bottom: 30px solid #737373;
        }

        .arrow-down {
            border-left: 30px solid transparent;
            border-right: 30px solid transparent;
            border-top: 30px solid #737373;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }

        .modal,
        .modal-open {
            overflow: hidden
        }

        .modal {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1050;
            display: none;
            outline: 0
        }

        .modal-open .modal {
            overflow-x: hidden;
            overflow-y: auto
        }

        .modal-dialog {
            position: relative;
            width: auto;
            margin: .5rem;
            pointer-events: none
        }

        .modal.show .modal-dialog {
            transform: translate(0)
        }

        .modal-dialog-centered {
            display: flex;
            align-items: center;
            min-height: calc(100% - 1rem)
        }

        .modal-content {
            position: relative;
            display: flex;
            flex-direction: column;
            width: 100%;
            pointer-events: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .2);
            border-radius: .3rem;
            box-shadow: 0 .25rem .5rem rgba(0, 0, 0, .5);
            outline: 0
        }

        .modal-backdrop {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040;
            background-color: #000
        }

        .modal-backdrop.fade {
            opacity: 0
        }

        .modal-backdrop.show {
            opacity: .26
        }

        .modal-header {
            display: flex;
            align-items: flex-start;
            justify-content: space-between;
            padding: 1rem;
            border-bottom: 1px solid #e9ecef;
            border-top-left-radius: .3rem;
            border-top-right-radius: .3rem
        }

        .modal-header .close {
            padding: 1rem;
            margin: -1rem -1rem -1rem auto
        }

        .modal-title {
            margin-bottom: 0;
            line-height: 1.5
        }

        .modal-body {
            position: relative;
            flex: 1 1 auto;
            padding: 1rem
        }

        .modal-footer {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            padding: 1rem;
            border-top: 1px solid #e9ecef
        }

        .modal-footer>:not(:first-child) {
            margin-left: .25rem
        }

        .modal-footer>:not(:last-child) {
            margin-right: .25rem
        }

        .modal-scrollbar-measure {
            position: absolute;
            top: -9999px;
            width: 50px;
            height: 50px;
            overflow: scroll
        }

        @media (min-width:576px) {
            .modal-dialog {
                max-width: 500px;
                margin: 1.75rem auto
            }

            .modal-dialog-centered {
                min-height: calc(100% - 3.5rem)
            }

            .modal-content {
                box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .5)
            }

            .modal-sm {
                max-width: 300px
            }
        }

        @media (min-width:992px) {
            .modal-lg {
                max-width: 800px
            }
        }

        .modal-dialog .modal-content {
            box-shadow: 0 27px 24px 0 rgba(0, 0, 0, .2), 0 40px 77px 0 rgba(0, 0, 0, .22);
            border-radius: 6px;
            border: none
        }

        .modal-dialog .modal-content .card-signup {
            margin: 0
        }

        .modal-dialog .modal-content .card-signup .modal-header {
            padding-top: 0
        }

        .modal-dialog .close:focus {
            outline: none
        }

        .modal-dialog .modal-header {
            border-bottom: none;
            padding: 24px 24px 0
        }

        .modal-dialog .modal-header .modal-title {
            text-align: center;
            width: 100%
        }

        .modal-dialog .modal-header .close {
            position: absolute;
            top: 15px;
            right: 20px
        }

        .modal-dialog .modal-body {
            padding: 24px 24px 16px
        }

        .modal-dialog .modal-footer {
            border-top: none;
            padding: 24px
        }

        .modal-dialog .modal-footer.text-center {
            text-align: center
        }

        .modal-dialog .modal-footer button {
            margin: 0;
            padding-left: 16px;
            padding-right: 16px;
            width: auto
        }

        .modal-dialog .modal-footer button.pull-left {
            padding-left: 5px;
            padding-right: 5px;
            position: relative;
            left: -5px
        }

        .modal-dialog .modal-body+.modal-footer {
            padding-top: 0
        }

        .modal-backdrop {
            background: rgba(0, 0, 0, .3)
        }

        .modal .modal-dialog {
            margin-top: 100px
        }

        .modal .modal-dialog.modal-login {
            width: 360px
        }

        .modal .modal-dialog.modal-login .modal-header .close {
            color: #fff;
            text-shadow: none;
            position: absolute
        }

        .modal .modal-dialog.modal-login .modal-footer {
            padding-bottom: 0;
            padding-top: 0
        }

        .modal .modal-dialog.modal-login .modal-body {
            padding-left: 4px;
            padding-bottom: 0;
            padding-top: 0
        }

        .modal .modal-dialog.modal-login .card-signup {
            margin-bottom: 0
        }

        .modal .modal-dialog.modal-signup {
            max-width: 900px
        }

        .modal .modal-dialog.modal-signup .info-horizontal {
            padding: 0 0 20px
        }

        .modal .modal-dialog.modal-signup .modal-title {
            text-align: center;
            width: 100%
        }

        .modal .modal-dialog.modal-signup .modal-footer {
            padding: 0 5px
        }

        .modal .modal-dialog.modal-signup .modal-header {
            padding-top: 0
        }

        .modal .modal-dialog.modal-signup .card-signup {
            padding: 40px 0;
            margin-bottom: 0
        }

        .modal .modal-dialog.modal-signup .modal-body {
            padding-bottom: 0;
            padding-top: 0
        }

        .modal .modal-header .close {
            color: #999
        }

        .modal .modal-header .close:focus,
        .modal .modal-header .close:hover {
            opacity: 1
        }

        .modal .modal-header .close i {
            font-size: 16px
        }

        .modal-notice .instruction {
            margin-bottom: 25px
        }

        .modal-notice .picture {
            max-width: 150px
        }

        .modal-notice .modal-content .btn-raised {
            margin-bottom: 15px
        }

        .modal-small {
            width: 300px;
            margin: 0 auto
        }

        .modal-small .modal-body {
            margin-top: 20px
        }

        div.dtr-modal {
            position: fixed;
            box-sizing: border-box;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 100;
            padding: 10em 1em
        }

        div.dtr-modal div.dtr-modal-display {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 50%;
            height: 50%;
            margin: auto;
            z-index: 102;
            overflow: auto;
            background-color: #f5f5f7;
            border: 1px solid #000;
            border-radius: .5em;
            box-shadow: 0 12px 30px rgba(0, 0, 0, .6)
        }

        div.dtr-modal div.dtr-modal-content {
            position: relative;
            padding: 1em
        }

        div.dtr-modal div.dtr-modal-close {
            position: absolute;
            top: 6px;
            right: 6px;
            width: 22px;
            height: 22px;
            border: 1px solid #eaeaea;
            background-color: #f9f9f9;
            text-align: center;
            border-radius: 3px;
            cursor: pointer;
            z-index: 12
        }

        div.dtr-modal div.dtr-modal-close:hover {
            background-color: #eaeaea
        }

        div.dtr-modal div.dtr-modal-background {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 101;
            background: rgba(0, 0, 0, .6)
        }

        @media screen and (max-width:767px) {
            div.dtr-modal div.dtr-modal-display {
                width: 95%
            }
        }

        div.dtr-bs-modal table.table tr:first-child td {
            border-top: none
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
    <div id="wrapper" class="wrapper">

        <?php require './Views/Header.php'?>
        <!-- Left side column. contains the logo and sidebar -->
        <?php require './Views/Menu.php'?>

        <!-- Content Wrapper. Contains page content -->
        <div id="page-wrapper" class="content-wrapper" ng-controller="myCtrl">
            <section class="content-header">
                <h1>
                    ภาพรวมการปฎิบัติงานประจำวัน
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-pie-chart"></i> รายงาน</a></li>
                    <li class="active">ภาพรวมการปฎิบัติงานประจำวัน</li>
                </ol>
            </section>
            <!-- /.row -->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box box-primary">
                            <div class="panel panel-default">
                                <div class="box-body">
                                    <!-- Date -->
                                    <div align="center" style="margin-top: 25px">
                                        <label class="box-title">
                                            <h1 style="color: rgb(0, 73, 102);font-weight: bold">สรุปภาพรวมการปฎิบัติงานประจำวัน</h1>
                                        </label>
                                    </div>
                                    <div class="form-group" style="margin-top: 25px">
                                        <div class="col-lg-3" style="text-align: center">
                                            <label>ประจำวัน:</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="col-lg-12">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="datepicker" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3" align="center">
                                            <button type="button" class="btn btn-block btn-info" style="width:40%;align-content: center" ng-click="GetReport()">เรียกดู</button>
                                        </div>
                                    </div>
                                    <!-- /.form group -->
                                </div>
                                <div align="center" style="margin-top: 25px">
                                    <label class="box-title">แสดงข้อมูลประจำวัน <h1 style="color: rgb(0, 204, 0);font-weight: bold">{{DateToShow | DateThai}}</h1></label>
                                </div>
                                <button class="btn btn-info btn-round" data-toggle="modal" data-target="#noticeModal" ng-show="false" id="showModal">
                                    Notice modal
                                </button>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table width="100%" class="table table-bordered" datatable="ng" dt-options="dtOptions" dt-instance="dtInstance" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>กลุ่มหน่วยงาน</th>
                                                    <th>หน่วยงาน</th>
                                                    <th>จำนวนบุคลากร</th>
                                                    <th>สาย</th>
                                                    <th>ขาดงาน</th>
                                                    <th>ลากิจส่วนตัว</th>
                                                    <th>ลาป่วย</th>
                                                    <th>ลาพักผ่อน</th>
                                                    <th>ลาศึกษาต่อ</th>
                                                    <th>ลาฝึกอบรม ดูงาน</th>
                                                    <th>ลาคลอดบุตร</th>
                                                    <th>ลาอุปสมบท,ประกอบพิธีฮัจย์</th>
                                                    <th>ลาอื่นๆ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="odd gradeX" style="cursor: pointer;" ng-repeat="itemReport in ReportSummary2" ng-click="ShowDetails(itemReport)">
                                                    <td style="color:maroon;font-weight: bold;text-align: left">{{itemReport.GROUP_NAME}}</td>
                                                    <td style="color:green;font-weight: bold;text-align: left">{{itemReport.NAME}}</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.PersonLength != 0"><span style="color: red">{{itemReport.PersonLength}} </span>คน</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.PersonLength == 0"><span style="color: red">รอการ Approve</span></td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.workLate != 0">{{itemReport.workLate}} ครั้ง</td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.workLate == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.workAbsence != 0">{{itemReport.workAbsence}} ครั้ง</td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.workAbsence == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave0 != 0">{{itemReport.Leave0}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave0DaySum != 0">{{itemReport.Leave0DaySum}} วัน</span><span ng-if="itemReport.Leave0DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave0 == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave1 != 0">{{itemReport.Leave1}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave1DaySum != 0">{{itemReport.Leave1DaySum}} วัน</span><span ng-if="itemReport.Leave1DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave1 == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave2 != 0">{{itemReport.Leave2}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave2DaySum != 0">{{itemReport.Leave2DaySum}} วัน</span><span ng-if="itemReport.Leave2DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave2 == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave3 != 0">{{itemReport.Leave3}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave3DaySum != 0">{{itemReport.Leave3DaySum}} วัน</span><span ng-if="itemReport.Leave3DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave3 == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave4 != 0">{{itemReport.Leave4}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave4DaySum != 0">{{itemReport.Leave4DaySum}} วัน</span><span ng-if="itemReport.Leave4DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave4 == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave5 != 0">{{itemReport.Leave5}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave5DaySum != 0">{{itemReport.Leave5DaySum}} วัน</span><span ng-if="itemReport.Leave5DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave5 == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave6 != 0">{{itemReport.Leave6}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave6DaySum != 0">{{itemReport.Leave6DaySum}} วัน</span><span ng-if="itemReport.Leave6DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave6 == 0">-</td>
                                                    <td style="color:green;font-weight: bold;text-align: center" ng-if="itemReport.Leave7 != 0">{{itemReport.Leave7}} ครั้ง / <span style="color:red" ng-if="itemReport.Leave7DaySum != 0">{{itemReport.Leave7DaySum}} วัน</span><span ng-if="itemReport.Leave7DaySum == 0"> - วัน</span></td>
                                                    <td style="color:red;font-weight: bold;text-align: center" ng-if="itemReport.Leave7 == 0">-</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="3" style="font-weight: bold;text-align: center">
                                                        <h4>รวม</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{workLateSum}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{workAbsenceSum}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave0}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave1}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave2}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave3}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave4}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave5}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave6}}</h4>
                                                    </th>
                                                    <th style="font-weight: bold;text-align: center">
                                                        <h4>{{Leave7}}</h4>
                                                    </th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <!-- /.table-responsive -->
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            </section>
            <!-- /.row -->

            <!-- notice modal -->
            <div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-notice">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel">สรุปรายละเอียดการปฏิบัติงานประจำหน่วยงาน : <span style="font-weight: bold;color:rebeccapurple">{{NAMEDETAIL}}</span></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                <i class="material-icons">close</i>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p style="text-align: center;color: teal;font-weight: bold">ประเภทการปฏิบัติงาน</p>
                            <div class="instruction" ng-repeat="S_Person in StatusDataPerson">
                                <div class="row">
                                    <p style="text-align: left;color: rgb(255, 0, 170);font-weight: bold;margin-left: 15px">คุณ : {{S_Person.name}}</p>
                                    <div class="col-md-6">
                                        <strong style="color:royalblue" ng-if="S_Person.Status == 'สาย'">{{S_Person.Status}}</strong>
                                        <strong style="color:royalblue" ng-if="S_Person.Status == 'สาย'">{{S_Person.Status}}</strong>
                                        <p class="description">วันที่ : <span style="font-weight: bold;color:rebeccapurple">{{S_Person.Day}}</span> เวลา <span style="font-weight: bold;color:tomato" ng-if="S_Person.Time != ''">{{S_Person.Time}}</span><span style="font-weight: bold;color:tomato" ng-if="S_Person.Time == ''"> - </span> น.</p>
                                    </div>
                                    <div class="col-md-6">
                                        <strong style="color:royalblue">หมายเหตุ*</strong>
                                        <p class="description" style="font-weight: bold;color: darkorange">{{S_Person.Comment}}</p>
                                    </div>
                                </div>
                                <hr style="border: 0.5px solid maroon;" />
                            </div>
                            <p style="text-align: center;color: teal;font-weight: bold">ประเภทการลา</p>
                            <div class="instruction" ng-repeat="L_Person in LeaveDataPerson">
                                <div class="row">
                                    <p style="text-align: left;color: rgb(255, 0, 170);font-weight: bold;margin-left: 15px">คุณ : {{L_Person.name}}</p>
                                    <div class="col-md-6">
                                        <strong style="color:royalblue">{{L_Person.T_Leave_Type}}</strong>
                                        <p class="description">วันที่เริ่ม : <span style="font-weight: bold;color:rebeccapurple">{{L_Person.T_Leave_Date_Start}}</span></p>
                                        <p class="description">วันที่สิ้นสุด : <span style="font-weight: bold;color:rebeccapurple">{{L_Person.T_Leave_Date_End}}</span></p>
                                        <p class="description">เหตุผล: {{L_Person.T_Leave_Reason}}</p>
                                        <p class="description">จำนวน: <span style="font-weight: bold;color:rebeccapurple">{{L_Person.T_Work_Summary | NumDeci}}</span> วันทำการ</p>
                                    </div>
                                    <div class="col-md-6">
                                        <strong style="color:royalblue">ประเภท</strong>
                                        <p class="description">{{L_Person.T_Day_Type_Start}}</p>
                                        <p class="description">{{L_Person.T_Day_Type_End}}</p>
                                    </div>
                                </div>
                                <hr style="border: 0.5px solid maroon;" />
                            </div>
                        </div>
                        <div class="modal-footer justify-content-center">
                            <button type="button" class="btn btn-danger btn-round" data-dismiss="modal">ปิดหน้าต่าง</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loader" align="center" ng-if="Loading">
                <img class="loaderImg" src="./Image/Preloader_2.gif" />
            </div>
        </div>
        <!-- /.content-wrapper -->
        <?php require './Views/Footer.php'?>
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="./tools/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="./tools/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="./tools/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="./tools/dist/js/demo.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="./tools/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./tools/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="./tools/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- date-range-picker -->
    <script src="./tools/bower_components/moment/min/moment.min.js"></script>
    <script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- bootstrap datepicker thai-->
    <script src="./tools/bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.th.min.js"></script>
    <!-- bootstrap color picker -->
    <script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- Select2 -->
    <script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="./tools/plugins/iCheck/icheck.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="./tools/dist/js/sb-admin-2.js"></script>

    <!-- SweetDialog JS-->
    <script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>

    <script src="./tools/Js/angular.min.js"></script>
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
    <script src="./tools/Js/angular-datatables.min.js"></script>
    <script src="./tools/Js/angular-sanitize.min.js"></script>
    <script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <script src="./tools/Js/angular-animate.min.js"></script>

    <!--   Core JS Files   -->
    <script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

    <script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

    <!-- Sharrre libray -->
    <script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
            if (<?php echo $this->model->CheckStatus ?>) {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
                setInterval(function () {
                    $.ajax({
                        type: "POST",
                        url: './ApiService/T_GetCountDataNoteOfLeave',
                        contentType: "application/json",
                        data: JSON.stringify({
                            DepCode: "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>
                        }),
                        error: function (data) {
                            console.log(data);
                        },
                        success: function (data) {
                            var ObjResult = JSON.parse(data);
                            if (ObjResult.Status) {
                                if (ObjResult.Message > 0) {
                                    md.showNotification('bottom', 'right', ObjResult.Message);
                                    $('#showCountLeave').html(ObjResult.Message);
                                } else {
                                    $('#showCountLeave').html('');
                                }
                            }
                        },
                    });
                }, 30000);
            }
        });
    </script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', {
                'placeholder': 'dd/mm/yyyy'
            })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', {
                'placeholder': 'mm/dd/yyyy'
            })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'YYYY-MM-DD'
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                            'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                        'MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'th'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })
    </script>
    <script>
        var app = angular.module("myApp", ["datatables", "ui.bootstrap", 'ngAnimate']);
        app.controller("myHeader", function ($scope, $http, $window) {
            $scope.LogOut = function () {
                $http.post('./ApiService/LogOut', {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        if (response.data) {
                            $window.location.href = './Login';
                        } else {
                            Swal({
                                type: 'error',
                                title: 'ออกจากระบบไม่สำเร็จ'
                            });
                        };
                    });
            }
        });
    </script>
    <script>
        app.controller("myCtrl", function ($scope, $http, $window, $timeout, $sce, DTOptionsBuilder, $uibModal) {
            $scope.Depart = <?php echo $this->Department; ?>;
            $scope.Holiday = <?php echo $this -> Holiday; ?> ;
            $scope.ReportSummary = [];
            $scope.ReportSummary2 = [];
            $scope.Loading = true;
            $scope.DateToShow = '';
            var monthSelect = '';

            $scope.init = function () {
                $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250, 500, 1000])
                $scope.DepartSelect = "" + <?php echo ($PersonalData["DataPerson"]["Dep_Code"]); ?>;

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!

                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }

                $scope.dateMax = today;

                document.getElementById('datepicker').value = dd + '/' + mm + '/' + yyyy;
                $scope.GetReport();
                // var dateNow = new Date();
                // $scope.MonthSelect = '' + (dateNow.getMonth() + 1);
                // $scope.YearSelect = '' + dateNow.getFullYear();
                // var monthInit = dateNow.getMonth() + 1;
                // if (monthInit < 10) {
                //     monthSelect = '0' + monthInit;
                // } else {
                //     monthSelect = monthInit;
                // }
                $scope.Loading = false;
            }

            $timeout($scope.init)

            $scope.GetReport = function () {
                $scope.ReportSummary = [];
                $scope.ReportSummary2 = [];
                $scope.Loading = true;
                var DateStart = $scope.GetTimeToDb(new Date(SetDateToDB(document.getElementById('datepicker').value)));
                $scope.DateToShow = SetDateToDB(document.getElementById('datepicker').value);
                // var DateEnd = $scope.GetTimeToDb(lastDay);
                var options = {
                    weekday: 'long'
                };
                var RangeDate = '';
                var RangeDateEnd = '';
                $scope.workLateSum = 0;
                var workLate = 0;
                $scope.workLeaveEarlySum = 0;
                var workLeaveEarly = 0;
                $scope.workAbsenceSum = 0;
                var workAbsence = 0;
                $scope.workNoDataInSum = 0;
                var workNoDataIn = 0;
                $scope.workNoDataOutSum = 0;
                var workNoDataOut = 0;
                $scope.Leave0 = 0;
                $scope.Leave1 = 0;
                $scope.Leave2 = 0;
                $scope.Leave3 = 0;
                $scope.Leave4 = 0;
                $scope.Leave5 = 0;
                $scope.Leave6 = 0;
                $scope.Leave7 = 0;
                $scope.GetDataTime = [];
                $scope.GetDataLeave = [];
                var countPerson = 0;

                // $scope.DateStartShow = $scope.GetTimeToDb(firstDay);
                // $scope.DateEndShow = $scope.GetTimeToDb(lastDay);

                $http.post('./ApiService/GetDepartReport', {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                    .then(function successCallback(response) {
                        $scope.Depart = response.data;

                        if ($scope.Depart.length > 0) {
                            data = {
                                'Date_Cordi': DateStart,
                            };

                            $http.post('./ApiService/GetDepartWorkReportOfDay', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                .then(function successCallback(response) {
                                    $scope.workReport = response.data;
                                    angular.forEach($scope.Depart, function (Per_Arr) {
                                        workLate = 0;
                                        workLeaveEarly = 0;
                                        workAbsence = 0;
                                        workNoDataIn = 0;
                                        workNoDataOut = 0;
                                        countPerson = 0;
                                        $scope.GetDataTime = [];
                                        $scope.GetDataLeave = [];
                                        angular.forEach($scope.workReport, function (Work_Arr, idx) {
                                            if (Per_Arr.Dep_Code == Work_Arr.Dep_Code) {
                                                countPerson++;
                                                if (Work_Arr.A_Status_in2 == 'สาย') {
                                                    // workLate += 1;
                                                    // $scope.workLateSum += 1;
                                                    // $scope.GetDataTime.push({
                                                    //     'PERID': Work_Arr.PERID,
                                                    //     'name': Work_Arr.NAME + ' ' + Work_Arr.SURNAME,
                                                    //     'Day': Work_Arr.A_Datetime,
                                                    //     'Time': Work_Arr.A_Time_in2,
                                                    //     'Status': Work_Arr.A_Status_in2,
                                                    //     'Comment': '',
                                                    //     'Comment_Date': ''
                                                    // });
                                                    var checkDayOff = true;
                                                    if ((new Date(Work_Arr.A_Datetime).toLocaleDateString('en-US', options) != 'Saturday') && (new Date(Work_Arr.A_Datetime).toLocaleDateString('en-US', options) != 'Sunday')) {
                                                        angular.forEach($scope.Holiday, function (dayOff, idx) {
                                                            if (dayOff.Date_stop == Work_Arr.A_Datetime) {
                                                                checkDayOff = false;
                                                            } else {
                                                                if (idx === $scope.Holiday.length - 1 && checkDayOff) {
                                                                    workLate += 1;
                                                                    $scope.workLateSum += 1;
                                                                    $scope.GetDataTime.push({
                                                                        'PERID': Work_Arr.PERID,
                                                                        'name': Work_Arr.NAME + ' ' + Work_Arr.SURNAME,
                                                                        'Day': Work_Arr.A_Datetime,
                                                                        'Time': Work_Arr.A_Time_in2,
                                                                        'Status': Work_Arr.A_Status_in2,
                                                                        'Comment': '',
                                                                        'Comment_Date': ''
                                                                    });
                                                                }
                                                            }
                                                        })

                                                    }
                                                } else if (Work_Arr.A_Status_in2 == 'ขาดงาน') {
                                                    // workAbsence += 1;
                                                    // $scope.workAbsenceSum += 1;
                                                    // $scope.GetDataTime.push({
                                                    //     'PERID': Work_Arr.PERID,
                                                    //     'name': Work_Arr.NAME + ' ' + Work_Arr.SURNAME,
                                                    //     'Day': Work_Arr.A_Datetime,
                                                    //     'Time': Work_Arr.A_Time_in2,
                                                    //     'Status': Work_Arr.A_Status_in2,
                                                    //     'Status2': '',
                                                    //     'Comment': '',
                                                    //     'Comment_Date': ''
                                                    // });
                                                    if ((new Date(Work_Arr.A_Datetime).toLocaleDateString('en-US', options) != 'Saturday') && (new Date(Work_Arr.A_Datetime).toLocaleDateString('en-US', options) != 'Sunday')) {
                                                        angular.forEach($scope.Holiday, function (dayOff, idx) {
                                                            if (dayOff.Date_stop == Work_Arr.A_Datetime) {

                                                            } else {
                                                                if (idx === $scope.Holiday.length - 1) {
                                                                    workAbsence += 1;
                                                                    $scope.workAbsenceSum += 1;
                                                                    $scope.GetDataTime.push({
                                                                        'PERID': Work_Arr.PERID,
                                                                        'name': Work_Arr.NAME + ' ' + Work_Arr.SURNAME,
                                                                        'Day': Work_Arr.A_Datetime,
                                                                        'Time': Work_Arr.A_Time_in2,
                                                                        'Status': Work_Arr.A_Status_in2,
                                                                        'Status2': '',
                                                                        'Comment': '',
                                                                        'Comment_Date': ''
                                                                    });
                                                                }
                                                            }
                                                        })

                                                    }
                                                }
                                            }
                                        })
                                        $scope.ReportSummary.push({
                                            'NAME': Per_Arr.Dep_name,
                                            'GROUP_NAME': Per_Arr.Dep_Group_name,
                                            'DEP_CODE': Per_Arr.Dep_Code,
                                            'workLeaveEarly': workLeaveEarly,
                                            'workLate': workLate,
                                            'workAbsence': workAbsence,
                                            'workNoDataIn': 0,
                                            'workNoDataOut': 0,
                                            'Leave0': 0,
                                            'Leave0DaySum': 0,
                                            'Leave1': 0,
                                            'Leave1DaySum': 0,
                                            'Leave2': 0,
                                            'Leave2DaySum': 0,
                                            'Leave3': 0,
                                            'Leave3DaySum': 0,
                                            'Leave4': 0,
                                            'Leave4DaySum': 0,
                                            'Leave5': 0,
                                            'Leave5DaySum': 0,
                                            'Leave6': 0,
                                            'Leave6DaySum': 0,
                                            'Leave7': 0,
                                            'Leave7DaySum': 0,
                                            'StatusDetails': $scope.GetDataTime,
                                            'LeaveStatusDetails': [],
                                            'CheckComment': false,
                                            'PersonLength': countPerson
                                        });
                                    })

                                    // console.log($scope.ReportSummary);

                                    data = {
                                        'Date_Cordi': DateStart
                                    };

                                    $http.post('./ApiService/GetDepartLeaveReportOfDay', data, {
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            }
                                        })
                                        .then(function successCallback(response) {
                                            $scope.leaveReport = response.data;
                                            angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                angular.forEach($scope.leaveReport, function (Leave_Arr, idx_leave) {
                                                    if (Report_Arr.DEP_CODE == Leave_Arr.Dep_Code) {
                                                        angular.forEach(Report_Arr.StatusDetails, function (Status_Item, sta_idx) {
                                                            if (Status_Item.Day >= Leave_Arr.T_Leave_Date_Start &&
                                                                Status_Item.Day <= Leave_Arr.T_Leave_Date_End) {
                                                                Report_Arr.StatusDetails.splice(sta_idx, 1);
                                                                Report_Arr.workLate -= 1;
                                                            }
                                                        });
                                                        switch (Leave_Arr.T_Leave_Type) {
                                                            case '0':
                                                                Report_Arr.Leave0 += 1;
                                                                $scope.Leave0 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave0DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'ลากิจส่วนตัว',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            case '1':
                                                                Report_Arr.Leave1 += 1;
                                                                $scope.Leave1 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave1DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'ลาป่วย',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            case '2':
                                                                Report_Arr.Leave2 += 1;
                                                                $scope.Leave2 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave2DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'ลาพักผ่อน',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            case '3':
                                                                Report_Arr.Leave3 += 1;
                                                                $scope.Leave3 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave3DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'ลาศึกษาต่อ',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            case '4':
                                                                Report_Arr.Leave4 += 1;
                                                                $scope.Leave4 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave4DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            case '5':
                                                                Report_Arr.Leave5 += 1;
                                                                $scope.Leave5 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave5DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'ลาคลอดบุตร',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            case '6':
                                                                Report_Arr.Leave6 += 1;
                                                                $scope.Leave6 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave6DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            case '7':
                                                                Report_Arr.Leave7 += 1;
                                                                $scope.Leave7 += 1;
                                                                if (Leave_Arr.T_Work_Summary != null) {
                                                                    Report_Arr.Leave7DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                                }
                                                                Report_Arr.LeaveStatusDetails.push({
                                                                    'PERID': Leave_Arr.PERID,
                                                                    'name': Leave_Arr.NAME + ' ' + Leave_Arr.SURNAME,
                                                                    'T_Leave_Type': 'อื่นๆ',
                                                                    'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                    'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                    'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                    'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                    'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                    'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                    'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                                });
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                    }

                                                });
                                            });

                                            data = {
                                                'Date_Cordi': DateStart
                                            };

                                            $http.post('./ApiService/GetCommentDepartReportOfDay', data, {
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    }
                                                })
                                                .then(function successCallback(response) {
                                                    $scope.CommentReport = response.data;
                                                    if ($scope.CommentReport.length > 0) {
                                                        angular.forEach($scope.ReportSummary, function (Report_Arr) {
                                                            angular.forEach(Report_Arr.StatusDetails, function (statusDetail) {
                                                                angular.forEach($scope.CommentReport, function (CommentArr) {
                                                                    if (statusDetail.PERID == CommentArr.PERID) {
                                                                        if (statusDetail.Day == CommentArr.Comment_Date) {
                                                                            statusDetail.Comment = CommentArr.Comment_Details;
                                                                            statusDetail.Comment_Date = CommentArr.Comment_Date;
                                                                            Report_Arr.CheckComment = true;
                                                                        }
                                                                    }
                                                                });
                                                            });
                                                        });
                                                    }
                                                });

                                            angular.forEach($scope.ReportSummary, function (Report_Arr, idx) {
                                                if (Report_Arr.LeaveStatusDetails.length <= 0 && Report_Arr.StatusDetails.length <= 0) {
                                                    // $scope.ReportSummary.splice(idx);
                                                } else {
                                                    $scope.ReportSummary2.push(Report_Arr);
                                                }

                                                // if(idx === $scope.ReportSummary.length - 1){
                                                //     $scope.ReportSummary2 = $scope.ReportSummary;
                                                //     console.log($scope.ReportSummary2);
                                                // }
                                            });

                                            // console.log($scope.ReportSummary);
                                        });
                                    $scope.Loading = false;
                                });

                        }
                    });
            }

            $scope.GetTimeToDb = function (date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1;

                var yyyy = date.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return date = yyyy + '-' + mm + '-' + dd;
            }

            var SetDateToDB = function (DateTpDB) {
                var DateSelect = DateTpDB;
                var Day = DateSelect.split('/')[0];
                var Month = DateSelect.split('/')[1];
                var Year = DateSelect.split('/')[2];
                return Year + "-" + Month + "-" + Day;
            }

            $scope.ShowDetails = function (ObjData) {
                document.getElementById('showModal').click();
                $scope.NAMEDETAIL = ObjData.NAME;
                $scope.StatusDataPerson = ObjData.StatusDetails;
                $scope.LeaveDataPerson = ObjData.LeaveStatusDetails;
            }
        });

        app.filter('NumDeci', function () {
            return function (value) {
                var DateTrans = "";
                if (value != undefined) {
                    var num1 = value.split('.')[0];
                    var num2 = value.split('.')[1];
                    if (num2 == 0) {
                        DateTrans = num1;
                    } else {
                        return value
                    }
                }
                return (
                    DateTrans
                );
            }
        });

        app.filter('DateThai', function () {
            return function (value) {
                var DateTrans = "";
                if (value != undefined) {
                    var Day = value.split('-')[2];
                    var Month = value.split('-')[1];
                    var Year = value.split('-')[0];
                    var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                        'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                    ];
                    DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                        (parseInt(Year) + 543);
                }
                return (
                    DateTrans
                );
            }
        });
    </script>
</body>

</html>