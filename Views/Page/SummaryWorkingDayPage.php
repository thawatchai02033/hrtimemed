<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบ เข้า-ออก งาน</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./tools/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="./tools/bower_components/Ionicons/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="./tools/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="./tools/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="./tools/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="./tools/bower_components/select2/dist/css/select2.min.css">
    <!-- MetisMenu CSS -->
    <link href="./tools/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Page level plugin CSS-->
    <link href="./tools/vendor/datatables2/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./tools/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- CSS Files -->
    <link href="./tools/notiTools/assets/css/material-dashboard.min.css?v=2.1.0" rel="stylesheet"/>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- fullCalendar -->
    <link rel="stylesheet" href="./tools/bower_components/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="./tools/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">

    <!-- Theme style -->
    <link rel="stylesheet" href="./tools/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./tools/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="./tools/sweetalert/dist/sweetalert2.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="./tools/Image/small_psu.gif"/>

    <!-- <script src="./tools/js/angular.min.js"></script> -->
    <!-- <script src="./tools/js/angular-datatables.min.js"></script> -->
    <!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        div.bootstrap-timepicker-widget.dropdown-menu.timepicker-orient-left.timepicker-orient-top.open {
            top: 440px;
            left: 217px;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th,
        table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            width: 1%;
        }

        .table > caption + thead > tr:first-child > td,
        .table > caption + thead > tr:first-child > th,
        .table > colgroup + thead > tr:first-child > td,
        .table > colgroup + thead > tr:first-child > th,
        .table > thead:first-child > tr:first-child > td,
        .table > thead:first-child > tr:first-child > th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        .swal2-popup {
            font-size: 1.6rem !important;
        }

        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            top: 0%;
            background: #FFF;
        }

        .loaderImg {
            position: absolute;
            z-index: 15;
            top: 50%;
            left: 50%;
            margin: -100px 0 0 -150px;
        }

        @media screen and (max-width: 400px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 90%;
            }
        }

        @media screen and (max-width: 500px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 70%;
            }
        }

        @media screen and (max-width: 800px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 400px) and (max-width: 700px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 75%;
            }
        }

        @media screen and (min-width: 768px) and (max-width: 1024px) {
            .loaderImg {
                position: absolute;
                z-index: 15;
                top: 50%;
                left: 60%;
            }
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini" ng-app="myApp">
<div id="wrapper" class="wrapper">

    <?php require './Views/Header.php' ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php require './Views/Menu.php' ?>

    <div class="content-wrapper" ng-controller="myCtrl">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                สรุปวันทำการ
                <small>การปฏิบัติงานประจำเดือน</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> สรุปวันทำการ</a></li>
                <li class="active">สรุปวันทำการ การปฏิบัติงาน</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content" ng-if="CheckPermiss">
            <div class="row">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#sum_work" data-toggle="tab">สรุปวันทำการบุคลากรในหน่วยงาน :
                                    <span style="color: #2C009F;font-weight: bold">ตาราง</span></a>
                            </li>
                            <li><a href="#sum_work_cal" data-toggle="tab">สรุปวันทำการบุคลากรในหน่วยงาน :
                                    <span style="color: #2C009F;font-weight: bold">ปฏิทิน</span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="sum_work">
                                <div class="box-body">
                                    <!-- Date -->
                                    <div class="form-group">
                                        <div class="col-lg-3" style="text-align: center">
                                            <label>หน่วยงาน:</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <select class="form-control select2" style="width: 100%;"
                                                    ng-model="DepartSelect" id="Select_Depart">
                                                <option ng-repeat="depart in Depart"
                                                        ng-selected="DepartSelect == depart.Dep_Code"
                                                        value="{{depart.Dep_Code}}">{{depart.Dep_name}} :
                                                    {{depart.Dep_Group_name}}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3"></div>
                                    </div>
                                    <!-- /.form group -->
                                </div>
                                <div class="box-body">
                                    <!-- Date -->
                                    <div class="form-group">
                                        <div class="col-lg-3" style="text-align: center">
                                            <label>เดือน - ปี :</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <select class="form-control select2" style="width: 100%;"
                                                            ng-model="MonthSelect"
                                                            id="Select_Month">
                                                        <option value="1">มกราคม</option>
                                                        <option value="2">กุมภาพันธ์</option>
                                                        <option value="3">มีนาคม</option>
                                                        <option value="4">เมษายน</option>
                                                        <option value="5">พฤษภาคม</option>
                                                        <option value="6">มิถุนายน</option>
                                                        <option value="7">กรกฎาคม</option>
                                                        <option value="8">สิงหาคม</option>
                                                        <option value="9">กันยายน</option>
                                                        <option value="10">ตุลาคม</option>
                                                        <option value="11">พฤศจิกายน</option>
                                                        <option value="12">ธันวาคม</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="form-control select2" style="width: 100%;"
                                                            ng-model="YearSelect"
                                                            id="Select_Year">
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2020">2020</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-3" align="center">
                                            <button class="btn btn-info" style="font-weight: bold;width: 150px"
                                                    ng-click="update()">เรียกดู
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.form group -->
                                </div>
                                <div align="center" style="margin-top: 25px">
                                    <button class="btn btn-success"
                                            style="font-weight: bold;color: white;cursor: pointer;font-size: 30px"
                                            ng-click="ApproveData()"
                                            ng-if="!CheckApproveCondi">ยืนยันข้อมูล
                                    </button>
                                    <h2 style="color: #2a6496" ng-if="CheckApproveCondi">ยืนยันข้อมูลประจำเดือน
                                        <span style="color: red">{{Date_n_ToShow | DateApprove}}</span> แล้ว</h2>
                                </div>
                                <div class="box-body">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered" datatable="ng"
                                                   dt-options="dtOptions" dt-instance="dtInstance" width="100%"
                                                   cellspacing="0">
                                                <thead>
                                                <tr>
                                                    <th>รหัสบุคลากร</th>
                                                    <th>ชื่อ - สกุล</th>
                                                    <th>ไม่มีข้อมูล</th>
                                                    <th>สาย</th>
                                                    <th>ขาดงาน</th>
                                                    <th>ลาคร่อม</th>
                                                    <th>สรุปยอดวันลา</th>
                                                    <th>Working Hour</th>
                                                    <th>สรุปวันทำการ</th>
                                                    <th>คิดเป็น %</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class="odd gradeX"
                                                    ng-style="{'background-color': CheckDoctor(itemReport.POS_WORK) ? 'antiquewhite': 'white'}"
                                                    ng-repeat="itemReport in ReportSummary"
                                                    ng-click="ShowDetails(itemReport)">
                                                    <td style="color:green;font-weight: bold;text-align: left">
                                                        {{itemReport.PERID}}
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: left">
                                                        {{itemReport.NAME}}
                                                        {{itemReport.SURNAME}}
                                                        <p style="
                                                            color: red;
                                                            font-size: 12px;
                                                            font-weight: bold;
                                                        " ng-if="CheckDoctor(itemReport.POS_WORK)">( อาจารย์แพทย์ )</p>
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.workNoDataIn != '0'">
                                                        {{itemReport.workNoDataIn}}
                                                    </td>
                                                    <td style="color:red;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.workNoDataIn == '0'">

                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.workLate != '0'">
                                                        {{itemReport.workLate}}
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.workLate == '0'">

                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.workAbsence != '0'">
                                                        {{itemReport.workAbsence}}
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.workAbsence == '0'">

                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.LeaveStatusDetails2.length > 0">
                                                        {{itemReport.LeaveStatusDetails2.length}} (ครั้ง)
                                                    </td>
                                                    <td style="color:red;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.LeaveStatusDetails2.length == 0">

                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.LeaveSum != '0'">
                                                        {{itemReport.LeaveSum}} (วัน)
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center"
                                                        ng-if="itemReport.LeaveSum == '0'">
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center;cursor: pointer"
                                                        ng-click="ShowDataWorking_H(itemReport)">
                                                        {{itemReport.Working_H_Summary}}
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center">
                                                        {{itemReport.WorkDaySum}} | {{itemReport.DateOfMonth}}
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center">
                                                        {{itemReport.WorkDaySum_PER | number:2}} %
                                                    </td>
                                                    <td style="color:green;font-weight: bold;text-align: center;align-content: center">
                                                        <input type="checkbox" style="transform: scale(2)"
                                                               ng-if="!itemReport.Approve"
                                                               ng-click="ApproveDayWork(itemReport)"/>
                                                        <p style="color: red;font-size: 13px"
                                                           ng-if="!itemReport.Approve">รอยืนยันข้อมูล</p>
                                                        <p ng-if="itemReport.Approve">
                                                            ยืนยันข้อมูลแล้ว
                                                        </p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                            <!-- /.table-responsive -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="sum_work_cal">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9" align="center" ng-if="Leave_n_ToShow != ''">
                                        <h2 style="color: #2a6496">สรุปผลการปฏิบัติงาน {{Leave_n_ToShow}}</h2>
                                        <h3 style="font-weight: bold">วันทำการ = {{Leave_s_ToShow}} วัน || Working Hour
                                            = {{sumworking}} ช.ม. </h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="box-body" style="border: 2px solid;border-radius: 11px;">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table width="100%" class="table table-bordered" datatable="ng"
                                                           dt-options="dtOptions2" dt-instance="dtInstance" width="100%"
                                                           cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th>ชื่อ - สกุล</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="odd gradeX" style="cursor: pointer;"
                                                            ng-repeat="itemReport in ReportSummary"
                                                            ng-click="ShowDetails(itemReport)">
                                                            <td style="color:green;font-weight: bold;text-align: left"
                                                                ng-click="ShowLeaveCheck(itemReport)">
                                                                {{itemReport.NAME}} {{itemReport.SURNAME}}
                                                                ({{itemReport.PERID}})
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>
                                                    <!-- /.table-responsive -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9" style="border: 2px solid;border-radius: 11px;">
                                        <div ui-calendar="uiConfig.calendar" ng-model="eventSources"
                                             calendar="myCalendar">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>

                    </div>
                </div>
                <!-- /.row -->
        </section>

        <section class="content" ng-if="!CheckPermiss">
            <div style="position: fixed;top: 50%;left: 30%;" align="center">
                <p style="font-size: 50px;font-weight: bold;color: red"> ขออภัย!!! คุณไม่ได้รับสิทธิในการใช้งานเมนู</p>
            </div>
        </section>
        <!-- /.content -->
        <div class="loader" align="center" ng-if="Loading">
            <img class="loaderImg" src="./Image/Preloader_2.gif"/>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <?php require './Views/Footer.php' ?>
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="./tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="./tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables JavaScript -->
<script src="./tools/vendor/datatables2/jquery.dataTables.js"></script>
<script src="./tools/vendor/datatables2/dataTables.bootstrap4.js"></script>
<!-- Demo scripts for this page-->
<script src="./tools/vendor/js/demo/datatables-demo.js"></script>
<!-- SlimScroll -->
<script src="./tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="./tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="./tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="./tools/dist/js/demo.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./tools/vendor/metisMenu/metisMenu.min.js"></script>

<!-- date-range-picker -->
<script src="./tools/bower_components/moment/min/moment.min.js"></script>
<!-- fullCalendar -->
<script src="./tools/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="./tools/bower_components/moment/moment.js"></script>
<script src="./tools/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="./tools/bower_components/fullcalendar/dist/locale/th.js"></script>

<script src="./tools/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="./tools/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="./tools/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="./tools/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="./tools/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="./tools/plugins/input-mask/jquery.inputmask.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="./tools/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- iCheck 1.0.1 -->
<script src="./tools/plugins/iCheck/icheck.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./tools/dist/js/sb-admin-2.js"></script>
<!-- SweetDialog JS-->
<script src="./tools/sweetalert/dist/sweetalert2.min.js"></script>


<script src="./tools/Js/angular.min.js"></script>
<!-- <script src="./tools/js/jquery.dataTables.min.js"></script> -->
<script src="./tools/Js/angular-sanitize.min.js"></script>
<script src="./tools/Js/ui-bootstrap-tpls-2.5.0.min.js"></script>
<script src="./tools/Js/angular-animate.min.js"></script>
<script src="./tools/Js/angular-datatables.min.js"></script>
<script src="./tools/Js/calendar.js"></script>


<!--   Core JS Files   -->
<script src="./tools/notiTools/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>

<script src="./tools/notiTools/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./tools/notiTools/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./tools/notiTools/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<link rel="stylesheet" type="text/css"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>

<!-- Sharrre libray -->
<script src="./tools/notiTools/assets/demo/jquery.sharrre.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        if (<?php echo $this->model->CheckStatus ?>) {
            $.ajax({
                type: "POST",
                url: './ApiService/T_GetCountDataNoteOfLeave',
                contentType: "application/json",
                data: JSON.stringify({
                    DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                }),
                error: function (data) {
                    console.log(data);
                },
                success: function (data) {
                    var ObjResult = JSON.parse(data);
                    if (ObjResult.Status) {
                        if (ObjResult.Message > 0) {
                            md.showNotification('bottom', 'right', ObjResult.Message);
                            $('#showCountLeave').html(ObjResult.Message);
                        } else {
                            $('#showCountLeave').html('');
                        }
                    }
                },
            });
            setInterval(function () {
                $.ajax({
                    type: "POST",
                    url: './ApiService/T_GetCountDataNoteOfLeave',
                    contentType: "application/json",
                    data: JSON.stringify({
                        DepCode: "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>
                    }),
                    error: function (data) {
                        console.log(data);
                    },
                    success: function (data) {
                        var ObjResult = JSON.parse(data);
                        if (ObjResult.Status) {
                            if (ObjResult.Message > 0) {
                                md.showNotification('bottom', 'right', ObjResult.Message);
                                $('#showCountLeave').html(ObjResult.Message);
                            } else {
                                $('#showCountLeave').html('');
                            }
                        }
                    },
                });
            }, 30000);
        }
    });
</script>
<!-- page script -->
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {
            'placeholder': 'dd/mm/yyyy'
        })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {
            'placeholder': 'mm/dd/yyyy'
        })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        })
        //Date range as a button
        $('#daterange-btn').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                    'MMMM D, YYYY'))
            }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: true,
            showMeridian: false,
            minuteStep: 1
        })
    })
</script>
<script>
    var app = angular.module("myApp", ["datatables", 'ui.calendar', "ui.bootstrap", 'ngAnimate']);
    app.controller("myHeader", function ($scope, $http, $window) {
        $scope.LogOut = function () {
            $http.post('./ApiService/LogOut', {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data) {
                        $window.location.href = './Login';
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ออกจากระบบไม่สำเร็จ'
                        });
                    }
                    ;
                });
        }
    });
</script>
<script>
    app.controller("myCtrl", function ($scope, $http, $window, $timeout, $interval, DTOptionsBuilder, $sce, $compile, uiCalendarConfig, $filter, $uibModal) {
        $scope.Depart = <?php echo $this->Department; ?>;
        $scope.Holiday = <?php echo $this->Holiday; ?>;
        $scope.Loading = true;
        $scope.CheckApproveCondi = true;
        $scope.countWorking = 0;
        $scope.PersonDep = [];
        $scope.workingDayData = [];
        $scope.fetch_workingDay = [];
        $scope.daysOfYear = [];
        // $scope.H_Leave = [];
        $scope.More_Leave_Data = [];
        $scope.eventSources = [];
        $scope.WorkingDataPersonal = [];
        $scope.workingHourData = [];
        $scope.CheckApproveData = [];
        $scope.DateStart = '';
        $scope.DateEnd = '';
        $scope.Leave_n_ToShow = '';
        $scope.Date_n_ToShow = '';
        $scope.Leave_s_ToShow = '';
        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250])
        $scope.dtOptions2 = DTOptionsBuilder.newOptions().withOption('lengthMenu', [20, 150, 200, 250])
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        $scope.events = [];
        $scope.CheckPermiss = true;

        $scope.init = function () {
            data = {
                'MENU_CODE': '010',
            };

            $http.post('./ApiService/CheckPermissMenu', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    if (response.data.Status) {
                        $scope.CheckPermiss = true;

                        var DateNow = new Date();
                        var check_Map_Dep = false;
                        $scope.YearSelect = '' + DateNow.getFullYear();
                        $scope.MonthSelect = '' + (DateNow.getMonth() + 1);

                        angular.forEach($scope.Depart, function (item, idx) {
                            if (item.Dep_Code == <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>) {
                                $scope.DepartSelect = "" + <?php echo($PersonalData["DataPerson"]["Dep_Code"]); ?>;
                                check_Map_Dep = true;
                                $scope.update();
                            } else {
                                if (idx == $scope.Depart.length - 1 && !check_Map_Dep) {
                                    $scope.DepartSelect = '';
                                    $scope.Loading = false;
                                }
                            }
                        })
                    } else {
                        $scope.CheckPermiss = false;
                        $scope.Loading = false;
                    }
                });
            // $scope.Loading = false;
        }

        $timeout($scope.init)

        $scope.update = function () {
            $scope.Loading = true;
            var DepartIsNull = angular.element(document.querySelector('#Select_Depart'));

            if (DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != null && DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.DepartSelect = DepartIsNull[0].options[DepartIsNull[0].selectedIndex].value;
            }
            data = {
                'Dep_Code': $scope.DepartSelect
            };
            $http.post('./ApiService/GetPersonFormDep', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.PersonDep = response.data;

                    data = {
                        'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                    };
                    $http.post('./ApiService/GetRefPersonal', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            if (response.data.length > 0) {
                                var Copies = []
                                angular.extend(Copies, $scope.PersonDep)
                                angular.forEach($scope.PersonDep, function (data_I, idx) {
                                    angular.forEach(response.data, function (Ref_I) {
                                            if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == $scope.DepartSelect) {
                                            } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == $scope.DepartSelect) {
                                                if(Copies.length > 0){
                                                    angular.forEach(Copies, function (data_Copy, idx_c) {
                                                        if(data_I.PERID == data_Copy.PERID){
                                                            Copies.splice(idx_c, 1)
                                                        }
                                                    })
                                                }
                                            }
                                    })
                                })
                                $scope.PersonDep = []
                                $scope.PersonDep = Copies

                                angular.forEach($scope.PersonDep, function (perItem) {
                                    perItem.DATEOFYEAR = [];
                                })
                                $scope.GetCalWorkingDayNew();
                            } else {
                                angular.forEach($scope.PersonDep, function (perItem) {
                                    perItem.DATEOFYEAR = [];
                                })
                                $scope.GetCalWorkingDayNew();
                            }
                        });

                    // $scope.CheckApprove();

                    // $scope.CalDortorCheck();
                });
        }

        $scope.CheckDoctor = function (PosWork) {
            if ((parseInt(PosWork)) >= 551 && ((parseInt(PosWork)) <= 558)
                || (parseInt(PosWork) == 800) || (parseInt(PosWork) == 221)
                || (parseInt(PosWork) == 211)|| (parseInt(PosWork) == 212)){
                return true;
            } else {
                return false;
            }
        }

        $scope.CheckDocMaster = function (item, time, status) {
            if ((parseInt(item.POS_WORK)) >= 551 && ((parseInt(item.POS_WORK)) <= 558)
                || (parseInt(item.POS_WORK) == 800) || (parseInt(item.POS_WORK) == 221)
                || (parseInt(item.POS_WORK) == 211)|| (parseInt(item.POS_WORK) == 212)){
                if (time == '') {
                    return 'สแกนบัตร -> ' + status;
                } else {
                    return 'สแกนบัตร -> ID CHECK-IN';
                }
            } else {
                return 'สแกนบัตร -> ' + time + ' ' + status;
            }
        }


        // ฟังก์ชันก์คำนวณการทำงานของแพทย์ออกตรวจประจำเดือน
        $scope.CalDortorCheck = function () {

            var monthChoose = angular.element(document.querySelector('#Select_Month'));
            var yearChoose = angular.element(document.querySelector('#Select_Year'));

            if (monthChoose[0].options[monthChoose[0].selectedIndex].value != null && monthChoose[0].options[monthChoose[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.MonthSelect = monthChoose[0].options[monthChoose[0].selectedIndex].value;
            }

            if (yearChoose[0].options[yearChoose[0].selectedIndex].value != null && yearChoose[0].options[yearChoose[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.YearSelect = yearChoose[0].options[yearChoose[0].selectedIndex].value;
            }

            if ($scope.MonthSelect < 10) {
                monthSelect = '0' + $scope.MonthSelect;
            } else {
                monthSelect = $scope.MonthSelect;
            }

            var date = new Date($scope.YearSelect + "-" + monthSelect + "-" + "01");
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var firstDay2 = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            var options = {
                weekday: 'long'
            };

            data = {
                'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                'Date_Cordi': $scope.GetTimeToDb(firstDay),
                'Date_Cordi2': $scope.GetTimeToDb(lastDay),
            };

            $http.post('./ApiService/GetDocTorSummary', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    var DataDoc = response.data;

                    if (DataDoc.length > 0) {
                        var firstDay2 = new Date(new Date(date.getFullYear(), date.getMonth(), 1));
                        var lastDay = new Date(new Date(date.getFullYear(), date.getMonth() + 1, 0));
                        var daysOfYear = [];
                        var DortorWorkingDay = [];
                        var options = {
                            weekday: 'long'
                        };

                        for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {

                            angular.forEach(DataDoc, function (DocTor_Arr) {
                                if ($scope.GetTimeToDb(d) == DocTor_Arr.day_of_month) {
                                    if (DortorWorkingDay.length == 0) {
                                        DortorWorkingDay.push({
                                            'PERID': DocTor_Arr.perid,
                                            'WORKINGDAY': 1
                                        })
                                    } else {
                                        var CheckMap = false;
                                        angular.forEach(DortorWorkingDay, function (Dor_item, idx) {
                                            if (Dor_item.PERID == DocTor_Arr.perid) {
                                                Dor_item.WORKINGDAY += 1;
                                                CheckMap = true;
                                            } else {
                                                if (idx === DortorWorkingDay.length - 1 && !CheckMap) {
                                                    DortorWorkingDay.push({
                                                        'PERID': DocTor_Arr.perid,
                                                        'WORKINGDAY': 1
                                                    })
                                                }
                                            }
                                        })
                                    }
                                }
                            });
                        }
                    }
                });
        }

        $scope.GetTimeToDb = function (date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1;

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return date = yyyy + '-' + mm + '-' + dd;
        }

        $scope.calArrayDate = function (fday, lday) {
            var firstDay2 = new Date(fday);
            var lastDay = new Date(lday);
            var daysOfYear = [];
            var options = {
                weekday: 'long'
            };
            for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                daysOfYear.push({
                    'DAY': $scope.GetTimeToDb(d),
                    'D_1Digit': d.getDate(),
                    'DAY_NAME': d.toLocaleDateString('en-US', options),
                    'STATUS': '-'
                });
            }
            return daysOfYear;
        }

        $scope.CheckApprove = function () {

            var monthForCheck = '';
            if ($scope.MonthSelect < 10) {
                monthForCheck = '0' + $scope.MonthSelect;
            } else {
                monthForCheck = $scope.MonthSelect;
            }

            data = {
                'Dep_Code': $scope.DepartSelect,
                'Date_Cordi': $scope.YearSelect + "-" + monthForCheck + "-" + "01"
            };
            $http.post('./ApiService/GetApproveDataSummary', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    if (response.data.length > 0) {
                        if ($scope.ReportSummary.length > 0) {
                            angular.forEach($scope.ReportSummary, function (report_item) {
                                angular.forEach(response.data, function (CheckData_item) {
                                    if (report_item.PERID == CheckData_item.perid) {
                                        report_item.Approve = true;
                                    }
                                })
                            })
                        }

                        $scope.CheckApproveData = response.data;
                        $scope.CheckApproveCondi = false;
                        // $scope.Date_n_ToShow = $scope.YearSelect + "-" + monthForCheck + "-" + "01";
                    } else {
                        $scope.CheckApproveCondi = false;
                    }
                    $scope.Loading = false;
                });
        }

        $scope.changeWorkDay = function (item_Change) {
            if (document.getElementById(item_Change.PERID).value != '') {
                if (parseFloat(document.getElementById(item_Change.PERID).value)) {
                    if (parseFloat(document.getElementById(item_Change.PERID).value) <= item_Change.DateOfMonth) {
                        angular.forEach($scope.ReportSummary, function (item) {
                            if (item.PERID == item_Change.PERID) {
                                item.WorkDaySum = parseFloat(document.getElementById(item_Change.PERID).value);
                                item.WorkDaySum_PER = ((parseFloat(item.WorkDaySum) / parseFloat(item.DateOfMonth)) * 100);
                            }
                        })
                    } else {
                        Swal({
                            type: 'error',
                            title: 'พบข้อผิดพลาด ไม่สามารถป้อนตัวเลขเกินจำนวนวันของเดือนได้'
                        });
                        document.getElementById(item_Change.PERID).value = item_Change.DateOfMonth;
                        angular.forEach($scope.ReportSummary, function (item) {
                            if (item.PERID == item_Change.PERID) {
                                item.WorkDaySum = parseFloat(document.getElementById(item_Change.PERID).value);
                                item.WorkDaySum_PER = ((parseFloat(item.WorkDaySum) / parseFloat(item.DateOfMonth)) * 100);
                            }
                        })
                    }
                } else {
                    Swal({
                        type: 'error',
                        title: 'พบข้อผิดพลาดโปรดตรวจสอบการกรอกข้อมูล'
                    });
                    document.getElementById(item_Change.PERID).value = 0;
                    angular.forEach($scope.ReportSummary, function (item) {
                        if (item.PERID == item_Change.PERID) {
                            item.WorkDaySum = parseFloat(document.getElementById(item_Change.PERID).value);
                            item.WorkDaySum_PER = ((parseFloat(item.WorkDaySum) / parseFloat(item.DateOfMonth)) * 100);
                        }
                    })
                }
            } else {
                Swal({
                    type: 'error',
                    title: 'พบข้อผิดพลาดโปรดตรวจสอบการกรอกข้อมูล'
                });
                document.getElementById(item_Change.PERID).value = 0;
                angular.forEach($scope.ReportSummary, function (item) {
                    if (item.PERID == item_Change.PERID) {
                        item.WorkDaySum = parseFloat(document.getElementById(item_Change.PERID).value);
                        item.WorkDaySum_PER = ((parseFloat(item.WorkDaySum) / parseFloat(item.DateOfMonth)) * 100);
                    }
                })
            }
        }

        $scope.ApproveDayWork = function (item) {
            angular.forEach($scope.ReportSummary, function (Report_I) {
                if (Report_I.PERID == item.PERID) {
                    if (Report_I.CheckApprove) {
                        Report_I.CheckApprove = false;
                    } else {
                        Report_I.CheckApprove = true;
                    }
                }
            })
        }

        $scope.ApproveData = function () {
            var CheckemptyData = false;
            angular.forEach($scope.ReportSummary, function (Report_I) {
                if (Report_I.CheckApprove) {
                    CheckemptyData = true;
                }
            })
            if (CheckemptyData) {
                var CheckCondiDate = new Date();
                // if (CheckCondiDate.getDate() <= 7 && (parseInt($scope.MonthSelect) == CheckCondiDate.getMonth()) && parseInt($scope.YearSelect) == CheckCondiDate.getFullYear()) {
                if ((parseInt($scope.MonthSelect) < (CheckCondiDate.getMonth() + 1)) && parseInt($scope.YearSelect) == CheckCondiDate.getFullYear()) {
                    if ($scope.PersonDep.length > 0) {
                        var Month = ';'
                        switch (parseInt(monthSelect)) {
                            case 1 :
                                Month = 'มกราคม';
                                break;
                            case 2 :
                                Month = 'กุมภาพันธ์';
                                break;
                            case 3 :
                                Month = 'มีนาคม';
                                break;
                            case 4 :
                                Month = 'เมษายน';
                                break;
                            case 5 :
                                Month = 'พฤษภาคม';
                                break;
                            case 6 :
                                Month = 'มิถุนายน';
                                break;
                            case 7 :
                                Month = 'กรกฎาคม';
                                break;
                            case 8 :
                                Month = 'สิงหาคม';
                                break;
                            case 9 :
                                Month = 'กันยายน';
                                break;
                            case 10 :
                                Month = 'ตุลาคม';
                                break;
                            case 11 :
                                Month = 'พฤษจิกายน';
                                break;
                            case 12 :
                                Month = 'ธันวาคม';
                                break;
                        }

                        Swal({
                            title: 'ยืนยันการทำรายการ',
                            text: "ต้องการยืนยันข้อมูลการปฏิบัติงานประจำเดือน " + Month + " ใช่หรือไม่ ?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'ยกเลิก',
                            confirmButtonText: 'ตกลง'
                        }).then((result) => {
                            if (result.value) {
                                data = {
                                    'Report_Data': $scope.ReportSummary,
                                    'Dep_Code': $scope.DepartSelect
                                };
                                $http.post('./ApiService/SubmitReportSummary', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                    .then(function successCallback(response) {
                                        var result = response.data;
                                        if (result.Status) {
                                            Swal({
                                                type: 'success',
                                                title: result.Message
                                            });
                                            $scope.update();
                                        } else {
                                            Swal({
                                                type: 'error',
                                                title: result.Message
                                            });
                                        }
                                    });
                            }
                        });
                    } else {
                        Swal({
                            type: 'error',
                            title: 'ไม่สามารถยืนยันข้อมูลได้ เนื่องจากไม่พบข้อมูลบุคลากรในหน่วยงาน'
                        });
                    }
                } else {
                    Swal({
                        type: 'error',
                        title: 'ไม่สามารถยืนยันข้อมูลได้ เนื่องจากเดือนดังกล่าวยังไม่ถึงกำหนดในการยืนยันข้อมูล'
                    });
                }
                // } else {
                //     Swal({
                //         type: 'error',
                //         title: 'ไม่สามารถยืนยันข้อมูลได้ เนื่องจากเกินวันเวลาที่กำหนดในการยืนยันข้อมูล'
                //     });
                // }
            } else {
                Swal({
                    type: 'error',
                    title: 'ไม่สามารถยืนยันข้อมูลได้ กรุณาเลือกบุคลากรที่ต้องการจะยืนยัน'
                });
            }

        }

        $scope.GetCalWorkingDayNew = function () {

            $scope.daysOfYear = [];
            $scope.More_Leave_Data = [];
            $scope.countWorking = 0;

            var monthChoose = angular.element(document.querySelector('#Select_Month'));
            var yearChoose = angular.element(document.querySelector('#Select_Year'));

            if (monthChoose[0].options[monthChoose[0].selectedIndex].value != null && monthChoose[0].options[monthChoose[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.MonthSelect = monthChoose[0].options[monthChoose[0].selectedIndex].value;
            }

            if (yearChoose[0].options[yearChoose[0].selectedIndex].value != null && yearChoose[0].options[yearChoose[0].selectedIndex].value != '? undefined:undefined ?') {
                $scope.YearSelect = yearChoose[0].options[yearChoose[0].selectedIndex].value;
            }

            if ($scope.MonthSelect < 10) {
                monthSelect = '0' + $scope.MonthSelect;
            } else {
                monthSelect = $scope.MonthSelect;
            }

            var date = new Date($scope.YearSelect + "-" + monthSelect + "-" + "01");
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var firstDay2 = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            var options = {
                weekday: 'long'
            };

            angular.forEach($scope.PersonDep, function (perItem) {
                firstDay2 = new Date(date.getFullYear(), date.getMonth(), 1);
                lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {

                    var checkHoliday = false;
                    $scope.countWorking++;

                    angular.forEach($scope.Holiday, function (dayOff, idx) {
                        if (dayOff.Date_stop == $scope.GetTimeToDb(d)) {
                            perItem.DATEOFYEAR.push({
                                'DAY': $scope.GetTimeToDb(d),
                                'DAY_NAME': d.toLocaleDateString('en-US', options),
                                'STATUS': '-',
                                'Holiday': dayOff,
                                'Holiday_Status': true,
                                'Leave_Status': false,
                                'leave_Detail': null,
                                'OPD': false,
                                'OPD_COUNT': 0,
                                'LeaveCondi': false
                            })
                            checkHoliday = true;
                        }
                    })

                    if (!checkHoliday) {
                        perItem.DATEOFYEAR.push({
                            'DAY': $scope.GetTimeToDb(d),
                            'DAY_NAME': d.toLocaleDateString('en-US', options),
                            'STATUS': '-',
                            'Holiday': [],
                            'Holiday_Status': false,
                            'Leave_Status': false,
                            'leave_Detail': null,
                            'OPD': false,
                            'OPD_COUNT': 0,
                            'LeaveCondi': false
                        })
                    }
                }
            })

            var DateStart = $scope.GetTimeToDb(firstDay);
            var DateEnd = $scope.GetTimeToDb(lastDay);

            $scope.DateStart = $scope.GetTimeToDb(firstDay);
            $scope.DateEnd = $scope.GetTimeToDb(lastDay);

            $scope.uiConfig.calendar.defaultDate = DateStart;

            data = {
                'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                'Date_Cordi': DateStart,
                'Date_Cordi2': DateEnd,
            };

            $http.post('./ApiService/GetDocTorSummary', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {

                    if (response.data.length > 0) {
                        angular.forEach($scope.PersonDep, function (Person_I) {
                            angular.forEach(response.data, function (DocTor_Arr) {
                                if (Person_I.PERID == DocTor_Arr.perid) {
                                    angular.forEach(Person_I.DATEOFYEAR, function (Date_I) {
                                        if (Date_I.DAY == DocTor_Arr.day_of_month) {
                                            // angular.forEach($scope.PersonDep, function (Person_I) {
                                            if (Person_I.PERID == DocTor_Arr.perid) {
                                                Date_I.OPD = true;
                                                Date_I.OPD_COUNT = DocTor_Arr.count;
                                            }
                                            // });
                                        }
                                    });
                                }
                            });
                        });
                    }

                    data = {
                        'Dep_Code': $scope.DepartSelect,
                        'Date_Start': DateStart,
                        'Date_End': DateEnd
                    };

                    $http.post('./ApiService/GetDataWorking', data, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                        .then(function successCallback(response) {
                            $scope.fetch_workingDay = response.data;

                            angular.forEach($scope.PersonDep, function (Person_I) {
                                $scope.H_Leave = [];
                                $scope.P_Leave = [];
                                $scope.C_Leave = [];
                                var checkCordi = true;

                                angular.forEach($scope.fetch_workingDay[0].LeaveData, function (Leave_I, idx) {
                                    if (Person_I.PERID == Leave_I.PERID) {
                                        if (parseFloat(Leave_I.T_Work_Summary) != 0 && parseFloat(Leave_I.T_Status_Leave) != 0) {
                                            var date1 = new Date(Leave_I.T_Leave_Date_Start);
                                            var date2 = new Date(Leave_I.T_Leave_Date_End);
                                            date1.setHours(0, 0, 0, 0);
                                            date2.setHours(0, 0, 0, 0);

                                            for (var d = date1; d.getTime() <= date2.getTime(); d.setDate(d.getDate() + 1)) {
                                                angular.forEach(Person_I.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                        if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                            // if (Leave_I.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Leave_I.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                            //     Leave_I.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Leave_I.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                            Date_I.Leave_Status = true;
                                                            Date_I.leave_Detail = Leave_I;
                                                            // }

                                                        }
                                                    }
                                                );
                                            }
                                        } else {
                                            var date1 = new Date(Leave_I.T_Leave_Date_Start);
                                            var date2 = new Date(Leave_I.T_Leave_Date_End);
                                            date1.setHours(0, 0, 0, 0);
                                            date2.setHours(0, 0, 0, 0);

                                            for (var d = date1; d.getTime() <= date2.getTime(); d.setDate(d.getDate() + 1)) {
                                                angular.forEach(Person_I.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                        if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                            // if (Leave_I.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Leave_I.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                            //     Leave_I.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Leave_I.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                            Date_I.Leave_Status = false;
                                                            Date_I.leave_Detail = Leave_I;
                                                            // }
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                    }
                                })

                                angular.forEach($scope.fetch_workingDay[0].LeaveData2, function (Leave_I, idx) {
                                    if (Person_I.PERID == Leave_I.PERID) {
                                        if (parseFloat(Leave_I.T_Work_Summary) != 0 && parseFloat(Leave_I.T_Status_Leave) != 0) {
                                            var date1 = new Date(Leave_I.T_Leave_Date_Start);
                                            var date2 = new Date(Leave_I.T_Leave_Date_End);
                                            date1.setHours(0, 0, 0, 0);
                                            date2.setHours(0, 0, 0, 0);

                                            for (var d = date1; d.getTime() <= date2.getTime(); d.setDate(d.getDate() + 1)) {
                                                angular.forEach(Person_I.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                        if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                            Date_I.Leave_Status = true;
                                                            Date_I.leave_Detail = Leave_I;
                                                        }
                                                    }
                                                );
                                            }
                                        } else {
                                            var date1 = new Date(Leave_I.T_Leave_Date_Start);
                                            var date2 = new Date(Leave_I.T_Leave_Date_End);
                                            date1.setHours(0, 0, 0, 0);
                                            date2.setHours(0, 0, 0, 0);

                                            for (var d = date1; d.getTime() <= date2.getTime(); d.setDate(d.getDate() + 1)) {
                                                angular.forEach(Person_I.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                        if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                            Date_I.Leave_Status = false;
                                                            Date_I.leave_Detail = Leave_I;
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                    }
                                })

                                var LeaveData = [];
                                var LeaveData2 = [];

                                angular.forEach(Person_I.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                    checkCordi = false;
                                    if (Date_I.Leave_Status) {
                                        if (LeaveData.length > 0) {
                                            angular.forEach(LeaveData, function (LeaveItem, idx_Ltm) {
                                                if (LeaveItem.No_Id == Date_I.leave_Detail.No_Id) {
                                                    checkCordi = true;
                                                } else {
                                                    if (!checkCordi && (idx_Ltm == LeaveData.length - 1)) {
                                                        // เพิ่มเงื่อนไขการนับวัน ส อ กรณีมี OPD ห้ามคิดลา ค่อม
                                                        //***---------------------------------------------***
                                                        if ((parseInt(Person_I.POS_WORK)) >= 551 && ((parseInt(Person_I.POS_WORK)) <= 558)
                                                            || (parseInt(Person_I.POS_WORK) == 800) || (parseInt(Person_I.POS_WORK) == 221)
                                                            || (parseInt(Person_I.POS_WORK) == 211)|| (parseInt(Person_I.POS_WORK) == 212)){
                                                            // เพิ่มเมื่อ 03/04/63 แก้ไขการคิดลาคร่อมอาจารย์ ผิดกรณี ส อ ลา ศ จ แต่ ส มา OPD
                                                            /////////////////////////////////////////////////////////////

                                                            if (LeaveData.length > 0) {
                                                                var CheckOPD = false
                                                                angular.forEach(LeaveData, function (leave_his) {
                                                                    angular.forEach(Person_I.DATEOFYEAR, function (Date_Check, idx_Dfy) {
                                                                        if (Date_Check.DAY > leave_his.T_Leave_Date_End &&
                                                                            Date_Check.DAY < Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                            if (Date_Check.DAY_NAME == 'Saturday' || Date_Check.DAY_NAME == 'Sunday') {
                                                                                if (Date_Check.OPD) {
                                                                                    CheckOPD = true
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                });

                                                                var DateNow = new Date(LeaveData[0].T_Leave_Date_End)
                                                                DateNow.setDate(DateNow.getDate() + 1)

                                                                if ($scope.GetTimeToDb(DateNow) != Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                    if (!CheckOPD) {
                                                                        if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                            Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                            LeaveData2.push(Date_I.leave_Detail)
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            /////////////////////////////////////////////////////////////


                                                            /*if (Person_I.DATEOFYEAR[idx_Dfy - 1].DAY_NAME == 'Saturday' || Person_I.DATEOFYEAR[idx_Dfy - 1].DAY_NAME == 'Sunday') {
                                                                if (!Person_I.DATEOFYEAR[idx_Dfy - 1].OPD) {
                                                                    if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                        Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                        LeaveData2.push(Date_I.leave_Detail)
                                                                    }
                                                                }
                                                            } else {
                                                                if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                    Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                    LeaveData2.push(Date_I.leave_Detail)
                                                                }
                                                            }*/
                                                        } else {
                                                            var DateNow = new Date(LeaveData[0].T_Leave_Date_End)
                                                            DateNow.setDate(DateNow.getDate() + 1)
                                                            if ($scope.GetTimeToDb(DateNow) != Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                    Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                    LeaveData2.push(Date_I.leave_Detail)
                                                                }
                                                            }
                                                        }

                                                        //***---------------------------------------------***
                                                    }
                                                }
                                            });
                                        } else {
                                            if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                LeaveData.push(Date_I.leave_Detail)
                                            }
                                        }
                                    } else {
                                        if (Date_I.leave_Detail != null) {
                                            if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                //*************************************
                                                // วันลา = false และ ประเภทการลา ไม่เท่ากับไปราชการ ให้เพักวันทำงาน
                                                //!*************************************
                                                if (parseInt(Date_I.leave_Detail.T_Leave_Type) != 7) {
                                                    if (LeaveData.length > 0) {
                                                        angular.forEach(LeaveData, function (LeaveItem, idx_Ltm) {
                                                            if (LeaveItem.No_Id == Date_I.leave_Detail.No_Id) {
                                                                checkCordi = true;
                                                            } else {
                                                                if (!checkCordi && (idx_Ltm == LeaveData.length - 1)) {
                                                                    // เพิ่มเงื่อนไขการนับวัน ส อ กรณีมี OPD ห้ามคิดลา ค่อม
                                                                    //***---------------------------------------------***
                                                                    if ((parseInt(Person_I.POS_WORK)) >= 551 && ((parseInt(Person_I.POS_WORK)) <= 558)
                                                                        || (parseInt(Person_I.POS_WORK) == 800) || (parseInt(Person_I.POS_WORK) == 221)
                                                                        || (parseInt(Person_I.POS_WORK) == 211)|| (parseInt(Person_I.POS_WORK) == 212)){

                                                                        // เพิ่มเมื่อ 03/04/63 แก้ไขการคิดลาคร่อมอาจารย์ ผิดกรณี ส อ ลา ศ จ แต่ ส มา OPD
                                                                        /////////////////////////////////////////////////////////////

                                                                        if (LeaveData.length > 0) {
                                                                            var CheckOPD = false
                                                                            angular.forEach(LeaveData, function (leave_his) {
                                                                                angular.forEach(Person_I.DATEOFYEAR, function (Date_Check, idx_Dfy) {
                                                                                    if (Date_Check.DAY > leave_his.T_Leave_Date_End &&
                                                                                        Date_Check.DAY < Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                                        if (Date_Check.DAY_NAME == 'Saturday' || Date_Check.DAY_NAME == 'Sunday') {
                                                                                            if (Date_Check.OPD) {
                                                                                                CheckOPD = true
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                            });

                                                                            var DateNow = new Date(LeaveData[0].T_Leave_Date_End)
                                                                            DateNow.setDate(DateNow.getDate() + 1)

                                                                            if ($scope.GetTimeToDb(DateNow) != Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                                if (!CheckOPD) {
                                                                                    if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                        Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                        LeaveData2.push(Date_I.leave_Detail)
                                                                                    }
                                                                                }
                                                                            }

                                                                        }

                                                                        /////////////////////////////////////////////////////////////

                                                                        /* ตัว เก่า คอมเม้นเมื่อ 03/04/63*/

                                                                        /*if (Person_I.DATEOFYEAR[idx_Dfy - 1].DAY_NAME == 'Saturday' || Person_I.DATEOFYEAR[idx_Dfy - 1].DAY_NAME == 'Sunday') {
                                                                            if (!Person_I.DATEOFYEAR[idx_Dfy - 1].OPD) {
                                                                                if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                    Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                    LeaveData2.push(Date_I.leave_Detail)
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                LeaveData2.push(Date_I.leave_Detail)
                                                                            }
                                                                        }*/
                                                                    } else {
                                                                        var DateNow = new Date(LeaveData[0].T_Leave_Date_End)
                                                                        DateNow.setDate(DateNow.getDate() + 1)
                                                                        if ($scope.GetTimeToDb(DateNow) != Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                            if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                LeaveData2.push(Date_I.leave_Detail)
                                                                            }
                                                                        }
                                                                    }

                                                                    //***---------------------------------------------***
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                            Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                            LeaveData.push(Date_I.leave_Detail)
                                                        }
                                                    }
                                                    //!*************************************
                                                    // วันลา = false และ ประเภทการลา เท่ากับไปราชการ ให้เพักวันทำงาน
                                                    //!*************************************
                                                } else if (parseInt(Date_I.leave_Detail.T_Work_Summary) > 0) {
                                                    if (LeaveData.length > 0) {
                                                        angular.forEach(LeaveData, function (LeaveItem, idx_Ltm) {
                                                            if (LeaveItem.No_Id == Date_I.leave_Detail.No_Id) {
                                                                checkCordi = true;
                                                            } else {
                                                                if (!checkCordi && (idx_Ltm == LeaveData.length - 1)) {
                                                                    // เพิ่มเงื่อนไขการนับวัน ส อ กรณีมี OPD ห้ามคิดลา ค่อม
                                                                    //***---------------------------------------------***
                                                                    if ((parseInt(Person_I.POS_WORK)) >= 551 && ((parseInt(Person_I.POS_WORK)) <= 558)
                                                                        || (parseInt(Person_I.POS_WORK) == 800) || (parseInt(Person_I.POS_WORK) == 221)
                                                                        || (parseInt(Person_I.POS_WORK) == 211)|| (parseInt(Person_I.POS_WORK) == 212)){

                                                                        // เพิ่มเมื่อ 03/04/63 แก้ไขการคิดลาคร่อมอาจารย์ ผิดกรณี ส อ ลา ศ จ แต่ ส มา OPD
                                                                        /////////////////////////////////////////////////////////////
                                                                        if (LeaveData.length > 0) {
                                                                            var CheckOPD = false
                                                                            angular.forEach(LeaveData, function (leave_his) {
                                                                                angular.forEach(Person_I.DATEOFYEAR, function (Date_Check, idx_Dfy) {
                                                                                    if (Date_Check.DAY > leave_his.T_Leave_Date_End &&
                                                                                        Date_Check.DAY < Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                                        if (Date_Check.DAY_NAME == 'Saturday' || Date_Check.DAY_NAME == 'Sunday') {
                                                                                            if (Date_Check.OPD) {
                                                                                                CheckOPD = true
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                            });

                                                                            var DateNow = new Date(LeaveData[0].T_Leave_Date_End)
                                                                            DateNow.setDate(DateNow.getDate() + 1)
                                                                            if ($scope.GetTimeToDb(DateNow) != Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                                if (!CheckOPD) {
                                                                                    if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                        Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                        LeaveData2.push(Date_I.leave_Detail)
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        /////////////////////////////////////////////////////////////

                                                                        /*if (Person_I.DATEOFYEAR[idx_Dfy - 1].DAY_NAME == 'Saturday' || Person_I.DATEOFYEAR[idx_Dfy - 1].DAY_NAME == 'Sunday') {
                                                                            if (!Person_I.DATEOFYEAR[idx_Dfy - 1].OPD) {
                                                                                if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                    Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                    LeaveData2.push(Date_I.leave_Detail)
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                LeaveData2.push(Date_I.leave_Detail)
                                                                            }
                                                                        }*/
                                                                    } else {
                                                                        var DateNow = new Date(LeaveData[0].T_Leave_Date_End)
                                                                        DateNow.setDate(DateNow.getDate() + 1)
                                                                        if ($scope.GetTimeToDb(DateNow) != Date_I.leave_Detail.T_Leave_Date_Start) {
                                                                            if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                                                Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                                                LeaveData2.push(Date_I.leave_Detail)
                                                                            }
                                                                        }
                                                                    }

                                                                    //***---------------------------------------------***
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                                            Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                                            LeaveData.push(Date_I.leave_Detail)
                                                        }
                                                    }
                                                    //!*************************************
                                                    // วันลา = false และ ประเภทการลา เท่ากับไปราชการ ให้เพักวันทำงาน
                                                    //!*************************************
                                                } else {
                                                    if (LeaveData.length > 0) {
                                                        if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                        } else if (Date_I.Holiday_Status) {
                                                        } else {
                                                            if (LeaveData2.length > 0) {
                                                                LeaveData.push(LeaveData2[LeaveData2.length - 1])

                                                                $scope.H_Leave.push({
                                                                    'Count': 0,
                                                                    'WorkingData_S': parseFloat(LeaveData[0].T_Work_Summary) + parseFloat(LeaveData[1].T_Work_Summary),
                                                                    'MoreWorkingData': (parseFloat(LeaveData[0].T_Work_Summary) + parseFloat(LeaveData[1].T_Work_Summary)),
                                                                    'Data': {
                                                                        'leave1': LeaveData[0],
                                                                        'leave2': LeaveData[1]
                                                                    }
                                                                })

                                                                angular.forEach($scope.H_Leave, function (H_item) {
                                                                    var i = 0;
                                                                    if (H_item.Data.leave1.T_Leave_Date_Start < Person_I.DATEOFYEAR[0].DAY) {
                                                                        var date1 = new Date(Person_I.DATEOFYEAR[0].DAY);
                                                                        var date2 = new Date(H_item.Data.leave2.T_Leave_Date_End);
                                                                    } else if (H_item.Data.leave2.T_Leave_Date_End > Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY) {
                                                                        var date1 = new Date(H_item.Data.leave1.T_Leave_Date_Start);
                                                                        var date2 = new Date(Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY);
                                                                    } else if (H_item.Data.leave1.T_Leave_Date_Start < Person_I.DATEOFYEAR[0].DAY && H_item.Data.leave2.T_Leave_Date_End > Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY) {
                                                                        var date1 = new Date(Person_I.DATEOFYEAR[0].DAY);
                                                                        var date2 = new Date(Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY);
                                                                    } else {
                                                                        var date1 = new Date(H_item.Data.leave1.T_Leave_Date_Start);
                                                                        var date2 = new Date(H_item.Data.leave2.T_Leave_Date_End);
                                                                    }

                                                                    if (date1.getTime() == date2.getTime()) {
                                                                        i++;
                                                                        if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                            i -= 0.5;
                                                                        } else if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                            i -= 0.5;
                                                                        }
                                                                        H_item.Count = i;
                                                                    } else {
                                                                        for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                                            i++;
                                                                            if ($scope.GetTimeToDb(d) == $scope.GetTimeToDb(date2)) {
                                                                                if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                                    i -= 0.5;
                                                                                } else if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                                    i -= 0.5;
                                                                                }
                                                                                if (H_item.Data.leave2.T_Day_Type_End == 'ครึ่งวัน (เช้า)') {
                                                                                    i -= 0.5;
                                                                                } else if (H_item.Data.leave2.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {
                                                                                    i -= 0.5;
                                                                                }
                                                                                H_item.Count = i;
                                                                            }
                                                                        }
                                                                    }
                                                                });

                                                                $scope.More_Leave_Data.push({
                                                                    'PERID': Person_I.PERID,
                                                                    'NAME': Person_I.NAME,
                                                                    'SURNAME': Person_I.SURNAME,
                                                                    'L_DATA': $scope.H_Leave
                                                                })
                                                            }
                                                            LeaveData = [];
                                                            LeaveData2 = [];
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (LeaveData.length > 0) {
                                                    if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                    } else if (Date_I.Holiday_Status) {
                                                    } else {
                                                        if (LeaveData2.length > 0) {
                                                            LeaveData.push(LeaveData2[LeaveData2.length - 1])

                                                            $scope.H_Leave.push({
                                                                'Count': 0,
                                                                'WorkingData_S': parseFloat(LeaveData[0].T_Work_Summary) + parseFloat(LeaveData[1].T_Work_Summary),
                                                                'MoreWorkingData': (parseFloat(LeaveData[0].T_Work_Summary) + parseFloat(LeaveData[1].T_Work_Summary)),
                                                                'Data': {
                                                                    'leave1': LeaveData[0],
                                                                    'leave2': LeaveData[1]
                                                                }
                                                            })

                                                            angular.forEach($scope.H_Leave, function (H_item) {
                                                                var i = 0;
                                                                if (H_item.Data.leave1.T_Leave_Date_Start < Person_I.DATEOFYEAR[0].DAY) {
                                                                    var date1 = new Date(Person_I.DATEOFYEAR[0].DAY);
                                                                    var date2 = new Date(H_item.Data.leave2.T_Leave_Date_End);
                                                                } else if (H_item.Data.leave2.T_Leave_Date_End > Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY) {
                                                                    var date1 = new Date(H_item.Data.leave1.T_Leave_Date_Start);
                                                                    var date2 = new Date(Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY);
                                                                } else if (H_item.Data.leave1.T_Leave_Date_Start < Person_I.DATEOFYEAR[0].DAY && H_item.Data.leave2.T_Leave_Date_End > Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY) {
                                                                    var date1 = new Date(Person_I.DATEOFYEAR[0].DAY);
                                                                    var date2 = new Date(Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY);
                                                                } else {
                                                                    var date1 = new Date(H_item.Data.leave1.T_Leave_Date_Start);
                                                                    var date2 = new Date(H_item.Data.leave2.T_Leave_Date_End);
                                                                }

                                                                if (date1.getTime() == date2.getTime()) {
                                                                    i++;
                                                                    if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                        i -= 0.5;
                                                                    } else if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                        i -= 0.5;
                                                                    }
                                                                    H_item.Count = i;
                                                                } else {

                                                                    for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                                        i++;
                                                                        if ($scope.GetTimeToDb(d) == $scope.GetTimeToDb(date2)) {
                                                                            if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                                i -= 0.5;
                                                                            } else if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                                i -= 0.5;
                                                                            }
                                                                            if (H_item.Data.leave2.T_Day_Type_End == 'ครึ่งวัน (เช้า)') {
                                                                                i -= 0.5;
                                                                            } else if (H_item.Data.leave2.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {
                                                                                i -= 0.5;
                                                                            }
                                                                            H_item.Count = i;
                                                                        }
                                                                    }
                                                                }
                                                            });

                                                            $scope.More_Leave_Data.push({
                                                                'PERID': Person_I.PERID,
                                                                'NAME': Person_I.NAME,
                                                                'SURNAME': Person_I.SURNAME,
                                                                'L_DATA': $scope.H_Leave
                                                            })
                                                        }
                                                        LeaveData = [];
                                                        LeaveData2 = [];
                                                    }
                                                }
                                            }
                                        } else {
                                            if (LeaveData.length > 0) {

                                                if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                } else if (Date_I.Holiday_Status) {
                                                } else {
                                                    if (LeaveData2.length > 0) {
                                                        LeaveData.push(LeaveData2[LeaveData2.length - 1])

                                                        $scope.H_Leave.push({
                                                            'Count': 0,
                                                            'WorkingData_S': parseFloat(LeaveData[0].T_Work_Summary) + parseFloat(LeaveData[1].T_Work_Summary),
                                                            'MoreWorkingData': (parseFloat(LeaveData[0].T_Work_Summary) + parseFloat(LeaveData[1].T_Work_Summary)),
                                                            'Data': {
                                                                'leave1': LeaveData[0],
                                                                'leave2': LeaveData[1]
                                                            }
                                                        })

                                                        angular.forEach($scope.H_Leave, function (H_item) {
                                                            var i = 0;
                                                            if (H_item.Data.leave1.T_Leave_Date_Start < Person_I.DATEOFYEAR[0].DAY) {
                                                                var date1 = new Date(Person_I.DATEOFYEAR[0].DAY);
                                                                var date2 = new Date(H_item.Data.leave2.T_Leave_Date_End);
                                                            } else if (H_item.Data.leave2.T_Leave_Date_End > Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY) {
                                                                var date1 = new Date(H_item.Data.leave1.T_Leave_Date_Start);
                                                                var date2 = new Date(Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY);
                                                            } else if (H_item.Data.leave1.T_Leave_Date_Start < Person_I.DATEOFYEAR[0].DAY && H_item.Data.leave2.T_Leave_Date_End > Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY) {
                                                                var date1 = new Date(Person_I.DATEOFYEAR[0].DAY);
                                                                var date2 = new Date(Person_I.DATEOFYEAR[Person_I.DATEOFYEAR.length - 1].DAY);
                                                            } else {
                                                                var date1 = new Date(H_item.Data.leave1.T_Leave_Date_Start);
                                                                var date2 = new Date(H_item.Data.leave2.T_Leave_Date_End);
                                                            }

                                                            if (date1.getTime() == date2.getTime()) {
                                                                i++;
                                                                if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                    i -= 0.5;
                                                                } else if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                    i -= 0.5;
                                                                }
                                                                H_item.Count = i;
                                                            } else {

                                                                for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                                    i++;
                                                                    if ($scope.GetTimeToDb(d) == $scope.GetTimeToDb(date2)) {
                                                                        if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                            i -= 0.5;
                                                                        } else if (H_item.Data.leave1.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                            i -= 0.5;
                                                                        }
                                                                        if (H_item.Data.leave2.T_Day_Type_End == 'ครึ่งวัน (เช้า)') {
                                                                            i -= 0.5;
                                                                        } else if (H_item.Data.leave2.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {
                                                                            i -= 0.5;
                                                                        }
                                                                        H_item.Count = i;
                                                                    }
                                                                }
                                                            }
                                                        });

                                                        $scope.More_Leave_Data.push({
                                                            'PERID': Person_I.PERID,
                                                            'NAME': Person_I.NAME,
                                                            'SURNAME': Person_I.SURNAME,
                                                            'L_DATA': $scope.H_Leave
                                                        })
                                                    }
                                                    LeaveData = [];
                                                    LeaveData2 = [];
                                                }
                                            }
                                        }
                                    }
                                });
                            })

                            // console.log($scope.PersonDep);
                            $scope.GetReport(DateStart, DateEnd);
                            // console.log($scope.More_Leave_Data);
                            // console.log($scope.daysOfYear);
                            // $scope.Loading = false;
                        });
                });
        }

        $scope.GetReport = function (S, E) {
            $scope.ReportSummary = [];
            $scope.ReportSummaryCopy = [];
            var DateStart = S;
            var DateEnd = E;
            $scope.workLateSum = 0;
            var workLate = 0;
            $scope.workLeaveEarlySum = 0;
            var workLeaveEarly = 0;
            $scope.workAbsenceSum = 0;
            var workAbsence = 0;
            $scope.workNoDataInSum = 0;
            var workNoDataIn = 0;
            $scope.workNoDataOutSum = 0;
            var workNoDataOut = 0;
            $scope.Leave0 = 0;
            $scope.Leave1 = 0;
            $scope.Leave2 = 0;
            $scope.Leave3 = 0;
            $scope.Leave4 = 0;
            $scope.Leave5 = 0;
            $scope.Leave6 = 0;
            $scope.Leave7 = 0;
            $scope.GetDataTime = [];
            $scope.GetDataLeave = [];

            var options = {
                weekday: 'long'
            };
                if ($scope.PersonDep.length > 0) {
                        data = {
                            'Dep_Code': [{'Dep_Code': $scope.DepartSelect}],
                            'Date_Cordi': DateStart,
                            'Date_Cordi2': DateEnd,
                        };

                        $http.post('./ApiService/GetWorkReport', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                $scope.workReport = response.data;
                                angular.forEach($scope.PersonDep, function (Per_Arr) {
                                    workLate = 0;
                                    workLeaveEarly = 0;
                                    workAbsence = 0;
                                    workNoDataIn = 0;
                                    workNoDataOut = 0;
                                    $scope.GetDataTime = [];
                                    $scope.GetDataLeave = [];
                                    angular.forEach($scope.workReport, function (Work_Arr, idx) {
                                        if (Per_Arr.PERID == Work_Arr.PERID) {
                                            if ((parseInt(Per_Arr.POS_WORK)) >= 551 && ((parseInt(Per_Arr.POS_WORK)) <= 558)
                                                || (parseInt(Per_Arr.POS_WORK) == 800) || (parseInt(Per_Arr.POS_WORK) == 221)
                                                || (parseInt(Per_Arr.POS_WORK) == 211)|| (parseInt(Per_Arr.POS_WORK) == 212)){
                                                angular.forEach(Per_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                    if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                        if (Work_Arr.A_Time_in2 != '') {
                                                            $scope.GetDataTime.push({
                                                                'Day': Work_Arr.A_Datetime,
                                                                'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_in2, Work_Arr.A_Status_in2),
                                                                'Status': '',
                                                                'Comment': '',
                                                                'Comment_Date': ''
                                                            });
                                                        } else if (Work_Arr.A_Time_ho != '') {
                                                            $scope.GetDataTime.push({
                                                                'Day': Work_Arr.A_Datetime,
                                                                'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_ho, Work_Arr.A_Status_ho),
                                                                'Status': '',
                                                                'Comment': '',
                                                                'Comment_Date': ''
                                                            });
                                                        } else if (Work_Arr.A_Time_hn != '') {
                                                            $scope.GetDataTime.push({
                                                                'Day': Work_Arr.A_Datetime,
                                                                'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_hn, Work_Arr.A_Status_hn),
                                                                'Status': '',
                                                                'Comment': '',
                                                                'Comment_Date': ''
                                                            });
                                                        } else if (Work_Arr.A_Time_out2 != '') {
                                                            $scope.GetDataTime.push({
                                                                'Day': Work_Arr.A_Datetime,
                                                                'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_out2, Work_Arr.A_Status_out2),
                                                                'Status': '',
                                                                'Comment': '',
                                                                'Comment_Date': ''
                                                            });
                                                        } else {
                                                            if (Date_I.OPD) {
                                                                $scope.GetDataTime.push({
                                                                    'Day': Work_Arr.A_Datetime,
                                                                    'Time': $scope.CheckDocMaster(Per_Arr, '', 'OPD'),
                                                                    'Status': '',
                                                                    'Comment': '',
                                                                    'Comment_Date': ''
                                                                });
                                                            }
                                                        }
                                                    }
                                                });

                                                /*if (Work_Arr.A_Time_in2 != '' || Work_Arr.A_Time_ho != '' ||
                                                    Work_Arr.A_Time_hn != '' || Work_Arr.A_Time_out2 != '') {
                                                    angular.forEach($scope.daysOfYear, function (Date_I, idx_Dfy) {
                                                        if(Date_I.DAY == Work_Arr.A_Datetime){
                                                            $scope.GetDataTime.push({
                                                                'Day': Work_Arr.A_Datetime,
                                                                'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_in2, Work_Arr.A_Status_in2),
                                                                'Status': '',
                                                                'Comment': '',
                                                                'Comment_Date': ''
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    angular.forEach($scope.daysOfYear, function (Date_I, idx_Dfy) {
                                                        if(Date_I.DAY == Work_Arr.A_Datetime){
                                                            if (Date_I.OPD) {
                                                                $scope.GetDataTime.push({
                                                                    'Day': Work_Arr.A_Datetime,
                                                                    'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_in2, ''),
                                                                    'Status': '',
                                                                    'Comment': '',
                                                                    'Comment_Date': ''
                                                                });
                                                            }
                                                        }
                                                    });
                                                }*/
                                            } else {
                                                if (Work_Arr.A_Status_in2 == 'สาย') {
                                                    angular.forEach(Per_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                        if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                            if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                            } else if (Date_I.Holiday_Status) {
                                                            } else if (Date_I.Leave_Status || Date_I.leave_Detail != null) {
                                                            } else {
                                                                workLate += 1;
                                                                $scope.workLateSum += 1;
                                                                $scope.GetDataTime.push({
                                                                    'Day': Work_Arr.A_Datetime,
                                                                    'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_in2, Work_Arr.A_Status_in2),
                                                                    'Status': '',
                                                                    'Comment': '',
                                                                    'Comment_Date': ''
                                                                });
                                                            }
                                                        }
                                                    });
                                                } else if (Work_Arr.A_Status_in2 == 'ขาดงาน') {
                                                    angular.forEach(Per_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                        if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                            if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                            } else if (Date_I.Holiday_Status) {
                                                            } else if (Date_I.Leave_Status || Date_I.leave_Detail != null) {
                                                            } else {
                                                                workAbsence += 1;
                                                                $scope.workAbsenceSum += 1;
                                                                $scope.GetDataTime.push({
                                                                    'Day': Work_Arr.A_Datetime,
                                                                    'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_in2, Work_Arr.A_Status_in2),
                                                                    'Status': '',
                                                                    'Comment': '',
                                                                    'Comment_Date': ''
                                                                });
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    /*angular.forEach($scope.daysOfYear, function (Date_I, idx_Dfy) {
                                                        if(Date_I.DAY == Work_Arr.A_Datetime){
                                                            if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                            } else if (Date_I.Holiday_Status) {
                                                            } else if(Date_I.Leave_Status || Date_I.leave_Detail != null){
                                                            } else {
                                                                $scope.GetDataTime.push({
                                                                    'Day': Work_Arr.A_Datetime,
                                                                    'Time': $scope.CheckDocMaster(Per_Arr, Work_Arr.A_Time_in2, Work_Arr.A_Status_in2),
                                                                    'Status': '',
                                                                    'Comment': '',
                                                                    'Comment_Date': ''
                                                                });
                                                            }
                                                        }
                                                    });*/
                                                }
                                            }
                                        }
                                    })
                                    $scope.ReportSummaryCopy.push({
                                        'PERID': Per_Arr.PERID,
                                        'NAME': Per_Arr.NAME + " " + Per_Arr.SURNAME,
                                        'Dep_Code': Per_Arr.Dep_Code,
                                        'POS_WORK': Per_Arr.POS_WORK,
                                        'NewPos': Per_Arr.NewPos,
                                        'Date_Submit': DateStart,
                                        'workLeaveEarly': workLeaveEarly,
                                        'workLate': workLate,
                                        'workAbsence': workAbsence,
                                        'workNoDataIn': 0,
                                        'workNoDataOut': 0,
                                        'Leave0': 0,
                                        'Leave0DaySum': 0,
                                        'Leave1': 0,
                                        'Leave1DaySum': 0,
                                        'Leave2': 0,
                                        'Leave2DaySum': 0,
                                        'Leave3': 0,
                                        'Leave3DaySum': 0,
                                        'Leave4': 0,
                                        'Leave4DaySum': 0,
                                        'Leave5': 0,
                                        'Leave5DaySum': 0,
                                        'Leave6': 0,
                                        'Leave6DaySum': 0,
                                        'Leave7': 0,
                                        'Leave7DaySum': 0,
                                        'LeaveSum': 0,
                                        'DateOfMonth': 0,
                                        'WorkDaySum': 0,
                                        'WorkDaySum_PER': 0,
                                        'StatusDetails': $scope.GetDataTime,
                                        'LeaveStatusDetails': [],
                                        'LeaveStatusDetails2': [],
                                        'CheckComment': false,
                                        'DATEOFYEAR': Per_Arr.DATEOFYEAR,
                                        'Approve': false,
                                        'CheckApprove': false
                                    });
                                })

                                data = {
                                    'Dep_Code': $scope.DepartSelect,
                                    'Date_Cordi': DateStart,
                                    'Date_Cordi2': DateEnd,
                                };

                                $http.post('./ApiService/GetLeaveReportSummary', data, {
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                })
                                    .then(function successCallback(response) {
                                        $scope.leaveReport = response.data;
                                        angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                            angular.forEach($scope.leaveReport, function (Leave_Arr, idx_leave) {
                                                if (Report_Arr.PERID == Leave_Arr.PERID) {
                                                    switch (Leave_Arr.T_Leave_Type) {
                                                        case '0':
                                                            Report_Arr.Leave0 += 1;
                                                            $scope.Leave0 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave0DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ลากิจส่วนตัว',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        case '1':
                                                            Report_Arr.Leave1 += 1;
                                                            $scope.Leave1 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave1DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ลาป่วย',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        case '2':
                                                            Report_Arr.Leave2 += 1;
                                                            $scope.Leave2 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave2DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ลาพักผ่อน',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        case '3':
                                                            Report_Arr.Leave3 += 1;
                                                            $scope.Leave3 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave3DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ลาศึกษาต่อ',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        case '4':
                                                            Report_Arr.Leave4 += 1;
                                                            $scope.Leave4 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave4DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ลาฝึกอบรม ดูงาน หรือ ปฏิบัติงานวิจัย',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        case '5':
                                                            Report_Arr.Leave5 += 1;
                                                            $scope.Leave5 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave5DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ลาคลอดบุตร',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        case '6':
                                                            Report_Arr.Leave6 += 1;
                                                            $scope.Leave6 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave6DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ลาอุปสมบทหรือลาไปประกอบพิธีฮัจย์',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        case '7':
                                                            Report_Arr.Leave7 += 1;
                                                            $scope.Leave7 += 1;
                                                            if (Leave_Arr.T_Work_Summary != null) {
                                                                Report_Arr.Leave7DaySum += parseFloat(Leave_Arr.T_Work_Summary);
                                                            }
                                                            Report_Arr.LeaveStatusDetails.push({
                                                                'PERID': Leave_Arr.PERID,
                                                                'T_Leave_Type': 'ไปราชการ',
                                                                'No_Id': Leave_Arr.No_Id,
                                                                'T_Leave_Reason': Leave_Arr.T_Leave_Reason,
                                                                'T_Leave_Date_Start': Leave_Arr.T_Leave_Date_Start,
                                                                'T_Day_Type_Start': Leave_Arr.T_Day_Type_Start,
                                                                'T_Leave_Date_End': Leave_Arr.T_Leave_Date_End,
                                                                'T_Day_Type_End': Leave_Arr.T_Day_Type_End,
                                                                'T_All_Summary': Leave_Arr.T_All_Summary,
                                                                'T_Work_Summary': Leave_Arr.T_Work_Summary
                                                            });
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }

                                            });
                                        });


                                        angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                            workNoDataOut = 0;
                                            workNoDataIn = 0;

                                            angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                if (Date_I.DAY >= $scope.GetTimeToDb(new Date(date.getFullYear(), date.getMonth(), date.getDate()))) {
                                                    workNoDataIn += 1;
                                                    $scope.workNoDataInSum += 1;
                                                    Report_Arr.workNoDataIn = workNoDataIn;
                                                    Report_Arr.StatusDetails.push({
                                                        'Day': Date_I.DAY,
                                                        'Time': '',
                                                        'Status': 'ไม่มีเวลาเข้างาน',
                                                        'Status2': 'In',
                                                        'Comment': '',
                                                        'Comment_Date': ''
                                                    });
                                                }
                                            });

                                            angular.forEach($scope.workReport, function (Work_Arr, idx) {
                                                if (Work_Arr.PERID == Report_Arr.PERID) {
                                                    if ((parseInt(Report_Arr.POS_WORK)) >= 551 && ((parseInt(Report_Arr.POS_WORK)) <= 558)
                                                        || (parseInt(Report_Arr.POS_WORK) == 800) || (parseInt(Report_Arr.POS_WORK) == 221)
                                                        || (parseInt(Report_Arr.POS_WORK) == 211)|| (parseInt(Report_Arr.POS_WORK) == 212)){
                                                        if (Work_Arr.A_Time_in2 == '' && Work_Arr.A_Time_ho == '' &&
                                                            Work_Arr.A_Time_hn == '' && Work_Arr.A_Time_out2 == '') {
                                                            angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                                if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                                    if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                                    } else if (Date_I.Holiday_Status) {
                                                                    } else if (Date_I.Leave_Status || Date_I.leave_Detail != null) {
                                                                    } else {
                                                                        if (!Date_I.OPD) {
                                                                            workNoDataIn += 1;
                                                                            $scope.workNoDataInSum += 1;
                                                                            Report_Arr.workNoDataIn = workNoDataIn;
                                                                            Report_Arr.StatusDetails.push({
                                                                                'Day': Work_Arr.A_Datetime,
                                                                                'Time': Work_Arr.A_Time_in2,
                                                                                'Status': 'ไม่มีเวลาเข้างาน',
                                                                                'Status2': 'In',
                                                                                'Comment': '',
                                                                                'Comment_Date': ''
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    } else if ((Report_Arr.NewPos >= 'C53' && Report_Arr.NewPos <= 'C61')
                                                        || Report_Arr.NewPos == 'C67' || Report_Arr.NewPos == 'C91'
                                                        || Report_Arr.NewPos == 'C92') {
                                                        if (Work_Arr.A_Time_in2 == '' && Work_Arr.A_Time_ho == '' &&
                                                            Work_Arr.A_Time_hn == '' && Work_Arr.A_Time_out2 == '') {
                                                            angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                                if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                                    if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                                    } else if (Date_I.Holiday_Status) {
                                                                    } else if (Date_I.Leave_Status || Date_I.leave_Detail != null) {
                                                                    } else {
                                                                        if (!Date_I.OPD) {
                                                                            workNoDataIn += 1;
                                                                            $scope.workNoDataInSum += 1;
                                                                            Report_Arr.workNoDataIn = workNoDataIn;
                                                                            Report_Arr.StatusDetails.push({
                                                                                'Day': Work_Arr.A_Datetime,
                                                                                'Time': Work_Arr.A_Time_in2,
                                                                                'Status': 'ไม่มีเวลาเข้างาน',
                                                                                'Status2': 'In',
                                                                                'Comment': '',
                                                                                'Comment_Date': ''
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    } else {
                                                        if (Work_Arr.A_Time_in2 == '') {
                                                            angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                                if (Date_I.DAY == Work_Arr.A_Datetime) {
                                                                    if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                                                                    } else if (Date_I.Holiday_Status) {
                                                                    } else if (Date_I.Leave_Status || Date_I.leave_Detail != null) {
                                                                    } else {
                                                                        workNoDataIn += 1;
                                                                        $scope.workNoDataInSum += 1;
                                                                        Report_Arr.workNoDataIn = workNoDataIn;
                                                                        Report_Arr.StatusDetails.push({
                                                                            'Day': Work_Arr.A_Datetime,
                                                                            'Time': Work_Arr.A_Time_in2,
                                                                            'Status': 'ไม่มีเวลาเข้างาน',
                                                                            'Status2': 'In',
                                                                            'Comment': '',
                                                                            'Comment_Date': ''
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            if (Work_Arr.A_Status_in2 != 'สาย') {
                                                                Report_Arr.StatusDetails.push({
                                                                    'Day': Work_Arr.A_Datetime,
                                                                    'Time': $scope.CheckDocMaster(Report_Arr, Work_Arr.A_Time_in2, Work_Arr.A_Status_in2),
                                                                    'Status': '',
                                                                    'Status2': 'In',
                                                                    'Comment': '',
                                                                    'Comment_Date': ''
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        });

                                        angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                            angular.forEach(Report_Arr.StatusDetails, function (status_item) {
                                                angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I, idx_Dfy) {
                                                    if (Date_I.DAY == status_item.Day) {
                                                        if (Date_I.OPD) {
                                                            if (status_item.Time.indexOf('OPD') < 0) {
                                                                status_item.Time += ', OPD'
                                                            }
                                                        }
                                                    }
                                                });
                                            });
                                        });

                                        data = {
                                            'Dep_Code': $scope.DepartSelect,
                                            'Date_Cordi': DateStart,
                                            'Date_Cordi2': DateEnd,
                                        };

                                        $http.post('./ApiService/GetCommentReport', data, {
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            }
                                        })
                                            .then(function successCallback(response) {
                                                $scope.CommentReport = response.data;
                                                if ($scope.CommentReport.length > 0) {
                                                    angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                                        angular.forEach(Report_Arr.StatusDetails, function (statusDetail) {
                                                            angular.forEach($scope.CommentReport, function (CommentArr) {
                                                                if (Report_Arr.PERID == CommentArr.PERID) {
                                                                    if (statusDetail.Day == CommentArr.Comment_Date) {
                                                                        statusDetail.Comment = CommentArr.Comment_Details;
                                                                        statusDetail.Comment_Date = CommentArr.Comment_Date;
                                                                        Report_Arr.CheckComment = true;
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    });
                                                }
                                            });

                                        angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                            angular.forEach($scope.More_Leave_Data, function (Leave_Item) {
                                                if (Report_Arr.PERID == Leave_Item.PERID) {
                                                    Report_Arr.LeaveStatusDetails2 = Leave_Item.L_DATA;
                                                }
                                            });
                                        });

                                        angular.forEach($scope.ReportSummaryCopy, function (Report_Arr) {
                                            var i = 0;
                                            // if (Report_Arr.PERID == '1638') {
                                            if (Report_Arr.LeaveStatusDetails2.length > 0) {

                                                angular.forEach(Report_Arr.LeaveStatusDetails2, function (Leave_Item) {
                                                    var date1 = new Date(Leave_Item.Data.leave1.T_Leave_Date_Start);
                                                    var date2 = new Date(Leave_Item.Data.leave2.T_Leave_Date_End);

                                                    for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                        angular.forEach(Report_Arr.DATEOFYEAR, function (dayItem) {
                                                            if (dayItem.DAY == $scope.GetTimeToDb(d)) {
                                                                dayItem.LeaveCondi = true;
                                                            }
                                                        })
                                                    }

                                                    i += parseFloat(Leave_Item.Count)
                                                });

                                                $scope.ArrayLeaveOther = [];

                                                angular.forEach(Report_Arr.DATEOFYEAR, function (dayItem) {
                                                    if (dayItem.Leave_Status && !dayItem.LeaveCondi) {
                                                        angular.forEach(Report_Arr.LeaveStatusDetails, function (Leave_ItemAll, idx_L_all) {
                                                            if (dayItem.leave_Detail.No_Id == Leave_ItemAll.No_Id) {
                                                                $scope.ArrayLeaveOther.push(Leave_ItemAll)
                                                            }
                                                        })
                                                    }
                                                })

                                                $scope.ArrayLeaveOther = UniqueArraybyId($scope.ArrayLeaveOther,
                                                    "No_Id");

                                                function UniqueArraybyId(collection, keyname) {
                                                    var output = [],
                                                        keys = [];

                                                    angular.forEach(collection, function (item) {
                                                        var key = item[keyname];
                                                        if (keys.indexOf(key) === -1) {
                                                            keys.push(key);
                                                            output.push(item);
                                                        }
                                                    });
                                                    return output;
                                                };

                                                if ($scope.ArrayLeaveOther.length > 0) {
                                                    angular.forEach($scope.ArrayLeaveOther, function (Leave_Item) {
                                                        var CheckCondiFrist = true;
                                                        if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY && Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                            var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                            var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                                        } else if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY) {
                                                            var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                            var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                                        } else if (Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                            var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                            var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                                        } else {
                                                            var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                            var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                                        }

                                                        date1.setHours(0, 0, 0, 0);
                                                        date2.setHours(0, 0, 0, 0);

                                                        if (parseFloat(Leave_Item.T_Work_Summary) > 0) {
                                                            if (date1.getTime() == date2.getTime()) {
                                                                i++;
                                                                if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                    i -= 0.5;
                                                                } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                    i -= 0.5;
                                                                }
                                                            } else {
                                                                for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                                    angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I) {
                                                                            if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                                                if ($scope.GetTimeToDb(d) == $scope.GetTimeToDb(date2)) {
                                                                                    if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (เช้า)') {
                                                                                        i += 0.5;
                                                                                    } else if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {
                                                                                        i += 0.5;
                                                                                    } else {
                                                                                        i += 1;
                                                                                    }
                                                                                } else {
                                                                                    if (CheckCondiFrist) {
                                                                                        if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                                            i += 0.5;
                                                                                        } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                                            i += 0.5;
                                                                                        } else {
                                                                                            i += 1;
                                                                                        }
                                                                                        CheckCondiFrist = false;
                                                                                    } else {
                                                                                        i += 1;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    );
                                                                }
                                                            }
                                                        }
                                                    })
                                                }

                                            } else {
                                                if (Report_Arr.LeaveStatusDetails.length > 0) {
                                                    angular.forEach(Report_Arr.LeaveStatusDetails, function (Leave_Item) {
                                                        var CheckCondiFrist = true;
                                                        if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY && Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                            var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                            var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                                        } else if (Leave_Item.T_Leave_Date_Start < Report_Arr.DATEOFYEAR[0].DAY) {
                                                            var date1 = new Date(Report_Arr.DATEOFYEAR[0].DAY);
                                                            var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                                        } else if (Leave_Item.T_Leave_Date_End > Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY) {
                                                            var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                            var date2 = new Date(Report_Arr.DATEOFYEAR[Report_Arr.DATEOFYEAR.length - 1].DAY);
                                                        } else {
                                                            var date1 = new Date(Leave_Item.T_Leave_Date_Start);
                                                            var date2 = new Date(Leave_Item.T_Leave_Date_End);
                                                        }

                                                        date1.setHours(0, 0, 0, 0);
                                                        date2.setHours(0, 0, 0, 0);

                                                        if (parseFloat(Leave_Item.T_Work_Summary) > 0) {
                                                            if (date1.getTime() == date2.getTime()) {
                                                                i++;
                                                                if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                    i -= 0.5;
                                                                } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                    i -= 0.5;
                                                                }
                                                            } else {
                                                                for (var d = date1; d <= date2; d.setDate(d.getDate() + 1)) {
                                                                    angular.forEach(Report_Arr.DATEOFYEAR, function (Date_I) {
                                                                            if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                                                if ($scope.GetTimeToDb(d) == $scope.GetTimeToDb(date2)) {
                                                                                    if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (เช้า)') {
                                                                                        i += 0.5;
                                                                                    } else if (Leave_Item.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {
                                                                                        i += 0.5;
                                                                                    } else {
                                                                                        i += 1;
                                                                                    }
                                                                                } else {
                                                                                    if (CheckCondiFrist) {
                                                                                        if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (เช้า)') {
                                                                                            i += 0.5;
                                                                                        } else if (Leave_Item.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {
                                                                                            i += 0.5;
                                                                                        } else {
                                                                                            i += 1;
                                                                                        }
                                                                                        CheckCondiFrist = false;
                                                                                    } else {
                                                                                        i += 1;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    );
                                                                }
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                            // }
                                            Report_Arr.LeaveSum = i;
                                            Report_Arr.DateOfMonth = Report_Arr.DATEOFYEAR.length;
                                            Report_Arr.WorkDaySum = parseFloat(Report_Arr.DATEOFYEAR.length) - (i + parseFloat(Report_Arr.workNoDataIn) + parseFloat(Report_Arr.workAbsence));
                                            Report_Arr.WorkDaySum_PER = ((parseFloat(Report_Arr.WorkDaySum) / parseFloat(Report_Arr.DateOfMonth)) * 100);
                                        });

                                        // $scope.Loading = false;
                                        // $scope.ShowLeaveCheck($scope.ReportSummaryCopy[0]);

                                        $scope.AddWorkingHour = [];

                                        angular.forEach($scope.ReportSummaryCopy, function (Report, idf_Ri) {
                                            if ((parseInt(Report.POS_WORK)) >= 551 && ((parseInt(Report.POS_WORK)) <= 558)
                                                || (parseInt(Report.POS_WORK) == 800) || (parseInt(Report.POS_WORK) == 221)
                                                || (parseInt(Report.POS_WORK) == 211)|| (parseInt(Report.POS_WORK) == 212)){
                                                $scope.AddWorkingHour.push($scope.funcAddWorking(Report, idf_Ri, $scope.ReportSummaryCopy.length - 1))
                                            }
                                        });

                                        data = {
                                            'DepCode': [{'Dep_Code': $scope.DepartSelect}]
                                        };
                                        $http.post('./ApiService/GetRefPersonal', data, {
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            }
                                        })
                                            .then(function successCallback(response) {
                                                if (response.data.length > 0) {
                                                    angular.forEach($scope.ReportSummaryCopy, function (data_I, idx) {
                                                        angular.forEach(response.data, function (Ref_I) {
                                                            if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code == $scope.DepartSelect) {
                                                            } else if (data_I.PERID == Ref_I.PERID && Ref_I.Dep_Code_Old == $scope.DepartSelect) {
                                                                $scope.ReportSummaryCopy.splice(idx, 1)
                                                            }
                                                        })
                                                    })
                                                }
                                            });

                                        angular.forEach($scope.ReportSummaryCopy, function (report_i) {
                                            report_i.StatusDetails = UniqueArraybyId(report_i.StatusDetails,
                                                "Day");
                                        })

                                        function UniqueArraybyId(collection, keyname) {
                                            var output = [],
                                                keys = [];

                                            angular.forEach(collection, function (item) {
                                                var key = item[keyname];
                                                if (keys.indexOf(key) === -1) {
                                                    keys.push(key);
                                                    output.push(item);
                                                }
                                            });
                                            return output;
                                        };

                                        $scope.ReportSummary = $scope.ReportSummaryCopy
                                        $scope.CheckApprove();

                                        console.log($scope.ReportSummary)
                                    });
                            });
                    }

        }

        $scope.funcAddWorking = function (item, indexArr, LengthArr) {
            $http.post('./ApiService/GetDataWorkingHour', {
                'PERID': item.PERID,
                'Date_Cordi': item.DATEOFYEAR[0].DAY
            }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    var Summary_W = 0
                    if (response.data.length > 0) {
                        var CheckRemarks = false;
                        angular.forEach(response.data, function (work_h, idx_work) {
                            Summary_W += parseFloat(work_h.working_hour);
                            if (work_h.remarks != null) {
                                CheckRemarks = true
                                item.Working_H_Summary = work_h.remarks
                            } else {
                                if (idx_work == response.data.length - 1) {
                                    var StringEncode = btoa(item.DATEOFYEAR[0].DAY.split('-')[0] + '-' + item.DATEOFYEAR[0].DAY.split('-')[1] + '-' + item.Dep_Code)

                                    $http.get('https://medhr.medicine.psu.ac.th/HrDss/SyncWh.php?pid=' + StringEncode)
                                        .then(function successCallback(response) {
                                            if (response.data) {
                                            }
                                        })
                                }
                            }
                        })
                        item.Working_H = response.data;
                    }

                    if (indexArr == LengthArr) {
                        $scope.Loading = false;
                    }
                });
        }

        $scope.ShowDataWorking_H = function (H_ITEM) {
            var modalInstance = $uibModal.open({
                templateUrl: './Views/Page/modalShowDetail_A.html',
                controller: 'ModalInstanceCtrl2',
                animation: true,
                resolve: {
                    params: function () {
                        return {
                            WorkingHourData: H_ITEM
                        };
                    }
                }
            });
            modalInstance.result.then(
                function (result) {
                    if (result) {
                    }
                    // console.log('called $modalInstance.close()');
                    // console.log(result);
                },
                function (result) {
                    // console.log('called $modalInstance.dismiss()');
                    // console.log(result);
                    // alert(result);
                }
            );
        }

        $scope.htmlTrusted = function (html) {
            return $sce.trustAsHtml(html);
        }

        $scope.SaveData = function () {

        }

        // แปลงค่าข้อมูลเวลา ให้อยู่ในรูปแบบ วัน เดือน ปี ตย. 12 ธันวาคม 2561
        $scope.TimeToShow = function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var Day = value.split('-')[2];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return DateTrans;
        }

        $scope.GetTimeToDb = function (date) {
            var dd = date.getDate();
            var mm = date.getMonth() + 1;

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            return date = yyyy + '-' + mm + '-' + dd;
        }

        $scope.showWorkingHour = function (H_ITEM) {
            var modalInstance = $uibModal.open({
                templateUrl: './Views/Page/modalShowDetail_W.html',
                controller: 'ModalInstanceCtrl',
                animation: true,
                resolve: {
                    params: function () {
                        return {
                            WorkingHourData: H_ITEM
                        };
                    }
                }
            });
            modalInstance.result.then(
                function (result) {
                    if (result) {
                    }
                    // console.log('called $modalInstance.close()');
                    // console.log(result);
                },
                function (result) {
                    // console.log('called $modalInstance.dismiss()');
                    // console.log(result);
                    // alert(result);
                }
            );
        };

        $scope.ShowLeaveCheck = function (item) {
            $scope.Loading = true;
            $scope.Leave_s_ToShow = item.WorkDaySum;
            $scope.Leave_n_ToShow = item.NAME;
            $scope.events.splice(0, $scope.events.length);
            angular.forEach(item.LeaveStatusDetails, function (leave_i) {
                $scope.events.push(
                    {
                        title: leave_i.T_Leave_Type + ' | ' + $filter('DateTimeth')(leave_i.T_Leave_Date_Start) + ' (' + leave_i.T_Day_Type_Start + ')' + '  -  ' + $filter('DateTimeth')(leave_i.T_Leave_Date_End) + ' (' + leave_i.T_Day_Type_End + ')',
                        start: new Date(parseInt(leave_i.T_Leave_Date_Start.split('-')[0]), parseInt(leave_i.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_i.T_Leave_Date_Start.split('-')[2])),
                        end: new Date(parseInt(leave_i.T_Leave_Date_End.split('-')[0]), parseInt(leave_i.T_Leave_Date_End.split('-')[1]) - 1, parseInt(leave_i.T_Leave_Date_End.split('-')[2]) + 1),
                        allDay: true,
                        className: 'dayOff'
                    }
                );
            })
            angular.forEach(item.StatusDetails, function (status_i) {
                $scope.events.push(
                    {
                        title: status_i.Status + ' ' + status_i.Time,
                        start: new Date(parseInt(status_i.Day.split('-')[0]), parseInt(status_i.Day.split('-')[1]) - 1, parseInt(status_i.Day.split('-')[2])),
                        allDay: true,
                        className: 'dayCondi'
                    }
                );
            })
            if (item.LeaveStatusDetails2.length > 0) {
                angular.forEach(item.LeaveStatusDetails2, function (leave_i, idx) {
                    if (leave_i.Data.leave1.T_Leave_Date_Start < $scope.DateStart) {
                        $scope.events.push(
                            {
                                title: 'ลาคร่อม ครั้งที่ ' + (idx + 1),
                                start: new Date(parseInt($scope.DateStart.split('-')[0]), parseInt($scope.DateStart.split('-')[1]) - 1, parseInt($scope.DateStart.split('-')[2])),
                                end: new Date(parseInt(leave_i.Data.leave2.T_Leave_Date_End.split('-')[0]), parseInt(leave_i.Data.leave2.T_Leave_Date_End.split('-')[1]) - 1, parseInt(leave_i.Data.leave2.T_Leave_Date_End.split('-')[2]) + 1),
                                allDay: true,
                                color: 'yellow',
                                textColor: 'black'
                            }
                        );
                    } else if (leave_i.Data.leave2.T_Leave_Date_End > $scope.DateEnd) {
                        $scope.events.push(
                            {
                                title: 'ลาคร่อม ครั้งที่ ' + (idx + 1),
                                start: new Date(parseInt(leave_i.Data.leave1.T_Leave_Date_Start.split('-')[0]), parseInt(leave_i.Data.leave1.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_i.Data.leave1.T_Leave_Date_Start.split('-')[2])),
                                end: new Date(parseInt($scope.DateEnd.split('-')[0]), parseInt($scope.DateEnd.split('-')[1]) - 1, parseInt($scope.DateEnd.split('-')[2]) + 1),
                                allDay: true,
                                color: 'yellow',
                                textColor: 'black'
                            }
                        );
                    } else {
                        $scope.events.push(
                            {
                                title: 'ลาคร่อม ครั้งที่ ' + (idx + 1),
                                start: new Date(parseInt(leave_i.Data.leave1.T_Leave_Date_Start.split('-')[0]), parseInt(leave_i.Data.leave1.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_i.Data.leave1.T_Leave_Date_Start.split('-')[2])),
                                end: new Date(parseInt(leave_i.Data.leave2.T_Leave_Date_End.split('-')[0]), parseInt(leave_i.Data.leave2.T_Leave_Date_End.split('-')[1]) - 1, parseInt(leave_i.Data.leave2.T_Leave_Date_End.split('-')[2]) + 1),
                                allDay: true,
                                color: 'yellow',
                                textColor: 'black'
                            }
                        );
                    }
                })
            }

            angular.forEach(item.DATEOFYEAR, function (Date_I, idx_Dfy) {
                    if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                        $scope.events.push(
                            {
                                start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                allDay: true,
                                overlap: true,
                                rendering: 'background',
                                color: '#ff7b83'
                            }
                        );
                    } else if (Date_I.Holiday_Status) {
                        $scope.events.push(
                            {
                                start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                allDay: true,
                                overlap: true,
                                rendering: 'background',
                                color: '#ff7b83'
                            }
                        );
                    } else {
                    }
                }
            );

            data = {
                'PERID': item.PERID,
                'Date_Cordi': item.DATEOFYEAR[0].DAY
            };

            $http.post('./ApiService/GetDataWorkingHour', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
                .then(function successCallback(response) {
                    $scope.workingHourData = [];
                    $scope.sumworking = 0;
                    if (response.data.length > 0) {
                        $scope.workingHourData = response.data;
                        var DateDupe = '';
                        var workHour = 0;
                        var mapDate = false;
                        angular.forEach(response.data, function (work_h) {
                            workHour = 0;
                            angular.forEach(response.data, function (work_h2, idx) {
                                if (work_h.date_lu == work_h2.date_lu) {
                                    workHour += parseFloat(work_h2.working_hour);
                                    mapDate == true;
                                    if (idx === response.data.length - 1) {

                                        if (work_h.remarks != '' || work_h.remarks != null) {
                                            work_h.Sum_Hour = work_h.remarks
                                        } else {
                                            var StringEncode = btoa(item.DATEOFYEAR[0].DAY.split('-')[0] + '-' + item.DATEOFYEAR[0].DAY.split('-')[1] + '-' + item.Dep_Code)

                                            $http.get('https://medhr.medicine.psu.ac.th/HrDss/SyncWh.php?pid=' + StringEncode)
                                                .then(function successCallback(response) {
                                                    if (response.data) {
                                                    }
                                                })
                                        }
                                    }
                                } else {
                                    if (!mapDate && idx === response.data.length - 1) {
                                        if (work_h.remarks != '' || work_h.remarks != null) {
                                            work_h.Sum_Hour = work_h.remarks
                                        } else {
                                            var StringEncode = btoa(item.DATEOFYEAR[0].DAY.split('-')[0] + '-' + item.DATEOFYEAR[0].DAY.split('-')[1] + '-' + item.Dep_Code)

                                            $http.get('https://medhr.medicine.psu.ac.th/HrDss/SyncWh.php?pid=' + StringEncode)
                                                .then(function successCallback(response) {
                                                    if (response.data) {
                                                    }
                                                })
                                        }
                                    }
                                }
                            })
                            // $scope.sumworking += parseFloat(work_h.working_hour);
                        })

                        response.data = UniqueArraybyId(response.data,
                            "date_lu");

                        function UniqueArraybyId(collection, keyname) {
                            var output = [],
                                keys = [];

                            angular.forEach(collection, function (item) {
                                var key = item[keyname];
                                if (keys.indexOf(key) === -1) {
                                    keys.push(key);
                                    output.push(item);
                                }
                            });
                            return output;
                        };

                        angular.forEach(response.data, function (work_h) {
                            $scope.events.push(
                                {
                                    title: 'Working Hour : ' + work_h.Sum_Hour + ' ช.ม.',
                                    start: new Date(parseInt(work_h.date_lu.split('-')[0]), parseInt(work_h.date_lu.split('-')[1]) - 1, parseInt(work_h.date_lu.split('-')[2])),
                                    allDay: true,
                                    color: '#ff7b83'
                                }
                            );
                        })

                        data = {
                            'PERID': item.PERID,
                            'Date_Cordi': item.DATEOFYEAR[0].DAY
                        }

                        $http.post('./ApiService/GetDataWorkingSummary', data, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                if (response.data.length > 0) {
                                    $scope.sumworking = response.data[0].remarks;
                                    $scope.Loading = false;
                                } else {
                                    $scope.Loading = false;
                                }
                            });
                    } else {
                        $scope.Loading = false;
                    }
                });

            var checkDate = false;
            var checkDate2 = false;
            var checkDate_H = false;
            var checkDate_N = false;
            angular.forEach(item.DATEOFYEAR, function (Date_I, idx_Dfy) {
                    if (Date_I.DAY_NAME == 'Saturday' || Date_I.DAY_NAME == 'Sunday') {
                        // ถ้าเงื่อนไขการลา = false
                        if (!Date_I.Leave_Status) {
                            angular.forEach($scope.ReportSummary, function (Report, idf_Ri) {
                                if (Report.PERID == item.PERID) {
                                    if (Report.LeaveStatusDetails2.length > 0) {
                                        angular.forEach(Report.LeaveStatusDetails2, function (leave_I, idf_Ri) {
                                            var firstDay2 = new Date(parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[2]));
                                            var lastDay = new Date(parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[2]));
                                            for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                                                if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                    checkDate = true;
                                                }
                                            }
                                        });

                                        if (checkDate) {
                                            $scope.events.push(
                                                {
                                                    title: '+++ ไม่ดิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'red',
                                                    textColor: 'white'
                                                }
                                            );
                                        } else {
                                            $scope.events.push(
                                                {
                                                    title: '+++ คิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'green',
                                                    textColor: 'white'
                                                }
                                            );
                                        }
                                    } else {
                                        $scope.events.push(
                                            {
                                                title: '+++ คิดวันทำการ +++',
                                                start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                allDay: true,
                                                color: 'green',
                                                textColor: 'white'
                                            }
                                        );
                                    }
                                }
                            });

                        } else {

                            $scope.events.push(
                                {
                                    title: '+++ ไม่ดิดวันทำการ +++',
                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                    allDay: true,
                                    color: 'red',
                                    textColor: 'white'
                                }
                            );

                            /*if (Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_Start != 'ครึ่งวัน (บ่าย)' &&
                                Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (เช้า)' && Date_I.leave_Detail.T_Day_Type_End != 'ครึ่งวัน (บ่าย)') {
                                $scope.events.push(
                                    {
                                        title: '+++ คิดวันทำการ 0.5 +++',
                                        start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                        end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                        allDay: true,
                                        color: 'green',
                                        textColor: 'white'
                                    }
                                );
                            } else {
                                $scope.events.push(
                                    {
                                        title: '+++ ไม่ดิดวันทำการ +++',
                                        start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                        end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                        allDay: true,
                                        color: 'red',
                                        textColor: 'white'
                                    }
                                );
                            }*/
                        }
                    } else if (Date_I.Holiday_Status) {
                        if (!Date_I.Leave_Status) {
                            angular.forEach($scope.ReportSummary, function (Report, idf_Ri) {
                                if (Report.PERID == item.PERID) {
                                    if (Report.LeaveStatusDetails2.length > 0) {
                                        angular.forEach(Report.LeaveStatusDetails2, function (leave_I, idf_Ri) {
                                            var firstDay2 = new Date(parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[2]));
                                            var lastDay = new Date(parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[2]));
                                            for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                                                if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                    checkDate_H = true;

                                                }
                                            }
                                        });

                                        if (checkDate_H) {
                                            $scope.events.push(
                                                {
                                                    title: '+++ ไม่ดิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'red',
                                                    textColor: 'white'
                                                }
                                            );
                                        } else {
                                            $scope.events.push(
                                                {
                                                    title: '+++ คิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'green',
                                                    textColor: 'white'
                                                }
                                            );
                                        }
                                    } else {
                                        $scope.events.push(
                                            {
                                                title: '+++ คิดวันทำการ +++',
                                                start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                allDay: true,
                                                color: 'green',
                                                textColor: 'white'
                                            }
                                        );
                                    }
                                }
                            });

                        } else {
                            $scope.events.push(
                                {
                                    title: '+++ ไม่ดิดวันทำการ +++',
                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                    allDay: true,
                                    color: 'red',
                                    textColor: 'white'
                                }
                            );
                        }
                    } else {
                        angular.forEach($scope.ReportSummary, function (Report, idf_Ri) {
                            if (Report.PERID == item.PERID) {
                                angular.forEach(Report.StatusDetails, function (Report_I, idf_Ri) {
                                    if (Report_I.Day == Date_I.DAY) {
                                        checkDate2 = true;
                                        if (!Date_I.Leave_Status && Report_I.Time.indexOf('สแกนบัตร') >= 0 && Report_I.Time.indexOf('OPD') < 0 ||
                                            !Date_I.Leave_Status && Report_I.Time.indexOf('สแกนบัตร') >= 0 && Report_I.Time.indexOf('OPD') >= 0) {
                                            if (Report.LeaveStatusDetails2.length > 0) {
                                                angular.forEach(Report.LeaveStatusDetails2, function (leave_I, idf_Ri) {
                                                    var firstDay2 = new Date(parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[2]));
                                                    var lastDay = new Date(parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[2]));
                                                    for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                                                        if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                            checkDate_N = true;

                                                        }
                                                    }
                                                });

                                                if (checkDate_N) {
                                                    $scope.events.push(
                                                        {
                                                            title: '+++ ไม่ดิดวันทำการ +++',
                                                            start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                            end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                            allDay: true,
                                                            color: 'red',
                                                            textColor: 'white'
                                                        }
                                                    );
                                                } else {
                                                    $scope.events.push(
                                                        {
                                                            title: '+++ คิดวันทำการ +++',
                                                            start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                            end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                            allDay: true,
                                                            color: 'green',
                                                            textColor: 'white'
                                                        }
                                                    );
                                                }
                                            } else {
                                                $scope.events.push(
                                                    {
                                                        title: '+++ คิดวันทำการ +++',
                                                        start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                        end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                        allDay: true,
                                                        color: 'green',
                                                        textColor: 'white'
                                                    }
                                                );
                                            }
                                        } else if (!Date_I.Leave_Status && Report_I.Time.indexOf('สแกนบัตร') < 0 && Report_I.Time.indexOf('OPD') >= 0) {

                                            $scope.events.push(
                                                {
                                                    title: '+++ คิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'green',
                                                    textColor: 'white'
                                                }
                                            );

                                        } else if (Date_I.Leave_Status && Report_I.Time.indexOf('ไม่มีเวลาเข้างาน') >= 0 && Report_I.Time.indexOf('OPD') < 0) {
                                            $scope.events.push(
                                                {
                                                    title: '+++ ไม่ดิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'red',
                                                    textColor: 'white'
                                                }
                                            );
                                        } else if (Date_I.Leave_Status && Report_I.Time.indexOf('สแกนบัตร') >= 0) {
                                            var checkCondiLeave = false;
                                            if (Report.LeaveStatusDetails.length > 0) {
                                                angular.forEach(Report.LeaveStatusDetails, function (leaveS_Detail, l_idx) {
                                                    if (leaveS_Detail.T_Leave_Date_Start == Date_I.DAY) {
                                                        checkCondiLeave = true;
                                                        if (Date_I.leave_Detail.T_Day_Type_Start == 'ครึ่งวัน (เช้า)' || Date_I.leave_Detail.T_Day_Type_Start == 'ครึ่งวัน (บ่าย)') {

                                                            $scope.events.push(
                                                                {
                                                                    title: '+++ คิดวันทำการ 0.5 +++',
                                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    allDay: true,
                                                                    color: 'green',
                                                                    textColor: 'white'
                                                                }
                                                            );

                                                            $scope.events.push(
                                                                {
                                                                    title: '+++ ไม่ดิดวันทำการ 0.5 +++',
                                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    allDay: true,
                                                                    color: 'red',
                                                                    textColor: 'white'
                                                                }
                                                            );

                                                        } else {
                                                            $scope.events.push(
                                                                {
                                                                    title: '+++ ไม่ดิดวันทำการ +++',
                                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    allDay: true,
                                                                    color: 'red',
                                                                    textColor: 'white'
                                                                }
                                                            );
                                                        }
                                                    } else if (leaveS_Detail.T_Leave_Date_End == Date_I.DAY) {
                                                        checkCondiLeave = true;
                                                        if (Date_I.leave_Detail.T_Day_Type_End == 'ครึ่งวัน (เช้า)' || Date_I.leave_Detail.T_Day_Type_End == 'ครึ่งวัน (บ่าย)') {

                                                            $scope.events.push(
                                                                {
                                                                    title: '+++ คิดวันทำการ 0.5 +++',
                                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    allDay: true,
                                                                    color: 'green',
                                                                    textColor: 'white'
                                                                }
                                                            );

                                                            $scope.events.push(
                                                                {
                                                                    title: '+++ ไม่ดิดวันทำการ 0.5 +++',
                                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    allDay: true,
                                                                    color: 'red',
                                                                    textColor: 'white'
                                                                }
                                                            );

                                                        } else {
                                                            $scope.events.push(
                                                                {
                                                                    title: '+++ ไม่ดิดวันทำการ +++',
                                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    allDay: true,
                                                                    color: 'red',
                                                                    textColor: 'white'
                                                                }
                                                            );
                                                        }
                                                    } else {
                                                        if (l_idx == Report.LeaveStatusDetails.length - 1 && !checkCondiLeave) {
                                                            $scope.events.push(
                                                                {
                                                                    title: '+++ ไม่ดิดวันทำการ +++',
                                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                                    allDay: true,
                                                                    color: 'red',
                                                                    textColor: 'white'
                                                                }
                                                            );
                                                        }
                                                    }
                                                })
                                            }


                                        } else {
                                            $scope.events.push(
                                                {
                                                    title: '+++ ไม่ดิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'red',
                                                    textColor: 'white'
                                                }
                                            );
                                        }
                                    }
                                })

                                if (!checkDate2) {
                                    if (!Date_I.Leave_Status) {
                                        if (Report.LeaveStatusDetails2.length > 0) {
                                            angular.forEach(Report.LeaveStatusDetails2, function (leave_I, idf_Ri) {
                                                var firstDay2 = new Date(parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave1.T_Leave_Date_Start.split('-')[2]));
                                                var lastDay = new Date(parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[0]), parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[1]) - 1, parseInt(leave_I.Data.leave2.T_Leave_Date_Start.split('-')[2]));
                                                for (var d = firstDay2; d <= lastDay; d.setDate(d.getDate() + 1)) {
                                                    if ($scope.GetTimeToDb(d) == Date_I.DAY) {
                                                        checkDate = true;
                                                    }
                                                }
                                            });

                                            if (checkDate) {
                                                $scope.events.push(
                                                    {
                                                        title: '+++ ไม่ดิดวันทำการ +++',
                                                        start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                        end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                        allDay: true,
                                                        color: 'red',
                                                        textColor: 'white'
                                                    }
                                                );
                                            } else {
                                                $scope.events.push(
                                                    {
                                                        title: '+++ คิดวันทำการ +++',
                                                        start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                        end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                        allDay: true,
                                                        color: 'green',
                                                        textColor: 'white'
                                                    }
                                                );
                                            }
                                        } else {
                                            $scope.events.push(
                                                {
                                                    title: '+++ คิดวันทำการ +++',
                                                    start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                    allDay: true,
                                                    color: 'green',
                                                    textColor: 'white'
                                                }
                                            );
                                        }
                                    } else {
                                        $scope.events.push(
                                            {
                                                title: '+++ ไม่ดิดวันทำการ +++',
                                                start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                                                allDay: true,
                                                color: 'red',
                                                textColor: 'white'
                                            }
                                        );
                                    }
                                }
                            }
                        });
                    }
                    checkDate = false;
                    checkDate2 = false;
                    checkDate_H = false;
                    checkDate_N = false;
                }
            );

            angular.forEach(item.DATEOFYEAR, function (Date_I, idx_Dfy) {
                if (Date_I.OPD_COUNT > 0) {
                    $scope.events.push(
                        {
                            title: 'OPD ' + Date_I.OPD_COUNT + ' คน',
                            start: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                            end: new Date(parseInt(Date_I.DAY.split('-')[0]), parseInt(Date_I.DAY.split('-')[1]) - 1, parseInt(Date_I.DAY.split('-')[2])),
                            allDay: true,
                            color: 'white',
                            textColor: 'crimson'
                        }
                    );
                }
            })

            // $scope.events.push(
            //     {
            //         title: 'คิดวันทำการ',
            //         start: new Date(2019,11,2),
            //         end: new Date(2019,11,2),
            //         allDay: true,
            //         color: 'green',
            //         textColor: 'black'
            //     }
            // );
            // $scope.events.push(
            //     {
            //         title: 'ไม่ิดวันทำการ',
            //         start: new Date(2019,11,11),
            //         end: new Date(2019,11,11),
            //         allDay: true,
            //         color: 'red',
            //         textColor: 'black'
            //     }
            // );
        }

        /* event source that calls a function on every view switch */
        $scope.eventsF = function (start, end, timezone, callback) {
            var s = new Date(start).getTime() / 1000;
            var e = new Date(end).getTime() / 1000;
            var m = new Date(start).getMonth();
            var events = [{
                title: 'Feed Me ' + m,
                start: s + (50000),
                end: s + (100000),
                allDay: false,
                className: ['customFeed']
            }];
            callback(events);
        };

        $scope.calEventsExt = {
            color: '#f00',
            textColor: 'yellow',
            events: [
                {
                    type: 'party',
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                },
                {
                    type: 'party',
                    title: 'Lunch 2',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                },
                {
                    type: 'party',
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/'
                }
            ]
        };
        /* alert on eventClick */
        $scope.alertOnEventClick = function (date, jsEvent, view) {
            $scope.alertMessage = (date.title + ' was clicked ');
        };
        /* alert on Drop */
        $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
            $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
        };
        /* alert on Resize */
        $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
            $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
        };
        /* add and removes an event source of choice */
        $scope.addRemoveEventSource = function (sources, source) {
            var canAdd = 0;
            angular.forEach(sources, function (value, key) {
                if (sources[key] === source) {
                    sources.splice(key, 1);
                    canAdd = 1;
                }
            });
            if (canAdd === 0) {
                sources.push(source);
            }
        };
        /* add custom event*/
        $scope.addEvent = function () {
            $scope.events.push({
                title: 'Open Sesame',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                className: ['openSesame']
            });
        };
        /* remove event */
        $scope.remove = function (index) {
            $scope.events.splice(index, 1);
        };
        /* Change View */
        $scope.changeView = function (view, calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        };
        /* Change View */
        $scope.renderCalender = function (calendar) {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        };
        /* Render Tooltip */
        $scope.eventRender = function (event, element, view) {
            element.attr({
                'tooltip': event.title,
                'tooltip-append-to-body': true
            });
            $compile(element)($scope);
            if (event.className == 'dayOff') {
                element.css({
                    'background-color': '#333333',
                    'border-color': '#333333'
                });
            } else if (event.className == 'dayCondi') {
                element.css({
                    'background-color': 'white',
                    'border-color': 'white',
                    'color': 'blueviolet'
                });
            }
        };
        /* config object */
        $scope.uiConfig = {
            calendar: {
                height: 1000,
                editable: false,
                header: {
                    left: '',
                    center: 'title',
                    right: ''
                },
                locale: 'th',
                defaultDate: "2019-05-01",
                eventClick: $scope.alertOnEventClick,
                eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,
                eventRender: $scope.eventRender,
                dayClick: function (date, allDay, jsEvent, view) {
                    if ($scope.workingHourData.length > 0) {
                        $scope.Work_hArray = [];
                        angular.forEach($scope.workingHourData, function (H_ITEM) {
                            if (H_ITEM.date_lu == $scope.GetTimeToDb(new Date(date._i))) {
                                $scope.Work_hArray.push(H_ITEM)
                            }
                        })
                        if ($scope.Work_hArray.length > 0) {
                            $scope.showWorkingHour($scope.Work_hArray)
                        }
                    }
                }
            },
            eventClick: function (event) {
                console.log(event);
            }
        };

        /* event sources array*/
        $scope.eventSources = [$scope.events];
        $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];
    })

    app.controller('ModalInstanceCtrl', ['$scope', '$http', '$sce', '$uibModalInstance', 'params', function ($scope, $http, $sce, $uibModalInstance, params) {
        $scope.WorkingHourData = params.WorkingHourData;

        $scope.CloseModal = function () {
            $uibModalInstance.close();
        }

    }]);

    app.controller('ModalInstanceCtrl2', ['$scope', '$http', '$sce', '$uibModalInstance', 'DTOptionsBuilder', 'params', function ($scope, $http, $sce, $uibModalInstance, DTOptionsBuilder, params) {
        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('lengthMenu', [100, 150, 200, 250])
        $scope.WorkingHourData = params.WorkingHourData;

        $scope.CloseModal = function () {
            $uibModalInstance.close();
        }

    }]);


    app.filter('DateThai', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('/')[1];
                var Month = value.split('/')[0];
                var Year = value.split('/')[2];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    Year;
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateApprove', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined && value != '') {
                var Day = value.split('-')[2];
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ];
                DateTrans = monthNames[parseInt(Month) - 1] + " " +
                    Year;
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('DateTimeth', function () {
        return function (value) {
            var DateTrans = "";
            if (value != undefined) {
                var Day = value.split('-')[2];
                var Month = value.split('-')[1];
                var Year = value.split('-')[0];
                var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤษจิกายน', 'ธันวาคม'
                ];
                DateTrans = Day + " " + monthNames[parseInt(Month) - 1] + " " +
                    (parseInt(Year) + 543);
            }
            return (
                DateTrans
            );
        }
    });

    app.filter('timeDate', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var ampm = Hour >= 12 ? 'PM' : 'AM';
            Hour = Hour % 12;
            Hour = Hour ? Hour : 12;
            var strTime = Hour + ':' + Minutes + ' ' +
                ampm;
            return (
                strTime
                // value.getHours() >= 13 ? (value.getHours() - 12) : (value.getHours())) + ":" + (
                // value.getMinutes() < 10 ? '0' : '') + value.getMinutes() + (value.getHours() > 11 ?
                // 'pm' : 'am'
            );
        }
    });

    app.filter('timeFormat2Digit', function () {
        return function (value) {
            // value = new Date(value);
            var Hour = value.split(':')[0];
            var Minutes = value.split(':')[1];
            var strTime = Hour + ':' + Minutes
            return (
                strTime
            );
        }
    });
</script>
</body>

</html>
