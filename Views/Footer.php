<footer class="main-footer" style="padding-bottom: 35px">
    <div class="col-md-6">
        <strong>Copyright &copy; 2019 <a href="http://medinfo.psu.ac.th" style="color: chartreuse;">ฝ่ายเทคโนโลยีสารสนเทศ</a>.</strong>
        คณะแพทยศาสตร์ มหาวิทยาลัยสงขลานครินทร์ 
    </div>
    <div class="col-md-6" align="right">
        <strong>( พบปัญหาการใช้งานติดต่อ <strong style="color: darkblue">ธวัชชัย จันทรทิพย์</strong> ฝ่ายเทคโนโลยีสารสนเทศ โทร.1951 )</strong>
    </div>
</footer>
