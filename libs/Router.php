<?php
class Router
{
    public function __construct()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        session_start();
        // print_r($url);

        if (empty($url[0])) {
            require './Controller/Login.php';
            $controller = new Login();
            return false;
        }

        $file = './Controller/' . $url[0] . '.php';
        $fileApi = './libs/' . $url[0] . '.php';
        if (file_exists($file)) {
            require $file;
        } else if (file_exists($fileApi)) {
            require $fileApi;
        } else {
            require './Controller/Error.php';
            $controller = new ErrorPage();
            return false;
        }

        //$controller = new $url[0];

        if (isset($url[1])) {
            $controller = new $url[0];
            $controller->{$url[1]}();
            //$controller = new $url[0]($url[1]);
        } else {
            $controller = new $url[0];
            //$controller->loadModels($url[0]);
        }

        // if (isset($url[2])) {
        //     // $controller->{$url[1]}($url[2]);

        // } else {
        //     if (isset($url[1])) {
        //         // $controller->{$url[1]}();
        //         $controller = new $url[0]($url[1]);
        //     }
        // }
    }
}
