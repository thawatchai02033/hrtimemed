<?php

class ApiService
{
    public function __construct()
    {

    }

    public function loadModels($name)
    {
        $path = './Models/' . $name . 'Model.php';
        if (file_exists($path)) {
            require './Models/' . $name . 'Model.php';
            $Modelsname = $name . 'Model';
            $this->model = new $Modelsname;
        } else {
        }
    }

    // ฟังก์ชั่น LOGIN
    // ตรวจสอบการ Login เข้าสู่ระบบ
    public function CheckLogin()
    {
        $this->loadModels("Login");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $username = $request->Username;
        $password = $request->Password;
        $this->model->CheckLoginModel($username, $password);
    }

    // ลืมรหัส PIN
    public function GetPinNumber()
    {
        $this->loadModels("Login");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $PERSONALID = $request->PERSONALID;
        $this->model->GETPINNUMBER($PERID, $PERSONALID);
    }
    //---------------------------------------------------------------------------------//
    // ฟังก์ชั่น Date
    public function GetDateAllData()
    {
        $this->loadModels("Date");
        $this->model->DateAllData();
    }

    public function GETDATADATEDEP()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $this->model->GetDataFromDep($DepCode, $DateConrdi);
    }

    // เรียกเรียกมูลบุคลากรในหน่วยงาน
    public function D_GetPersonDepart()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETPERSONFROMDEPART($DepCode);
    }

    public function GETDATADATEDEP_CONDI()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GetDataFromDep_Condi($DepCode, $DateConrdi, $PERID);
    }

    // เลือกบุคลากรในหน่วยงานทุกคนที่ได้จากการตั้งค่าเวลาปฏิบัติงานกรณีพิเศษ โดยใช้เงื่อนไขวันที่และรหัสของฝ่ายมาเปรียบเทียบ
    public function GetPersonDate()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $this->model->GETPERSONDATE($DepCode, $DateConrdi);
    }

    // เลือกบุคลากรรายบุคคลที่ได้จากการตั้งค่าเวลาปฏิบัติงานกรณีพิเศษ โดยใช้เงื่อนไขวันที่มาเปรียบเทียบ และ PERID มาเปรียบเทียบ
    public function GetPersonDatePersonal()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GETPERSONDATEPERSONAL($DepCode, $DateConrdi, $PERID);
    }

    // เลือกบุคลากรในหน่วยงานทุกคน โดยใช้เงื่อนไขวันที่มาเปรียบเทียบ
    public function GetPersonDateNoCordition()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETPERSONDATENOCORD($DepCode);
    }

    // เลือกบุคลากรในหน่วยงานทุกคน
    public function GetTimeSetDep()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $this->model->GETDATESETTING($DateConrdi, $DepCode);
    }

    // เลือกข้อมูลตาช่วงเวลาวันที่จากผู้ใช้งาน
    public function GetRangeTime()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $TimeStart = $request->TimeStart;
        $TimeEnd = $request->TimeEnd;
        $DepCode = $request->DepCode;
        $this->model->GETRANGETIMEDATA($TimeStart, $TimeEnd, $DepCode);
    }

    //เรียกข้อมูลรหัสบัตรเข้าปฏิบัติงานรายบุคคล
    public function GetCardNo()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->GETCARDNOPERSON($PERID);
    }

    //ยืนยันข้อมูลเวลาการปัฏิบัติงานโดยหัวหน้างานหรือผู้มีสิทภายในหน่วยงานที่รับผิดชอบ
    public function ApproveData()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $C_PERID = $request->C_PERID;
        $U_PERID = $request->U_PERID;
        $OBJDATA = $request->OBJDATA;
        $DATENOW = $request->DATENOW;
        $this->model->APPROVEDATATIME($C_PERID, $U_PERID, $OBJDATA, $DATENOW);
    }

    //ยืนยันข้อมูลเวลาการปัฏิบัติงานโดยหัวหน้างานหรือผู้มีสิทภายในหน่วยงานที่รับผิดชอบ
    public function GetDataApprove()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $this->model->GETDATAAPPORVEDEPART($DepCode, $DateConrdi);
    }

    //แก้ไขข้อมูลบุคลากรภายในหน่วยงาน ที่ได้จากการ Approve
    public function UpdateDataApprove()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $OBJDATA = $request->OBJDATA;
        $this->model->UPDATEDATAFORMAPPROVE($DepCode, $DateConrdi, $PERID, $OBJDATA);
    }

    //เรียกข้อมูลการลาของบุคลากรในฝ่าย โดยใช้เงื่อนไขของวันที่
    public function GetLeavePerson()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $this->model->GETLEAVEPERSONDATA($DepCode, $DateConrdi);
    }

    //แก้ไขสเตตัสขาดงานของบุคลากร
    public function UpdateAbsence()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $No_Id = $request->No_Id;
        $PERID_person = $request->PERID_person;
        $PERID = $request->PERID;
        $DepCode = $request->DepCode;
        $DateConrdi  = $request->DateConrdi;
        $this->model->SAVEABSENCEDATA($No_Id, $PERID, $DepCode, $DateConrdi, $PERID_person);
    }

    //คำนวณหาจำนวนวัน โดยใช้วันที่ปัจจุบันลบกับวันที่เลือกทำรายการ เพื่อหาค่าจำนวนวัน < 30 ไม่อนุญาติให้แก้ไขข้อมูล
    public function CalDateDiff()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DateStart = $request->DateStart;
        $DateEnd = $request->DateEnd;
        $this->model->CALDATEDIFFCONDI($DateStart, $DateEnd);
    }

    //คำนวณหาวันที่ หักลบ จากการคลิกปุ่มเลื่อนวันที่ไปข้างหน้า หรือ หลัง
    public function CalDateSub()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DateStart = $request->DateStart;
        $IntevalDay = $request->IntevalDay;
        $this->model->CALDATESUBCONDI($DateStart, $IntevalDay);
    }

    //คำนวณหาวันที่ บวกเพิ่ม จากการคลิกปุ่มเลื่อนวันที่ไปข้างหน้า หรือ หลัง
    public function CalDateAdd()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DateStart = $request->DateStart;
        $IntevalDay = $request->IntevalDay;
        $this->model->CALDATEADDCONDI($DateStart, $IntevalDay);
    }

    //บันทึก Comment
    public function SaveComment()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $PerId = $request->PerId;
        $DateConrdi = $request->DateConrdi;
        $Comment = $request->Comment;
        $Time_I = $request->Time_I;
        $Time_O = $request->Time_O;
        $PerId_Create = $request->PerId_Create;
        $this->model->SAVECOMMENTDEP($DepCode, $PerId, $DateConrdi, $Comment, $PerId_Create, $Time_I, $Time_O);
    }

    //เรียกดูคอม Comment ประจำวัน
    public function GetComment()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $this->model->GETCOMMENTDEP($DepCode, $DateConrdi);
    }

    //เรียกดูคอม Comment ทัั้งหมด
    public function GetAllComment()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $this->model->GETALLCOMMENTDEP($DepCode, $DateConrdi);
    }

    //อนุมัติข้อมูลการขอเปลี่ยนแปลงเวลาที่ได้รับมาจากการ Comment ประจำวันจากผู้ใช้งาน
    public function ApproveCommentData()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $CommentData = $request->CommentData;
        $Condi = $request->Condi;
        $this->model->APPROVECOMMENTDATA($CommentData, $Condi);
    }

    //เรียกดูคอม Comment สำหรับออกรายงาน
    public function GetCommentReport()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETCOMMENTREPORTDEP($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    public function GetRefPersonal()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETREFPESONAL($DepCode);
    }

    public function D_GetEditTime()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $date = $request->date;
        $this->model->D_GETEDITTIME($DepCode,$date);
    }

    public function ConsiderEditTime()
    {
        $this->loadModels("Date");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $EditTimeData = $request->EditTimeData;
        $Dep_Code = $request->Dep_Code;
        $PERID = $request->PERID;
        $this->model->CONSIDEREDITTIME($EditTimeData,$Dep_Code,$PERID);
    }
    //---------------------------------------------------------------------------------//

    //---------------------------------------------------------------------------------//
    // ฟังก์ชั่น ManageTime
    // เรียกดูข้อมูลตั้งค่าเวลาตามหน่วยงาน
    public function GetDepartTimeSet()
    {
        $this->loadModels("ManageTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GET_SET_DEPART($DepCode);
    }

    // บันทึกเวลาของหน่วยงาน
    public function InsertDapartTime()
    {
        $this->loadModels("ManageTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date = $request->Date;
        $TimeN = $request->TimeN;
        $TimeO = $request->TimeO;
        $TimeHN = $request->TimeHN;
        $TimeHO = $request->TimeHO;
        $Perid = $request->Perid;
        $DepCode = $request->DepCode;
        $door_Data = $request->door_Data;
        $this->model->INSERTDEPARTTIME($Date, $TimeN, $TimeO, $TimeHN, $TimeHO, $Perid, $DepCode, $door_Data);
    }

    // บันทึกเวลาของบุคลากรรายบุคคล
    public function InsertPersonTime()
    {
        $this->loadModels("ManageTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // $Date = $request->Date;
        $TimeN = $request->TimeN;
        $TimeO = $request->TimeO;
        $TimeHN = $request->TimeHN;
        $TimeHO = $request->TimeHO;
        $DoorID = $request->DoorID;
        $Perid = $request->Perid;
        $DepCode = $request->DepCode;
        $this->model->INSERTPERSONTIME($TimeN, $TimeO, $TimeHN, $TimeHO, $DoorID, $Perid, $DepCode);
    }

    // เลือกบุคลากรในหน่วยงานทุกคน
    public function GetPersonDepart()
    {
        $this->loadModels("ManageTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETPERSONDEP($DepCode);
    }

    //---------------------------------------------------------------------------------//

    //---------------------------------------------------------------------------------//
    // ฟังก์ชั่น TakeALeave
    // เรียกเรียกมูลบุคลากรในหน่วยงาน
    public function T_GetPersonDepart()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETPERSONFROMDEPART($DepCode);
    }

    // บันทึกข้อมูลการลาของบุคลากรในหน่วยงาน
    public function T_InsertDataLeave()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DayTypeEnd = $request->DayTypeEnd;
        $DayTypeStart = $request->DayTypeStart;
        $DepCode = $request->DepCode;
        $Country = $request->Country;
        $approver = $request->approver;
        $Leave_Date_End = $request->Leave_Date_End;
        $Leave_Date_Start = $request->Leave_Date_Start;
        $Leave_Reason = $request->Leave_Reason;
        $Leave_TypeS = $request->Leave_TypeS;
        $PerId = $request->PerId;
        $PerId_Create = $request->PerId_Create;
        $DateSum = $request->DateSum;
        $DateSumWork = $request->DateSumWork;
        $this->model->INSERTDATALEAVEPERSON($DayTypeEnd, $DayTypeStart, $DepCode, $Leave_Date_End, $Leave_Date_Start, $Leave_Reason, $Leave_TypeS, $PerId, $PerId_Create, $DateSum, $DateSumWork, $Country, $approver);
    }

    // เรียกมูลบุคลากรในหน่วยงาน
    public function T_SelectLeavePersonData()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PerId = $request->PerId;
        $this->model->SELECTLEAVEPERSON($PerId);
    }

    // เรียกข้อมูลการลาของบุคลากรในหน่วยงานรายบุคคล
    public function T_GetLeavePersonData()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $PerId = $request->PerId;
        $this->model->GETLEAVEPERSONDATADEP($DepCode, $PerId);
    }

    // แก้ไขข้อมูลการลาของบุคลากรในหน่วยงานรายบุคคล
    public function T_UpdateLeavePerson()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $No_Id = $request->No_Id;
        $LeaveTSelect = $request->LeaveTSelect;
        $DateStart = $request->DateStart;
        $LeaveS_Select = $request->LeaveS_Select;
        $DateEnd = $request->DateEnd;
        $LeaveE_Select = $request->LeaveE_Select;
        $Details = $request->Details;
        $DayOff = $request->DayOff;
        $DayAllLeave = $request->DayAllLeave;
        $Status = $request->Status;
        $PERID = $request->PERID;
        $DepCode = $request->DepCode;
        $this->model->UPDATELEAVEPERSONDATA($No_Id, $LeaveTSelect, $DateStart, $LeaveS_Select, $DateEnd, $LeaveE_Select, $Details, $PERID, $Status, $DepCode, $DayOff, $DayAllLeave);
    }

    // ลบข้อมูลการลาของบุคลากรในหน่วยงานรายบุคคล
    public function T_DeleteDataLeave()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $No_Id = $request->No_Id;
        $DepCode = $request->DepCode;
        $this->model->DELETEDATA($No_Id, $DepCode);
    }

    // เรียกข้อมูลการลาของบุคลากรทุกคนในหน่วยงาน
    public function GetMoreLeave()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETMORELEAVE($DepCode);
    }
    //---------------------------------------------------------------------------------//

    //---------------------------------------------------------------------------------//
    // ฟังก์ชั่น TakeALeaveUser
    // เรียกเรียกมูลบุคลากรในหน่วยงาน

    // บันทึกข้อมูลการลาของบุคลากรในหน่วยงาน
    public function T_InsertDataLeaveUser()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DayTypeEnd = $request->DayTypeEnd;
        $DayTypeStart = $request->DayTypeStart;
        $DepCode = $request->DepCode;
        $Country = $request->Country;
        $approver = $request->approver;
        $EditCode = $request->EditCode;
        $Leave_Date_End = $request->Leave_Date_End;
        $Leave_Date_Start = $request->Leave_Date_Start;
        $Leave_Reason = $request->Leave_Reason;
        $Leave_TypeS = $request->Leave_TypeS;
        $PerId = $request->PerId;
        $PerId_Create = $request->PerId_Create;
        $DateSum = $request->DateSum;
        $DateSumWork = $request->DateSumWork;
        $this->model->INSERTDATALEAVEUSERPERSON($DayTypeEnd, $DayTypeStart, $DepCode, $EditCode, $Leave_Date_End, $Leave_Date_Start, $Leave_Reason, $Leave_TypeS, $PerId, $PerId_Create, $DateSum, $DateSumWork, $Country, $approver);
    }

    // เรียกมูลบุคลากรในหน่วยงาน
    public function T_SelectLeaveUersPersonData()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PerId = $request->PerId;
        $this->model->SELECTLEAVEUSERPERSON($PerId);
    }

    // เรียกข้อมูลการลาของบุคลากรในหน่วยงานรายบุคคล
    public function T_GetLeaveUserPersonData()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $PerId = $request->PerId;
        $this->model->GETLEAVEUSERPERSONDATADEP($DepCode, $PerId);
    }

    // อัพเดทข้อมูลวันที่รวม และวันที่ทำการรายการลาสำหรับคนวันที่ในการลา = 10 (คนที่ลาผ่านระบบเก่า)
    public function T_UpdateDateCount()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $PerId = $request->PerId;
        $DataLeaveAll = $request->DataLeaveAll;
        $this->model->UPDATEDATECOUNT($DepCode, $PerId, $DataLeaveAll);
    }

    // อัพเดทจำนวนครั้งการลา
    public function T_UpdateTime()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $ObjLeaveTime = $request->ObjLeaveTime;
        $this->model->UPDATELEAVETIME($ObjLeaveTime);
    }

    // เรียกข้อมูลการตั้งค่าเริ่มต้นของการลา
    public function T_GetLeveStart()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PerId = $request->PerId;
        $TypeLeave = $request->TypeLeave;
        $this->model->GETLEAVESTART($PerId, $TypeLeave);
    }

    // แก้ไขข้อมูลการลาของบุคลากรในหน่วยงานรายบุคคล
    public function T_UpdateLeaveUserPerson()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $No_Id = $request->No_Id;
        $LeaveTSelect = $request->LeaveTSelect;
        $DateStart = $request->DateStart;
        $LeaveS_Select = $request->LeaveS_Select;
        $DateEnd = $request->DateEnd;
        $LeaveE_Select = $request->LeaveE_Select;
        $Details = $request->Details;
        $PERID = $request->PERID;
        $this->model->UPDATELEAVEUSERPERSONDATA($No_Id, $LeaveTSelect, $DateStart, $LeaveS_Select, $DateEnd, $LeaveE_Select, $Details, $PERID);
    }

    // ลบข้อมูลการลาของบุคลากรในหน่วยงานรายบุคคล
    public function T_DeleteDataLeaveUser()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $No_Id = $request->No_Id;
        $this->model->DELETEDATAUSER($No_Id);
    }

    // เรียกข้อมูลประวัติการลาล่าสุดเพื่อนำข้อมูลไปแสดงในการพิมพ์ใบลา ป่วย ลาคลอดบุตร ลากิจกส่วนตัว
    public function T_GetHistoryLeaveUers()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PerId = $request->PerId;
        $TypeLeave = $request->TypeLeave;
        $DateCondi = $request->DateCondi;
        $this->model->GETHISTORYLEAVEUSER($PerId, $TypeLeave, $DateCondi);
    }

    // เรียกข้อมูลประวัติการลาสรุปโดยใช้เงื่อนไขปีงบประมาณ
    public function GetHistoryLeaveYear()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date_Start = $request->Date_Start;
        $Date_End = $request->Date_End;
        $PERID = $request->PERID;
        $Dep_Code = $request->Dep_Code;
        $Leave_Type = $request->Leave_Type;
        $this->model->GETHISTORYLEAVEYEAR($Date_Start, $Date_End, $PERID, $Dep_Code, $Leave_Type);
    }

    // เรียกข้อมูลประวัติการลาย้อนหลังล่าสุด
    public function GetHistoryLeaveLast()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PerId = $request->PerId;
        $TypeLeave = $request->TypeLeave;
        $DateCondi = $request->DateCondi;
        $DepCode = $request->DepCode;
        $this->model->GETHISTORYLEAVELAST($PerId, $DepCode, $TypeLeave, $DateCondi);
    }

    // เรียกข้อมูลประวัติการลาย้อนหลังล่าสุด
    public function T_GetLeaveUserSet()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $PerId = $request->PerId;
        $this->model->GETLEAVESET($DepCode, $PerId);
    }

    public function GetMaxHistoryLeave()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Year = $request->Year;
        $PerId = $request->PerId;
        $this->model->GETMAXVALUELEAVE($Year, $PerId);
    }

    public function UpdateApproveManager()
    {
        $this->loadModels("TakeALeaveUser");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $ManagerAppr = $request->ManagerAppr;
        $ApproveFrist = $request->ApproveFrist;
        $Investigate = $request->Investigate;
        $LeaveId = $request->LeaveId;
        $this->model->UPDATEAPPROVEMANAGER($ManagerAppr, $ApproveFrist, $Investigate, $LeaveId);
    }

    //---------------------------------------------------------------------------------//

    //---------------------------------------------------------------------------------//
    // ฟังก์ชั่น NoteOfLeave

    // แก้ไขอัพเดทสถานะข้อมูลใบลา ( อนุมัติ , ไม่อนุมัติ )
    public function T_UpdateDataNoteOfLeave()
    {
        $this->loadModels("NoteOfLeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $No_Id = $request->No_Id;
        $Status = $request->Status;
        $DayOff = $request->DayOff;
        $PerId_Update = $request->PerId_Update;
        $Dep_Code = $request->Dep_Code;
        $this->model->UPDATEDATANOTEOFLEAVE($No_Id, $Status, $PerId_Update, $DayOff, $Dep_Code);
    }

    // แก้ไขอัพเดทสถานะข้อมูลใบลา แบบ list รายการติ๊กเลือก ( อนุมัติ , ไม่อนุมัติ )
    public function T_ListUpdateDataNoteOfLeave()
    {
        $this->loadModels("NoteOfLeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Data_Consider = $request->Data_Consider;
        $PerId_Update = $request->PerId_Update;
        $DepCode = $request->DepCode;
        $this->model->LISTUPDATEDATANOTEOFLEAVE($Data_Consider, $PerId_Update, $DepCode);
    }

    // เรียกมูลใบลาทั้งหมดภายในหน่วยงาน
    public function T_GetDataNoteOfLeave()
    {
        $this->loadModels("NoteOfLeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETALLDATANOTEOFLEAVE($DepCode);
    }

    // ลบใบลาบุคลากรในหน่วยงาน
//    public function T_DeleteDataNoteOfLeave()
//    {
//        $this->loadModels("NoteOfLeave");
//        $postdata = file_get_contents("php://input");
//        $request = json_decode($postdata);
//        $No_Id = $request->No_Id;
//        $this->model->DELETEDATANOTEOFLEAVE($No_Id);
//    }

    // เรียกมูลใบลาทั้งหมดโดยการนับ count
    public function T_GetCountDataNoteOfLeave()
    {
        $this->loadModels("NoteOfLeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETCOUNTDATANOTEOFLEAVE($DepCode);
    }

    // ลบข้อมูลการลาของบุคลากรในหน่วยงานรายบุคคล
    public function T_DeleteDataNoteOfLeave()
    {
        $this->loadModels("TakeALeave");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $No_Id = $request->No_Id;
        $DepCode = $request->DepCode;
        $this->model->DELETEDATA($No_Id, $DepCode);
    }
    //---------------------------------------------------------------------------------//

    // MorePage (SumaryWorkPage) สรุปรายงานการปฏิบัติงานประจำช่วงเวลา

    //  เรียกข้อมูลบุคลากรในหน่วยงานทุกคน
    public function GetPersonReport()
    {
        $this->loadModels("More");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $this->model->GETPERSONALDEPART($Dep_Code);
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetLeaveReport()
    {
        $this->loadModels("More");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTLEAVESUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //  เรียกข้อมูลการปฏิบัติงานของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetWorkReport()
    {
        $this->loadModels("More");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTWORKSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //  เรียกข้อมูลบุคลากรในหน่วยงาน
    public function GETALLDATAPERSON()
    {
        $this->loadModels("More");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTWORKSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //เรียกดูคอม Comment สำหรับออกรายงาน
    public function GetAllCommentReport()
    {
        $this->loadModels("More");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETCOMMENTREPORTDEP($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }
    //---------------------------------------------------------------------------------//

    // Month (SumaryWorkOfMonthPage) สรุปรายงานการปฏิบัติงานประจำช่วงเวลา

    //  เรียกข้อมูลบุคลากรในหน่วยงานทุกคน
    public function GetPersonReportOfMonth()
    {
        $this->loadModels("Month");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $this->model->GETPERSONALDEPART($Dep_Code);
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetLeaveReportOfMonth()
    {
        $this->loadModels("Month");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTLEAVEOFMONTH($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //  เรียกข้อมูลการปฏิบัติงานของบุคลากรในหน่วยงานประจำเดือนและปี
    public function GetWorkReportOfMonth()
    {
        $this->loadModels("Month");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTWORKOFMONTH($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //  เรียกข้อมูลแพทย์ออกตรวจเพื่อนำมาคำนวณวันปฏิบัติงานของแพทย์
    public function GetDocTorSummary()
    {
        $this->loadModels("Month");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETDOCTORSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //---------------------------------------------------------------------------------//

    // MonthSummary (SumaryWorkOfMonthPageByPersonal) สรุปรายงานการปฏิบัติงานประจำช่วงเวลา

    //  เรียกข้อมูลการปฏิบัติงานของบุคลากรในหน่วยงานประจำเดือนและปี
    public function GetWorkReportBYPersonal()
    {
        $this->loadModels("MonthSummary");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->DepCode;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $PERID = $request->PERID;
        $this->model->GETREPORTWORKBYPERSONAL($Dep_Code, $Date_Cordi, $Date_Cordi2, $PERID);
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานประจำเดือนและปี
    public function GetLeaveReportBYPersonal()
    {
        $this->loadModels("MonthSummary");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $PERID = $request->PERID;
        $this->model->GETREPORTLEAVEBYPERSONAL($Dep_Code, $Date_Cordi, $Date_Cordi2, $PERID);
    }

    // เรียกเรียกมูลบุคลากรในหน่วยงาน
    public function GetPerson_MonthSum()
    {
        $this->loadModels("MonthSummary");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETPERSONFROMDEPART($DepCode);
    }

    //  เรียกข้อมูลแพทย์ออกตรวจเพื่อนำมาคำนวณวันปฏิบัติงานของแพทย์
    public function GetDocTorSummaryBy_P()
    {
        $this->loadModels("MonthSummary");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $PERID = $request->PERID;
        $this->model->GETDOCTORSUMMARYBY_P($Dep_Code, $Date_Cordi, $Date_Cordi2, $PERID);
    }

    //---------------------------------------------------------------------------------//

    // ConfigPrint หน้าต่างสำหรับตั้งค่าการพิมพ์ใบลา

    //  เพิ่มรายชื่อบุคคลผู็มีสิทธิอนุมัติใบลา โดยหน่วยงานนั้นๆ
    public function SaveConfigLeave()
    {
        $this->loadModels("ConfigPrint");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $A_M = $request->A_M;
        $A_L = $request->A_L;
        $A_P = $request->A_P;
        $A_I = $request->A_I;
        $Perid = $request->Perid;
        $DepCode = $request->DepCode;
        $this->model->SAVECONFIGFORPRINT($A_M, $A_L, $A_P, $A_I, $Perid, $DepCode);
    }

    //  เรียกข้อมูลบุคคลผู้มีสิทธิอนุมัติใบลา โดยหน่วยงานนั้นๆ
    public function GetConfigLeave()
    {
        $this->loadModels("ConfigPrint");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GETALLAPPROVEPERSONLEAVE($DepCode);
    }

    //---------------------------------------------------------------------------------//

    // RegisMailPage (RegisMail) ลงทะเบียนแจ้งเตือนผ่านไลน์

    //  บันทึกข้อมูลการลงทะเบียนแจ้งเตือนผ่านไลน์
    public function SaveDataRegis()
    {
        $this->loadModels("RegisMail");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Token = $request->Token;
        $PerID = $request->PerID;
        $DepCode = $request->DepCode;
        $this->model->SAVEDATAREGIS($Token, $PerID, $DepCode);
    }

    //---------------------------------------------------------------------------------//

    //---------------------------------------------------------------------------------//
    // ฟังก์ชั่น LeaveRegis
    // บันทึกการตั้งค่าข้อมูลการลาเริ่มต้นของบุคลากรในหน่วยงาน
    public function L_RegisSaveData()
    {
        $this->loadModels("LeaveRegis");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $DepCode = $request->DepCode;
        $_TimeLeave0 = $request->_TimeLeave0;
        $_DayLeave0 = $request->_DayLeave0;
        $_DayWorkLeave0 = $request->_DayWorkLeave0;
        $_TimeLeave1 = $request->_TimeLeave1;
        $_DayLeave1 = $request->_DayLeave1;
        $_DayWorkLeave1 = $request->_DayWorkLeave1;
        $_DayRestLeave2 = $request->_DayRestLeave2;
        $_Year = $request->_Year;
        $PERID_Update = $request->PERID_Update;
        $this->model->REGISSAVEDATA($PERID, $DepCode, $_TimeLeave0, $_DayLeave0, $_DayWorkLeave0, $_TimeLeave1, $_DayLeave1, $_DayWorkLeave1, $_DayRestLeave2, $_Year, $PERID_Update);
    }

    // เรียกข้อมูลการตั้งค่าการลาเบื้องต้น ของบุคลากรในหน่วยงาน
    public function L_GetDataRegis()
    {
        $this->loadModels("LeaveRegis");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $this->model->GERDATAREGIS($DepCode);
    }

    //---------------------------------------------------------------------------------//

    // ReportPage สรุปรายงานการปฏิบัติงานประจำเดือนระดับหน่วยงาน

    //  เรียกข้อมูลบุคลากรในหน่วยงานทุกคน
    public function GetDepartReport()
    {
        $this->loadModels("Report");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->GETDEPART();
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetDepartLeaveReport()
    {
        $this->loadModels("Report");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTLEAVESUMMARY($Date_Cordi, $Date_Cordi2);
    }

    //  เรียกข้อมูลการปฏิบัติงานของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetDepartWorkReport()
    {
        $this->loadModels("Report");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTWORKSUMMARY($Date_Cordi, $Date_Cordi2);
    }

    //  เรียกข้อมูลบุคลากรในหน่วยงาน
    public function GETALLDATADEPART()
    {
        $this->loadModels("Report");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTWORKSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    public function GetCommentDepartReport()
    {
        $this->loadModels("Report");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETCOMMENTREPORTDEP($Date_Cordi, $Date_Cordi2);
    }
    //---------------------------------------------------------------------------------//

    // ReportPage สรุปรายงานการปฏิบัติงานประจำเดือนระดับหน่วยงาน

    //  เรียกข้อมูลการปฏิบัติงานของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetDepartWorkReportOfDay()
    {
        $this->loadModels("ReportOfDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date_Cordi = $request->Date_Cordi;
        $this->model->GETREPORTWORKOFDAYSUMMARY($Date_Cordi);
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetDepartLeaveReportOfDay()
    {
        $this->loadModels("ReportOfDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date_Cordi = $request->Date_Cordi;
        $this->model->GETREPORTLEAVEOFDAYSUMMARY($Date_Cordi);
    }

    public function GetCommentDepartReportOfDay()
    {
        $this->loadModels("Report");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Date_Cordi = $request->Date_Cordi;
        $this->model->GETCOMMENTREPORTOFDAYDEP($Date_Cordi);
    }

    //---------------------------------------------------------------------------------//

    // ฟังก์ชั่น WORKTIME
    public function GETDATADATEDEP_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GetDataFromDep($DepCode, $DateConrdi, $PERID);
    }

    //ฟังก์เรียกข้อมูลเวลาเพื่อไปแสดงในรูปแบบของ timeline โดยใช้เงื่อนไขวันที่
    public function GETTIMELINEDATA_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateStart = $request->DateStart;
        $DateEnd = $request->DateEnd;
        $PERID = $request->PERID;
        $this->model->GetTimeLineData($DepCode, $DateStart, $DateEnd, $PERID);
    }

    // เลือกบุคลากรในหน่วยงานทุกคนที่ได้จากการตั้งค่าเวลาปฏิบัติงานกรณีพิเศษ โดยใช้เงื่อนไขวันที่และรหัสของฝ่ายมาเปรียบเทียบ
    public function GetPersonDate_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GETPERSONDATE($DepCode, $DateConrdi, $PERID);
    }

    //เรียกข้อมูลการลาของบุคลากรในฝ่าย โดยใช้เงื่อนไขของวันที่
    public function GetLeavePerson_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GETLEAVEPERSONDATA($DepCode, $DateConrdi, $PERID);
    }

    //ยืนยันข้อมูลเวลาการปัฏิบัติงานโดยหัวหน้างานหรือผู้มีสิทภายในหน่วยงานที่รับผิดชอบ
    public function GetDataApprove_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GETDATAAPPORVEDEPART($DepCode, $DateConrdi, $PERID);
    }

    //เรียกดูคอม Comment ประจำวัน
    public function GetComment_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GETCOMMENTDEP($DepCode, $DateConrdi, $PERID);
    }

    //เรียกดูคอม Comment ประจำวัน
    public function GetAllComment_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $PERID = $request->PERID;
        $this->model->GETALLCOMMENTDEP($DepCode, $PERID);
    }

    // เรียกเรียกมูลบุคลากรในหน่วยงาน
    public function T_GetPersonDepart_USER()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $PERID = $request->PERID;
        $this->model->GETPERSONFROMDEPART($DepCode, $PERID);
    }

    // เลือกบุคลากรในหน่วยงานทุกคน
    public function GetTimeSetDepUser()
    {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $DepCode = $request->DepCode;
        $DateConrdi = $request->DateConrdi;
        $PERID = $request->PERID;
        $this->model->GETDATESETTING($DateConrdi, $DepCode, $PERID);
    }

    //ตรวจสอบข้อมูลการลงบันทึก Comment
    public function CheckComment() {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PerId = $request->PerId;
        $DateConrdi = $request->DateConrdi;
        $this->model->CheckComment($PerId, $DateConrdi);
    }

    public function SaveEditTIme() {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $date = $request->date;
        $time_in = $request->time_in;
        $time_out = $request->time_out;
        $Create_By= $request->Create_By;
        $Create_Time = $request->Create_Time;
        $this->model->SAVADATAEDITTIME($PERID, $date, $time_in, $time_out, $Create_By, $Create_Time);
    }

    public function CheckEditTime() {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $date = $request->date;
        $this->model->CHECKDATAEDITTIME($PERID, $date);
    }

    public function getEditTime() {
        $this->loadModels("WorkTime");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $date = $request->date;
        $this->model->GETDATAEDITTIME($PERID, $date);
    }
    //---------------------------------------------------------------------------------//

    // SummaryWorkingDay สรุปวันทำการ การปฏิบัติงานประจำเดือน

    //  เรียกข้อมูลบุคลากรในหน่วยงานโดยใช้เงื่อนไข Dep_Code
    public function GetPersonFormDep()
    {
        $this->loadModels("SummaryWorkingDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $this->model->GETPERSONFORMDEP($Dep_Code);
    }

    //  เรียกข้อมูลการปฏิบัติงานจากตาราง Approve และข้อมูลการลา
    public function GetDataWorking()
    {
        $this->loadModels("SummaryWorkingDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Start = $request->Date_Start;
        $Date_End = $request->Date_End;
        $this->model->GETDATAWORKING($Dep_Code,$Date_Start,$Date_End);
    }

    public function SubmitReportSummary()
    {
        $this->loadModels("SummaryWorkingDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Report_Data = $request->Report_Data;
        $Dep_Code = $request->Dep_Code;
        $this->model->SUBMITREPORTSUMMARY($Report_Data, $Dep_Code);
    }

    //  เรียกข้อมูลเพื่อ ตรวจสอบการ Approve ข้อมูลซํ้า
    public function GetApproveDataSummary()
    {
        $this->loadModels("SummaryWorkingDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $this->model->GETAPPROVEDATASUMMARY($Dep_Code, $Date_Cordi);
    }

    //  เรียกข้อมูลบุคลากรในหน่วยงานทุกคน
    public function GetPersonReportSummary()
    {
        $this->loadModels("SummaryWorkingDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $this->model->GETPERSONALDEPART($Dep_Code);
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetLeaveReportSummary()
    {
        $this->loadModels("SummaryWorkingDay");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTLEAVESUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //---------------------------------------------------------------------------------//

    // ReportLoadUnit สรุปวันทำการ การปฏิบัติงานประจำเดือน

    //  เรียกข้อมูลบุคลากรในหน่วยงานโดยใช้เงื่อนไข PERID
    public function GetPersonFormPerid()
    {
        $this->loadModels("ReportLoadUnit");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->GETPERSONFORMPERID($PERID);
    }

    //  เรียกข้อมูลการปฏิบัติงานจากตาราง Approve และข้อมูลการลา
    public function GetDataWorkingPerson()
    {
        $this->loadModels("ReportLoadUnit");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $Date_Start = $request->Date_Start;
        $Date_End = $request->Date_End;
        $this->model->GETDATAWORKINGPERSON($PERID,$Date_Start,$Date_End);
    }

    //  เรียกข้อมูลบุคลากรในหน่วยงานทุกคน
    public function GetPersonReportSummaryPerson()
    {
        $this->loadModels("ReportLoadUnit");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->GETPERSONALDEPART($PERID);
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetLeaveReportSummaryPerson()
    {
        $this->loadModels("ReportLoadUnit");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETREPORTLEAVESUMMARY($PERID, $Date_Cordi, $Date_Cordi2);
    }

    //  เรียกข้อมูลการลาของบุคลากรในหน่วยงานในช่วงเวลาดังกล่าว
    public function GetDataWorkingHour()
    {
        $this->loadModels("ReportLoadUnit");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $Date_Cordi = $request->Date_Cordi;
        $this->model->GETDATAWORKINGHOUR($PERID, $Date_Cordi);
    }

    public function GetDataWorkingSummary()
    {
        $this->loadModels("ReportLoadUnit");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $Date_Cordi = $request->Date_Cordi;
        $this->model->GETDATAWORKINGSUMMARY($PERID, $Date_Cordi);
    }

    //  เรียกข้อมูลแพทย์ออกตรวจเพื่อนำมาคำนวณวันปฏิบัติงานของแพทย์
    public function GetDocTorSummaryLOAD()
    {
        $this->loadModels("ReportLoadUnit");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Dep_Code = $request->Dep_Code;
        $Date_Cordi = $request->Date_Cordi;
        $Date_Cordi2 = $request->Date_Cordi2;
        $this->model->GETDOCTORSUMMARY($Dep_Code, $Date_Cordi, $Date_Cordi2);
    }

    //---------------------------------------------------------------------------------//

    // ManagePermission จัดการสิทธิ์การเข้าใช้งานระบบ

    //  บันทึกสิทธิ์ รายชื่อผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function SaveMasterData()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->SAVEMASTERDATA($PERID);
    }

    //  บันทึกสิทธิ์ รายชื่อผู้ตัวแมน,รองหัวหน้า,หัวหน้าประจำหน่วยมีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function SaveViceData()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->SAVEVICEDATA($PERID);
    }

    // เรียกข้อมูล รายชื่อผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function GETMasterData()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->GETMASTERDATA();
    }

    // เรียกข้อมูล รายชื่อผู้ตัวแมน,รองหัวหน้า,หัวหน้าประจำหน่วยผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function GETViceData()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->GETVICEDATA();
    }

    // เรียกข้อมูลหน่วยงานสำหรับบุคลากรที่ได้รับสิทธิในการเข้าถึง
    public function GetPermissMaster()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->GETPERMISSMASTER($PERID);
    }

    // เรียกข้อมูลหน่วยงานสำหรับบุคลากรที่ได้รับสิทธิในการเข้าถึง
    public function GetPermissVice()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->GETPERMISSVICE($PERID);
    }

    //  บันทึกสิทธิ์ ผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function SaveDataPermissAdmin()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $PERID_PERSON = $request->PERID_PERSON;
        $Permiss_Dep = $request->Permiss_Dep;
        $this->model->SAVEDATAPERMISSADMIN($PERID,$PERID_PERSON,$Permiss_Dep);
    }

    //  บันทึกสิทธิ์ ผู้ตัวแมน,รองหัวหน้า,หัวหน้าประจำหน่วยผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function SaveDataPermissVice()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $PERID_PERSON = $request->PERID_PERSON;
        $Permiss_Dep = $request->Permiss_Dep;
        $this->model->SAVEDATAPERMISSVICE($PERID,$PERID_PERSON,$Permiss_Dep);
    }

    //  บันทึกสิทธิ์ ผู้ตัวแมน,รองหัวหน้า,หัวหน้าประจำหน่วยผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function SaveDataFuncVice()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $PERID_PERSON = $request->PERID_PERSON;
        $Func_Permiss = $request->Func_Permiss;
        $Option_Permiss = $request->Option_Permiss;
        $this->model->SAVEDATAFUNCVICE($PERID,$PERID_PERSON,$Func_Permiss,$Option_Permiss);
    }

    //  ลบข้อมูลสิทธิ์ ผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function DeleteDataPermissAdmin()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->DELETEDATAPERMISSADMIN($PERID);
    }

    //  ลบข้อมูลสิทธิ์ ผู้มีสิทธิ์ในการจัดการข้อมูลประจำหน่วยงาน ของ Admin
    public function DeleteDataPermissVice()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->DELETEDATAPERMISSVICE($PERID);
    }

    //  ลบข้อมูลผู้โอนย้ายข้อมูลสังกัดหน่วยงานอื่น โดยระบุรายบุคคล
    public function DeletePersonChangeDep()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $this->model->DELETEPERSONCHANGEDEP($PERID);
    }

    //  บันทึกข้อมูล บุคลากรมีการปรับเปลี่ยนข้อมูลหน่วยงานต้นสังกัด เพื่อให้หน่วยที่ปรับเปลี่ยนจัดการข้อมูล
    public function SaveDataPersonChangeDep()
    {
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $PERID = $request->PERID;
        $Dep_Code = $request->Dep_Code;
        $Dep_Code_Old = $request->Dep_Code_Old;
        $PERID_CREATE = $request->PERID_CREATE;
        $this->model->SAVEDATAPERSONCHANGEDEP($PERID,$Dep_Code,$Dep_Code_Old,$PERID_CREATE);
    }

    // เรียกข้อมูลบุคลากร ที่มีการย้ายรายชื่อให้่หน่วยงานอื่นรับผิดชอบดูแล
    public function getDataPersonChangeDep(){
        $this->loadModels("ManagePermission2");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->GETDATAPERSONCHANGEDEP();
    }
    //---------------------------------------------------------------------------------//

    // PermissionCheckModel

    // เรียกเรียกมูลบุคลากรในหน่วยงาน
    public function CheckPermissMenu()
    {
        $this->loadModels("PermissionCheck");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $MENU_CODE= $request->MENU_CODE;
        $this->model->CHECKPERMISSIONMENU($MENU_CODE);
    }
    //---------------------------------------------------------------------------------//

    // SaveLineNotifyModel

    //---------------------------------------------------------------------------------//

    public function saveLineNotify()
    {
        $this->loadModels("lineNotify");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $Line_Token= $request->Line_Token;
        $this->model->SAVELINENOTIFY($Line_Token);
    }
    //---------------------------------------------------------------------------------//


    // LogOut ออกจากระบบ
    public function LogOut()
    {
        // session_destroy();
        // echo "session destroy";
        // header("location: ../MVCworkTime2/Login");
        // return true;
        if (isset($_COOKIE["PERID"])) {
            //   session_start();
            echo false;
        } else {
            session_destroy();
//            unset($_COOKIE['access_token']);
//            unset($_SESSION['vurl']);
//            setcookie('access_token', null, -1, '/');
//            header( "location: https://medhr.medicine.psu.ac.th/" );
            echo true;
        }
    }
}
