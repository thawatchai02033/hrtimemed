<?php

class Database
{
    // public $hostDB = null;
    public function __construct($pathTable)
    {
        // parent::__construct('mysql:host=localhost;dbname=hrtime', 'root', '');
        // $this->hostDB = mysqli_connect(constant("DBHOST"), constant("DBUSERNAME"), constant("DBPASSWORD"), $pathTable);
        $this->OpenConnection($pathTable);
    }

    public function OpenConnection($pathTable)
    {
        // DB ตัวหลักที่
         $this->hostDB = mysqli_connect("172.29.1.6", "HRTIME", "*HRTIME*", $pathTable);
        $this->hostDB2 = mysqli_connect("172.29.1.11", "HRTIME", "*HRTIME*", $pathTable);

        // DB สำรอง
//        $this->hostDB = mysqli_connect("localhost", "root", "", $pathTable);
        $this->hostDB->set_charset("utf8");
        $this->hostDB2->set_charset("utf8");
    }

    public function CloseConnection()
    {
        if (isset($this->hostDB)) {
            mysqli_close($this->hostDB);
        }
    }
}
