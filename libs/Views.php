<?php
class Views
{
    public function __construct()
    {
    }

    public function render($page)
    {
        require './Views/' . $page . '.php';
        
    }
}
