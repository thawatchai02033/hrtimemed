<?php
class Controller
{
    public function __construct($name = false)
    {
        $this->views = new Views();
        $this->loadModels($name);
        // $this->views->msg = $this->model->GetAllData();
        //$this->views->getdata = $this->model->GetAllData();
    }

    function loadModels($name){
        $path = './Models/'.$name.'Model.php';
        if(file_exists($path)){
            require './Models/'.$name.'Model.php';
            $Modelsname = $name.'Model';
            $this->model = new $Modelsname;
            //$this->model->GetAllData();
        }
    }
}
